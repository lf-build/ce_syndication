using System;
using System.Collections.Generic;
using CreditExchange.Syndication.EmailAge.Proxy;
using Xunit;

namespace CreditExchange.Syndication.EmailAge.Tests
{
    public class EmailAgeMappingTests
    {
        [Fact]
        public void TestResultMapping()
        {
            var smLinks = new List<Smlink>()
            {
                new Smlink() {Link = "Link1", Source = "Google"},
                new Smlink() {Link = "Link2", Source = "Microsoft"}
            };
            const int smFriends = 2;
            const int domainrisklevelId = 3;
            const int domainrelevantinfoId = 4;
            const string email = "nayan.gp@sigmainfo.net";
            var firstVerificationDate = DateTimeOffset.Now.AddDays(1);
            const string location = "Ahmedabad";
            var domainAge = DateTimeOffset.Now.AddDays(2);
            const string gender = "Male";
            var emailAge = DateTimeOffset.Now.AddDays(3);
            const int eaRiskBandId = 5;
            const string fraudRisk = "R1";
            var lastVerificationDate= DateTimeOffset.Now.AddDays(4);
            const int totalhits = 29;
            const int eaStatusId = 22;
            const string status = "ok";
            const int eaReasonId = 21;
            int? uniquehits=15;
            int? eaAdviceId=16;
            const string domainname = "domain.com";
            const string title = "title";
            const string domainExists = "Yes";
            const string domaincompany = "domain";
            const string domaincountryname = "domain name";
            const string eaScore = "RS212";
            const string company = "Company";
            const string domaincorporate = "DomainCorporate";
            const string eaReason = "EaReason";
            const string domainrelevantinfo = "domain relvantinformation";
            const string eaAdvice = "Advice";
            const string eaRiskBand = "riskBand";
            const string dob = "13/11/1982";
            const string domainriskcountry = "China";
            const string domaincategory = "NoCategory";
            const string imageurl = "ImageUrl";
            const string emailExists = "EmailExist";
            const string country = "CountryDetail";
            const string domainrisklevel = "domainRiskLevel";
            const string eName = "Ename";
            var result = new VerifyEmailResponse(new Result
            {
                Smlinks = smLinks,
                SmFriends = smFriends,
                DomainrisklevelId = domainrisklevelId,
                DomainrelevantinfoId = domainrelevantinfoId,
                Email = email,
                FirstVerificationDate = firstVerificationDate,
                Location = location,
                DomainAge = domainAge,
                Gender = gender,
                EmailAge = emailAge,
                EaRiskBandId = eaRiskBandId,
                FraudRisk = fraudRisk,
                LastVerificationDate = lastVerificationDate,
                Totalhits = totalhits,
                EaStatusId = eaStatusId,
                Status = status,
                EaReasonId = eaReasonId,
                Uniquehits = uniquehits,
                EaAdviceId = eaAdviceId,
                Domainname = domainname,
                Title = title,
                DomainExists = domainExists,
                Domaincompany = domaincompany,
                Domaincountryname = domaincountryname,
                EaScore = eaScore,
                Company = company,
                Domaincorporate = domaincorporate,
                EaReason = eaReason,
                Domainrelevantinfo = domainrelevantinfo,
                EaAdvice = eaAdvice,
                EaRiskBand = eaRiskBand,
                Dob = dob,
                Domainriskcountry = domainriskcountry,
                Domaincategory = domaincategory,
                Imageurl = imageurl,
                EmailExists = emailExists,
                Country = country,
                Domainrisklevel = domainrisklevel,
                EName = eName
            });

            Assert.Equal(result.SocialMediaLinks[0].Source, smLinks[0].Source);
            Assert.Equal(result.SocialMediaLinks[0].Link, smLinks[0].Link);

            Assert.Equal(result.SocialMediaLinks[1].Source, smLinks[1].Source);
            Assert.Equal(result.SocialMediaLinks[1].Link, smLinks[1].Link);

            Assert.Equal(result.SocialMediaLinks.Count, smLinks.Count);

            Assert.Equal(result.SocialMediaFriends, smFriends);
            Assert.Equal(result.DomainRiskLevelId, domainrisklevelId);
            Assert.Equal(result.DomainRelevantInfoId, domainrelevantinfoId);
            Assert.Equal(result.Email, email);
            Assert.Equal(result.FirstVerificationDate, firstVerificationDate);
            Assert.Equal(result.Location, location);
            Assert.Equal(result.DomainAge, domainAge);
            Assert.Equal(result.Gender, gender);
            Assert.Equal(result.EmailAge, emailAge);
            Assert.Equal(result.RiskBandId, eaRiskBandId);

            Assert.Equal(result.FraudRisk, fraudRisk);
            Assert.Equal(result.LastVerificationDate, lastVerificationDate);
            Assert.Equal(result.TotalHits, totalhits);
            Assert.Equal(result.StatusId, eaStatusId);
            Assert.Equal(result.Status, status);
            Assert.Equal(result.ReasonId, eaReasonId);

            Assert.Equal(result.UniqueHits, uniquehits);
            Assert.Equal(result.AdviceId, eaAdviceId);
            Assert.Equal(result.DomainName, domainname);
            Assert.Equal(result.Title, title);
            Assert.Equal(result.DomainExists, domainExists);
            Assert.Equal(result.DomainCompany, domaincompany);
            Assert.Equal(result.DomainCountryName, domaincountryname);
            Assert.Equal(result.Score, eaScore);
            Assert.Equal(result.Company, company);
            Assert.Equal(result.DomainCorporate, domaincorporate);
            Assert.Equal(result.Reason, eaReason);
            Assert.Equal(result.DomainRelevantInfo, domainrelevantinfo);
            Assert.Equal(result.Advice, eaAdvice);
            Assert.Equal(result.RiskBand, eaRiskBand);
            Assert.Equal(result.DateOfBirth, dob);
            Assert.Equal(result.DomainRiskCountry, domainriskcountry);
            Assert.Equal(result.DomainCategory, domaincategory);
            Assert.Equal(result.ImageUrl, imageurl);
            Assert.Equal(result.EmailExists, emailExists);
            Assert.Equal(result.Country, country);
            Assert.Equal(result.DomainRiskLevel, domainrisklevel);
            Assert.Equal(result.Name, eName);


        }

        [Fact]
        public void TestResultMappingWithSomeValuesNull()
        {
            List<Smlink> smLinks = null;
            const int smFriends = 2;
            const int domainrisklevelId = 3;
            const int domainrelevantinfoId = 4;
            const string email = "nayan.gp@sigmainfo.net";
            var firstVerificationDate = DateTimeOffset.Now.AddDays(1);
            const string location = "Ahmedabad";
            var domainAge = DateTimeOffset.Now.AddDays(2);
            const string gender = "Male";
            var emailAge = DateTimeOffset.Now.AddDays(3);
            const int eaRiskBandId = 5;
            const string fraudRisk = "R1";
            var lastVerificationDate = DateTimeOffset.Now.AddDays(4);
            const int totalhits = 29;
            const int eaStatusId = 22;
            const string status = "ok";
            const int eaReasonId = 21;
            int? uniquehits = null;
            int? eaAdviceId = null;
            const string domainname = "domain.com";
            const string title = null;
            const string domainExists = "Yes";
            const string domaincompany = "domain";
            const string domaincountryname = "domain name";
            const string eaScore = "RS212";
            const string company = "Company";
            const string domaincorporate = null;
            const string eaReason = "EaReason";
            const string domainrelevantinfo = "domain relvantinformation";
            const string eaAdvice = "Advice";
            const string eaRiskBand = "riskBand";
            const string dob = "13/11/1982";
            const string domainriskcountry = "China";
            const string domaincategory = "NoCategory";
            const string imageurl = "ImageUrl";
            const string emailExists = "EmailExist";
            const string country = "CountryDetail";
            const string domainrisklevel = "domainRiskLevel";
            const string eName = "Ename";
            var result = new VerifyEmailResponse(new Result
            {
                Smlinks = smLinks,
                SmFriends = smFriends,
                DomainrisklevelId = domainrisklevelId,
                DomainrelevantinfoId = domainrelevantinfoId,
                Email = email,
                FirstVerificationDate = firstVerificationDate,
                Location = location,
                DomainAge = domainAge,
                Gender = gender,
                EmailAge = emailAge,
                EaRiskBandId = eaRiskBandId,
                FraudRisk = fraudRisk,
                LastVerificationDate = lastVerificationDate,
                Totalhits = totalhits,
                EaStatusId = eaStatusId,
                Status = status,
                EaReasonId = eaReasonId,
                Uniquehits = uniquehits,
                EaAdviceId = eaAdviceId,
                Domainname = domainname,
                Title = title,
                DomainExists = domainExists,
                Domaincompany = domaincompany,
                Domaincountryname = domaincountryname,
                EaScore = eaScore,
                Company = company,
                Domaincorporate = domaincorporate,
                EaReason = eaReason,
                Domainrelevantinfo = domainrelevantinfo,
                EaAdvice = eaAdvice,
                EaRiskBand = eaRiskBand,
                Dob = dob,
                Domainriskcountry = domainriskcountry,
                Domaincategory = domaincategory,
                Imageurl = imageurl,
                EmailExists = emailExists,
                Country = country,
                Domainrisklevel = domainrisklevel,
                EName = eName
            });

            Assert.Null(result.SocialMediaLinks);

            Assert.Equal(result.SocialMediaFriends, smFriends);
            Assert.Equal(result.DomainRiskLevelId, domainrisklevelId);
            Assert.Equal(result.DomainRelevantInfoId, domainrelevantinfoId);
            Assert.Equal(result.Email, email);
            Assert.Equal(result.FirstVerificationDate, firstVerificationDate);
            Assert.Equal(result.Location, location);
            Assert.Equal(result.DomainAge, domainAge);
            Assert.Equal(result.Gender, gender);
            Assert.Equal(result.EmailAge, emailAge);
            Assert.Equal(result.RiskBandId, eaRiskBandId);

            Assert.Equal(result.FraudRisk, fraudRisk);
            Assert.Equal(result.LastVerificationDate, lastVerificationDate);
            Assert.Equal(result.TotalHits, totalhits);
            Assert.Equal(result.StatusId, eaStatusId);
            Assert.Equal(result.Status, status);
            Assert.Equal(result.ReasonId, eaReasonId);

            Assert.Equal(result.UniqueHits, uniquehits);
            Assert.Equal(result.AdviceId, eaAdviceId);
            Assert.Equal(result.DomainName, domainname);
            Assert.Equal(result.Title, title);
            Assert.Equal(result.DomainExists, domainExists);
            Assert.Equal(result.DomainCompany, domaincompany);
            Assert.Equal(result.DomainCountryName, domaincountryname);
            Assert.Equal(result.Score, eaScore);
            Assert.Equal(result.Company, company);
            Assert.Equal(result.DomainCorporate, domaincorporate);
            Assert.Equal(result.Reason, eaReason);
            Assert.Equal(result.DomainRelevantInfo, domainrelevantinfo);
            Assert.Equal(result.Advice, eaAdvice);
            Assert.Equal(result.RiskBand, eaRiskBand);
            Assert.Equal(result.DateOfBirth, dob);
            Assert.Equal(result.DomainRiskCountry, domainriskcountry);
            Assert.Equal(result.DomainCategory, domaincategory);
            Assert.Equal(result.ImageUrl, imageurl);
            Assert.Equal(result.EmailExists, emailExists);
            Assert.Equal(result.Country, country);
            Assert.Equal(result.DomainRiskLevel, domainrisklevel);
            Assert.Equal(result.Name, eName);


        }

        [Fact]
        public void TestResultMappingWithSomeValuesNullAndEmpty()
        {
            var smLinks = new List<Smlink>();
            const int smFriends = 2;
            const int domainrisklevelId = 3;
            const int domainrelevantinfoId = 4;
            const string email = "";
            var firstVerificationDate = DateTimeOffset.Now.AddDays(1);
            const string location = "Ahmedabad";
            var domainAge = DateTimeOffset.Now.AddDays(2);
            const string gender = "Male";
            var emailAge = DateTimeOffset.Now.AddDays(3);
            const int eaRiskBandId = 5;
            const string fraudRisk = "R1";
            var lastVerificationDate = DateTimeOffset.Now.AddDays(4);
            const int totalhits = 29;
            const int eaStatusId = 22;
            const string status = "ok";
            const int eaReasonId = 21;
            int? uniquehits = null;
            int? eaAdviceId = null;
            const string domainname = "domain.com";
            const string title = null;
            const string domainExists = "Yes";
            const string domaincompany = "";
            const string domaincountryname = "domain name";
            const string eaScore = "RS212";
            const string company = "Company";
            const string domaincorporate = null;
            const string eaReason = "EaReason";
            const string domainrelevantinfo = "domain relvantinformation";
            const string eaAdvice = "Advice";
            const string eaRiskBand = "riskBand";
            const string dob = "13/11/1982";
            const string domainriskcountry = "China";
            const string domaincategory = "NoCategory";
            const string imageurl = "ImageUrl";
            const string emailExists = "EmailExist";
            const string country = "CountryDetail";
            const string domainrisklevel = null;
            const string eName = "Ename";
            var result = new VerifyEmailResponse(new Result
            {
                Smlinks = smLinks,
                SmFriends = smFriends,
                DomainrisklevelId = domainrisklevelId,
                DomainrelevantinfoId = domainrelevantinfoId,
                Email = email,
                FirstVerificationDate = firstVerificationDate,
                Location = location,
                DomainAge = domainAge,
                Gender = gender,
                EmailAge = emailAge,
                EaRiskBandId = eaRiskBandId,
                FraudRisk = fraudRisk,
                LastVerificationDate = lastVerificationDate,
                Totalhits = totalhits,
                EaStatusId = eaStatusId,
                Status = status,
                EaReasonId = eaReasonId,
                Uniquehits = uniquehits,
                EaAdviceId = eaAdviceId,
                Domainname = domainname,
                Title = title,
                DomainExists = domainExists,
                Domaincompany = domaincompany,
                Domaincountryname = domaincountryname,
                EaScore = eaScore,
                Company = company,
                Domaincorporate = domaincorporate,
                EaReason = eaReason,
                Domainrelevantinfo = domainrelevantinfo,
                EaAdvice = eaAdvice,
                EaRiskBand = eaRiskBand,
                Dob = dob,
                Domainriskcountry = domainriskcountry,
                Domaincategory = domaincategory,
                Imageurl = imageurl,
                EmailExists = emailExists,
                Country = country,
                Domainrisklevel = domainrisklevel,
                EName = eName
            });

            Assert.NotNull(result.SocialMediaLinks);
            Assert.Empty(result.SocialMediaLinks);

            Assert.Equal(result.SocialMediaFriends, smFriends);
            Assert.Equal(result.DomainRiskLevelId, domainrisklevelId);
            Assert.Equal(result.DomainRelevantInfoId, domainrelevantinfoId);
            Assert.Equal(result.Email, email);
            Assert.Equal(result.FirstVerificationDate, firstVerificationDate);
            Assert.Equal(result.Location, location);
            Assert.Equal(result.DomainAge, domainAge);
            Assert.Equal(result.Gender, gender);
            Assert.Equal(result.EmailAge, emailAge);
            Assert.Equal(result.RiskBandId, eaRiskBandId);

            Assert.Equal(result.FraudRisk, fraudRisk);
            Assert.Equal(result.LastVerificationDate, lastVerificationDate);
            Assert.Equal(result.TotalHits, totalhits);
            Assert.Equal(result.StatusId, eaStatusId);
            Assert.Equal(result.Status, status);
            Assert.Equal(result.ReasonId, eaReasonId);

            Assert.Equal(result.UniqueHits, uniquehits);
            Assert.Equal(result.AdviceId, eaAdviceId);
            Assert.Equal(result.DomainName, domainname);
            Assert.Equal(result.Title, title);
            Assert.Equal(result.DomainExists, domainExists);
            Assert.Equal(result.DomainCompany, domaincompany);
            Assert.Equal(result.DomainCountryName, domaincountryname);
            Assert.Equal(result.Score, eaScore);
            Assert.Equal(result.Company, company);
            Assert.Equal(result.DomainCorporate, domaincorporate);
            Assert.Equal(result.Reason, eaReason);
            Assert.Equal(result.DomainRelevantInfo, domainrelevantinfo);
            Assert.Equal(result.Advice, eaAdvice);
            Assert.Equal(result.RiskBand, eaRiskBand);
            Assert.Equal(result.DateOfBirth, dob);
            Assert.Equal(result.DomainRiskCountry, domainriskcountry);
            Assert.Equal(result.DomainCategory, domaincategory);
            Assert.Equal(result.ImageUrl, imageurl);
            Assert.Equal(result.EmailExists, emailExists);
            Assert.Equal(result.Country, country);
            Assert.Equal(result.DomainRiskLevel, domainrisklevel);
            Assert.Equal(result.Name, eName);


        }
    }
}