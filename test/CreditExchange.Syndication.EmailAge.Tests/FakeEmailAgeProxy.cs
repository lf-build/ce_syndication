﻿using CreditExchange.Syndication.EmailAge.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.EmailAge.Tests
{
    public class FakeEmailAgeProxy : IEmailAgeProxy
    {
        public Result VerifyEmail(string email)
        {
            return VerifyEmail_Response();
        }
        public static Result VerifyEmail_Response()
        {
            var response = new Result()
            {
                Company = "Company",
                Country = "CountryDetail",
                Dob = "13/11/1982",
                DomainAge = DateTimeOffset.Now.AddDays(2),
                Domaincategory = "NoCategory",
                Domaincompany = "domain",
                Domaincorporate = "DomainCorporate",
                Domaincountryname = "domain name",
                DomainExists = "Yes",
                Domainname = "domain.com",
                Domainrelevantinfo = "domain relvantinformation",
                DomainrelevantinfoId = 4,
                Domainriskcountry = "China",
                Domainrisklevel = "domainRiskLevel",
                DomainrisklevelId = 3,
                EaAdvice = "Advice",
                EaAdviceId = 16,
                EaReason = "EaReason",
                EaReasonId = 21,
                EaRiskBand = "riskBand",
                EaRiskBandId = 5,
                EaScore = "RS212",
                EaStatusId = 22,
                Email = "nayan.gp@sigmainfo.net",
                EmailAge = DateTimeOffset.Now.AddDays(3),
                EmailExists = "EmailExist",
                EName = "Ename",
                FirstVerificationDate = DateTimeOffset.Now.AddDays(1),
                FraudRisk = "R1",
                Gender = "Male",
                Imageurl = "ImageUrl",
                LastVerificationDate = DateTimeOffset.Now.AddDays(4),
                Location = "Ahmedabad",
                SmFriends = 2,
                Smlinks = new List<Smlink>()
                {
                    new Smlink()
                    {
                        Link="Link1",
                        Source="Google"
                    }

                },
                Status = "ok",
                Title = "title",
                Totalhits = 29,
                Uniquehits = 15

            };
            return response;
        }
    }
}

