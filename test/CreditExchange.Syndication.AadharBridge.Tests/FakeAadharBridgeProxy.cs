﻿
using CreditExchange.Syndication.AadharBridge.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditExchange.Syndication.AadharBridge.Model;

namespace CreditExchange.Syndication.AadharBridge.Tests
{
    public class FakeAadharBridgeProxy : IAadhaarBridgeProxy
    {
        //public class FakeAadharBridgeProxy : IAadhaarBridgeProxy
        //{
        //    public IAadhaarOtpResponse GenerateOneTimePassword(string aadharId)
        //    {
        //       if(aadharId== "715515757356")
        //         return GenerateOneTimePassword_Response();

        //        return null;
        //    }

        //    public IAadhaarKycResponse VerifyAadhar(IKYC request)
        //    {
        //        if (request.AuthCaptureRequest.AadhaarId == "715515757356")
        //            return KnowYourCustomer_Response();
        //        return null;
        //    }

        //    public IAadhaarAuthResponse IsAadharValid(IAadhaarAuthRequest request)
        //    {
        //        if (request.AadharId == "715515757356")
        //            return VerifyAadhaar_Response();
        //        return null;
        //    }

        //    public static AadhaarOtpResponse GenerateOneTimePassword_Response()
        //    {
        //        var response = new AadhaarOtpResponse
        //        {
        //           AadharReferenceCode = "c56e45be50764ec2bcd089705f74f3e6",
        //           AadharStatusCode="200" ,
        //            Success=true
        //        };
        //        return response;
        //    }
        //    public static AadhaarKycResponse KnowYourCustomer_Response()
        //    {
        //        var response = new AadhaarKycResponse
        //        {
        //          AadharReferenceCode = "b356024aaa5743d9b58ebe330d56c8a9",
        //          AadharStatusCode ="",
        //          Kyc =new AadhaarKyc
        //          {
        //           Photo = "/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0a\r\nHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIy\r\nMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADIAKADASIA\r\nAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQA\r\nAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3\r\nODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWm\r\np6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEA\r\nAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSEx\r\nBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElK\r\nU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3\r\nuLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD1wUve\r\ngClxWJIClopaAE706k7inYoAB0pe1GKXHFACYp1JilxQAUUuKO1AxKKXHNLjmgBmGznPHpSgZOaU\r\n0uMUWAaeTS9KXFHrQA2kIp1GKAIKKbzmnUxAfumlBpKUUAOpabSg0DH8E0UmaTcDTAdS1HuHrS5o\r\nAfRmmq2eaXNFgHZ4zS03qaWkAoooFHegA7UUUtACEYpMUtFAFQA96dQKBQAUUvFGKADNBYL1IH1O\r\nKRiRgevf0rL1PVrXTYRLcyFVYEpGBl5cdSBnpyOTx0yeRRYC5d30VqgLkgk46HA9ST0AHvXL6v4u\r\ntbSGVz5LzLwsdw5RXGT0BwxOBkHbg9A3rx+teK77U3Ntbj7IFOD5bZKZ4+9jOeowADj05B5yPS3k\r\nlLTMTlsk46k09EtRqLZ2F58ShCcWJuJcvuIkVY1UcfKuAxx25wferln8WISrfbLF1bPyrAQRj33E\r\nVy9tpEIOCn51oR6PblceWDUOtFdDX2LO3tPiL4fngMk081qc42SQs5Pv8gatiz8R6RqDqlrqVrI7\r\nDKp5gDY/3Tg15XL4ct3B2Ext6isq88PXcat5TCVT2PWhVIsTpSR79kjg8H0pc8da8H0XxfrnhydY\r\nWeSS3HHkzHcuOOnp+BFeu+G/EUXiG1MyQvGQSuCcgkAH+tWZvTc3QcULSZzThxQAGlpDS4osIMUl\r\nOxxSYoAq0opKM8UgFpaYDkU7PFAFHUryOztnmmbCL7Zz+A6+mO5OOuK8q8Wa7Le35sIywlJAuArb\r\njuH8J7EjgccKQQOrE9F471w2mx0fH2dg0a7ThpOQM/7p+bPTOOcjB4bwzYgq19IvzHhB6CneyuVG\r\nPM7GhZ6cYIVDdeu3sv8A9etBLcLjjFSrgngVLt/KuWcmzuhBJDUTBxirkAULjFQop/CpeR0rNFNX\r\nHsFx2qpIQvQ9O1TsxCk96oXErY6VVybEVxa214CsyA+9c/eS6loUoFreXIteiqshAUfTNbW8g5Bq\r\nK7jW6iZHHatYT5WZTgmbnhX4iOrwW2qS74iCDMwGV44PA6dR+XvXqSurqGQgqwyCO9fMksT2c5hY\r\ncfwmvW/hp4jN9pLaZcN++tMCPJ5MfbHPbp+Iro0eqOW1tD0IYpetMDgjinDikIdSZpN1HWgCpnNB\r\n6UUfjQMRfWmXMwgtpZSMhFLYHenk1leIrn7F4a1G5LKGS3faW6bsYA/E4H40gPGPGd+1/rkkCsWS\r\nBvJUsMEleGJ+rbq2LBRb2MUY7KK5K3Q3F5Fkkndkk8k110QwoFFTRWNqKNC3YEVaAz1xVK3Uk5xV\r\n1Vb1rnkdaJEANObINCKcdc1IyjFZjKxBaqk0LVoHailiQB61mXer2yL8rg01FsluxWZCOeaj/lVa\r\nbWlYEKoPpRb3iSk7uD71fI0jPnTM/VoRInPUdDU/gfUH07xPa5YiO4zA/wCPT/x4LVm9h82Bsdfa\r\nuVZ5IpwyNtdGyp9COhrek7qxhVWtz6TtZ969eaubqwNMuVkCOrBlYAgjoa2w2e9UZEgNLmmjgYoo\r\nAr5pVpvalHrSAXiuV+IrMPBl1GoOJXjQ4GcfMGH5lQPxrqarX9pFfWrQzxLKh/gbv2/PBNMD5608\r\nbZYz3ziunkuI4FUucZrL1nSzoeutZs+9Vkyj4xuQ8g/XHB9was3MLTXA3fdAwBmlOztc1pvTQsDW\r\n4omGCcVbttfilOOayzYWarhzuY9qge1jiG5EdR2yMVD5exquZbnZQ3sTgHNOku0K8HIrkra7lRgo\r\nGV7mumt4jNa7gecZFZyVjaOpm396drJuwCa5yaNrmU4b5c9av6gkj3BRjxn86Zb2jSswWSGPHRpm\r\nwCfQCqizOaRDHbW0SjLZb3apAI92VIqgZ9QllWN12oM5LRgD8DipbaKaWXG0KPUVTVupCa7G1bDe\r\nm0/SuW1BRFqEiN8oDHOa66KPy0AyOBWRq1rH9rinK/e6ketKnL3hVI6HqmkwvZWFrBI2XhjSNj6k\r\nACumt33KDXA+Fb2S4s/KYnbGAI8+nT/Cu5teIxWidzCSadmXc+9FRZpd1O4iEHvSk00U6kAoz3NJ\r\nk0ZHSkzjmmB5t8UNMLS2l9EiAlSrEL8zkc/jgZ/DNYEe6Szhk25do1JHvivRPGuni/8AD8kipme3\r\ndZIyBk4zhh9MEn8K4CxObNBjBAI/I4qZ7GtIyALjztvzRIfvP/EfpUFvZTG8QzSiVEfPOcsPc10P\r\nkb2yQMCplt1BHFTztaG3JcoR2+2Usi7dxxtBOB+db9kf3KgemKpTrsVdo5z0FaWnIWj+UZxWctUb\r\nQVjGv7YM7A/e9apxREcN1HANa9+hM/IwSe9VGgMEoZvmQ9faoiwkkQGwWQknJNSJaJCmcYrS2LsD\r\nKcrUMr8DP6U3JiUUVjjnIx6VSvoxLBz/AAnNW5Wy2Rn2FVpGBGM9aqO5E7WOn8NfKoAGPlJ/AkV2\r\n9lLlMGuI0Es0oIBCCHA/Mf8A1/yrr7DqetaQ2Oer8RrZ4ozUWT61IDxVGY0GlFNpc0xC9qa3SlHW\r\nkamBDNGk0LxSDdG6lWHqDwRXltxYy6bqFzbSsCRIWGOmD/nP0Ir1RzgVxvjS2KfZr9AcbvJkABOM\r\n5IPsOCMn1ApNGlOVmc9HgH5sH0xVkKODWcH56kCraS8AelZM64sluZlt/wB4ULKo6Cq1rrWwsUSR\r\nOcjC06WbcpHc+tRoilC7MiDpk0WQOTvZFe61Ga7k/cwuzdSXG0CoYWvZrhRcOuwEE7eKtCWKIEu6\r\ngeo5JpnnQuf3UgJ9DwamyQNy3L2fLJ/umoJJOCc4Aqq9w6nBX3FJ5m4c4xSa0BSQFs8561UdyX+l\r\nSyvkVXb7wB5qooymzrvDN3FKDAoxIqZYemDj+ortbBetYOlH/iVWfPHkpj6YFbtk2DitUrHPJ3Zp\r\nbc04CkB4FLQIiFLmkFL3qhBnikJopD0oAjc81geJ9Pu9Sso4rUBgGyyFsc8YOfbmt5qiega0PJl3\r\noSjqVZThlYcgjtU6N2FaPirT/sOpfao0xBc5Y4HCv/F+fX3JPpWPFKOp71nJHRTkEj+W5ZifYDvU\r\nax3N5KS58pB0zzxUj7RIGPSnYeQ/uzg9BSTNFoxf7JTA3zsf0qrNbWyEqmSfXJqZtNvZG3NcY9hS\r\n/wBlGIbpZmY+mabaNHNPSxXWPYuVdvxOaUSfuyOM0kq7OAaqs+3PFQlcyk0iVnwfamKDNNHGhw7s\r\nFXPTJOBUbPkVVmvZbXEsDlXQjDDsauK1sZSZ65bokcaRxjCKAoHoBW1Zr8o4ry7QfGcyyJHqZDxk\r\n/wCuVcMv1A4I+gz9a9UsmSSBJI3V0YBlZTkEHoQe9W4tGF7lsHFLmkoFIY2im5pdwqhC0hpN1Ixo\r\nAa1ZuraraaRaNc3cm1eiqOWc+ijuf0HfAqTVdSg0mwlu7g/Ki5CjqxyAAPxIH414vq+qXOqXslzc\r\nyMzsTgE8IP7o9B/+uqirsGW/EHii81i7VmOy3jJKQr0HufU47n8MZNRW0wmQNG3/ANasZsnPNS6a\r\nlw96kFuhd5DgKKucVYISszYaRwRkHrU0NwFHDZqNeuyRcMDgg0pt42bOMH1BxXPobq6Lpu32ct9a\r\ngmuHC9ePrVf7KSc+c350htYyPmdm/GlZF8zIJboE8kknsKjCu53NwOwqwyRRZ2gD+tQvIW4HSmvI\r\nhp9SORsDavU1DcWxa0bPUkVdit8De/Sumh8OldAvr67jIdbVpIYj/DwcE++O3bPPPTOdaNPVha5w\r\n0Q2/hXb+DPFf9myrp96+LRzhHPSJj6/7J9ex56ZI4sLT1OGxXc1dWZyn0GjhlyDmnZrg9Ll8UJod\r\ntdaZLa3imID7NcphlxgAhgRuyOckj8a6vTLnUZbdTqVrBbzFQdsUxfnvkYwPoC31rgeIop25ka8r\r\n7FnNRT3dva7PtFxFCHbanmOF3N6DPU+1Q/vSfnlJ4xtUbR9fX9aqXV7p+nylbm6tbaWQbtssqoz9\r\ns4JyfrXNLMaadoK5SpvqSjWYnlCQ2t5IN212MJjCe58zaSP93NOe9lZQAAh74Of1/wDrVRN2kivI\r\nJFSBBlpGOAB35rGvPGOi2b7BM9wQcHyFyB75OAR9Ca5J4zEVny0lb0NFCMdx/iPTUn0q/mhhj+1P\r\nGC8u0bmVWDEE9TwvA+leYydTnivRrrxzpEUQMKzXDlchVTaAfRif5gGvP7vUJViijsppYIwgDKh2\r\nkkepGN3rzXflvtoRcaqfzM6vK9inwy5BB5xW14TXHiixcgYDOef9xqy4NV1aA7otRulOMcTMP61N\r\na6teQX32x5pnnAbbI0m4hiCM/OGHQn/EV6FVSnBxXVGcbJ3PTL3wsNahkubeW3t5lGBuiPznngsp\r\n4HTkqTwPpXF3dldWFy9vdI0Mqdm6EdiD0IrQ074i6hZIkctvaTR8eYzRlZJPqwOB/wB81qt4w0TX\r\nIBFqEUlpIAWD/wCtVGyMBSPmPbtjiuejRnTpKMt0a86bOSKTDkcj2pDHcEfdP410oitixR/s869V\r\nkTDBh6+30NPNjaYyLaL67BUObWjR0KF1dM5ZbR3cBmA/2Ryfyq3HZeWMiMg/3n/wrccxwJn5EUdz\r\ngAVi3Ws2kThlH2k9dhBVO3Xox7gj5eQCGINVHmnsKSjDVm54Y8KT+I7xgS6WcfEs4A6/3VzwT+eO\r\n/UZ2vF15Y6d4cmtDcGN7pjCnlsJz8jrvUtu5PYliDyeprzvUPE+sanbfZJblkswCq2sIEcSrnONi\r\n4U49wT71jNGzNuYkn1p1MLGpy8z2dzmdW7LUoUOfKcsuepQAn/x6lCo7Dy3+buHG3H69PeqWxh91\r\niPxqxA5R3ZlyWQqK6m3YzPbtDvNLntkstPu4ZXtUCbVbnAAH4jpyOK1WKyIQOD2Poa8EtZ57SdZ7\r\neWSKVejxuVYfQjmtx/G2vmVHF6oKgbsQp85Hc8fyxXz9fJ5ufNTlv3OiNdW1IL/xprd+WC3X2WI/\r\nwWw2d/73LZ/GuallaWRiSSSSWJ6k09/apNN0661O9is7OFpp5ThUX+ZPQD1J4r3KdGFNWgrHO5Nh\r\n9puGtlgeeVoU+7GXJUfQdBUP3jzXVah8PvEFhki1S6QDJa2fdj22nDE/QGlsfh54lvDGf7P8iN/+\r\nWk8iqF+q5Lj/AL5q0l0FqcwBTSuTXq2mfCaBdr6pqMkh4Jitl2gHuNzZLD8FNdRD4K8OWAYRaRbu\r\nG5InBm59t5OKY7HgsMEk86QQxtJNIcJGgyzH0AHJNbVp4M8RXqM0Oj3I2nlZsQn8pCte72VvBZwC\r\nG1gjghHSOJAqj8BU5HPFFx2PItK+Fuq3jIb+4t7KI8sufMkHtgfL+O7j0Nalx8KLPynS31C4W5U4\r\nLOqsnTP3Rg9CD1r0sJnkdaJQDKikkllyB6YPP/oQoC1jyjR/hpqP9qCO71KGC2Rj80RYyMOgwCNo\r\n9+Tj0NVr17vRdQm07UVVpIpQglQY3qfusAfUEHGeMkZ4r117WN0JJOMHIryn4nTqnie58id98dhE\r\nkoViBlXaQByD7xkcHtyO8yip6MuEnF6Glp3gJtf0eO81G4uIpbj95AINvlxpj5SQRznIPXOCB2Na\r\nGkfCbSraPOqTyX0vdVzEg+mDn9fyrvoYzEvlouI1QKqAYAHP9KnYlU4FC0VkS3d3Zz58E+HZojCd\r\nGslQrgmOEI3/AH0uGzx1z3rPufhZ4Ynt2SG1uLVz/wAtIrl2Yfg5YfpXX2/3CSoVmYk+/YH8gKsD\r\npTEeXXXwZtmYfY9YliXuJ4BKfzUp/Kqdz8HLyOIm01eCaTPCywGIY/3gzfyr16igVjxMfCPxHnm4\r\n0oD1E8h/9p1nXfw28UwzmKPTRcqP+WkVxGF/8eYH9K9g1zxPZaZbXcUNzC+opC7RwtkgOANu8j7i\r\n5ZBuJA56158virxxIkLQ2l7cPIyzOsmlGJI+hCK4ch4zkj5trdORzgCw0fCuxGlxebe3I1BlCuVK\r\ntEsnQ4G0EqD75PtXV+EvCdr4YsnjXbPcuT5tzs2lgDwMZOB04zRRSbdxpG+sPO4mpRCznAGFFFFE\r\nQJRAAKgmhznBJoopjGLG2OlDRnFFFADghFKyFmUgDIB/mKKKaAXaxQgDnHFeDeIb7+17vVdQXc63\r\nTnYSOdmDsH/fKgUUVPUEfQwXErc8bRx+JpJsBAT0FFFMQy3h8m3ihDElEVc/QYqYEjGaKKQD6KKK\r\nYHm/xDsNPt76O8uGkjhltpGuQHyrfvrWPOGSQLw2SVXJ2j0rlRcaTqQ32fkXkp+V5jfX00gzubcy\r\nrHHn+Inn1A7CiiiyejGtz//Z\r\n",
        //           Poa =new Poa
        //           {
        //               Co = "S/O,",
        //               District = "Ahmedabad",
        //               House = "F-202, Icb City",
        //               Landmark = "Behind Vishwas City",
        //               PinCode = "382481",
        //               PostOffice = "Gota",
        //               State = "Gujarat",
        //               Street = "Gota Road",
        //               Vtc = "Daskroi",

        //           },
        //           Poi =new Poi
        //           {
        //               Dob = "18-11-1982",
        //               Gender = "M",
        //               Name = "Paregi Nayan"
        //           }
        //          },
        //          Success =true,

        //        };
        //        return response;
        //    }

        //    public static AadhaarAuthResponse VerifyAadhaar_Response()
        //    {
        //        var response = new AadhaarAuthResponse
        //        {
        //            AadharReferenceCode = "b85d7b7e381b47bf8d6539d308eb0e3e",
        //            AadharStatusCode = "200",
        //            Success = true
        //        };
        //        return response;
        //    }
        public IAadhaarOtpResponse GenerateOneTimePassword(string aadharId)
        {
            throw new NotImplementedException();
        }

        public IAadhaarAuthResponse IsAadharValid(IAadhaarAuthRequest request)
        {
            throw new NotImplementedException();
        }

        public IAadhaarKycResponse VerifyAadhar(IKyc request)
        {
            throw new NotImplementedException();
        }
    }
}
