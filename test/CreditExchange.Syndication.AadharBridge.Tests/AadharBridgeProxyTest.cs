﻿using System;
using CreditExchange.Syndication.AadharBridge;
using CreditExchange.Syndication.AadharBridge.Model;
using CreditExchange.Syndication.AadharBridge.Proxy;
using Xunit;

namespace CreditExchange.AadharBridge.Tests
{
    public class AadharBridgeProxyTest
    {
        [Fact]
        public void ThrowArgumentExceptionWithNullConfiguration()
        {
            Assert.Throws<ArgumentNullException>(() => new AadhaarBridgeProxy(null));
        }
        [Fact]
        public void ThrowArgumentExceptionWithMissingConfiguration()
        {
            var configuration = new AadhaarBridgeConfiguration
            {
                CertificateType = CertificateType.preprod,
                LocationType = "pincode",
                MatchingStrategy = MatchingStrategy.exact,
            };
            Assert.Throws<ArgumentException>(() => new AadhaarBridgeProxy(configuration));
        }
        [Fact]
        public void SuccessWithValidConfiguration()
        {
            var configuration = new AadhaarBridgeConfiguration
            {
                CertificateType = CertificateType.preprod,
                GatewayUrl = "http://localhost:8181/",
                LocationType = "pincode",
                MatchingStrategy = MatchingStrategy.exact,
            };
            var client = new AadhaarBridgeProxy(configuration);
            Assert.NotNull(client);
        }
    }
}
