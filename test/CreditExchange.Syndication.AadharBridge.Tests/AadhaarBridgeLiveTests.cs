﻿using CreditExchange.Syndication.AadharBridge;
using CreditExchange.Syndication.AadharBridge.Model;
using CreditExchange.Syndication.AadharBridge.Proxy;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;
using Xunit;
using Moq;

namespace CreditExchange.AadharBridge.Tests
{
    public class AadhaarBridgeLiveTests
    {
        public static string AadhaarId = "715515757356";
        public static IAadhaarAuthRequest GetAadhaarBridgeAuthGoodRequest()
        {
            IAadhaarAuthRequest request = new AadhaarAuthRequest();
            request.AadhaarId = AadhaarId;
            request.Name = "Paregi Nayan";
            request.Pincode = "382481";
            //request.Modality = Modality.demo;

            return request;
        }
        public static IAadhaarKycRequest GetAadhaarBridgeKycGoodRequest()
        {
            IAadhaarKycRequest request = new AadhaarKycRequest();
            request.AadhaarId = AadhaarId;
            request.Pincode = "382481";
            //request.Consent = 'Y';
            //request.Modality = Modality.otp;
            request.Otp = "484510";

            return request;
        }
        public static IAadhaarBridgeConfiguration GoodConfiguration
        {
            get
            {
                IAadhaarBridgeConfiguration configuration = new AadhaarBridgeConfiguration()
                {
                    CertificateType = CertificateType.preprod,
                    GatewayUrl = "https://localhost:8980/",
                    LocationType = "pincode",
                    MatchingStrategy = MatchingStrategy.exact,
                };

                return configuration;
            }
        }
        //[Fact]
        //public async void VerifyAadhaar()
        //{
        //    IAadhaarBridgeService service = new AadhaarBridgeService(new AadhaarBridgeProxy(GoodConfiguration), GoodConfiguration, new Mock<IEventHubClient>().Object,new Mock<ILookupService>().Object);
        //    var result = await service.IsAadharValid(GetAadhaarBridgeAuthGoodRequest());
        //    Assert.NotNull(result);
        //    Assert.Equal(true, result.Success);
        //}
      
        [Fact]
        public async void KnowYourCustomer()
        {
            IAadhaarBridgeService service = new AadhaarBridgeService(new AadhaarBridgeProxy(GoodConfiguration), GoodConfiguration, new Mock<IEventHubClient>().Object,new Mock<ILookupService>().Object);
            var result = await service.VerifyAadhaar("","",GetAadhaarBridgeKycGoodRequest());
            Assert.NotNull(result);
            Assert.NotNull(result.Kyc);
        }
    }
}
