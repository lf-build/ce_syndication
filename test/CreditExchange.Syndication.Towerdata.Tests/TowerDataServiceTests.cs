using System;
using CreditExchange.Syndication.TowerData.Proxy;
using Xunit;

namespace CreditExchange.Syndication.TowerData.Tests
{
    public class TowerDataServiceTests
    {
        [Fact]
        public void ThrowsExceptionOnConfigurationIsNull()
        {
            Assert.Throws<ArgumentNullException>(
                () => new TowerDataService(new TowerDataServiceProxy(null)));
        }

        [Fact]
        public void SuccessfullyCreateServiceObjectWithOnlyApiKey()
        {
            var dummy = new TowerDataService(
                new TowerDataServiceProxy(new TowerDataConfiguration()
                {
                    EmailPersonalizationApiKey = "DummyKey",
                    VerificationApiKey = "DummyKey"
                }));
            Assert.NotNull(dummy);
        }

        [Fact]
        public void ThrowsExceptionOnConfigurationValuesMissing()
        {
            Assert.Throws<ArgumentException>(
                () => new TowerDataService(new TowerDataServiceProxy(new TowerDataConfiguration())));
        }

   

    }
}