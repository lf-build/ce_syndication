using System;
using CreditExchange.Syndication.TowerData.Proxy;
using Xunit;

namespace CreditExchange.Syndication.TowerData.Tests.Live
{
    public class EmailVerificationLiveTest
    {
        private static TowerDataService GetTowerDataService()
            => new TowerDataService(new TowerDataServiceProxy(new TowerDataConfiguration
            {
                EmailPersonalizationApiKey =
                    Environment.GetEnvironmentVariable("TOWERDATA_PERSONALIZATION_APIKEY"),
                VerificationApiKey =
                    Environment.GetEnvironmentVariable("TOWERDATA_VERIFICATION_APIKEY") 
            }));


        [Fact(Skip = "LiveTest")]
        public void NullEmailThrowsArgumentException()
        {
            Assert.Throws<ArgumentException>(() => GetTowerDataService().VerifyEmail(null));
        }

        //[Fact(Skip = "LiveTest")]
        //public void EmptyEmailThrowsArgumentException()
        //{
        //    Assert.Throws<ArgumentException>(() => GetTowerDataService().VerifyEmail("  "));
        //}

        //[Fact(Skip = "LiveTest")]
        //public void ValidEmailReturnIsValidTrueForMySigmaId()
        //{
        //    var response = GetTowerDataService().VerifyEmail("nayan.gp@sigmainfo.net");
        //    Assert.NotNull(response);
        //    Assert.Equal(true, response.IsValid);
        //    Assert.Equal(50, response.StatusCode);
        //    Assert.Equal("sigmainfo.net", response.Domain);
        //    Assert.Equal("nayan.gp", response.Username);
        //    Assert.Equal("nayan.gp@sigmainfo.net", response.EmailAddress);
        //    Assert.Equal(5, response.ValidationLevel);
        //    Assert.Equal(false, response.Role);
        //}

        //[Fact(Skip = "LiveTest")]
        //public void ValidEmailReturnIsValidTrueForMyHotmailId()
        //{
        //    var response = GetTowerDataService().VerifyEmail("nayan.paregi@hotmail.com");
        //    Assert.NotNull(response);
        //    Assert.Equal(true, response.IsValid);
        //    Assert.Equal(50, response.StatusCode);
        //    Assert.Equal("freeisp", response.DomainType);
        //    Assert.Equal("hotmail.com", response.Domain);
        //    Assert.Equal("nayan.paregi", response.Username);
        //    Assert.Equal("nayan.paregi@hotmail.com", response.EmailAddress);
        //    Assert.Equal(5, response.ValidationLevel);
        //    Assert.Equal(false, response.Role);
        //}

        //[Fact(Skip = "LiveTest")]
        //public void ValidEmailReturnIsValidTrueForMyGmailId()
        //{
        //    var response = GetTowerDataService().VerifyEmail("nayan.paregi@gmail.com");
        //    Assert.NotNull(response);
        //    Assert.Equal(true, response.IsValid);
        //    Assert.Equal(50, response.StatusCode);
        //    Assert.Equal("freeisp", response.DomainType);
        //    Assert.Equal("gmail.com", response.Domain);
        //    Assert.Equal("nayan.paregi", response.Username);
        //    Assert.Equal("nayan.paregi@gmail.com", response.EmailAddress);
        //    Assert.Equal(5, response.ValidationLevel);
        //    Assert.Equal(false, response.Role);
        //}

        //[Fact(Skip = "LiveTest")]
        //public void InValidEmailWithValidDomainReturns400()
        //{
        //    var response = GetTowerDataService().VerifyEmail("nayan.paregi@sigmainfo.net");
        //    Assert.NotNull(response);
        //    Assert.Equal(false, response.IsValid);
        //    Assert.Equal(400, response.StatusCode);
        //}

        //[Fact(Skip = "LiveTest")]
        //public void InValidEmailWithInValidDomainReturns400()
        //{
        //    var response = GetTowerDataService().VerifyEmail("nayan.paregi@nayanparegi.com");
        //    Assert.NotNull(response);
        //    Assert.Equal(false, response.IsValid);
        //    Assert.Equal(310, response.StatusCode);
        //}

        //[Fact(Skip = "LiveTest")]
        //public void InvalidSyntaxtEmail1()
        //{
        //    var response = GetTowerDataService().VerifyEmail("nayan.paregi");
        //    Assert.NotNull(response);
        //    Assert.Equal(false, response.IsValid);
        //    Assert.Equal(150, response.StatusCode);
        //}

        //[Fact(Skip = "LiveTest")]
        //public void InvalidSyntaxtEmail2()
        //{
        //    var response = GetTowerDataService().VerifyEmail("nayan@.com");
        //    Assert.NotNull(response);
        //    Assert.Equal(false, response.IsValid);
        //    Assert.Equal(115, response.StatusCode);
        //}

        //[Fact(Skip = "LiveTest")]
        //public void SaleEmailReturnRoleTrue()
        //{
        //    var response = GetTowerDataService().VerifyEmail("sales@sigmainfo.net");
        //    Assert.NotNull(response);
        //    Assert.Equal(true, response.IsValid);
        //    Assert.Equal(50, response.StatusCode);
        //    Assert.Equal(true, response.Role);

        //}
    }
}