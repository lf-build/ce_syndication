using System;
using CreditExchange.Syndication.TowerData.Proxy;
using Xunit;

namespace CreditExchange.Syndication.TowerData.Tests.Live
{
    public class EmailInformationServiceLiveTest
    {
        private static TowerDataService GetTowerDataService()
            => new TowerDataService(new TowerDataServiceProxy(new TowerDataConfiguration
            {
                EmailPersonalizationApiKey =
                    Environment.GetEnvironmentVariable("TOWERDATA_PERSONALIZATION_APIKEY"),
                VerificationApiKey =
                    Environment.GetEnvironmentVariable("TOWERDATA_VERIFICATION_APIKEY") 
            }));


        //[Fact(Skip = "LiveTest")]
        //public void NonExistEmailWithValidDomain()
        //{
        //    var response=GetTowerDataService().GetEmailInformation("nayan.paregi@sigmainfo.net");
        //    Assert.NotNull(response);
        //}

        //[Fact(Skip = "LiveTest")]
        //public void NonExistEmailWithInValidDomain()
        //{
        //    var response = GetTowerDataService().GetEmailInformation("nayan.paregi@nayanparegi.com");
        //    Assert.NotNull(response);
        //}

        //[Fact(Skip = "LiveTest")]
        //public void TestInvalidEmailSyntextThrowsException()
        //{
        //    Assert.Throws<TowerDataException>(() => GetTowerDataService().GetEmailInformation("nayan.paregi"));
        //}

        //[Fact(Skip = "LiveTest")]
        //public void TestNullEmailAddressThrowsException()
        //{
        //    Assert.Throws<ArgumentException>(() => GetTowerDataService().GetEmailInformation("  "));
        //}

        //[Fact(Skip = "LiveTest")]
        //public void TestWithSomeData1()
        //{
        //    var response = GetTowerDataService().GetEmailInformation("mmathew@sigmainfo.net");
        //    Assert.NotNull(response);
        //}

        //[Fact(Skip = "LiveTest")]
        //public void TestWithSomeData2()
        //{
        //    var response = GetTowerDataService().GetEmailInformation("happymanoj@hotmail.com");
        //    Assert.NotNull(response);
        //}
      
    }
}