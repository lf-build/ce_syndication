using System;
using System.Net;
using Xunit;
using CreditExchange.Syndication.TowerData.Proxy;

namespace CreditExchange.Syndication.TowerData.Tests.Live
{
    public class IpVerificationServiceLiveTest
    {
        private static TowerDataService GetTowerDataService()
            => new TowerDataService(new TowerDataServiceProxy(new TowerDataConfiguration
            {
                EmailPersonalizationApiKey =
                    Environment.GetEnvironmentVariable("TOWERDATA_PERSONALIZATION_APIKEY") ,
                VerificationApiKey =
                    Environment.GetEnvironmentVariable("TOWERDATA_VERIFICATION_APIKEY")
            }));

        [Fact(Skip = "LiveTest")]
        public void NullIpThrowsArgumentException()
        {
            Assert.Throws<ArgumentNullException>(() => GetTowerDataService().VerifyIpAddress(null));
        }

      
        //[Fact(Skip = "LiveTest")]
        //public void VerifySigmaIpAddress()
        //{
        //    var response1 = GetTowerDataService().VerifyIpAddress(IPAddress.Parse("52.9.167.180"));
        //    Assert.NotNull(response1);
        //}


    }
}