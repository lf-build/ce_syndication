using System;
using CreditExchange.Syndication.TowerData.Proxy.Verification;
using Xunit;

namespace CreditExchange.Syndication.TowerData.Tests
{
    public class EmailVerificationServiceTest
    {
        private static TowerDataService TowerDataService { get; } =
       new TowerDataService(new FakeTowerDataServiceProxy());


        [Fact]
        public void ThrowsArgumentExceptionOnNullArgument()
        {
            Assert.Throws<ArgumentException>(() => TowerDataService.VerifyEmail(null));
        }

        //[Fact]
        //public void ThrowsArgumentExceptionOnEmptyArgument()
        //{
        //    Assert.Throws<ArgumentException>(() => TowerDataService.VerifyEmail(" "));
        //}


        //[Fact]
        //public void VerifyEmailResponses()
        //{
        //    foreach (var email in FakeTowerDataServiceProxy.EmailWithResponse)
        //    {
        //        var response = TowerDataService.VerifyEmail(email.Key);
        //        Assert.NotNull(response);
        //        Assert.Equal(email.Value, response.StatusCode);
        //    }

        //}

        //[Fact]
        //public void VerifyEmail5Response()
        //{

        //    var response = TowerDataService.VerifyEmail("5@5.com");
        //    Assert.NotNull(response);

        //    var expectedResponse = FakeTowerDataServiceProxy.Email5ProxyResponse;
        //    VerifyResponse(expectedResponse, response);
        //}

        //[Fact]
        //public void VerifyEmail6Response()
        //{

        //    var response = TowerDataService.VerifyEmail("6@6.com");
        //    Assert.NotNull(response);

        //    var expectedResponse = FakeTowerDataServiceProxy.Email6ProxyResponse;
        //    VerifyResponse(expectedResponse, response);
        //}

        private void VerifyResponse(EmailValidationProxyResponse expectedProxyResponse, IEmailVerificationResponse response)
        {
            if (expectedProxyResponse == null)
                throw new ArgumentNullException(nameof(expectedProxyResponse));

            if (response == null)
                throw new ArgumentNullException(nameof(response));

            Assert.Equal(expectedProxyResponse.Domain, response.Domain);
            Assert.Equal(expectedProxyResponse.DomainType, response.DomainType);
            Assert.Equal(expectedProxyResponse.EmailAddress, response.EmailAddress);
            Assert.Equal(expectedProxyResponse.Role, response.Role);

            Assert.Equal(expectedProxyResponse.StatusCode, response.StatusCode);
            Assert.Equal(expectedProxyResponse.StatusDesc, response.StatusDescription);
            Assert.Equal(expectedProxyResponse.Username, response.Username);
            Assert.Equal(expectedProxyResponse.ValidationLevel, response.ValidationLevel);

        }
    }
}