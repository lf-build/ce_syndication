using System;
using System.Net;
using CreditExchange.Syndication.TowerData.Proxy.Verification;
using Xunit;

namespace CreditExchange.Syndication.TowerData.Tests
{
    public class IpVerificationServiceTest
    {
        private static TowerDataService TowerDataService { get; } =
       new TowerDataService(new FakeTowerDataServiceProxy());


        [Fact]
        public void ThrowsArgumentExceptionOnNullArgument()
        {
            Assert.Throws<ArgumentNullException>(() => TowerDataService.VerifyIpAddress(null));
        }

       


        //[Fact]
        //public void VerifyIp1Response()
        //{
        //    var response = TowerDataService.VerifyIpAddress(IPAddress.Parse("1.1.1.1"));
        //    Assert.NotNull(response);

        //    var expectedResponse = FakeTowerDataServiceProxy.IpResponse1;
        //    VerifyResponse(expectedResponse, response);
        //}



        //[Fact]
        //public void VerifyEmail6Response1()
        //{
        //    var response = TowerDataService.VerifyIpAddress(IPAddress.Parse("2.2.2.2"));

        //    Assert.NotNull(response);

        //    var expectedResponse = FakeTowerDataServiceProxy.IpResponse2;
        //    VerifyResponse(expectedResponse, response);
        //}

        private void VerifyResponse(IpVerificationProxyResponse expectedProxyResponse, IIpVerificationResponse response)
        {
           if (expectedProxyResponse == null)
                throw new ArgumentNullException(nameof(expectedProxyResponse));

            if (response == null)
                throw new ArgumentNullException(nameof(response));

            Assert.Equal(expectedProxyResponse.StatusCode, response.StatusCode);
            Assert.Equal(expectedProxyResponse.City, response.City);
            Assert.Equal(expectedProxyResponse.CountryCode, response.CountryCode);
            Assert.Equal(expectedProxyResponse.CountryName, response.CountryName);
            Assert.Equal(expectedProxyResponse.Domain, response.Domain);
            Assert.Equal(expectedProxyResponse.IpAddress, response.IpAddress);
            Assert.Equal(expectedProxyResponse.Isp, response.Isp);
            Assert.Equal(expectedProxyResponse.Region, response.Region);
            Assert.Equal(expectedProxyResponse.StatusDesc, response.StatusDescription);

        }
    }
}