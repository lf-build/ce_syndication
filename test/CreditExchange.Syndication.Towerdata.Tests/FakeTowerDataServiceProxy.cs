using System;
using System.Collections.Generic;
using CreditExchange.Syndication.TowerData.Proxy;
using CreditExchange.Syndication.TowerData.Proxy.EmailInformation;
using CreditExchange.Syndication.TowerData.Proxy.Verification;

namespace CreditExchange.Syndication.TowerData.Tests
{
    public class FakeTowerDataServiceProxy : ITowerDataServiceProxy
    {
        public static EmailInformationProxyResponse Email1EmailInformationProxyResponse { get; } = new EmailInformationProxyResponse()
        {
            Interests = new Dictionary<string, bool>()
            {
                {"football", true},
                {"cricket", false},
                { "programing", true }
            },
            EmailActivityMetricsResponse = new EmailActivityMetricsProxyResponse()
            {
                DateFirstSeen = "2016-12-21",
                Velocity = 9,
                Longevity = 3,
                Popularity = 9,
                MonthLastOpen = "2016-05"
            },
            Gender = "Male",
            Age = "30-35",
            MaritalStatus = "Married",
            PresenceOfChildren = "Yes",
            NetWorth = "200M",
            LengthOfResidence = "15",
            Occupation = "Service",
            Education = "BCA",
            HouseholdIncome = "200M-500M",
            HomeMarketValue = "2000M",
            HomeOwnerStatus = "Own",
            Zip = "382481"
        };

        public static EmailInformationProxyResponse Email2EmailInformationProxyResponse { get; } = new EmailInformationProxyResponse()
        {
            Interests = new Dictionary<string, bool>
            {
                {"football", false},
                {"cricket", false},
                { "programing", false }
            },
            EmailActivityMetricsResponse = new EmailActivityMetricsProxyResponse()
            {
                DateFirstSeen = "now",
                Velocity = 9,
                Longevity = 3,
                Popularity = 9,
                MonthLastOpen = "2016-05"
            },
            Gender = "Male",
            Age = "30-35",
            MaritalStatus = "Married",
            PresenceOfChildren = "Yes",
            NetWorth = "200M",
            LengthOfResidence = "15",
            Occupation = "Service",
            Education = "BCA",
            HouseholdIncome = "200M-500M",
            HomeMarketValue = "2000M",
            HomeOwnerStatus = "Own",
            Zip = "382481"
        };

        public static EmailInformationProxyResponse Email3EmailInformationProxyResponse { get; } = new EmailInformationProxyResponse
        {
            Interests = null,
            EmailActivityMetricsResponse = new EmailActivityMetricsProxyResponse
            {
                DateFirstSeen = "now",
                Velocity = 9,
                Longevity = 3,
                Popularity = 9,
                MonthLastOpen = "2016-05"
            },
            Gender = "Male",
            Age = "30-35",
            MaritalStatus = "Married",
            PresenceOfChildren = "Yes",
            NetWorth = "200M",
            LengthOfResidence = "15",
            Occupation = "Service",
            Education = "BCA",
            HouseholdIncome = "200M-500M",
            HomeMarketValue = "2000M",
            HomeOwnerStatus = "Own",
            Zip = "382481"
        };

        public EmailInformationProxyResponse GetEmailInformation(string email)
        {
            switch (email)
            {
                case "1@1.com":
                    return Email1EmailInformationProxyResponse;
                case "2@2.com":
                    return Email2EmailInformationProxyResponse;
                case "3@3.com":
                    return Email3EmailInformationProxyResponse;
                default:
                    throw new Exception("Unknow error occured");
            }
        }

        public EmailValidationProxyResponse VerifyEmail(string email)
        {
            if (email == null)
                throw new ArgumentNullException(nameof(email));

            if (email == "okNull@1.com")
                return new EmailValidationProxyResponse() { Ok = null };

            if (email == "5@5.com")
                return Email5ProxyResponse;

            if (email == "6@6.com")
                return Email6ProxyResponse;

            if (EmailWithResponse.ContainsKey(email))
                return new EmailValidationProxyResponse() { StatusCode = EmailWithResponse[email] };

            return new EmailValidationProxyResponse() { Ok = false };
        }

        public IpVerificationProxyResponse VerifyIpAddress(string ip)
        {
            switch (ip)
            {
                case "1.1.1.1":
                    return IpResponse1;
                case "2.2.2.2":
                    return IpResponse2;
            }

            return new IpVerificationProxyResponse()
            {
                StatusCode = 15,
                Ok = false,
                StatusDesc = "IP not found"
            };

        }


        public static Dictionary<string, int> EmailWithResponse { get; } = new Dictionary<string, int>()
        {
            {"1@1.com", 50},
            {"2@2.com", 400},
            {"3@2.com", 300},
        };

        public static EmailValidationProxyResponse Email5ProxyResponse { get; } = new EmailValidationProxyResponse()
        {
            StatusCode = 50,
            Domain = "5",
            Ok = true,
            EmailAddress = "5@5.com",
            Role = false,
            ValidationLevel = 5,
            DomainType = "mydomain",
            StatusDesc = "OK Fine",
            Username = "5"
        };


        public static EmailValidationProxyResponse Email6ProxyResponse { get; } = new EmailValidationProxyResponse()
        {
            StatusCode = 50,
            Domain = "6",
            Ok = true,
            EmailAddress = "6@6.com",
            Role = true,
            ValidationLevel = 6,
            DomainType = null,
            StatusDesc = "OK Fine",
            Username = "6"
        };

        public static IpVerificationProxyResponse IpResponse1 { get; } = new IpVerificationProxyResponse()
        {
            StatusCode = 10,
            Domain = "sigma",
            Ok = true,
            StatusDesc = "IP found",
            CountryCode = "IND",
            CountryName = "India",
            Region = "Naranpura",
            IpAddress = "1.1.1.1",
            Isp = "MyISP",
            City = "Ahmedabad"
        };


        public static IpVerificationProxyResponse IpResponse2 { get; } = new IpVerificationProxyResponse()
        {
            StatusCode = 15,
            Ok = false,
            StatusDesc = "IP not found"
        };
    }
}