using System;
using System.ComponentModel;
using System.Linq;
using Xunit;

namespace CreditExchange.Syndication.TowerData.Tests
{
    public class EmailInformationTests
    {
        [Fact]
        public void ThrowsArgumentExceptionOnNullArgument()
        {
            Assert.Throws<ArgumentException>(() => TowerDataService.GetEmailInformation(null));
        }

        //[Fact]
        //public void ThrowsArgumentExceptionOnEmptyArgument()
        //{
        //    Assert.Throws<ArgumentException>(() => TowerDataService.GetEmailInformation(" "));
        //}

        //[Fact]
        //public void VerifyEmail1Response()
        //{
        //    var response= TowerDataService.GetEmailInformation("1@1.com");
        //    Assert.NotNull(response);
        //    VerifyResponse(FakeTowerDataServiceProxy.Email1EmailInformationProxyResponse, response);
        //}

        //[Fact]
        //public void VerifyEmail2Response()
        //{
        //    var response = TowerDataService.GetEmailInformation("2@2.com");
        //    Assert.NotNull(response);
        //    Assert.Equal(0,response.Interests.Count);
        //    VerifyResponse(FakeTowerDataServiceProxy.Email2EmailInformationProxyResponse, response);
        //}

        //[Fact]
        //public void VerifyEmail3Response()
        //{
        //    var response = TowerDataService.GetEmailInformation("3@3.com");
        //    Assert.NotNull(response);
        //    VerifyResponse(FakeTowerDataServiceProxy.Email3EmailInformationProxyResponse, response);
        //}

        private static TowerDataService TowerDataService { get; } =
            new TowerDataService(new FakeTowerDataServiceProxy());

        private void VerifyResponse(Proxy.EmailInformation.EmailInformationProxyResponse expectedEmailInformationProxyResponse, IEmailInformation response)
        {
            if (expectedEmailInformationProxyResponse == null)
                throw new ArgumentNullException(nameof(expectedEmailInformationProxyResponse));

            if (response == null)
                throw new ArgumentNullException(nameof(response));

            Assert.Equal(expectedEmailInformationProxyResponse.Age, response.Demographics.AgeRange);
            Assert.Equal(expectedEmailInformationProxyResponse.Education, response.Demographics.Education);

            if (string.Equals(expectedEmailInformationProxyResponse.Gender, "Male", StringComparison.OrdinalIgnoreCase))
            {
                Assert.Equal(Gender.Male, response.Demographics.Gender);
            }
            else if (string.Equals(expectedEmailInformationProxyResponse.Gender, "Female", StringComparison.OrdinalIgnoreCase))
            {
                Assert.Equal(Gender.Female, response.Demographics.Gender);
            }
            else
            {
                throw new InvalidEnumArgumentException();
            }

            Assert.Equal(expectedEmailInformationProxyResponse.NetWorth, response.Demographics.NetWorthRange);
            Assert.Equal(expectedEmailInformationProxyResponse.Zip, response.Demographics.Zip);

            Assert.Equal(expectedEmailInformationProxyResponse.HomeMarketValue, response.Household.HomeMarketValue);
            Assert.Equal(expectedEmailInformationProxyResponse.HomeOwnerStatus, response.Household.HomeOwnerStatus);
            Assert.Equal(expectedEmailInformationProxyResponse.HouseholdIncome, response.Household.HouseholdIncome);
            Assert.Equal(expectedEmailInformationProxyResponse.LengthOfResidence, response.Household.LengthOfResidence);
            Assert.Equal(expectedEmailInformationProxyResponse.MaritalStatus, response.Household.MaritalStatus);
            Assert.Equal(expectedEmailInformationProxyResponse.Occupation, response.Household.Occupation);
            Assert.Equal(expectedEmailInformationProxyResponse.PresenceOfChildren, response.Household.PresenceOfChildren);

            if (expectedEmailInformationProxyResponse.Interests != null)
            {
                var expectedInterests =
                    (from interest in expectedEmailInformationProxyResponse.Interests where interest.Value select interest.Key).ToList();

                Assert.Equal(expectedInterests.Count, response.Interests.Count);
                Assert.True(expectedInterests.SequenceEqual(response.Interests));
            }
            if (response.EmailActivityMetrics.DateFirstSeen.HasValue)
                Assert.Equal(expectedEmailInformationProxyResponse.EmailActivityMetricsResponse.DateFirstSeen, response.EmailActivityMetrics.DateFirstSeen.GetValueOrDefault().ToString("yyyy-MM-dd"));
            else
                Assert.Equal(null, response.EmailActivityMetrics.DateFirstSeen);

            
            Assert.Equal(expectedEmailInformationProxyResponse.EmailActivityMetricsResponse.Longevity, response.EmailActivityMetrics.Longevity);
            Assert.Equal(expectedEmailInformationProxyResponse.EmailActivityMetricsResponse.MonthLastOpen, response.EmailActivityMetrics.MonthLastOpen);
            Assert.Equal(expectedEmailInformationProxyResponse.EmailActivityMetricsResponse.Popularity, response.EmailActivityMetrics.Popularity);
            Assert.Equal(expectedEmailInformationProxyResponse.EmailActivityMetricsResponse.Velocity, response.EmailActivityMetrics.Velocity);

        }

    }
}