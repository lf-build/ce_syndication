﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CreditExchange.Syndication.EmailHunter.Client;
using CreditExchange.Syndication.EmailHunter.Request;
using CreditExchange.Syndication.EmailHunter.Response;
using Moq;
using Xunit;
using CreditExchange.Syndication.EmailHunter.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;

namespace CreditExchange.Syndication.EmailHunter.Tests
{
    public class EmailHunterServiceTest
    {
        #region DomainSearch
        [Fact]
        public async void DomainSearch_Should_ThrowsArgumentNullException_WithNullArgument_()
        {
            var mock = Mock.Of<IEmailHunterProxy>();           
            var service = new EmailHunterService(mock,Mock.Of<ILogger>());

            await Assert.ThrowsAsync<InvalidArgumentException>(() => service.DomainSearch(null));
        }

        [Fact]
        public async void DomainSearch_Should_ThrowsArgumentException_WithInvalidArguments()
        {
            Mock<IEmailHunterProxy> _mockEmailHunterProxy = new Mock<IEmailHunterProxy>();
            var service = new EmailHunterService(Mock.Of<IEmailHunterProxy>(), Mock.Of<ILogger>());
            
            await Assert.ThrowsAsync<InvalidArgumentException>(() => service.DomainSearch(string.Empty));
                    
            await Assert.ThrowsAsync<InvalidArgumentException>(() => service.DomainSearch(" "));
        }

        [Fact]
        public async void DomainSearch_Should_ReturnsDomainSearchResult_WithValidArguments()
        {
            var mock =new Mock<IEmailHunterProxy>();
            var config = Mock.Of<IEmailHunterConfiguration>();       
            var tenanttime = Mock.Of<ITenantTime>();
            var eventhub = Mock.Of<IEventHubClient>();
            var logger = Mock.Of<ILogger>();
            var service = new EmailHunterService(mock.Object, Mock.Of<ILogger>());

            mock.Setup(s => s.DomainSearch(It.IsAny<IDomainSearchRequest>()))
                .Returns(() => Task.FromResult<IDomainSearchResponse>(new DomainSearchResponse
                {
                    Emails = new List<IEmailResponse>
                    {
                        new EmailResponse
                        {
                            Sources = new List<ISourceResponse>
                            {
                                new SourceResponse()
                            }
                        }
                    }
                }));         


            var domainSearchResult = await service.DomainSearch("sigmainfo.net");

            Assert.NotNull(domainSearchResult);
        }

        [Fact]
        public async void EmailFinder_Should_ThrowsArgumentNullException_WithNullArgument()
        {
            var mock = Mock.Of<IEmailHunterProxy>();
            var config = Mock.Of<IEmailHunterConfiguration>();        
            var tenanttime = Mock.Of<ITenantTime>();
            var eventhub = Mock.Of<IEventHubClient>();
            var logger = Mock.Of<ILogger>();
            var service = new EmailHunterService(mock, Mock.Of<ILogger>());

            await Assert.ThrowsAsync<ArgumentNullException>(() => service.EmailFinder(null));
        }

        #endregion


        #region EmailFinder
        [Fact]
        public async void EmailFinder_Should_ThrowsArgumentException_WithInvalidArguments()
        {
            var mock = Mock.Of<IEmailHunterProxy>();
            var config = Mock.Of<IEmailHunterConfiguration>();           
            var tenanttime = Mock.Of<ITenantTime>();
            var eventhub = Mock.Of<IEventHubClient>();
            var logger = Mock.Of<ILogger>();
            var service = new EmailHunterService(mock, Mock.Of<ILogger>());

            var emailFinder = new EmailFinder();

            await Assert.ThrowsAsync<ArgumentException>(() => service.EmailFinder(emailFinder));

            emailFinder.Domain = string.Empty;
            await Assert.ThrowsAsync<ArgumentException>(() => service.EmailFinder(emailFinder));

            emailFinder.Domain = "";
            await Assert.ThrowsAsync<ArgumentException>(() => service.EmailFinder(emailFinder));

            emailFinder.Domain = " ";
            await Assert.ThrowsAsync<ArgumentException>(() => service.EmailFinder(emailFinder));

            emailFinder.Domain = "sigmainfo.net";

            emailFinder.FirstName = string.Empty;
            await Assert.ThrowsAsync<ArgumentException>(() => service.EmailFinder(emailFinder));

            emailFinder.FirstName = "";
            await Assert.ThrowsAsync<ArgumentException>(() => service.EmailFinder(emailFinder));

            emailFinder.FirstName = " ";
            await Assert.ThrowsAsync<ArgumentException>(() => service.EmailFinder(emailFinder));

            emailFinder.FirstName = "test";
            emailFinder.LastName = string.Empty;
            await Assert.ThrowsAsync<ArgumentException>(() => service.EmailFinder(emailFinder));

            emailFinder.LastName = "";
            await Assert.ThrowsAsync<ArgumentException>(() => service.EmailFinder(emailFinder));

            emailFinder.LastName = " ";
            await Assert.ThrowsAsync<ArgumentException>(() => service.EmailFinder(emailFinder));
        }

        [Fact]
        public async void EmailFinder_Should_ReturnsEmailFinderResult_WithValidArguments()
        {
            var mock =new Mock<IEmailHunterProxy>();
            var config = Mock.Of<IEmailHunterConfiguration>();         
            var tenanttime = Mock.Of<ITenantTime>();
            var eventhub = Mock.Of<IEventHubClient>();
            var logger = Mock.Of<ILogger>();
            var service = new EmailHunterService(mock.Object, Mock.Of<ILogger>());

            mock.Setup(s => s.EmailFinder(It.IsAny<IEmailFinderRequest>()))
                .Returns(() => Task.FromResult<IEmailFinderResponse>(new EmailFinderResponse
                {
                    Sources = new List<ISourceResponse>
                    {
                        new SourceResponse()
                    }
                }));

            var emailFinder = new EmailFinder
            {
                Domain = "sigmainfo.net",
                FirstName = "test",
                LastName = "user"
            };

            var emailFinderResult = await service.EmailFinder(emailFinder);

            Assert.NotNull(emailFinderResult);
        }

        #endregion

        #region EmailVerifier

        [Fact]
        public async void EmailVerifier_Should_ThrowsArgumentNullException_WithNullArgument()
        {
            var mock = new Mock<IEmailHunterProxy>();
            var config = Mock.Of<IEmailHunterConfiguration>();          
            var tenanttime = Mock.Of<ITenantTime>();
            var eventhub = Mock.Of<IEventHubClient>();
            var logger = Mock.Of<ILogger>();
            var service = new EmailHunterService( mock.Object, Mock.Of<ILogger>());


            await Assert.ThrowsAsync<InvalidArgumentException>(() => service.EmailVerifier(null));
        }

        [Fact]
        public async void EmailVerifier_Should_ThrowsArgumentException_WithInvalidArgument()
        {
            var mock = new Mock<IEmailHunterProxy>();
        
            var service = new EmailHunterService( mock.Object, Mock.Of<ILogger>());   
           
            await Assert.ThrowsAsync<InvalidArgumentException>(() => service.EmailVerifier(string.Empty));
            await Assert.ThrowsAsync<InvalidArgumentException>(() => service.EmailVerifier(" "));
        }

        [Fact]
        public async void EmailVerifier_Should_ReturnsEmailVerificationResult_WithValidArguments()
        {
            var mock = new Mock<IEmailHunterProxy>();
            var config = Mock.Of<IEmailHunterConfiguration>();          
            var tenanttime = Mock.Of<ITenantTime>();
            var eventhub = Mock.Of<IEventHubClient>();
            var logger = Mock.Of<ILogger>();
            var service = new EmailHunterService(mock.Object, Mock.Of<ILogger>());


            mock.Setup(s => s.EmailVerifier(It.IsAny<IEmailVerificationRequest>()))
                .Returns(() => Task.FromResult<IEmailVerificationResponse>(new EmailVerificationResponse
                {
                    Sources = new List<ISourceResponse>
                    {
                        new SourceResponse()
                    }
                }));

       

            var emailVerificationResult = await service.EmailVerifier("test@user.com");

            Assert.NotNull(emailVerificationResult);
        }

        #endregion

        [Fact]
        public async void EmailCount_Should_ThrowsArgumentNullException_WithNullArgument()
        {
            var mock = new Mock<IEmailHunterProxy>();           
            var service = new EmailHunterService(mock.Object, Mock.Of<ILogger>());

            await Assert.ThrowsAsync<ArgumentException>(() => service.EmailCount(null));
        }

        [Fact]
        public async void EmailCount_Should_ThrowsArgumentException_WithInvalidArgument()
        {
            var mock = new Mock<IEmailHunterProxy>();         
            var service = new EmailHunterService(mock.Object, Mock.Of<ILogger>());     
            await Assert.ThrowsAsync<ArgumentException>(() => service.EmailCount(string.Empty));

        }

        [Fact]
        public async void EmailCount_Should_ReturnsEmailCountResult_WithValidArguments()
        {
            var mock = new Mock<IEmailHunterProxy>();          
            var service = new EmailHunterService(mock.Object, Mock.Of<ILogger>());

            mock.Setup(s => s.EmailCount(It.IsAny<IEmailCountRequest>()))
                .Returns(() => Task.FromResult<IEmailCountResponse>(new EmailCountResponse()));

           
            var emailCountResult = await service.EmailCount("sigmainfo.net");

            Assert.NotNull(emailCountResult);
        }

        [Fact]
        public async void EmailCount_Should_Throw_Exception_When_Response_Is_Null()
        {
            var mock = new Mock<IEmailHunterProxy>();
            var service = new EmailHunterService(mock.Object, Mock.Of<ILogger>());

            mock.Setup(s => s.EmailCount(It.IsAny<IEmailCountRequest>()))
                .Returns(() => Task.FromResult<IEmailCountResponse>(null));

            await Assert.ThrowsAsync<NotFoundException>(() => service.EmailCount("sigmainfo.net"));
        }
    }
}
