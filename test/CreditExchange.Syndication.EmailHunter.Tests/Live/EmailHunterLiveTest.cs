﻿using LendFoundry.Foundation.Logging;
using CreditExchange.Syndication.EmailHunter.Client;
using CreditExchange.Syndication.EmailHunter.Configuration;
using Moq;
using Xunit;

namespace CreditExchange.Syndication.EmailHunter.Tests
{
    public class EmailHunterLiveTest
    {
        private IEmailHunterService Service { get; }

        

        public EmailHunterLiveTest()
        {
            var configuration = new EmailHunterConfiguration
            {
                ApiKey = "a787c4d7ceb14a723e162ed6fabec80ee2f8db9b"
            };

            var emailHunterProxy = new EmailHunterProxy(configuration);

            Service = new EmailHunterService(emailHunterProxy, Mock.Of<ILogger>());
        }

       
        [Fact]
        public async void DomainSearch()
        {
            var domainSearchResult = await Service.DomainSearch("stripe.com");

            Assert.NotNull(domainSearchResult);
        }

     
       [Fact]
        public async void EmailFinder()
        {
            var emailFinder = new EmailFinder
            {
                Domain = "asana.com",
                FirstName = "Dustin",
                LastName = "Moskovitz"
            };

            var emailFinderResult = await Service.EmailFinder(emailFinder);

            Assert.NotNull(emailFinderResult);
        }

      
       [Fact]
        public async void EmailVerification()
        {
            var emailVerificationResult = await Service.EmailVerifier("steli@close.io");

            Assert.NotNull(emailVerificationResult);
        }


      [Fact]
        public async void EmailCount()
        {
            var emailCountResult = await Service.EmailCount("stripe.com");

            Assert.NotNull(emailCountResult);
        }
    }
}
