﻿using CreditExchange.Syndication.EmailHunter.Client;
using CreditExchange.Syndication.EmailHunter.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.Syndication.EmailHunter.Tests
{
    public class EmailHunterProxyTest
    {
        [Fact]
        public void Should_Throw_Exception_When_Value_Configuration_Null()
        {
            Assert.Throws<ArgumentNullException>(() => new EmailHunterProxy(null));
        }

        [Fact]
        public void Should_Throw_Exception_When_Value_Route_Null()
        {
            var configuration = new EmailHunterConfiguration()
            {

                ApiUrl = "http://pro.viewdns.info",

                ApiVersion = "1",
                ExpirationInDays = 30

            };
            Assert.Throws<ArgumentNullException>(() => new EmailHunterProxy(configuration, null));
        }

        [Fact]
        public void Should_Throw_Exception_When_Configuration_APi_Key_Is_Null()
        {
            var configuration = new EmailHunterConfiguration()
            {

                ApiUrl = "http://pro.viewdns.info",
                ApiKey = null,
                ApiVersion = "1",
                ExpirationInDays = 30

            };

            var route = new EmailHunterRoutes()
            {
                DomainSearch = null,
                EmailCount = "1",
                EmailFinder = "testFind",
                EmailVerification = "test@123.com"


            };
            Assert.Throws<ArgumentException>(() => new EmailHunterProxy(configuration, route));
        }

        [Fact]
        public void Should_Throw_Exception_When_Configuration_APi_Version_Is_Null()
        {
            var configuration = new EmailHunterConfiguration()
            {

                ApiUrl = "http://pro.viewdns.info",
                ApiKey = "1",
                ApiVersion = null,
                ExpirationInDays = 30

            };

            var route = new EmailHunterRoutes()
            {
                DomainSearch = null,
                EmailCount = "1",
                EmailFinder = "testFind",
                EmailVerification = "test@123.com"


            };
            Assert.Throws<ArgumentException>(() => new EmailHunterProxy(configuration, route));
        }


        [Fact]
        public void Should_Throw_Exception_When_Route_Domain_Search_Is_Null()
        {
            var configuration = new EmailHunterConfiguration()
            {

                ApiUrl = "http://pro.viewdns.info",
                ApiKey = "1",
                ApiVersion = "1",
                ExpirationInDays = 30

            };

            var route = new EmailHunterRoutes()
            {
                DomainSearch = null,
                  EmailCount = "1",
                   EmailFinder = "testFind",
                    EmailVerification = "test@123.com"
              

            };
            Assert.Throws<ArgumentException>(() => new EmailHunterProxy(configuration, route));
        }

        [Fact]
        public void Should_Throw_Exception_When_Route_EmailFinder_Is_Null()
        {
            var configuration = new EmailHunterConfiguration()
            {

                ApiUrl = "http://pro.viewdns.info",
                ApiKey = "1",
                ApiVersion = "1",
                ExpirationInDays = 30

            };

            var route = new EmailHunterRoutes()
            {
                DomainSearch = "domainserach",
                EmailCount = "1",
                EmailFinder = null,
                EmailVerification = "test@123.com"


            };
            Assert.Throws<ArgumentException>(() => new EmailHunterProxy(configuration, route));
        }


        [Fact]
        public void Should_Throw_Exception_When_Route_EmailVerification_Is_Null()
        {
            var configuration = new EmailHunterConfiguration()
            {

                ApiUrl = "http://pro.viewdns.info",
                ApiKey = "1",
                ApiVersion = "1",
                ExpirationInDays = 30

            };

            var route = new EmailHunterRoutes()
            {
                DomainSearch = "domainserach",
                EmailCount = "1",
                EmailFinder = "1",
                EmailVerification = null


            };
            Assert.Throws<ArgumentException>(() => new EmailHunterProxy(configuration, route));
        }

        [Fact]
        public void Should_Throw_Exception_When_Route_EmailCount_Is_Null()
        {
            var configuration = new EmailHunterConfiguration()
            {

                ApiUrl = "http://pro.viewdns.info",
                ApiKey = "1",
                ApiVersion = "1",
                ExpirationInDays = 30

            };

            var route = new EmailHunterRoutes()
            {
                DomainSearch = "domainserach",
                EmailCount = null,
                EmailFinder = "1",
                EmailVerification = "1"


            };
            Assert.Throws<ArgumentException>(() => new EmailHunterProxy(configuration, route));
        }

      
    }
}
