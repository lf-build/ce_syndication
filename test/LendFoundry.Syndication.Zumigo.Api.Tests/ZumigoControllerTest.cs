﻿using Microsoft.AspNet.Mvc;
using Xunit;
using LendFoundry.Foundation.Services;
using CreditExchange.Zumigo.Api.Controllers;
using CreditExchange.Syndication.Zumigo;

namespace LendFoundry.Syndication.Zumigo.Api.Tests
{
    public class ZumigoControllerTest
    {
        //private ApiController GetController(IZumigoService zumigoService)
        //{
        //    return new ApiController(zumigoService);
        //}
        //private IZumigoService GetZumigoService()
        //{
        //    return new ZumigoService(new ZumigoConfiguration()
        //    {
        //        UserName = "creditxchangeapi",
        //        Password = "cred17Exchang3Ap!",
        //        AddDeviceUrl = "https://api.zumigo.com/zumigo/addDevice.jsp",
        //        DeleteDeviceUrl = "https://api.zumigo.com/zumigo/deleteDevice.jsp",
        //        GetDeviceStatusUrl = "https://api.zumigo.com/zumigo/deviceStatus.jsp",
        //        LocationAndAddressUrl = "https://api.zumigo.com/zumigo/locationAndAddress.jsp",
        //        ValidateOnlineTransactionUrl = "https://api.zumigo.com/zumigo/validateOnlineTransaction.jsp",
        //        OptInType = OptInType.Whitelist,
        //        OptInMethod = OptInMethod.TCO
        //    }, new FakeZumigoServiceTest());
        //}

        //[Fact]
        //public void AddDeviceReturnOnSuccess()
        //{
        //    var zumigoService = GetZumigoService();
        //    var response = GetController(zumigoService).AddDevice(new AddDeviceRequest()
        //    {
        //        MobileDeviceNumber = "919845901584",
        //        //OptInType = "Whitelist",
        //        //OptInMethod = "TCO",
        //        OptlnId = "12345xyz",
        //        OptInTimeStamp = System.DateTime.UtcNow.ToString("yyyyMMddHHmmssffff")
        //    });
        //    Assert.NotNull(response);
        //    Assert.IsType<JsonResult>(response);
        //}

        //[Fact]
        //public void AddDeviceReturnErrorOnIncompleteRequest()
        //{
        //    var zumigoService = GetZumigoService();
        //    var response = GetController(zumigoService).AddDevice(new AddDeviceRequest());
        //    Assert.IsType<ErrorResult>(response);
        //    var result = response as ErrorResult;
        //    Assert.NotNull(result);
        //    Assert.Equal(result.StatusCode, 404);
        //}

        //[Fact]
        //public void AddDeviceReturnErrorOnNullRequest()
        //{
        //    var zumigoService = GetZumigoService();
        //    var response = GetController(zumigoService).AddDevice(null);
        //    Assert.IsType<ErrorResult>(response);
        //    var result = response as ErrorResult;
        //    Assert.NotNull(result);
        //    Assert.Equal(result.StatusCode, 404);
        //}

        //[Fact]
        //public void DeleteDeviceReturnOnSuccess()
        //{
        //    //var zumigoService = GetZumigoService();
        //    //var response = GetController(zumigoService).DeleteDevice(new DeleteDeviceRequest()
        //    //{
        //    //    MobileDeviceNumber = "919845901584"
        //    //});
        //    //Assert.NotNull(response);
        //    //Assert.IsType<JsonResult>(response);
        //}

        //[Fact]
        //public void DeleteDeviceReturnErrorOnIncompleteRequest()
        //{
        //    //var zumigoService = GetZumigoService();
        //    //var response = GetController(zumigoService).DeleteDevice(new DeleteDeviceRequest());
        //    //Assert.IsType<ErrorResult>(response);
        //    //var result = response as ErrorResult;
        //    //Assert.NotNull(result);
        //    //Assert.Equal(result.StatusCode, 404);
        //}

        //[Fact]
        //public void DeleteDeviceReturnErrorOnNullRequest()
        //{
        //    //var zumigoService = GetZumigoService();
        //    //var response = GetController(zumigoService).DeleteDevice(null);
        //    //Assert.IsType<ErrorResult>(response);
        //    //var result = response as ErrorResult;
        //    //Assert.NotNull(result);
        //    //Assert.Equal(result.StatusCode, 404);
        //}

        //[Fact]
        //public void VerifyGetDeviceStatusSuccess()
        //{
        //    //var zumigoService = GetZumigoService();
        //    //var response = GetController(zumigoService).GetDeviceStatus("919825522544");

        //    //Assert.NotNull(response);
        //    //Assert.IsType<JsonResult>(response);
        //}

        //[Fact]
        //public void VerifyMobileLocationAddressOnSucess()
        //{
        //    //var zumigoService = GetZumigoService();
        //    //var response = GetController(zumigoService).GetMobileLocationAddress("919825522544");

        //    //Assert.NotNull(response);
        //    //Assert.IsType<JsonResult>(response);
        //}

        //[Fact]
        //public void ValidateOnlineTransactionOnSucess()
        //{
        //    //var zumigoService = GetZumigoService();
        //    //var response = GetController(zumigoService).ValidateOnlineTransaction(new ValidateOnlineTransactionRequest {
        //    //    MobileDeviceNumber = "919825522544"
        //    //});

        //    //Assert.NotNull(response);
        //    //Assert.IsType<JsonResult>(response);
        //}

        //[Fact]
        //public void GetDeviceStatusErrorOnNullArgument()
        //{
        //    var zumigoService = GetZumigoService();
        //    var response = GetController(zumigoService).GetDeviceStatus(null);

        //    Assert.IsType<ErrorResult>(response);
        //    var result = response as ErrorResult;
        //    Assert.NotNull(result);
        //    Assert.Equal(result.StatusCode, 404);
        //}

        //[Fact]
        //public void MobileLocationAddressErrorOnNullArgument()
        //{
        //    var zumigoService = GetZumigoService();
        //    var response = GetController(zumigoService).GetMobileLocationAndAddress(null);

        //    Assert.IsType<ErrorResult>(response);
        //    var result = response as ErrorResult;
        //    Assert.NotNull(result);
        //    Assert.Equal(result.StatusCode, 404);
        //}

        //[Fact]
        //public void ValidateOnlineTransactionErrorOnNullArgument()
        //{
        //    var zumigoService = GetZumigoService();
        //    var response = GetController(zumigoService).ValidateOnlineTransaction(null);

        //    Assert.IsType<ErrorResult>(response);
        //    var result = response as ErrorResult;
        //    Assert.NotNull(result);
        //    Assert.Equal(result.StatusCode, 404);
        //}

    }
}
