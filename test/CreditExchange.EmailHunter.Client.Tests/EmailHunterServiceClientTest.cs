﻿using CreditExchange.Syndication.EmailHunter;
using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using Xunit;

namespace CreditExchange.EmailHunter.Client.Tests
{
    public class EmailHunterServiceClientTest
    {
        public EmailHunterServiceClientTest()
        {
            MockServiceClient = new Mock<IServiceClient>();
            EmailHunterServiceClient = new EmailHunterService(MockServiceClient.Object);
        }

        private EmailHunterService EmailHunterServiceClient { get; }

        private IRestRequest Request { get; set; }

        private Mock<IServiceClient> MockServiceClient { get; }

        [Fact]
        public async void Client_Verify_Email()
        {
            //MockServiceClient.Setup(s => s.ExecuteAsync<EmailVerificationResult>(It.IsAny<RestRequest>()))
            //    .Callback<IRestRequest>(r => Request = r).ReturnsAsync(new EmailVerificationResult());

            var result = await EmailHunterServiceClient.EmailVerifier("steli@close.io");
            Assert.Equal("verify-email/{entitytype}/{entityid}/{email}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }
    }
}
