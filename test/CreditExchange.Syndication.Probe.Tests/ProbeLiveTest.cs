﻿using CreditExchange.Syndication.Probe.Proxy; //Probe.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Lenddo.Probe.Tests
{
    public class ProbeLiveTest
    {
        private IProbeServiceConfiguration ProbeServiceConfiguration { get; }
        private IProbeProxy ProbeProxy { get; }

        public ProbeLiveTest()
        {
            ProbeServiceConfiguration = new ProbeServiceConfiguration
            {
                ApiVersion = "1.1",
                Url = "https://api.probe42.in/probe_lite/",
                ApiKey = "mtqxzS3UsB84U2AETUy7w6qdVxn7fWZL9b84qhKI"
            };
            ProbeProxy = new ProbeProxy(ProbeServiceConfiguration);
        }

      
        [Fact]
        public void SearchCompanyTestSuccess()
        {
            var response = ProbeProxy.SearchCompany(new SearchRequest
            {
                limit=25,
                offset=0,
                filters= "{\"nameStartsWith\":\"Probe\"}"
            });
            Assert.NotNull(response);
        }

        [Fact]
        public void SearchAuthorizedSignatoryTestSuccess()
        {
            var response = ProbeProxy.SearchAuthorizedSignatory(new SearchRequest
            {
                limit = 25,
                offset = 0,
                filters = "{\"pan\":\"ADHPR6632H\"}"
            });
            Assert.NotNull(response);
        }

        [Fact]
        public void GetCompanyDetailTestSuccess()
        {
            var response = ProbeProxy.GetCompanyDetail(new DetailRequest
            {
               cin= "U73100KA2005PTC036337"
            });
            Assert.NotNull(response);
        }
        [Fact]
        public void GetFinancialDetailTestSuccess()
        {
            var response = ProbeProxy.GetFinancialDetail(new DetailRequest
            {
                cin = "U73100KA2005PTC036337"
            });
            Assert.NotNull(response);
        }
        [Fact]
        public void GetFinancialDataStatusTestSuccess()
        {
            var response = ProbeProxy.GetFinancialDataStatus(new DetailRequest
            {
                cin = "U73100KA2005PTC036337"
            });
            Assert.NotNull(response);
        }
        [Fact]
        public void GetChargesTestSuccess()
        {
            var response = ProbeProxy.GetChargesDetail(new DetailRequest
            {
                cin = "U73100KA2005PTC036337"
            });
            Assert.NotNull(response);
        }
        [Fact]
        public void GetAuthorisedSignatoryDetailTestSuccess()
        {
            var response = ProbeProxy.GetAuthorizedSignatoryDetail(new DetailRequest
            {
                cin = "U73100KA2005PTC036337"
            });
            Assert.NotNull(response);
        }
    }
}
