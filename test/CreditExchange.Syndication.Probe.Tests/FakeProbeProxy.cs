﻿using CreditExchange.Syndication.Probe.Proxy;

namespace LendFoundry.Lenddo.Probe.Tests
{
    public class FakeProbeProxy : IProbeProxy
    {
        public static FinancialDetailResponse GetFinancialDetailResponse1()
        {
            var financialDetailResponse = new FinancialDetailResponse()
            {
                meta = new Metadata
                {
                    apiversion = "1.1",
                    last_updated = "2016-03-08"
                },
                data = new FinancialData
                {
                    financials = new Financial[]
                    {
                        new Financial
                        {
                           year= "2015-03-31",
                           nature= "STANDALONE",
                           stated_on= "2015-03-31",
                           capital_wip= 0,
                           cash_and_bank_balances= 309423550,
                           investments= 0,
                           inventories= 0,
                           net_fixed_assets= 76244361,
                           other_assets= 175883410,
                           total_assets= 624684793,
                           trade_receivables= 63133472,
                           share_capital= 103834297,
                           reserves_and_surplus= 270189861,
                          others= 0,
                          long_term_borrowings= 0,
                          other_current_liabilities_and_provisions= 40115026,
                          other_long_term_liabilities_and_provisions= 16834935,
                          trade_payables= 193710674,
                          short_term_borrowings= 0,
                          total_equity= 374024158,
                          total_equity_and_liabilities= 624684793,
                          depreciation= 14868068,
                          income_tax= 0,
                          interest= 0,
                          operating_cost= 843161603,
                          operating_profit= -508075379,
                          other_income= 58311646,
                          profit_before_interest_and_tax= -464631801,
                          profit_before_tax= -464631801,
                          revenue= 335086224,
                          net_income= -464631801,
                          exceptional_item_before_tax= 0,
                          pnl_version= "0"
                        }
                    }
                }



            };
            return financialDetailResponse;
        }
        public static FinancialDetailResponse GetFinancialDetailResponse2()
        {
            var financialDetailResponse = new FinancialDetailResponse()
            {
                meta = new Metadata
                {
                    apiversion = "1.1",
                    last_updated = "2016-03-08"
                },
                data = new FinancialData
                {
                    financials = new Financial[]
                    {
                        new Financial
                        {
                           year= "2015-03-31",
                           nature= "STANDALONE",
                           stated_on= "2015-03-31",
                           capital_wip= 0,
                           cash_and_bank_balances= 309423550,
                           investments= 0,
                           inventories= 0,
                           net_fixed_assets= 76244361,
                           other_assets= 175883410,
                           total_assets= 624684793,
                           trade_receivables= 63133472,
                           share_capital= 103834297,
                           reserves_and_surplus= 270189861,
                          others= 0,
                          long_term_borrowings= 0,
                          other_current_liabilities_and_provisions= 40115026,
                          other_long_term_liabilities_and_provisions= 16834935,
                          trade_payables= 193710674,
                          short_term_borrowings= 0,
                          total_equity= 374024158,
                          total_equity_and_liabilities= 624684793,
                          depreciation= 14868068,
                          income_tax= 0,
                          interest= 0,
                          operating_cost= 843161603,
                          operating_profit= -508075379,
                          other_income= 58311646,
                          profit_before_interest_and_tax= -464631801,
                          profit_before_tax= -464631801,
                          revenue= 335086224,
                          minority_interest_and_profit_from_associates_and_joint_ventures = null,
                          exceptional_items_before_tax = null,
                          pnl_version= "1",
                          profit_after_tax_but_before_exceptional_items_after_tax =0,
                          profit_before_tax_and_exceptional_items_before_tax=0,
                          profit_after_tax =193496102
                        }
                    }
                }
            };
            return financialDetailResponse;
        }
        public static CompanySearchResponse GetCompanySearchResponse()
        {
            var companySearchResponse = new CompanySearchResponse
            {
                meta = new SearchMetadata
                {
                    apiversion = "1.1"
                },
                data = new CompanyData
                {
                    companies = new[]
                    {
                        new Syndication.Probe.Proxy.SearchCompany
                        {
                            cin ="U73100KA2005PTC036337",
                            legal_name ="Public Limited Indian Non-Government Company"
                        }
                    },
                    hasMore = true,
                    totalCount = 1

                }

            };
            return companySearchResponse;
        }
        public static CompanyDetailResponse GetCompanyDetailResponse()
        {
            var authorizedSignatoryResponse = new CompanyDetailResponse
            {
                meta = new Metadata
                {
                    apiversion = "1.1",
                    last_updated = "2016-03-08"
                },
                data = new CompanyDetailData
                {
                    company = new Company
                    {
                        authorized_capital=0,
                        cin="",
                        classification="",
                        efiling_status="",
                        incorporation_date="",
                        last_agm_date="",
                        last_filing_date="",
                        legal_name="",
                        next_cin="",
                        paid_up_capital=0,
                        registered_address = new Registered_Address
                        {
                            address_line1="",
                            address_line2="",
                            city="",
                            pincode="",
                            state=""

                        }

                    },

                },




            };
            return authorizedSignatoryResponse;
        }
        public static AuthorizedSignatorySearchResponse GetAuthorizedSignatorySearchResponse()
        {
            var authorizedSignatorySearchResponse = new AuthorizedSignatorySearchResponse
            {
                meta = new SearchMetadata
                {
                    apiversion = "1.1"
                },
                data = new AuthorizedSignatoryData
                {
                    authorizedsignatories = new[]
                    {
                        new AuthorizedSignatories
                        {
                              name= "string",
		pan= "string",
		din= "string",
		date_of_birth= "string",
		age= 0,
		nationality= "string",
        address= new AuthorizedSignatoryAddress
        {
                address_line1= "string",
          address_line2= "string",
          city= "string",
          pincode= "string",
          state= "string"
        },
        companies=new[]
        {
         new Syndication.Probe.Proxy.SearchCompany
         {
              cin ="U73100KA2005PTC036337",
              legal_name ="Public Limited Indian Non-Government Company"
         }
            },

              }

                    },
                    hasMore = true,
                    totalCount = 0
                }

            };
            return authorizedSignatorySearchResponse;
        }
        public static AuthorizedSignatoryResponse GetAuthorizedSignatoryResponse()
        {
            var authorizedSignatoryResponse = new AuthorizedSignatoryResponse
            {
                meta = new Metadata
                {
                    apiversion = "1.1",
                    last_updated= "2016-03-08"
                },
                data = new CompanyAuthorizedSignatories
                {
                    authorizedsignatories = new[]
                    {
                        new AuthorizedSignatoriesDetail
                        {
                              name= "string",
                               pan= "string",
                            din= "string",
                          date_of_birth= "string",
                          age= 0,
                           date_of_appointment="",
                            date_of_appointment_for_current_designation="",
                            date_of_cessation=""

              }

                    },
                         
                },
               
                    
                

            };
            return authorizedSignatoryResponse;
        }

        public static ChargesResponse GetChargesDetailResponse()
        {
            var chargesResponse = new ChargesResponse
            {
                metadata = new Metadata
                {
                    apiversion="1.1",
                    last_updated=""
                },
                data = new ChargeData
                {
                    charges =new[]
                    {
                        new Charge
                        {
                            amount=0,
                            date="",
                            holder_name="",
                            type=""
                        }
                    }
                }

            };
            return chargesResponse;
        }

        public static FinancialDataStatusResponse GetFinancialDataStatusResponse()
        {
            var financialDataStatusResponse = new FinancialDataStatusResponse
            {
              meta = new SearchMetadata
              {
                  apiversion=""
              }  ,
              data=new FinancialDataData
              {
                  datastatus = new FinancialDataDataStatus
                  {
                      last_fin_year_end="",
                      last_updated=""
                  }
              }
            };
            return financialDataStatusResponse;
        }
        public AuthorizedSignatoryResponse GetAuthorizedSignatoryDetail(DetailRequest companydetailrequest)
        {
            if (companydetailrequest.Cin == "U73100KA2005PTC036337")
                return GetAuthorizedSignatoryResponse();
            return null;
        }

        public ChargesResponse GetChargesDetail(DetailRequest companydetailrequest)
        {
            if (companydetailrequest.Cin == "U73100KA2005PTC036337")
                return GetChargesDetailResponse();
            return null;
        }

        public CompanyDetailResponse GetCompanyDetail(DetailRequest companydetailrequest)
        {
            if (companydetailrequest.Cin == "U73100KA2005PTC036337")
                return GetCompanyDetailResponse();
            return null;
        }

        public FinancialDataStatusResponse GetFinancialDataStatus(DetailRequest companydetailrequest)
        {
            if (companydetailrequest.Cin == "U73100KA2005PTC036337")
                return GetFinancialDataStatusResponse();
            return null;
        }

        public FinancialDetailResponse GetFinancialDetail(DetailRequest companydetailrequest)
        {
            if (companydetailrequest.Cin == "U73100KA2005PTC036337")
                return GetFinancialDetailResponse1();
            else if(companydetailrequest.Cin == "U73100KA2005PTC036339")
                return GetFinancialDetailResponse2();
            return null;
        }

        public AuthorizedSignatorySearchResponse SearchAuthorizedSignatory(SearchRequest searchrequest)
        {
            if (searchrequest.Filter == "{\"pan\":\"ADHPR6632H\"}")
                return GetAuthorizedSignatorySearchResponse();
            return null;
        }

        public CompanySearchResponse SearchCompany(SearchRequest searchrequest)
        {
            if (searchrequest.Filter == "{\"nameStartsWith\":\"Probe\"}")
                return GetCompanySearchResponse();
            return null;
        }

    }
}
