﻿using LendFoundry.Lenddo.Probe.Proxy;
using CreditExchange.Syndication.Probe;
using CreditExchange.Syndication.Probe.Proxy;
using System;
using Xunit;

namespace LendFoundry.Lenddo.Probe.Tests
{
    public class ProbeProxyTest
    {
        [Fact]
        public void ThrowArgumentExceptionWithNullConfiguration()
        {
            Assert.Throws<ArgumentNullException>(() => new ProbeProxy(null));
        }
        [Fact]
        public void ThrowArgumentExceptionWithMissingConfiguration()
        {
            var configuration = new ProbeServiceConfiguration()
            {
                ApiVersion = "1.1",
                Url = "https://api.probe42.in/probe_lite/"
            };
            Assert.Throws<ArgumentException>(() => new ProbeProxy(configuration));
        }

        [Fact]
        public void ThrowArgumentExceptionWithNullParameterProbeProxy_GetCompanyDetail()
        {
            var configuration = new ProbeServiceConfiguration()
            {
                ApiVersion = "1.1",
                Url = "https://api.probe42.in/probe_lite/",
                ApiKey = "mtqxzS3UsB84U2AETUy7w6qdVxn7fWZL9b84qhKI"

            };
            Assert.Throws<ArgumentNullException>(() =>
            {
                var proxy = new ProbeProxy(configuration);
                proxy.GetCompanyDetail(null);
            });
        }

        [Fact]
        public void ThrowArgumentExceptionWithNullParameterProbeProxy_GetAuthorizedSignatoryDetail()
        {
            var configuration = new ProbeServiceConfiguration()
            {
                ApiVersion = "1.1",
                Url = "https://api.probe42.in/probe_lite/",
                ApiKey = "mtqxzS3UsB84U2AETUy7w6qdVxn7fWZL9b84qhKI"
            };
            Assert.Throws<ArgumentNullException>(() =>
            {
                var proxy = new ProbeProxy(configuration);
                proxy.GetAuthorizedSignatoryDetail(null);
            });
        }
        [Fact]
        public void ThrowArgumentExceptionWithNullParameterProbeProxy_GetChargesDetail()
        {
            var configuration = new ProbeServiceConfiguration()
            {
                ApiVersion = "1.1",
                Url = "https://api.probe42.in/probe_lite/",
                ApiKey = "mtqxzS3UsB84U2AETUy7w6qdVxn7fWZL9b84qhKI"
            };
            Assert.Throws<ArgumentNullException>(() =>
            {
                var proxy = new ProbeProxy(configuration);
                proxy.GetChargesDetail(null);
            });
        }
        [Fact]
        public void ThrowArgumentExceptionWithNullParameterProbeProxy_GetFinancialDataStatus()
        {
            var configuration = new ProbeServiceConfiguration()
            {
                ApiVersion = "1.1",
                Url = "https://api.probe42.in/probe_lite/",
                ApiKey = "mtqxzS3UsB84U2AETUy7w6qdVxn7fWZL9b84qhKI"
            };
            Assert.Throws<ArgumentNullException>(() =>
            {
                var proxy = new ProbeProxy(configuration);
                proxy.GetFinancialDataStatus(null);
            });
        }

        [Fact]
        public void ThrowArgumentExceptionWithNullParameterProbeProxy_GetFinancialDetail()
        {
            var configuration = new ProbeServiceConfiguration()
            {
                ApiVersion = "1.1",
                Url = "https://api.probe42.in/probe_lite/",
                ApiKey = "mtqxzS3UsB84U2AETUy7w6qdVxn7fWZL9b84qhKI"
            };
            Assert.Throws<ArgumentNullException>(() =>
            {
                var proxy = new ProbeProxy(configuration);
                proxy.GetFinancialDetail(null);
            });
        }
        [Fact]
        public void ThrowArgumentExceptionWithNullParameterProbeProxy_SearchCompany()
        {
            var configuration = new ProbeServiceConfiguration()
            {
                ApiVersion = "1.1",
                Url = "https://api.probe42.in/probe_lite/",
                ApiKey = "mtqxzS3UsB84U2AETUy7w6qdVxn7fWZL9b84qhKI"
            };
            Assert.Throws<ArgumentNullException>(() =>
            {
                var proxy = new ProbeProxy(configuration);
                proxy.SearchCompany(null);
            });
        }
        [Fact]
        public void ThrowArgumentExceptionWithNullParameterProbeProxy_SearchAuthorizedSignatory()
        {
            var configuration = new ProbeServiceConfiguration()
            {
                ApiVersion = "1.1",
                Url = "https://api.probe42.in/probe_lite/",
                ApiKey = "mtqxzS3UsB84U2AETUy7w6qdVxn7fWZL9b84qhKI"
            };
            Assert.Throws<ArgumentNullException>(() =>
            {
                var proxy = new ProbeProxy(configuration);
                proxy.SearchAuthorizedSignatory(null);
            });
        }
    }
}