﻿using LendFoundry.Lenddo.Probe.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Lenddo.Probe.Tests
{
    public class ProbeServiceTest
    {
        private static IProbeServiceConfiguration ProbeServiceConfiguration { get; } = new ProbeServiceConfiguration
        {
            ApiVersion = "1.1",
            Url = "https://api.probe42.in/probe_lite/",
            ApiKey = "mtqxzS3UsB84U2AETUy7w6qdVxn7fWZL9b84qhKI"
        };

        private ProbeService ProbeService { get; } = new ProbeService(ProbeServiceConfiguration, new FakeProbeProxy());
        [Fact]
        public void VerifyThrowsArgumentExceptionWithSearchCompanyRequestInValidFileterFormat()
        {
            Assert.Throws<ArgumentNullException>(() => ProbeService.SearchAuthorizedSignatory(
                new GetSearchRequest
                {
                    Filters = null
                }));
        }
        [Fact]
        public void VerifyThrowsArgumentExceptionWithSearchAuthorizedSignatoryRequestFileterNull()
        {
            Assert.Throws<ArgumentNullException>(() => ProbeService.SearchAuthorizedSignatory(
                new GetSearchRequest
                {
                    Filters =null
                }));
        }
        [Fact]
        public void VerifyThrowsArgumentExceptionWithNullGetCompanyDetailRequest()
        {
            Assert.Throws<ArgumentNullException>(() => ProbeService.GetCompanyDetail(null));
        }
        [Fact]
        public void VerifyThrowsArgumentExceptionWithGetCompanyDetailRequestInvalidCinFormat()
        {
            Assert.Throws<ArgumentNullException>(() => ProbeService.GetCompanyDetail(
                new GetCompanyDetailRequest
                {
                    Cin ="adf"
                }));
        }
        [Fact]
        public void VerifyThrowsArgumentExceptionWithMissingGetCompanyDetailRequest()
        {
            Assert.Throws<ArgumentException>(() => ProbeService.GetCompanyDetail(new GetCompanyDetailRequest()));
        }
        [Fact]
        public void VerifyThrowsArgumentExceptionWithNullGetFinancialDataStatusRequest()
        {
            Assert.Throws<ArgumentNullException>(() => ProbeService.GetFinancialDataStatus(null));
        }
        [Fact]
        public void VerifyThrowsArgumentExceptionWithMissingGetFinancialDataStatusRequest()
        {
            Assert.Throws<ArgumentException>(() => ProbeService.GetFinancialDataStatus(new GetCompanyDetailRequest()));
        }
        [Fact]
        public void VerifyThrowsArgumentExceptionWithNullGetFinancialDetailRequest()
        {
            Assert.Throws<ArgumentNullException>(() => ProbeService.GetFinancialDetail(null));
        }
        [Fact]
        public void VerifyThrowsArgumentExceptionWithMissingGetFinancialDetailRequest()
        {
            Assert.Throws<ArgumentException>(() => ProbeService.GetFinancialDetail(new GetCompanyDetailRequest()));
        }
        [Fact]
        public void VerifyThrowsArgumentExceptionWithNullGetChargesDetailRequest()
        {
            Assert.Throws<ArgumentNullException>(() => ProbeService.GetChargesDetail(null));
        }
        [Fact]
        public void VerifyThrowsArgumentExceptionWithMissingGetChargesDetailRequest()
        {
            Assert.Throws<ArgumentException>(() => ProbeService.GetChargesDetail(new GetCompanyDetailRequest()));
        }
        [Fact]
        public void VerifyThrowsArgumentExceptionWithNullGetAuthorisedSignatoryDetailRequest()
        {
            Assert.Throws<ArgumentNullException>(() => ProbeService.GetAuthorisedSignatoryDetail(null));
        }
        [Fact]
        public void VerifyThrowsArgumentExceptionWithMissingGetAuthorisedSignatoryDetailRequest()
        {
            Assert.Throws<ArgumentException>(() => ProbeService.GetAuthorisedSignatoryDetail(new GetCompanyDetailRequest()));
        }

        [Fact]
        public void CompanySearach_MappingTest()
        {
            var searchresponse = ProbeService.SearchCompany(new GetSearchRequest
            {
                Filters= "{\"nameStartsWith\":\"Probe\"}"
            });

        }
    }
}
