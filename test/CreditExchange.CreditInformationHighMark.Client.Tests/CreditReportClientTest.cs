﻿using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.Crif;
using CreditExchange.Syndication.Crif.CreditReport.Request;
using CreditExchange.Syndication.Crif.CreditReport.Response;
using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using Xunit;

namespace CreditExchange.CreditInformationHighMark.Client.Tests
{
    public class CreditReportClientTest
    {

        private Crif.CreditReport.Client.CrifCreditReportService Client { get; }
        private IRestRequest Request { get; set; }
        private Mock<IServiceClient> ServiceClient { get; }

        public CreditReportClientTest()
        {
            ServiceClient = new Mock<IServiceClient>();
            Client = new Crif.CreditReport.Client.CrifCreditReportService(ServiceClient.Object);
        }


        [Fact]
        public async void Client_SendCreditReportInquiry()
        {
            var response = new CreditReportInquiryResponse();
            var panVerificationRequest = new CreditReportInquiryRequest() { Name = "abc", Gender = Gender.Female, Pan = "12346ffl", Dob = DateTime.Now , Addresses = new List<CreditExchange.Syndication.Crif.CreditReport.SendInquiry.IAddressSegment>(),DrivingLicNo = "123" ,LandlineNo ="123456" ,MobileNo = "69584714" ,Passport = "1234567989",VoterId= "1"};

            ServiceClient.Setup(x => x.ExecuteAsync<CreditReportInquiryResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(response);

            var result = await Client.SendInquiry("123", "123", panVerificationRequest);
            Assert.Equal("/{entitytype}/{entityid}", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }


        [Fact]
        public async void Client_GetReport()
        {
            var response = new GetCreditReportResponse();

            ServiceClient.Setup(x => x.ExecuteAsync<GetCreditReportResponse>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => Request = r)
               .ReturnsAsync(response);

            var result = await Client.GetReport("123", "123", "123","123");
            Assert.Equal("/{entitytype}/{entityid}/{inquiryreferencenumber}/{reportid}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

    }
}
