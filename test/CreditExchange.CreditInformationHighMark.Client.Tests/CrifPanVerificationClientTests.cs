﻿using CreditExchange.Crif.PanVerification.Client;
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.Crif;
using CreditExchange.Syndication.Crif.PanVerification.Proxy;
using CreditExchange.Syndication.Crif.PanVerification.Request;
using CreditExchange.Syndication.Crif.PanVerification.Response;
using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.CreditInformationHighMark.Client.Tests
{
    public class CrifPanVerificationClientTests
    {
        private CrifPanVerificationService Client { get; }
        private IRestRequest Request { get; set; }
        private Mock<IServiceClient> ServiceClient { get; }

        public CrifPanVerificationClientTests()
        {
            ServiceClient = new Mock<IServiceClient>();
            Client = new CrifPanVerificationService(ServiceClient.Object);
        }


        [Fact]
        public async void Client_VerifyPan()
        {
            var response = new GetPanVerificationResponse();
            var panVerificationRequest = new PanVerificationRequest() { Name = "abc", Gender = Gender.Female, Pan = "12346ffl", Dob = DateTime.Now };

            ServiceClient.Setup(x => x.ExecuteAsync<IPanVerificationResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(response);

            var result = await Client.VerifyPan("123", "123", panVerificationRequest);
            Assert.Equal("/{entityType}/{entityId}", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }

    }
}
