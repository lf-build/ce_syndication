﻿using LendFoundry.EventHub.Client;
using CreditExchange.ViewDNS.Api.Controllers;
using Moq;
using CreditExchange.Syndication.ViewDNS.Tests;
using Xunit;
using System.Threading.Tasks;
using CreditExchange.CompanyDb.Abstractions;
using System.Collections.Generic;
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.ViewDNS;
using CreditExchange.Syndication.EmailHunter;

namespace CreditExchange.ViewDNS.Api.Tests
{
    /// <summary>
    /// View dns controller test class
    /// </summary>
    public class ViewDnsControllerTest
    {
        /// <summary>
        /// Gets the controller.
        /// </summary>
        /// <param name="viewdnsService">The viewdns service.</param>
        /// <returns></returns>
        private ApiController GetController(IViewDnsService viewdnsService)
        {
            return new ApiController(viewdnsService);
        }

        /// <summary>
        /// Gets the view DNS service.
        /// </summary>
        /// <returns></returns>
        private IViewDnsService GetViewDnsService()
        {
            return new ViewDnsService(new FakeViewDnsProxy(), Mock.Of<ICompanyService>(), Mock.Of<IEmailHunterService>(), new DomainMatching(), new FilterDomain(), new CompanyNameGenerator(), Mock.Of<IEventHubClient>(), Mock.Of<IEntityValidator>());
        }

        /// <summary>
        /// Gets the view DNS service.
        /// </summary>
        /// <param name="companyDetail">The company detail.</param>
        /// <returns></returns>
        private IViewDnsService GetViewDnsService(Mock<ICompanyService> companyService, Mock<IEmailHunterService> emailHunterService, Mock<IEntityValidator> entityValidator)
        {
            return new ViewDnsService(new FakeViewDnsProxy(), companyService.Object, emailHunterService.Object, new DomainMatching(), new FilterDomain(), new CompanyNameGenerator(), Mock.Of<IEventHubClient>(), entityValidator.Object);
        }

        /// <summary>
        /// Verifies the employee return on success.
        /// </summary>
        //[Fact]
        //public void VerifyEmployeeReturnOnSuccess()
        //{
        //    var mockValidator = new Mock<IEntityValidator>();
        //    mockValidator.Setup(v => v.EnsureEntityType(It.IsAny<string>())).Returns("app");

        //    var mockCompanyDetail = new Mock<ICompanyService>();
        //    mockCompanyDetail.Setup(c => c.GetCompanyWithDirectors(It.IsAny<string>())).Returns(Task.FromResult(GetCompanyDetails()));

        //    var mockEmailHunter = new Mock<IEmailHunterService>();
        //    mockEmailHunter.Setup(c => c.EmailVerifier(It.IsAny<string>())).ReturnsAsync(GetEmailHunterDetails());

        //    var viewdnsService = GetViewDnsService(mockCompanyDetail, mockEmailHunter, mockValidator);

        //    var response = GetController(viewdnsService).VerifyEmployment("app", "1", "hiren.p@bacreations.com", "B & A CREATIONS PRIVATE LIMITED", "U18109DL2008PTC177167");
        //    Assert.NotNull(response);
        //}

        /// <summary>
        /// Verifies the employee return error on incomplete request.
        /// </summary>
        //[Fact]
        //public void VerifyEmployeeReturnErrorOnIncompleteRequest()
        //{
        //    var mockValidator = new Mock<IEntityValidator>();
        //    mockValidator.Setup(v => v.EnsureEntityType(It.IsAny<string>())).Returns("app");

        //    var mockCompanyDetail = new Mock<ICompanyService>();
        //    mockCompanyDetail.Setup(c => c.GetCompanyWithDirectors(It.IsAny<string>())).Returns(Task.FromResult(GetCompanyDetails()));

        //    var mockEmailHunter = new Mock<IEmailHunterService>();
        //    mockEmailHunter.Setup(c => c.EmailVerifier(It.IsAny<string>())).ReturnsAsync(GetEmailHunterDetails());

        //    var viewdnsService = GetViewDnsService(mockCompanyDetail, mockEmailHunter, mockValidator);

        //    var response = GetController(viewdnsService).VerifyEmployment("app", "1", null, null, null);

        //    Assert.IsType<ErrorResult>(response.Result);
        //    var result = response.Result as ErrorResult;
        //    Assert.NotNull(result);
        //    Assert.Equal(result.StatusCode, 400);
        //}

        /// <summary>
        /// Gets the company details.
        /// </summary>
        /// <returns></returns>
        private CompanyWithDirectors GetCompanyDetails()
        {
            return new CompanyWithDirectors
            {
                Company = new Company
                {
                    Cin = "U18109DL2008PTC177167",
                    Domain = "www.infosys.com",
                    Name = "B & A CREATIONS PRIVATE LIMITED"
                },
                Directors = new List<CompanyDirector>
                {
                    new CompanyDirector
                    {
                        Cin = "U18109DL2008PTC177167",
                        Din = "01627194",
                        Name = "MANIK GUPTA"
                    }
                }
            };
        }

        /// <summary>
        /// Gets the email hunter details.
        /// </summary>
        /// <returns></returns>
        private EmailVerificationResult GetEmailHunterDetails()
        {
            return new EmailVerificationResult
            {
                Email = "hiren.p@ambitiousleggings.com",
                Gibberish = false,
                MxRecords = true,
                Regexp = true,
                Disposable = false,
                AcceptAll = true,
                Result = "",
                Score = 100,
                SmtpCheck = true,
                SmtpServer = true,
                Sources = new List<SourceResult>(),
                Webmail = false
            };
        }
    }
}