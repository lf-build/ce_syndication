﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.Crif;
using CreditExchange.Syndication.Crif.CreditReport;
using CreditExchange.Syndication.Crif.CreditReport.Proxy;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Inquiry;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Inquiry.Response;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;
using CreditExchange.Syndication.Crif.CreditReport.Request;
using CreditExchange.Syndication.Crif.CreditReport.SendInquiry;
using CreditExchange.Syndication.Crif.PanVerification;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using LendFoundry.Foundation.Date;

namespace CreditExchange.Syndication.CreditInformationHighMark.Tests
{
    public class CrifCreditReportServiceTests
    {

        private Mock<ICrifCreditReportConfiguration> Configuration { get; }
        private Mock<ICrifCreditReportProxy> Proxy { get; }
        private Mock<IEventHubClient> EventHubClient { get; }
        private Mock<ILookupService> Lookup { get; }
        private ICrifCreditReportService creditReportService { get; }
        private ICrifCreditReportConfiguration crifCreditReportConfiguration;
        private ICrifCreditReportRepository crifCreditReportRepository { get; }


        public CrifCreditReportServiceTests()
        {
            EventHubClient = new Mock<IEventHubClient>();
             crifCreditReportConfiguration = new CrifCreditReportConfiguration() { ActionType = "SUBMIT", ApiUrl = "123", Password = "123", UserId = "123", SubMemberId = "1", MemberId = "123", ProductType = "INDV", ProductVersion = "3.0", AuthTitle = "123" ,InquiryPurposeType= "ACCTORIG" ,CreditInquiryStage = "PRESCREEN" };

            // Configuration = new Mock<ICrifPanVerificationConfiguration>();
                Proxy = new Mock<ICrifCreditReportProxy>();
            Lookup = new Mock<ILookupService>();

            creditReportService = new CrifCreditReportService(crifCreditReportConfiguration, Proxy.Object, EventHubClient.Object, Lookup.Object, crifCreditReportRepository, new UtcTenantTime());
        }

        [Fact]
        public async void SendCreditReportInquiry_With_Valid_Argument()
        {
            crifCreditReportConfiguration.RequestVolumeType = "SINGLE";
            var reportInquiryRequest = new CreditReportInquiryRequest() { Name = "abc", Gender = Gender.Female, Pan = "12346ffl", Dob = DateTime.Now, DrivingLicNo = "134689", Passport = "123456", VoterId = "444", Addresses = new List<IAddressSegment>() };
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("ABC", "1");

            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Proxy.Setup(x => x.SendInquiry(It.IsAny<SendInquiryRequest>())).Returns(new AcknowledgementResponse());

            var result = await creditReportService.SendInquiry("123", "123", reportInquiryRequest);
            Assert.NotNull(result);
        }

        [Fact]
        public void SendCreditReportInquiry_With_InValid_Argument()
        {
            var reportInquiryRequest = new CreditReportInquiryRequest() { Gender = Gender.Female, Dob = DateTime.Now, };
            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("", "123", reportInquiryRequest));

            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("123", "", reportInquiryRequest));

            Assert.ThrowsAsync<InvalidArgumentException>(() => creditReportService.SendInquiry(" ", "123", reportInquiryRequest));

            Assert.ThrowsAsync<InvalidArgumentException>(() => creditReportService.SendInquiry("123", "123", reportInquiryRequest));

            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("ABC", "1");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => creditReportService.SendInquiry("123", "123", null));

            //With Null Name in Request Argument
            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("123", "123", reportInquiryRequest));

            //With Null Pan in Request Argument
            reportInquiryRequest.Name = "abc";
            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("123", "123", reportInquiryRequest));

            //with Null Addresses in request argument
            reportInquiryRequest.Pan = "12345fff";
            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("123", "123", reportInquiryRequest));
            
        }


        [Fact]
        public async void GetReport_With_Valid_Argument()
        {
          
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("ABC", "1");

            //set the ReportActionType for Configuration
            crifCreditReportConfiguration.ReportActionType = "ISSUE";
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Proxy.Setup(x => x.IssueReport(It.IsAny<ReportRequest>())).Returns(new ReportResponse());

            var result = await creditReportService.GetReport("123", "123", "132","123");
            Assert.NotNull(result);
            Assert.NotNull(result.ReferenceNumber);
        }

        [Fact]
        public void GetReport_With_InValid_Argument()
        {

            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.GetReport("", "123", "123", "123"));

            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.GetReport("123", "", "123", "123"));

            Assert.ThrowsAsync<InvalidArgumentException>(() => creditReportService.GetReport(" ", "123", "123", "123"));

            Assert.ThrowsAsync<InvalidArgumentException>(() => creditReportService.GetReport("123", "123", "123", "132"));

            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("ABC", "1");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => creditReportService.GetReport("123", "123", " ", "123"));

            //With Null Name in Request Argument
            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.GetReport("123", "123", "123", " "));


            }
        }
}
