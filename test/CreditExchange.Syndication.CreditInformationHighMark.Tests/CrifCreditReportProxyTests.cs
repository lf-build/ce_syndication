﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;
using CreditExchange.Syndication.Crif;
using CreditExchange.Syndication.Crif.CreditReport;
using CreditExchange.Syndication.Crif.CreditReport.Proxy;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Inquiry;
using CreditExchange.Syndication.Crif.CreditReport.Request;
using CreditExchange.Syndication.Crif.CreditReport.SendInquiry;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using LendFoundry.Foundation.Date;

namespace CreditExchange.Syndication.CreditInformationHighMark.Tests
{
    public class CrifCreditReportProxyTests
    {

        private Mock<ICrifCreditReportConfiguration> Configuration { get; }
        private Mock<ICrifCreditReportProxy> Proxy { get; }
        private Mock<IEventHubClient> EventHubClient { get; }
        private Mock<ILookupService> Lookup { get; }
        private ICrifCreditReportService creditReportService { get; }
        private ICrifCreditReportConfiguration creditReportConfiguration { get; }

        private ICrifCreditReportRepository crifCreditReportRepository { get; }

        public CrifCreditReportProxyTests()
        {
            EventHubClient = new Mock<IEventHubClient>();
            creditReportConfiguration = new CrifCreditReportConfiguration()
            {
                ActionType = "SUBMIT",
                AuthFlag = "123",
                AuthTitle = "1223",
                MemberPreOverride = "123",
                TestFlag = "123",
                RequestVolumeType = "SINGLE",
                LosName = "123",
                LosVersion = "1.0",
                LosVendor = "test"
            };

            // Configuration = new Mock<ICrifPanVerificationConfiguration>();
            Proxy = new Mock<ICrifCreditReportProxy>();
            Lookup = new Mock<ILookupService>();
           // crifCreditReportRepository = new Mock<ICrifCreditReportRepository>();
         //   creditReportService = new CrifCreditReportService(creditReportConfiguration, Proxy.Object, EventHubClient.Object, crifCreditReportRepository, Lookup.Object, new UtcTenantTime());
        }

        [Fact]
        public void SendCreditReportInquiry_With_InValid_Argument_For_CrifPanVerificationConfiguration()
        {
            var reportInquiryRequest = new CreditReportInquiryRequest() { Name = "abc", Gender = Gender.Female, Pan = "12346ffl", Dob = DateTime.Now, DrivingLicNo = "134689", Passport = "123456", VoterId = "444", Addresses = new List<IAddressSegment>() };

            // var sendInquiryRequest = new SendInquiryRequest() { Name = "abc", Gender = Gender.Female, Pan = "12346ffl", Dob = DateTime.Now };
            //Mock<LendFoundry.Syndication.Crif.PanVerification.Proxy.Request> proxy = new Mock<Request>(It.IsAny<ICrifPanVerificationConfiguration>(), It.IsAny<IPanVerificationRequest>());
            // ICrifPanVerificationConfiguration panVerificationConfiguration = new CrifPanVerificationConfiguration() { ActionType = "123", ApiUrl = "123", ServiceType = "123", RequestVolumeType = "123", Password = "123", UserId = "123", SubMemberId = "1", MemberId = "123", ProductType = "123", ProductVersion = "123" };
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("ABC", "1");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);

            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("123", "123", reportInquiryRequest));

            creditReportConfiguration.ProductType = "123";
            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("123", "123", reportInquiryRequest));

            creditReportConfiguration.ProductVersion = "123";
            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("123", "123", reportInquiryRequest));

            creditReportConfiguration.MemberId = "123";
            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("123", "123", reportInquiryRequest));

            creditReportConfiguration.SubMemberId = "123";
            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("123", "123", reportInquiryRequest));

            creditReportConfiguration.UserId = "123";
            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("123", "123", reportInquiryRequest));

            creditReportConfiguration.Password = "123";
            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("123", "123", reportInquiryRequest));

            creditReportConfiguration.RequestVolumeType = "123";
            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("123", "123", reportInquiryRequest));

            creditReportConfiguration.ApiUrl = "123";
            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("123", "123", reportInquiryRequest));

            creditReportConfiguration.SubMemberId = "123";
            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("123", "123", reportInquiryRequest));

            creditReportConfiguration.ProductType = "INDV";
            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("123", "123", reportInquiryRequest));

            creditReportConfiguration.ProductVersion = "1.0";
            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("123", "123", reportInquiryRequest));

            creditReportConfiguration.RequestVolumeType = "SINGLE";
            Assert.ThrowsAsync<ArgumentException>(() => creditReportService.SendInquiry("123", "123", reportInquiryRequest));

        }

    }
}
