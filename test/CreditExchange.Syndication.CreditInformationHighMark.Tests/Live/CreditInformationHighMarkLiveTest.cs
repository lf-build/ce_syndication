﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;
using CreditExchange.Syndication.Crif;
using CreditExchange.Syndication.Crif.PanVerification;
using CreditExchange.Syndication.Crif.PanVerification.Proxy;
using CreditExchange.Syndication.Crif.PanVerification.Request;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.Syndication.CreditInformationHighMark.Tests.Live
{
    public class CreditInformationHighMarkLiveTest
    {

        private ICrifVerficationProxy CrifverificationProxy { get; }

        private ICrifPanVerificationConfiguration crifPanVerificationConfiguration { get; }


        private Mock<ICrifPanVerificationConfiguration> Configuration { get; }
       // private Mock<ICrifVerficationProxy> Proxy { get; }
        private Mock<IEventHubClient> EventHubClient { get; }
        private Mock<ILookupService> Lookup { get; }
        private ICrifPanVerificationService verificationService { get; }


        public CreditInformationHighMarkLiveTest()
        {
            crifPanVerificationConfiguration = new CrifPanVerificationConfiguration()
            {

                SubMemberId = "GREAT MEERA FINLEASE",
                UserId = "uat@greatmeera.in",
                Password = "8F4EFE649A06987F6D8697B30B72D55284AD6DF3",
                MemberId = "NBF0000119",
                ActionType = "SUBMIT",
                LosName = "LosName",
                Certificate = "MIIEZzCCA0+gAwIBAgIJAJ7Fg6eL2STKMA0GCSqGSIb3DQEBBQUAMIHJMQswCQYDVQQGEwJJTjEUMBIGA1UECAwLTWFoYXJhc2h0cmExKTAnBgNVBAcMIDQwMiwgU2hlaWwgRXN0YXRlLCAxNTgsIENTVCBSb2FkMRkwFwYDVQQKDBB0ZXN0LmhpZ2htYXJrLmluMRMwEQYDVQQLDAogSGlnaCBNYXJrMRkwFwYDVQQDDBB0ZXN0LmhpZ2htYXJrLmluMS4wLAYJKoZIhvcNAQkBFh92aW5heS5jaG91ZGhhcnlAY3JpZmhpZ2htYXJrLmluMB4XDTE1MTEwMjExMzIxM1oXDTE3MTEwMTExMzIxM1owgckxCzAJBgNVBAYTAklOMRQwEgYDVQQIDAtNYWhhcmFzaHRyYTEpMCcGA1UEBwwgNDAyLCBTaGVpbCBFc3RhdGUsIDE1OCwgQ1NUIFJvYWQxGTAXBgNVBAoMEHRlc3QuaGlnaG1hcmsuaW4xEzARBgNVBAsMCiBIaWdoIE1hcmsxGTAXBgNVBAMMEHRlc3QuaGlnaG1hcmsuaW4xLjAsBgkqhkiG9w0BCQEWH3ZpbmF5LmNob3VkaGFyeUBjcmlmaGlnaG1hcmsuaW4wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDDcACUQjYbIuvE3yWOcX69cN720kzl294UflD/6xNy4DsLGkWQCTgCBR6q6qV/41jedNmksJsAN56FpIVOJvXvH+IeyiVNaFDB5AUhu6CTpmKAMuTwuMYH5cjieKmNf+VZSlD27GiApvrTbIRwKrNf+NaX9zqG5pEoAPy2CxJUUBrpiPsILcLv7VYzazEPVfKDXiyDbaTymp01yfQHfiaQPKC5nMeduX2Qf3qECT6wNHlwBihnKjIDDNOJ7n5Q3bwUIJA39qs8IJyOLbw3Xz9N4eYH0g2hgss42DBN+s5bhcndEiA8UanWQR2KN2rgyu8cIQxfljgyr2D8xj5y7ud1AgMBAAGjUDBOMB0GA1UdDgQWBBQfZnZWajW80PDuU3yiE8IbDe7+uDAfBgNVHSMEGDAWgBQfZnZWajW80PDuU3yiE8IbDe7+uDAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBAQAY7djP9kk8t0gfok2nE0tgInyEIhlhJzrrV9L37a0rCs/coXPLIJf3/AT97vyjic/lXxMdol58/EBQR7fcQIvVubfxKwnxaVF+evlvwUGTu/iU2y37FbKDQTaMTmnR9Pv/o3syvV6CJeyXHZxN5TxLyhsd+sKcrhdakbDWoSlZHa7Tjs8Om8aPiWGz3DgxmyrGXiGfT4I+YUfX2uisc/nm65sZ70ClCby7YouJoOuRWWKff2W6x60JoVHAbXcskba8oiHnBkufQ8piHejvEnMFwhF+dPGifyITmOEK3FVcto3CVBHavmsRIqjqJqOR3UsMfth7LygxAKJRf8OgctCE",
                InquiryDateTime = DateTime.Now,
                TestFlag= "HMTEST",
                LosVendor = "LosVendor",
                MemberPreOverride= "N",
                AuthTitle= "USER",
                AuthFlag ="Y",
                LosVersion = "LosVersion",
               
            };
            EventHubClient = new Mock<IEventHubClient>();
            Lookup = new Mock<ILookupService>();
            CrifverificationProxy = new CrifVerficationProxy(crifPanVerificationConfiguration);
            verificationService = new CrifPanVerificationService(crifPanVerificationConfiguration, CrifverificationProxy, EventHubClient.Object, Lookup.Object);

        }

        [Fact]
        public void PanVerificationLive()
        {
            var panVerificationRequest = new PanVerificationRequest() { Name = "abc", Gender = Gender.Female, Pan = "12346ffl", Dob = DateTime.Now };           
            
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("ABC", "1");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
           
            var result =  verificationService.VerifyPan("123", "123", panVerificationRequest);
            Assert.NotNull(result.Result);
        }


        

    }
}
