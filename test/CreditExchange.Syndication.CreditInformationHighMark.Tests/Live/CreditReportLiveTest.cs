﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using CreditExchange.Syndication.Crif;
using CreditExchange.Syndication.Crif.CreditReport;
using CreditExchange.Syndication.Crif.CreditReport.Proxy;
using CreditExchange.Syndication.Crif.CreditReport.Request;
using CreditExchange.Syndication.Crif.CreditReport.SendInquiry;
using CreditExchange.Syndication.Crif.PanVerification;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.Syndication.CreditInformationHighMark.Tests.Live
{
    public class CreditReportLiveTest
    {
       private ICrifCreditReportProxy CrifverificationProxy { get; }
        private ICrifCreditReportConfiguration crifCreditReportConfiguration { get; }

        private ICrifCreditReportService creditReportService { get; }
        private Mock<IEventHubClient> EventHubClient { get; }
        private Mock<ILookupService> Lookup { get; }
       


        public CreditReportLiveTest()
        {
            crifCreditReportConfiguration = new CrifCreditReportConfiguration()
            {
                SubMemberId = "GREAT MEERA FINLEASE",
                UserId = "uat@greatmeera.in",
                Password = "8F4EFE649A06987F6D8697B30B72D55284AD6DF3",
                MemberId = "NBF0000119",
                ActionType = "SUBMIT",
                LosName = "LosName",
                Certificate =  "MIIEZzCCA0+gAwIBAgIJAJ7Fg6eL2STKMA0GCSqGSIb3DQEBBQUAMIHJMQswCQYDVQQGEwJJTjEUMBIGA1UECAwLTWFoYXJhc2h0cmExKTAnBgNVBAcMIDQwMiwgU2hlaWwgRXN0YXRlLCAxNTgsIENTVCBSb2FkMRkwFwYDVQQKDBB0ZXN0LmhpZ2htYXJrLmluMRMwEQYDVQQLDAogSGlnaCBNYXJrMRkwFwYDVQQDDBB0ZXN0LmhpZ2htYXJrLmluMS4wLAYJKoZIhvcNAQkBFh92aW5heS5jaG91ZGhhcnlAY3JpZmhpZ2htYXJrLmluMB4XDTE1MTEwMjExMzIxM1oXDTE3MTEwMTExMzIxM1owgckxCzAJBgNVBAYTAklOMRQwEgYDVQQIDAtNYWhhcmFzaHRyYTEpMCcGA1UEBwwgNDAyLCBTaGVpbCBFc3RhdGUsIDE1OCwgQ1NUIFJvYWQxGTAXBgNVBAoMEHRlc3QuaGlnaG1hcmsuaW4xEzARBgNVBAsMCiBIaWdoIE1hcmsxGTAXBgNVBAMMEHRlc3QuaGlnaG1hcmsuaW4xLjAsBgkqhkiG9w0BCQEWH3ZpbmF5LmNob3VkaGFyeUBjcmlmaGlnaG1hcmsuaW4wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDDcACUQjYbIuvE3yWOcX69cN720kzl294UflD/6xNy4DsLGkWQCTgCBR6q6qV/41jedNmksJsAN56FpIVOJvXvH+IeyiVNaFDB5AUhu6CTpmKAMuTwuMYH5cjieKmNf+VZSlD27GiApvrTbIRwKrNf+NaX9zqG5pEoAPy2CxJUUBrpiPsILcLv7VYzazEPVfKDXiyDbaTymp01yfQHfiaQPKC5nMeduX2Qf3qECT6wNHlwBihnKjIDDNOJ7n5Q3bwUIJA39qs8IJyOLbw3Xz9N4eYH0g2hgss42DBN+s5bhcndEiA8UanWQR2KN2rgyu8cIQxfljgyr2D8xj5y7ud1AgMBAAGjUDBOMB0GA1UdDgQWBBQfZnZWajW80PDuU3yiE8IbDe7+uDAfBgNVHSMEGDAWgBQfZnZWajW80PDuU3yiE8IbDe7+uDAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBAQAY7djP9kk8t0gfok2nE0tgInyEIhlhJzrrV9L37a0rCs/coXPLIJf3/AT97vyjic/lXxMdol58/EBQR7fcQIvVubfxKwnxaVF+evlvwUGTu/iU2y37FbKDQTaMTmnR9Pv/o3syvV6CJeyXHZxN5TxLyhsd+sKcrhdakbDWoSlZHa7Tjs8Om8aPiWGz3DgxmyrGXiGfT4I+YUfX2uisc/nm65sZ70ClCby7YouJoOuRWWKff2W6x60JoVHAbXcskba8oiHnBkufQ8piHejvEnMFwhF+dPGifyITmOEK3FVcto3CVBHavmsRIqjqJqOR3UsMfth7LygxAKJRf8OgctCE",
                InquiryDateTime = DateTime.Now,
                TestFlag = "HMTEST",
                LosVendor = "LosVendor",
                MemberPreOverride = "N",
                AuthTitle = "USER",
                AuthFlag = "Y",
                LosVersion = "LosVersion",
                ProductType = "INDV",
                RequestVolumeType = "SINGLE",
                InquiryPurposeType = "ACCTORIG",
                CreditInquiryStage = "PRESCREEN",   
                ProductVersion = "1.0",
                ApiUrl = "https://test.highmark.in/Inquiry/doGet.service/requestResponse"
            };

             Mock<ITenantTime> tenantTime  = new Mock<ITenantTime>();
            Mock<ILogger> logger = new Mock<ILogger>();
            EventHubClient = new Mock<IEventHubClient>();
            Lookup = new Mock<ILookupService>();

           CrifverificationProxy = new CrifCreditReportProxy(crifCreditReportConfiguration, tenantTime.Object, logger.Object);
           // creditReportService = new CrifCreditReportService(crifCreditReportConfiguration, CrifverificationProxy, EventHubClient.Object, Lookup.Object);

        }


        //[Fact]
        //public void CreditReport_SendInquiry_Live_Test()
        //{
        //    var reportInquiryRequest = new CreditReportInquiryRequest() { Name = "abc", Gender = Gender.Female, Pan = "12346ffl", Dob = DateTime.Now, DrivingLicNo = "134689", Passport = "123456", VoterId = "444", Addresses = new List<IAddressSegment>() };

        //    Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
        //    categoryLookup.Add("ABC", "1");
        //    Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);

        //    var result = creditReportService.SendCreditReportInquiry("123", "123", reportInquiryRequest);
        //    Assert.NotNull(result.Result);
        //}



        [Fact]
        public void CreditReport_GetReport_Live_Test()
        {
            //var reportInquiryRequest = new CreditReportInquiryRequest() { Name = "abc", Gender = Gender.Female, Pan = "12346ffl", Dob = DateTime.Now, DrivingLicNo = "134689", Passport = "123456", VoterId = "444", Addresses = new List<IAddressSegment>() };

            crifCreditReportConfiguration.ReportActionType = "ISSUE";
            crifCreditReportConfiguration.RequestVolumeType = "INDV";
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("ABC", "1");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);

            var result = creditReportService.GetReport("123", "123", "123", "123");
            Assert.NotNull(result.Result);
        }
    }
}
