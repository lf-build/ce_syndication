﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.Crif;
using CreditExchange.Syndication.Crif.Events;
using CreditExchange.Syndication.Crif.PanVerification;
using CreditExchange.Syndication.Crif.PanVerification.Proxy;
using CreditExchange.Syndication.Crif.PanVerification.Request;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.Syndication.CreditInformationHighMark.Tests
{
    public class CrifPanVerificationServiceTests
    {

         private Mock<ICrifPanVerificationConfiguration> Configuration { get; }
        private Mock<ICrifVerficationProxy> Proxy { get; }
        private Mock<IEventHubClient> EventHubClient { get; }
        private Mock<ILookupService> Lookup { get; }
        private ICrifPanVerificationService verificationService { get; }

        public CrifPanVerificationServiceTests()
        {
            EventHubClient = new Mock<IEventHubClient>();
            ICrifPanVerificationConfiguration panVerificationConfiguration = new CrifPanVerificationConfiguration() { ActionType = "123", ApiUrl = "123", ServiceType = "123", RequestVolumeType = "123", Password = "123", UserId = "123", SubMemberId = "1", MemberId = "123", ProductType = "123", ProductVersion = "123" };

           // Configuration = new Mock<ICrifPanVerificationConfiguration>();
            Proxy = new Mock<ICrifVerficationProxy>();
            Lookup = new Mock<ILookupService>();
            verificationService = new CrifPanVerificationService(panVerificationConfiguration, Proxy.Object, EventHubClient.Object, Lookup.Object);
        }

        [Fact]
        public async void VerifyPan_With_Valid_Argument()
        {
            var panVerificationRequest = new PanVerificationRequest() { Name = "abc", Gender = Gender.Female, Pan = "12346ffl", Dob = DateTime.Now };
            //Mock<LendFoundry.Syndication.Crif.PanVerification.Proxy.Request> proxy = new Mock<Request>(It.IsAny<ICrifPanVerificationConfiguration>(), It.IsAny<IPanVerificationRequest>());
            ICrifPanVerificationConfiguration panVerificationConfiguration = new CrifPanVerificationConfiguration() { ActionType = "123", ApiUrl = "123", ServiceType = "123", RequestVolumeType = "123", Password = "123", UserId = "123", SubMemberId = "1", MemberId = "123", ProductType = "123", ProductVersion = "123" };
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("ABC", "1");

            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Proxy.Setup(x => x.PanVerification(It.IsAny<Request>())).Returns(new VERIFICATIONREPORT());

            EventHubClient.Setup(x => x.Publish("PanVerification", new PanVerificationRequested()
            {
                EntityType = "Syndication-Crif",
                EntityId = "12305",               
                Request = panVerificationRequest,
                ReferenceNumber = "123"
            })).ReturnsAsync(true);

            var result = await verificationService.VerifyPan("123", "123", panVerificationRequest);
        }
        

        [Fact]
        public  void VerifyPan_With_InValid_Argument()
        {
            var panVerificationRequest = new PanVerificationRequest() { Name = "abc", Gender = Gender.Female, Pan = "", Dob = DateTime.Now };

            Assert.ThrowsAsync<ArgumentException>(() => verificationService.VerifyPan(null, "123", panVerificationRequest));

            Assert.ThrowsAsync<ArgumentException>(() => verificationService.VerifyPan("application", null, panVerificationRequest));

            Assert.ThrowsAsync<InvalidArgumentException>(() => verificationService.VerifyPan(" ", "123", panVerificationRequest));

            Assert.ThrowsAsync<InvalidArgumentException>(() => verificationService.VerifyPan("application", "123", panVerificationRequest));

            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("ABC", "1");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => verificationService.VerifyPan("application", "123", null));

            
            Assert.ThrowsAsync<ArgumentException>(() => verificationService.VerifyPan("application", "123", panVerificationRequest));

            panVerificationRequest.Pan = "12345fff";
            Assert.ThrowsAsync<ArgumentException>(() => verificationService.VerifyPan("application", " ", panVerificationRequest));

        }

      


        }
}
