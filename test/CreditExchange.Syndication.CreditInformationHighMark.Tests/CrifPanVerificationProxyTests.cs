﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;
using CreditExchange.Syndication.Crif;
using CreditExchange.Syndication.Crif.PanVerification;
using CreditExchange.Syndication.Crif.PanVerification.Proxy;
using CreditExchange.Syndication.Crif.PanVerification.Request;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.Syndication.CreditInformationHighMark.Tests
{
    public class CrifPanVerificationProxyTests
    {

        private Mock<ICrifPanVerificationConfiguration> Configuration { get; }
        private Mock<ICrifVerficationProxy> Proxy { get; }
        private Mock<IEventHubClient> EventHubClient { get; }
        private Mock<ILookupService> Lookup { get; }
        private ICrifPanVerificationService verificationService { get; }
        private ICrifPanVerificationConfiguration  panVerificationConfiguration;

        public CrifPanVerificationProxyTests()
        {
            EventHubClient = new Mock<IEventHubClient>();
             panVerificationConfiguration = new CrifPanVerificationConfiguration();

            // Configuration = new Mock<ICrifPanVerificationConfiguration>();
            Proxy = new Mock<ICrifVerficationProxy>();
            Lookup = new Mock<ILookupService>();
            verificationService = new CrifPanVerificationService(panVerificationConfiguration, Proxy.Object, EventHubClient.Object, Lookup.Object);
        }

        [Fact]
        public void VerifyPan_With_InValid_Argument_For_CrifPanVerificationConfiguration()
        {

            var panVerificationRequest = new PanVerificationRequest() { Name = "abc", Gender = Gender.Female, Pan = "12346ffl", Dob = DateTime.Now };
            //Mock<LendFoundry.Syndication.Crif.PanVerification.Proxy.Request> proxy = new Mock<Request>(It.IsAny<ICrifPanVerificationConfiguration>(), It.IsAny<IPanVerificationRequest>());
            //ICrifPanVerificationConfiguration panVerificationConfiguration = new CrifPanVerificationConfiguration() { ActionType = "123", ApiUrl = "123", ServiceType = "123", RequestVolumeType = "123", Password = "123", UserId = "123", SubMemberId = "1", MemberId = "123", ProductType = "123", ProductVersion = "123" };
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("ABC", "1");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);

            Assert.ThrowsAsync<ArgumentException>(() => verificationService.VerifyPan("application", "123", panVerificationRequest));

            panVerificationConfiguration.ProductType = "123";
            Assert.ThrowsAsync<ArgumentException>(() => verificationService.VerifyPan("application", "123", panVerificationRequest));

            panVerificationConfiguration.ProductVersion = "123";
            Assert.ThrowsAsync<ArgumentException>(() => verificationService.VerifyPan("application", "123", panVerificationRequest));

            panVerificationConfiguration.MemberId = "123";
            Assert.ThrowsAsync<ArgumentException>(() => verificationService.VerifyPan("application", "123", panVerificationRequest));

            panVerificationConfiguration.SubMemberId = "123";
            Assert.ThrowsAsync<ArgumentException>(() => verificationService.VerifyPan("application", "123", panVerificationRequest));

            panVerificationConfiguration.UserId = "123";
            Assert.ThrowsAsync<ArgumentException>(() => verificationService.VerifyPan("application", "123", panVerificationRequest));

            panVerificationConfiguration.Password = "123";
            Assert.ThrowsAsync<ArgumentException>(() => verificationService.VerifyPan("application", "123", panVerificationRequest));

            panVerificationConfiguration.RequestVolumeType = "123";
            Assert.ThrowsAsync<ArgumentException>(() => verificationService.VerifyPan("application", "123", panVerificationRequest));

            panVerificationConfiguration.ApiUrl = "123";
            Assert.ThrowsAsync<ArgumentException>(() => verificationService.VerifyPan("application", "123", panVerificationRequest));

            panVerificationConfiguration.SubMemberId = "123";
            Assert.ThrowsAsync<ArgumentException>(() => verificationService.VerifyPan("application", "123", panVerificationRequest));

            panVerificationConfiguration.ServiceType = "123";
            Assert.ThrowsAsync<ArgumentException>(() => verificationService.VerifyPan("application", "123", panVerificationRequest));

            panVerificationConfiguration.ActionType = "123";
            Assert.ThrowsAsync<ArgumentException>(() => verificationService.VerifyPan("application", "123", panVerificationRequest));



        }

    }
}
