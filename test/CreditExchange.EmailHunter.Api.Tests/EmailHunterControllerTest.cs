﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Services;
using CreditExchange.EmailHunter.Api.Controllers;
using CreditExchange.Syndication.EmailHunter.Client;
using CreditExchange.Syndication.EmailHunter.Request;
using CreditExchange.Syndication.EmailHunter.Response;
using Microsoft.AspNet.Mvc;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using CreditExchange.Syndication.EmailHunter;

namespace CreditExchange.EmailHunter.Api.Tests
{
    public class EmailHunterControllerTest
    {
       // private ApiController EmailHunterController { get; }

        private static ApiController GetControllerWithHunterService(IEmailHunterService emailHunterService = null)
        {
            return new ApiController(emailHunterService, Mock.Of<IEventHubClient>());
        }
        private static ApiController GetController()
        {
            return new ApiController(Mock.Of<IEmailHunterService>(), Mock.Of<IEventHubClient>());
        }

     
        [Fact]
        public async void EmailHunter_DomainSearch_WithNoArgument()
        {
            var mock = new Mock<IEmailHunterProxy>();
            IEmailHunterService _emailHunterService = new EmailHunterService(mock.Object);
            var request = GetControllerWithHunterService(_emailHunterService);
            var response = (ErrorResult)await request.DomainSearch(string.Empty,string.Empty,string.Empty);
            Assert.Equal(response.StatusCode, 400);
        }
        [Fact]
        public async void EmailHunter_DomainSearch_ReturnsSuccess()
        {

            var mock = new Mock<IEmailHunterProxy>();
            var eventhub = Mock.Of<IEventHubClient>();
            var service = new EmailHunterService(mock.Object);

            mock.Setup(s => s.DomainSearch(It.IsAny<IDomainSearchRequest>()))
               .Returns(() => Task.FromResult<IDomainSearchResponse>(new DomainSearchResponse
               {
                   Emails = new List<IEmailResponse>
                    {
                        new EmailResponse
                        {
                            Sources = new List<ISourceResponse>
                            {
                                new SourceResponse()
                            }
                        }
                    }
               }));

            var request = GetControllerWithHunterService(service);

            var response = (HttpOkObjectResult)await request.DomainSearch("1", "1", "stripe.com");


            Assert.Equal(response.StatusCode, 200);
        }
        [Fact]
        public async void EmailHunter_EmailFinder_WithNoArgument()
        {
            var mock = new Mock<IEmailHunterProxy>();
            IEmailHunterService _emailHunterService = new EmailHunterService(mock.Object);
            var request = GetControllerWithHunterService(_emailHunterService);
            var response = (ErrorResult)await request.EmailFinder(new EmailFinder());
            Assert.Equal(response.StatusCode, 400);
        }
        [Fact]
        public async void EmailHunter_EmailFinder_ReturnsSuccess()
        {
            var mock = new Mock<IEmailHunterProxy>();
            var eventhub = Mock.Of<IEventHubClient>();
            var service = new EmailHunterService(mock.Object);

            mock.Setup(s => s.EmailFinder(It.IsAny<IEmailFinderRequest>()))
               .Returns(() => Task.FromResult<IEmailFinderResponse>(new EmailFinderResponse
               {
                   Sources = new List<ISourceResponse>
                    {
                        new SourceResponse()
                    }
               }));

            var request = GetControllerWithHunterService(service);

            var response = (HttpOkObjectResult)await request.EmailFinder(new EmailFinder()
            {
                Company = "",
                Domain = "asana.com",
                FirstName = "Dustin",
                LastName = "Moskovitz"
            });
            Assert.Equal(response.StatusCode, 200);
        }
        [Fact]
        public async void EmailHunter_EmailVerification_WithNoArgument()
        {
            var mock = new Mock<IEmailHunterProxy>();
            IEmailHunterService _emailHunterService = new EmailHunterService(mock.Object);
            var request = GetControllerWithHunterService(_emailHunterService);
            var response = (ErrorResult)await request.EmailVerifier("1", "1", string.Empty);
            Assert.Equal(response.StatusCode, 400);
        }
        [Fact]
        public async void EmailHunter_EmailVerification_ReturnsSuccess()
        {
            var mock = new Mock<IEmailHunterProxy>();
            var eventhub = Mock.Of<IEventHubClient>();
            var service = new EmailHunterService(mock.Object);

            mock.Setup(s => s.EmailVerifier(It.IsAny<IEmailVerificationRequest>()))
                 .Returns(() => Task.FromResult<IEmailVerificationResponse>(new EmailVerificationResponse
                 {
                     Sources = new List<ISourceResponse>
                    {
                        new SourceResponse()
                    }
                 }));

            var request = GetControllerWithHunterService(service);
            var response = (HttpOkObjectResult)await request.EmailVerifier("1", "1", "steli@close.io");

            Assert.Equal(response.StatusCode, 200);
        }
        [Fact]
        public async void EmailHunter_EmailCount_WithNoArgument()
        {
            var mock = new Mock<IEmailHunterProxy>();
            IEmailHunterService _emailHunterService = new EmailHunterService(mock.Object);
            var request = GetControllerWithHunterService(_emailHunterService);
            var response = (ErrorResult)await request.EmailCount("1", "1", string.Empty);
            Assert.Equal(response.StatusCode, 400);
        }
        [Fact]
        public async void EmailHunter_EmailCount_ReturnsSuccess()
        {

            var mock = new Mock<IEmailHunterProxy>();
            var eventhub = Mock.Of<IEventHubClient>();
            var service = new EmailHunterService(mock.Object);

            mock.Setup(s => s.EmailCount(It.IsAny<IEmailCountRequest>()))
              .Returns(() => Task.FromResult<IEmailCountResponse>(new EmailCountResponse()));

            var request = GetControllerWithHunterService(service);

            var response = (HttpOkObjectResult)await request.EmailCount("1", "1", "stripe.com");
            Assert.Equal(response.StatusCode, 200);
        }
    }
}
