﻿using CreditExchange.Crif.CreditReport.Api.Controllers;
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.Crif;
using CreditExchange.Syndication.Crif.CreditReport;
using CreditExchange.Syndication.Crif.CreditReport.Request;
using CreditExchange.Syndication.Crif.CreditReport.Response;
using Microsoft.AspNet.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.CreditInformationHighMark.Api.Tests
{
    public class CreditReportControllerTest
    {
        private Mock<ICrifCreditReportService> CrifVerificationService { get; }
        private ApiController apiController { get; }


        public CreditReportControllerTest()
        {
            CrifVerificationService = new Mock<ICrifCreditReportService>();
            apiController = new ApiController(CrifVerificationService.Object);
        }


        [Fact]
        public async void SendCreditReportInquiry_Returns_Ok()
        {
            var panVerificationRequest = new CreditReportInquiryRequest() { Name = "abc", Gender = Gender.Female, Pan = "12346ffl", Dob = DateTime.Now };

            CrifVerificationService.Setup(x => x.SendInquiry(It.IsAny<string>(), It.IsAny<string>(), panVerificationRequest)).ReturnsAsync(new CreditReportInquiryResponse());
            var result = (HttpOkObjectResult)await apiController.SendInquiry("123", "123", panVerificationRequest);

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 200);

        }

        [Fact]
        public async void SendCreditReportInquiry_Returns_Error_CrifException()
        {
            var panVerificationRequest = new CreditReportInquiryRequest() { Name = "abc", Gender = Gender.Female, Pan = "12346ffl", Dob = DateTime.Now };

            CrifVerificationService.Setup(x => x.SendInquiry(It.IsAny<string>(), It.IsAny<string>(), panVerificationRequest)).Throws(new CrifException());
            var result = (ErrorResult)await apiController.SendInquiry("123", "123", panVerificationRequest);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }


        //[Fact]
        //public async void IssueReport_Returns_Ok()
        //{
          
        //    CrifVerificationService.Setup(x => x.GetReport(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(new GetCreditReportResponse());
        //    var result = (HttpOkObjectResult)await apiController.IssueReport("123", "123", "123", "123");

        //    Assert.NotNull(result);
        //    Assert.Equal(result.StatusCode, 200);

        //}

        //[Fact]
        //public async void IssueReport_Returns_Error_CrifException()
        //{
        //    CrifVerificationService.Setup(x => x.GetReport(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws(new CrifException());
        //    var result = (ErrorResult)await apiController.IssueReport("123", "123", "123", "123");
        //    Assert.NotNull(result);
        //    Assert.Equal(result.StatusCode, 400);
        //}
    }
}
