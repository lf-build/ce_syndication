﻿using CreditExchange.Crif.PanVerification.Api.Controllers;
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.Crif;
using CreditExchange.Syndication.Crif.PanVerification;
using CreditExchange.Syndication.Crif.PanVerification.Proxy;
using CreditExchange.Syndication.Crif.PanVerification.Request;
using CreditExchange.Syndication.Crif.PanVerification.Response;
using Microsoft.AspNet.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.CreditInformationHighMark.Api.Tests
{
    public class CrifPanVerificationControllerTest
    {
        private Mock<ICrifPanVerificationService> CrifVerificationService { get; }
        private ApiController apiController { get; }


        public CrifPanVerificationControllerTest()
        {
            CrifVerificationService = new Mock<ICrifPanVerificationService>();
            apiController = new ApiController(CrifVerificationService.Object);
        }


        [Fact]
        public async void VerifyPan_Returns_Ok()
        {
            var panVerificationRequest = new PanVerificationRequest() { Name = "abc", Gender = Gender.Female, Pan = "12346ffl", Dob = DateTime.Now };

            CrifVerificationService.Setup(x => x.VerifyPan(It.IsAny<string>(), It.IsAny<string>(), panVerificationRequest)).ReturnsAsync(new GetPanVerificationResponse());
            var result = (HttpOkObjectResult)await apiController.VerifyPan("123", "123", panVerificationRequest);

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 200);
           
        }

        [Fact]
        public async void VerifyPan_Returns_Error_CrifException()
        {
            var panVerificationRequest = new PanVerificationRequest() { Name = "abc", Gender = Gender.Female, Pan = "12346ffl", Dob = DateTime.Now };

            CrifVerificationService.Setup(x => x.VerifyPan(It.IsAny<string>(), It.IsAny<string>(), panVerificationRequest)).Throws(new CrifException());
            var result = (ErrorResult)await apiController.VerifyPan("123", "123", panVerificationRequest);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }

    }
}
