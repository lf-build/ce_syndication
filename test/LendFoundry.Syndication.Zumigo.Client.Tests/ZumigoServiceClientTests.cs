﻿using CreditExchange.Syndication.Zumigo;
using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Syndication.Zumigo.Client.Tests
{
    public class ZumigoServiceClientTests
    {
        //public ZumigoServiceClientTests()
        //{
        //    MockServiceClient = new Mock<IServiceClient>();
        //    ZumigoServiceClient = new ZumigoService(MockServiceClient.Object);
        //}

        //private ZumigoService ZumigoServiceClient { get; }
        //private IRestRequest Request { get; set; }
        //private Mock<IServiceClient> MockServiceClient { get; }

        //[Fact]
        //public void Client_Add_Device()
        //{
        //    var request = new AddDeviceRequest()
        //    {
        //        MobileDeviceNumber = "919825820589",
        //        OptInTimeStamp = "",
        //        OptlnId = "test123"
        //    };

        //    MockServiceClient.Setup(s => s.Execute<AddDeviceResponse>(It.IsAny<IRestRequest>()))
        //        .Callback<IRestRequest>(r => Request = r);
        //    ZumigoServiceClient.AddDevice(request);
        //    Assert.Equal("/", Request.Resource);
        //    Assert.Equal(Method.POST, Request.Method);
        //}

        //[Fact]
        //public void Client_Delete_Device()
        //{
        //    MockServiceClient.Setup(s => s.Execute<DeleteDeviceResponse>(It.IsAny<IRestRequest>()))
        //        .Callback<IRestRequest>(r => Request = r);

        //    ZumigoServiceClient.DeleteDevice("919825820589");
        //    Assert.Equal("/", Request.Resource);
        //    Assert.Equal(Method.DELETE, Request.Method);
        //    //Assert.NotNull(result);
        //}

        //[Fact]
        //public void Client_Get_Device_Status()
        //{
        //    MockServiceClient.Setup(s => s.Execute<GetDeviceStatusResponse>(It.IsAny<IRestRequest>()))
        //        .Callback<IRestRequest>(r => Request = r);
          
        //    var result = ZumigoServiceClient.GetDeviceStatus("919825820589");
        //    Assert.Equal("/", Request.Resource);
        //    Assert.Equal(Method.GET, Request.Method);
        //    Assert.NotNull(result);
        //}

        //[Fact]
        //public void Client_Get_MobileLocation_And_Address()
        //{
        //    MockServiceClient.Setup(s => s.Execute<GetMobileLocationAndAddressResponse>(It.IsAny<IRestRequest>()))
        //        .Callback<IRestRequest>(r => Request = r);

        //    var result = ZumigoServiceClient.GetMobileLocationAndAddress("919825820589");
        //    Assert.Equal("/address", Request.Resource);
        //    Assert.Equal(Method.GET, Request.Method);
        //    Assert.NotNull(result);
        //}

        //[Fact]
        //public void Client_Validate_Online_Transaction()
        //{
        //    var request = new ValidateOnlineTransactionRequest()
        //    {
        //        MobileDeviceNumber = "919825820589"
        //    };

        //    MockServiceClient.Setup(s => s.Execute<ValidateOnlineTransactionResponse>(It.IsAny<IRestRequest>()))
        //        .Callback<IRestRequest>(r => Request = r);

        //    var result = ZumigoServiceClient.ValidateOnlineTransaction(request);
        //    Assert.Equal("/is-valid", Request.Resource);
        //    Assert.Equal(Method.POST, Request.Method);
        //    Assert.NotNull(result);
        //}
    }
}
