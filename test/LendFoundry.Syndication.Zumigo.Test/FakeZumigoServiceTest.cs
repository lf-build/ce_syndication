﻿using CreditExchange.Syndication.Zumigo;
using CreditExchange.Syndication.Zumigo.Proxy;
using CreditExchange.Syndication.Zumigo.Proxy.ProxyRequest;
using LendFoundry.Foundation.Services;
using System;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Zumigo.Test
{
    public class FakeZumigoServiceTest : IZumigoProxy
    {
        public FakeZumigoServiceTest()
        {

        }
        public string AddDevice(AddDeviceProxyRequest addDeviceRequest)
        {
            if (addDeviceRequest.MobileDeviceNumber == "919427500701")
            {
                return "MDN_OPTED_IN";
            }
            else if (addDeviceRequest.MobileDeviceNumber == "9845901584")
            {
                throw new NotSupportedException(nameof(addDeviceRequest));
            }
            else if (addDeviceRequest.MobileDeviceNumber == "919845901584")
            {
                throw new NotFoundException("Mobile device number is already added.");
            }
            else if (addDeviceRequest.MobileDeviceNumber == "919845901584")
            {
                throw new NotFoundException("The SMS has been sent, and user response is pending");
            }
            else if (addDeviceRequest.MobileDeviceNumber == "9845901584")
            {
                throw new NotSupportedException(nameof(addDeviceRequest));
            }
            else
            {
                throw new ZumigoException("Device is not added successfully.Please try again.");
            }

            //else if (addDeviceRequest.MobileDeviceNumber == "919845901584")
            //{
            //    return new AddDeviceProxyResponse()
            //    {
            //        StatusCode = "ALREADY_ADDED",
            //        StatusCodeDescription = "Received when MDN is already added to the account and the user opted into the request."
            //    };
            //}
            //else if (addDeviceRequest.MobileDeviceNumber == "919845901584")
            //{
            //    return new AddDeviceProxyResponse()
            //    {
            //        StatusCode = Convert.ToString(AddDeviceSuccessCodes.MDN_OPT_IN_REQ_PENDING),
            //        StatusCodeDescription = Extension.GetDescription(AddDeviceSuccessCodes.MDN_OPT_IN_REQ_PENDING)
            //    };
            //}                
        }

        public string DeleteDevice(string mobileDeviceNumber)
        {
            if (mobileDeviceNumber == "919845901584")
            {
                return "DELETED";
            }
            else if (mobileDeviceNumber == "919845901584")
            {
                throw new ZumigoException("The number was not identified to a carrier.");
            }               
            else if (mobileDeviceNumber == "919427500701")
            {
                throw new NotFoundException("Mobile device number is Not Found");
            }
            else if (mobileDeviceNumber == "9427")
            {
                throw new NotFoundException("Mobile device number is Not valid");
            }
            else
            {
                throw new ZumigoException("Device is not removed successfully.Please try again.");
            }            
        }

        public string GetDeviceStatus(string mobileDeviceNumber)
        {
            switch(mobileDeviceNumber)
            {
                case "919825522544":
                    return "MDN_OPTED_IN";
                case "919662701275":
                    throw new ZumigoException("Consumer Opted out during the consent process.");
                case "919662701375":
                    return "Received if the device is already Pending.";
                case "919662701385":
                    throw new ZumigoException("The number was not identified to a carrier.");
            }

            return null;
        }

        public static string GetMobileLocationAddressResponse()
        {
            string MobileDeviceNumber = "919825522544";
            string Latitude = "23.00";
            string Longitude = "72.00";
            string Address = @"424-427, Ratna Highstreet,
                            Nr.Naranpura Police Chowky,
                            Naranpura Cross Road,
                            Ahmedabad - 380013";
            string Accuracy = "1000";
            DateTimeOffset Timestamp = Convert.ToDateTime("2015-04-23T12:41:16.000-0700");

            return string.Concat(MobileDeviceNumber, Latitude, Longitude, Address, Accuracy, Timestamp.ToString());
        }

        public string GetMobileLocationAddress(string mobileDeviceNumber)
        {
            if (mobileDeviceNumber == "919825522544")
                return GetMobileLocationAddressResponse();

            return null;
        }

        public static string ValidateOnlineTransactionResponse()
        {
            string DistanceFromIP = "4753.222177650772";
            string DistanceFromLatitudeLongitude = "23.00";
            string DistanceFromWifi = string.Empty;
            string DistanceFromAddress = string.Empty;
            string AverageDistance = "2913.116261425729";

            return string.Concat(DistanceFromIP, DistanceFromLatitudeLongitude, DistanceFromWifi, DistanceFromAddress, AverageDistance);
        }


        public string ValidateOnlineTransaction(ValidateOnlineTransactionProxyRequest validateOnlineTransactionProxyRequest)
        {
            if (validateOnlineTransactionProxyRequest.MobileDeviceNumber == "919825522544")
                return ValidateOnlineTransactionResponse();

            return null;
        }

        Task<string> IZumigoProxy.AddDevice(AddDeviceProxyRequest deviceRequest)
        {
            throw new NotImplementedException();
        }

        Task<string> IZumigoProxy.DeleteDevice(string mobileDeviceNumber)
        {
            throw new NotImplementedException();
        }

        Task<string> IZumigoProxy.GetDeviceStatus(string mobileDeviceNumber)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetDeviceLocationAddress(string mobileDeviceNumber)
        {
            throw new NotImplementedException();
        }

        public Task<string> VerifyDeviceLocation(ValidateOnlineTransactionProxyRequest validateOnlineTransactionProxyRequest)
        {
            throw new NotImplementedException();
        }
    }
}
