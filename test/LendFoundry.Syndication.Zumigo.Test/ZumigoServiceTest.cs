﻿using CreditExchange.Syndication.Zumigo;
using System;
using Xunit;

namespace LendFoundry.Syndication.Zumigo.Test
{
    public class ZumigoServiceTest
    {
        //private ZumigoService zumigoService { get; } = new ZumigoService(new ZumigoConfiguration()
        //{
        //    UserName = "creditxchangeapi",
        //    Password = "cred17Exchang3Ap!",
        //    AddDeviceUrl = "https://api.zumigo.com/zumigo/addDevice.jsp",
        //    DeleteDeviceUrl = "https://api.zumigo.com/zumigo/deleteDevice.jsp",
        //    GetDeviceStatusUrl = "https://api.zumigo.com/zumigo/deviceStatus.jsp",
        //    LocationAndAddressUrl = "https://api.zumigo.com/zumigo/locationAndAddress.jsp",
        //    ValidateOnlineTransactionUrl = "https://api.zumigo.com/zumigo/validateOnlineTransaction.jsp",
        //    OptInType = OptInType.Whitelist,
        //    OptInMethod = OptInMethod.TCO
        //}, new FakeZumigoServiceTest());

        //[Fact]
        //public void VerifyThrowsArgumentExceptionWithConfigurationIsNull()
        //{
        //    Assert.Throws<ArgumentNullException>(() => new ZumigoService(null, new FakeZumigoServiceTest()));
        //    //Assert.Throws<ArgumentNullException>(() => objZumigoService.AddDevice(null));
        //}

        //[Fact]
        //public void VerifyThrowsArgumentExceptionWithRequestObjectIsNull()
        //{
        //    Assert.Throws<ArgumentNullException>(() => zumigoService.AddDevice(null));
        //}

        //[Fact]
        //public void VerifyThrowsArgumentExceptionWithMobileNumberEmpty()
        //{
        //    Assert.Throws<ArgumentException>(() => zumigoService.AddDevice(new AddDeviceRequest()
        //    {
        //        MobileDeviceNumber = ""
        //    }
        //    ));
        //}

        ////[Fact]
        ////public void VerifyThrowsArgumentExceptionWithOptInTypeIsEmpty()
        ////{
        ////    Assert.Throws<ArgumentException>(() => zumigoService.AddDevice(new AddDeviceRequest()
        ////    {
        ////        MobileDeviceNumber = "919845901584",
        ////        //OptInType = "",
        ////        //OptInMethod = "TCO"
        ////    }
        ////    ));
        ////}

        //#region Add Device Test Cases
        //[Fact]
        //public void VerifyAddDeviceSuccess()
        //{
        //    try
        //    {
        //        zumigoService.AddDevice(new AddDeviceRequest()
        //        {
        //            MobileDeviceNumber = "919427500701",
        //            //OptInType = "Whitelist",
        //            // OptInMethod = "TCO",
        //            OptlnId = "12345xyz",
        //            OptInTimeStamp = System.DateTime.UtcNow.ToString("yyyyMMddHHmmssffff")
        //        });

        //        Assert.True(true);
        //    }
        //    catch
        //    {
        //        Assert.True(false);
        //    }
        //}

        //[Fact]
        //public void VerifyAddDeviceAlreadyAdded()
        //{
        //    try {

        //        zumigoService.AddDevice(new AddDeviceRequest()
        //        {
        //            MobileDeviceNumber = "919845901584",
        //            //OptInType = "Whitelist",
        //            //OptInMethod = "TCO",
        //            OptlnId = "12345xyz",
        //            OptInTimeStamp = System.DateTime.UtcNow.ToString("yyyyMMddHHmmssffff")
        //        });

        //        Assert.True(true);
        //    }
        //    catch
        //    {
        //        Assert.True(false);
        //    }
        //}

        //[Fact]
        //public void VerifyAddDeviceCarrierNotSupported()
        //{
        //    Assert.Throws<NotSupportedException>(() => zumigoService.AddDevice(new AddDeviceRequest()
        //    {
        //        MobileDeviceNumber = "9845901584",
        //        //OptInType = "Whitelist",
        //        //OptInMethod = "TCO",
        //        OptlnId = "12345xyz",
        //        OptInTimeStamp = System.DateTime.UtcNow.ToString("yyyyMMddHHmmssffff")
        //    }
        //    ));
        //}

        //[Fact]
        //public void VerifyAddDeviceErrorCodes()
        //{
        //    Assert.Throws<ZumigoException>(() => zumigoService.AddDevice(new AddDeviceRequest()
        //    {
        //        MobileDeviceNumber = "9898901584",
        //        //OptInType = "Whitelist",
        //        //OptInMethod = "TCO",
        //        OptlnId = "12345xyz",
        //        OptInTimeStamp = System.DateTime.UtcNow.ToString("yyyyMMddHHmmssffff")
        //    }
        //    ));
        //}

        //[Fact]
        //public void VerifyAddDeviceWithOtherOptInType()
        //{

        //    Assert.Throws<ZumigoException>(() => zumigoService.AddDevice(new AddDeviceRequest()
        //    {
        //        MobileDeviceNumber = "919845901684",
        //        //OptInType = "InitiateOptIn",
        //        OptlnId = "12345xyz",
        //        OptInTimeStamp = System.DateTime.UtcNow.ToString("yyyyMMddHHmmssffff")
        //    }
        //   ));
        //}
        //#endregion

        //#region Delete Device Test Cases

        //[Fact]
        //public void VerifyDeleteDeviceSuccess()
        //{
        //    try
        //    {
        //        zumigoService.DeleteDevice("919845901584");           
        //    }
        //    catch
        //    {
        //        Assert.True(false);
        //    }
        //}

        //[Fact]
        //public void VerifyDeleteDeviceNotExistsNumber()
        //{
        //    try
        //    {
        //        zumigoService.DeleteDevice("919427500701");

        //        Assert.Equal(Convert.ToString(DeviceSetUpErrorCodes.NUMBER_NOT_FOUND), "NUMBER_NOT_FOUND");
        //    }
        //    catch
        //    {
        //        Assert.True(false);
        //    }
        //}

        //[Fact]
        //public void VerifyDeleteDeviceNotValidNumber()
        //{
        //    try
        //    {
        //        zumigoService.DeleteDevice("9427");

        //        Assert.Equal(Convert.ToString(DeviceSetUpErrorCodes.MDN_NOTVALID), "NOTVALID");
        //    }
        //    catch
        //    {
        //        Assert.True(false);
        //    }           
        //}

        //#endregion

        //[Fact]
        //public void VerifyThrowsArgumentExceptionWithRequestObjectIsNullForGetDeviceStatus()
        //{
        //    Assert.Throws<ArgumentNullException>(() => zumigoService.GetDeviceStatus(null));
        //}

        //[Fact]
        //public void VerifyGetDeviceStatusSuccess()
        //{
        //    try
        //    {
        //        var response = zumigoService.GetDeviceStatus("919825522544");

        //        var result = Enum.IsDefined(typeof(DeviceSuccessCodes), response);

        //        Assert.True(result);
        //    }
        //    catch
        //    {
        //        Assert.True(false);
        //    }
        //}

        //[Fact]
        //public void VerifyGetMobileLocationAddressSuccess()
        //{
        //    var getMobileLocationAddressProxyResponse = zumigoService.GetMobileLocationAndAddress("919825522544");

        //    Assert.Equal("23.00", getMobileLocationAddressProxyResponse.Latitude);
        //}

        //[Fact]
        //public void ValidateOnlineTransactionSuccess()
        //{
        //    var validateOnlineTransactionProxyResponse = zumigoService.ValidateOnlineTransaction(new ValidateOnlineTransactionRequest()
        //    {
        //        MobileDeviceNumber = "919825522544"
        //    }
        //    );
        //    Assert.Equal("5678.2550772", validateOnlineTransactionProxyResponse.DistanceFromIP);
        //}
    }
}
