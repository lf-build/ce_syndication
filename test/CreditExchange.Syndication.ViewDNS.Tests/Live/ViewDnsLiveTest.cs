﻿using CreditExchange.Syndication.ViewDNS.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.Syndication.ViewDNS.Tests.Live
{
    public class ViewDnsLiveTest
    {
        /// <summary>
        /// Gets the view DNS proxy.
        /// </summary>
        /// <value>
        /// The view DNS proxy.
        /// </value>
        private IViewDnsProxy ViewDnsProxy { get; }

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        private IViewDnsConfiguration Configuration { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewDnsLiveTest" /> class.
        /// </summary>
        public ViewDnsLiveTest()
        {
            Configuration = new ViewDnsConfiguration()
            {
                ApiBaseUrl = "http://pro.viewdns.info",
                ApiKey = "7eb568767fd1afa11296d82cd7becedf5593c4da",
                ApiOutputType = "json",
                ReverseWhoisUrl = "reversewhois"
            };
            ViewDnsProxy = new ViewDnsProxy(Configuration);
        }

        /// <summary>
        /// Searches the domain by company name test success.
        /// </summary>
        [Fact]
        public void SearchDomainByCompanyNameTestSuccess()
        {
            var response = ViewDnsProxy.GetDomainDetails("B A CREATIONS");
            Assert.NotNull(response);
        }

        /// <summary>
        /// Searches the domain by director name test success.
        /// </summary>
        [Fact]
        public void SearchDomainByDirectorNameTestSuccess()
        {
            var response = ViewDnsProxy.GetDomainDetails("MANIK GUPTA");
            Assert.NotNull(response);
        }
    }
}
