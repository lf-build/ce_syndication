﻿using CreditExchange.Syndication.ViewDNS.Proxy;
using System;
using System.Threading.Tasks;
using CreditExchange.Syndication.ViewDNS.Proxy.ReverseWhois;

namespace CreditExchange.Syndication.ViewDNS.Tests
{
    /// <summary>
    /// Fake View Dns Proxy class
    /// </summary>
    /// <seealso cref="IViewDnsProxy" />
    public class FakeViewDnsProxy : IViewDnsProxy
    {
        /// <summary>
        /// Gets the domain detail response.
        /// </summary>
        /// <returns></returns>
        public static QueryResponse GetDomainDetailResponse1()
        {
            var queryResponse = new QueryResponse
            {
                Query = new Query
                {
                    NameServer = "B A CREATIONS",
                    Tool = "reversewhois_PRO"
                },
                Response = new Proxy.ReverseWhois.Response
                {
                    Matches = new Match[]
                    {
                        new Match
                        {
                            CreatedDate = "2011-10-01",
                            Domain = "bacreations.com",
                            Registrar = "GODADDY.COM, LLC"
                        },
                        new Match
                        {
                            CreatedDate = "2010-10-19",
                            Domain = "bacreationsllc.com",
                            Registrar = "GODADDY.COM, LLC"
                        }
                    },
                    CurrentPage = "1",
                    ResultCount = 2,
                    TotalPages = "1"
                }
            };
            return queryResponse;
        }

        /// <summary>
        /// Gets the domain detail response2.
        /// </summary>
        /// <returns></returns>
        public static QueryResponse GetDomainDetailResponse2()
        {
            var queryResponse = new QueryResponse
            {
                Query = new Query
                {
                    NameServer = "MANIK GUPTA",
                    Tool = "reversewhois_PRO"
                },
                Response = new Proxy.ReverseWhois.Response
                {
                    Matches = new Match[]
                    {
                        new Match
                        {
                            CreatedDate = "2015-11-25",
                            Domain = "ambitiousleggings.com",
                            Registrar = "BIGROCK SOLUTIONS LIMITED"
                        },
                        new Match
                        {
                            CreatedDate = "2012-12-18",
                            Domain = "arccaresolutions.com",
                            Registrar = "PDR LTD. D/B/A PUBLICDOMAINREGISTRY.COM"
                        }
                    },
                    CurrentPage = "1",
                    ResultCount = 2,
                    TotalPages = "1"
                }
            };
            return queryResponse;
        }

        /// <summary>
        /// Gets the name of the domain details by company.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public Task<QueryResponse> GetDomainDetails(string name)
        {
            if (string.Equals("B  A CREATIONS", name, StringComparison.InvariantCultureIgnoreCase))
                return Task.FromResult(GetDomainDetailResponse1());
            if (string.Equals("MANIK GUPTA", name, StringComparison.InvariantCultureIgnoreCase))
                return Task.FromResult(GetDomainDetailResponse2());
            return null;
        }
    }
}
