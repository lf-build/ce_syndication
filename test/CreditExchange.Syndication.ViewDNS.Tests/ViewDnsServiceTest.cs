﻿using CreditExchange.CompanyDb.Abstractions;
using LendFoundry.EventHub.Client;
using CreditExchange.Syndication.ViewDNS.Request;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using CreditExchange.Syndication.EmailHunter;

namespace CreditExchange.Syndication.ViewDNS.Tests
{
    /// <summary>
    /// View dns service test class
    /// </summary>
    public class ViewDnsServiceTest
    {
        ///// <summary>
        ///// Gets the view DNS configuration.
        ///// </summary>
        ///// <value>
        ///// The view DNS configuration.
        ///// </value>
        //private static IViewDnsConfiguration ViewDnsConfiguration { get; } = new ViewDnsConfiguration
        //{
        //    ApiOutputType = "json",
        //    ApiBaseUrl = "http://pro.viewdns.info",
        //    ApiKey = "7eb568767fd1afa11296d82cd7becedf5593c4da",
        //    ReverseWhoisUrl = "reversewhois"
        //};

        /////// <summary>
        /////// Gets the view DNS service.
        /////// </summary>
        /////// <value>
        /////// The view DNS service.
        /////// </value>
        ////private ViewDnsService ViewDnsService { get; } = new ViewDnsService(new FakeViewDnsProxy(),Mock.Of<ICompanyDetail>(),Mock.Of<IEventHubClient>(),Mock.Of<ILookupService>());

        ///// <summary>
        ///// Gets the view DNS object fake.
        ///// </summary>
        ///// <param name="companyDetail">The company detail.</param>
        ///// <param name="entityValidator">The entity validator.</param>
        ///// <returns></returns>
        //private static IViewDnsService GetViewDnsObjectFake(Mock<ICompanyService> companyService, Mock<IEmailHunterService> emailHunterService, Mock<IEntityValidator> entityValidator)
        //{
        //    return new ViewDnsService(new FakeViewDnsProxy(), companyService.Object, emailHunterService.Object, new FilterDomain(), Mock.Of<IEventHubClient>(), entityValidator.Object);
        //}

        ///// <summary>
        ///// Gets the view DNS object fake.
        ///// </summary>
        ///// <returns></returns>
        //private static IViewDnsService GetViewDnsObjectFake()
        //{
        //    return new ViewDnsService(new FakeViewDnsProxy(), Mock.Of<ICompanyService>(), Mock.Of<IEmailHunterService>(), new DomainMatching(), new FilterDomain(), new CompanyNameGenerator(), Mock.Of<IEventHubClient>(), Mock.Of<IEntityValidator>());
        //}

        ///// <summary>
        ///// Verifies the type of the throws argument exception for entity.
        ///// </summary>
        //[Fact]
        //public void VerifyThrowsArgumentExceptionForEntityType()
        //{
        //    var ViewDnsService = GetViewDnsObjectFake();
        //    Assert.ThrowsAsync<ArgumentNullException>(() => ViewDnsService.VerifyEmployment("", "1", null, null, null));
        //}

        ///// <summary>
        ///// Verifies the throws argument exception for entity identifier.
        ///// </summary>
        //[Fact]
        //public void VerifyThrowsArgumentExceptionForEntityId()
        //{
        //    var ViewDnsService = GetViewDnsObjectFake();
        //    Assert.ThrowsAsync<ArgumentNullException>(() => ViewDnsService.VerifyEmployment("app", "", null, null, null));
        //}

        ///// <summary>
        ///// Verifies the throws argument exception for email address.
        ///// </summary>
        //[Fact]
        //public void VerifyThrowsArgumentExceptionForEmailAddress()
        //{
        //    var ViewDnsService = GetViewDnsObjectFake();
        //    Assert.ThrowsAsync<ArgumentNullException>(() => ViewDnsService.VerifyEmployment("app", "1", null, null, null));
        //}

        ///// <summary>
        ///// Verifies the name of the throws argument exception for company.
        ///// </summary>
        //[Fact]
        //public void VerifyThrowsArgumentExceptionForCompanyName()
        //{
        //    var ViewDnsService = GetViewDnsObjectFake();
        //    Assert.ThrowsAsync<ArgumentNullException>(() => ViewDnsService.VerifyEmployment("app", "1", "hiren.p@bacreations.com", null, null));
        //}

        ///// <summary>
        ///// Verifies the throws argument exception for cin.
        ///// </summary>
        //[Fact]
        //public void VerifyThrowsArgumentExceptionForCin()
        //{
        //    var ViewDnsService = GetViewDnsObjectFake();
        //    Assert.ThrowsAsync<ArgumentNullException>(() => ViewDnsService.VerifyEmployment("app", "1", "hiren.p@bacreations.com", "B & A CREATIONS PRIVATE LIMITED", null));
        //}

        ///// <summary>
        ///// Verifies the throws argument exception for invalid email address.
        ///// </summary>
        //[Fact]
        //public void VerifyThrowsArgumentExceptionForInvalidEmailAddress()
        //{
        //    var ViewDnsService = GetViewDnsObjectFake();
        //    Assert.ThrowsAsync<ArgumentNullException>(() => ViewDnsService.VerifyEmployment("app", "1", "InvalidEmailAddress", "B & A CREATIONS PRIVATE LIMITED", null));
        //}

        ///// <summary>
        ///// Successes the name of the verify employee by company.
        ///// </summary>
        //[Fact]
        //public void SuccessVerifyEmploymentByCompanyNameMatched()
        //{
        //    var mockValidator = new Mock<IEntityValidator>();
        //    mockValidator.Setup(v => v.EnsureEntityType(It.IsAny<string>())).Returns("app");

        //    var mockCompanyDetail = new Mock<ICompanyService>();
        //    mockCompanyDetail.Setup(c => c.GetCompanyWithDirectors(It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>())).Returns(Task.FromResult(GetCompanyDetails()));

        //    var mockEmailHunter = new Mock<IEmailHunterService>();
        //    mockEmailHunter.Setup(c => c.EmailVerifier(It.IsAny<string>())).ReturnsAsync(GetEmailHunterDetails());

        //    var ViewDnsService = GetViewDnsObjectFake(mockCompanyDetail, mockEmailHunter, mockValidator);

        //    var response = ViewDnsService.VerifyEmployment("app", "1", "hiren.p@bacreations.com", "B & A CREATIONS PRIVATE LIMITED", "U18109DL2008PTC177167");
        //    Assert.NotNull(response);
        //}

        ///// <summary>
        ///// Successes the name of the verify employee by director.
        ///// </summary>
        //[Fact]
        //public void SuccessVerifyEmploymentByDirectorNameMatched()
        //{
        //    var mockValidator = new Mock<IEntityValidator>();
        //    mockValidator.Setup(v => v.EnsureEntityType(It.IsAny<string>())).Returns("app");

        //    var mockCompanyDetail = new Mock<ICompanyService>();
        //    mockCompanyDetail.Setup(c => c.GetCompanyWithDirectors(It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>())).ReturnsAsync(GetCompanyDetails());

        //    var mockEmailHunter = new Mock<IEmailHunterService>();
        //    mockEmailHunter.Setup(c => c.EmailVerifier(It.IsAny<string>())).ReturnsAsync(GetEmailHunterDetails());

        //    var ViewDnsService = GetViewDnsObjectFake(mockCompanyDetail, mockEmailHunter, mockValidator);

        //    var response = ViewDnsService.VerifyEmployment("app", "1", "hiren.p@ambitiousleggings.com", "B & A CREATIONS PRIVATE LIMITED", "U18109DL2008PTC177167");
        //    Assert.NotNull(response);
        //}

        ///// <summary>
        ///// Successes the verify employee by company name not matched.
        ///// </summary>
        //[Fact]
        //public void SuccessVerifyEmploymentByCompanyNameNotMatched()
        //{
        //    var mockValidator = new Mock<IEntityValidator>();
        //    mockValidator.Setup(v => v.EnsureEntityType(It.IsAny<string>())).Returns("app");

        //    var mockCompanyDetail = new Mock<ICompanyService>();
        //    mockCompanyDetail.Setup(c => c.GetCompanyWithDirectors(It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>())).Returns(Task.FromResult(GetCompanyDetails()));

        //    var mockEmailHunter = new Mock<IEmailHunterService>();
        //    mockEmailHunter.Setup(c => c.EmailVerifier(It.IsAny<string>())).ReturnsAsync(GetEmailHunterDetails());

        //    var ViewDnsService = GetViewDnsObjectFake(mockCompanyDetail, mockEmailHunter, mockValidator);

        //    var response = ViewDnsService.VerifyEmployment("app", "1", "hiren.p@sigmainfosolutions.com", "B & A CREATIONS PRIVATE LIMITED", "U18109DL2008PTC177167");
        //    Assert.NotNull(response);
        //}

        //[Fact]
        //public void SuccessVerifyEmploymentByEmailHunterMatched()
        //{
        //    var mockValidator = new Mock<IEntityValidator>();
        //    mockValidator.Setup(v => v.EnsureEntityType(It.IsAny<string>())).Returns("app");

        //    var mockCompanyDetail = new Mock<ICompanyService>();
        //    mockCompanyDetail.Setup(c => c.GetCompanyWithDirectors(It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>())).Returns(Task.FromResult(GetCompanyDetails()));

        //    var mockEmailHunter = new Mock<IEmailHunterService>();
        //    mockEmailHunter.Setup(c => c.EmailVerifier(It.IsAny<string>())).ReturnsAsync(GetEmailHunterDetails());

        //    var ViewDnsService = GetViewDnsObjectFake(mockCompanyDetail, mockEmailHunter, mockValidator);

        //    var response = ViewDnsService.VerifyEmployment("app", "1", "hiren.p@ambitiousleggings.com", "B & A CREATIONS PRIVATE LIMITED", "U18109DL2008PTC177167");
        //    Assert.NotNull(response);
        //}

        ///// <summary>
        ///// Gets the company details.
        ///// </summary>
        ///// <returns></returns>
        //private CompanyWithDirectors GetCompanyDetails()
        //{
        //    return new CompanyWithDirectors
        //    {
        //        Company = new Company
        //        {
        //            Cin = "U18109DL2008PTC177167",
        //            Domain = "www.infosys.com",
        //            Name = "B & A CREATIONS PRIVATE LIMITED"
        //        },
        //        Directors = new List<CompanyDirector>
        //        {
        //            new CompanyDirector
        //            {
        //                Cin = "U18109DL2008PTC177167",
        //                Din = "01551588",
        //                Name = "MANIK GUPTA"
        //            }
        //        }
        //    };
        //}

        ///// <summary>
        ///// Gets the email hunter details.
        ///// </summary>
        ///// <returns></returns>
        //private EmailVerificationResult GetEmailHunterDetails()
        //{
        //    return new EmailVerificationResult
        //    {
        //        Email = "hiren.p@ambitiousleggings.com",
        //        Gibberish = false,
        //        MxRecords = true,
        //        Regexp = true,
        //        Disposable = false,
        //        AcceptAll = true,
        //        Result = "",
        //        Score = 100,
        //        SmtpCheck = true,
        //        SmtpServer = true,
        //        Sources = new List<SourceResult>(),
        //        Webmail = false
        //    };
        //}
    }
}