﻿using CreditExchange.Syndication.ViewDNS.Proxy;
using System;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.Syndication.ViewDNS.Tests
{
    /// <summary>
    /// View dns proxy test class
    /// </summary>
    public class ViewDnsProxyTest
    {
        /// <summary>
        /// Throws the argument exception with null configuration.
        /// </summary>
        [Fact]
        public void ThrowArgumentExceptionWithNullConfiguration()
        {
            Assert.Throws<ArgumentNullException>(() => new ViewDnsProxy(null));
        }
        
        /// <summary>
        /// Throws the argument exception with missing configuration.
        /// </summary>
        [Fact]
        public void ThrowArgumentExceptionWithMissingConfiguration_ApiKey()
        {
            var configuration = new ViewDnsConfiguration()
            {
                ApiOutputType = "json",
                ApiBaseUrl = "http://pro.viewdns.info",
                ApiKey = null
            };
            Assert.Throws<ArgumentNullException>(() => new ViewDnsProxy(configuration));
        }

        /// <summary>
        /// Throws the type of the argument exception with missing configuration API output.
        /// </summary>
        [Fact]
        public void ThrowArgumentExceptionWithMissingConfiguration_ApiOutputType()
        {
            var configuration = new ViewDnsConfiguration()
            {
                ApiOutputType = null,
                ApiBaseUrl = "http://pro.viewdns.info",
                ApiKey = "7eb568767fd1afa11296d82cd7becedf5593c4da",
                ReverseWhoisUrl = "reversewhois"
            };
            Assert.Throws<ArgumentNullException>(() => new ViewDnsProxy(configuration));
        }

        /// <summary>
        /// Throws the argument exception with missing configuration API base URL.
        /// </summary>
        [Fact]
        public void ThrowArgumentExceptionWithMissingConfiguration_ApiBaseUrl()
        {
            var configuration = new ViewDnsConfiguration()
            {
                ApiOutputType = "json",
                ApiBaseUrl = null,
                ApiKey = "7eb568767fd1afa11296d82cd7becedf5593c4da",
                ReverseWhoisUrl = "reversewhois"
            };
            Assert.Throws<ArgumentNullException>(() => new ViewDnsProxy(configuration));
        }

        /// <summary>
        /// Throws the argument exception with missing configuration reverse whois URL.
        /// </summary>
        [Fact]
        public void ThrowArgumentExceptionWithMissingConfiguration_ReverseWhoisUrl()
        {
            var configuration = new ViewDnsConfiguration()
            {
                ApiOutputType = "json",
                ApiBaseUrl = "http://pro.viewdns.info",
                ApiKey = "7eb568767fd1afa11296d82cd7becedf5593c4da",
                ReverseWhoisUrl = null
            };
            Assert.Throws<ArgumentNullException>(() => new ViewDnsProxy(configuration));
        }

        /// <summary>
        /// Throws the argument exception with null parameter probe proxy get domain details.
        /// </summary>
        [Fact]
        public void ThrowArgumentExceptionWithNullParameterProbeProxy_GetDomainDetails()
        {
            var configuration = new ViewDnsConfiguration()
            {
                ApiOutputType = "json",
                ApiBaseUrl = "http://pro.viewdns.info",
                ApiKey = "7eb568767fd1afa11296d82cd7becedf5593c4da",
                ReverseWhoisUrl = "reversewhois"
            };
            Assert.ThrowsAsync<ArgumentNullException>(() =>
            {
                var proxy = new ViewDnsProxy(configuration);
                return Task.Run(() => proxy.GetDomainDetails(null));
            });
        }
    }
}
