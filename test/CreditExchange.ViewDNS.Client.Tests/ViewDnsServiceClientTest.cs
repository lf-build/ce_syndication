﻿using Moq;
using RestSharp;
using LendFoundry.Foundation.Services;
using Xunit;

namespace CreditExchange.ViewDNS.Client.Tests
{
    /// <summary>
    /// View dns service client test class
    /// </summary>
    public class ViewDnsServiceClientTest
    {
        /// <summary>
        /// Gets the view DNS service client.
        /// </summary>
        /// <value>
        /// The view DNS service client.
        /// </value>
        private CreditExchange.ViewDNS.Client.ViewDnsService ViewDnsServiceClient { get; }

        /// <summary>
        /// Gets or sets the request.
        /// </summary>
        /// <value>
        /// The request.
        /// </value>
        private IRestRequest Request { get; set; }

        /// <summary>
        /// Gets the mock service client.
        /// </summary>
        /// <value>
        /// The mock service client.
        /// </value>
        private Mock<IServiceClient> MockServiceClient { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewDnsServiceClientTest"/> class.
        /// </summary>
        public ViewDnsServiceClientTest()
        {
            MockServiceClient = new Mock<IServiceClient>();
            ViewDnsServiceClient = new ViewDnsService(MockServiceClient.Object);
        }

        /// <summary>
        /// Clients the verify employee.
        /// </summary>
        [Fact]
        public void Client_Verify_Employee()
        {
            var companyName = "B & A Creations Private Limited";
            var cin = "U18109DL2008PTC177167";
            var emailAddress = "hiren.p@bacreations.com";

            MockServiceClient.Setup(s => s.ExecuteAsync<Syndication.ViewDNS.Response.EmployeeVerification>(It.IsAny<RestRequest>()))
                .Callback<IRestRequest>(r => Request = r).ReturnsAsync(new Syndication.ViewDNS.Response.EmployeeVerification(false, "None"));

            ViewDnsServiceClient.VerifyEmployment("app", "1", emailAddress, companyName, cin).Wait();
            Assert.Equal("{entityType}/{entityId}/{emailAddress}/{companyName}/{cin}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
        }
    }
}