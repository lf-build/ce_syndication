﻿using LendFoundry.Security.Tokens;
using CreditExchange.Syndication.ViewDNS;

namespace CreditExchange.ViewDNS.Client
{
    /// <summary>
    /// View dns service client factory interface
    /// </summary>
    public interface IViewDnsServiceClientFactory
    {
        /// <summary>
        /// Creates the specified reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns></returns>
        IViewDnsService Create(ITokenReader reader);
    }
}
