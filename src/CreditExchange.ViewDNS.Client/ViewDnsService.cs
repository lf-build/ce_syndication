﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using CreditExchange.Syndication.ViewDNS;
using RestSharp;
using System.Net;

namespace CreditExchange.ViewDNS.Client
{
    /// <summary>
    /// View dns service class
    /// </summary>
    /// <seealso cref="IViewDnsService" />
    public class ViewDnsService : IViewDnsService
    {
        /// <summary>
        /// Gets the client.
        /// </summary>
        /// <value>
        /// The client.
        /// </value>
        private IServiceClient Client { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewDnsService"/> class.
        /// </summary>
        /// <param name="client">The client.</param>
        public ViewDnsService(IServiceClient client)
        {
            Client = client;
        }

        /// <summary>
        /// Gets the employee verification.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="emailAddress"></param>
        /// <param name="companyName"></param>
        /// <param name="cin"></param>
        /// <returns></returns>
        public async Task<Syndication.ViewDNS.Response.IEmployeeVerification> VerifyEmployment(string entityType, string entityId, string emailAddress, string companyName, string cin)
        {
            var request = new RestRequest("{entityType}/{entityId}/{emailAddress}/{companyName}/{cin}", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("emailAddress", emailAddress);
            request.AddUrlSegment("companyName", WebUtility.UrlEncode(companyName));
            request.AddUrlSegment("cin", cin);
            return await Client.ExecuteAsync<Syndication.ViewDNS.Response.EmployeeVerification>(request);
        }
    }
}
