﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace CreditExchange.ViewDNS.Client
{
    /// <summary>
    /// View Dns Service Client Extensions class
    /// </summary>
    public static class ViewDnsServiceClientExtensions
    {
        /// <summary>
        /// Adds the view DNS service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="port">The port.</param>
        /// <returns></returns>
        public static IServiceCollection AddViewDnsService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IViewDnsServiceClientFactory>(p => new ViewDnsServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IViewDnsServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
