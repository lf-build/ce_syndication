﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using CreditExchange.Syndication.ViewDNS;
using System;

namespace CreditExchange.ViewDNS.Client
{
    /// <summary>
    /// View dns service client factory class
    /// </summary>
    /// <seealso cref="IViewDnsServiceClientFactory" />
    public class ViewDnsServiceClientFactory : IViewDnsServiceClientFactory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewDnsServiceClientFactory"/> class.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="port">The port.</param>
        public ViewDnsServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }
        /// <summary>
        /// Gets the provider.
        /// </summary>
        /// <value>
        /// The provider.
        /// </value>
        private IServiceProvider Provider { get; }

        /// <summary>
        /// Gets the endpoint.
        /// </summary>
        /// <value>
        /// The endpoint.
        /// </value>
        private string Endpoint { get; }

        /// <summary>
        /// Gets the port.
        /// </summary>
        /// <value>
        /// The port.
        /// </value>
        private int Port { get; }

        /// <summary>
        /// Creates the specified reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns></returns>
        public IViewDnsService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new ViewDnsService(client);
        }
    }
}
