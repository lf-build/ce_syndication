﻿using System;
using System.Threading.Tasks;
using LendFoundry.EventHub.Client;
using CreditExchange.Syndication.EmailAge.Proxy;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Lookup;
using System.Linq;

namespace CreditExchange.Syndication.EmailAge
{
    public class EmailAgeService : IEmailAgeService
    {
        public EmailAgeService(IEmailAgeProxy proxy, IEventHubClient eventHub, ILookupService lookup)
        {
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));
             Proxy = proxy;
            EventHub = eventHub;
            Lookup = lookup;
        }
        public static string ServiceName { get; } = "email-age";
        private IEmailAgeProxy Proxy { get; }
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }
        public async Task<IVerifyEmailResponse> VerifyEmail(string entityType, string entityId, string email)
        {
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentException("Enity Type is require", nameof(entityType));
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentException("Entity Id is require", nameof(entityId));
         
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(email));
            var response = await Task.Run(()=> Proxy.VerifyEmail(email));
            if (response == null)
                throw new NotFoundException("email not found.");

            var referencenumber = Guid.NewGuid().ToString("N");
            var result = new VerifyEmailResponse(response);

                await EventHub.Publish(new EmailVerificationRequested()
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = email,
                    ReferenceNumber = referencenumber,
                     Name = ServiceName
                });

            return result;
        }
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }

    }
}