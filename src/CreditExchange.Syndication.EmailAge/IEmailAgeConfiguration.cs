﻿namespace CreditExchange.Syndication.EmailAge
{
    public interface IEmailAgeConfiguration
    {
        string BaseUrl { get; set; }
        string AccountSid { get; set; }
        string AuthToken { get; set; }
        int ExpirationInDays { get; set; }
    }
}