﻿using System;
using System.Collections.Generic;
using System.Linq;
using CreditExchange.Syndication.EmailAge.Proxy;

namespace CreditExchange.Syndication.EmailAge
{
    public class VerifyEmailResponse : IVerifyEmailResponse
    {
        public VerifyEmailResponse()
        {
            
        }

        public VerifyEmailResponse(Result response)
        {
            Email = response.Email;
            Name = response.EName;
            EmailAge = response.EmailAge;
            DomainAge = response.DomainAge;
            FirstVerificationDate = response.FirstVerificationDate;
            LastVerificationDate = response.LastVerificationDate;
            Status = response.Status;
            Country = response.Country;
            FraudRisk = response.FraudRisk;
            Score = response.EaScore;
            Reason = response.EaReason;
            StatusId = response.EaStatusId;
            ReasonId = response.EaReasonId;
            AdviceId = response.EaAdviceId;
            Advice = response.EaAdvice;
            RiskBandId = response.EaRiskBandId;
            RiskBand = response.EaRiskBand;
            DateOfBirth = response.Dob;
            Gender = response.Gender;
            Location = response.Location;
            SocialMediaFriends = response.SmFriends;
            TotalHits = response.Totalhits;
            UniqueHits = response.Uniquehits;
            ImageUrl = response.Imageurl;
            EmailExists = response.EmailExists;
            DomainExists = response.DomainExists;
            Company = response.Company;
            Title = response.Title;
            DomainName = response.Domainname;
            DomainCompany = response.Domaincompany;
            DomainCountryName = response.Domaincountryname;
            DomainCategory = response.Domaincategory;
            DomainCorporate = response.Domaincorporate;
            DomainRiskLevel = response.Domainrisklevel;
            DomainRelevantInfo = response.Domainrelevantinfo;
            DomainRiskLevelId = response.DomainrisklevelId;
            DomainRelevantInfoId = response.DomainrelevantinfoId;
            DomainRiskCountry = response.Domainriskcountry;
            if (response.Smlinks != null)
            {
                SocialMediaLinks =
                    new List<ISocialMediaLink>(
                        response.Smlinks.Select(s => new SocialMediaLink {Link = s.Link, Source = s.Source}).ToList());

            }
        }

        public string Email { get; set; }
        public string Name { get; set; }
        public DateTimeOffset? EmailAge { get; set; }
        public DateTimeOffset? DomainAge { get; set; }
        public DateTimeOffset? FirstVerificationDate { get; set; }
        public DateTimeOffset? LastVerificationDate { get; set; }
        /// <summary>
        /// GeneralError: Unidentified error generated at the time of your request.
        ///Certified: The email exists and we have its creation data.
        ///Verified: The email exists but we are not sure when it was created.
        ///EmailInexistent: The email does not exist but the domain does.
        ///ValidDomain: The email domain exists but we are not sure if the email address exists.
        ///DomainInexistent: The domain doesn't exist.
        /// </summary>
        public string Status { get; set; }
        public string Country { get; set; }
        public string FraudRisk { get; set; }
        public string Score { get; set; }
        public string Reason { get; set; }
        /// <summary>
        /// 0 - GeneralError
        ///1 - Certified
        ///2 - Verified
        ///3 - EmailNonxistent
        ///4 - ValidDomain
        ///5 - DomainInexistent
        /// </summary>
        public int? StatusId { get; set; }

        /// <summary>
        ///1 - Fraud Level X 
        ///2 - Email does not exist
        ///3 - Domain does not exist
        ///4 - Risky Domain
        ///5 - Risky Country
        ///6 - Risky Email Name
        ///7 - Numeric Email
        ///8 - Limited History for Email
        ///9 - Email Recently Created
        ///10 - Email linked to High Risk Account
        ///11 - Good Level X
        ///12 - Low Risk Domain
        ///13 - Email Created X Years Ago
        ///14 - Email Created at least X Years Ago
        ///15 - Email Linked to Low Risk Account
        ///16 - InvalidEmailSyntax
        ///17 - Mailbox is Full
        ///18 - Mailbox is Inactive
        ///19 - Corporate Link
        ///20 - Mailbox is Expired
        ///21 - User Defined Risk Domain
        ///22 - User Defined Low Risk Domain
        ///23 - Velocity Level X
        ///24 - Risk Domain Category
        ///25 - Low Risk Domain Category
        ///26 - High Risk Email Account
        ///27 - Email Created at least X Months Ago
        ///28 - Valid Email From X Country Domain
        ///29 - Valid Domain From X Country
        ///30 - Potentially Breached Email
        ///31 - Fraud Emails Linked X
        ///32 - Good Email Linked Level X
        ///33 - Fraud IP Level X
        ///34 - Good IP Level X
        ///35 - Risky Proxy IP
        ///36 - Risk IP Behavior
        ///37 - Risky IP Country
        ///38 - IP Not Found
        ///39 - IP Invalid Syntax Format
        ///40 - High Risk IP
        ///51 - Good Popularity
        ///52 - Risk Domain Category Review
        ///53 - Tumbling Abuse
        ///54 - Email Enumeration for Company
        ///55 - Email Enumeration for Industry
        ///56 - Creation Date Velocity
        ///61 - Customer Email not Provided
        ///62 - Risk Email Pattern
        ///63 - Suspected Fraud
        ///101 - Low Risk Email Domain for Company
        ///102 - Low Risk IP for Company
        ///103 - Low Risk IP Geolocation for Company
        ///104 - Low Risk Email Domain for Industry
        ///105 - Low Risk IP for Industry
        ///106 - Low Risk IP Geolocation for Industry
        ///107 - Low Risk Email Domain for Network
        ///108 - Low Risk IP for Network
        ///109 - Low Risk IP Geolocation for Network
        ///110 - Very Low Risk Email Domain for Company
        ///111 - Very Low Risk IP for Company
        ///112 - Very Low Risk IP Geolocation for Company
        ///113 - Very Low Risk Email Domain for Industry
        ///114 - Very Low Risk IP for Industry
        ///115 - Very Low Risk IP Geolocation for Industry
        ///116 - Very Low Risk Email Domain for Network
        ///117 - Very Low Risk IP for Network
        ///118 - Very Low Risk IP Geolocation for Network
        ///121 - High Risk Email Domain for Company
        ///122 - High Risk IP for Company
        ///123 - High Risk IP Geolocation for Company
        ///124 - High Risk Email Domain for Industry
        ///125 - High Risk IP for Industry
        ///126 - High Risk IP Geolocation for Industry
        ///127 - High Risk Email Domain for Network
        ///128 - High Risk IP for Network
        ///129 - High Risk IP Geolocation for Network
        ///130 - Very High Risk Email Domain for Company
        ///131 - Very High Risk IP for Company
        ///132 - Very High Risk IP Geolocation for Company
        ///133 - Very High Risk Email Domain for Industry
        ///134 - Very High Risk IP for Industry
        ///135 - Very High Risk IP Geolocation for Industry
        ///136 - Very High Risk Email Domain for Network
        ///137 - Very High Risk IP for Network
        ///138 - Very High Risk IP Geolocation for Network
        ///
        /// </summary>
        public int? ReasonId { get; set; }

        /// <summary>
        /// 1 - Fraud Review
        ///2 - Unclear Risk
        ///3 - Lower Fraud Risk
        ///4 - Moderate Fraud Risk
        ///11 - Data Entry Review
        ///1001 - Custom Fraud Score Define.
        /// </summary>
        public int? AdviceId { get; set; }
        public string Advice { get; set; }
        public int RiskBandId { get; set; }
        public string RiskBand { get; set; }
        public string DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string Location { get; set; }
        public int? SocialMediaFriends { get; set; }
        public int? TotalHits { get; set; }
        public int? UniqueHits { get; set; }
        public string ImageUrl { get; set; }

        /// <summary>
        /// Yes
        ///No
        ///Not Anymore
        ///Not Sure
        /// </summary>
        public string EmailExists { get; set; }

        /// <summary>
        /// Yes
        ///No
        ///Not Anymore
        ///Not Sure
        /// </summary>
        public string DomainExists { get; set; }

        public string Company { get; set; }
        public string Title { get; set; }
        public string DomainName { get; set; }
        public string DomainCompany { get; set; }
        public string DomainCountryName { get; set; }
        public string DomainCategory { get; set; }
        public string DomainCorporate { get; set; }
        public string DomainRiskLevel { get; set; }
        public string DomainRelevantInfo { get; set; }
        /// <summary>
        /// Possible values: 
        ///1 - VeryHigh
        ///2 - High
        ///3 - Moderate
        ///4 - Low
        ///5 - VeryLow
        ///6 - Review
        /// 
        /// </summary>
        public int? DomainRiskLevelId { get; set; }

        /// <summary>
        /// 501 - NoKnownRisk
        ///502 - RiskCountry
        ///503 - HighRiskCategory
        ///504 - HighRiskDomain
        ///505 - RecentlyCreated
        ///506 - InvalidDomain
        ///507 - LowRiskCategory
        ///508 - ValidDomainFromCountry
        ///509 - ValidDomain
        ///510 - LowRiskDomain
        ///511 - ReviewRiskCategory
        ///521 - LowRiskEmailDomainforCompany
        ///522 - LowRiskEmailDomainforIndustry
        ///523 - LowRiskEmailDomainforNetwork
        ///524 - VeryLowRiskEmailDomainforCompany
        ///525 - VeryLowRiskEmailDomainforIndustry
        ///526 - VeryLowRiskEmailDomainforNetwork
        ///527 - HighRiskEmailDomainforCompany
        ///528 - HighRiskEmailDomainforIndustry
        ///529 - HighRiskEmailDomainforNetwork
        ///530 - VeryHighRiskEmailDomainforCompany
        ///531 - VeryHighRiskEmailDomainforIndustry
        ///532 - VeryHighRiskEmailDomainforNetwork
        /// </summary>
        public int? DomainRelevantInfoId { get; set; }
        public string DomainRiskCountry { get; set; }
        public List<ISocialMediaLink> SocialMediaLinks { get; set; }
    }
}