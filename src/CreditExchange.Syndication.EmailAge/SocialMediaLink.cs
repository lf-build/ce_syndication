namespace CreditExchange.Syndication.EmailAge
{
    public class SocialMediaLink : ISocialMediaLink
    {
        public string Source { get; set; }
        public string Link { get; set; }
    }
}