namespace CreditExchange.Syndication.EmailAge
{
    public interface ISocialMediaLink
    {
        string Source { get; set; }
        string Link { get; set; }
    }
}