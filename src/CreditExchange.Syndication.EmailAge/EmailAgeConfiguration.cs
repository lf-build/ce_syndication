﻿namespace CreditExchange.Syndication.EmailAge
{
    public class EmailAgeConfiguration : IEmailAgeConfiguration
    {
        public string BaseUrl { get; set; } = "https://api.emailage.com/emailagevalidator";
        public string AccountSid { get; set; } = "15C360410934428EBFC907DBCFAC5B88";
        public string AuthToken { get; set; } = "2068E399EAFF4D87831C53A5C699A33C";
        public int ExpirationInDays { get; set; }
    }
}