﻿using System;
using System.Runtime.Serialization;

namespace CreditExchange.Syndication.EmailAge
{
    [Serializable]
    public class EmailAgeException : Exception
    {
        public EmailAgeException()
        {
        }

        public EmailAgeException(string message) : base(message)
        {
        }

        public EmailAgeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EmailAgeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}