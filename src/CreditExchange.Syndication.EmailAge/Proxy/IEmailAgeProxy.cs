﻿namespace CreditExchange.Syndication.EmailAge.Proxy
{
    public interface IEmailAgeProxy
    {
        Result VerifyEmail(string email);
    }
}