using System;
using System.Collections.Generic;

namespace CreditExchange.Syndication.EmailAge.Proxy
{
    public class Query
    {
        public string Email { get; set; }
        public string QueryType { get; set; }
        public int Count { get; set; }
        public DateTimeOffset Created { get; set; }
        public string Lang { get; set; }
        public int ResponseCount { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public List<Result> Results { get; set; }
    }
}