namespace CreditExchange.Syndication.EmailAge.Proxy
{
    public class Smlink
    {
        public string Source { get; set; }
        public string Link { get; set; }
    }
}