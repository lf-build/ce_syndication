namespace CreditExchange.Syndication.EmailAge.Proxy
{
    public class QueryResult
    {
        public Query Query { get; set; }
        public QueryResponseStatus ResponseStatus { get; set; }
    }
}