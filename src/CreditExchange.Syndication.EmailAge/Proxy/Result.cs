using System;
using System.Collections.Generic;

namespace CreditExchange.Syndication.EmailAge.Proxy
{
    public class Result
    {
        public string Email { get; set; }
        public string EName { get; set; }
        public DateTimeOffset? EmailAge { get; set; }
        public DateTimeOffset? DomainAge { get; set; }
        public DateTimeOffset? FirstVerificationDate { get; set; }
        public DateTimeOffset? LastVerificationDate { get; set; }
        /// <summary>
        /// GeneralError: Unidentified error generated at the time of your request.
        ///Certified: The email exists and we have its creation data.
        ///Verified: The email exists but we are not sure when it was created.
        ///EmailInexistent: The email does not exist but the domain does.
        ///ValidDomain: The email domain exists but we are not sure if the email address exists.
        ///DomainInexistent: The domain doesn't exist.
        /// </summary>
        public string Status { get; set; }
        public string Country { get; set; }
        public string FraudRisk { get; set; }
        public string EaScore { get; set; }
        public string EaReason { get; set; }
        /// <summary>
        /// 0 - GeneralError
        ///1 - Certified
        ///2 - Verified
        ///3 - EmailNonxistent
        ///4 - ValidDomain
        ///5 - DomainInexistent
        /// </summary>
        public int? EaStatusId { get; set; }

        /// <summary>
        ///1 - Fraud Level X 
        ///2 - Email does not exist
        ///3 - Domain does not exist
        ///4 - Risky Domain
        ///5 - Risky Country
        ///6 - Risky Email Name
        ///7 - Numeric Email
        ///8 - Limited History for Email
        ///9 - Email Recently Created
        ///10 - Email linked to High Risk Account
        ///11 - Good Level X
        ///12 - Low Risk Domain
        ///13 - Email Created X Years Ago
        ///14 - Email Created at least X Years Ago
        ///15 - Email Linked to Low Risk Account
        ///16 - InvalidEmailSyntax
        ///17 - Mailbox is Full
        ///18 - Mailbox is Inactive
        ///19 - Corporate Link
        ///20 - Mailbox is Expired
        ///21 - User Defined Risk Domain
        ///22 - User Defined Low Risk Domain
        ///23 - Velocity Level X
        ///24 - Risk Domain Category
        ///25 - Low Risk Domain Category
        ///26 - High Risk Email Account
        ///27 - Email Created at least X Months Ago
        ///28 - Valid Email From X Country Domain
        ///29 - Valid Domain From X Country
        ///30 - Potentially Breached Email
        ///31 - Fraud Emails Linked X
        ///32 - Good Email Linked Level X
        ///33 - Fraud IP Level X
        ///34 - Good IP Level X
        ///35 - Risky Proxy IP
        ///36 - Risk IP Behavior
        ///37 - Risky IP Country
        ///38 - IP Not Found
        ///39 - IP Invalid Syntax Format
        ///40 - High Risk IP
        ///51 - Good Popularity
        ///52 - Risk Domain Category Review
        ///53 - Tumbling Abuse
        ///54 - Email Enumeration for Company
        ///55 - Email Enumeration for Industry
        ///56 - Creation Date Velocity
        ///61 - Customer Email not Provided
        ///62 - Risk Email Pattern
        ///63 - Suspected Fraud
        ///101 - Low Risk Email Domain for Company
        ///102 - Low Risk IP for Company
        ///103 - Low Risk IP Geolocation for Company
        ///104 - Low Risk Email Domain for Industry
        ///105 - Low Risk IP for Industry
        ///106 - Low Risk IP Geolocation for Industry
        ///107 - Low Risk Email Domain for Network
        ///108 - Low Risk IP for Network
        ///109 - Low Risk IP Geolocation for Network
        ///110 - Very Low Risk Email Domain for Company
        ///111 - Very Low Risk IP for Company
        ///112 - Very Low Risk IP Geolocation for Company
        ///113 - Very Low Risk Email Domain for Industry
        ///114 - Very Low Risk IP for Industry
        ///115 - Very Low Risk IP Geolocation for Industry
        ///116 - Very Low Risk Email Domain for Network
        ///117 - Very Low Risk IP for Network
        ///118 - Very Low Risk IP Geolocation for Network
        ///121 - High Risk Email Domain for Company
        ///122 - High Risk IP for Company
        ///123 - High Risk IP Geolocation for Company
        ///124 - High Risk Email Domain for Industry
        ///125 - High Risk IP for Industry
        ///126 - High Risk IP Geolocation for Industry
        ///127 - High Risk Email Domain for Network
        ///128 - High Risk IP for Network
        ///129 - High Risk IP Geolocation for Network
        ///130 - Very High Risk Email Domain for Company
        ///131 - Very High Risk IP for Company
        ///132 - Very High Risk IP Geolocation for Company
        ///133 - Very High Risk Email Domain for Industry
        ///134 - Very High Risk IP for Industry
        ///135 - Very High Risk IP Geolocation for Industry
        ///136 - Very High Risk Email Domain for Network
        ///137 - Very High Risk IP for Network
        ///138 - Very High Risk IP Geolocation for Network
        ///
        /// </summary>
        public int? EaReasonId { get; set; }

        /// <summary>
        /// 1 - Fraud Review
        ///2 - Unclear Risk
        ///3 - Lower Fraud Risk
        ///4 - Moderate Fraud Risk
        ///11 - Data Entry Review
        ///1001 - Custom Fraud Score Define.
        /// </summary>
        public int? EaAdviceId { get; set; }
        public string EaAdvice { get; set; }
        public int EaRiskBandId { get; set; }
        public string EaRiskBand { get; set; }
        public string Dob { get; set; }
        public string Gender { get; set; }
        public string Location { get; set; }
        public int? SmFriends { get; set; }
        public int? Totalhits { get; set; }
        public int? Uniquehits { get; set; }
        public string Imageurl { get; set; }

        /// <summary>
        /// Yes
        ///No
        ///Not Anymore
        ///Not Sure
        /// </summary>
        public string EmailExists { get; set; }

        /// <summary>
        /// Yes
        ///No
        ///Not Anymore
        ///Not Sure
        /// </summary>
        public string DomainExists { get; set; }

        public string Company { get; set; }
        public string Title { get; set; }
        public string Domainname { get; set; }
        public string Domaincompany { get; set; }
        public string Domaincountryname { get; set; }
        public string Domaincategory { get; set; }
        public string Domaincorporate { get; set; }
        public string Domainrisklevel { get; set; }
        public string Domainrelevantinfo { get; set; }
        /// <summary>
        /// Possible values: 
        ///1 - VeryHigh
        ///2 - High
        ///3 - Moderate
        ///4 - Low
        ///5 - VeryLow
        ///6 - Review
        /// 
        /// </summary>
        public int? DomainrisklevelId { get; set; }

        /// <summary>
        /// 501 - NoKnownRisk
        ///502 - RiskCountry
        ///503 - HighRiskCategory
        ///504 - HighRiskDomain
        ///505 - RecentlyCreated
        ///506 - InvalidDomain
        ///507 - LowRiskCategory
        ///508 - ValidDomainFromCountry
        ///509 - ValidDomain
        ///510 - LowRiskDomain
        ///511 - ReviewRiskCategory
        ///521 - LowRiskEmailDomainforCompany
        ///522 - LowRiskEmailDomainforIndustry
        ///523 - LowRiskEmailDomainforNetwork
        ///524 - VeryLowRiskEmailDomainforCompany
        ///525 - VeryLowRiskEmailDomainforIndustry
        ///526 - VeryLowRiskEmailDomainforNetwork
        ///527 - HighRiskEmailDomainforCompany
        ///528 - HighRiskEmailDomainforIndustry
        ///529 - HighRiskEmailDomainforNetwork
        ///530 - VeryHighRiskEmailDomainforCompany
        ///531 - VeryHighRiskEmailDomainforIndustry
        ///532 - VeryHighRiskEmailDomainforNetwork
        /// </summary>
        public int? DomainrelevantinfoId { get; set; }
        public string Domainriskcountry { get; set; }
        public List<Smlink> Smlinks { get; set; }
    }
}