namespace CreditExchange.Syndication.EmailAge.Proxy
{
    public class QueryResponseStatus
    {
        public string Status { get; set; }
        public string ErrorCode { get; set; }
        public string Description { get; set; }
    }
}