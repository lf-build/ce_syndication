﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using RestSharp;
using Newtonsoft.Json;
using System.Security.Cryptography;
using RestSharp.Contrib;

namespace CreditExchange.Syndication.EmailAge.Proxy
{
    public class EmailAgeProxy : IEmailAgeProxy
    {
        public EmailAgeProxy(IEmailAgeConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if(string.IsNullOrWhiteSpace(configuration.BaseUrl))
                throw new ArgumentNullException(nameof(configuration.BaseUrl));

            if (string.IsNullOrWhiteSpace(configuration.AccountSid))
                throw new ArgumentNullException(nameof(configuration.AccountSid));

            if (string.IsNullOrWhiteSpace(configuration.AuthToken))
                throw new ArgumentNullException(nameof(configuration.AuthToken));

            if (configuration.BaseUrl.EndsWith("/") == false)
            {
                configuration.BaseUrl = configuration.BaseUrl + "/";
            }

            Configuration = configuration;
        }

        private IEmailAgeConfiguration Configuration { get; }

        public Result VerifyEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(email));

            var restRequest = new RestRequest(Method.POST);
            var client = new RestClient(Configuration.BaseUrl);

            restRequest.AddParameter("text/plain", email, ParameterType.RequestBody);
            var result= ExecuteRequest<QueryResult>(client,restRequest);
            if (result?.ResponseStatus == null)
            {
                throw new EmailAgeException("No response returned");
            }

            if (result.ResponseStatus.Status!= "success")
            {
                throw new EmailAgeException(result.ResponseStatus.Description);
            }
            if (!result.Query.Results.Any())
            {
                throw new EmailAgeException("No response returned");
            }

            return result.Query.Results.First();
        }

        private T ExecuteRequest<T>(IRestClient client, IRestRequest request) where T : class
        {
            var randomString = GetRandomString(10);
            var timeStamp = Math.Floor((DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds).ToString(CultureInfo.InvariantCulture);

            var query = new Dictionary<string, string>
            {
                {"format", "json"},
                {"oauth_consumer_key", Configuration.AccountSid},
                {"oauth_nonce", randomString},
                {"oauth_signature_method", "HMAC-SHA1"},
                {"oauth_timestamp", timeStamp},
                {"oauth_version", "1.0"}
            };

            var queryParameters = string.Join("&", (from x in query
                orderby x.Key
                select x.Key + (x.Value == null ? string.Empty : "=" + UrlEncode(x.Value))).ToArray());

            var input = string.Concat(request.Method.ToString().ToUpper(), "&", UrlEncode(Configuration.BaseUrl), "&",
                UrlEncode(queryParameters));

            string oauthSignature;
            using (var hashAglorithm = new HMACSHA1())
            {
                hashAglorithm.Key = Encoding.ASCII.GetBytes(Configuration.AuthToken + "&");
                oauthSignature = Convert.ToBase64String(hashAglorithm.ComputeHash(Encoding.ASCII.GetBytes(input)));
            }
             
            request.Resource= string.Concat( "?", queryParameters, "&oauth_signature=", UrlEncode(oauthSignature));
            request.AddHeader("Content-Type", "application/json");

            var response = client.Execute(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new EmailAgeException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new EmailAgeException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
                    throw new EmailAgeException(
                        $"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");
            }

            try
            {
                //TODO: check later it is adding some characters like below
                //var content= response.Content.TrimStart("ï»¿".ToCharArray());
                string content;
                using (var memoryStream=new System.IO.MemoryStream(response.RawBytes))
                {
                    using (var stream=new System.IO.StreamReader(memoryStream))
                    {
                        content = stream.ReadToEnd();
                    }
                }
                content= HttpUtility.UrlDecode(content.Replace("%22", "\\%22").Replace("%5c", "\\%5c"));
                return JsonConvert.DeserializeObject<T>(content);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response.Content, exception);
            }
        }


        private const string UnreservedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~";
        private static Random Random { get; }= new Random();

        private static string GetRandomString(int length)
        {
            var stringBuilder = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                stringBuilder.Append(UnreservedChars[Random.Next(0, 25)]);
            }
            return stringBuilder.ToString();
        }

        private static string UrlEncode(string value)
        {
            var stringBuilder = new StringBuilder();
            foreach (var c in value)
            {
                if (UnreservedChars.IndexOf(c) != -1)
                {
                    stringBuilder.Append(c);
                }
                else
                {
                    stringBuilder.Append($"%{(int) c:X2}");
                }
            }
            return stringBuilder.ToString();
        }

    }
}