﻿using System.Threading.Tasks;

namespace CreditExchange.Syndication.EmailAge
{
    public interface IEmailAgeService
    {
       Task<IVerifyEmailResponse> VerifyEmail(string entityType, string entityId, string email);
    }
}