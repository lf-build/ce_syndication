﻿using System;
using System.Collections.Generic;
using CreditExchange.Syndication.EmailAge.Proxy;

namespace CreditExchange.Syndication.EmailAge
{
    public interface IVerifyEmailResponse
    {
        string Advice { get; set; }
        int? AdviceId { get; set; }
        string Company { get; set; }
        string Country { get; set; }
        string DateOfBirth { get; set; }
        DateTimeOffset? DomainAge { get; set; }
        string DomainCategory { get; set; }
        string DomainCompany { get; set; }
        string DomainCorporate { get; set; }
        string DomainCountryName { get; set; }
        string DomainExists { get; set; }
        string DomainName { get; set; }
        string DomainRelevantInfo { get; set; }
        int? DomainRelevantInfoId { get; set; }
        string DomainRiskCountry { get; set; }
        string DomainRiskLevel { get; set; }
        int? DomainRiskLevelId { get; set; }
        string Email { get; set; }
        DateTimeOffset? EmailAge { get; set; }
        string EmailExists { get; set; }
        DateTimeOffset? FirstVerificationDate { get; set; }
        string FraudRisk { get; set; }
        string Gender { get; set; }
        string ImageUrl { get; set; }
        DateTimeOffset? LastVerificationDate { get; set; }
        string Location { get; set; }
        string Name { get; set; }
        string Reason { get; set; }
        int? ReasonId { get; set; }
        string RiskBand { get; set; }
        int RiskBandId { get; set; }
        string Score { get; set; }
        int? SocialMediaFriends { get; set; }
        List<ISocialMediaLink> SocialMediaLinks { get; set; }
        string Status { get; set; }
        int? StatusId { get; set; }
        string Title { get; set; }
        int? TotalHits { get; set; }
        int? UniqueHits { get; set; }
    }
}