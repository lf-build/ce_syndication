﻿using LendFoundry.SyndicationStore.Events;

namespace CreditExchange.Syndication.EmailAge
{
    public class EmailVerificationRequested : SyndicationCalledEvent
    {
    }
}
