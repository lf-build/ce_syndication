﻿using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace CreditExchange.YodleeYsl.Abstractions
{
    public interface IYodleeRepository :IRepository<IYodleeData>
    {
        Task<IYodleeData> GetByEntityId(string entityType, string entityId);
    }
}
