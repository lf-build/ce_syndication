﻿namespace CreditExchange.YodleeYsl.Abstractions
{
    public class CobrandSession : ICobrandSession
    {
        public string ApplicationId  { get;   set;}

        public long CobrandId { get; set; }
        

        public string CobSessionId { get; set; }

        public string Locale { get; set; }
        
    }
}
