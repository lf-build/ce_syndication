﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.YodleeYsl.Abstractions
{
    public class YodleeData : Aggregate,IYodleeData
    {
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ICobrandSession, CobrandSession>))]
        public ICobrandSession CobSession { get; set; }
        public string UserSession { get; set; }
        public string ProviderAccountId { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IUserData, UserData>))]
        public IUserData UserData { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccountNo { get; set; }

        public string ItemId { get; set; }
        public string ItemAccountId { get; set; }
        public bool IsRegistered { get; set; }
        public string FastLinkAccessToken { get; set; }
        public TimeBucket StartDate { get; set; }

        public TimeBucket ReportPulleddDate { get; set; }
    }
}
