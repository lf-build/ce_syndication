﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.YodleeYsl.Abstractions
{
    public class UserData :IUserData
    {
       public string LoginName { get; set; }
       public string Password { get; set; }
       public string Email { get; set; }
    }
}
