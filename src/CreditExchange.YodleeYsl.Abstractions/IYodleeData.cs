﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace CreditExchange.YodleeYsl.Abstractions
{
    public interface IYodleeData :IAggregate
    {
         string EntityId { get; set; }
         string EntityType { get; set; }
         ICobrandSession CobSession { get; set; }
         string UserSession { get; set; }
         string ProviderAccountId { get; set; }
         string ItemId { get; set; }
         string ItemAccountId { get; set; }
        string FastLinkAccessToken { get; set; }
        string BankAccountName { get; set; }
        string BankAccountNo { get; set; }
        IUserData UserData { get; set; }
        bool IsRegistered { get; set; }

        TimeBucket StartDate { get; set; }

        TimeBucket ReportPulleddDate { get; set; }

    }
}
