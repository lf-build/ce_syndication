﻿namespace CreditExchange.YodleeYsl.Abstractions
{
    public interface ICobrandSession
    {
        string ApplicationId { get; set; }
        long CobrandId { get; set; }
        string Locale { get; set; }
        string CobSessionId { get; set; }
    }
}
