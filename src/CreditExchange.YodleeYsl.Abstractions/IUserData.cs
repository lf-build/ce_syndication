﻿namespace CreditExchange.YodleeYsl.Abstractions
{
    public interface IUserData
    {
        string LoginName { get; set; }
        string Password { get; set; }
        string Email { get; set; }
    }
}
