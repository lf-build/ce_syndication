﻿using CreditExchange.Syndication.YodleeYsl;
using CreditExchange.Syndication.YodleeYsl.Request;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CreditExchange.YodleeYsl.Abstractions
{
    public interface IYodleeYslService
    {
        Task<ICobrandLoginResponse> LoginForCobrand(string entityType, string entityId);
        Task<IUserRegistrationResponse> RegisterUser(string entityType, string entityId, IUserRegistrationRequest request);
        Task<IUserLoginResponse> LoginUser(string entityType, string entityId);
        Task<ISearchBankResponse> SearchBank(string entityType, string entityId, string searchName);
        Task<IBankLoginResponse> GetBankLoginForm(string entityType, string entityId, string providerId);
        Task<IProviderAccountRefreshStatus> PostBankLoginForm(string entityType, string entityId, string providerId, IUpdateBankLogin loginForm);
        IMfaRefreshResponse GetMfaStatusWithRefreshInformation(string cobrandSessionId, string userSessionId, string providerAccountId);
        string SendMfaDetails(string cobrandSessionId, string userSessionId, string providerAccountId, dynamic mfaChallenge);
        IFinalRefreshResponse GetRefreshStatus(string cobrandSessionId, string userSessionId, string providerAccountId);
        Task<IList<IItemSummaries>> GetItemSummaries(string entityType, string entityId);
        Task<IFinalReport> FetchReport(string entityType, string entityId);
        Task<Syndication.YodleeYsl.Response.IFastlinkAccessTokenResponse> GetFastlinkAccessToken(string entityType, string entityId);
        Task<IGetFastLinkUrlResponse> GetFalstLinkUrl(string entityType, string entityId, FastLinkUrlRequest fastlinkReuest);
        Task<Syndication.YodleeYsl.Response.IGetBankAccountResponse> GetUserBankAccounts(string entityType, string entityId);
        string StartRefreshStatus(string cobrandSessionId, string userSessionId, string providerAccountId);
        Task<IFinalReport> GetReport(string entityType, string entityId);
        Task<string> RemoveLinkAccount(string entityType, string entityId);

        Task<IYodleeData> GetStatus(string entityType, string entityId);


    }
}
