﻿using System;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.Probe;
#if DOTNET2
using  Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using LendFoundry.Foundation.Logging;

namespace CreditExchange.Probe.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IProbeService probeService,ILogger logger):base(logger)
        {
            if (probeService == null)
                throw new ArgumentNullException(nameof(probeService));
            ProbeService = probeService;
        }

        private IProbeService ProbeService { get; }

        //[HttpPost("search-company")]
        //public async Task<IActionResult> SearchCompany([FromBody] GetSearchRequest searchRequest)
        //{
        //    return await ExecuteAsync(async () => Ok(await ProbeService.SearchCompany(searchRequest)));

        //}
        //[HttpPost("search-authorized-signatory")]
        //public async Task<IActionResult> SearchAuthorizedSignatory([FromBody]GetSearchRequest searchRequest)
        //{
        //    return await ExecuteAsync(async () => Ok(await ProbeService.SearchAuthorizedSignatory(searchRequest)));

        //}

        //[HttpGet("company-detail/{entitytype}/{entityid}/{cin}")]
        //public async Task<IActionResult> GetCompanyDetail(string entityid,string entitytype,string cin)
        //{
        //    return await ExecuteAsync(async () => Ok(await ProbeService.GetCompanyDetail(new GetCompanyDetailRequest()
        //    {
        //        EntityType = entitytype,
        //        EntityId = entityid,
        //        Cin = cin
        //    } )));

        //}

        //[HttpGet("authorisedsignatory-detail/{entitytype}/{entityid}/{cin}")]
        //public async Task<IActionResult> GetAuthorisedSignatoryDetail(string entityid, string entitytype, string cin)
        //{
        //    return await ExecuteAsync(async () => Ok(await ProbeService.GetAuthorisedSignatoryDetail(new GetCompanyDetailRequest()
        //    {
        //        EntityType = entitytype,
        //        EntityId = entityid,
        //        Cin = cin
        //    })));

        //}

        //[HttpGet("charges-detail/{entitytype}/{entityid}/{cin}")]
        //public async Task<IActionResult> GetChargesDetail(string entityid, string entitytype, string cin)
        //{
        //    return await ExecuteAsync(async () => Ok(await ProbeService.GetChargesDetail(new GetCompanyDetailRequest()
        //    {
        //        EntityType = entitytype,
        //        EntityId = entityid,
        //        Cin = cin
        //    })));

        //}

        //[HttpGet("financial-data-status/{entitytype}/{entityid}/{cin}")]
        //public async Task<IActionResult> GetFinancialDataStatus(string entityid, string entitytype, string cin)
        //{
        //    return await ExecuteAsync(async () => Ok(await ProbeService.GetFinancialDataStatus(new GetCompanyDetailRequest()
        //    {
        //        EntityType = entitytype,
        //        EntityId = entityid,
        //        Cin = cin
        //    })));

        //}

        [HttpGet("{entitytype}/{entityid}/{cin}/finance")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.Probe.Response.IGetFinancialDetailResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetFinancialDetail(string entityType, string entityId,string cin)
        {
            Logger.Info($"[Probe] GetFinancialDetail endpoint invoked for [{entityType}] with id #{entityId} with cin #{cin}");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await ProbeService.GetFinancialDetail(entityType, entityId, cin));
                }
                catch (ProbeException exception)
                {
                    Logger.Error($"[Probe] Error occured when GetFinancialDetail endpoint was called for [{entityType}] with id #{entityId} with cin #{cin}", exception);
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
    }
}
