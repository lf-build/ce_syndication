﻿using System.Net;

namespace CreditExchange.Syndication.TowerData
{
    public interface IIPAddressRequest
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        IPAddress IpAddress { get; set; }
    }
}