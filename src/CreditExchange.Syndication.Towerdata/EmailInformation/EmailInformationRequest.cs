﻿namespace CreditExchange.Syndication.TowerData
{
    public class EmailInformationRequest : IEmailInformationRequest
    {
        public string Email { get; set; }
        public string EntityId { get; set; }
        public string EntityType { get; set; }
    }
}
