﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using CreditExchange.Syndication.TowerData.Proxy.EmailInformation;

namespace CreditExchange.Syndication.TowerData
{
    public class EmailInformation : IEmailInformation
    {
        public EmailInformation()
        {
            
        }

        internal EmailInformation(EmailInformationProxyResponse emailInformationProxyResponse)
        {
            Demographics=new Demographics();
            Household=new Household();
            Interests=new List<string>();

            Demographics.AgeRange = emailInformationProxyResponse.Age;
            Demographics.Education = emailInformationProxyResponse.Education;

            switch (emailInformationProxyResponse.Gender.ToLower())
            {
                case "male":
                    Demographics.Gender = Gender.Male;
                    break;
                case "female":
                    Demographics.Gender = Gender.Female;
                    break;
                default:
                    throw new InvalidEnumArgumentException($"Invalid value {emailInformationProxyResponse.Gender} for Geneder");
            }
            
            Demographics.NetWorthRange = emailInformationProxyResponse.NetWorth;
            Demographics.Zip = emailInformationProxyResponse.Zip;

            Household.HomeMarketValue = emailInformationProxyResponse.HomeMarketValue;
            Household.HomeOwnerStatus = emailInformationProxyResponse.HomeOwnerStatus;
            Household.HouseholdIncome = emailInformationProxyResponse.HouseholdIncome;
            Household.LengthOfResidence = emailInformationProxyResponse.LengthOfResidence;
            Household.MaritalStatus = emailInformationProxyResponse.MaritalStatus;
            Household.Occupation = emailInformationProxyResponse.Occupation;
            Household.PresenceOfChildren = emailInformationProxyResponse.PresenceOfChildren;

            if (emailInformationProxyResponse.Interests != null)
                Interests.AddRange(
                    emailInformationProxyResponse.Interests.Where(interest => interest.Value).Select(interest => interest.Key));

            EmailActivityMetrics=new EmailActivityMetrics(emailInformationProxyResponse.EmailActivityMetricsResponse);
        }

        public IDemographics Demographics { get; set; }
        public IHousehold Household { get; set; }
        public List<string> Interests { get; set; }
        public IEmailActivityMetrics EmailActivityMetrics { get; set; }
    }
}