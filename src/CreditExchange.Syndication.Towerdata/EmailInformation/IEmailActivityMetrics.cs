using System;

namespace CreditExchange.Syndication.TowerData
{
    public interface IEmailActivityMetrics
    {
        ///<summary>
        /// The date (YYYY-MM-DD) that TowerData first encountered the email address.
        ///</summary>
        DateTime? DateFirstSeen { get; set; }

        /// <summary>
        /// A score (0-3) describing when TowerData first encountered the email address.
        /// 0	TowerData has not encountered this email address before.
        /// 1	TowerData first encountered this email within the last month.
        /// 2	TowerData first encountered this email within the last year.
        /// 3	TowerData first encountered this email over a year ago.
        /// </summary>      
        int Longevity { get; set; }

        /// <summary>
        /// A score (0-10) reflecting the activity of the email over the last 3 months as viewed by TowerData.
        /// </summary>      
        int Velocity { get; set; }

        /// <summary>
        /// A score (0-10) reflecting the popularity of the email as viewed by TowerData in the past 12 months.
        /// </summary>      
        int Popularity { get; set; }

        ///<summary>
        /// The month (YYYY-MM) that TowerData last detected an open by the email address.
        /// </summary>
        string MonthLastOpen { get; set; }
    }
}