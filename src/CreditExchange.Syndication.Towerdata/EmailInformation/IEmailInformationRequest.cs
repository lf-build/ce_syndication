﻿namespace CreditExchange.Syndication.TowerData
{
    public interface IEmailInformationRequest
    {
        string Email { get; set; }
        string EntityId { get; set; }
        string EntityType { get; set; }
    }
}