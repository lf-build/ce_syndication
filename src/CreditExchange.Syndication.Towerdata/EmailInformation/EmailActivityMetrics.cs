﻿using System;
using System.Globalization;
using CreditExchange.Syndication.TowerData.Proxy.EmailInformation;

namespace CreditExchange.Syndication.TowerData
{
    public class EmailActivityMetrics : IEmailActivityMetrics
    {
        public EmailActivityMetrics()
        {
            
        }

        internal EmailActivityMetrics(EmailActivityMetricsProxyResponse emailActivityMetricsResponse)
        {
            if (emailActivityMetricsResponse == null)
                throw new ArgumentNullException(nameof(emailActivityMetricsResponse));

            if (!string.IsNullOrWhiteSpace(emailActivityMetricsResponse.DateFirstSeen) &&
                !string.Equals(emailActivityMetricsResponse.DateFirstSeen, "now", StringComparison.OrdinalIgnoreCase))
            {
                DateFirstSeen = DateTime.ParseExact(emailActivityMetricsResponse.DateFirstSeen, "yyyy-MM-dd",
                    CultureInfo.InvariantCulture, DateTimeStyles.None);
            }

            Longevity = emailActivityMetricsResponse.Longevity;
            Velocity = emailActivityMetricsResponse.Velocity;
            Popularity = emailActivityMetricsResponse.Popularity;
            MonthLastOpen = emailActivityMetricsResponse.MonthLastOpen;
        }

        ///<summary>
        /// The date (YYYY-MM-DD) that TowerData first encountered the email address.
        ///</summary>
        public DateTime? DateFirstSeen { get; set; }

        /// <summary>
        /// A score (0-3) describing when TowerData first encountered the email address.
        /// 0	TowerData has not encountered this email address before.
        /// 1	TowerData first encountered this email within the last month.
        /// 2	TowerData first encountered this email within the last year.
        /// 3	TowerData first encountered this email over a year ago.
        /// </summary>      
        public int Longevity { get; set; }

        /// <summary>
        /// A score (0-10) reflecting the activity of the email over the last 3 months as viewed by TowerData.
        /// </summary>      
        public int Velocity { get; set; }

        /// <summary>
        /// A score (0-10) reflecting the popularity of the email as viewed by TowerData in the past 12 months.
        /// </summary>      
        public int Popularity { get; set; }

        ///<summary>
        /// The month (YYYY-MM) that TowerData last detected an open by the email address.
        /// </summary>
        //TODO: Do we need DateTime with 1st date? or string will be fine?
        public string MonthLastOpen { get; set; }

    }
}