﻿namespace CreditExchange.Syndication.TowerData
{
    public interface IHousehold
    {
        /// <summary>
        /// Market value of person’s home. In ranges of $25K and $50K increments.
        /// 1k-25k; 25k-50k; 50k-75k; 75k-100k; 100k-150k;
        /// 150k-200k; 200k-250k; 250k-300k; 300k-350k; 350k-500k; 500k-1mm; 1mm+
        /// </summary>
        string HomeMarketValue { get; set; }

        /// <summary>
        /// Whether the person owns or rents their home.
        /// Example : Own; Rent
        /// </summary>
        string HomeOwnerStatus { get; set; }

        /// <summary>
        /// Income of household by range
        /// Example
        /// 0-15k; 15k-25k; 25k-35k; 35k-50k; 50k-75k;
        /// 75k-100k; 100k-125k; 125k-150k; 150k-175k;
        /// 175k-200k; 200k-250k; 250k+
        /// </summary>
        string HouseholdIncome { get; set; }

        /// <summary>
        /// Number of years spent in the current residence.
        /// Reported as number; not range.
        /// /// </summary>
        string LengthOfResidence { get; set; }

        /// <summary>
        /// Marital status
        /// Example : Single; Married
        /// </summary>
        string MaritalStatus { get; set; }

        /// <summary>
        /// Occupation
        /// Blue Collar Worker; Business Owner; Civil Service;
        /// Technology; Executive/Upper Management; Health
        /// Services; Homemaker; Middle Management; Military
        /// Personnel; Nurse; Part Time; Professional; Retired;
        /// Secretary; Student; Teacher; White Collar Worker
        /// </summary>
        string Occupation { get; set; }

        /// <summary>
        /// Yes; No
        /// </summary>
        string PresenceOfChildren { get; set; }
    }
}