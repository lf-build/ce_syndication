﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.TowerData
{
    public interface IEmailInformation
    {
        IDemographics Demographics { get; set; }
        IHousehold Household { get; set; }
        List<string> Interests { get; set; }
        IEmailActivityMetrics EmailActivityMetrics { get; set; }
    }
}