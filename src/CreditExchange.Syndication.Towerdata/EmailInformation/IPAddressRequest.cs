﻿using System.Net;

namespace CreditExchange.Syndication.TowerData
{
    public class IPAddressRequest : IIPAddressRequest
    {
        public IPAddress IpAddress { get; set; }

        public string EntityId { get; set; }
        public string EntityType { get; set; }

    }
}
