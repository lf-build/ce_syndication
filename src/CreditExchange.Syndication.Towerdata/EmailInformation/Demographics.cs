﻿namespace CreditExchange.Syndication.TowerData
{
    public class Demographics : IDemographics
    {
        /// <summary>
        /// Age Range Example : 18-20; 21-24; 25-34; 35-44; 45-54; 55-64; 65+
        /// </summary>
        public string AgeRange { get; set; }

        /// <summary>
        /// Indicates the highest known level of education the person has completed.
        /// Example : Completed High School; Attended College;
        /// Completed College; Completed Graduate School;
        /// Attended Vocational/Technical
        /// </summary>
        public string Education { get; set; }

        /// <summary>
        /// Male; Female
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// Indicates the net worth of the household.
        /// Example : 0-5k, 5k-10k, 10k-25k, 25k-50k, 50k-100k, 100k-250k, 
        /// 250k-500k, 500k-750k, 750k-1mm, 1mm+
        /// </summary>
        public string NetWorthRange { get; set; }

        /// <summary>
        /// Location-based data by zip code
        /// </summary>
        public string Zip { get; set; }
    }
}