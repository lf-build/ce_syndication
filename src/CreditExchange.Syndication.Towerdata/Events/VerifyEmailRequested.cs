﻿using LendFoundry.SyndicationStore.Events;

namespace CreditExchange.Syndication.TowerData.Events
{
    public class VerifyEmailRequested : SyndicationCalledEvent
    {
    }
}
