﻿namespace CreditExchange.Syndication.TowerData
{
    public class IpVerificationResponse : IIpVerificationResponse
    {

        public IpVerificationResponse(Proxy.Verification.IpVerificationProxyResponse ipVerificationProxyResponse)
        {
            StatusCode = ipVerificationProxyResponse.StatusCode;
            StatusDescription = ipVerificationProxyResponse.StatusDesc;
            IpAddress = ipVerificationProxyResponse.IpAddress;
            CountryCode = ipVerificationProxyResponse.CountryCode;
            CountryName = ipVerificationProxyResponse.CountryName;
            Region = ipVerificationProxyResponse.Region;
            City = ipVerificationProxyResponse.City;
            Isp = ipVerificationProxyResponse.Isp;
            Domain = ipVerificationProxyResponse.Domain;
        }

        /// <summary>
        /// Status Code Status Description
        /// 5   Timeout
        /// 10  IP found
        /// 15  IP not found
        /// 100 Invalid IP address
        /// 999 Internal error
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// Description of status
        /// </summary>
        public string StatusDescription { get; set; }

        /// <summary>
        /// IP address
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// Two character ISO 3166 country code
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Name of country from ISO 3166
        /// </summary>
        public string CountryName { get; set; }

        /// <summary>
        /// Region or state name
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// Name of city
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Internet Service Provider or company name
        /// </summary>
        public string Isp { get; set; }

        /// <summary>
        /// Domain name
        /// </summary>
        public string Domain { get; set; }
    }
}