﻿namespace CreditExchange.Syndication.TowerData
{
    public interface IIpVerificationResponse
    {
        /// <summary>
        /// Status Code Status Description
        /// 5   Timeout
        /// 10  IP found
        /// 15  IP not found
        /// 100 Invalid IP address
        /// 999 Internal error
        /// </summary>
        int StatusCode { get; set; }

        /// <summary>
        /// Description of status
        /// </summary>
        string StatusDescription { get; set; }

        /// <summary>
        /// IP address
        /// </summary>
        string IpAddress { get; set; }

        /// <summary>
        /// Two character ISO 3166 country code
        /// </summary>
        string CountryCode { get; set; }

        /// <summary>
        /// Name of country from ISO 3166
        /// </summary>
        string CountryName { get; set; }

        /// <summary>
        /// Region or state name
        /// </summary>
        string Region { get; set; }

        /// <summary>
        /// Name of city
        /// </summary>
        string City { get; set; }

        /// <summary>
        /// Internet Service Provider or company name
        /// </summary>
        string Isp { get; set; }

        /// <summary>
        /// Domain name
        /// </summary>
        string Domain { get; set; }
    }
}