﻿namespace CreditExchange.Syndication.TowerData
{
    public interface ITowerDataConfiguration
    {
        string EmailPersonalizationApiKey { get; set; }
        string EmailPersonalizationApiBaseUrl { get; set; }
        string VerificationApiKey { get; set; }
        string VerificationApiBaseUrl { get; set; }
        int ExpirationInDays { get; set; }
    }
}