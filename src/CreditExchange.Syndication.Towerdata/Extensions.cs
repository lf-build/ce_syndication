﻿using CreditExchange.Syndication.TowerData.Proxy;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace CreditExchange.Syndication.TowerData
{
    public static class Extensions
    {
        public static void AddTowerDataService(IServiceCollection services)
        {
            services.AddTransient<ITowerDataService, TowerDataService>();
            services.AddTransient<ITowerDataServiceProxy, TowerDataServiceProxy>();
        }
    }
}
