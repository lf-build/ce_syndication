﻿using System;
using CreditExchange.Syndication.TowerData.Proxy;
using System.Threading.Tasks;
using CreditExchange.Syndication.TowerData.Events;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using System.Linq;

namespace CreditExchange.Syndication.TowerData
{
    public class TowerDataService : ITowerDataService
    {
        public TowerDataService(ITowerDataServiceProxy towerDataServiceProxy, IEventHubClient eventHubClient, ILookupService lookup)
        {
            if (towerDataServiceProxy == null)
                throw new ArgumentNullException(nameof(towerDataServiceProxy));
            EventHub = eventHubClient;
            TowerDataServiceProxy = towerDataServiceProxy;
            Lookup = lookup;
        }
        public static string ServiceName { get; } = "towerdata";
        private ITowerDataServiceProxy TowerDataServiceProxy { get; }
    
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }
        public async Task<IEmailInformation> GetEmailInformation(string entityType, string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentException("Email cannot be null or whitespace.", nameof(email));
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentNullException(nameof(entityType));
            entityType = EnsureEntityType(entityType);
            var response = await TowerDataServiceProxy.GetEmailInformation(email);
            var referencenumber = Guid.NewGuid().ToString("N");
            await EventHub.Publish(new EmailInformationRequested()
            {
                EntityType = entityType,
                Response = response,
                Request = email,
                ReferenceNumber = referencenumber,
                Name = ServiceName
            });
            return new EmailInformation(response);
        }

        public async Task<IEmailVerificationResponse> VerifyEmail(string entityType, string email)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(email))
                    throw new ArgumentException("Email cannot be null or whitespace.", nameof(email));
                if (string.IsNullOrEmpty(entityType))
                    throw new ArgumentNullException(nameof(entityType));
                entityType = EnsureEntityType(entityType);
                var emailResponse = await TowerDataServiceProxy.VerifyEmail(email);
                var referencenumber = Guid.NewGuid().ToString("N");
                await EventHub.Publish(new VerifyEmailRequested()
                {
                    EntityType = entityType,
                    Response = emailResponse,
                    Request = email,
                    ReferenceNumber = referencenumber,
                    Name = ServiceName
                });
                return new EmailVerificationResponse(emailResponse);
            }
            catch (TowerDataException exception)
            {
                await EventHub.Publish(new VerifyEmailRequestedFail()
                {
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = email,
                    ReferenceNumber = null,
                    Name = ServiceName
                });
                throw new TowerDataException(exception.Message);
            }

        }

        public async Task<IIpVerificationResponse> VerifyIpAddress(string entityType, string ipAddress)
        {
            if (ipAddress == null)
                throw new ArgumentNullException(nameof(ipAddress));
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentNullException(nameof(entityType));
            entityType = EnsureEntityType(entityType);
            return new IpVerificationResponse(await TowerDataServiceProxy.VerifyIpAddress(ipAddress));
        }
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}