using CreditExchange.Syndication.TowerData.Proxy.EmailInformation;
using CreditExchange.Syndication.TowerData.Proxy.Verification;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.TowerData.Proxy
{
    public interface ITowerDataServiceProxy
    {
        Task<EmailInformationProxyResponse> GetEmailInformation(string email);
        Task<EmailValidationProxyResponse> VerifyEmail(string email);
        Task<IpVerificationProxyResponse> VerifyIpAddress(string ip);
    }
}