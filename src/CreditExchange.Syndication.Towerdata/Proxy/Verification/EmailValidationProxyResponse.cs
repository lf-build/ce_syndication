using Newtonsoft.Json;

namespace CreditExchange.Syndication.TowerData.Proxy.Verification
{
    public class EmailValidationProxyResponse
    {
        [JsonProperty(PropertyName = "ok")]
        public bool? Ok { get; set; }

        [JsonProperty(PropertyName = "status_code")]
        public int StatusCode { get; set; }

        [JsonProperty(PropertyName = "status_desc")]
        public string StatusDesc { get; set; }

        [JsonProperty(PropertyName = "address")]
        public string EmailAddress { get; set; }

        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }

        [JsonProperty(PropertyName = "domain")]
        public string Domain { get; set; }

        [JsonProperty(PropertyName = "validation_level")]
        public int ValidationLevel { get; set; }

        [JsonProperty(PropertyName = "domain_type")]
        public string DomainType { get; set; }

        [JsonProperty(PropertyName = "role")]
        public bool Role { get; set; }

        //TODO:corrections

    }
}