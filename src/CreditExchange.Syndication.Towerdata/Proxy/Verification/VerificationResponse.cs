using Newtonsoft.Json;

namespace CreditExchange.Syndication.TowerData.Proxy.Verification
{
    public class VerificationProxyResponse
    {
        [JsonProperty(PropertyName = "email")]
        public EmailValidationProxyResponse EmailValidationProxyResponse { get; set; }

        [JsonProperty(PropertyName = "ip")]
        public IpVerificationProxyResponse IpVerificationProxyResponse{ get; set; }
    }
}