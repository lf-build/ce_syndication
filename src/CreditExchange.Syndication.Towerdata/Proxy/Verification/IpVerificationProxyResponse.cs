﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.TowerData.Proxy.Verification
{
    public class IpVerificationProxyResponse
    {
        [JsonProperty(PropertyName = "ok")]
        public bool? Ok { get; set; }

        [JsonProperty(PropertyName = "status_code")]
        public int StatusCode { get; set; }

        [JsonProperty(PropertyName = "status_desc")]
        public string StatusDesc { get; set; }

        [JsonProperty(PropertyName = "address")]
        public string IpAddress { get; set; }

        [JsonProperty(PropertyName = "country_code")]
        public string CountryCode { get; set; }

        [JsonProperty(PropertyName = "country_name")]
        public string CountryName { get; set; }

        [JsonProperty(PropertyName = "region")]
        public string Region { get; set; }

        [JsonProperty(PropertyName = "city")]
        public string City { get; set; }

        [JsonProperty(PropertyName = "ISP")]
        public string Isp { get; set; }

        [JsonProperty(PropertyName = "domain")]
        public string Domain { get; set; }
    }
}