﻿using CreditExchange.Syndication.TowerData.Proxy.EmailInformation;
using CreditExchange.Syndication.TowerData.Proxy.Verification;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.TowerData.Proxy
{
    public class TowerDataServiceProxy : ITowerDataServiceProxy
    {
        public TowerDataServiceProxy(ITowerDataConfiguration towerDataConfiguration)
        {
            if (towerDataConfiguration == null)
                throw new ArgumentNullException(nameof(towerDataConfiguration));

            if (string.IsNullOrWhiteSpace(towerDataConfiguration.EmailPersonalizationApiKey))
                throw new ArgumentException("EmailPersonalization Api Key is required", nameof(towerDataConfiguration.EmailPersonalizationApiKey));

            if (string.IsNullOrWhiteSpace(towerDataConfiguration.EmailPersonalizationApiBaseUrl))
                throw new ArgumentException("EmailPersonalization Api BaseUrl is required", nameof(towerDataConfiguration.EmailPersonalizationApiBaseUrl));

            if (string.IsNullOrWhiteSpace(towerDataConfiguration.VerificationApiKey))
                throw new ArgumentException("Verification Api Key is required", nameof(towerDataConfiguration.VerificationApiKey));

            if (string.IsNullOrWhiteSpace(towerDataConfiguration.VerificationApiBaseUrl))
                throw new ArgumentException("Verification Api BaseUrl is required", nameof(towerDataConfiguration.VerificationApiBaseUrl));

            TowerDataConfiguration = towerDataConfiguration;
        }

        private ITowerDataConfiguration TowerDataConfiguration { get; }

        public async Task<EmailInformationProxyResponse> GetEmailInformation(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentException("Argument is null or whitespace", nameof(email));

            var client = new RestClient(TowerDataConfiguration.EmailPersonalizationApiBaseUrl);
            var request = new RestRequest(Method.GET);
            request.AddQueryParameter("email", email);
            request.AddQueryParameter("api_key", TowerDataConfiguration.EmailPersonalizationApiKey);

            return await Task.Run(()=> ExecuteRequest<EmailInformationProxyResponse>(client, request));
        }

        public async Task<EmailValidationProxyResponse> VerifyEmail(string email)
        {
            if (email == null)
                throw new ArgumentNullException(nameof(email));

            var response = await Task.Run(()=> ExecuteRequest(email, null));
            if (response?.EmailValidationProxyResponse?.Ok == null)
                throw new TowerDataException("Error occured processing request");

            return  response.EmailValidationProxyResponse;
        }

        public async Task<IpVerificationProxyResponse> VerifyIpAddress(string ip)
        {
            if (ip == null)
                throw new ArgumentNullException(nameof(ip));

            var response =await Task.Run(()=> ExecuteRequest(null, ip));
            if (response?.IpVerificationProxyResponse?.Ok == null)
                throw new TowerDataException("Error occured processing request");

            return response.IpVerificationProxyResponse;
        }

        private VerificationProxyResponse ExecuteRequest(string email, string ip)
        {
            if (string.IsNullOrWhiteSpace(email) && string.IsNullOrWhiteSpace(ip))
                throw new ArgumentException("Email or IP is required");

            var client = new RestClient(TowerDataConfiguration.VerificationApiBaseUrl);
            var request = new RestRequest(Method.GET);

            if (!string.IsNullOrWhiteSpace(email))
                request.AddQueryParameter("email", email);

            if (!string.IsNullOrWhiteSpace(ip))
                request.AddQueryParameter("ip", ip);

            request.AddQueryParameter("license", TowerDataConfiguration.VerificationApiKey);

            return ExecuteRequest<VerificationProxyResponse>(client, request);
        }

        private static T ExecuteRequest<T>(IRestClient client, IRestRequest request)
        {
            var response = client.Execute(request);

            if (response.ErrorException != null)
                throw new TowerDataException("Exception occured while executing request", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK)
                throw new TowerDataException(response.Content);

            return JsonConvert.DeserializeObject<T>(response.Content);

        }
    }
}