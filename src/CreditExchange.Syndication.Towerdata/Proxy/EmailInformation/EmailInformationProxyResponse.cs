using System.Collections.Generic;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.TowerData.Proxy.EmailInformation
{
    public class EmailInformationProxyResponse
    {
        #region Demographics

        [JsonProperty(PropertyName = "age")]
        public string Age { get; set; }

        [JsonProperty(PropertyName = "gender")]
        public string Gender { get; set; }

        [JsonProperty(PropertyName = "zip")]
        public string Zip { get; set; }

        [JsonProperty(PropertyName = "education")]
        public string Education { get; set; }

        [JsonProperty(PropertyName = "net_worth")]
        public string NetWorth { get; set; }

        #endregion

        #region Household

        [JsonProperty(PropertyName = "home_market_value")]
        public string HomeMarketValue { get; set; }

        [JsonProperty(PropertyName = "home_owner_status")]
        public string HomeOwnerStatus { get; set; }

        [JsonProperty(PropertyName = "household_income")]
        public string HouseholdIncome { get; set; }

        [JsonProperty(PropertyName = "length_of_residence")]
        public string LengthOfResidence { get; set; }

        [JsonProperty(PropertyName = "marital_status")]
        public string MaritalStatus { get; set; }

        [JsonProperty(PropertyName = "occupation")]
        public string Occupation { get; set; }
        
        [JsonProperty(PropertyName = "presence_of_children")]
        public string PresenceOfChildren { get; set; }

        #endregion  

        [JsonProperty(PropertyName = "interests")]
        public Dictionary<string,bool> Interests { get; set; }

        [JsonProperty(PropertyName = "eam")]
        public EmailActivityMetricsProxyResponse EmailActivityMetricsResponse
        {
            get;
            set;
        }

        //TODO: Purchase
    }
}