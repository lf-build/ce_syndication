using System.Net;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.TowerData
{
    public interface ITowerDataService
    {
        Task<IEmailInformation> GetEmailInformation(string entityType, string email);
        Task<IEmailVerificationResponse> VerifyEmail(string entityType, string email);
        Task<IIpVerificationResponse> VerifyIpAddress(string entityType, string ipAddress);

    }
}