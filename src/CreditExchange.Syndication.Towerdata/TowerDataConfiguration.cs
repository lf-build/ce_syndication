﻿namespace CreditExchange.Syndication.TowerData
{
    public class TowerDataConfiguration : ITowerDataConfiguration
    {
        public string EmailPersonalizationApiKey { get; set; }
        public string EmailPersonalizationApiBaseUrl { get; set; } = "https://api.towerdata.com/v5/td";
        public string VerificationApiKey { get; set; }
        public string VerificationApiBaseUrl { get; set; } = "http://api10.towerdata.com/person";

        public int ExpirationInDays { get; set; }
    }
}