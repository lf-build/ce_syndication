using CreditExchange.Syndication.TowerData.Proxy.Verification;

namespace CreditExchange.Syndication.TowerData
{
    public class EmailVerificationResponse : IEmailVerificationResponse
    {
        public EmailVerificationResponse()
        {
            
        }

        public EmailVerificationResponse(EmailValidationProxyResponse emailValidationProxyResponse)
        {
            StatusCode = emailValidationProxyResponse.StatusCode;
            StatusDescription = emailValidationProxyResponse.StatusDesc;
            EmailAddress = emailValidationProxyResponse.EmailAddress;
            Username = emailValidationProxyResponse.Username;
            Domain = emailValidationProxyResponse.Domain;
            ValidationLevel = emailValidationProxyResponse.ValidationLevel;
            DomainType = emailValidationProxyResponse.DomainType;
            Role = emailValidationProxyResponse.Role;
            IsValid = emailValidationProxyResponse.Ok.GetValueOrDefault();
        }

        /// <summary>
        /// StatusCode Status Description
        /// 5 Timeout
        /// 10 Syntax OK
        /// 20 Syntax OK and domain valid according to the domain database
        /// 30 Syntax OK and domain exists
        /// 40 Syntax OK, domain exists, and domain can receive email
        /// 45 Domain does not support validation (accepts all mailboxes)
        /// 50 Syntax OK, domain exists, and mailbox does not reject mail
        /// 100 General syntax error
        /// 110 Invalid character in address
        /// 115 Invalid domain syntax
        /// 120 Invalid username syntax
        /// 125 Invalid username syntax for that domain
        /// 130 Address is too long
        /// 135 Address has unbalanced parentheses, brackets, or quotes
        /// 140 Address does not have a username
        /// 145 Address does not have a domain
        /// 150 Address does not have an @ sign
        /// 155 Address has more than one @ sign
        /// 200 Invalid top-level-domain (TLD) in address
        /// 205 IP address not allowed as a domain
        /// 210 Comments are not allowed in email addresses
        /// 215 Unquoted spaces are not allowed in email addresses
        /// 310 Domain does not exist
        /// 315 Domain does not have a valid IP address
        /// 325 Domain cannot receive email
        /// 400 The mailbox is invalid or the username does not exist at the domain
        /// 410 Mailbox is full and can not receive email at this time
        /// 420 Mail is not accepted for this domain
        /// 500 Addresses with that username are not allowed
        /// 505 Addresses with that domain are not allowed
        /// 510 Address is a bot or other suppression
        /// 512 Address is a role-based account
        /// 520 Address is a known bouncer
        /// 525 Address is a spamtrap or known complainer
        /// 530 Address has opted out from commercial email
        /// 999 Internal error
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// Description of status
        /// </summary>
        public string StatusDescription { get; set; }

        /// <summary>
        /// The email address
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// The portion of the email address to the left of the @
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// The portion of the email address to the right of the @
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// The actual level of validation performed on the email address.
        /// 0 = Timeout occurred
        /// 2 = Syntax and domain checked against database
        /// 3 = Syntax and domain existence confirmed in real-time
        /// 4 = Syntax and domain receives email confirmed in real-time
        /// 5 = Syntax, domain, and mailbox checked
        /// </summary>
        public int ValidationLevel { get; set; }

        /// <summary>
        ///A classification of the type of domain used in the email address.
        ///�disposable� = The domain belongs to a disposable or temporary
        ///email service. You may wish to block emails of this type.
        ///�wireless� = The email is for a wireless service and the FCC
        ///prohibits sending unsolicited emails to these domains.
        ///�null� = The type is unknown.
        /// </summary>
        public string DomainType { get; set; }

        /// <summary>
        /// A true/false flag indicating whether the email address is a rolebased
        /// account, such as sales@ or info@.
        /// </summary>
        public bool Role { get; set; }

        /// <summary>
        /// Email Address is valid
        /// </summary>
        public bool IsValid { get; set; }
    }
}