﻿using System;
using System.Runtime.Serialization;

namespace CreditExchange.Syndication.TowerData
{
    [Serializable]
    public class TowerDataException : Exception
    {
        public TowerDataException()
        {
        }

        public TowerDataException(string message) : base(message)
        {
        }

        public TowerDataException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected TowerDataException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}