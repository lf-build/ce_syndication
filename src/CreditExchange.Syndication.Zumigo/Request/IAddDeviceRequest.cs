﻿
namespace CreditExchange.Syndication.Zumigo.Request
{
    public interface IAddDeviceRequest :ISource
    {
        string MobileDeviceNumber { get; set; }
      
        string OptlnId { get; set; }
        string OptInTimeStamp { get; set; }
    }
}
