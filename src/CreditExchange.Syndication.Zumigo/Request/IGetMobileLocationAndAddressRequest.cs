﻿namespace CreditExchange.Syndication.Zumigo.Request
{
    public interface IGetMobileLocationAndAddressRequest
    {
        string MobileDeviceNumber { get; }
    }
}
