﻿namespace CreditExchange.Syndication.Zumigo.Request
{
    public class GetDeviceStatusRequest : IGetDeviceStatusRequest
    {
        public string MobileDeviceNumber { get; set; }
    }
}
