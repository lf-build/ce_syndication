﻿namespace CreditExchange.Syndication.Zumigo.Request
{
    public class Source : ISource
    {
        public string EntityId { get;set;}
        public string EntityType { get; set; }
     
    }
}