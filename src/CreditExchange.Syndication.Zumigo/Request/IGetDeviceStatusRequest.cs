﻿namespace CreditExchange.Syndication.Zumigo.Request
{
    public interface IGetDeviceStatusRequest
    {
        string MobileDeviceNumber { get; }
    }
}
