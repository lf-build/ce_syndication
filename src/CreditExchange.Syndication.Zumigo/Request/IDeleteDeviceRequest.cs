﻿namespace CreditExchange.Syndication.Zumigo.Request
{
    public interface IDeleteDeviceRequest
    {
        string MobileDeviceNumber { get; set; }
    }
}
