﻿namespace CreditExchange.Syndication.Zumigo.Request
{
  public interface ISource
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
    }
}
