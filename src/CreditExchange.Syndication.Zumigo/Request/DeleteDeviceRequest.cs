﻿
namespace CreditExchange.Syndication.Zumigo.Request
{
    public class DeleteDeviceRequest : IDeleteDeviceRequest
    {
        public string MobileDeviceNumber { get; set; }
    }
}
