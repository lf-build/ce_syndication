﻿namespace CreditExchange.Syndication.Zumigo.Request
{
    public class ValidateOnlineTransactionRequest : IValidateOnlineTransactionRequest
    {
        public ValidateOnlineTransactionRequest()
        {

        }

        public string IPAddress { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Address { get; set; }
        public string WifiAddresses { get; set; }
        public string MobileNumber { get; set; }
        public bool IsRetriable { get; set; }

        public bool IsRetryDistance { get; set; }
    }
}
