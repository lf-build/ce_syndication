﻿
namespace CreditExchange.Syndication.Zumigo.Request
{
    public interface IValidateOnlineTransactionRequest 
    {
        string IPAddress { get; set; }
        string Latitude { get; set; }
        string Longitude { get; set; }
        string Address { get; set; }
        string WifiAddresses { get; set; }
        string MobileNumber { get; set; }
        bool IsRetryDistance { get; set; }
        bool IsRetriable { get; set; }
    }
}
