﻿namespace CreditExchange.Syndication.Zumigo.Request
{
    public class AddDeviceRequest :Source, IAddDeviceRequest
    {
        public string MobileDeviceNumber { get; set; }
        public string OptlnId { get; set; }
        public string OptInTimeStamp { get; set; }
    }
}
