﻿namespace CreditExchange.Syndication.Zumigo.Request
{
    public class GetMobileLocationAndAddressRequest : IGetMobileLocationAndAddressRequest
    {
        public string MobileDeviceNumber { get; set; }
    }
}
