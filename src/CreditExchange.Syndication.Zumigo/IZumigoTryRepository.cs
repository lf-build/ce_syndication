﻿using CreditExchange.Syndication.Zumigo.Request;
using CreditExchange.Syndication.Zumigo.Response;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Zumigo
{
   public interface IZumigoTryRepository : IRepository<IZumigoTryData>
    {
        Task<List<IZumigoTryData>> GetFailedRequest(string entityType, string entityId);
        void AddTryData(string entityType, string entityId, IValidateOnlineTransactionRequest validateOnlineTransactionRequest, IValidateOnlineTransactionResponse response, DateTimeOffset tryDate);
    }
}
