﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Zumigo
{
    public interface IEventConfiguration
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        string Response { get; set; }
        string ReferenceNumber { get; set; }
        string Request { get; set; }
        string EventName { get; set; }
        
    }
}