﻿using LendFoundry.Security.Tokens;

namespace CreditExchange.Syndication.Zumigo
{
    public interface IZumigoRetryRepositoryFactory
    {
        IZumigoRetryRepository Create(ITokenReader reader);
    }
}