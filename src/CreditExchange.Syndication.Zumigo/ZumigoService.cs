﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using CreditExchange.Syndication.Zumigo.Events;
using CreditExchange.Syndication.Zumigo.Proxy;
using CreditExchange.Syndication.Zumigo.Proxy.ProxyRequest;
using CreditExchange.Syndication.Zumigo.Request;
using CreditExchange.Syndication.Zumigo.Response;

using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;

namespace CreditExchange.Syndication.Zumigo
{
    public class ZumigoService : IZumigoService
    {
        private IZumigoProxy ZumigoProxy { get; }
        private static Dictionary<string, ResponseCodes> GenericResponseCodes { get; }
        private static Dictionary<string, ResponseCodes> DeviceSetupResponseCodes { get; }
        private static Dictionary<string, ResponseCodes> ApiSpecificResponseCodes { get; }
        private static Dictionary<string, ResponseCodes> AddDeviceResponseCodes { get; }
        private static Dictionary<string, ResponseCodes> DeleteDeviceResponseCodes { get; }
        private static Dictionary<string, ResponseCodes> DeviceStatusResponseCodes { get; }
      


        static ZumigoService()
        {
            GenericResponseCodes = new Dictionary<string, ResponseCodes>(StringComparer.InvariantCultureIgnoreCase);
            DeviceSetupResponseCodes = new Dictionary<string, ResponseCodes>(StringComparer.InvariantCultureIgnoreCase);
            ApiSpecificResponseCodes = new Dictionary<string, ResponseCodes>(StringComparer.InvariantCultureIgnoreCase);
            AddDeviceResponseCodes = new Dictionary<string, ResponseCodes>(StringComparer.InvariantCultureIgnoreCase);
            DeleteDeviceResponseCodes = new Dictionary<string, ResponseCodes>(StringComparer.InvariantCultureIgnoreCase);
            DeviceStatusResponseCodes = new Dictionary<string, ResponseCodes>(StringComparer.InvariantCultureIgnoreCase);


            AddDeviceResponseCodes.Add("MDN_OPTED_IN", new ResponseCodes { IsSuccess = true, IsRetriable = false, Description = "Received when MDN is already added to the account and the user opted into the request." });
            AddDeviceResponseCodes.Add("PENDING", new ResponseCodes { IsSuccess = false, IsRetriable = true, Description = "Waiting for consent process to be completed." });
            AddDeviceResponseCodes.Add("ALREADY_ADDED", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "Received when MDN is already added to the account and the user opted into the request." });

            DeleteDeviceResponseCodes.Add("DELETED", new ResponseCodes { IsSuccess = true, IsRetriable = false, Description = "Mobile Device Number is deleted." });
            DeleteDeviceResponseCodes.Add("NUMBER_NOT_FOUND", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "The number was not identified to a carrier." });

            DeviceStatusResponseCodes.Add("MDN_OPTED_IN", new ResponseCodes { IsSuccess = true, IsRetriable = false, Description = "Received when MDN is already added to the account and the user opted into the request." });
            DeviceStatusResponseCodes.Add("MDN_OPTED_OUT", new ResponseCodes { IsSuccess = true, IsRetriable = true, Description = "Consumer Opted out during the consent process." });
            DeviceStatusResponseCodes.Add("MDN_OPT_IN_REQ_SENT", new ResponseCodes { IsSuccess = true, IsRetriable = true, Description = "Received if the device is already Pending." });
            DeviceStatusResponseCodes.Add("NUMBER_NOT_FOUND", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "The number was not identified to a carrier." });
            DeviceStatusResponseCodes.Add("CARRIER_NOT_SUPPORTED", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "The carrier of the identified Mobile Device Number is not supported by Zumigo for the requested service." });
            DeviceStatusResponseCodes.Add("CARRIER_NOT_SUPPORTED_FOR_CUSTOMER", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "Received when Mobile Device Number is already added to the account but the user opted_out at a later time." });
            DeviceStatusResponseCodes.Add("OPT_IN_ERROR", new ResponseCodes { IsSuccess = false, IsRetriable = true, Description = "Error occured while user opted into the request." });
            DeviceStatusResponseCodes.Add("LANDLINE_MDN", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "The MDN added is a landline, not mobile device." });

            //System Response Codes
            GenericResponseCodes.Add("AUTH_FAILED", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "Authentication credentials failed." });
            GenericResponseCodes.Add("SYSTEM_ERROR", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "General system failure where response was not possible (Unexpected Exception on server side)." });
            GenericResponseCodes.Add("REQUEST_TIMED_OUT", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "The timeout is configurable to each customer. Default is 30sec. Timeouts occur when carriers are unable to respond to the request within the allotted time." });
            GenericResponseCodes.Add("LOCATION_ERROR", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "Service Exception on server side." });

            //Device Setup Response Codes
            DeviceSetupResponseCodes.Add("OPTIN_DETAILS_INVALID", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "Opt in details were invalid." });
            DeviceSetupResponseCodes.Add("OPT_IN_ERROR", new ResponseCodes { IsSuccess = false, IsRetriable = true, Description = "Error occured while user opted into the request." });
            DeviceSetupResponseCodes.Add("PENDING", new ResponseCodes { IsSuccess = false, IsRetriable = true, Description = "Waiting for consent process to be completed." });
            DeviceSetupResponseCodes.Add("CARRIER_NOT_SUPPORTED_FOR_CUSTOMER", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "The carrier will not support this specific Mobile Device Number for services." });
            DeviceSetupResponseCodes.Add("MDN_NOT_RESOLVED", new ResponseCodes { IsSuccess = false, IsRetriable = true, Description = "The Mobile Device Number is valid but was unable to determine carrier details." });
            DeviceSetupResponseCodes.Add("MDN_OPT_IN_REQ_FAILED", new ResponseCodes { IsSuccess = false, IsRetriable = true, Description = "Failure to send Optin SMS." });
            DeviceSetupResponseCodes.Add("MDN_OPT_IN_REQ_SENT", new ResponseCodes { IsSuccess = true, IsRetriable = true, Description = "Received if the device is already Pending." });
            DeviceSetupResponseCodes.Add("MDN_OPTED_OUT", new ResponseCodes { IsSuccess = true, IsRetriable = true, Description = "Consumer Opted out during the consent process." });
            DeviceSetupResponseCodes.Add("DENIED", new ResponseCodes { IsSuccess = false, IsRetriable = true, Description = "Received when Mobile Device Number is already added to the account but the user opted_out at a later time." });
            DeviceSetupResponseCodes.Add("NO_DEFINED_OWNER", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "No owner is defined for the Mobile Device Number. This could be due to no contract or confidential data." });
            DeviceSetupResponseCodes.Add("SUBID_LOOKUP_ERROR", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "The device subid presented was not valid." });
            DeviceSetupResponseCodes.Add("MDN_NOTVALID", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "The Mobile Device Number submitted is not a valid Mobile Device Number or format." });
            DeviceSetupResponseCodes.Add("LANDLINE_MDN", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "The MDN added is a landline, not mobile device." });
            DeviceSetupResponseCodes.Add("INACTIVE_SUBSCRIBER", new ResponseCodes { IsSuccess = false, IsRetriable = true, Description = "Service is temporarily suspended to this MDN." });
            DeviceSetupResponseCodes.Add("NUMBER_NOT_FOUND", new ResponseCodes { IsSuccess = false,IsRetriable = true, Description = "The number was not identified to a carrier." });
            DeviceSetupResponseCodes.Add("MDN_PORTED", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "The MDN was ported to a new carrier. New consumer consent and AddDevice will be required." });

            //Api Specific Response Codes
            ApiSpecificResponseCodes.Add("MDN_ROAMING", new ResponseCodes { IsSuccess = false, IsRetriable = true, Description = "Roaming outside service area and not able to identify specific location." });
            ApiSpecificResponseCodes.Add("MDN_NOT_REACHABLE", new ResponseCodes { IsSuccess = false, IsRetriable = true, Description = "Device is either turned off or not reachable due to roaming." });
            ApiSpecificResponseCodes.Add("DENIED", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "Location service to this device are blocked by the carrier." });
            ApiSpecificResponseCodes.Add("CARRIER_NOT_SUPPORTED", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "The carrier of the identified MDN is not supported by Zumigo for the requested service." });
            ApiSpecificResponseCodes.Add("NO_DEFINED_OWNER", new ResponseCodes { IsSuccess = false, IsRetriable = false, Description = "No owner is defined for the MDN. This could be due to no contract or confidential data." });
            ApiSpecificResponseCodes.Add("SUBID_LOOKUP_ERROR", new ResponseCodes { IsSuccess = false, IsRetriable = true, Description = "Can not return subscriber ID." });
            ApiSpecificResponseCodes.Add("INVALID_REQUESTID", new ResponseCodes { IsSuccess = false, IsRetriable = true, Description = "Returned for asynchronous calls when requestid is not specified." });
            ApiSpecificResponseCodes.Add("ADDRESS_NOT_RESOLVED", new ResponseCodes { IsSuccess = false, IsRetriable = true, Description = "The input address was not able to be resolved to geo-coordinates." });
            ApiSpecificResponseCodes.Add("IP_NOT_RESOLVED", new ResponseCodes { IsSuccess = false, IsRetriable = true, Description = "Supplied IP address was not resolved to a location or is invalid." });
            ApiSpecificResponseCodes.Add("WIFI_NOT_RESOLVED", new ResponseCodes { IsSuccess = false, IsRetriable = true, Description = "Supplied wifi address(es) was not resolved to a location or is invalid." });
            ApiSpecificResponseCodes.Add("LAT_LONG_INVALID", new ResponseCodes { IsSuccess = false, IsRetriable = true, Description = "Supplied lat/long is invalid." });
        }

        public ZumigoService(IZumigoProxy zumigoProxy, IEventHubClient eventHub, ILookupService lookup, IZumigoTryRepository zumigoTryRepository, IZumigoRetryRepository zumigoRetryRepository , IZumigoConfiguration zumigoConfiguration)
        {
           
            ZumigoProxy = zumigoProxy;
            EventHub = eventHub;
            Lookup = lookup;
            ZumigoTryRepository = zumigoTryRepository;
            ZumigoRetryRepository = zumigoRetryRepository;
            if (zumigoConfiguration == null)
                throw new ArgumentNullException(nameof(zumigoConfiguration));
            ZumigoConfiguration = zumigoConfiguration;
        }

        public static string ServiceName { get; } = "zumigo";
        private IEventHubClient EventHub { get; }

        private ILookupService Lookup { get; }
        private IZumigoTryRepository ZumigoTryRepository { get; }
        private IZumigoRetryRepository ZumigoRetryRepository { get; }
        private IZumigoConfiguration ZumigoConfiguration { get; }

        public async Task<IDeviceAdded> AddDevice(string entitytype, string entityid, string mobileDeviceNumber)
        {
            mobileDeviceNumber = ValidateCountryCode(mobileDeviceNumber);
            if (string.IsNullOrEmpty(entitytype))
                throw new ArgumentException("EntityType is reuire", nameof(entitytype));
            if (string.IsNullOrEmpty(entityid))
                throw new ArgumentException("EntityId is reuire", nameof(entityid));
            entitytype = EnsureEntityType(entitytype);
            var addDeviceRequestProxy = new AddDeviceProxyRequest(mobileDeviceNumber);
            var result = await ZumigoProxy.AddDevice(addDeviceRequestProxy);
            ValidateResponse(result);

            ResponseCodes responseCode;
            ValidateResponse(result, out responseCode, AddDeviceResponseCodes, GenericResponseCodes,
               DeviceSetupResponseCodes, ApiSpecificResponseCodes);

            if (responseCode == null)
                throw new ZumigoException(result);

            var response = new ZumigoDeviceAdded
            {
                ReferenceNumber = Guid.NewGuid().ToString("N"),
                EntityId = entityid,
                EntityType = entitytype,
                Response = result,
                Request = mobileDeviceNumber,
                Name = ServiceName

            };

            await EventHub.Publish(response);
            return response;
        }

        private static void ValidateResponse(string response, out ResponseCodes responseCode, params Dictionary<string, ResponseCodes>[] codesToCheck)
        {
            responseCode = null;
            foreach (var responseCodese in codesToCheck)
            {
                if (!responseCodese.TryGetValue(response, out responseCode))
                    continue;

                if (responseCode.IsSuccess == false)
                {
                    throw new ZumigoException(responseCode.Description);
                }
                   
                break;
            }
        }

        public async Task DeleteDevice(string entitytype, string entityid, string mobileDeviceNumber)
        {
            mobileDeviceNumber = ValidateCountryCode(mobileDeviceNumber);
            if (string.IsNullOrEmpty(entitytype))
                throw new ArgumentException("EntityType is reuire", nameof(entitytype));
            if (string.IsNullOrEmpty(entityid))
                throw new ArgumentException("EntityId is reuire", nameof(entityid));
            entitytype = EnsureEntityType(entitytype);
            ValidateMobileDeviceNumber(mobileDeviceNumber);
            var result = await ZumigoProxy.DeleteDevice(mobileDeviceNumber);
            ResponseCodes responseCode;
            ValidateResponse(result, out responseCode, DeleteDeviceResponseCodes, GenericResponseCodes,
              DeviceSetupResponseCodes, ApiSpecificResponseCodes);

            if (responseCode == null)
                throw new ZumigoException(result);
            var response = new ZumigoDeviceDeleted()
            {
                ReferenceNumber = Guid.NewGuid().ToString("N"),
                EntityId = entityid,
                EntityType = entitytype,
                Response = result,
                Request = mobileDeviceNumber,
                Name = ServiceName
            };
            await EventHub.Publish(response);
        }

        public async Task<IGetDeviceStatusResponse> GetDeviceStatus(string entitytype, string entityid, string mobileDeviceNumber)
        {
            mobileDeviceNumber = ValidateCountryCode(mobileDeviceNumber);
            if (string.IsNullOrEmpty(entitytype))
                throw new ArgumentException("EntityType is reuire", nameof(entitytype));
            if (string.IsNullOrEmpty(entityid))
                throw new ArgumentException("EntityId is reuire", nameof(entityid));
            entitytype = EnsureEntityType(entitytype);
            ValidateMobileDeviceNumber(mobileDeviceNumber);

            var response = await ZumigoProxy.GetDeviceStatus(mobileDeviceNumber);
            ValidateResponse(response);
            ResponseCodes responseCode;
            ValidateResponse(response, out responseCode, DeviceStatusResponseCodes, GenericResponseCodes,
             DeviceSetupResponseCodes, ApiSpecificResponseCodes);

            if (responseCode == null)
                throw new ZumigoException(response);
            var referencenumber = Guid.NewGuid().ToString("N");

            await EventHub.Publish(new ZumigoDeviceStatusRequested()
            {
                ReferenceNumber = referencenumber,
                EntityId = entityid,
                EntityType = entitytype,
                Response = "Ok",
                Request = mobileDeviceNumber,
                Name = ServiceName
            });

            return new GetDeviceStatusResponse()
            {
                Status = (DeviceSuccessCodes)Enum.Parse(typeof(DeviceSuccessCodes), response),
                ReferenceNumber = referencenumber
            };
        }

        public async Task<IGetMobileLocationAndAddressResponse> GetDeviceLocationAddress(string entitytype, string entityid, string mobileDeviceNumber)
        {
            if (string.IsNullOrEmpty(entitytype))
                throw new ArgumentException("EntityType is reuire", nameof(entitytype));
            if (string.IsNullOrEmpty(entityid))
                throw new ArgumentException("EntityId is reuire", nameof(entityid));
            entitytype = EnsureEntityType(entitytype);
            ValidateMobileDeviceNumber(mobileDeviceNumber);
            var response = await ZumigoProxy.GetDeviceLocationAddress(mobileDeviceNumber);
            ValidateMobileLocationAndAddress(response);
            ResponseCodes responseCode;
            ValidateResponse(response, out responseCode, AddDeviceResponseCodes, GenericResponseCodes,
               DeviceSetupResponseCodes, ApiSpecificResponseCodes);
            var referencenumber = Guid.NewGuid().ToString("N");
            var locationAndAddressResponse = new GetMobileLocationAndAddressResponse { ReferenceNumber = referencenumber };
            var strindex = response.IndexOf('"', 0);
            if (strindex != -1)
                locationAndAddressResponse.Address = response.Substring(strindex + 1, response.IndexOf('"', strindex + 1) - strindex);
            response = response.Replace(locationAndAddressResponse.Address, "");
            var responseParameters = response.Split(',').ToList();

            if (responseParameters.Count == 6)
            {
                if (!string.IsNullOrWhiteSpace(responseParameters[0]))
                    locationAndAddressResponse.MobileDeviceNumber = responseParameters[0];

                if (!string.IsNullOrWhiteSpace(responseParameters[1]))
                    locationAndAddressResponse.Latitude = responseParameters[1];

                if (!string.IsNullOrWhiteSpace(responseParameters[2]))
                    locationAndAddressResponse.Longitude = responseParameters[2];

                if (!string.IsNullOrWhiteSpace(responseParameters[4]))
                    locationAndAddressResponse.Accuracy = responseParameters[4];

                if (!string.IsNullOrWhiteSpace(responseParameters[5]))
                {
                    DateTime date;
                    DateTime.TryParse(responseParameters[5], out date);
                    locationAndAddressResponse.Timestamp = new DateTimeOffset(date);
                }
            }

            await EventHub.Publish(new ZumigoDeviceLocationRequested()
            {
                ReferenceNumber = referencenumber,
                EntityId = entityid,
                EntityType = entitytype,
                Response = response,
                Request = mobileDeviceNumber,
                Name = ServiceName
            });
            return locationAndAddressResponse;
        }

        public async Task<IValidateOnlineTransactionResponse> VerifyDeviceLocation(string entityType, string entityId, string mobileDeviceNumber, IValidateOnlineTransactionRequest validateOnlineTransactionRequest)
        {
            mobileDeviceNumber= ValidateCountryCode(mobileDeviceNumber);
            validateOnlineTransactionRequest.MobileNumber = mobileDeviceNumber;
            ResponseCodes responseCode = new ResponseCodes();
            try
            {
                if (string.IsNullOrEmpty(entityType))
                    throw new ArgumentException("EntityType is reuire", nameof(entityType));
                if (string.IsNullOrEmpty(entityId))
                    throw new ArgumentException("EntityId is reuire", nameof(entityId));
                entityType = EnsureEntityType(entityType);
                Validation.ValidateRequest(validateOnlineTransactionRequest);
                var validateOnlineTransactionRequestProxy = new ValidateOnlineTransactionProxyRequest(mobileDeviceNumber, validateOnlineTransactionRequest);

                var response = await ZumigoProxy.VerifyDeviceLocation(validateOnlineTransactionRequestProxy);

                if (response == null)
                    throw new ArgumentNullException(nameof(response));
          
                ValidateResponse(response, out responseCode, AddDeviceResponseCodes, GenericResponseCodes,
                   DeviceSetupResponseCodes, ApiSpecificResponseCodes);
                
                var referencenumber = Guid.NewGuid().ToString("N");
                var locationResponse = new ValidateOnlineTransactionResponse();
                var responseParameters = response.Split(',').ToList();

                if (responseParameters.Count == 5)
                {
                    if (!string.IsNullOrWhiteSpace(responseParameters[0]))
                        locationResponse.DistanceFromIp = responseParameters[0];

                    if (!string.IsNullOrWhiteSpace(responseParameters[1]))
                        locationResponse.DistanceFromLatitudeLongitude = responseParameters[1];

                    if (!string.IsNullOrWhiteSpace(responseParameters[2]))
                        locationResponse.DistanceFromWifi = responseParameters[2];

                    if (!string.IsNullOrWhiteSpace(responseParameters[3]))
                        locationResponse.DistanceFromAddress = responseParameters[3];

                    if (!string.IsNullOrWhiteSpace(responseParameters[4]))
                        locationResponse.AverageDistance = responseParameters[4];
                }
                locationResponse.ReferenceNumber = referencenumber;
                await EventHub.Publish(new ZumigoValidatedOnlineTransaction()
                {
                    ReferenceNumber = referencenumber,
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = locationResponse,
                    Request = validateOnlineTransactionRequest,                   
                    Name = ServiceName
                });
                return locationResponse;
            }
            catch (Exception ex)
            {
                var locationResponse = new ValidateOnlineTransactionResponse();
                locationResponse.ErrorMessage = ex.Message;
                validateOnlineTransactionRequest.IsRetriable = responseCode != null ? responseCode.IsRetriable : false;

                await EventHub.Publish(new ZumigoValidatedOnlineTransactionFail()
                {
                    ReferenceNumber = null,
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = locationResponse,
                    Request = validateOnlineTransactionRequest,
                    Name = ServiceName,

                });
                throw;
            }           
        }
     

        public async Task<List<IZumigoRetryData>> GetRetryRequest()
        {
            var failedReqest = await ZumigoRetryRepository.GetRetryRequest();          
            return failedReqest;
        }
       
        private string ValidateCountryCode(string mobileNumber)
        {
             if((mobileNumber.Length > ZumigoConfiguration.MobileNumberMaxLenght && !mobileNumber.Contains(ZumigoConfiguration.CountryCode)) || mobileNumber.Length < ZumigoConfiguration.MobileNumberMaxLenght)
                throw new ZumigoException("Mobile number Device is not Valid");
             
            else if (mobileNumber.Length == ZumigoConfiguration.MobileNumberMaxLenght)
                mobileNumber = ZumigoConfiguration.CountryCode + mobileNumber;
            return mobileNumber;
        }

        public static void ValidateMobileDeviceNumber(string mobileDeviceNumber)
        {
            if (string.IsNullOrWhiteSpace(mobileDeviceNumber))
                throw new ArgumentException("Mobile Device Number is required", nameof(mobileDeviceNumber));
        }

        private void ValidateResponse(string response)
        {
            if (string.IsNullOrWhiteSpace(response))
                throw new ArgumentNullException(nameof(response));
        }
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
        private static void ValidateMobileLocationAndAddress(string response)
        {
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.Contains(","))
                return;
            response = response.Replace("\n", "");
            ResponseCodes responseCode;

            if (GenericResponseCodes.TryGetValue(response, out responseCode))
            {
                if (responseCode.IsSuccess == false)
                    throw new ZumigoException("Error while getting Location : " + responseCode.Description);
            }
            else if (DeviceSetupResponseCodes.TryGetValue(response, out responseCode))
            {
                if (responseCode.IsSuccess == false)
                    throw new ZumigoException("Error while getting Location : " + responseCode.Description);
            }
            else if (ApiSpecificResponseCodes.TryGetValue(response, out responseCode))
            {
                if (responseCode.IsSuccess == false)
                    throw new ZumigoException("Error while getting Location : " + responseCode.Description);
            }
            else { throw new ZumigoException("Error while getting Location : " + response); }
        }

    }
}
