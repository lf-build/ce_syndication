﻿using System;
using System.ComponentModel;

namespace CreditExchange.Syndication.Zumigo
{
    public enum OptInType
    {
        Whitelist,
        InitiateOptIn
    }

    public enum OptInMethod
    {
        TCO,
        MA,
        One,
        TCP,
        Other
    }

    public enum DeviceSuccessCodes
    {
        MDN_OPTED_IN,
        MDN_OPTED_OUT,
        MDN_OPT_IN_REQ_SENT
    }

    public enum AddDeviceSuccessCodes
    {
        [Description("Received when MDN is already added to the account and the user opted into the request.")]
        MDN_OPTED_IN,
        [Description("Used during the InitiateOptIn request process.The SMS has been sent and user response is pending.")]
        MDN_OPT_IN_REQ_PENDING, //Instead of PENDING we are getting this value
        [Description("Received when MDN is already added to the account and the user opted into the request.")]
        ALREADY_ADDED
    }
    public enum DeviceSetUpErrorCodes
    {

        [Description("Opt in details were invalid.")]
        OPTIN_DETAILS_INVALID,
        [Description("Error occured while user opted into the request.")]
        OPT_IN_ERROR,
        [Description("The carrier will not support this specific Mobile Device Number for services.")]
        CARRIER_NOT_SUPPORTED_FOR_CUSTOMER,
        [Description("The Mobile Device Number is valid but was unable to determine carrier details.")]
        MDN_NOT_RESOLVED,
        [Description("Failure to send Optin SMS.")]
        MDN_OPT_IN_REQ_FAILED,
        [Description("Received if the device is already Pending.")]
        MDN_OPT_IN_REQ_SENT,
        [Description("Consumer Opted out during the consent process.")]
        MDN_OPTED_OUT,
        [Description("Received when Mobile Device Number is already added to the account but the user opted_out at a later time.")]
        DENIED,
        [Description("The carrier of the identified Mobile Device Number is not supported by Zumigo for the requested service.")]
        CARRIER_NOT_SUPPORTED,
        [Description("No owner is defined for the Mobile Device Number. This could be due to no contract or confidential data.")]
        NO_DEFINED_OWNER,
        [Description("The device subid presented was not valid.")]
        SUBID_LOOKUP_ERROR,
        [Description("The Mobile Device Number submitted is not a valid Mobile Device Number or format.")]
        MDN_NOTVALID,
        [Description("The number was not identified to a carrier.")]
        NUMBER_NOT_FOUND,
        [Description("The MDN added is a landline, not mobile device.")]
        LANDLINE_MDN = 15
    }

    public enum SystemResponseCodes
    {
        [Description("Authentication credentials failed.")]
        AUTH_FAILED,
        [Description("General system failure where response was not possible (Unexpected Exception on server side).")]
        SYSTEM_ERROR,
        [Description("The timeout is configurable to each customer. Default is 30sec. Timeouts occur when carriers are unable to respond to the request within the allotted time.")]
        REQUEST_TIMED_OUT,
        [Description("Service Exception on server side.")]
        LOCATION_ERROR
    }

    public enum DeleteDeviceSuccessCodes
    {
        [Description("Mobile Device Number is deleted.")]
        DELETED
    }

    public static class Extension
    {
        public static string GetDescription(this Enum value)
        {
            // variables  
            var enumType = value.GetType();
            var field = enumType.GetField(value.ToString());
            var attributes = field.GetCustomAttributes(typeof(DescriptionAttribute), false);

            // return  
            return attributes.Length == 0 ? value.ToString() : ((DescriptionAttribute)attributes[0]).Description;
        }
    }

    //public static class Validation
    //{
    //    public static void ValidateConfiguration(IZumigoConfiguration zumigoConfiguration)
    //    {
    //        if (zumigoConfiguration == null)
    //            throw new ArgumentNullException(nameof(zumigoConfiguration));

    //        if (zumigoConfiguration.UserName == null)
    //            throw new ArgumentNullException("User name is required", (nameof(zumigoConfiguration.UserName)));

    //        if (string.IsNullOrWhiteSpace(zumigoConfiguration.Password))
    //            throw new ArgumentException("Password is required", nameof(zumigoConfiguration.Password));

    //        if (string.IsNullOrWhiteSpace(zumigoConfiguration.OptInType))
    //            throw new ArgumentException("OptInType is required", nameof(zumigoConfiguration.OptInType));

    //        if (string.IsNullOrWhiteSpace(zumigoConfiguration.AddDeviceUrl))
    //            throw new ArgumentException("Add device url is required", nameof(zumigoConfiguration.AddDeviceUrl));

    //        if (string.IsNullOrWhiteSpace(zumigoConfiguration.AddDeviceUrl))
    //            throw new ArgumentException("Delete device url is required", nameof(zumigoConfiguration.DeleteDeviceUrl));

    //        if (string.IsNullOrWhiteSpace(zumigoConfiguration.GetDeviceStatusUrl))
    //            throw new ArgumentException("Get device status url is required", nameof(zumigoConfiguration.GetDeviceStatusUrl));

    //        if (string.IsNullOrWhiteSpace(zumigoConfiguration.LocationAndAddressUrl))
    //            throw new ArgumentException("LocationAndAddressUrl is required", nameof(zumigoConfiguration.LocationAndAddressUrl));

    //        if (string.IsNullOrWhiteSpace(zumigoConfiguration.ValidateOnlineTransactionUrl))
    //            throw new ArgumentException("ValidateOnlineTransactionUrl is required", nameof(zumigoConfiguration.ValidateOnlineTransactionUrl));
    //    }

    //    public static void ValidateRequest(IAddDeviceRequest addDeviceRequest)
    //    {
    //        if (addDeviceRequest == null)
    //            throw new ArgumentNullException(nameof(addDeviceRequest));

    //        ValidateMobileDeviceNumber(addDeviceRequest.MobileDeviceNumber);

    //        if (string.IsNullOrWhiteSpace(addDeviceRequest.OptInTimeStamp))
    //            throw new ArgumentException("OptIn TimeStamp is required", nameof(addDeviceRequest.OptInTimeStamp));
    //    }

    //    public static void ValidateRequest(IDeleteDeviceRequest deleteDeviceRequest)
    //    {
    //        if (deleteDeviceRequest == null)
    //            throw new ArgumentNullException(nameof(deleteDeviceRequest));

    //        ValidateMobileDeviceNumber(deleteDeviceRequest.MobileDeviceNumber);
    //    }

    //    public static void ValidateRequest(IGetDeviceStatusRequest getDeviceStatusRequest)
    //    {
    //        if (getDeviceStatusRequest == null)
    //            throw new ArgumentNullException(nameof(getDeviceStatusRequest));

    //        ValidateMobileDeviceNumber(getDeviceStatusRequest.MobileDeviceNumber);
    //    }

    //    public static void ValidateRequest(IGetMobileLocationAddressRequest getMobileLocationAndAddressRequest)
    //    {
    //        if (getMobileLocationAndAddressRequest == null)
    //            throw new ArgumentNullException(nameof(getMobileLocationAndAddressRequest));

    //        ValidateMobileDeviceNumber(getMobileLocationAndAddressRequest.MobileDeviceNumber);
    //    }

    //    public static void ValidateRequest(IValidateOnlineTransactionRequest validateOnlineTransactionRequest) {

    //        if (validateOnlineTransactionRequest == null)
    //            throw new ArgumentNullException(nameof(validateOnlineTransactionRequest));

    //        ValidateMobileDeviceNumber(validateOnlineTransactionRequest.MobileDeviceNumber);
    //    }

    //    public static void ValidateRequest(GetMobileLocationAddressProxyResponse getMobileLocationAddressProxyResponse) {
    //        if (getMobileLocationAddressProxyResponse == null)
    //            throw new ArgumentNullException(nameof(getMobileLocationAddressProxyResponse));
    //    }

    //    public static void ValidateRequest(ValidateOnlineTransactionProxyResponse validateOnlineTransactionProxyResponse) {
    //        if (validateOnlineTransactionProxyResponse == null)
    //            throw new ArgumentNullException(nameof(validateOnlineTransactionProxyResponse));
    //    }
    //    public static void ValidateMobileDeviceNumber(string mobileDeviceNumber)
    //    {
    //        if (string.IsNullOrWhiteSpace(mobileDeviceNumber))
    //            throw new ArgumentException("Mobile Device Number is required", nameof(mobileDeviceNumber));
    //    }

        

    //}
}
