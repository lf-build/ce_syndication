﻿
namespace CreditExchange.Syndication.Zumigo.Response
{
    public class AddDeviceResponse : IAddDeviceResponse
    {
        public string StatusCode { get; set; }
        public string StatusCodeDescription { get; set; }

        //public AddDeviceResponse(AddDeviceProxyResponse addDeviceProxyResponse)
        //{
        //    if (addDeviceProxyResponse?.StatusCode != null)
        //        StatusCode = addDeviceProxyResponse.StatusCode;

        //    if (addDeviceProxyResponse?.StatusCodeDescription != null)
        //        StatusCodeDescription = addDeviceProxyResponse.StatusCodeDescription;
        //}
    }
}
