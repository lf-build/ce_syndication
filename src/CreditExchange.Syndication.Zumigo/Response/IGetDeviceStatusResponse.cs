﻿
namespace CreditExchange.Syndication.Zumigo.Response
{ 
    public interface IGetDeviceStatusResponse 
    {
        DeviceSuccessCodes Status { get; set; }

        string ReferenceNumber { get; set; }


    }
}
