﻿namespace CreditExchange.Syndication.Zumigo.Response
{
    public class ValidateOnlineTransactionResponse : IValidateOnlineTransactionResponse
    {
        public string DistanceFromIp { get; set; }

        public string DistanceFromLatitudeLongitude { get; set; }

        public string DistanceFromWifi { get; set; }

        public string DistanceFromAddress { get; set; }

        public string AverageDistance { get; set; }      
        public  string ReferenceNumber { get; set; }      

        public string ErrorMessage { get; set; }

    }
}
