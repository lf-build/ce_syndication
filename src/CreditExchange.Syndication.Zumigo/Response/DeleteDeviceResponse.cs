﻿namespace CreditExchange.Syndication.Zumigo.Response
{
    public class DeleteDeviceResponse : IDeleteDeviceResponse
    {
        public string StatusCode { get; set; }
        public string StatusCodeDescription { get; set; }
        //public DeleteDeviceResponse(DeleteDeviceProxyResponse deleteDeviceProxyResponse)
        //{
        //    if (deleteDeviceProxyResponse?.StatusCode != null)
        //        StatusCode = deleteDeviceProxyResponse.StatusCode;

        //    if (deleteDeviceProxyResponse?.StatusCodeDescription != null)
        //        StatusCodeDescription = deleteDeviceProxyResponse.StatusCodeDescription;
        //}
    }
}
