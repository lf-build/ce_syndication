﻿namespace CreditExchange.Syndication.Zumigo.Response
{
    public interface IValidateOnlineTransactionResponse
    {
        string DistanceFromIp { get; }

        string DistanceFromLatitudeLongitude { get; }

        string DistanceFromWifi { get; }

        string DistanceFromAddress { get; }

        string AverageDistance { get; }

        string ReferenceNumber { get; set; }

        string ErrorMessage { get; set; }

    }
}
