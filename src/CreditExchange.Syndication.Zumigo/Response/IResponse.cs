﻿
namespace CreditExchange.Syndication.Zumigo.Response
{
    public interface IResponse
    {
        string StatusCode { get; set; }
        string StatusCodeDescription { get; set; }
    }
}
