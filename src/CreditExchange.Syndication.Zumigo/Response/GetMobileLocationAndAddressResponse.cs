﻿using System;

namespace CreditExchange.Syndication.Zumigo.Response
{
    public class GetMobileLocationAndAddressResponse : IGetMobileLocationAndAddressResponse
    {
        public string MobileDeviceNumber { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string Address { get; set; }

        public string Accuracy { get; set; }

        public DateTimeOffset Timestamp { get; set; }

        public string ReferenceNumber { get; set; }
    }
}
