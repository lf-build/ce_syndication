﻿using System;
namespace CreditExchange.Syndication.Zumigo.Response
{
    public interface IGetMobileLocationAndAddressResponse
    {
        string MobileDeviceNumber { get; set; }

        string Latitude { get; set; }

        string Longitude { get; set; }

        string Address { get; set; }

        string Accuracy { get; set; }

        DateTimeOffset Timestamp { get; set; }

        string ReferenceNumber { get; set; }
    }
}
