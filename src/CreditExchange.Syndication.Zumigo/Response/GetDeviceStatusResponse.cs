﻿namespace CreditExchange.Syndication.Zumigo.Response
{
    public class GetDeviceStatusResponse : IGetDeviceStatusResponse
    {
        public DeviceSuccessCodes Status { get; set; }
        public string ReferenceNumber { get; set; }
    }
}
