﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Zumigo
{
    public class EventConfiguration : IEventConfiguration
    {
        public  string EntityId { get; set; }
        public string ReferenceNumber { get; set; }
        public string EntityType { get; set; }        
        public string Response { get; set; }
        public string Request { get; set; }
        public string EventName { get; set; }
      
    }
}