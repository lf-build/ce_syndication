﻿using LendFoundry.SyndicationStore.Events;

namespace CreditExchange.Syndication.Zumigo.Events
{
    public class ZumigoDeviceDeleted : SyndicationCalledEvent, IDeviceDeleted
    {
     
    }
}
