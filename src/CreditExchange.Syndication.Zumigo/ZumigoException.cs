﻿using System;

namespace CreditExchange.Syndication.Zumigo
{
    public class ZumigoException : Exception
    {

        public ZumigoException()
        {
        }

        public ZumigoException(string message) : base(message)
        {
        }

        public ZumigoException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
