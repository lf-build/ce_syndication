﻿using CreditExchange.Syndication.Zumigo.Request;
using CreditExchange.Syndication.Zumigo.Response;
using LendFoundry.Foundation.Persistence;
using System;

namespace CreditExchange.Syndication.Zumigo
{
  public interface IZumigoTryData : IAggregate
    {
         string EntityId { get; set; }
         string EntityType { get; set; }
         IValidateOnlineTransactionResponse Response { get; set; }
         IValidateOnlineTransactionRequest Request { get; set; }
         string MobileDeviceNumber { get; set; }
        DateTimeOffset TryDate { get; set; }

    }
}
