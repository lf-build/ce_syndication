﻿using CreditExchange.Syndication.Zumigo.Request;
using CreditExchange.Syndication.Zumigo.Response;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Zumigo
{
    public class ZumigoRetryData :  Aggregate, IZumigoRetryData
    {
        public ZumigoRetryData()
        {

        }
        public string EntityId { get; set; }
        public string EntityType { get; set; }

        public string MoblieNumber { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IValidateOnlineTransactionRequest, ValidateOnlineTransactionRequest>))]
        public IValidateOnlineTransactionRequest Request { get; set; }
     
    }
}
