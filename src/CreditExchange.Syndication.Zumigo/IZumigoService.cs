﻿using System.Threading.Tasks;
using CreditExchange.Syndication.Zumigo.Events;
using CreditExchange.Syndication.Zumigo.Request;
using CreditExchange.Syndication.Zumigo.Response;
using System.Collections.Generic;

namespace CreditExchange.Syndication.Zumigo
{
    public interface IZumigoService
    {
        Task<IDeviceAdded> AddDevice(string entitytype, string entityid, string mobileDeviceNumber);
        Task DeleteDevice(string entitytype, string entityid, string mobileDeviceNumber);
        Task<IGetDeviceStatusResponse> GetDeviceStatus(string entitytype, string entityid, string mobileDeviceNumber);
        Task<IGetMobileLocationAndAddressResponse> GetDeviceLocationAddress(string entitytype, string entityid, string mobileDeviceNumber);
        Task<IValidateOnlineTransactionResponse> VerifyDeviceLocation(string entityType, string entityId, string mobileDeviceNumber, IValidateOnlineTransactionRequest validateOnlineTransactionRequest);

        Task<List<IZumigoRetryData>> GetRetryRequest();

    }
}
