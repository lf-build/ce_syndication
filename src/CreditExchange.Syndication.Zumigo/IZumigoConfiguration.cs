﻿
using System.Collections.Generic;

namespace CreditExchange.Syndication.Zumigo
{
    public interface IZumigoConfiguration
    {

        IServiceUrls ServiceUrls { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        int RetryDistance { get; set; }
        string CountryCode { get; set; }
        int MobileNumberMaxLenght { get; set; }
        OptInType OptInType { get; set; }
        OptInMethod OptInMethod { get; set; }
        List<EventConfiguration> Events { get; set; }
        int MaxFailureRetryCount { get; set; }
        int MaxLocationRetryCount { get; set; }
    }
}
