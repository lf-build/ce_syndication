﻿namespace CreditExchange.Syndication.Zumigo
{
        public class ResponseCodes
        {
            public bool IsSuccess { get; set; }
            public bool IsRetriable { get; set; }
        
            public string Description { get; set; }
        }
    
}
