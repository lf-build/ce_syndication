﻿using System;
using CreditExchange.Syndication.Zumigo.Proxy.ProxyRequest;
using CreditExchange.Syndication.Zumigo.Request;

namespace CreditExchange.Syndication.Zumigo
{
    public static class Validation
    {
       

        public static void ValidateRequest(AddDeviceProxyRequest addDeviceProxyRequest) {

            if(addDeviceProxyRequest == null)
                    throw new ArgumentNullException(nameof(addDeviceProxyRequest));

            ValidateMobileDeviceNumber(addDeviceProxyRequest.MobileDeviceNumber);

            ValidateOptInTimeStamp(addDeviceProxyRequest.OptInTimeStamp);
        }

      public static void ValidateRequest(IAddDeviceRequest addDeviceRequest)
        {
            if (addDeviceRequest == null)
                throw new ArgumentNullException(nameof(addDeviceRequest));

            ValidateMobileDeviceNumber(addDeviceRequest.MobileDeviceNumber);

            ValidateOptInTimeStamp(addDeviceRequest.OptInTimeStamp);
        }

        public static void ValidateRequest(IDeleteDeviceRequest deleteDeviceRequest)
        {
            if (deleteDeviceRequest == null)
                throw new ArgumentNullException(nameof(deleteDeviceRequest));

            ValidateMobileDeviceNumber(deleteDeviceRequest.MobileDeviceNumber);
        }

        public static void ValidateRequest(IGetDeviceStatusRequest getDeviceStatusRequest)
        {
            if (getDeviceStatusRequest == null)
                throw new ArgumentNullException(nameof(getDeviceStatusRequest));

            ValidateMobileDeviceNumber(getDeviceStatusRequest.MobileDeviceNumber);
        }

        public static void ValidateRequest(IValidateOnlineTransactionRequest validateOnlineTransactionRequest)
        {

            if (validateOnlineTransactionRequest == null)
                throw new ArgumentNullException(nameof(validateOnlineTransactionRequest));

            //ValidateMobileDeviceNumber(validateOnlineTransactionRequest.MobileDeviceNumber);
        }

        public static void ValidateRequest(ValidateOnlineTransactionProxyRequest validateOnlineTransactionProxyRequest){

            if (validateOnlineTransactionProxyRequest == null)
                throw new ArgumentNullException(nameof(validateOnlineTransactionProxyRequest));
        }
        public static void ValidateMobileDeviceNumber(string mobileDeviceNumber)
        {
            if (string.IsNullOrWhiteSpace(mobileDeviceNumber))
                throw new ArgumentException("Mobile Device Number is required", nameof(mobileDeviceNumber));
        }

        public static void ValidateOptInTimeStamp(string optinTimestamp)
        {
            if (string.IsNullOrWhiteSpace(optinTimestamp))
                throw new ArgumentException("OptIn Timestamp is required", nameof(optinTimestamp));
        }

        public static void ValidateOptInType(string optInType)
        {
            if (string.IsNullOrWhiteSpace(optInType))
                throw new ArgumentException("OptIn Type is required", nameof(optInType));
        }
    }
}
