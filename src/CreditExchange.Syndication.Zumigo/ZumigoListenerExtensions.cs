﻿﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif

namespace CreditExchange.Syndication.Zumigo
{
    public static class ZumigoListenerExtensions
    {
        public static void UseZumigoListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IZumigoListener>().Start();
        }
    }
}
