﻿using System;

using LendFoundry.Configuration.Client;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using CreditExchange.Syndication.Zumigo.Response;
using CreditExchange.Syndication.Zumigo.Request;
using LendFoundry.VerificationEngine.Client;
using LendFoundry.VerificationEngine;
using System.Threading.Tasks;
// using LendFoundry.StatusManagement.Client;
// using LendFoundry.StatusManagement;
using LendFoundry.Configuration;

namespace CreditExchange.Syndication.Zumigo
{
    public class ZumigoListener : IZumigoListener
    {
        public ZumigoListener
        (
            ITokenHandler tokenHandler,
            ILoggerFactory loggerFactory,
            IEventHubClientFactory eventHubFactory,
            IConfigurationServiceFactory<ZumigoConfiguration> configurationZumigoFactory,
            IVerificationEngineServiceClientFactory verificationEngineServiceFactory,
            ITenantServiceFactory tenantServiceFactory,
            ITenantTimeFactory tenantTimeFactory,
            IConfigurationServiceFactory configurationFactory,
            IZumigoRetryRepositoryFactory zumigoRetryRepository,
            IZumigoTryRepositoryFactory zumigoTryRepository
            //IStatusManagementServiceFactory statusManagementServiceFactory
        )
        {
            EnsureParameter(nameof(tokenHandler), tokenHandler);
            EnsureParameter(nameof(loggerFactory), loggerFactory);
            EnsureParameter(nameof(eventHubFactory), eventHubFactory);
            EnsureParameter(nameof(configurationZumigoFactory), configurationZumigoFactory);
            EnsureParameter(nameof(tenantServiceFactory), tenantServiceFactory);
            EnsureParameter(nameof(tenantTimeFactory), tenantTimeFactory);
            EnsureParameter(nameof(configurationFactory), configurationFactory);

            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            EventHubFactory = eventHubFactory;
            ConfigurationZumigoFactory = configurationZumigoFactory;
            TenantServiceFactory = tenantServiceFactory;
            TenantTimeFactory = tenantTimeFactory;
            ConfigurationFactory = configurationFactory;
            RetryRepository = zumigoRetryRepository;
            TryRepository = zumigoTryRepository;
            VerificationEngineServiceFactory = verificationEngineServiceFactory;
            //StatusManagementServiceFactory = statusManagementServiceFactory;
        }

        //private IStatusManagementServiceFactory StatusManagementServiceFactory { get; set; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IConfigurationServiceFactory<ZumigoConfiguration> ConfigurationZumigoFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IZumigoRetryRepositoryFactory RetryRepository { get; }
        private IZumigoTryRepositoryFactory TryRepository { get; }
        private IVerificationEngineServiceClientFactory VerificationEngineServiceFactory { get; }
        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                tenantService.GetActiveTenants().ForEach(tenant =>
                {
                    var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName, null, "system", null);                 
                    var reader = new StaticTokenReader(token.Value);
                    var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
                    var configuration = ConfigurationZumigoFactory.Create(reader).Get();
                    var eventHub = EventHubFactory.Create(reader);
                    var retryRepository = RetryRepository.Create(reader);
                    var tryRepository = TryRepository.Create(reader);
                    var verificationEngine = VerificationEngineServiceFactory.Create(reader);
                    //var statusManagement = StatusManagementServiceFactory.Create(reader);
                    if (configuration != null)
                    {
                        foreach (var entity in configuration.Events)
                        {
                            eventHub.On(entity.EventName, (@event) =>
                            {
                                Task.Run(async ()=> await ParseAttribute(entity, logger, retryRepository, tryRepository, tenantTime, configuration, verificationEngine, eventHub,
                                //statusManagement,
                                 @event));
                            });
                            logger.Info($"Event #{entity.EventName} attached to listener");

                        }
                    }
                    else
                    {
                        logger.Info("No configuration found to attach events");
                    }
                    eventHub.StartAsync();
                });


                logger.Info("Listening to events");
            }
            catch (Exception ex)
            {
                logger.Error("Connection error listening to events", ex);
                Start();
            }
        }

        private static async Task ParseAttribute(IEventConfiguration eventConfiguration, ILogger logger, IZumigoRetryRepository retryRepository,
            IZumigoTryRepository tryRepository, ITenantTime tenantTime, ZumigoConfiguration configuration, IVerificationEngineService verificationEngineService, IEventHubClient eventHubClient//IEntityStatusService entityStatusService
            , IEventInfo @event)
        {
            var entityId = "";
            var entityType = "";
            try
            {
                logger.Info($"Zumigo Retry Task started for event {@event.Name} entityId: {entityId}");

                entityId = eventConfiguration.EntityId.FormatWith(@event);
                entityType = eventConfiguration.EntityType.FormatWith(@event);
                string responseString = eventConfiguration.Response.FormatWith(@event);
                dynamic responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<ValidateOnlineTransactionResponse>(responseString);
                string requestString = eventConfiguration.Request.FormatWith(@event);
                dynamic requestData = Newtonsoft.Json.JsonConvert.DeserializeObject<ValidateOnlineTransactionRequest>(requestString);
                double distance =0;


                if (responseData.ErrorMessage != null && requestData.IsRetriable == true)
                {
                    var tryRequest = await Task.Run(() => tryRepository.GetFailedRequest(entityType, entityId));
                    tryRepository.AddTryData(entityType, entityId, requestData, responseData, tenantTime.Now);
                    if (tryRequest.Count == 0)
                    {
                        retryRepository.AddRetryData(entityType, entityId, requestData);
                    }
                    else if (tryRequest.Count > configuration.MaxFailureRetryCount)
                    {
                        await retryRepository.DeleteRetryData(entityType, entityId);
                        await verificationEngineService.InitiateManualVerification("application", entityId,  "LocationVerification", "PhysicalManual");
                    }
                }
                else if (responseData.ErrorMessage == null)
                {
                    var isNumber = responseData.DistanceFromAddress != null? double.TryParse(responseData.DistanceFromAddress, out distance) : false;
                    if (isNumber == true && distance > configuration.RetryDistance)
                    {
                        requestData.IsRetryDistance = true;
                        var tryRequest = await Task.Run(() => tryRepository.GetFailedRequest(entityType, entityId));
                        tryRepository.AddTryData(entityType, entityId, requestData, responseData, tenantTime.Now);
                        if (tryRequest.Count == 0)
                        {
                            retryRepository.AddRetryData(entityType, entityId, requestData);
                        }
                        else if (tryRequest.Count > configuration.MaxLocationRetryCount)
                        {
                            await retryRepository.DeleteRetryData(entityType, entityId);
                            await verificationEngineService.InitiateManualVerification("application", entityId,  "LocationVerification", "PhysicalManual");
                        }
                    }
                    else                   
                        await retryRepository.DeleteRetryData(entityType, entityId);                    
                }
                logger.Info($"Zumigo Retry Task started  completed for event {@event.Name} entityId: {entityId}");
            }
            catch (Exception ex)
            {
                logger.Error($"Error parsing {@event.Name} event for EventId:{@event.Id}, EntityId:{entityId}", ex);
            }
        }

        private static void EnsureParameter(string name, object value)
        {
            if (value == null) throw new ArgumentNullException(nameof(value), $"{name} cannot be null.");
        }
    }
}
