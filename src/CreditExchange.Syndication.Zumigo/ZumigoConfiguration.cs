﻿
using System.Collections.Generic;

namespace CreditExchange.Syndication.Zumigo
{
    public class ZumigoConfiguration : IZumigoConfiguration
    {
        public IServiceUrls ServiceUrls { get; set; } = new ServiceUrls();
        public string UserName { get; set; } = "creditxchangeapi";
        public string Password { get; set; } = "cred17Exchang3Ap!";
        public int RetryDistance { get; set; }
        public int MaxFailureRetryCount { get; set; }
        public int MaxLocationRetryCount { get; set; }
        public string CountryCode { get; set; }

        public int MobileNumberMaxLenght { get; set; }
        public OptInType OptInType { get; set; } = OptInType.Whitelist;
        public OptInMethod OptInMethod { get; set; }
        public List<EventConfiguration> Events { get; set; }
    }
}
