﻿using LendFoundry.Security.Tokens;

namespace CreditExchange.Syndication.Zumigo
{
    public interface IZumigoTryRepositoryFactory
    {
        IZumigoTryRepository Create(ITokenReader reader);
    }
}