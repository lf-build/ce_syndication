﻿using CreditExchange.Syndication.Zumigo.Request;
using CreditExchange.Syndication.Zumigo.Response;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;

namespace CreditExchange.Syndication.Zumigo
{
  public interface IZumigoRetryData : IAggregate
    {
         string EntityId { get; set; }
         string EntityType { get; set; }
        string MoblieNumber { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IValidateOnlineTransactionRequest, ValidateOnlineTransactionRequest>))]
        IValidateOnlineTransactionRequest Request { get; set; }
        
    }
}
