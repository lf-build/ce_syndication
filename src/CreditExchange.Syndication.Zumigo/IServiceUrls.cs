﻿namespace CreditExchange.Syndication.Zumigo
{
    public interface IServiceUrls
    {
        string AddDeviceUrl { get; set; }
        string DeleteDeviceUrl { get; set; }
        string GetDeviceStatusUrl { get; set; }
        string LocationAndAddressUrl { get; set; }
        string ValidateOnlineTransactionUrl { get; set; }
    }
}