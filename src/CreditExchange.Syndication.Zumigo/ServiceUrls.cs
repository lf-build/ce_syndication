﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Zumigo
{
    public class ServiceUrls :IServiceUrls
    {
        public string AddDeviceUrl { get; set; } = "https://api.zumigo.com/zumigo/addDevice.jsp";
        public string DeleteDeviceUrl { get; set; } = "https://api.zumigo.com/zumigo/deleteDevice.jsp";
        public string GetDeviceStatusUrl { get; set; } = "https://api.zumigo.com/zumigo/deviceStatus.jsp";

        public string ValidateOnlineTransactionUrl { get; set; } =
            "https://api.zumigo.com/zumigo/validateOnlineTransaction.jsp";

        public string LocationAndAddressUrl { get; set; } = "https://api.zumigo.com/zumigo/locationAndAddress.jsp";
    }
}
