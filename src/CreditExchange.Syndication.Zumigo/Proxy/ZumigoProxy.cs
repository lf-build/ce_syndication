﻿using System;
using System.Net;
using System.Threading.Tasks;
using CreditExchange.Syndication.Zumigo.Proxy.ProxyRequest;
using RestSharp;
using LendFoundry.Foundation.Logging;

namespace CreditExchange.Syndication.Zumigo.Proxy
{
    public class ZumigoProxy : IZumigoProxy
    {
        private IZumigoConfiguration ZumigoConfiguration { get; }
        private ILogger Logger { get; }

        public ZumigoProxy(IZumigoConfiguration zumigoConfiguration, ILogger logger)
        {
            if (zumigoConfiguration == null)
                throw new ArgumentNullException(nameof(zumigoConfiguration));

            if (string.IsNullOrWhiteSpace(zumigoConfiguration.UserName))
                throw new ArgumentException("User name is required", nameof(zumigoConfiguration.UserName));

            if (string.IsNullOrWhiteSpace(zumigoConfiguration.Password))
                throw new ArgumentException("Password is required", nameof(zumigoConfiguration.Password));

            if (!Enum.IsDefined(typeof(OptInType), Convert.ToString(zumigoConfiguration.OptInType)))
                throw new ArgumentException("Value is OptInType is invalid", nameof(zumigoConfiguration.OptInType));

            if (zumigoConfiguration.ServiceUrls == null)
                throw new ArgumentNullException(nameof(zumigoConfiguration.ServiceUrls));

            if (string.IsNullOrWhiteSpace(zumigoConfiguration.ServiceUrls.AddDeviceUrl))
                throw new ArgumentException("Add device url is required", nameof(zumigoConfiguration.ServiceUrls.AddDeviceUrl));

            if (string.IsNullOrWhiteSpace(zumigoConfiguration.ServiceUrls.GetDeviceStatusUrl))
                throw new ArgumentException("Get device status url is required", nameof(zumigoConfiguration.ServiceUrls.GetDeviceStatusUrl));

            if (string.IsNullOrWhiteSpace(zumigoConfiguration.ServiceUrls.DeleteDeviceUrl))
                throw new ArgumentException("Delete device url is required", nameof(zumigoConfiguration.ServiceUrls.DeleteDeviceUrl));

            if (string.IsNullOrWhiteSpace(zumigoConfiguration.ServiceUrls.LocationAndAddressUrl))
                throw new ArgumentException("LocationAndAddressUrl is required", nameof(zumigoConfiguration.ServiceUrls.LocationAndAddressUrl));

            if (string.IsNullOrWhiteSpace(zumigoConfiguration.ServiceUrls.ValidateOnlineTransactionUrl))
                throw new ArgumentException("ValidateOnlineTransactionUrl is required", nameof(zumigoConfiguration.ServiceUrls.ValidateOnlineTransactionUrl));

            ZumigoConfiguration = zumigoConfiguration;
            Logger = logger;
        }

        public async Task<string> AddDevice(AddDeviceProxyRequest addDeviceRequest)
        {
            var restClient = new RestClient(ZumigoConfiguration.ServiceUrls.AddDeviceUrl);
            var restRequest = new RestRequest(Method.GET);

            restRequest.AddParameter("username", ZumigoConfiguration.UserName, ParameterType.QueryString);
            restRequest.AddParameter("password", ZumigoConfiguration.Password, ParameterType.QueryString);
            restRequest.AddParameter("mdn", addDeviceRequest.MobileDeviceNumber, ParameterType.QueryString);
            restRequest.AddParameter("OptInType", ZumigoConfiguration.OptInType, ParameterType.QueryString);

            OptInType optType;
            if (Enum.TryParse(Convert.ToString(ZumigoConfiguration.OptInType), true, out optType) && optType == OptInType.Whitelist)
            {
                restRequest.AddParameter("OptInMethod", ZumigoConfiguration.OptInMethod, ParameterType.QueryString);
            }
            restRequest.AddParameter("OptInTimestamp", addDeviceRequest.OptInTimeStamp, ParameterType.QueryString);
            restRequest.AddParameter("OptInId", addDeviceRequest.OptlnId, ParameterType.QueryString);
            return await ExecuteRequestAsync(restClient, restRequest);
        }

        private async Task<string> ExecuteRequestAsync(IRestClient client, IRestRequest request)
        {
            var response =await client.ExecuteTaskAsync(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new ZumigoException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK)
                throw new ZumigoException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
                    throw new ZumigoException(
                        $"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");
            }

            return  response.Content;
        }

        public async Task<string> DeleteDevice(string mobileDeviceNumber)
        {
            var restClient = new RestClient(ZumigoConfiguration.ServiceUrls.DeleteDeviceUrl);
            var restRequest = new RestRequest(Method.DELETE);

            restRequest.AddParameter("username", ZumigoConfiguration.UserName, ParameterType.QueryString);
            restRequest.AddParameter("password", ZumigoConfiguration.Password, ParameterType.QueryString);
            restRequest.AddParameter("mdn", mobileDeviceNumber, ParameterType.QueryString);
            return await ExecuteRequestAsync(restClient, restRequest);
        }

        public async Task<string> GetDeviceStatus(string mobileDeviceNumber)
        {
            var client = new RestClient(ZumigoConfiguration.ServiceUrls.GetDeviceStatusUrl);
            var request = new RestRequest(Method.GET);
            request.AddParameter("username", ZumigoConfiguration.UserName, ParameterType.QueryString);
            request.AddParameter("password", ZumigoConfiguration.Password, ParameterType.QueryString);
            request.AddParameter("mdn", mobileDeviceNumber, ParameterType.QueryString);
            return await ExecuteRequestAsync(client, request);
        }

        public async Task<string> GetDeviceLocationAddress(string mobileDeviceNumber)
        {
            var client = new RestClient(ZumigoConfiguration.ServiceUrls.LocationAndAddressUrl);
            var request = new RestRequest(Method.GET);
            request.AddParameter("username", ZumigoConfiguration.UserName, ParameterType.QueryString);
            request.AddParameter("password", ZumigoConfiguration.Password, ParameterType.QueryString);
            request.AddParameter("mdn", mobileDeviceNumber, ParameterType.QueryString);
            return await ExecuteRequestAsync(client, request);
        }

        public async Task<string> VerifyDeviceLocation(ValidateOnlineTransactionProxyRequest validateOnlineTransactionProxyRequest)
        {
         
            var client = new RestClient(ZumigoConfiguration.ServiceUrls.ValidateOnlineTransactionUrl);
            var request = new RestRequest(Method.POST);

            request.AddParameter("username", ZumigoConfiguration.UserName, ParameterType.QueryString);
            request.AddParameter("password", ZumigoConfiguration.Password, ParameterType.QueryString);
            request.AddParameter("mdn", validateOnlineTransactionProxyRequest.MobileDeviceNumber, ParameterType.QueryString);

            if (!string.IsNullOrEmpty(validateOnlineTransactionProxyRequest.IpAddress))
            {
                request.AddParameter("ipaddress", validateOnlineTransactionProxyRequest.IpAddress);
            }
            if (!string.IsNullOrEmpty(validateOnlineTransactionProxyRequest.Latitude))
            {
                request.AddParameter("latitude", validateOnlineTransactionProxyRequest.Latitude);
            }
            if (!string.IsNullOrEmpty(validateOnlineTransactionProxyRequest.Longitude))
            {
                request.AddParameter("longitude", validateOnlineTransactionProxyRequest.Longitude);
            }
            if (!string.IsNullOrEmpty(validateOnlineTransactionProxyRequest.Address))
            {
                request.AddParameter("address", validateOnlineTransactionProxyRequest.Address);
            }
            if (!string.IsNullOrEmpty(validateOnlineTransactionProxyRequest.WifiAddresses))
            {
                request.AddParameter("wifiaddresses", validateOnlineTransactionProxyRequest.WifiAddresses);
            }
            return await ExecuteRequestAsync(client, request);
        }
    }
}
