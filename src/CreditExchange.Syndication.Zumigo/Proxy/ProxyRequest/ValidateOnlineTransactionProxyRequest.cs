﻿using System;
using CreditExchange.Syndication.Zumigo.Request;

namespace CreditExchange.Syndication.Zumigo.Proxy.ProxyRequest
{
    public class ValidateOnlineTransactionProxyRequest
    {
        public string MobileDeviceNumber { get; set; }
        public string IpAddress { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Address { get; set; }
        public string WifiAddresses { get; set; }

        public ValidateOnlineTransactionProxyRequest()
        {

        }

        public ValidateOnlineTransactionProxyRequest(string mobileDeviceNumber, IValidateOnlineTransactionRequest validateOnlineTransactionRequest)
        {
            if (validateOnlineTransactionRequest == null)
                throw new ArgumentNullException(nameof(validateOnlineTransactionRequest));

            MobileDeviceNumber = mobileDeviceNumber;
            IpAddress = validateOnlineTransactionRequest.IPAddress;
            Latitude = validateOnlineTransactionRequest.Latitude;
            Longitude = validateOnlineTransactionRequest.Longitude;
            Address = validateOnlineTransactionRequest.Address;
            WifiAddresses = validateOnlineTransactionRequest.WifiAddresses;
        }


    }
}
