﻿
namespace CreditExchange.Syndication.Zumigo.Proxy.ProxyRequest
{
    public class AddDeviceProxyRequest
    {
        public AddDeviceProxyRequest()
        {
        }
        public string MobileDeviceNumber { get; set; }
        public string OptlnId { get; set; }
        public string OptInTimeStamp { get; set; }
        public AddDeviceProxyRequest(string mobileDeviceNumber)
        {
            MobileDeviceNumber = mobileDeviceNumber;
           // OptInTimeStamp = addDeviceRequest.OptInTimeStamp;
        }
    }
}

