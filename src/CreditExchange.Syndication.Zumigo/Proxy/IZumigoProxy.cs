﻿using System.Threading.Tasks;
using CreditExchange.Syndication.Zumigo.Proxy.ProxyRequest;

namespace CreditExchange.Syndication.Zumigo.Proxy
{
    public interface IZumigoProxy
    {
        Task<string> AddDevice(AddDeviceProxyRequest deviceRequest);
        Task<string> DeleteDevice(string mobileDeviceNumber);
        Task<string> GetDeviceStatus(string mobileDeviceNumber);
        Task<string> GetDeviceLocationAddress(string mobileDeviceNumber);
        Task<string> VerifyDeviceLocation(ValidateOnlineTransactionProxyRequest validateOnlineTransactionProxyRequest);
    }
}
