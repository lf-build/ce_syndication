﻿

using CreditExchange.Syndication.Zumigo.Request;
using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Zumigo
{
    public interface IZumigoRetryRepository : IRepository<IZumigoRetryData>
    {
        Task<List<IZumigoRetryData>> GetRetryRequest();
        void AddRetryData(string entityType, string entityId, IValidateOnlineTransactionRequest validateOnlineTransactionRequest);
        Task DeleteRetryData(string entityType, string entityId);
    }
}