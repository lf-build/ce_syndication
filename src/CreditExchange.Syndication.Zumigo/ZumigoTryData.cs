﻿using CreditExchange.Syndication.Zumigo.Request;
using CreditExchange.Syndication.Zumigo.Response;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Zumigo
{
    public class ZumigoTryData :  Aggregate, IZumigoTryData
    {
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IValidateOnlineTransactionResponse, ValidateOnlineTransactionResponse>))]
        public IValidateOnlineTransactionResponse Response { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IValidateOnlineTransactionRequest, ValidateOnlineTransactionRequest>))]
        public IValidateOnlineTransactionRequest Request { get; set; }
        public string MobileDeviceNumber { get; set; }
        public DateTimeOffset TryDate { get; set; }
    }
}
