﻿using System;
using System.Net;
using CreditExchange.Syndication.AadharBridge.Model;
using Newtonsoft.Json;
using RestSharp;
using JsonSerializer = CreditExchange.Syndication.AadharBridge.Model.JsonSerializer;

namespace CreditExchange.Syndication.AadharBridge.Proxy
{
    public class AadhaarBridgeProxy : IAadhaarBridgeProxy
    {
        public AadhaarBridgeProxy(IAadhaarBridgeConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            Configuration = configuration;
        }
        private IAadhaarBridgeConfiguration Configuration { get; }
        public IAadhaarAuthResponse IsAadharValid(IAadhaarAuthRequest request)
        {
            
            var authJson = new AadhaarVerification
            {
                AadhaarId = request.AadhaarId,
                CertificateType = Configuration.CertificateType.ToString(),
                Modality = Configuration.ValidationModality.ToString(),
                Location = new AadhaarVerificationLocation { Type = Configuration.LocationType, Pincode = request.Pincode},
                Demographics = new Demographics { Name = new Name { MatchingStrategy = Configuration.MatchingStrategy.ToString(), NameValue = request.Name } }
            };
         
            return Execute<AadhaarAuthResponse, AadhaarVerification>(Configuration.ServiceUrls.AadhaarValidationUrl, authJson);
        }
        public IAadhaarOtpResponse GenerateOneTimePassword(string aadhaarId)
        {
            var otpJson = new Otp {
                AadhaarId = aadhaarId,
                CertificateType = Configuration.CertificateType.ToString(),
                Channel = Configuration.OtpChannel.ToString(),
                DeviceId = string.Empty,
                OtpLocation = new OtpLocation
                {
                    Altitude = string.Empty,
                    Latitude = string.Empty,
                    Longitude = string.Empty,
                    Pincode = string.Empty,
                    Type = string.Empty
                }
            };
            return Execute<AadhaarOtpResponse, Otp>(Configuration.ServiceUrls.AadharOtpUrl, otpJson);
        }
        public IAadhaarKycResponse VerifyAadhar(IKyc request)
        {
           return Execute<AadhaarKycResponse, Kyc>(Configuration.ServiceUrls.AadhaarVerificationUrl, request as Kyc);
        }

      

        private  TResponse Execute<TResponse,TRequest>(string url, TRequest json) where TResponse : class
        {
            var restClient = new RestClient(Configuration.GatewayUrl);
            var restRequest = new RestRequest(Method.POST)
            {
                Resource = url,
                JsonSerializer = new JsonSerializer()
            };
            restRequest.AddJsonBody(json);
            var response = restClient.Execute(restRequest);
            if (response != null && ((response.StatusCode == HttpStatusCode.OK) &&
                (response.ResponseStatus == ResponseStatus.Completed)))
            {
                var obj = response.Content;
                var res = JsonConvert.DeserializeObject<TResponse>(obj);
                return res;
            }
            if (response != null)
                throw new AadhaarBridgeNetworkException(response.StatusCode, response.Content );
            return null;
        }
    }
}
