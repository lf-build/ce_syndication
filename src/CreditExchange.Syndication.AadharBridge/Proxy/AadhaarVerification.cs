﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.AadharBridge.Proxy
{
    public class AadhaarVerification
    {
        [JsonProperty(PropertyName = "aadhaar-id")]
        public string AadhaarId { get; set; }
        [JsonProperty(PropertyName = "location")]
        public AadhaarVerificationLocation Location { get; set; }
        [JsonProperty(PropertyName = "modality")]
        public string Modality { get; set; }
        [JsonProperty(PropertyName = "certificate-type")]
        public string CertificateType { get; set; }
        [JsonProperty(PropertyName = "demographics")]
        public Demographics Demographics { get; set; }
    }
    public class AadhaarVerificationLocation
    {
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "pincode")]
        public string Pincode { get; set; }
    }
    public class Demographics
    {
        [JsonProperty(PropertyName = "name")]
        public Name Name { get;set;}
    }
    public class Name
    {
        [JsonProperty(PropertyName = "matching-strategy")]
        public string MatchingStrategy { get; set; }
        [JsonProperty(PropertyName = "name-value")]
        public string NameValue { get; set; }
    }
}
