﻿using CreditExchange.Syndication.AadharBridge.Model;

namespace CreditExchange.Syndication.AadharBridge.Proxy
{
    public interface IAadhaarBridgeProxy
    {
        IAadhaarAuthResponse IsAadharValid(IAadhaarAuthRequest request);
        IAadhaarOtpResponse GenerateOneTimePassword(string aadharId);
        IAadhaarKycResponse VerifyAadhar(IKyc request);
     }
}
