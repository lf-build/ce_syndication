﻿using System;
using System.Net;

namespace CreditExchange.Syndication.AadharBridge.Proxy
{
    public class AadhaarBridgeNetworkException : Exception
    {
        public AadhaarBridgeNetworkException(HttpStatusCode httpStatusCode, string response) : base("Error Occured. Status Code:" + httpStatusCode + "\r\nResponse:"  + response )
        {
            
        }
    }
}
