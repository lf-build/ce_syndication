﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.AadharBridge.Proxy
{
    public class Otp
    {
        [JsonProperty(PropertyName = "aadhaar-id")]
        public string AadhaarId { get; set; }
        [JsonProperty(PropertyName = "device-id")]
        public string DeviceId { get; set; }
        [JsonProperty(PropertyName = "certificate-type")]
        public string CertificateType { get; set; }
        [JsonProperty(PropertyName = "channel")]
        public string Channel { get; set; }
        [JsonProperty(PropertyName = "location")]
        public OtpLocation OtpLocation { get; set; }
    }
    public class OtpLocation
    {
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "latitude")]
        public string Latitude { get; set; }
        [JsonProperty(PropertyName = "longitude")]
        public string Longitude { get; set; }
        [JsonProperty(PropertyName = "altitude")]
        public string Altitude { get; set; }
        [JsonProperty(PropertyName = "pincode")]
        public string Pincode { get; set; }
    }
}
