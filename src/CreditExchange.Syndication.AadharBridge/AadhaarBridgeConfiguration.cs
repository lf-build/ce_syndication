﻿using CreditExchange.Syndication.AadharBridge.Model;

namespace CreditExchange.Syndication.AadharBridge
{
    public class AadhaarBridgeConfiguration : IAadhaarBridgeConfiguration
    {
        public string LocationType { get; set; } = "pincode";
        public CertificateType CertificateType { get; set; }
        public MatchingStrategy MatchingStrategy { get; set; }
        public OtpChannel OtpChannel { get; set; } = OtpChannel.SMS;
        public string GatewayUrl { get; set; }
        public Modality ValidationModality { get; set; } = Modality.demo;
        public Modality VerificationModality { get; set; } = Modality.otp;
        public IServiceUrls ServiceUrls { get; set; } = new ServiceUrls();

        public char Consent { get; set; } = 'Y';
    }
}
