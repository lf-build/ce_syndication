﻿namespace CreditExchange.Syndication.AadharBridge
{
    public interface IServiceUrls
    {
        string AadhaarValidationUrl { get; set; }
        string AadhaarVerificationUrl { get; set; }
        string AadharOtpUrl { get; set; }
    }
}
