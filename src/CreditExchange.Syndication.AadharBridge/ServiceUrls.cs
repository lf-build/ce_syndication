﻿namespace CreditExchange.Syndication.AadharBridge
{
    public class ServiceUrls : IServiceUrls
    {
        public string AadhaarValidationUrl { get; set; } = "auth/raw";
        public string AadhaarVerificationUrl { get; set; } = "kyc/raw";
        public string AadharOtpUrl { get; set; } = "otp";
    }
}