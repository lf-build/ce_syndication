﻿namespace CreditExchange.Syndication.AadharBridge.Model
{
    public interface IAadhaarKycRequest
    {
        string AadhaarId { get; set; }
        string Pincode { get; set; }
        string Otp { get; set; }
       
      }
}
