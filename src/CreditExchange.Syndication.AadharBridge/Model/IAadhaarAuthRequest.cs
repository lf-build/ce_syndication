﻿namespace CreditExchange.Syndication.AadharBridge.Model
{
    public interface IAadhaarAuthRequest
    {
      
        string AadhaarId { get; set; }
        string Pincode { get; set; }
        string Name { get; set; }
      
    }
}
