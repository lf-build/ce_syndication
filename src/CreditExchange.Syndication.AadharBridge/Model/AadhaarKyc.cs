﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.AadharBridge.Model
{
    public class AadhaarKyc
    {
        [JsonProperty(PropertyName = "photo")]
        public string Photo { get; set; }
        [JsonProperty(PropertyName = "poi")]
        public PersonName PersonName { get; set; }
        [JsonProperty(PropertyName = "poa")]
        public PersonAddress Poa { get; set; }
    }

   
    public class PersonName
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "dob")]
        public string Dob { get; set; }
        [JsonProperty(PropertyName = "gender")]
        public string Gender { get; set; }
    }

   
    public class PersonAddress
    {
        [JsonProperty(PropertyName = "co")]
        public string Co { get; set; }
        [JsonProperty(PropertyName = "street")]
        public string Street { get; set; }
        [JsonProperty(PropertyName = "house")]
        public string House { get; set; }
        [JsonProperty(PropertyName = "lm")]
        public string Landmark { get; set; }
        [JsonProperty(PropertyName = "vtc")]
        public string Vtc { get; set; }
        [JsonProperty(PropertyName = "dist")]
        public string District { get; set; }
        [JsonProperty(PropertyName = "state")]
        public string State { get; set; }
        [JsonProperty(PropertyName = "pc")]
        public string PinCode { get; set; }
        [JsonProperty(PropertyName = "po")]
        public string PostOffice { get; set; }
    }
}
