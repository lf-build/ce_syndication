﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.AadharBridge.Model
{
    public class AadhaarAuthResponse : IAadhaarAuthResponse
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }
        [JsonProperty(PropertyName = "aadhaar-reference-code")]
        //Return guid from Aadhaar
        public string AadharReferenceCode { get; set; }
    }
}
