﻿namespace CreditExchange.Syndication.AadharBridge.Model
{
    public interface IKyc
    {
        AuthCaptureRequest AuthCaptureRequest { get; set; }
        char Consent { get; set; }
    }
}