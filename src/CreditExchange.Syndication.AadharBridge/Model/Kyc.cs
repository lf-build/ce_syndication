﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.AadharBridge.Model
{
    public class Kyc : IKyc
    {
        [JsonProperty(PropertyName = "consent")]
        public char Consent { get; set; }
        [JsonProperty(PropertyName = "auth-capture-request")]
        public AuthCaptureRequest AuthCaptureRequest { get; set; }
    }
    public class AuthCaptureRequest
    {
        [JsonProperty(PropertyName = "aadhaar-id")]
        public string AadhaarId { get; set; }
        [JsonProperty(PropertyName = "location")]
        public KycLocation Location { get; set; }
        [JsonProperty(PropertyName = "modality")]
        public string Modality { get; set; }
        [JsonProperty(PropertyName = "certificate-type")]
        public string CertificateType { get; set; }
        [JsonProperty(PropertyName = "otp")]
        public string Otp { get; set; }
    }
    public class KycLocation
    {
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "pincode")]
        public string Pincode { get; set; }
    }
}
