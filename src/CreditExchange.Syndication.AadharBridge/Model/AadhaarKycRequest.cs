﻿namespace CreditExchange.Syndication.AadharBridge.Model
{
    public class AadhaarKycRequest : IAadhaarKycRequest
    {
        public string AadhaarId { get; set; }
        public string Pincode { get; set; }
        public string Otp { get; set; }
      
    }
}
