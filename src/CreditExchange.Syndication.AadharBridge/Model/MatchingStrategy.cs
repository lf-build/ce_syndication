﻿namespace CreditExchange.Syndication.AadharBridge.Model
{
    public enum MatchingStrategy
    {
        /// <summary>
        /// exact match
        /// </summary>
        exact,
        /// <summary>
        /// partial match
        /// </summary>
       partial
    }
}
