﻿namespace CreditExchange.Syndication.AadharBridge.Model
{
    public interface IAadhaarKycResponse
    {
        bool Success { get; set; }
        string AadharStatusCode { get; set; }
        string AadharReferenceCode { get; set; }
        AadhaarKyc Kyc { get; set; }
    }
}
