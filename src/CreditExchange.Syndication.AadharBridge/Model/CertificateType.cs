﻿namespace CreditExchange.Syndication.AadharBridge.Model
{
    public enum CertificateType
    {
        /// <summary>
        /// Preproduction
        /// </summary>
        preprod,
        /// <summary>
        /// Production
        /// </summary>
        prod
    }
}
