﻿namespace CreditExchange.Syndication.AadharBridge.Model
{
    public interface IAadhaarAuthResponse
    {
        bool Success { get; set; }
        string AadharReferenceCode { get; set; }
    }
}
