﻿namespace CreditExchange.Syndication.AadharBridge.Model
{
    public class AadhaarAuthRequest : IAadhaarAuthRequest
    {
        public string AadhaarId { get; set; }
        public string Pincode { get; set; }
        public string Name { get; set; }
      
    }
}
