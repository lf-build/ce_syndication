﻿namespace CreditExchange.Syndication.AadharBridge.Model
{
    public interface IAadhaarOtpResponse
    {
        bool Success { get; set; }
        string AadharStatusCode { get; set; }
        string AadharReferenceCode { get; set; }
    }
}
