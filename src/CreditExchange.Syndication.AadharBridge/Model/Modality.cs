﻿namespace CreditExchange.Syndication.AadharBridge.Model
{
    public enum Modality
    {
        /// <summary>
        /// Demographics for authentication
        /// </summary>
        demo,
        /// <summary>
        /// One time password
        /// </summary>
        otp
    }
}
