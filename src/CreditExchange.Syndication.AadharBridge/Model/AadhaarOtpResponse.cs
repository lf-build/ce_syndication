﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.AadharBridge.Model
{
    public class AadhaarOtpResponse : IAadhaarOtpResponse
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }
        [JsonProperty(PropertyName = "aadhaar-status-code")]
        public string AadharStatusCode { get; set; }
        [JsonProperty(PropertyName = "aadhaar-reference-code")]
        public string AadharReferenceCode { get; set; }
    }
}
