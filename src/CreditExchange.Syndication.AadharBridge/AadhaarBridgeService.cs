﻿using System;
using System.Threading.Tasks;
using LendFoundry.EventHub.Client;
using CreditExchange.Syndication.AadharBridge.Events;
using CreditExchange.Syndication.AadharBridge.Model;
using CreditExchange.Syndication.AadharBridge.Proxy;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Lookup;
using System.Linq;

namespace CreditExchange.Syndication.AadharBridge
{
    public class AadhaarBridgeService : IAadhaarBridgeService
    {
        public AadhaarBridgeService(IAadhaarBridgeProxy aadhaarBridgeProxy, IAadhaarBridgeConfiguration configuration, IEventHubClient eventHub, ILookupService lookup)
        {
            if (aadhaarBridgeProxy == null)
                throw new ArgumentNullException(nameof(aadhaarBridgeProxy));
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            AadhaarBridgeProxy = aadhaarBridgeProxy;
            Configuration = configuration;
            EventHub = eventHub;
            Lookup = lookup;
        }
        public const string ServiceName = "aadhaarbridge";
        public IAadhaarBridgeProxy AadhaarBridgeProxy { get; }
        private IAadhaarBridgeConfiguration Configuration { get; }
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }
        public async Task<IAadhaarAuthResponse> IsAadharValid(string entityType, string entityId, IAadhaarAuthRequest request)
        {
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentException("EntityType is require", nameof(entityType));
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            entityType = EnsureEntityType(entityType);
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            var response = AadhaarBridgeProxy.IsAadharValid(request);
            var referencenumber = Guid.NewGuid().ToString("N");
            await EventHub.Publish(new AadhaarValidationRequested
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = response,
                Request = request,
                ReferenceNumber = referencenumber,
                Name = ServiceName

            });
            return response;
        }

        public async Task<IAadhaarOtpResponse> GenerateOneTimePassword(string entityType, string entityId, string aadhaarId)
        {
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentException("EntityType is require", nameof(entityType));
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            entityType = EnsureEntityType(entityType);
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(aadhaarId))
                throw new ArgumentException("Aadhaar-Id cannot be empty");

            var response = AadhaarBridgeProxy.GenerateOneTimePassword(aadhaarId);
            var referencenumber = Guid.NewGuid().ToString("N");
            await EventHub.Publish(new AadhaarOtpSent()
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = response,
                Request = aadhaarId,
                ReferenceNumber = referencenumber,
                Name = ServiceName
            });
            return response;
        }
        public async Task<IAadhaarKycResponse> VerifyAadhaar(string entityType, string entityId,IAadhaarKycRequest request)
        {
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentException("EntityType is require", nameof(entityType));
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
          
            entityType = EnsureEntityType(entityType);
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            var kycJson = new Kyc
            {
                Consent = Configuration.Consent,
                AuthCaptureRequest = new AuthCaptureRequest
                {
                    AadhaarId = request.AadhaarId,
                    CertificateType = Configuration.CertificateType.ToString(),
                    Location = new KycLocation
                    {
                        Pincode = request.Pincode,
                        Type = Configuration.LocationType
                    },
                    Modality = Configuration.VerificationModality.ToString(),
                    Otp = request.Otp
                }
            };

            var response = AadhaarBridgeProxy.VerifyAadhar(kycJson);
            var referencenumber = Guid.NewGuid().ToString("N");
            await EventHub.Publish(new AadhaarVerificationRequested()
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = response,
                Request = request,
                ReferenceNumber = referencenumber,
                Name = ServiceName
            });

            return response;
        }
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}
