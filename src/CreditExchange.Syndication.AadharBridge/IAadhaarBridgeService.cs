﻿using System.Threading.Tasks;
using CreditExchange.Syndication.AadharBridge.Model;

namespace CreditExchange.Syndication.AadharBridge
{
    public interface IAadhaarBridgeService
    {
      Task<IAadhaarAuthResponse> IsAadharValid(string entityType, string entityId,IAadhaarAuthRequest request);
      Task<IAadhaarOtpResponse> GenerateOneTimePassword(string entityType, string entityId,string aadharId);
      Task<IAadhaarKycResponse> VerifyAadhaar(string entityType, string entityId,IAadhaarKycRequest request);
    }
}
