﻿using CreditExchange.Syndication.AadharBridge.Model;

namespace CreditExchange.Syndication.AadharBridge
{
    public interface IAadhaarBridgeConfiguration
    {
        string LocationType { get; set; } 
        CertificateType CertificateType { get; set; }
        MatchingStrategy MatchingStrategy { get; set; }
        OtpChannel OtpChannel { get; set; }
        string GatewayUrl { get; set; }
        IServiceUrls ServiceUrls { get; set; }
        Modality ValidationModality { get; set; }
        Modality VerificationModality { get; set; }
        char Consent { get; set; }
      
    }
}
