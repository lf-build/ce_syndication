﻿using LendFoundry.Foundation.Persistence;
using CreditExchange.TowerData.Api.ViewModel;
using System.Collections.Generic;

namespace CreditExchange.TowerData.Api
{
    public interface IGetEmailInformationRespository :IRepository<IGetEmailInformation>
    {
        List<IGetEmailInformation> Get(string entityType, string entityId);
    }
}
