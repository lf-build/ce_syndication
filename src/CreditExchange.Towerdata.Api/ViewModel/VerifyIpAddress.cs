﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.TowerData.Api.ViewModel
{
    public class VerifyIpAddress :Aggregate, IVerifyIpAddress
    {
        public string EntityType { get; set; }

        public string EntityId { get; set; }

        public string IpAddress { get; set; }

        public DateTimeOffset ReportDate { get; set; }

        public DateTimeOffset ExpirationDate { get; set; }

        public object Response { get; set; }
    }
}
