﻿using LendFoundry.Foundation.Persistence;
using System;

namespace CreditExchange.TowerData.Api.ViewModel
{
    public interface IVerifyEmail :IAggregate
    {
        string Email { get; set; }
        string EntityId { get; set; }
        string EntityType { get; set; }
        DateTimeOffset ExpirationDate { get; set; }
        DateTimeOffset ReportDate { get; set; }
        object Response { get; set; }
    }
}