﻿using LendFoundry.Foundation.Persistence;
using System;

namespace CreditExchange.TowerData.Api.ViewModel
{
    public interface IVerifyIpAddress :IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        DateTimeOffset ExpirationDate { get; set; }
        string IpAddress { get; set; }
        DateTimeOffset ReportDate { get; set; }
        object Response { get; set; }
    }
}