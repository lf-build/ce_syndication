﻿#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using CreditExchange.TowerData.Api.Events;
using CreditExchange.TowerData.Api.ViewModel;
using Newtonsoft.Json;
using System.Collections.Generic;
using CreditExchange.Syndication.TowerData;
using System;

namespace CreditExchange.TowerData.Api
{
    public class TowerdataPersistenceService : ITowerdataPersistenceService
    {
        public TowerdataPersistenceService
            (
            ITowerDataConfiguration configuration,
            IGetEmailInformationRespository getEmailInformationRespository,
            IVerifyEmailRespository verifyEmailRepository,
            IVerifyIpAddressRepository verifyIpAddressRepository,
            ITenantTime tenantTime,
            IEventHubClient eventHubClient,
            ILogger logger
            )
        {
            Configuration = configuration;
            GetEmailInformationRespository = getEmailInformationRespository;
            VerifyEmailRespository = verifyEmailRepository;
            VerifyIpAddressRepository = VerifyIpAddressRepository;
            TenantTime = tenantTime;
            EventHub = eventHubClient;
            Logger = logger;
        }
        private ITowerDataConfiguration Configuration { get; }
        private IGetEmailInformationRespository GetEmailInformationRespository { get; }
        private IVerifyEmailRespository VerifyEmailRespository { get; }
        private IVerifyIpAddressRepository VerifyIpAddressRepository { get; }

        private ITenantTime TenantTime { get; }

        private IEventHubClient EventHub { get; }

        private ILogger Logger { get; }

        public void AddGetEmailInformation(IEmailInformationRequest emailInformationRequest, IEmailInformation getEmailInformationresponse)
        {
            var now = TenantTime.Now;
            var addval = new GetEmailInformation
            {
                EntityId = emailInformationRequest.EntityId,
                EntityType = emailInformationRequest.EntityType,
                ExpirationDate = now.AddDays(Configuration.ExpirationInDays),
                Response = getEmailInformationresponse,
                ReportDate = now,
                Email = emailInformationRequest.Email

            };
            addval.Response = JsonConvert.SerializeObject(addval.Response);

            GetEmailInformationRespository.Add(addval);
            Logger.Info($"Domain Search with Domain:{emailInformationRequest.Email}, ReportId:#{addval.Id}, ExpirationDate:#{addval.ExpirationDate}, added in database");
            var referencenumber = Guid.NewGuid().ToString("N");
            EventHub.Publish(new GetEmailInformationAdded()
            {

                EntityId = emailInformationRequest.EntityId,
                EntityType = emailInformationRequest.EntityType,
                Request = emailInformationRequest,
                ReferenceNumber = referencenumber,
                Response = getEmailInformationresponse
                //SyndicationName = Settings.ServiceName
            });
            Logger.Info($"GetEmailInformation response for AadharId:{emailInformationRequest.Email}, with event #{nameof(GetEmailInformationAdded)} was processed");
        }
        public void AddVerifyEmail(IEmailInformationRequest verifyEmailrequest, IEmailVerificationResponse verifyEmailresponse)
        {
            var now = TenantTime.Now;
            var addval = new VerifyEmail
            {
                EntityId = verifyEmailrequest.EntityId,
                EntityType = verifyEmailrequest.EntityType,
                ExpirationDate = now.AddDays(Configuration.ExpirationInDays),
                Response = verifyEmailresponse,
                ReportDate = now,
                Email = verifyEmailrequest.Email
            };
            addval.Response = JsonConvert.SerializeObject(addval.Response);

            VerifyEmailRespository.Add(addval);
            Logger.Info($"GetEmailInformation with Domain:{addval.Email}, ReportId:#{addval.Id}, ExpirationDate:#{addval.ExpirationDate}, added in database");
            var referencenumber = Guid.NewGuid().ToString("N");
            EventHub.Publish(new VerifyEmailAdded()
            {

                EntityId = verifyEmailrequest.EntityId,
                EntityType = verifyEmailrequest.EntityType,
                Request = verifyEmailrequest,
                Response = verifyEmailresponse,
                ReferenceNumber = referencenumber
                //SyndicationName = Settings.ServiceName

            });
            Logger.Info($"EmailFinder response for Domain:{addval.Email}, with event #{nameof(VerifyEmailAdded)} was processed");
        }

        public void AddVerifyIpAddress(IIPAddressRequest ipaddressRequest, IIpVerificationResponse ipverificationResponse)
        {
            var now = TenantTime.Now;
            var addval = new VerifyIpAddress
            {
                EntityId = ipaddressRequest.EntityId,
                EntityType = ipaddressRequest.EntityType,
                ExpirationDate = now.AddDays(Configuration.ExpirationInDays),
                Response = ipverificationResponse,
                ReportDate = now,
                IpAddress = ipaddressRequest.IpAddress.ToString()
            };
            addval.Response = JsonConvert.SerializeObject(addval.Response);

            VerifyIpAddressRepository.Add(addval);
            Logger.Info($"Domain Search with Email:{ipaddressRequest.IpAddress.ToString()}, ReportId:#{addval.Id}, ExpirationDate:#{addval.ExpirationDate}, added in database");
            var referencenumber = Guid.NewGuid().ToString("N");
            EventHub.Publish(new VerifyIpAddressAdded()
            {
                EntityId = ipaddressRequest.EntityId,
                EntityType = ipaddressRequest.EntityType,
                Response = ipverificationResponse,
                Request = ipaddressRequest,
                ReferenceNumber = referencenumber
                //SyndicationName = Settings.ServiceName

            });
            Logger.Info($"VerifyIpAddress response for {ipaddressRequest.IpAddress.ToString()}, with event #{nameof(VerifyIpAddressAdded)} was processed");
        }


        public List<IGetEmailInformation> GetGetEmailInformation(string entityType, string entityId)
        {
            return GetEmailInformationRespository.Get(entityType, entityId);

        }


        public List<IVerifyIpAddress> GetVerifyIpAddress(string entityType, string entityId)
        {
            return VerifyIpAddressRepository.Get(entityType, entityId);

        }
        public List<IVerifyEmail> GetVerifyEmail(string entityType, string entityId)
        {
            return VerifyEmailRespository.Get(entityType, entityId);

        }
    }
}
