﻿using LendFoundry.Foundation.Persistence;
using CreditExchange.TowerData.Api.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.TowerData.Api
{
   public  interface IVerifyEmailRespository :IRepository<IVerifyEmail>
    {
        List<IVerifyEmail> Get(string entityType, string entityId);
    }
}
