﻿using LendFoundry.SyndicationStore.Events;

namespace CreditExchange.TowerData.Api.Events
{
    public class VerifyIpAddressAdded : SyndicationCalledEvent
    {
        public VerifyIpAddressAdded()
        {

        }
        //public VerifyIpAddressAdded(VerifyIpAddress verifyIpAddress)
        //{
        //    VerifyIpAddress = verifyIpAddress;
        //}
        //public IVerifyIpAddress VerifyIpAddress { get; set; }
    }
}
