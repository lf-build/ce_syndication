﻿using LendFoundry.SyndicationStore.Events;

namespace CreditExchange.TowerData.Api.Events
{
    public class VerifyEmailAdded : SyndicationCalledEvent
    {
        public VerifyEmailAdded()
        {

        }
        //public VerifyEmailAdded(VerifyEmail verifyEmail)
        //{
        //    VerifyEmail = verifyEmail;
        //}
        //public IVerifyEmail VerifyEmail { get; set; }
    }
}
