﻿using System.Collections.Generic;
using CreditExchange.TowerData.Api.ViewModel;
using CreditExchange.Syndication.TowerData;

namespace CreditExchange.TowerData.Api
{
    public interface ITowerdataPersistenceService
    {
        void AddGetEmailInformation(IEmailInformationRequest emailInformationRequest, IEmailInformation getEmailInformationresponse);
        void AddVerifyEmail(IEmailInformationRequest verifyEmailrequest, IEmailVerificationResponse verifyEmailresponse);
        void AddVerifyIpAddress(IIPAddressRequest ipaddressRequest, IIpVerificationResponse ipverificationResponse);
        List<IGetEmailInformation> GetGetEmailInformation(string entityType, string entityId);
        List<IVerifyEmail> GetVerifyEmail(string entityType, string entityId);
        List<IVerifyIpAddress> GetVerifyIpAddress(string entityType, string entityId);
    }
}