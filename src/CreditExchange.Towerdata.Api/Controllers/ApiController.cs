﻿using CreditExchange.Syndication.TowerData;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
#if DOTNET2
using  Microsoft.AspNetCore.Mvc;
using LendFoundry.EventHub;
#else
using Microsoft.AspNet.Mvc;
using LendFoundry.EventHub.Client;
#endif
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CreditExchange.TowerData.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(ITowerDataService service, ILogger logger) : base(logger)
        {
            Service = service;
          
        }
        
        private ILookupService Lookup { get; }

        private ITowerDataService Service { get; }

        private IEventHubClient EventHubClient { get; }


        [HttpGet("{entityType}/email/{email}/info")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.TowerData.IEmailInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetEmailInformation(string entityType, string email)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Service.GetEmailInformation(entityType, email));
                }
                catch (TowerDataException ex)
                {
                    Logger.Error($"The method GetEmailInformation({email}) raised an error: {ex.Message}");
                    throw new NotFoundException($"Information not found with email:{email}");
                }
            });
        }

        [HttpGet("{entityType}/email/{email}/verify")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.TowerData.IEmailVerificationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> VerifyEmail(string entityType, string email)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var decodedString = System.Net.WebUtility.UrlDecode(email);
                    return Ok(await Service.VerifyEmail(entityType, decodedString));
                }
                catch (TowerDataException ex)
                {
                    Logger.Error($"The method VerifyEmail({email}) raised an error: {ex.Message}");
                    throw new NotFoundException($"Information not found with email:{email}");
                }
            });
        }

        [HttpGet("{entityType}/ip/{ip}/verify")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.TowerData.IIpVerificationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> VerifyIpAddress(string entityType,string ip)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Service.VerifyIpAddress(entityType, ip));
                }
                catch (TowerDataException ex)
                {
                    Logger.Error($"The method VerifyIpAddress({ip}) raised an error: {ex.Message}");
                    throw new NotFoundException($"Information not found with IpAddress:{ip}");
                }
            });
        }
      
    }
}