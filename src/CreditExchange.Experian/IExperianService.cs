﻿using System.Threading.Tasks;
using CreditExchange.Syndication.Experian;

namespace CreditExchange.Experian
{
    public interface IExperianService
    {
       
        Task<IAbridgedReportResponse> GetReport(string entityType, string entityId, ISingleActionRequest request);
        Task<ISingleActionResponse> InvokeSingleAction(string entityType, string entityId, ISingleActionRequest request);
       
    }
}