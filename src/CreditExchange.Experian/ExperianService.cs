﻿using CreditExchange.Experian.Persistence;
using CreditExchange.Syndication.Experian;
using LendFoundry.Clients.DecisionEngine;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Experian
{
    public class ExperianService : IExperianService
    {
        public ExperianService(IEventHubClient eventHub, ILookupService lookup, ILogger logger, IExperianSyndicationService experiansyndicaiton,
           IExperianRepository experianRespository, IDecisionEngineService decisionEngineService, ExperianConfiguration configuration)
        {
            EventHub = eventHub;
            Lookup = lookup;
            Logger = logger;
            if (experiansyndicaiton == null)
                throw new ArgumentNullException(nameof(experiansyndicaiton));
            ExperianSyndication = experiansyndicaiton;
            if (experianRespository == null)
                throw new ArgumentNullException(nameof(experianRespository));
            ExperianRepository = experianRespository;
            DecisionEngineService = decisionEngineService;
            Configuration = configuration;
        }
        private IEventHubClient EventHub { get; }
        public static string ServiceName { get; } = "Experian";
        private ILookupService Lookup { get; }
        private ILogger Logger { get; }
        private IExperianSyndicationService ExperianSyndication { get; }
        private IExperianRepository ExperianRepository { get; }
        private IDecisionEngineService DecisionEngineService { get; }
        private ExperianConfiguration Configuration { get; }
        public void ValidateRequest(ISingleActionRequest request)
        {
            if (string.IsNullOrEmpty(request.Gender))
                throw new ArgumentException("Gender is require");
            if (string.IsNullOrEmpty(request.DateOfBirth.ToString()))
                throw new ArgumentException("DateOfBirth is require");
            if (string.IsNullOrEmpty(request.FirstName))
                throw new ArgumentException("ApplicantFirstName is require");
            if (string.IsNullOrEmpty(request.SurName))
                throw new ArgumentException("ApplicantSurName is require");
            if (string.IsNullOrEmpty(request.Flatno))
                throw new ArgumentException("Flatno is require");
            if (string.IsNullOrEmpty(request.State))
                throw new ArgumentException("State is require");

            if (string.IsNullOrEmpty(request.Pincode))
                throw new ArgumentException("Pincode is require");
            if (string.IsNullOrEmpty(request.MobileNo))
                throw new ArgumentException("MobileNo is require");
            if (string.IsNullOrEmpty(request.Email))
                throw new ArgumentException("Email is require");


        }

        public async Task<IAbridgedReportResponse> GetReport(string entityType, string entityId, ISingleActionRequest request)
        {

            if (request == null)
                throw new ArgumentException("Invalid Request Payload");
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentException("EnityType is require");
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentException("EntityId is require");
            IAbridgedReportResponse reportresponse = null;
            var referenceNumber = Guid.NewGuid().ToString("N");
            try
            {
                entityType = EnsureEntityType(entityType);


                var singleactionresponse = await InvokeSingleAction(entityType, entityId, request);
                if (singleactionresponse != null)
                {
                    if (string.IsNullOrEmpty(singleactionresponse.ErrorString))
                    {
                        var reportRequestStr = "stgOneHitId=" + singleactionresponse.StageOneId_ + "&stgTwoHitId=" + singleactionresponse.StageTwoId_ + "&clientName=" + Configuration.ClientName;
                        reportresponse = await ExperianSyndication.GetAbridgedReport(entityType, entityId, reportRequestStr);
                        if (reportresponse != null)
                        {

                            var experiandata = await ExperianRepository.GetByEntityId(entityType, entityId, "abridgedreport");


                            if (experiandata != null)
                            {
                                if (!string.IsNullOrEmpty(reportresponse.ErrorString))
                                {
                                    experiandata.Status = "failed";
                                }
                                else
                                {
                                    experiandata.Status = "success";
                                }
                                experiandata.Api = "abridgedreport";
                                experiandata.Request = reportRequestStr;
                                experiandata.Response = Newtonsoft.Json.JsonConvert.SerializeObject(reportresponse).ToString();
                                experiandata.ProcessDate = DateTime.Now;
                               
                                ExperianRepository.Update(experiandata);
                            }
                            else
                            {
                                experiandata = new ExperianData();
                                if (!string.IsNullOrEmpty(reportresponse.ErrorString))
                                {
                                    experiandata.Status = "failed";
                                }
                                else
                                {
                                    experiandata.Status = "success";
                                }
                                experiandata.EntityId = entityId;
                                experiandata.EntityType = entityType;
                                experiandata.Api = "abridgedreport";
                                experiandata.Request = reportRequestStr;
                                experiandata.Response = Newtonsoft.Json.JsonConvert.SerializeObject(reportresponse).ToString();
                                experiandata.ProcessDate = DateTime.Now;

                                ExperianRepository.Add(experiandata);
                            }
                            await EventHub.Publish(new ExperianReportPulled
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = reportresponse,
                                Request = reportRequestStr,
                                ReferenceNumber = referenceNumber,
                                Name = ServiceName
                            });


                        }
                    }
                }
            }
            catch (ExperianException exception)
            {
                await EventHub.Publish(new ExperianReportPullFailed
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = request,
                    ReferenceNumber = referenceNumber,
                    Name = ServiceName
                });
                Logger.Error("[Experian] Error occured when GetReport method was called. Error: ", exception);
                throw new ExperianException(exception.Message);
            }
            return reportresponse;
        }
        public async Task<ISingleActionResponse> InvokeSingleAction(string entityType, string entityId, ISingleActionRequest request)
        {
            if (request == null)
                throw new ArgumentException("Invalid Request Payload");
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentException("EnityType is require");
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentException("EntityId is require");
            ISingleActionResponse response = null;
            ValidateRequest(request);
            var referenceNumber = Guid.NewGuid().ToString("N");
            try
            {
                var requeststr = BuildExperainRequest(request, Configuration);
                response = await ExperianSyndication.InvokeSingleAction(entityType, entityId, requeststr);
                if (response != null)
                {

                    var experiandata = await ExperianRepository.GetByEntityId(entityType, entityId, "singleaction");


                    if (experiandata != null)
                    {
                        experiandata.Request = requeststr;
                        if (!string.IsNullOrEmpty(response.ErrorString))
                        {
                            experiandata.Status = "failed";
                        }
                        else
                        {
                            experiandata.Status = "success";
                        }
                      
                        experiandata.Api = "singleaction";
                        experiandata.ProcessDate = DateTime.Now;
                        experiandata.Response = Newtonsoft.Json.JsonConvert.SerializeObject(response).ToString();
                        ExperianRepository.Update(experiandata);
                    }
                    else
                    {
                        experiandata = new ExperianData();
                        if (!string.IsNullOrEmpty(response.ErrorString))
                        {
                            experiandata.Status = "failed";
                        }
                        else
                        {
                            experiandata.Status = "success";
                        }
                        experiandata.Api = "singleaction";
                        experiandata.EntityId = entityId;
                        experiandata.EntityType = entityType;
                        experiandata.ProcessDate = DateTime.Now;
                        experiandata.Request = requeststr;
                        experiandata.Response = Newtonsoft.Json.JsonConvert.SerializeObject(response).ToString();
                        ExperianRepository.Add(experiandata);
                    }
                    await EventHub.Publish(new SingleActionInvoked
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = response,
                        Request = request,
                        ReferenceNumber = referenceNumber,
                        Name = ServiceName
                    });
                    if (!string.IsNullOrEmpty(response.ErrorString))
                    {
                        throw new ExperianException(response.ErrorString);
                    }


                }
            }
            catch (ExperianException exception)
            {
                await EventHub.Publish(new SingleActionPullFailed
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = request,
                    ReferenceNumber = referenceNumber,
                    Name = ServiceName
                });
                Logger.Error("[Experian] Error occured when InvokeSingleAction method was called from ExperianService. Error: ", exception);
                throw new ExperianException(exception.Message);

            }
            return response;
        }
        private string BuildExperainRequest(ISingleActionRequest request, ExperianConfiguration configuration)
        {
            try
            {
                object executionResult = DecisionEngineService.Execute<dynamic, dynamic>(
                     Configuration.ExperianRequestBuilderRule,
                      new { payload = request, configuration });

                if (executionResult != null)
                {
                    var propertyValuePairs = ((JObject)executionResult).ToObject<Dictionary<string, object>>();
                    if (!string.IsNullOrEmpty(Convert.ToString(propertyValuePairs["message"])))
                    {
                        throw new ExperianException(propertyValuePairs["message"].ToString());
                    }
                    var requeststr = propertyValuePairs["request"].ToString();

                    return requeststr;
                }

            }
            catch (ExperianException ex)
            {
                Logger.Info("Error occured in rule execution for BuilExperian Request", ex);
                throw new ExperianException(ex.Message);
            }

            return null;
        }
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;

        }
    }
}
