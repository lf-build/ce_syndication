﻿using CreditExchange.Syndication.Zumigo;
using CreditExchange.Syndication.Zumigo.Request;
using CreditExchange.Syndication.Zumigo.Response;
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CreditExchange.Zumigo.Persistance
{
    public class ZumigoTryRepository : MongoRepository<IZumigoTryData, ZumigoTryData>, IZumigoTryRepository
    {
        static ZumigoTryRepository()
        {
            BsonClassMap.RegisterClassMap<ZumigoTryData>(map =>
            {
                map.AutoMap();
                var type = typeof(ZumigoTryData);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<ValidateOnlineTransactionResponse>(map =>
            {
                map.AutoMap();
                var type = typeof(ValidateOnlineTransactionResponse);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<ValidateOnlineTransactionRequest>(map =>
            {
                map.AutoMap();
                var type = typeof(ValidateOnlineTransactionRequest);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public ZumigoTryRepository(LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "zumigo-try")
        {
            CreateIndexIfNotExists("zumigo-try",
               Builders<IZumigoTryData>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId));
        }
        public async Task<List<IZumigoTryData>> GetFailedRequest(string entityType, string entityId)
        {
            return await Query.Where(a => a.EntityType == entityType && a.EntityId == entityId).ToListAsync();
        }

        public void AddTryData(string entityType, string entityId, IValidateOnlineTransactionRequest validateOnlineTransactionRequest, IValidateOnlineTransactionResponse response, DateTimeOffset tryDate)
        {
            var zumigoRetryData = new ZumigoTryData() { EntityType = entityType, EntityId = entityId, Request = validateOnlineTransactionRequest, Response = response, TryDate = tryDate };
            Add(zumigoRetryData);
        }

    }
}
