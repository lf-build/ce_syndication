﻿using CreditExchange.Syndication.Zumigo;
using CreditExchange.Syndication.Zumigo.Request;
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CreditExchange.Zumigo.Persistance
{
    public class ZumigoRetryRepository : MongoRepository<IZumigoRetryData, ZumigoRetryData>, IZumigoRetryRepository
    {
        static ZumigoRetryRepository()
        {
            BsonClassMap.RegisterClassMap<ZumigoRetryData>(map =>
            {
                map.AutoMap();
                var type = typeof(ZumigoRetryData);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
           
        }

        public ZumigoRetryRepository(LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "zumigo-retry")
        {

            CreateIndexIfNotExists("zumigo-retry",
                Builders<IZumigoRetryData>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId),true);
        }


        public async Task<List<IZumigoRetryData>> GetRetryRequest()
        {
            return await Query.Where(a => a.TenantId == TenantService.Current.Id).ToListAsync();
        }

        public void AddRetryData(string entityType, string entityId, IValidateOnlineTransactionRequest validateOnlineTransactionRequest)
        {
            var zumigoRetryData = new ZumigoRetryData() { EntityType = entityType, EntityId = entityId, Request = validateOnlineTransactionRequest };
            Add(zumigoRetryData);
        }
        public async Task DeleteRetryData(string entityType, string entityId)
        {
            var retryRequest = await Query.FirstOrDefaultAsync(a => a.EntityId == entityId
                                    && a.EntityType == entityType);
            if(retryRequest != null)
            {
                Remove(retryRequest);
            }
        }



    }
}
