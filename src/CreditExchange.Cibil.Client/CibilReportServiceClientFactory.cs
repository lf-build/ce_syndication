﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;

namespace CreditExchange.Cibil.Client
{
    public class CibilReportServiceClientFactory :ICibilReportServiceClientFactory
    {
        public CibilReportServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
           
        }
        private IServiceProvider Provider { get; }
  
        private string Endpoint { get; }

        private int Port { get; }

        public ICibilReportService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new CibilReportService(client);
        }
    }
}
