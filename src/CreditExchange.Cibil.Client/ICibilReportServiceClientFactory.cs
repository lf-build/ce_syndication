﻿using LendFoundry.Security.Tokens;

namespace CreditExchange.Cibil.Client
{
    public  interface ICibilReportServiceClientFactory
    {
        ICibilReportService Create(ITokenReader reader);
    }
}
