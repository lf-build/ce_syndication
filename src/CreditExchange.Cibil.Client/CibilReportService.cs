﻿using System.Threading.Tasks;
using CreditExchange.Syndication.Cibil.Request;
using CreditExchange.Syndication.Cibil.Response;
using RestSharp;
using LendFoundry.Foundation.Logging;
using CreditExchange.Syndication.Cibil;
using LendFoundry.Foundation.Client;

namespace CreditExchange.Cibil.Client
{
    public class CibilReportService : ICibilReportService
    {
        public CibilReportService(IServiceClient client)
        {
            Client = client;
        
        }
        private ILogger Logger { get; }
        private IServiceClient Client { get; }
        public async  Task<IReport> GetReport(string entityType, string entityId, IReportRequest request)
        {
          
            var restrequest = new RestRequest("/{entitytype}/{entityid}", Method.POST);
            restrequest.AddUrlSegment("entitytype", entityType);
            restrequest.AddUrlSegment("entityid", entityId);
            restrequest.AddJsonBody(request);
            return await Client.ExecuteAsync<Report>(restrequest);
        }

        public async Task<ICIRHtml> GetHtmlReport(string entityType, string entityId)
        {
            var restrequest = new RestRequest("/{entitytype}/{entityid}/htmlreport", Method.GET);
            restrequest.AddUrlSegment("entitytype", entityType);
            restrequest.AddUrlSegment("entityid", entityId);
      
            return await Client.ExecuteAsync<CIRHtml>(restrequest);
        }

        public async Task<ICibilData> GetCibilData(string entityType, string entityId)
        {
            var restrequest = new RestRequest("/{entitytype}/{entityid}/cibildata", Method.GET);
            restrequest.AddUrlSegment("entitytype", entityType);
            restrequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<CibilData>(restrequest);
        }
    }
}
    