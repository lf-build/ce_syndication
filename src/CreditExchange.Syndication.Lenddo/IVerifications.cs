﻿namespace CreditExchange.Syndication.Lenddo
{
    public interface IVerifications
    {
        bool? Name { get; set; }
        bool? University { get; set; }
        bool? Employer { get; set; }
        bool? Phone { get; set; }
        bool? Birthday { get; set; }
        bool? TopEmployer { get; set; }
    }
}