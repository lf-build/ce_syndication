﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Lenddo
{
    public interface IGetApplicationVerificationResponse
    {
        string PartnerScriptId { get; set; }
        int Updated { get; set; }
        List<string> DuplicateProfiles { get; set; }
        bool? VerifiedByFacebook { get; set; }
        string FacebookPhotoUrl { get; set; }
        int Created { get; set; }
        List<string> Flags { get; set; }
        IVerifications Verifications { get; set; }
        string ApplicationId { get; set; }
        string Id { get; set; }
        string PartnerId { get; set; }
        IProbes Probes { get; set; }
    }
}