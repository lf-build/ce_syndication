﻿using LendFoundry.Foundation.Persistence;

namespace CreditExchange.Syndication.Lenddo
{
    public class LenddoData :Aggregate,ILenddoData
    {
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public bool Islinked { get; set; }
        public string Score { get; set; }
    }
}
