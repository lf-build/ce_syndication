﻿namespace CreditExchange.Syndication.Lenddo
{
    public interface IUniversityModel
    {
        string University { get; set; }
    }
}