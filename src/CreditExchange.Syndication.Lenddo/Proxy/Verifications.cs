using Newtonsoft.Json;

namespace CreditExchange.Syndication.Lenddo.Proxy
{
    public class Verifications
    {
        [JsonProperty("name")]
        public bool? Name { get; set; }

        [JsonProperty("university")]
        public bool? University { get; set; }

        [JsonProperty("employer")]
        public bool? Employer { get; set; }

        [JsonProperty("phone")]
        public bool? Phone { get; set; }

        [JsonProperty("birthday")]
        public bool? Birthday { get; set; }

        [JsonProperty("top_employer")]
        public bool? TopEmployer { get; set; }
    }
}