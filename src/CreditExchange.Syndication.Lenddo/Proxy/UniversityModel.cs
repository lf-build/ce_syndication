using Newtonsoft.Json;

namespace CreditExchange.Syndication.Lenddo.Proxy
{
    public class UniversityModel
    {
        [JsonProperty("university")]
        public string University { get; set; }
    }
}