using Newtonsoft.Json;

namespace CreditExchange.Syndication.Lenddo.Proxy
{
    public class EmployerModel
    {
        [JsonProperty("employer")]
        public string Employer { get; set; }
    }
}