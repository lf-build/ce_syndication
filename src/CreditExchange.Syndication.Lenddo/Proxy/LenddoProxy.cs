﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using RestSharp;
using LendFoundry.Foundation.Logging;

namespace CreditExchange.Syndication.Lenddo.Proxy
{
    public class LenddoProxy : ILenddoProxy
    {
        public LenddoProxy(ILenddoConfiguration configuration,ILogger logger)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if(string.IsNullOrWhiteSpace(configuration.ApiId))
                throw new ArgumentNullException(nameof(configuration.ApiId));

            if (string.IsNullOrWhiteSpace(configuration.ApiBaseUrl))
                throw new ArgumentNullException(nameof(configuration.ApiBaseUrl));

            if (string.IsNullOrWhiteSpace(configuration.ApiSecret))
                throw new ArgumentNullException(nameof(configuration.ApiSecret));

            Configuration = configuration;
            Logger = logger;
        }
        private ILogger Logger { get; }
        private ILenddoConfiguration Configuration { get; }


        public GetApplicationVerificationResponse GetClientVerification(string clientId)
        {
            if (string.IsNullOrWhiteSpace(clientId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(clientId));

            var client = new RestClient(Configuration.ApiBaseUrl.TrimEnd('/'));
            var request = new RestRequest(Configuration.ClientVerificationUrl.TrimEnd('/') + "/" + clientId, Method.GET);
           request.AddParameter("partner_script_id", Configuration.PartnerScriptId, ParameterType.QueryString);

            return ExecuteRequest<GetApplicationVerificationResponse>(client, request);
        }

        public GetApplicationScoreResponse GetClientScore(string clientId)
        {
            if (string.IsNullOrWhiteSpace(clientId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(clientId));
            Logger.Info($"[Lenddo] GetClientScore method invoked for data clientId :{clientId}");
            var client = new RestClient(Configuration.ApiBaseUrl.TrimEnd('/'));
            var request = new RestRequest(Configuration.ClientScoreUrl.TrimEnd('/') + "/" + clientId, Method.GET);
            request.AddParameter("partner_script_id", Configuration.PartnerScriptId, ParameterType.QueryString);
            return ExecuteRequest<GetApplicationScoreResponse>(client, request);
        }

        private  T ExecuteRequest<T>(IRestClient client, IRestRequest request) where T : class
        {
            var date = DateTime.UtcNow.ToString("ddd, d MMM yyyy HH:mm:ss") + " GMT";
            var headerAuthKey = GenerateHeaderAuthKey(request.Method.ToString(), date, "/" + request.Resource, null);
            request.AddHeader("Authorization", headerAuthKey);
            request.AddHeader("Date", date);

            var response = client.Execute(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
            {
                Logger.Error($"[Lenddo] Error occured when  { request.Resource } was called with headerAuthKey {headerAuthKey}. Error:", response.ErrorException);
                throw new LenddoException(response.ErrorException.Message);
                
            }

            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                Logger.Error($"[Lenddo] Error occured when  { request.Resource } was called with headerAuthKey {headerAuthKey}.. Error:", response.Content);
                throw new LenddoException(response.Content);
            }

            if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
            {
                Logger.Error($"[Lenddo] Error occured when  { request.Resource } was called with headerAuthKey {headerAuthKey}.. Error:", response.StatusDescription);
                throw new LenddoException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");
            }

            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                Logger.Error($"[Lenddo] Error occured when  { request.Resource } was called with headerAuthKey {headerAuthKey}. Error:", response.Content);
                throw new LenddoException(response.Content);
            }

            if (response.StatusCode != HttpStatusCode.OK)
            {
                Logger.Error($"[Lenddo] Error occured when  { request.Resource } was called with headerAuthKey {headerAuthKey}. Error:", response.Content ?? "");
                throw new LenddoException(response.Content);
            }

            try
            {
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (LenddoException exception)
            {
                Logger.Error($"[Lenddo] Error occured when  { request.Resource } was called with headerAuthKey {headerAuthKey}. Error:", response.Content);
                throw new LenddoException("Unable to deserialize:" + response.Content, exception);
            }
        }

        protected string GenerateHeaderAuthKey(string method, string date, string uriAbsolutePath, string requestBodyJson)
        {
            var str = "LENDDO " + Configuration.ApiId + ":";
            var requestBody = "";
            if (!string.IsNullOrEmpty(requestBodyJson))
            {
                using (var md5Hash = MD5.Create())
                {
                    requestBody = GetMd5Hash(md5Hash, requestBodyJson);
                }
            }
            var content = string.Concat(method, "\n", requestBody, "\n", date, "\n", uriAbsolutePath);
            var valueAndReset = Encode(content, Configuration.ApiSecret);
            return str + Convert.ToBase64String(valueAndReset);
        }

        private static string GetMd5Hash(MD5 md5Hash, string input)
        {
            var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            var sBuilder = new StringBuilder();
            for (var i = 0; i < data.Length; i++)
            {
                sBuilder.Append(i.ToString("x2"));
            }
            return sBuilder.ToString();
        }

        private static byte[] Encode(string input, string key)
        {
            var keyBytes = Encoding.UTF8.GetBytes(key);

            var myhmacsha1 = new HMACSHA1(keyBytes);
            var byteArray = Encoding.ASCII.GetBytes(input);
            var stream = new MemoryStream(byteArray);
            return myhmacsha1.ComputeHash(stream);
        }


    }
}