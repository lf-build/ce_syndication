﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.Lenddo.Proxy
{
    public class GetApplicationScoreResponse
    {
        [JsonProperty("partner_script_id")]
        public string PartnerScriptId { get; set; }

        [JsonProperty("created")]
        public int Created { get; set; }

        [JsonProperty("score")]
        public int Score { get; set; }

        [JsonProperty("flags")]
        public string[] Flags { get; set; }

        [JsonProperty("client_id")]
        public string ClientId { get; set; }

        [JsonProperty("partner_id")]
        public string PartnerId { get; set; }
    }
}