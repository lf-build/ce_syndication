﻿namespace CreditExchange.Syndication.Lenddo.Proxy
{
    public interface ILenddoProxy
    {
        GetApplicationVerificationResponse GetClientVerification(string clientId);
        GetApplicationScoreResponse GetClientScore(string clientId);
    }
}