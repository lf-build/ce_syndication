using Newtonsoft.Json;

namespace CreditExchange.Syndication.Lenddo.Proxy
{
    public class Probes
    {
        [JsonProperty("name")]
        public string[] Name { get; set; }

        [JsonProperty("university")]
        public UniversityModel University { get; set; }

        [JsonProperty("employer")]
        public EmployerModel Employer { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("birthday")]
        public int?[] Birthday { get; set; }

        [JsonProperty("top_employer")]
        public string TopEmployer { get; set; }
    }
}