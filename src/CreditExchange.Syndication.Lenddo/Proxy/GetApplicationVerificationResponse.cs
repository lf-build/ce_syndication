using Newtonsoft.Json;

namespace CreditExchange.Syndication.Lenddo.Proxy
{
    public class GetApplicationVerificationResponse
    {
        [JsonProperty("partner_script_id")]
        public string PartnerScriptId { get; set; }

        [JsonProperty( "updated")]
        public int Updated { get; set; }

        [JsonProperty( "duplicate_profiles")]
        public string[] DuplicateProfiles { get; set; }

        [JsonProperty( "verified_by_facebook")]
        public bool? VerifiedByFacebook { get; set; }

        [JsonProperty("facebook_photo_url")]
        public string FacebookPhotoUrl { get; set; }

        [JsonProperty( "created")]
        public int Created { get; set; }

        [JsonProperty( "flags")]
        public string[] Flags { get; set; }

        [JsonProperty("verifications")]
        public Verifications Verifications { get; set; }

        [JsonProperty( "client_id")]
        public string ApplicationId { get; set; }

        [JsonProperty( "_id")]
        public string Id { get; set; }

        [JsonProperty( "partner_id")]
        public string PartnerId { get; set; }

        [JsonProperty( "probes")]
        public Probes Probes { get; set; }
    }
}