﻿using System.Collections.Generic;
using System.Linq;

namespace CreditExchange.Syndication.Lenddo
{
    public class GetApplicationScoreResponse : IGetApplicationScoreResponse
    {

        public GetApplicationScoreResponse()
        {
            
        }

        public GetApplicationScoreResponse(Proxy.GetApplicationScoreResponse getApplicationScoreResponse)
        {
            PartnerScriptId = getApplicationScoreResponse.PartnerScriptId;
            Created = getApplicationScoreResponse.Created;
            Score = getApplicationScoreResponse.Score;
            if (getApplicationScoreResponse.Flags != null && getApplicationScoreResponse.Flags.Any())
                Flags = new List<string>(getApplicationScoreResponse.Flags);
            ClientId = getApplicationScoreResponse.ClientId;
            PartnerId = getApplicationScoreResponse.PartnerId;
        }

        public string PartnerScriptId { get; set; }
        public int Created { get; set; }
        public int Score { get; set; }
        public List<string> Flags { get; set; }
        public string ClientId { get; set; }
        public string PartnerId { get; set; }
    }
}