﻿namespace CreditExchange.Syndication.Lenddo
{
    public class EmployerModel : IEmployerModel
    {
        public EmployerModel()
        {
            
        }

        public EmployerModel(Proxy.EmployerModel employerModel)
        {
            Employer = employerModel.Employer;
        }


        public string Employer { get; set; }
    }
}