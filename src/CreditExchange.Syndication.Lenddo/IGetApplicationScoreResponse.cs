﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Lenddo
{
    public interface IGetApplicationScoreResponse
    {
        string PartnerScriptId { get; set; }
        int Created { get; set; }
        int Score { get; set; }
        List<string> Flags { get; set; }
        string ClientId { get; set; }
        string PartnerId { get; set; }
    }
}