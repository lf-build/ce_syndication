﻿using LendFoundry.Foundation.Persistence;

namespace CreditExchange.Syndication.Lenddo
{
    public interface ILenddoData : IAggregate
    {
         string EntityId { get; set; }
         string EntityType { get; set; }
         bool Islinked { get; set; }
         string Score { get; set; }
    }
}
