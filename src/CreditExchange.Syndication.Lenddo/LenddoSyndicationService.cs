﻿using System;
using System.Threading.Tasks;
using CreditExchange.Syndication.Lenddo.Proxy;

namespace CreditExchange.Syndication.Lenddo
{
    public class LenddoSyndicationService : ILenddoSyndicationService
    {
        public LenddoSyndicationService(ILenddoProxy proxy)
        {
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));

            Proxy = proxy;
          
        }
        public static string ServiceName { get; } = "lenddo";

        private ILenddoProxy Proxy { get; }
      
        public async Task<IGetApplicationVerificationResponse> GetClientVerification(string entityType, string entityId, string clientId)
        {
         
            var response = await Task.Run(() => Proxy.GetClientVerification(clientId));
            return new GetApplicationVerificationResponse(response);
         
            
        }

        public async Task<IGetApplicationScoreResponse> GetClientScore(string entityType, string entityId, string clientId)
        {
                  var response = await Task.Run(() => Proxy.GetClientScore(clientId));
               return new GetApplicationScoreResponse(response);
              
          

        }
        
    
    }
}