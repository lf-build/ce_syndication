﻿namespace CreditExchange.Syndication.Lenddo
{
    public interface IEmployerModel
    {
        string Employer { get; set; }
    }
}