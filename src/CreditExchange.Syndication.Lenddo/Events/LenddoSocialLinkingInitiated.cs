﻿using LendFoundry.SyndicationStore.Events;

namespace CreditExchange.Syndication.Lenddo.Events
{
    public class LenddoSocialLinkingInitiated : SyndicationCalledEvent
    {
    }
}
