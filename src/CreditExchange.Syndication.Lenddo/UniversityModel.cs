﻿namespace CreditExchange.Syndication.Lenddo
{
    public class UniversityModel : IUniversityModel
    {
        public UniversityModel()
        {
            
        }

        public UniversityModel(Proxy.UniversityModel universityModel)
        {
            University = universityModel.University;
        }
        

        public string University { get; set; }
    }
}