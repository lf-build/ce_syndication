﻿
using CreditExchange.Syndication.Lenddo;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Lenddo
{
    public interface ILenddoSyndicationService
    {
       Task<IGetApplicationVerificationResponse> GetClientVerification(string entityType, string entityId,string clientId);
       Task<IGetApplicationScoreResponse> GetClientScore(string entityType, string entityId,string clientId);
    }
}