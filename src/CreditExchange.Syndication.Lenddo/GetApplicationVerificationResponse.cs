using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace CreditExchange.Syndication.Lenddo
{
    public class GetApplicationVerificationResponse : IGetApplicationVerificationResponse
    {

        public GetApplicationVerificationResponse()
        {
            
        }

        public GetApplicationVerificationResponse(
            Proxy.GetApplicationVerificationResponse getApplicationVerificationResponse)
        {
            PartnerScriptId = getApplicationVerificationResponse.PartnerScriptId;
            Updated = getApplicationVerificationResponse.Updated;
            if (getApplicationVerificationResponse.DuplicateProfiles != null &&
                getApplicationVerificationResponse.DuplicateProfiles.Any())
                DuplicateProfiles = new List<string>(getApplicationVerificationResponse.DuplicateProfiles);
            VerifiedByFacebook = getApplicationVerificationResponse.VerifiedByFacebook;
            FacebookPhotoUrl = getApplicationVerificationResponse.FacebookPhotoUrl;
            Created = getApplicationVerificationResponse.Created;
            if (getApplicationVerificationResponse.Flags != null &&
                getApplicationVerificationResponse.Flags.Any())
                Flags = new List<string>(getApplicationVerificationResponse.Flags);
            if (getApplicationVerificationResponse.Verifications != null)
                Verifications = new Verifications(getApplicationVerificationResponse.Verifications);
            ApplicationId = getApplicationVerificationResponse.ApplicationId;
            Id = getApplicationVerificationResponse.Id;
            PartnerId = getApplicationVerificationResponse.PartnerId;
            if (getApplicationVerificationResponse.Probes != null)
                Probes = new Probes(getApplicationVerificationResponse.Probes);
        }

        public string PartnerScriptId { get; set; }
        public int Updated { get; set; }
        public List<string> DuplicateProfiles { get; set; }
        public bool? VerifiedByFacebook { get; set; }
        public string FacebookPhotoUrl { get; set; }
        public int Created { get; set; }
        public List<string> Flags { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IVerifications, Verifications>))]
        public IVerifications Verifications { get; set; }
        public string ApplicationId { get; set; }
        public string Id { get; set; }
        public string PartnerId { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IProbes, Probes>))]
        public IProbes Probes { get; set; }
    }
}