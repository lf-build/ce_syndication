﻿using System.Security.Cryptography.X509Certificates;

namespace CreditExchange.Syndication.Lenddo
{
    public class LenddoConfiguration : ILenddoConfiguration
    {
        public string ApiId { get; set; } = "57847605f7a5795ec7345d38";

        public string ApiSecret { get; set; } =
            "mi0ihoYnX4DI7E1qP7+lUWylmrzpM1E2ClR9xCDr0YWpb3jY9Mw9SZU1nxizsPPhG/eXqkQGj24PYMoNPrwMgg==";

        public string ApiBaseUrl { get; set; } = "https://scoreservice.lenddo.com/";
        public string ClientVerificationUrl { get; set; } = "ClientVerification";
        public string ClientScoreUrl { get; set; } = "ClientScore";

        public string PartnerScriptId { get; set; } = "57846ad1f7a5795ec7345d34";
    }
}