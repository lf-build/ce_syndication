﻿namespace CreditExchange.Syndication.Lenddo
{
    public interface ILenddoConfiguration
    {
        string ApiId { get; set; }
        string ApiSecret { get; set; }
        string ApiBaseUrl { get; set; }
        string ClientVerificationUrl { get; set; }
        string ClientScoreUrl { get; set; }

        string PartnerScriptId { get; set; }


    }
}