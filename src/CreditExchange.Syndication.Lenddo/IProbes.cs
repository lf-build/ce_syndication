﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Lenddo
{
    public interface IProbes
    {
        List<string> Name { get; set; }
        IUniversityModel University { get; set; }
        IEmployerModel Employer { get; set; }
        string Phone { get; set; }
        List<int?> Birthday { get; set; }
        string TopEmployer { get; set; }
    }
}