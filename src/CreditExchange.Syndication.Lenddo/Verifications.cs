namespace CreditExchange.Syndication.Lenddo
{
    public class Verifications : IVerifications
    {
        public Verifications()
        {
            
        }

        public Verifications(Proxy.Verifications verifications)
        {
            Name = verifications.Name;
            University = verifications.University;
            Employer = verifications.Employer;
            Phone = verifications.Phone;
            Birthday = verifications.Birthday;
            TopEmployer = verifications.TopEmployer;
        }

        public bool? Name { get; set; }
        public bool? University { get; set; }
        public bool? Employer { get; set; }
        public bool? Phone { get; set; }
        public bool? Birthday { get; set; }
        public bool? TopEmployer { get; set; }
    }
}