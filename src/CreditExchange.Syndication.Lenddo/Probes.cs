﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace CreditExchange.Syndication.Lenddo
{
    public class Probes : IProbes
    {
        public Probes()
        {
            
        }

        public Probes(Proxy.Probes probes)
        {
            if (probes.Name != null && probes.Name.Any())
                Name = new List<string>(probes.Name);

            if (probes.University != null)
                University = new UniversityModel(probes.University);

            if (probes.Employer != null)
                Employer = new EmployerModel(probes.Employer);

            Phone = probes.Phone;

            if (probes.Birthday != null && probes.Birthday.Any())
                Birthday = new List<int?>(probes.Birthday);

            TopEmployer = probes.TopEmployer;
        }

        public List<string> Name { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IUniversityModel, UniversityModel>))]
        public IUniversityModel University { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmployerModel, EmployerModel>))]
        public IEmployerModel Employer { get; set; }
        public string Phone { get; set; }
        public List<int?> Birthday { get; set; }
        public string TopEmployer { get; set; }
    }
}