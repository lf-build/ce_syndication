﻿using System;
using System.Runtime.Serialization;

namespace CreditExchange.Syndication.Lenddo
{
    [Serializable]
    public class LenddoException : Exception
    {
        public string ErrorCode { get; set; }

        public LenddoException()
        {
        }
        public LenddoException(string message) : this(null, message, null)
        {
        }

        public LenddoException(string errorCode,string message) : this(errorCode, message, null)
        {
        }

        public LenddoException(string errorCode, string message, Exception innerException) : base(message, innerException)
        {
            ErrorCode = errorCode;
        }
        public LenddoException(string message, Exception innerException) : this(null, message, innerException)
        {
        }

        protected LenddoException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}