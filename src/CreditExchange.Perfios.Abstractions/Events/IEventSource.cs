﻿namespace CreditExchange.Perfios.Events
{
    public interface IEventSource
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        string ReferenceNumber { get; set; }
        object Request { get; set; }
        object Response { get; set; }
    }
}