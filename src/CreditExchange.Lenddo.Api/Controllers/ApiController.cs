﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.Lenddo;
#if DOTNET2
using  Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using LendFoundry.Foundation.Logging;
using CreditExchange.Lenddo.Abstractions;

namespace CreditExchange.Lenddo.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(ILenddoService service,ILogger logger):base(logger)
        {
            Service = service;
        }

        private ILenddoService Service { get; }

        [HttpGet("{entitytype}/{entityid}/{clientid}/verification")]
#if DOTNET2
        [ProducesResponseType(typeof(IGetApplicationVerificationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetClientVerification(string entityType, string entityId, string clientid)
        {
            Logger.Info($"[Lenddo] GetClientVerification endpoint invoked for [{entityType}] with id #{entityId} with  clientid {clientid}");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var response = await Task.Run(() => Service.GetClientVerification(entityType, entityId, clientid));
                    return Ok(response);
                }
                catch(LenddoException exception)
                {
                    Logger.Error($"[Lenddo] Error occured when GetClientVerification endpoint was called for [{entityType}] with id #{entityId} with  clientid { clientid}", exception);
                    return ErrorResult.BadRequest(exception.Message);
                }
                
            });
        }

        [HttpGet("{entitytype}/{entityid}/{clientid}/score")]
#if DOTNET2
        [ProducesResponseType(typeof(IGetApplicationScoreResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif        
        public async Task<IActionResult> GetClientScore(string entityType, string entityId, string clientid)
        {
            Logger.Info($"[Lenddo] GetClientScore endpoint invoked for [{entityType}] with id #{entityId} with  clientid {clientid}");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var response = (await Task.Run(() => Service.GetClientScore(entityType, entityId, clientid)));
                    return Ok(response);
                }
                catch(LenddoException exception)
                {
                    Logger.Error($"[Lenddo] Error occured when GetClientScore endpoint was called for [{entityType}] with id #{entityId} with  clientid { clientid}", exception);
                    if (exception.Message.Contains("NOT_FOUND"))
                        throw new NotFoundException("NOT FOUND");
                    return ErrorResult.BadRequest(exception.Message);
                }
                
            });
        }
    }
}
