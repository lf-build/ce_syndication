﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public class YodleeBaseRequest: IYodleeBaseRequest
    {
        public YodleeBaseRequest()
        {
            
        }

        public YodleeBaseRequest(IYodleeBaseRequest yodleeBaseRequest)
        {
            CobrandToken = yodleeBaseRequest?.CobrandToken;
            UserSessionToken = yodleeBaseRequest?.UserSessionToken;
        }
        public string CobrandToken { get; set; }
        public string UserSessionToken { get; set; }
    }
}
