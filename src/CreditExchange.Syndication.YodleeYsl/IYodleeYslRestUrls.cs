﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IYodleeYslRestUrls
    {
        string BaseFetchReportUrl { get; set; }
        string BaseItemSummariesUrl { get; set; }
        string BaseRestUrl { get; set; }
        string CobrandLogin { get; set; }
        string FetchReport { get; set; }
        string GetBankLoginForm { get; set; }
        string GetMfaDetails { get; set; }
        string ItemSummaries { get; set; }
        string LoginUser { get; set; }
        string PostMfaDetails { get; set; }
        string RefreshDetails { get; set; }
        string RegisterUser { get; set; }
       
        string PostBankLoginForm { get; set; }
        string UpdateBankLoginForm { get; set; }
        string StartRefreshDetails { get; set; }
        string FastLinkUrl { get; set; }
       
        string AccessToken { get; set; }
        string GetUserAccounts { get; set; }
       
        string UserCredentialsInstanceType { get; set; }
        string UserProfileInstanceType { get; set; }
        string PreferredCurrency { get; set; }
        string PreferredDateFormate { get; set; }

        string SearchForBank { get; set; }
        string RemoveSiteAccount { get; set; }
        string SiteAccountId { get; set; }
    }
}