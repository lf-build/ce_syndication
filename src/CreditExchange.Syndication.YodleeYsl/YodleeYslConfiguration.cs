﻿
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class YodleeYslConfiguration : IYodleeYslConfiguration
    {
        public YodleeYslConfiguration()
        {
            //RestUrls = new YodleeYslRestUrls();
            //FastLink = new FastLinkConfiguration();
            //Cobrand = new CobrandConfiguration();
            //User = new UserConfiguartion();

        }
       
        public string UserCredentialsInstanceType { get; set; } = "com.yodlee.ext.login.PasswordCredentials";
        public string UserProfileInstanceType { get; set; } = "com.yodlee.core.usermanagement.UserProfile";
        public string PreferredCurrency { get; set; } = "PREFERRED_CURRENCY~USD";
        public string PreferredDateFormate { get; set; } = "PREFERRED_DATE_FORMAT~MM/dd/yyyy";
        [JsonConverter(typeof(InterfaceConverter<IUserConfiguration, UserConfiguartion>))]
        public IUserConfiguration User { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IYodleeYslRestUrls, YodleeYslRestUrls>))]
    
        public IYodleeYslRestUrls RestUrls { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ICobrandConfiguration, CobrandConfiguration>))]
        public ICobrandConfiguration Cobrand { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IFastLinkConfiguration, FastLinkConfiguration>))]
        public IFastLinkConfiguration FastLink { get; set; }
    }
}
