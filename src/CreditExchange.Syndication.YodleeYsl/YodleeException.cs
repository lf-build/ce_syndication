﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.YodleeYsl
{

    [Serializable]
    public class YodleeException : Exception
    {
        public YodleeException()
        {
        }

        public YodleeException(string message) : base(message)
        {
            if (!string.IsNullOrWhiteSpace(message))
            {
                try
                {
                    var errorsresponse = JsonConvert.DeserializeObject<ErrorResponse>(message);
                    Description = errorsresponse.ErrorMessage;
                }
                catch(Exception ex)
                {
                    Description = Message;
                }
              
            }
            else
            {
                Description = "Bad Request";
            }

        }
        public string Description { get; set; }
        public YodleeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected YodleeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
