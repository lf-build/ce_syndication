﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IYodleeBaseRequest
    {
        string CobrandToken { get; set; }
        string UserSessionToken { get; set; }
    }
}
