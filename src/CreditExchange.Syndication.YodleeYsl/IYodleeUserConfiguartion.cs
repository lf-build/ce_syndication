﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.YodleeYsl
{
   public interface IUserConfiguration
    {
         string LoginName { get; set; } 
         string Password { get; set; } 
         string Locale { get; set; } 
    }
}
