﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public class FastLinkConfiguration :IFastLinkConfiguration
    {
        public string FastLinkAppId { get; set; } = "10003600";
        public string FastLinkLaunchForm { get; set; } = "<form id='fastlinkForm' style='display:none' action='##FastLinkBaseUrl##' method='POST'><input type='text' name='app' value='##AppId##' /><input type='text' name='rsession' value='##UserSessionToken##' /><input type='text' name='token'  value='##FastlinkAccessToken##' /><input type='text' name='redirectReq' value='##IsRedirect##'/><input type='text' name='extraParams' value='##ExtraParams##' /></form><script>document.getElementById('fastlinkForm').submit();</script>";
        public string FastLinkFlow { get; set; } = "0";
        public string BankSearchKeyWord { get; set; } = "";
        public string SiteId { get; set; } = "";
        public string CallBackUrl { get; set; } = "";
        public string SiteAccountId { get; set; } = "";
    }
}
