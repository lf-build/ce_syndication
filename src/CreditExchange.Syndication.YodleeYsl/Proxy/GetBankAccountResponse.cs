﻿
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl.Proxy
{
    public class GetBankAccountResponse: IGetBankAccountResponse
    {
        [JsonConverter(typeof(YodleeListJsonConverter<List<BankAccount>, IBankAccount>))]
        [JsonProperty("account")]
        public IList<IBankAccount> Accounts { get; set; }
    }
}
