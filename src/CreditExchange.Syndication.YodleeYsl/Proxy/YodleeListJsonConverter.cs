﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Linq;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class YodleeListJsonConverter<T, I> : JsonConverter 
        where T : IList 
    {
        public override bool CanConvert(Type objectType) => true;

        public override object ReadJson(JsonReader reader,
         Type objectType, object existingValue, JsonSerializer serializer)
        {
            return serializer.Deserialize<T>(reader).Cast<I>().ToList();
        }

        public override void WriteJson(JsonWriter writer,
            object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }
    }
}
