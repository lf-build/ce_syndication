﻿using CreditExchange.Syndication.YodleeYsl.Request;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl.Proxy
{
    public interface IYodleeYslRestProxy
    {
        IFinalReport FetchReport(string cobrandSessionId, string userSessionId, string itemId, string itemAccountId);
        IBankLoginResponse GetBankLoginForm(string cobrandSessionId, string providerId);
        IList<IItemSummaries> GetItemSummaries(string cobrandSessionId, string userSessionId, string providerAccountId);
        IMfaRefreshResponse GetMfaStatusWithRefreshInformation(string cobrandSessionId, string userSessionId, string providerAccountId);
        IFinalRefreshResponse GetRefreshStatus(string cobrandSessionId, string userSessionId, string providerAccountId);
        ICobrandLoginResponse LoginForCobrand();
        IUserLoginResponse LoginUser(string cobrandSessionId, string loginName, string password);
        IProviderAccountRefreshStatus PostBankLoginForm(string cobrandSessionId, string userSessionId, string providerId, IUpdateBankLogin loginForm);
        string UpdateBankLoginForm(string cobrandSessionId, string userSessionId, string providerAccountId, IUpdateBankLogin loginForm);
        IUserRegistrationResponse RegisterUser(string cobrandSessionId, IUserRegistrationRequest request);
        ISearchBankResponse SearchBank(string cobrandSessionId, string searchName);
        string SendMfaDetails(string cobrandSessionId, string userSessionId, string providerAccountId, dynamic mfaChallenge);

        string StartSiteRefresh(string cobrandSessionId, string userSessionId, string providerAccountId);

        IFastlinkAccessTokenResponse GetFastlinkAccessToken(Proxy.FastlinkAccessTokenRequest fastlinkAccessTokenRequest);
        IGetBankAccountResponse GetUserBankAccounts(string cobrandSessionId, string userSessionId);
        string GetFalstLinkUrl(string fastlinkToken, string userSessionId, IFastLinkUrlRequest fastlinkReuest);

        string RemoveSiteAccount(string cobrandSessionId, string userSessionId, string providerAccountId);
    }
}