﻿using System;

namespace CreditExchange.Syndication.YodleeYsl.Proxy
{
    public interface IBankAccount
    {
        string Container { get; set; }

        long ProviderAccountId { get; set; }

        string AccountName { get; set; }

        long Id { get; set; }
        string AccountNumber { get; set; }

        IBalance AvailableBalance { get; set; }

        string AccountType { get; set; }
        DateTime? CreatedDate { get; set; }

        bool? IsAsset { get; set; }

        IBalance Balance { get; set; }

        long ProviderId { get; set; }

        string ProviderName { get; set; }
        IRefreshInfo RefreshInfo { get; set; }
        string AccountStatus { get; set; }
    }
}
