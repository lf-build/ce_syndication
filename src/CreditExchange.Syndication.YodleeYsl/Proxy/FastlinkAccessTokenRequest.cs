﻿namespace CreditExchange.Syndication.YodleeYsl.Proxy
{
    public class FastlinkAccessTokenRequest : YodleeBaseRequest, IFastlinkAccessTokenRequest
    {
        public FastlinkAccessTokenRequest()
        {
        }

        public FastlinkAccessTokenRequest(Request.IFastlinkAccessTokenRequest fastlinkAccessTokenRequest) : base(fastlinkAccessTokenRequest)
        {
        }
    }
}
