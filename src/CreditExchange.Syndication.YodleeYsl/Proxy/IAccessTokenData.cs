﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl.Proxy
{
    public interface IAccessTokenData
    {
        IList<IAccessToken> AccessTokens { get; set; }
    }
}
