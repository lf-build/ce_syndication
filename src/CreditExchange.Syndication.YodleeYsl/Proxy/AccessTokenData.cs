﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl.Proxy
{
    public class AccessTokenData: IAccessTokenData
    {
        [JsonConverter(typeof(InterfaceListConverter<IAccessToken,AccessToken>))]
        [JsonProperty("accessTokens")]
        public IList<IAccessToken> AccessTokens { get; set; }
    }
}
