﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl.Proxy
{
    public class Balance: IBalance
    {
        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }
    }
}
