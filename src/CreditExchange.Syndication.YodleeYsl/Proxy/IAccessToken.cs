﻿namespace CreditExchange.Syndication.YodleeYsl.Proxy
{
    public interface IAccessToken
    {
         string AppId { get; set; }
         string Value { get; set; }
    }
}
