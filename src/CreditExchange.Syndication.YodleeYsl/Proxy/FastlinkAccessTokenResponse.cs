﻿
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl.Proxy
{
    public class FastlinkAccessTokenResponse : IFastlinkAccessTokenResponse
    {
        [JsonConverter(typeof(InterfaceConverter<IAccessTokenData, AccessTokenData>))]
        [JsonProperty("user")]
        public IAccessTokenData AccessTokenData { get; set; }
    }
}
