﻿using CreditExchange.Syndication.YodleeYsl.Request;
using LendFoundry.Foundation.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace CreditExchange.Syndication.YodleeYsl.Proxy
{
    public class YodleeYslRestProxy : IYodleeYslRestProxy
    {

        public YodleeYslRestProxy(IYodleeYslConfiguration configuration, ILogger logger)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration)); 

            Configuration = configuration;
            Logger = logger;

        }
        private ILogger Logger { get; }
        private IYodleeYslConfiguration Configuration { get; set; }

        private string GetPossibleError(string content)
        {
            var errorResponse = JsonConvert.DeserializeObject<ErrorResponse>(content);
            if (errorResponse != null && string.IsNullOrWhiteSpace(errorResponse.ErrorMessage) == false)
            {
                throw new ApplicationException(errorResponse.ErrorMessage);
            }

            return content;
        }

        public ICobrandLoginResponse LoginForCobrand()
        {
            
            ICobrandLoginRequest request = new CobrandLoginRequest(Configuration.Cobrand.LoginName, Configuration.Cobrand.Password, Configuration.Cobrand.Locale);
            Logger.Info($"[Yodlee] LoginForCobrand method invoked for data { (request != null ? JsonConvert.SerializeObject(request) : null)}");
            var client = new RestClient(Configuration.RestUrls.BaseRestUrl + Configuration.RestUrls.CobrandLogin);
            var restrequest = new RestRequest(Method.POST);
            restrequest.AddParameter("text/json", JsonConvert.SerializeObject(request), ParameterType.RequestBody);
            return ExecuteRequest<CobrandLoginResponse>(client, restrequest);
          
        }

        public IUserRegistrationResponse RegisterUser(string cobrandSessionId, IUserRegistrationRequest request)
        {

            //var client = new RestClient(Configuration.RestUrls.BaseRestUrl + Configuration.RestUrls.RegisterUser);
            //var restrequest = new RestRequest(Method.POST);
            //restrequest.AddParameter("text/json", JsonConvert.SerializeObject(request), ParameterType.RequestBody);

            //restrequest.AddHeader("Authorization", "{cobSession=" + cobrandSessionId + "}");
            //return ExecuteRequest<UserRegistrationResponse>(client, restrequest);


            var httpRequest = new Http();
            httpRequest.Url = new Uri(Configuration.RestUrls.BaseRestUrl + Configuration.RestUrls.RegisterUser);
            var authorizationHeader = new HttpHeader();
            authorizationHeader.Name = "Authorization";
            authorizationHeader.Value = "{cobSession=" + cobrandSessionId + "}";
            httpRequest.Headers.Add(authorizationHeader);
            httpRequest.RequestContentType = "application/json";
            httpRequest.RequestBody = JsonConvert.SerializeObject(request);
           var response = httpRequest.Post();
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new YodleeException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK)
                throw new YodleeException(response.Content);
            return JsonConvert.DeserializeObject<UserRegistrationResponse>(response.Content);
        }

        public IUserLoginResponse LoginUser(string cobrandSessionId,string loginName,string password)
        {
            IUserLoginRequest request = new UserLoginRequest(loginName,password, Configuration.User.Locale);
            var client = new RestClient(Configuration.RestUrls.BaseRestUrl + Configuration.RestUrls.LoginUser);
            var restrequest = new RestRequest(Method.POST);
            restrequest.AddParameter("text/json", JsonConvert.SerializeObject(request), ParameterType.RequestBody);
            restrequest.AddHeader("Authorization", "{cobSession=" + cobrandSessionId + "}");
            return ExecuteRequest<UserLoginResponse>(client, restrequest);


            //var httpRequest = new Http();
            //httpRequest.Url = new Uri(Configuration.RestUrls.BaseRestUrl + Configuration.RestUrls.LoginUser);
            //var authorizationHeader = new HttpHeader();
            //authorizationHeader.Name = "Authorization";
            //authorizationHeader.Value = "{cobSession=" + cobrandSessionId + "}";
            //httpRequest.Headers.Add(authorizationHeader);
            //httpRequest.RequestContentType = "application/json";
            //httpRequest.RequestBody = JsonConvert.SerializeObject(request);
            //return JsonConvert.DeserializeObject<UserLoginResponse>(GetPossibleError(httpRequest.Post().Content));
        }

        public ISearchBankResponse SearchBank(string cobrandSessionId, string searchName)
        {

            //var client = new RestClient(Configuration.RestUrls.BaseRestUrl + Configuration.RestUrls.SearchForBank);
            //var restrequest = new RestRequest(Method.GET);
            //restrequest.AddParameter()
            //restrequest.AddHeader("Authorization", "{cobSession=" + cobrandSessionId + "}");
            //return ExecuteRequest<SearchBankResponse>(client, restrequest);


            var httpRequest = new Http();
            httpRequest.Url = new Uri(string.Format(Configuration.RestUrls.BaseRestUrl + Configuration.RestUrls.SearchForBank, searchName));
            var authorizationHeader = new HttpHeader();
            authorizationHeader.Name = "Authorization";
            authorizationHeader.Value = "{cobSession=" + cobrandSessionId + "}";
            httpRequest.Headers.Add(authorizationHeader);
            httpRequest.RequestContentType = "application/json";
            return JsonConvert.DeserializeObject<SearchBankResponse>(GetPossibleError(httpRequest.Get().Content));
        }

        public IBankLoginResponse GetBankLoginForm(string cobrandSessionId, string providerId)
        {
            var httpRequest = new Http();
            httpRequest.Url = new Uri(string.Format(Configuration.RestUrls.BaseRestUrl + Configuration.RestUrls.GetBankLoginForm, providerId));
            var authorizationHeader = new HttpHeader();
            authorizationHeader.Name = "Authorization";
            authorizationHeader.Value = "{cobSession=" + cobrandSessionId + "}";
            httpRequest.Headers.Add(authorizationHeader);
            httpRequest.RequestContentType = "application/json";
            return JsonConvert.DeserializeObject<BankLoginResponse>(GetPossibleError(httpRequest.Get().Content));
        }

        public IProviderAccountRefreshStatus PostBankLoginForm(string cobrandSessionId, string userSessionId, string providerId, IUpdateBankLogin loginForm)
        {
            var httpRequest = new Http();
            httpRequest.Url = new Uri(string.Format(Configuration.RestUrls.BaseRestUrl + Configuration.RestUrls.PostBankLoginForm, providerId));
            var authorizationHeader = new HttpHeader();
            authorizationHeader.Name = "Authorization";
            authorizationHeader.Value = "{cobSession=" + cobrandSessionId + ", userSession=" + userSessionId + "}";
            httpRequest.Headers.Add(authorizationHeader);
            httpRequest.RequestContentType = "application/json";
            httpRequest.RequestBody = JsonConvert.SerializeObject(loginForm);
            return JsonConvert.DeserializeObject<ProviderAccountRefreshStatus>(GetPossibleError(httpRequest.Post().Content));
        }

        public string UpdateBankLoginForm(string cobrandSessionId, string userSessionId, string providerAccountId, IUpdateBankLogin loginForm)
        {
            var httpRequest = new Http();
            httpRequest.Url = new Uri(string.Format(Configuration.RestUrls.BaseRestUrl + Configuration.RestUrls.UpdateBankLoginForm, providerAccountId));
            var authorizationHeader = new HttpHeader();
            authorizationHeader.Name = "Authorization";
            authorizationHeader.Value = "{cobSession=" + cobrandSessionId + ", userSession=" + userSessionId + "}";
            httpRequest.Headers.Add(authorizationHeader);
            httpRequest.RequestContentType = "application/json";
            httpRequest.RequestBody = JsonConvert.SerializeObject(loginForm);
            return GetPossibleError(httpRequest.Put().Content);
        }

        public IMfaRefreshResponse GetMfaStatusWithRefreshInformation(string cobrandSessionId, string userSessionId, string providerAccountId)
        {
            var httpRequest = new Http();
            httpRequest.Url = new Uri(string.Format(Configuration.RestUrls.BaseRestUrl + Configuration.RestUrls.GetMfaDetails, providerAccountId));
            var authorizationHeader = new HttpHeader();
            authorizationHeader.Name = "Authorization";
            authorizationHeader.Value = "{cobSession=" + cobrandSessionId + ", userSession=" + userSessionId + "}";
            httpRequest.Headers.Add(authorizationHeader);
            httpRequest.RequestContentType = "application/json";
            return JsonConvert.DeserializeObject<MfaRefreshResponse>(GetPossibleError(httpRequest.Get().Content));
        }

        public string SendMfaDetails(string cobrandSessionId, string userSessionId, string providerAccountId, dynamic mfaChallenge)
        {
            var httpRequest = new Http();
            httpRequest.Url = new Uri(string.Format(Configuration.RestUrls.BaseRestUrl + Configuration.RestUrls.PostMfaDetails, providerAccountId));
            var authorizationHeader = new HttpHeader();
            authorizationHeader.Name = "Authorization";
            authorizationHeader.Value = "{cobSession=" + cobrandSessionId + ", userSession=" + userSessionId + "}";
            httpRequest.Headers.Add(authorizationHeader);
            httpRequest.RequestContentType = "application/json";
            httpRequest.RequestBody = JsonConvert.SerializeObject(mfaChallenge);
            return GetPossibleError(httpRequest.Put().Content);
        }

        public IFinalRefreshResponse GetRefreshStatus(string cobrandSessionId, string userSessionId, string providerAccountId)
        {
            var httpRequest = new Http();
            httpRequest.Url = new Uri(string.Format(Configuration.RestUrls.BaseRestUrl + Configuration.RestUrls.RefreshDetails, providerAccountId));
            var authorizationHeader = new HttpHeader();
            authorizationHeader.Name = "Authorization";
            authorizationHeader.Value = "{cobSession=" + cobrandSessionId + ", userSession=" + userSessionId + "}";
            httpRequest.Headers.Add(authorizationHeader);
            httpRequest.RequestContentType = "application/json";
            return JsonConvert.DeserializeObject<FinalRefreshResponse>(GetPossibleError(httpRequest.Get().Content));
        }
        public string StartSiteRefresh(string cobrandSessionId, string userSessionId, string providerAccountId)
        {
            var client = new RestClient(Configuration.RestUrls.BaseRestUrl);
            var request = new RestRequest(Configuration.RestUrls.StartRefreshDetails, Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            request.RequestFormat = DataFormat.Json;
            request.AddParameter("cobSessionToken", cobrandSessionId);
            request.AddParameter("userSessionToken", userSessionId);
            request.AddParameter("memSiteAccId", providerAccountId);
            request.AddParameter("refreshParameters.refreshPriority", 1);

            return GetPossibleError(client.Execute(request).Content);
        }
        public IList<IItemSummaries> GetItemSummaries(string cobrandSessionId, string userSessionId, string providerAccountId)
        {


            Logger.Info($"[Yodlee] GetItemSummaries method invoked for cobrandSessionId : { cobrandSessionId },userSessionId :{userSessionId},providerAccountId : {providerAccountId} ");
            var client = new RestClient(Configuration.RestUrls.BaseItemSummariesUrl);
            var request = new RestRequest(Configuration.RestUrls.ItemSummaries, Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            request.RequestFormat = DataFormat.Json;
            request.AddParameter("cobSessionToken", cobrandSessionId);
            request.AddParameter("userSessionToken", userSessionId);
            request.AddParameter("memSiteAccId", providerAccountId);
             var response =   ExecuteRequest<List<ItemSummaries>>(client, request);
            return new List<IItemSummaries>(response);
          //  return new List<IItemSummaries>(JsonConvert.DeserializeObject<List<ItemSummaries>>(client.Execute(request).Content));
        }


        public IFinalReport FetchReport(string cobrandSessionId, string userSessionId, string itemId, string itemAccountId)
        {
            Logger.Info($"[Yodlee] FetchReport method invoked for cobrandSessionId : { cobrandSessionId },userSessionId :{userSessionId},itemAccountId : {itemAccountId},itemId :{itemId} ");
            var client = new RestClient(Configuration.RestUrls.BaseFetchReportUrl);
            var request = new RestRequest(Configuration.RestUrls.FetchReport, Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            request.RequestFormat = DataFormat.Json;
            request.AddParameter("cobSessionToken", cobrandSessionId);
            request.AddParameter("userSessionToken", userSessionId);
            request.AddParameter("itemId", itemId);
            request.AddParameter("itemAccountId", itemAccountId);
            return ExecuteRequest<FinalReport>(client, request);
         //   return JsonConvert.DeserializeObject<FinalReport>(client.Execute(request).Content);
        }
        public IFastlinkAccessTokenResponse GetFastlinkAccessToken(Proxy.FastlinkAccessTokenRequest fastlinkAccessTokenRequest)
        {
            if (fastlinkAccessTokenRequest == null)
                throw new ArgumentNullException(nameof(fastlinkAccessTokenRequest));
            if (string.IsNullOrWhiteSpace(fastlinkAccessTokenRequest.CobrandToken))
                throw new ArgumentNullException(nameof(fastlinkAccessTokenRequest.CobrandToken));
            if (string.IsNullOrWhiteSpace(fastlinkAccessTokenRequest.UserSessionToken))
                throw new ArgumentNullException(nameof(fastlinkAccessTokenRequest.UserSessionToken));
            Logger.Info($"[Yodlee] GetFastlinkAccessToken method invoked for data : { (fastlinkAccessTokenRequest != null ? JsonConvert.SerializeObject(fastlinkAccessTokenRequest) : null)} ");
            var client = new RestClient(Configuration.RestUrls.BaseRestUrl.TrimEnd('/'));
            IRestRequest request = new RestRequest(Configuration.RestUrls.AccessToken.TrimEnd('/'), Method.GET);
            AddHeaderWithUserAndCobrandToken(ref request, fastlinkAccessTokenRequest.CobrandToken, fastlinkAccessTokenRequest.UserSessionToken);
            request.AddParameter("appIds", Configuration.FastLink.FastLinkAppId);
            return ExecuteRequest<FastlinkAccessTokenResponse>(client, request);

           // return JsonConvert.DeserializeObject<FastlinkAccessTokenResponse>(client.Execute(request).Content);
           
        }
        public IGetBankAccountResponse GetUserBankAccounts(string cobrandSessionId, string userSessionId)
        {
           
            if (string.IsNullOrWhiteSpace(cobrandSessionId))
                throw new ArgumentNullException(nameof(cobrandSessionId));
            if (string.IsNullOrWhiteSpace(userSessionId))
                throw new ArgumentNullException(nameof(userSessionId));
            Logger.Info($"[Yodlee] GetUserBankAccounts method invoked for cobrandSessionId : { cobrandSessionId },userSessionId :{userSessionId} ");
            var client = new RestClient(Configuration.RestUrls.BaseRestUrl);
            IRestRequest request = new RestRequest(Configuration.RestUrls.GetUserAccounts.TrimEnd('/'), Method.GET);
            AddHeaderWithUserAndCobrandToken(ref request, cobrandSessionId, userSessionId);
            return ExecuteRequest<GetBankAccountResponse>(client, request);
          //  return JsonConvert.DeserializeObject<Proxy.GetBankAccountResponse>(client.Execute(request).Content);
            
        }
        public string RemoveSiteAccount(string cobrandSessionId,string userSessionId, string providerAccountId)
        {
            if (string.IsNullOrWhiteSpace(cobrandSessionId))
                throw new ArgumentNullException(nameof(cobrandSessionId));
            if (string.IsNullOrWhiteSpace(userSessionId))
                throw new ArgumentNullException(nameof(userSessionId));
            Logger.Info($"[Yodlee] GetUserBankAccounts method invoked for cobrandSessionId : { cobrandSessionId },userSessionId :{userSessionId},providerAccountId :{providerAccountId} ");
            var client = new RestClient(Configuration.RestUrls.BaseFetchReportUrl);
            var request = new RestRequest(Configuration.RestUrls.RemoveSiteAccount, Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Accept", "application/json");
            request.RequestFormat = DataFormat.Json;
            request.AddParameter("cobSessionToken", cobrandSessionId);
            request.AddParameter("userSessionToken", userSessionId);
            request.AddParameter("memSiteAccId", providerAccountId);
           // return ExecuteRequest<string>(client, request);
           return client.Execute(request).Content;
        }
     
        public string GetFalstLinkUrl(string fastlinkToken, string userSessionId, IFastLinkUrlRequest fastlinkReuest)
        {
            if (fastlinkReuest == null)
                throw new ArgumentNullException(nameof(fastlinkReuest));
            StringBuilder additionalParam = new StringBuilder();
            FastlinkGenerationModel generateFLModel = new FastlinkGenerationModel();
            if (!string.IsNullOrWhiteSpace(fastlinkReuest.BankSearchKeyWord))
                AddAdditionalParam(ref additionalParam, "keyword", fastlinkReuest.BankSearchKeyWord);
            if (fastlinkReuest.FastLinkFlow != FastLinkFlows.normal)
            {
                if (fastlinkReuest.FastLinkFlow == FastLinkFlows.add)
                {
                    if (fastlinkReuest.SiteId == 0)
                        throw new ArgumentNullException($"{nameof(fastlinkReuest.SiteId)} is required for add flow");
                    AddAdditionalParam(ref additionalParam, "siteId", fastlinkReuest.SiteId.ToString());
                    AddAdditionalParam(ref additionalParam, "flow", fastlinkReuest.FastLinkFlow.ToString());
                }
                if (fastlinkReuest.FastLinkFlow == FastLinkFlows.edit || fastlinkReuest.FastLinkFlow == FastLinkFlows.refresh)
                {
                    if (fastlinkReuest.SiteAccountId == 0)
                        throw new ArgumentNullException($"{nameof(fastlinkReuest.SiteAccountId)} is required for edit or refresh flow");
                    AddAdditionalParam(ref additionalParam, "siteAccountId", fastlinkReuest.SiteAccountId.ToString());
                    AddAdditionalParam(ref additionalParam, "flow", fastlinkReuest.FastLinkFlow.ToString());
                }
            }
            if (!string.IsNullOrWhiteSpace(fastlinkReuest.CallBackUrl))
            {
                AddAdditionalParam(ref additionalParam, "callback", fastlinkReuest.CallBackUrl);
                generateFLModel.IsRedirect = true;
            }
            generateFLModel.FastLinkBaseUrl = Configuration.RestUrls.FastLinkUrl;
            generateFLModel.AppId = Configuration.FastLink.FastLinkAppId;
            generateFLModel.UserSessionToken = userSessionId;
            generateFLModel.FastlinkAccessToken = fastlinkToken;
            generateFLModel.ExtraParams = additionalParam.ToString();
            return ReplaceFastLinkHtmlValue(generateFLModel);
        }
        private string ReplaceFastLinkHtmlValue(FastlinkGenerationModel generateFLModel)
        {
            if (generateFLModel == null)
                throw new ArgumentNullException(nameof(generateFLModel));
            string fastLinkHtml = Configuration.FastLink.FastLinkLaunchForm;
            var regex = new Regex(@"(#{2})\S+(#{2})");
            var match = regex.Match(fastLinkHtml);
            while (match.Success)
            {
                var value = match.Value;
                var memberName = value.Trim('#');
                var propertyInfo = generateFLModel.GetType().GetProperty(memberName);
                var memberValue = propertyInfo.GetValue(generateFLModel);
                fastLinkHtml = fastLinkHtml.Replace(value, memberValue != null ? memberValue.ToString() : string.Empty);
                match = match.NextMatch();
            }
            return fastLinkHtml;
        }
      
        private void AddHeaderWithUserAndCobrandToken(ref IRestRequest request, string cobrandToken, string userSessionToken)
        {
            request.AddHeader("Authorization", "{userSession=" + userSessionToken + ",cobSession=" + cobrandToken + "}");
        }
        private void AddAdditionalParam(ref StringBuilder additionalParam, string parameterName, string parameterValue)
        {
            if (string.IsNullOrWhiteSpace(additionalParam.ToString()))
                additionalParam.Append($"{parameterName}={parameterValue}");
            else
                additionalParam.Append($"&{parameterName}={parameterValue}");
        }
        private  T ExecuteRequest<T>(IRestClient client, IRestRequest request) where T : class
        {
            var response = client.Execute(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
            {
                Logger.Error($"[Yodlee] Error occured when {request.Resource} was called. Error:", response.ErrorException);
                throw new YodleeException(response.ErrorException.Message);
            }

            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK)
            {
                Logger.Error($"[Yodlee] Error occured when {request.Resource} was called. Error:", response.Content);
                throw new YodleeException(response.Content);
            }

          
            return JsonConvert.DeserializeObject<T>(response.Content);
        }
    }
}
