﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl.Proxy
{
    public class AccessToken: IAccessToken
    {
        [JsonProperty("appId")]
        public string AppId { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
