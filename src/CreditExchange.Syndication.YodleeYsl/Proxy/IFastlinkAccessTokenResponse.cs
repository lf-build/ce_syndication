﻿
namespace CreditExchange.Syndication.YodleeYsl.Proxy
{
    public interface IFastlinkAccessTokenResponse
    {
        IAccessTokenData AccessTokenData { get; set; }
    }
}
