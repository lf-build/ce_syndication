﻿namespace CreditExchange.Syndication.YodleeYsl.Proxy
{
    public interface IBalance
    {
        decimal Amount { get; set; }
        string Currency { get; set; }
    }
}
