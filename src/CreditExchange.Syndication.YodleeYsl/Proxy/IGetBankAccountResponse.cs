﻿
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl.Proxy
{
    public interface IGetBankAccountResponse
    {
        IList<IBankAccount> Accounts { get; set; }
    }
}
