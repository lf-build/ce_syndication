﻿using System;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMfaRefreshInformation
    {
        string AdditionalStatus { get; set; }
        DateTime LastRefreshAttempt { get; set; }
        DateTime LastRefreshed { get; set; }
        string Status { get; set; }
        int StatusCode { get; set; }
        string StatusMessage { get; set; }
    }
}