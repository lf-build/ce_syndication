﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IIncome
    {
        string AnnualIncome { get; set; }
        string AvgIncome { get; set; }
    }
}