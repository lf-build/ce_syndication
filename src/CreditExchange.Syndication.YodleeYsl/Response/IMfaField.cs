﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMfaField
    {
        string Id { get; set; }
        bool IsOptional { get; set; }
        string Name { get; set; }
        string Type { get; set; }
        string Value { get; set; }
        string ValueEditable { get; set; }
        List<int> Image { get; set; }
    }
}