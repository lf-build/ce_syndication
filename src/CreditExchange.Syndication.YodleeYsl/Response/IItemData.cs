﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IItemData
    {
        List<Account> Accounts { get; set; }
    }
}