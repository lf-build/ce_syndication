﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface INameResponse
    {
        string First { get; set; }
        string Last { get; set; }
    }
}