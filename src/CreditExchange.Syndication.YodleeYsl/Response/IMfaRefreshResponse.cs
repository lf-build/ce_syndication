﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMfaRefreshResponse
    {
        IMfaProviderAccount ProviderAccount { get; set; }
    }
}