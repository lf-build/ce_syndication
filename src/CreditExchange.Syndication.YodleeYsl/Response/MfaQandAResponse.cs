﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class MfaQandAResponse : IMfaQandAResponse
    {
        [JsonProperty("loginForm")]
        [JsonConverter(typeof(YodleeJsonConverter<MfaLoginFormQandA>))]
        public IMfaLoginFormQandA LoginForm { get; set; }
    }
}
