﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IDuration
    {
        string FromDate { get; set; }
        string ToDate { get; set; }
    }
}