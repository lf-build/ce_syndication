﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class MonthlyTotal : IMonthlyTotal
    {
        [JsonProperty("creditTotalList")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<CreditTotalList>, ICreditTotalList>))]
        public IList<ICreditTotalList> CreditTotalList { get; set; }

        [JsonProperty("incomeTotalList")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<IncomeTotalList>, IIncomeTotalList>))]
        public IList<IIncomeTotalList> IncomeTotalList { get; set; }
    }
}
