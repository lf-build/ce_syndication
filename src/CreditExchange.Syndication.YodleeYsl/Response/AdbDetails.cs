﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class AdbDetails : IAdbDetails
    {
        [JsonProperty("avgDailyBalance")]
        public string AvgDailyBalance { get; set; }
    }
}
