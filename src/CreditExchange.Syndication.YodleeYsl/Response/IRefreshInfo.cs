﻿using System;

namespace CreditExchange.Syndication.YodleeYsl.Response
{
    public interface IRefreshInfo
    {
        int StatusCode { get; set; }
        string StatusMessage { get; set; }

        DateTime? LastRefreshed { get; set; }
        DateTime? LastRefreshAttempt { get; set; }
        DateTime? NextRefreshScheduled { get; set; }
    }
}
