﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class SubTransactionDataObject : ISubTransactionDataObject
    {
        [JsonProperty("dateToString")]
        public string DateToString { get; set; }

        [JsonProperty("originalDescription")]
        public string OriginalDescription { get; set; }

        [JsonProperty("categoryId")]
        public int CategoryId { get; set; }

        [JsonProperty("categoryTypeId")]
        public int CategoryTypeId { get; set; }

        [JsonProperty("transactionBaseTypeId")]
        public int TransactionBaseTypeId { get; set; }

        [JsonProperty("category")]
        public string Category { get; set; }

        [JsonProperty("parentCategory")]
        public string ParentCategory { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("amount")]
        public double Amount { get; set; }

        [JsonProperty("txnType")]
        public string TxnType { get; set; }

        [JsonProperty("txnCategoryTypeId")]
        public int TxnCategoryTypeId { get; set; }
    }
}
