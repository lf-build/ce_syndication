﻿
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl.Response
{
    public class FastlinkAccessTokenResponse : IFastlinkAccessTokenResponse
    {
        public FastlinkAccessTokenResponse() { }
        public FastlinkAccessTokenResponse(Proxy.IFastlinkAccessTokenResponse fastlinkAccessTokenResponse)
        {
            if (fastlinkAccessTokenResponse != null)
                AccessTokenData = new AccessTokenData(fastlinkAccessTokenResponse.AccessTokenData);
        }
        [JsonConverter(typeof(InterfaceConverter<IAccessTokenData,AccessTokenData>))]
        public IAccessTokenData AccessTokenData { get; set; }
    }
}
