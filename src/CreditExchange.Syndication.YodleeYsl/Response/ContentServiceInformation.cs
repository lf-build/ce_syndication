﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class ContentServiceInformation : IContentServiceInformation
    {
        [JsonProperty("contentServiceId")]
        public int ContentServiceId { get; set; }
    }
}
