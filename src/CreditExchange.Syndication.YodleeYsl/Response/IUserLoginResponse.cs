﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IUserLoginResponse
    {
        IUserResponse User { get; set; }
    }
}