﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{

    public class FinalReport : IFinalReport
    {

        [JsonProperty("metaData")]
        [JsonConverter(typeof(YodleeJsonConverter<MetaData>))]
        public IMetaData MetaData { get; set; }

        [JsonProperty("ambDetails")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<AmbDetail>, IAmbDetail>))]
        public IList<IAmbDetail> AmbDetails { get; set; }

        [JsonProperty("adbDetails")]
        [JsonConverter(typeof(YodleeJsonConverter<AdbDetails>))]
        public IAdbDetails AdbDetails { get; set; }

        [JsonProperty("categoryDetails")]
        [JsonConverter(typeof(YodleeJsonConverter<CategoryDetails>))]
        public ICategoryDetails CategoryDetails { get; set; }

        [JsonProperty("transactionDataObjects")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<TransactionDataObject>, ITransactionDataObject>))]
        public IList<ITransactionDataObject> TransactionDataObjects { get; set; }

        [JsonProperty("monthlyTotal")]
        [JsonConverter(typeof(YodleeJsonConverter<MonthlyTotal>))]
        public IMonthlyTotal MonthlyTotal { get; set; }

        public string ReferenceNumber { get; set; }

    
    }

}
