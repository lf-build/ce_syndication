﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class MfaRowQandA : IMfaRowQandA
    {
        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("field")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<MfaFieldQandA>, IMfaFieldQandA>))]
        public IList<IMfaFieldQandA> Field { get; set; }
    }
}
