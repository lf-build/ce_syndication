﻿namespace CreditExchange.Syndication.YodleeYsl.Response
{
    public interface IBalance
    {
        decimal Amount { get; set; }
        string Currency { get; set; }
    }
}
