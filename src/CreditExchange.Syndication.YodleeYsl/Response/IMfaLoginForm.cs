﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMfaLoginForm
    {
        string FormType { get; set; }
        int MfaTimeout { get; set; }
        IList<IMfaRow> Row { get; set; }
    }
}