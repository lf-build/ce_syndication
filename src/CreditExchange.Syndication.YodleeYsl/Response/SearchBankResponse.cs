﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class SearchBankResponse : ISearchBankResponse
    {
        [JsonProperty("provider")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<Provider>, IProvider>))]
        public IList<IProvider> Provider { get; set; }
    }
}
