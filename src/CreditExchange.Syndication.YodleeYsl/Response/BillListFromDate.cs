﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class BillListFromDate : IBillListFromDate
    {
        [JsonProperty("date")]
        public string Date { get; set; }
    }
}
