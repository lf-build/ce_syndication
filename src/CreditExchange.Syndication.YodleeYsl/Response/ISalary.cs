﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface ISalary
    {
        string AvgSalary { get; set; }
        string Count { get; set; }
        string TotalSalary { get; set; }
    }
}