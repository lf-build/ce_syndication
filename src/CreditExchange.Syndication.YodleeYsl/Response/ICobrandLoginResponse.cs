﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface ICobrandLoginResponse
    {
        string ApplicationId { get; set; }
        long CobrandId { get; set; }
        string Locale { get; set; }
        ISession Session { get; set; }
    }
}