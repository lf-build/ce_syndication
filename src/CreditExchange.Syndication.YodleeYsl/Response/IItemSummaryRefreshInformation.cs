﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IItemSummaryRefreshInformation
    {
        ItemAccessStatus ItemAccessStatus { get; set; }
        string ItemCreateDate { get; set; }
        int ItemId { get; set; }
        int LastUpdateAttemptTime { get; set; }
        int LastUpdatedTime { get; set; }
        int NextUpdateTime { get; set; }
        string RefreshMode { get; set; }
        int RefreshRequestTime { get; set; }
        int RefreshType { get; set; }
        ResponseCodeType ResponseCodeType { get; set; }
        int RetryCount { get; set; }
        int StatusCode { get; set; }
        string UserActionRequiredSince { get; set; }
        UserActionRequiredType UserActionRequiredType { get; set; }
    }
}