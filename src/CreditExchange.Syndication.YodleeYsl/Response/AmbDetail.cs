﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class AmbDetail : IAmbDetail
    {
        [JsonProperty("monthNumber")]
        public string MonthNumber { get; set; }

        [JsonProperty("monthName")]
        public string MonthName { get; set; }

        [JsonProperty("avgAmount")]
        public string AvgAmount { get; set; }

        [JsonProperty("minBalance")]
        public string MinBalance { get; set; }

        [JsonProperty("maxBalance")]
        public string MaxBalance { get; set; }

        [JsonProperty("eomBalance")]
        public string EomBalance { get; set; }
    }
}
