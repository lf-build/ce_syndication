﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class MfaRow : IMfaRow
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("fieldRowChoice")]
        public string FieldRowChoice { get; set; }

        [JsonProperty("form")]
        public string Form { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("field")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<MfaField>, IMfaField>))]
        public IList<IMfaField> Field { get; set; }
    }
}
