﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class MfaTokenResponse : IMfaTokenResponse
    {
        [JsonProperty("loginForm")]
        [JsonConverter(typeof(YodleeJsonConverter<MfaLoginFormToken>))]
        public IMfaLoginFormToken LoginForm { get; set; }
    }
}
