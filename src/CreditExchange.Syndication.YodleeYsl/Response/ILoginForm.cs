﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface ILoginForm
    {
        string ForgetPasswordURL { get; set; }
        string FormType { get; set; }
        int Id { get; set; }
        IList<IRow> Row { get; set; }
    }
}