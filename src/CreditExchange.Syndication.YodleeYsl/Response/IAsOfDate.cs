﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IAsOfDate
    {
        string Date { get; set; }
    }
}