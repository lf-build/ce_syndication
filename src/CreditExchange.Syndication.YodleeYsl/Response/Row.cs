﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class Row : IRow
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("form")]
        public string Form { get; set; }

        [JsonProperty("fieldRowChoice")]
        public string FieldRowChoice { get; set; }

        [JsonProperty("field")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<Field>, IField>))]
        public IList<IField> Field { get; set; }
    }
}
