﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface ITransfer
    {
        string TotalCredit { get; set; }
        string TotalDebit { get; set; }
    }
}