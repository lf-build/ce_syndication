﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class MfaLoginForm : IMfaLoginForm
    {
        [JsonProperty("mfaTimeout")]
        public int MfaTimeout { get; set; }

        [JsonProperty("formType")]
        public string FormType { get; set; }

        [JsonProperty("row")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<MfaRow>, IMfaRow>))]
        public IList<IMfaRow> Row { get; set; }
    }
}
