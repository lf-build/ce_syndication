﻿using System;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IProvider
    {
        string BaseUrl { get; set; }
        List<string> ContainerNames { get; set; }
        string CountryISOCode { get; set; }
        string Favicon { get; set; }
        string ForgetPasswordUrl { get; set; }
        string Help { get; set; }
        int Id { get; set; }
        DateTime LastModified { get; set; }
        string LoginUrl { get; set; }
        string Logo { get; set; }
        string Name { get; set; }
        bool OAuthSite { get; set; }
        string PrimaryLanguageISOCode { get; set; }
        string Priority { get; set; }
        string Status { get; set; }
    }
}