﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class ItemAccessStatus : IItemAccessStatus
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
