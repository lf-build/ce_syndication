﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMetaData
    {
        string Currency { get; set; }
        string DateFormatString { get; set; }
        IDuration Duration { get; set; }
        bool IsManual { get; set; }
        string AccountHolder { get; set; }
        string AccountNumber { get; set; }
    }
}