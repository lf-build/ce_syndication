﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface ICreditTotalList
    {
        string Key { get; set; }
        string Total { get; set; }
    }
}