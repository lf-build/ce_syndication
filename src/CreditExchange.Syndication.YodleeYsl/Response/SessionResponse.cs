﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class SessionResponse : ISessionResponse
    {
        [JsonProperty("userSession")]
        public string UserSession { get; set; }
    }
}
