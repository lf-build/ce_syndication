﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class ProviderAccount : IProviderAccount
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("providerId")]
        public int ProviderId { get; set; }

        [JsonProperty("isManual")]
        public bool IsManual { get; set; }

        [JsonProperty("aggregationSource")]
        public string AggregationSource { get; set; }

        [JsonProperty("refreshInfo")]
        [JsonConverter(typeof(YodleeJsonConverter<RefreshInformation>))]
        public IRefreshInformation RefreshInfo { get; set; }
    }
}
