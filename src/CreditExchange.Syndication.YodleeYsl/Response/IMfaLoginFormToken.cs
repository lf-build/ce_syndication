﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMfaLoginFormToken
    {
        IList<IMfaRowToken> Row { get; set; }
    }
}