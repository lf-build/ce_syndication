﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.YodleeYsl
{
   public interface IGetFastLinkUrlResponse
    {
        string HtmlForm { get; set; }
    }
}
