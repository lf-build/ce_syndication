﻿namespace CreditExchange.Syndication.YodleeYsl.Response
{
    public class ItemAccount
    {
        public string ItemId { get; set; }
        public string ItemAccountId { get; set; }

        public string BankAccountNo { get; set; }
        public string BankAccountName { get; set; }

    }
}
