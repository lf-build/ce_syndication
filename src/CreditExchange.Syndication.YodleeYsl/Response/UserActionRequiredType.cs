﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class UserActionRequiredType : IUserActionRequiredType
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
