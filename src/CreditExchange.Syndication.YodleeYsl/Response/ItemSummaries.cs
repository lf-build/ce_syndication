﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class ItemSummaries :IItemSummaries
    {
       public   int ContentServiceId { get; set; }
        public ContentServiceInformation ContentServiceInfo { get; set; }
        public bool IsCustom { get; set; }
        public bool IsDisabled { get; set; }
        public int IsHeld { get; set; }
        public bool IsPrepop { get; set; }
        public bool IsSharedItem { get; set; }
        public ItemData ItemData { get; set; }
        public string ItemDisplayName { get; set; }
        public int ItemId { get; set; }
        public int ItemStatus { get; set; }
        public int MemSiteAccId { get; set; }
        public ItemSummaryRefreshInformation RefreshInfo { get; set; }
    }
}
