﻿namespace CreditExchange.Syndication.YodleeYsl.Response
{
    public class AccessToken: IAccessToken
    {
        public AccessToken() { }
        public AccessToken(Proxy.IAccessToken accessToken)
        {
            AppId = accessToken?.AppId;
            Value = accessToken?.Value;
        }
        public string AppId { get; set; }
        public string Value { get; set; }
    }
}
