﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IProviderAccount
    {
        string AggregationSource { get; set; }
        int Id { get; set; }
        bool IsManual { get; set; }
        int ProviderId { get; set; }
        IRefreshInformation RefreshInfo { get; set; }
    }
}