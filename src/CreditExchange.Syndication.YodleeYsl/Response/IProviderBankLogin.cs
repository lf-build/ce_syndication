﻿using System;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IProviderBankLogin
    {
        string AuthType { get; set; }
        string BaseUrl { get; set; }
        List<string> ContainerNames { get; set; }
        string Favicon { get; set; }
        string ForgetPasswordUrl { get; set; }
        int Id { get; set; }
        string LanguageISOCode { get; set; }
        DateTime LastModified { get; set; }
        ILoginForm LoginForm { get; set; }
        string LoginUrl { get; set; }
        string Logo { get; set; }
        string Name { get; set; }
        bool OAuthSite { get; set; }
        string PrimaryLanguageISOCode { get; set; }
        string Status { get; set; }
    }
}