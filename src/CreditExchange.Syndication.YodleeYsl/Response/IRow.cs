﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IRow
    {
        IList<IField> Field { get; set; }
        string FieldRowChoice { get; set; }
        string Form { get; set; }
        int Id { get; set; }
        string Label { get; set; }
    }
}