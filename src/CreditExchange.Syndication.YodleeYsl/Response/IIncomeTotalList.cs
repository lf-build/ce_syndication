﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IIncomeTotalList
    {
        string Key { get; set; }
        string Total { get; set; }
    }
}