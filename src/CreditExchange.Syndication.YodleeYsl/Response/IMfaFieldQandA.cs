﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMfaFieldQandA
    {
        string Id { get; set; }
        string Value { get; set; }
    }
}