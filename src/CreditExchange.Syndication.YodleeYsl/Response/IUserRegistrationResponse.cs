﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IUserRegistrationResponse
    {
        IUserResponse User { get; set; }
    }
}