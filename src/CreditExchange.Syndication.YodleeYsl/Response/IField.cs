﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IField
    {
        int Id { get; set; }
        bool IsOptional { get; set; }
        string Name { get; set; }
        string Type { get; set; }
        string Value { get; set; }
        bool ValueEditable { get; set; }
    }
}