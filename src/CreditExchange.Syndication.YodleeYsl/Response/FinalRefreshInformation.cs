﻿using Newtonsoft.Json;
using System;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class FinalRefreshInformation : IFinalRefreshInformation
    {
        [JsonProperty("refreshStatus")]
        public string RefreshStatus { get; set; }

        [JsonProperty("statusCode")]
        public int StatusCode { get; set; }

        [JsonProperty("statusMessage")]
        public string StatusMessage { get; set; }

        [JsonProperty("nextRefreshScheduled")]
        public DateTime NextRefreshScheduled { get; set; }

        [JsonProperty("lastRefreshed")]
        public DateTime LastRefreshed { get; set; }

        [JsonProperty("lastRefreshAttempt")]
        public DateTime LastRefreshAttempt { get; set; }
    }
}
