﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IBillListToDate
    {
        string Date { get; set; }
    }
}