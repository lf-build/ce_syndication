﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMfaProviderAccount
    {
        string AggregationSource { get; set; }
        int Id { get; set; }
        bool IsManual { get; set; }
        IMfaLoginForm LoginForm { get; set; }
        int ProviderId { get; set; }
        IMfaRefreshInformation RefreshInfo { get; set; }
    }
}