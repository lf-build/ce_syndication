﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class ItemSummaryRefreshInformation : IItemSummaryRefreshInformation
    {
        [JsonProperty("itemId")]
        public int ItemId { get; set; }

        [JsonProperty("statusCode")]
        public int StatusCode { get; set; }

        [JsonProperty("refreshType")]
        public int RefreshType { get; set; }

        [JsonProperty("refreshRequestTime")]
        public int RefreshRequestTime { get; set; }

        [JsonProperty("lastUpdatedTime")]
        public int LastUpdatedTime { get; set; }

        [JsonProperty("lastUpdateAttemptTime")]
        public int LastUpdateAttemptTime { get; set; }

        [JsonProperty("itemAccessStatus")]
        public ItemAccessStatus ItemAccessStatus { get; set; }

        [JsonProperty("userActionRequiredType")]
        public UserActionRequiredType UserActionRequiredType { get; set; }

        [JsonProperty("userActionRequiredSince")]
        public string UserActionRequiredSince { get; set; }

        [JsonProperty("itemCreateDate")]
        public string ItemCreateDate { get; set; }

        [JsonProperty("nextUpdateTime")]
        public int NextUpdateTime { get; set; }

        [JsonProperty("responseCodeType")]
        public ResponseCodeType ResponseCodeType { get; set; }

        [JsonProperty("retryCount")]
        public int RetryCount { get; set; }

        [JsonProperty("refreshMode")]
        public string RefreshMode { get; set; }
    }
}
