﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class Account : IAccount
    {
        [JsonProperty("isSeidMod")]
        public int IsSeidMod { get; set; }

        [JsonProperty("acctTypeId")]
        public int AcctTypeId { get; set; }

        [JsonProperty("acctType")]
        public string AcctType { get; set; }

        [JsonProperty("localizedAcctType")]
        public string LocalizedAcctType { get; set; }

        [JsonProperty("billPreferenceId")]
        public int BillPreferenceId { get; set; }

        [JsonProperty("billingAccountId")]
        public int BillingAccountId { get; set; }

        [JsonProperty("customName")]
        public string CustomName { get; set; }

        [JsonProperty("isDeleted")]
        public int IsDeleted { get; set; }

        [JsonProperty("lastUpdated")]
        public int LastUpdated { get; set; }

        [JsonProperty("hasDetails")]
        public int HasDetails { get; set; }

        [JsonProperty("accountNumber")]
        public string AccountNumber { get; set; }

        [JsonProperty("dueDate")]
        [JsonConverter(typeof(YodleeJsonConverter<DueDate>))]
        public IDueDate DueDate { get; set; }

        [JsonProperty("lastPaymentDate")]
        [JsonConverter(typeof(YodleeJsonConverter<LastPaymentDate>))]
        public ILastPaymentDate LastPaymentDate { get; set; }

        [JsonProperty("derivedAutopayEnrollmentStatusId")]
        public int DerivedAutopayEnrollmentStatusId { get; set; }

        [JsonProperty("derivedAutopayEnrollmentStatus")]
        public string DerivedAutopayEnrollmentStatus { get; set; }

        [JsonProperty("localizedDerivedAutopayEnrollmentStatus")]
        public string LocalizedDerivedAutopayEnrollmentStatus { get; set; }

        [JsonProperty("accountName")]
        public string AccountName { get; set; }

        [JsonProperty("userAutopayEnrollmentStatusId")]
        public int UserAutopayEnrollmentStatusId { get; set; }

        [JsonProperty("userAutopayEnrollmentStatus")]
        public string UserAutopayEnrollmentStatus { get; set; }

        [JsonProperty("localizedUserAutopayEnrollmentStatus")]
        public string LocalizedUserAutopayEnrollmentStatus { get; set; }

        [JsonProperty("billListToDate")]
        [JsonConverter(typeof(YodleeJsonConverter<BillListToDate>))]
        public IBillListToDate BillListToDate { get; set; }

        [JsonProperty("billListFromDate")]
        [JsonConverter(typeof(YodleeJsonConverter<BillListFromDate>))]
        public IBillListFromDate BillListFromDate { get; set; }

        [JsonProperty("autopayEnrollmentStatusId")]
        public int AutopayEnrollmentStatusId { get; set; }

        [JsonProperty("autopayEnrollmentStatus")]
        public string AutopayEnrollmentStatus { get; set; }

        [JsonProperty("localizedAutopayEnrollmentStatus")]
        public string LocalizedAutopayEnrollmentStatus { get; set; }

        [JsonProperty("asofDate")]
        [JsonConverter(typeof(YodleeJsonConverter<AsOfDate>))]
        public IAsOfDate AsofDate { get; set; }

        [JsonProperty("created")]
        public int Created { get; set; }

        [JsonProperty("nickName")]
        public string NickName { get; set; }

        [JsonProperty("itemDataTableId")]
        public int ItemDataTableId { get; set; }

        [JsonProperty("itemAccountStatusId")]
        public int ItemAccountStatusId { get; set; }

        [JsonProperty("includeInNetworth")]
        public int IncludeInNetworth { get; set; }

        [JsonProperty("isAsset")]
        public int IsAsset { get; set; }

        [JsonProperty("isLinkedItemAccount")]
        public bool IsLinkedItemAccount { get; set; }

        [JsonProperty("itemAccountExtensions")]
        public IList<string> ItemAccountExtensions { get; set; }

        [JsonProperty("isUpdatePastTransaction")]
        public bool IsUpdatePastTransaction { get; set; }

        [JsonProperty("isUpdateTxCategory")]
        public bool IsUpdateTxCategory { get; set; }

        [JsonProperty("createOpeningTxn")]
        public bool CreateOpeningTxn { get; set; }

        [JsonProperty("cacheItemId")]
        public int CacheItemId { get; set; }

        [JsonProperty("isItemAccountDeleted")]
        public int IsItemAccountDeleted { get; set; }

        [JsonProperty("accountId")]
        public int AccountId { get; set; }

        [JsonProperty("level")]
        public int Level { get; set; }

        [JsonProperty("accountDisplayName")]
        [JsonConverter(typeof(YodleeJsonConverter<AccountDisplayName>))]
        public IAccountDisplayName AccountDisplayName { get; set; }

        [JsonProperty("itemAccountId")]
        public int ItemAccountId { get; set; }

        [JsonProperty("baseTagDataId")]
        public string BaseTagDataId { get; set; }

        [JsonProperty("refreshTime")]
        public int RefreshTime { get; set; }
    }
}
