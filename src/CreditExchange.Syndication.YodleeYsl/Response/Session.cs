﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class Session : ISession
    {
        [JsonProperty("cobSession")]
        public string CobSession { get; set; }
    }
}
