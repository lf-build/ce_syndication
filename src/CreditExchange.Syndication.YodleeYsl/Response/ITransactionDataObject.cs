﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface ITransactionDataObject
    {
        string Count { get; set; }
        string TotalAmt { get; set; }
        IList<ISubTransactionDataObject> TransactionDataObjects { get; set; }
        string TxnListType { get; set; }
    }
}