﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public class GetFastLinkUrlResponse :IGetFastLinkUrlResponse
    {
        public GetFastLinkUrlResponse()
        {

        }
        public GetFastLinkUrlResponse(string htmlForm)
        {
            HtmlForm = htmlForm;
        }
        public string HtmlForm { get; set; }
    }
}
