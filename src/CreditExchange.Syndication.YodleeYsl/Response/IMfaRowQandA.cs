﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMfaRowQandA
    {
        IList<IMfaFieldQandA> Field { get; set; }
        string Label { get; set; }
    }
}