﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class ProviderAccountRefreshStatus : IProviderAccountRefreshStatus
    {
        [JsonProperty("providerAccount")]
        [JsonConverter(typeof(YodleeJsonConverter<ProviderAccount>))]
        public IProviderAccount ProviderAccount { get; set; }
    }
}
