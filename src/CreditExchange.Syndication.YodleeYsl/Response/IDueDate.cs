﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IDueDate
    {
        string Date { get; set; }
    }
}