﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class AccountDisplayName : IAccountDisplayName
    {
        [JsonProperty("defaultNormalAccountName")]
        public string DefaultNormalAccountName { get; set; }
    }
}
