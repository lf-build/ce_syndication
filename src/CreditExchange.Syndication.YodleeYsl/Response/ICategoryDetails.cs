﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface ICategoryDetails
    {
        IAvgIncomeExpenseRatio AvgIncomeExpenseRatio { get; set; }
        object Expense { get; set; }
        IIncome Income { get; set; }
        ISalary Salary { get; set; }
        ITransfer Transfer { get; set; }
    }
}