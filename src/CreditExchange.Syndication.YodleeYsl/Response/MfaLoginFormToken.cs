﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class MfaLoginFormToken : IMfaLoginFormToken
    {
        [JsonProperty("row")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<MfaRowToken>, IMfaRowToken>))]
        public IList<IMfaRowToken> Row { get; set; }
    }
}
