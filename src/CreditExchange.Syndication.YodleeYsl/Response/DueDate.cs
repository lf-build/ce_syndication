﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class DueDate : IDueDate
    {

        [JsonProperty("date")]
        public string Date { get; set; }
    }
}
