﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class ProviderBankLogin : IProviderBankLogin
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("loginUrl")]
        public string LoginUrl { get; set; }

        [JsonProperty("baseUrl")]
        public string BaseUrl { get; set; }

        [JsonProperty("favicon")]
        public string Favicon { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("oAuthSite")]
        public bool OAuthSite { get; set; }

        [JsonProperty("lastModified")]
        public DateTime LastModified { get; set; }

        [JsonProperty("forgetPasswordUrl")]
        public string ForgetPasswordUrl { get; set; }

        [JsonProperty("languageISOCode")]
        public string LanguageISOCode { get; set; }

        [JsonProperty("primaryLanguageISOCode")]
        public string PrimaryLanguageISOCode { get; set; }

        [JsonProperty("containerNames")]
        public List<string> ContainerNames { get; set; }

        [JsonProperty("loginForm")]
        [JsonConverter(typeof(YodleeJsonConverter<LoginForm>))]
        public ILoginForm LoginForm { get; set; }

        [JsonProperty("authType")]
        public string AuthType { get; set; }
    }
}
