﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMfaFieldToken
    {
        string Id { get; set; }
        string Value { get; set; }
    }
}