﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class UserRegistrationResponse : IUserRegistrationResponse
    {
        [JsonProperty("user")]
        [JsonConverter(typeof(YodleeJsonConverter<UserResponse>))]
        public IUserResponse User { get; set; }
    }
}
