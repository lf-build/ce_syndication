﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class MfaProviderAccount : IMfaProviderAccount
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("providerId")]
        public int ProviderId { get; set; }

        [JsonProperty("isManual")]
        public bool IsManual { get; set; }

        [JsonProperty("aggregationSource")]
        public string AggregationSource { get; set; }

        [JsonProperty("refreshInfo")]
        [JsonConverter(typeof(YodleeJsonConverter<MfaRefreshInformation>))]
        public IMfaRefreshInformation RefreshInfo { get; set; }

        [JsonProperty("loginForm")]
        [JsonConverter(typeof(YodleeJsonConverter<MfaLoginForm>))]
        public IMfaLoginForm LoginForm { get; set; }
    }
}
