﻿

namespace CreditExchange.Syndication.YodleeYsl.Response
{
    public interface IFastlinkAccessTokenResponse
    {
        IAccessTokenData AccessTokenData { get; set; }
    }
}
