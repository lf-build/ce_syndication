﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface ISession
    {
        string CobSession { get; set; }
    }
}