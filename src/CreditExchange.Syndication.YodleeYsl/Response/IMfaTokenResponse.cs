﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMfaTokenResponse
    {
        IMfaLoginFormToken LoginForm { get; set; }
    }
}