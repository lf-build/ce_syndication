﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IAdbDetails
    {
        string AvgDailyBalance { get; set; }
    }
}