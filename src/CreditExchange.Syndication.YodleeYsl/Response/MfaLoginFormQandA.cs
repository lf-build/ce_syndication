﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class MfaLoginFormQandA : IMfaLoginFormQandA
    {
        [JsonProperty("mfaTimeout")]
        public int MfaTimeout { get; set; }

        [JsonProperty("formType")]
        public string FormType { get; set; }

        [JsonProperty("row")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<MfaRowQandA>, IMfaRowQandA>))]
        public IList<IMfaRowQandA> Row { get; set; }
    }
}
