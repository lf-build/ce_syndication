﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IContentServiceInformation
    {
        int ContentServiceId { get; set; }
    }
}