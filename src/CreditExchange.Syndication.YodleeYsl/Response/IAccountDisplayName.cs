﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IAccountDisplayName
    {
        string DefaultNormalAccountName { get; set; }
    }
}