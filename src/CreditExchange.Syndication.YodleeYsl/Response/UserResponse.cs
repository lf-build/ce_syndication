﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class UserResponse : IUserResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("loginName")]
        public string LoginName { get; set; }

        [JsonProperty("session")]
        [JsonConverter(typeof(YodleeJsonConverter<SessionResponse>))]
        public ISessionResponse Session { get; set; }

        [JsonProperty("name")]
        [JsonConverter(typeof(YodleeJsonConverter<NameResponse>))]
        public INameResponse Name { get; set; }

        [JsonProperty("preferences")]
        [JsonConverter(typeof(YodleeJsonConverter<PreferencesResponse>))]
        public IPreferencesResponse Preferences { get; set; }
    }
}
