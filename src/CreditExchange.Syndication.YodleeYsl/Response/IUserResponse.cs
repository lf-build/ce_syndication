﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IUserResponse
    {
        int Id { get; set; }
        string LoginName { get; set; }
        INameResponse Name { get; set; }
        IPreferencesResponse Preferences { get; set; }
        ISessionResponse Session { get; set; }
    }
}