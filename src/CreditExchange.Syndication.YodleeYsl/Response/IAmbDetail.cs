﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IAmbDetail
    {
        string AvgAmount { get; set; }
        string EomBalance { get; set; }
        string MaxBalance { get; set; }
        string MinBalance { get; set; }
        string MonthName { get; set; }
        string MonthNumber { get; set; }
    }
}