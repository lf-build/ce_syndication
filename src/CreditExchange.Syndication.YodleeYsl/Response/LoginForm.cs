﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class LoginForm : ILoginForm
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("forgetPasswordURL")]
        public string ForgetPasswordURL { get; set; }

        [JsonProperty("formType")]
        public string FormType { get; set; }

        [JsonProperty("row")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<Row>, IRow>))]
        public IList<IRow> Row { get; set; }
    }
}
