﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IPreferencesResponse
    {
        string Currency { get; set; }
        string DateFormat { get; set; }
        string Locale { get; set; }
        string TimeZone { get; set; }
    }
}