﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMfaImageResponse
    {
        IList<IMfaFieldImage> Field { get; set; }
    }
}