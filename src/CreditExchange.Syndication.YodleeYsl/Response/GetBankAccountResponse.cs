﻿
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
namespace CreditExchange.Syndication.YodleeYsl.Response
{
    public class GetBankAccountResponse : IGetBankAccountResponse
    {
        public GetBankAccountResponse() { }
        public GetBankAccountResponse(Proxy.IGetBankAccountResponse getBankAccountResponse)
        {
            if (getBankAccountResponse != null && getBankAccountResponse.Accounts.Count > 0)
            {
                CopyAccountsList(getBankAccountResponse.Accounts);
            }
        }
        [JsonConverter(typeof(InterfaceListConverter<IBankAccount,BankAccount>))]
        public IList<IBankAccount> Accounts { get; set; }
        private void CopyAccountsList(IList<Proxy.IBankAccount> accounts)
        {
            Accounts = new List<IBankAccount>();
            foreach (var account in accounts)
            {
                Accounts.Add(new BankAccount(account));
            }
        }
    }
}
