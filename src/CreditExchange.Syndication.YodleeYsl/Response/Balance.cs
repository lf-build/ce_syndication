﻿namespace CreditExchange.Syndication.YodleeYsl.Response
{
    public class Balance : IBalance
    {
        public Balance() { }
        public Balance(Proxy.IBalance balance)
        {
            if (balance != null)
            {
                Amount = balance.Amount;
                Currency = balance.Currency;
            }
        }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
    }
}
