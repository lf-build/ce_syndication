﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMfaRow
    {
        IList<IMfaField> Field { get; set; }
        string FieldRowChoice { get; set; }
        string Form { get; set; }
        string Id { get; set; }
        string Label { get; set; }
    }
}