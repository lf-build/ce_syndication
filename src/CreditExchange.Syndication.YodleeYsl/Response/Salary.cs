﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class Salary : ISalary
    {
        [JsonProperty("totalSalary")]
        public string TotalSalary { get; set; }

        [JsonProperty("avgSalary")]
        public string AvgSalary { get; set; }

        [JsonProperty("count")]
        public string Count { get; set; }
    }
}
