﻿namespace CreditExchange.Syndication.YodleeYsl.Response
{
    public interface IAccessToken
    {
         string AppId { get; set; }
         string Value { get; set; }
    }
}
