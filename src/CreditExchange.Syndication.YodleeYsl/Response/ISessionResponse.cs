﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface ISessionResponse
    {
        string UserSession { get; set; }
    }
}