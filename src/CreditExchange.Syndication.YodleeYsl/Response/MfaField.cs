﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class MfaField : IMfaField
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("isOptional")]
        public bool IsOptional { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("valueEditable")]
        public string ValueEditable { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
        
        [JsonProperty("image")]
        public List<int> Image { get; set; }
    }
}
