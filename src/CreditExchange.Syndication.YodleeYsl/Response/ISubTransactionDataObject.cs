﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface ISubTransactionDataObject
    {
        double Amount { get; set; }
        string Category { get; set; }
        int CategoryId { get; set; }
        int CategoryTypeId { get; set; }
        string Currency { get; set; }
        string DateToString { get; set; }
        string OriginalDescription { get; set; }
        string ParentCategory { get; set; }
        int TransactionBaseTypeId { get; set; }
        int TxnCategoryTypeId { get; set; }
        string TxnType { get; set; }
    }
}