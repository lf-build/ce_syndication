﻿using System;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IFinalRefreshInformation
    {
        DateTime LastRefreshAttempt { get; set; }
        DateTime LastRefreshed { get; set; }
        DateTime NextRefreshScheduled { get; set; }
        string RefreshStatus { get; set; }
        int StatusCode { get; set; }
        string StatusMessage { get; set; }
    }
}