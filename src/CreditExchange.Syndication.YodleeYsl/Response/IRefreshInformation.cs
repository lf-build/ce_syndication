﻿using System;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IRefreshInformation
    {
        DateTime LastRefreshAttempt { get; set; }
        DateTime LastRefreshed { get; set; }
        DateTime NextRefreshScheduled { get; set; }
        string Status { get; set; }
        int StatusCode { get; set; }
        string StatusMessage { get; set; }
    }
}