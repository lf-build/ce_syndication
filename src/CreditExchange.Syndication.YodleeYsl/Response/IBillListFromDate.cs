﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IBillListFromDate
    {
        string Date { get; set; }
    }
}