﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IResponseCodeType
    {
        int ResponseCodeTypeId { get; set; }
    }
}