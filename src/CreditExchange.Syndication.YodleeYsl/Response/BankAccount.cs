﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;

namespace CreditExchange.Syndication.YodleeYsl.Response
{
    public class BankAccount : IBankAccount
    {
        public BankAccount() { }
        public BankAccount(Proxy.IBankAccount account)
        {
            if (account != null)
            {
                Container = account.Container;
                ProviderAccountId = account.ProviderAccountId;
                AccountName = account.AccountName;
                Id = account.Id;
                AccountNumber = account.AccountNumber;
                AvailableBalance =new Balance(account.AvailableBalance);
                AccountType = account.AccountType;
                CreatedDate = account.CreatedDate;
                IsAsset = account.IsAsset;
                Balance = new Balance(account.Balance);
                ProviderId = account.ProviderId;
                ProviderName = account.ProviderName;
                RefreshInfo = new RefreshInfo(account.RefreshInfo);
                AccountStatus = account.AccountStatus;
            }
        }
        public string Container { get; set; }
        public long ProviderAccountId { get; set; }
        public string AccountName { get; set; }
        public long Id { get; set; }
        public string AccountNumber { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IBalance,Balance>))]
        public IBalance AvailableBalance { get; set; }
        public string AccountType { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? IsAsset { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IBalance, Balance>))]
        public IBalance Balance { get; set; }
        public long ProviderId { get; set; }
        public string ProviderName { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRefreshInfo,RefreshInfo>))]
        public IRefreshInfo RefreshInfo { get; set; }
        public string AccountStatus { get; set; }
    }
}
