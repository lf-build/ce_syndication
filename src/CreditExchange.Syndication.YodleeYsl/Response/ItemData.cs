﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class ItemData : IItemData
    {
        [JsonProperty("accounts")]
        public List<Account> Accounts { get; set; }
    }
}
