﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class AsOfDate : IAsOfDate
    {
        [JsonProperty("date")]
        public string Date { get; set; }
    }
}
