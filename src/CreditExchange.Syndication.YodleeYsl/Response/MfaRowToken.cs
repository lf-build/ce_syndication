﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class MfaRowToken : IMfaRowToken
    {
        [JsonProperty("field")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<MfaFieldToken>, IMfaFieldToken>))]
        public IList<IMfaFieldToken> Field { get; set; }
    }
}
