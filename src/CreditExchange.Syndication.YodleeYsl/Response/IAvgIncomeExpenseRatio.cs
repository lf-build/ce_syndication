﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IAvgIncomeExpenseRatio
    {
        string AvgIncomeToExpenseRatio { get; set; }
    }
}