﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface ISearchBankResponse
    {
        IList<IProvider> Provider { get; set; }
    }
}