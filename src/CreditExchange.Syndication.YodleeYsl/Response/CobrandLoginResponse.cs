﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class CobrandLoginResponse : ICobrandLoginResponse
    {
        [JsonProperty("cobrandId")]
        public long CobrandId { get; set; }

        [JsonProperty("applicationId")]
        public string ApplicationId { get; set; }

        [JsonProperty("session")]
        [JsonConverter(typeof(YodleeJsonConverter<Session>))]
        public ISession Session { get; set; }

        [JsonProperty("locale")]
        public string Locale { get; set; }
    }
}
