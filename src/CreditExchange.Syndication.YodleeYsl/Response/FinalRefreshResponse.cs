﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class FinalRefreshResponse : IFinalRefreshResponse
    {
        [JsonProperty("providerAccountId")]
        public int ProviderAccountId { get; set; }

        [JsonProperty("refreshInfo")]
        [JsonConverter(typeof(YodleeJsonConverter<FinalRefreshInformation>))]
        public IFinalRefreshInformation RefreshInfo { get; set; }
    }
}
