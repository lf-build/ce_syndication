﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class LastPaymentDate : ILastPaymentDate
    {

        [JsonProperty("date")]
        public string Date { get; set; }
    }
}
