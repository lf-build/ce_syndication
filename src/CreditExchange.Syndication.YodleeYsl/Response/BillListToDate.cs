﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class BillListToDate : IBillListToDate
    {
        [JsonProperty("date")]
        public string Date { get; set; }
    }
}
