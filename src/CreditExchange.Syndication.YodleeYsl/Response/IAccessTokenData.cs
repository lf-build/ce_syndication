﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl.Response
{
    public interface IAccessTokenData
    {
        IList<IAccessToken> AccessTokens { get; set; }
    }
}
