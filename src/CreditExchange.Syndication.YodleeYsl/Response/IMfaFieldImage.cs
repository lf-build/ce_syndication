﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMfaFieldImage
    {
        string Id { get; set; }
        string Value { get; set; }
    }
}