﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IAccount
    {
        IAccountDisplayName AccountDisplayName { get; set; }
        int AccountId { get; set; }
        string AccountName { get; set; }
        string AccountNumber { get; set; }
        string AcctType { get; set; }
        int AcctTypeId { get; set; }
        IAsOfDate AsofDate { get; set; }
        string AutopayEnrollmentStatus { get; set; }
        int AutopayEnrollmentStatusId { get; set; }
        string BaseTagDataId { get; set; }
        int BillingAccountId { get; set; }
        IBillListFromDate BillListFromDate { get; set; }
        IBillListToDate BillListToDate { get; set; }
        int BillPreferenceId { get; set; }
        int CacheItemId { get; set; }
        int Created { get; set; }
        bool CreateOpeningTxn { get; set; }
        string CustomName { get; set; }
        string DerivedAutopayEnrollmentStatus { get; set; }
        int DerivedAutopayEnrollmentStatusId { get; set; }
        IDueDate DueDate { get; set; }
        int HasDetails { get; set; }
        int IncludeInNetworth { get; set; }
        int IsAsset { get; set; }
        int IsDeleted { get; set; }
        int IsItemAccountDeleted { get; set; }
        bool IsLinkedItemAccount { get; set; }
        int IsSeidMod { get; set; }
        bool IsUpdatePastTransaction { get; set; }
        bool IsUpdateTxCategory { get; set; }
        IList<string> ItemAccountExtensions { get; set; }
        int ItemAccountId { get; set; }
        int ItemAccountStatusId { get; set; }
        int ItemDataTableId { get; set; }
        ILastPaymentDate LastPaymentDate { get; set; }
        int LastUpdated { get; set; }
        int Level { get; set; }
        string LocalizedAcctType { get; set; }
        string LocalizedAutopayEnrollmentStatus { get; set; }
        string LocalizedDerivedAutopayEnrollmentStatus { get; set; }
        string LocalizedUserAutopayEnrollmentStatus { get; set; }
        string NickName { get; set; }
        int RefreshTime { get; set; }
        string UserAutopayEnrollmentStatus { get; set; }
        int UserAutopayEnrollmentStatusId { get; set; }
    }
}