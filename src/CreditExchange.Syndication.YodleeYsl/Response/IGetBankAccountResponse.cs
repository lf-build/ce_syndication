﻿
using System.Collections.Generic;
namespace CreditExchange.Syndication.YodleeYsl.Response
{
    public interface IGetBankAccountResponse
    {
        IList<IBankAccount> Accounts { get; set; }
    }
}
