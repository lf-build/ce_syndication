﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMfaQandAResponse
    {
        IMfaLoginFormQandA LoginForm { get; set; }
    }
}