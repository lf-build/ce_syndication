﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class BankLoginResponse : IBankLoginResponse
    {
        [JsonProperty("provider")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<ProviderBankLogin>, IProviderBankLogin>))]
        public IList<IProviderBankLogin> Provider { get; set; }
    }
}
