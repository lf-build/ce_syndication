﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IUserActionRequiredType
    {
        string Name { get; set; }
    }
}