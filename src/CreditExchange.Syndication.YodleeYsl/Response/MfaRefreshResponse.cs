﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class MfaRefreshResponse : IMfaRefreshResponse
    {
        [JsonProperty("providerAccount")]
        [JsonConverter(typeof(YodleeJsonConverter<MfaProviderAccount>))]
        public IMfaProviderAccount ProviderAccount { get; set; }
    }
}
