﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class CategoryDetails : ICategoryDetails
    {

        [JsonProperty("income")]
        [JsonConverter(typeof(YodleeJsonConverter<Income>))]
        public IIncome Income { get; set; }

        [JsonProperty("expense")]
        public object Expense { get; set; }

        [JsonProperty("transfer")]
        [JsonConverter(typeof(YodleeJsonConverter<Transfer>))]
        public ITransfer Transfer { get; set; }

        [JsonProperty("avgIncomeExpenseRatio")]
        [JsonConverter(typeof(YodleeJsonConverter<AvgIncomeExpenseRatio>))]
        public IAvgIncomeExpenseRatio AvgIncomeExpenseRatio { get; set; }

        [JsonProperty("salary")]
        [JsonConverter(typeof(YodleeJsonConverter<Salary>))]
        public ISalary Salary { get; set; }
    }
}
