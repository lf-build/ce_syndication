﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IProviderAccountRefreshStatus
    {
        IProviderAccount ProviderAccount { get; set; }
    }
}