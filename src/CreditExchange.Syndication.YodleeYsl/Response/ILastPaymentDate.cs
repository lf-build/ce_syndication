﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface ILastPaymentDate
    {
        string Date { get; set; }
    }
}