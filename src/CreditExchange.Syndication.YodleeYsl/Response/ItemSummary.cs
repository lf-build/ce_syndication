﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class ItemSummary : IItemSummary
    {
        


        [JsonProperty("itemId")]
        public int ItemId { get; set; }

        [JsonProperty("contentServiceId")]
        public int ContentServiceId { get; set; }

        [JsonProperty("contentServiceInfo")]
        public ContentServiceInformation ContentServiceInfo { get; set; }

        [JsonProperty("itemDisplayName")]
        public string ItemDisplayName { get; set; }

        [JsonProperty("refreshInfo")]
        public ItemSummaryRefreshInformation RefreshInfo { get; set; }

        [JsonProperty("isCustom")]
        public bool IsCustom { get; set; }

        [JsonProperty("isDisabled")]
        public bool IsDisabled { get; set; }

        [JsonProperty("itemData")]
        public ItemData ItemData { get; set; }

        [JsonProperty("itemStatus")]
        public int ItemStatus { get; set; }

        [JsonProperty("isHeld")]
        public int IsHeld { get; set; }

        [JsonProperty("isSharedItem")]
        public bool IsSharedItem { get; set; }

        [JsonProperty("isPrepop")]
        public bool IsPrepop { get; set; }

        [JsonProperty("memSiteAccId")]
        public int MemSiteAccId { get; set; }
    }
}
