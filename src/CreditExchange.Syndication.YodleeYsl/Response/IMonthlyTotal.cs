﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMonthlyTotal
    {
        IList<ICreditTotalList> CreditTotalList { get; set; }
        IList<IIncomeTotalList> IncomeTotalList { get; set; }
    }
}