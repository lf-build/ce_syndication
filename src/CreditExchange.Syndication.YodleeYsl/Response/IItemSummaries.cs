﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.YodleeYsl
{
   public interface IItemSummaries
    {
         int ContentServiceId { get; set; }
         ContentServiceInformation ContentServiceInfo { get; set; }
         bool IsCustom { get; set; }
         bool IsDisabled { get; set; }
         int IsHeld { get; set; }
         bool IsPrepop { get; set; }
         bool IsSharedItem { get; set; }
         ItemData ItemData { get; set; }
         string ItemDisplayName { get; set; }
         int ItemId { get; set; }
         int ItemStatus { get; set; }
         int MemSiteAccId { get; set; }
         ItemSummaryRefreshInformation RefreshInfo { get; set; }
    }
}
