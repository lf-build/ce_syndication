﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class MfaImageResponse : IMfaImageResponse
    {
        [JsonProperty("field")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<MfaFieldImage>, IMfaFieldImage>))]
        public IList<IMfaFieldImage> Field { get; set; }
    }
}
