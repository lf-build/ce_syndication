﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMfaLoginFormQandA
    {
        string FormType { get; set; }
        int MfaTimeout { get; set; }
        IList<IMfaRowQandA> Row { get; set; }
    }
}