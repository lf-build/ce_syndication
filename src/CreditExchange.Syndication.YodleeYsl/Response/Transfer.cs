﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class Transfer : ITransfer
    {
        [JsonProperty("totalCredit")]
        public string TotalCredit { get; set; }

        [JsonProperty("totalDebit")]
        public string TotalDebit { get; set; }
    }
}
