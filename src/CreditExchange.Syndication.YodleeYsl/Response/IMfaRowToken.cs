﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IMfaRowToken
    {
        IList<IMfaFieldToken> Field { get; set; }
    }
}