﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class Provider : IProvider
    {
        [JsonProperty("PRIORITY")]
        public string Priority { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("loginUrl")]
        public string LoginUrl { get; set; }

        [JsonProperty("baseUrl")]
        public string BaseUrl { get; set; }

        [JsonProperty("favicon")]
        public string Favicon { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("help")]
        public string Help { get; set; }

        [JsonProperty("oAuthSite")]
        public bool OAuthSite { get; set; }

        [JsonProperty("primaryLanguageISOCode")]
        public string PrimaryLanguageISOCode { get; set; }

        [JsonProperty("countryISOCode")]
        public string CountryISOCode { get; set; }

        [JsonProperty("lastModified")]
        public DateTime LastModified { get; set; }

        [JsonProperty("forgetPasswordUrl")]
        public string ForgetPasswordUrl { get; set; }

        [JsonProperty("containerNames")]
        public List<string> ContainerNames { get; set; }
    }
}
