﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class MetaData : IMetaData
    {
        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("duration")]
        [JsonConverter(typeof(YodleeJsonConverter<Duration>))]
        public IDuration Duration { get; set; }

        [JsonProperty("dateFormatString")]
        public string DateFormatString { get; set; }
        [JsonProperty("isManual")]
        public bool IsManual { get; set; }
        [JsonProperty("accountHolder")]
        public string AccountHolder { get; set; }
        [JsonProperty("accountNumber")]
        public string AccountNumber { get; set; }
    }
}
