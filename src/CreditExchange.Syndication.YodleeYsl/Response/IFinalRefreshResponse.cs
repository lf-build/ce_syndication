﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IFinalRefreshResponse
    {
        int ProviderAccountId { get; set; }
        IFinalRefreshInformation RefreshInfo { get; set; }
    }
}