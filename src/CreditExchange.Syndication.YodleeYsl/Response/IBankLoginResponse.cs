﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IBankLoginResponse
    {
        IList<IProviderBankLogin> Provider { get; set; }
    }
}