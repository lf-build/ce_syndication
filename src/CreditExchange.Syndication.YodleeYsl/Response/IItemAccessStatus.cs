﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IItemAccessStatus
    {
        string Name { get; set; }
    }
}