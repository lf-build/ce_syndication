﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class Income : IIncome
    {
        [JsonProperty("avgIncome")]
        public string AvgIncome { get; set; }

        [JsonProperty("annualIncome")]
        public string AnnualIncome { get; set; }
    }
}
