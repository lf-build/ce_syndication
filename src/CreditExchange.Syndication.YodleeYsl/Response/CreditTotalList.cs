﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class CreditTotalList : ICreditTotalList
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("total")]
        public string Total { get; set; }
    }
}
