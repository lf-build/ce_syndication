﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class TransactionDataObject : ITransactionDataObject
    {
        [JsonProperty("txnListType")]
        public string TxnListType { get; set; }

        [JsonProperty("count")]
        public string Count { get; set; }

        [JsonProperty("totalAmt")]
        public string TotalAmt { get; set; }

        [JsonProperty("transactionDataObjects")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<SubTransactionDataObject>, ISubTransactionDataObject>))]
        public IList<ISubTransactionDataObject> TransactionDataObjects { get; set; }
    }
}
