﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class ResponseCodeType : IResponseCodeType
    {
        [JsonProperty("responseCodeTypeId")]
        public int ResponseCodeTypeId { get; set; }
    }
}
