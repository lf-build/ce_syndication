﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class AvgIncomeExpenseRatio : IAvgIncomeExpenseRatio
    {

        [JsonProperty("avgIncomeToExpenseRatio")]
        public string AvgIncomeToExpenseRatio { get; set; }
    }
}
