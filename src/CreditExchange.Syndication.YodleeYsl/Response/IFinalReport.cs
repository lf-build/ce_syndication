﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IFinalReport
    {
        IAdbDetails AdbDetails { get; set; }
        IList<IAmbDetail> AmbDetails { get; set; }
        ICategoryDetails CategoryDetails { get; set; }
        IMetaData MetaData { get; set; }
        IMonthlyTotal MonthlyTotal { get; set; }
        IList<ITransactionDataObject> TransactionDataObjects { get; set; }
        string ReferenceNumber { get; set; }
       
    }
}