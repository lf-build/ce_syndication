﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface ICobrandConfiguration
    {
        string LoginName { get; set; } 
         string Password { get; set; }
         string Locale { get; set; }
    }
}
