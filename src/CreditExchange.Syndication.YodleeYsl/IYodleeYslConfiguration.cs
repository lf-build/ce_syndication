﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IYodleeYslConfiguration
    {
        IYodleeYslRestUrls RestUrls { get; set; }
        IUserConfiguration User { get; set; }
        ICobrandConfiguration Cobrand { get; set; }
        IFastLinkConfiguration FastLink { get; set; }
        string UserCredentialsInstanceType { get; set; }
        string UserProfileInstanceType { get; set; }
        string PreferredCurrency { get; set; }
        string PreferredDateFormate { get; set; }
    }
}