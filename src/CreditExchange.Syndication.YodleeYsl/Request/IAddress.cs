﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IAddress
    {
        string Address1 { get; set; }
        string City { get; set; }
        string Country { get; set; }
        string State { get; set; }
        string Zip { get; set; }
    }
}