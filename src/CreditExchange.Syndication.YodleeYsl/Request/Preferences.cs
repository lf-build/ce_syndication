﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class Preferences : IPreferences
    {
        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("timeZone")]
        public string TimeZone { get; set; }

        [JsonProperty("dateFormat")]
        public string DateFormat { get; set; }

        [JsonProperty("locale")]
        public string Locale { get; set; }
    }
}
