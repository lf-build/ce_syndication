﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class UpdateBankLoginField : IUpdateBankLoginField
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
