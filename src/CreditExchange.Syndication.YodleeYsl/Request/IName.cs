﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IName
    {
        string First { get; set; }
        string Last { get; set; }
    }
}