﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class UpdateBankLoginRow : IUpdateBankLoginRow
    {
        [JsonProperty("field")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<UpdateBankLoginField>, IUpdateBankLoginField>))]
        public IList<IUpdateBankLoginField> Field { get; set; }
    }
}
