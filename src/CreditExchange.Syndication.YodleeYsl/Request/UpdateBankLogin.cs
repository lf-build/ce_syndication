﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class UpdateBankLogin : IUpdateBankLogin
    {
        [JsonProperty("loginForm")]
        [JsonConverter(typeof(YodleeJsonConverter<UpdateBankLoginForm>))]
        public IUpdateBankLoginForm LoginForm { get; set; }
    }
}
