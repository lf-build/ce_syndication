﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class Cobrand : ICobrand
    {
       public Cobrand()
       {

       }
        public Cobrand(string cobrandLoginUserName, string cobrandPassword, string locale)
        {
            CobrandLogin = cobrandLoginUserName;
            CobrandPassword = cobrandPassword;
            Locale = locale;
        }
        [JsonProperty("cobrandLogin")]
        public string CobrandLogin { get; set; }

        [JsonProperty("cobrandPassword")]
        public string CobrandPassword { get; set; }

        [JsonProperty("locale")]
        public string Locale { get; set; }
    }
}
