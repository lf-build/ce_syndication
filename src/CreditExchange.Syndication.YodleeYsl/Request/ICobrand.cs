﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface ICobrand
    {
        string CobrandLogin { get; set; }
        string CobrandPassword { get; set; }
        string Locale { get; set; }
    }
}