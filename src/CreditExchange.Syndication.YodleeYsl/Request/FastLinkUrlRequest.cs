﻿namespace CreditExchange.Syndication.YodleeYsl.Request
{
    public class FastLinkUrlRequest :  IFastLinkUrlRequest
    {
        public FastLinkUrlRequest()
        {
        }

        public FastLinkUrlRequest(IFastLinkUrlRequest fastLinkUrlRequest)
        {
            if (fastLinkUrlRequest != null)
            {
                
                FastLinkFlow = fastLinkUrlRequest.FastLinkFlow;
                BankSearchKeyWord = fastLinkUrlRequest.BankSearchKeyWord;
                SiteId = fastLinkUrlRequest.SiteId;
                SiteAccountId = fastLinkUrlRequest.SiteAccountId;
                CallBackUrl = fastLinkUrlRequest.CallBackUrl;
            }
        }
       
        public FastLinkFlows FastLinkFlow { get; set; }
        public string BankSearchKeyWord { get; set; }
        public long SiteId { get; set; }
        public long SiteAccountId { get; set; }
        public string CallBackUrl { get; set; }
    }  
}
