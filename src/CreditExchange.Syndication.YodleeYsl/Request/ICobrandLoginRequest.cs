﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface ICobrandLoginRequest
    {
        ICobrand Cobrand { get; set; }
    }
}