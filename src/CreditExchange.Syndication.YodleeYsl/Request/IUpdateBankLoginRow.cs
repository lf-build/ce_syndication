﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IUpdateBankLoginRow
    {
        IList<IUpdateBankLoginField> Field { get; set; }
    }
}