﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class UserRegistrationRequest : IUserRegistrationRequest
    {
        public UserRegistrationRequest()
        {

        }
        public UserRegistrationRequest(string loginName, string password, string email)
        {
            User = new User(loginName, password, email);
        }

        [JsonProperty("user")]
        [JsonConverter(typeof(YodleeJsonConverter<User>))]
        public IUser User { get; set; }
    }
}
