﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class UserRequest : IUserRequest
    {
        public UserRequest()
        {

        }
        public UserRequest(string loginName,string password,string locale)
        {
            LoginName = loginName;
            Password = password;
            Locale = locale;
        }
        [JsonProperty("loginName")]
        public string LoginName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("locale")]
        public string Locale { get; set; }
    }
}
