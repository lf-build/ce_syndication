﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IUserRequest
    {
        string Locale { get; set; }
        string LoginName { get; set; }
        string Password { get; set; }
    }
}