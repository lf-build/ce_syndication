﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IUser
    {
        IAddress Address { get; set; }
        string Email { get; set; }
        string LoginName { get; set; }
        IName Name { get; set; }
        string Password { get; set; }
        IPreferences Preferences { get; set; }
    }
}