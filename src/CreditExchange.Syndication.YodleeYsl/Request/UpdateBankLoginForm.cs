﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class UpdateBankLoginForm : IUpdateBankLoginForm
    {
        [JsonProperty("row")]
        [JsonConverter(typeof(YodleeListJsonConverter<List<UpdateBankLoginRow>, IUpdateBankLoginRow>))]
        public IList<IUpdateBankLoginRow> Row { get; set; }
    }
}
