﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class UserLoginRequest : IUserLoginRequest
    {
        public UserLoginRequest()
        {

        }
        public UserLoginRequest(string loginName, string password, string locale)
        {
            User = new UserRequest(loginName, password, locale);
        }
        [JsonProperty("user")]
        [JsonConverter(typeof(YodleeJsonConverter<UserRequest>))]
        public IUserRequest User { get; set; }
    }
}
