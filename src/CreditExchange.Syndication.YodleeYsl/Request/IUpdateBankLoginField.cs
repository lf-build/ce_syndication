﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IUpdateBankLoginField
    {
        int Id { get; set; }
        string Value { get; set; }
    }
}