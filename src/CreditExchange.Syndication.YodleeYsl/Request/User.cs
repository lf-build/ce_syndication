﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class User : IUser
    {
        public User()
        {

        }
        public User(string loginName,string password,string email)
        {
            LoginName = loginName;
            Password = password;
            Email = email;
        }
        [JsonProperty("loginName")]
        public string LoginName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("name")]
        [JsonConverter(typeof(YodleeJsonConverter<Name>))]
        public IName Name { get; set; }

        [JsonProperty("address")]
        [JsonConverter(typeof(YodleeJsonConverter<Address>))]
        public IAddress Address { get; set; }

        [JsonProperty("preferences")]
        [JsonConverter(typeof(YodleeJsonConverter<Preferences>))]
        public IPreferences Preferences { get; set; }
    }
}
