﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class CobrandLoginRequest : ICobrandLoginRequest
    {
        public CobrandLoginRequest()
        {
            
        }
        public CobrandLoginRequest(string cobrandLoginUserName, string cobrandPassword,string locale)
        {
            Cobrand = new Cobrand(cobrandLoginUserName, cobrandPassword, locale);
        }
        [JsonProperty("cobrand")]
        [JsonConverter(typeof(YodleeJsonConverter<Cobrand>))]
        public ICobrand Cobrand { get; set; }
    }
}
