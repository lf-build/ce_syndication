﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IUpdateBankLogin
    {
        IUpdateBankLoginForm LoginForm { get; set; }
    }
}