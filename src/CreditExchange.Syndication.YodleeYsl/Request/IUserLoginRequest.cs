﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IUserLoginRequest
    {
        IUserRequest User { get; set; }
    }
}