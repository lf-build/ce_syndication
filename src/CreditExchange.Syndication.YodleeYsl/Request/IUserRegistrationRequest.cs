﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IUserRegistrationRequest
    {
        IUser User { get; set; }
    }
}