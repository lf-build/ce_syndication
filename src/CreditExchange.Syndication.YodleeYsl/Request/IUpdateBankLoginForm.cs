﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeYsl
{
    public interface IUpdateBankLoginForm
    {
        IList<IUpdateBankLoginRow> Row { get; set; }
    }
}