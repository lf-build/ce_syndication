﻿namespace CreditExchange.Syndication.YodleeYsl.Request
{
    public interface IFastLinkUrlRequest
    {
        FastLinkFlows FastLinkFlow { get; set; }
        string BankSearchKeyWord { get; set; }
        long SiteId { get; set; }
        long SiteAccountId { get; set; }
        string CallBackUrl { get; set; }
    }
}
