﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeYsl
{
    public class Name : IName
    {
        [JsonProperty("first")]
        public string First { get; set; }

        [JsonProperty("last")]
        public string Last { get; set; }
    }
}
