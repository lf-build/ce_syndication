﻿namespace CreditExchange.Syndication.YodleeYsl.Request
{
    public enum FastLinkFlows
    {
        normal,
        add,
        edit,
        refresh
    }
}
