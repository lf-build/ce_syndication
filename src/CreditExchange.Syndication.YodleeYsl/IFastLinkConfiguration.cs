﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.YodleeYsl
{
  public  interface IFastLinkConfiguration
    {
         string FastLinkAppId { get; set; }
         string FastLinkLaunchForm { get; set; } 
         string FastLinkFlow { get; set; } 
         string BankSearchKeyWord { get; set; } 
         string SiteId { get; set; }
         string CallBackUrl { get; set; } 
         string SiteAccountId { get; set; }
    }
}
