﻿namespace CreditExchange.Syndication.YodleeYsl
{
    public class YodleeYslRestUrls : IYodleeYslRestUrls
    {

        public string BaseRestUrl { get; set; } //= "https://yieapstage.api.yodlee.com/ysl/private-CreditExchange/v1";

        public string BaseItemSummariesUrl { get; set; }// = "https://yieap.stage.yodleeinteractive.com/services/srest/private-CreditExchange/v1.0";

        public string BaseFetchReportUrl { get; set; } //= "https://yieap.stage.yodleeinteractive.com/services/srest/private-CreditExchange/v1.0";

        public string FastLinkUrl { get; set; }// = "https://yieapnode.stage.yodleeinteractive.com/authenticate/private-CreditExchange/?channelAppName=yieap";

        public string FastLinkAppId { get; set; } = "10003600";

        public string CobrandLogin { get; set; } = "/cobrand/login";

        public string RegisterUser { get; set; } = "/user/register";

        public string LoginUser { get; set; } = "/user/login";

        public string SearchBank { get; set; } = "/providers?name={0}";

        public string GetBankLoginForm { get; set; } = "/providers/{0}";

        public string PostBankLoginForm { get; set; } = "/providers/providerAccounts?providerId={0}";

        public string UpdateBankLoginForm { get; set; } = "/providers/providerAccounts?providerAccountIds={0}";

        public string GetMfaDetails { get; set; } = "/providers/providerAccounts/{0}";

        public string PostMfaDetails { get; set; } = "/providers/providerAccounts?providerAccountIds={0}";

        public string RefreshDetails { get; set; } = "/refresh/{0}";

        public string StartRefreshDetails { get; set; } = "/jsonsdk/Refresh/startSiteRefresh";

        public string ItemSummaries { get; set; } = "/jsonsdk/DataService/getItemSummariesForSite";

        public string FetchReport { get; set; } = "/accountanalysis/fetchReport";

        public string AccessToken { get; set; } = "/user/accessTokens";

        public string GetUserAccounts { get; set; } = "/accounts";

        public string RemoveSiteAccount { get; set; } = "/jsonsdk/SiteAccountManagement/removeSiteAccount";

        public string UserCredentialsInstanceType { get; set; } = "com.yodlee.ext.login.PasswordCredentials";
        public string UserProfileInstanceType { get; set; } = "com.yodlee.core.usermanagement.UserProfile";
        public string PreferredCurrency { get; set; } = "PREFERRED_CURRENCY~USD";
        public string PreferredDateFormate { get; set; } = "PREFERRED_DATE_FORMAT~MM/dd/yyyy";

        public string SearchForBank { get; set; } = "/jsonsdk/SiteTraversal/searchSiteWithFilter";

        public string SiteAccountId { get; set; } = "";
    }
}
