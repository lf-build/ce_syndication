﻿namespace CreditExchange.Syndication.Crif
{
    public interface IApplicantName
    {
        string Name1 { get; set; }
        string Name2 { get; set; }
        string Name3 { get; set; }
        string Name4 { get; set; }
        string Name5 { get; set; }
    }
}