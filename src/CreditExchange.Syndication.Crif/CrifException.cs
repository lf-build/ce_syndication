﻿using System;
using System.Runtime.Serialization;

namespace CreditExchange.Syndication.Crif
{
    [Serializable]
    public class CrifException : Exception
    {
        public CrifException()
        {
        }

        public CrifException(string message) : base(message)
        {
        }

        public CrifException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected CrifException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
