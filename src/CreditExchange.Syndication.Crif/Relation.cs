namespace CreditExchange.Syndication.Crif
{
    public class Relation : IRelation
    {
        public string Value { get; set; }
        public RelationType Type { get; set; }
    }
}