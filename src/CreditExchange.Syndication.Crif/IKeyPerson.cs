﻿using CreditExchange.Syndication.Crif.CreditReport.SendInquiry;

namespace CreditExchange.Syndication.Crif
{
    public interface IKeyPerson
    {
        string Name { get; set; }
        ApplicantSegmentKeyPersonType Type { get; set; }
    }
}