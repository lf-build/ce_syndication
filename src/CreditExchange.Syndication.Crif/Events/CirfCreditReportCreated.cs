﻿using LendFoundry.SyndicationStore.Events;

namespace CreditExchange.Syndication.Crif.Events
{
    public class CirfCreditReportCreated : SyndicationCalledEvent
    {
    }
}
