﻿namespace CreditExchange.Syndication.Crif.Events
{
    public class EventSource
    {
        public string ReferenceNumber { get; set; }
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public object Request { get; set; }
        public object Response { get; set; }
    }
}
