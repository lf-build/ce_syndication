﻿namespace CreditExchange.Syndication.Crif
{
    public interface IIdentification
    {
        IdentificationType Type { get; set; }
        string Value { get; set; }
    }
}