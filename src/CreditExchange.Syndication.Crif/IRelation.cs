﻿namespace CreditExchange.Syndication.Crif
{
    public interface IRelation
    {
        RelationType Type { get; set; }
        string Value { get; set; }
    }
}