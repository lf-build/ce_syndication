using System.IO;
using System.Text;

namespace CreditExchange.Syndication.Crif
{
    internal sealed class ExtentedStringWriter : StringWriter
    {
        public ExtentedStringWriter(Encoding desiredEncoding)
        {
            Encoding = desiredEncoding;
        }

        public override Encoding Encoding { get; }
    }
}