namespace CreditExchange.Syndication.Crif
{

    public class Identification : IIdentification
    {
        public IdentificationType Type { get; set; }
        public string Value { get; set; }
    }
}