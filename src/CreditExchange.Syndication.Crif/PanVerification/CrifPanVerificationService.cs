﻿using System;
using System.Threading.Tasks;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using CreditExchange.Syndication.Crif.Events;
using CreditExchange.Syndication.Crif.PanVerification.Proxy;

using CreditExchange.Syndication.Crif.PanVerification.Request;
using CreditExchange.Syndication.Crif.PanVerification.Response;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using System.Linq;
using System.Text.RegularExpressions;

namespace CreditExchange.Syndication.Crif.PanVerification
{
    public class CrifPanVerificationService :ICrifPanVerificationService
    {
        public CrifPanVerificationService(ICrifPanVerificationConfiguration configuration, ICrifVerficationProxy proxy, IEventHubClient eventHub, ILookupService lookup)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));

            Configuration = configuration;
            Proxy = proxy;
            EventHub = eventHub;
            Lookup = lookup;
        }
        private ICrifPanVerificationConfiguration Configuration { get; }
        private ICrifVerficationProxy Proxy { get; }
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }

        public static string ServiceName { get; } = "crif-pan-verification";
        public async Task<IPanVerificationResponse> VerifyPan(string entityType, string entityId,IPanVerificationRequest panVerificationRequest)
        {

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} can not be null");
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} can not be null");
            entityType = EnsureEntityType(entityType);
            if (panVerificationRequest == null)
                throw new ArgumentException($"paload data can not be null or empty");
            if (string.IsNullOrEmpty(panVerificationRequest.Pan))
                throw new ArgumentException($"{nameof(panVerificationRequest.Pan)} can not be null");
            if (string.IsNullOrEmpty(panVerificationRequest.Name))
                throw new ArgumentException($"{nameof(panVerificationRequest.Name)} can not be null");
            if(string.IsNullOrEmpty(panVerificationRequest.Dob.ToString()))
                throw new ArgumentException($"{nameof(panVerificationRequest.Dob)} can not be null");
            DateTime temp;
            if (!DateTime.TryParse(panVerificationRequest.Dob.ToString(), out temp))
            {
                throw new ArgumentException($"{nameof(panVerificationRequest.Dob)} value is Not valid datetime");
            }
            Regex regex = new Regex(@"^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$");
           if (!regex.IsMatch(panVerificationRequest.Pan))
            {
                throw new ArgumentException($"{nameof(panVerificationRequest.Pan)} value is Not valid ");
            }
           if(!(Enum.IsDefined(typeof(Gender), panVerificationRequest.Gender)))
                {
                throw new ArgumentException($"{nameof(panVerificationRequest.Gender)} value is Not valid ");
              }


                var proxyRequest = new Proxy.Request(Configuration, panVerificationRequest);
            var response = new GetPanVerificationResponse(Proxy.PanVerification(proxyRequest));
            var referenceNumber = Guid.NewGuid().ToString("N");
            response.ReferenceNumber = referenceNumber;
            await EventHub.Publish(new PanVerificationRequested
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = response,
                Request = panVerificationRequest,
                ReferenceNumber = referenceNumber,
                Name = ServiceName
            });
            return response;
        }

        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}
