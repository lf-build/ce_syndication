﻿using System;
using System.Net;
using RestSharp;
using Newtonsoft.Json;
using LendFoundry.Foundation.Logging;

namespace CreditExchange.Syndication.Crif.PanVerification.Proxy
{
    public class CrifVerficationProxy :ICrifVerficationProxy
    {
        public CrifVerficationProxy(ICrifPanVerificationConfiguration configuration,ILogger logger)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (string.IsNullOrWhiteSpace(configuration.UserId))
                throw new ArgumentException(nameof(configuration.UserId));
            if (string.IsNullOrWhiteSpace(configuration.Password))
                throw new ArgumentException(nameof(configuration.Password));
            if (string.IsNullOrWhiteSpace(configuration.ApiUrl))
                throw new ArgumentException(nameof(configuration.ApiUrl));
            if (string.IsNullOrWhiteSpace(configuration.MemberId))
                throw new ArgumentException(nameof(configuration.MemberId));
            if (string.IsNullOrWhiteSpace(configuration.ProductType))
                throw new ArgumentException(nameof(configuration.ProductType));
            if (string.IsNullOrWhiteSpace(configuration.ProductVersion))
                throw new ArgumentException(nameof(configuration.ProductVersion));
            if (string.IsNullOrWhiteSpace(configuration.RequestVolumeType))
                throw new ArgumentException(nameof(configuration.RequestVolumeType));
            Configuration = configuration;
            Logger = logger;
        }
        private ILogger Logger { get; set; }
        private ICrifPanVerificationConfiguration Configuration { get; }
        public VERIFICATIONREPORT PanVerification(Request panVerificationRequest)
        {
            Logger.Info($"[CRIF] PanVerification method invoked for data  { (panVerificationRequest != null ? JsonConvert.SerializeObject(panVerificationRequest) : null)}");
            var client = new RestClient(Configuration.ApiUrl);
            var request = new RestRequest(Method.POST);
            var requestxml = XmlSerialization.Serialize(panVerificationRequest);
          
            request.AddHeader("userId", Configuration.UserId);
            request.AddHeader("password", Configuration.Password);
            request.AddHeader("requestXML", requestxml);
            //X509Certificate2 certificate = new X509Certificate2();
            //certificate.Import(Encoding.ASCII.GetBytes(Configuration.Certificate));
            //client.ClientCertificates=new X509Certificate2Collection(certificate);
            ServicePointManager.ServerCertificateValidationCallback +=
                (sender, cert, chain, sslPolicyErrors) => true;
            var response = client.Execute(request);

            if (response.ErrorException != null)
            {
                Logger.Error($"[CRIF] Error occured when PanVerification method was called. Error:", response.ErrorException);
                throw new CrifException("PAN Verification Request failed", response.ErrorException);
            }


            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK)
            {
                Logger.Error($"[CRIF] Error occured when PanVerification method was called. Error:", response.Content);
                throw new CrifException(
                    $"PAN Verification failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");
            }

            return XmlSerialization.Deserialize<VERIFICATIONREPORT>(response.Content);
        }
    }
}
