﻿namespace CreditExchange.Syndication.Crif.PanVerification.Proxy
{
    public interface ICrifVerficationProxy
    {
        VERIFICATIONREPORT PanVerification(Request panVerificationRequest);
    }
}