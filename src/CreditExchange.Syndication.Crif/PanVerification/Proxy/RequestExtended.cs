﻿using System;
using CreditExchange.Syndication.Crif.PanVerification.Request;
using CreditExchange.Syndication.Crif.PanVerification.Model;

namespace CreditExchange.Syndication.Crif.PanVerification.Proxy
{
    public partial class Request
    {
        public Request()
        {
        }

        public Request(ICrifPanVerificationConfiguration verificationConfiguration,
            IPanVerificationRequest getvPanVerificationRequest)
        {
            if (verificationConfiguration == null)
                throw new ArgumentNullException(nameof(verificationConfiguration));
            if (getvPanVerificationRequest == null)
                throw new ArgumentNullException(nameof(getvPanVerificationRequest));
            if (string.IsNullOrWhiteSpace(verificationConfiguration.ProductType))
                throw new ArgumentException("ProductType is required", nameof(verificationConfiguration.ProductType));
            if (string.IsNullOrWhiteSpace(verificationConfiguration.ProductVersion))
                throw new ArgumentException("ProductVersion is required",
                    nameof(verificationConfiguration.ProductVersion));
            if (string.IsNullOrWhiteSpace(verificationConfiguration.MemberId))
                throw new ArgumentException("MemberId is required", nameof(verificationConfiguration.MemberId));
            if (string.IsNullOrWhiteSpace(verificationConfiguration.SubMemberId))
                throw new ArgumentException("SubMemberId is required", nameof(verificationConfiguration.SubMemberId));
            if (string.IsNullOrWhiteSpace(verificationConfiguration.UserId))
                throw new ArgumentException("UserId is required", nameof(verificationConfiguration.UserId));
            if (string.IsNullOrWhiteSpace(verificationConfiguration.Password))
                throw new ArgumentException("Password is required", nameof(verificationConfiguration.Password));
            if (string.IsNullOrWhiteSpace(verificationConfiguration.RequestVolumeType))
                throw new ArgumentException("RequestVolumeType is required",
                    nameof(verificationConfiguration.RequestVolumeType));
            if (string.IsNullOrWhiteSpace(verificationConfiguration.ApiUrl))
                throw new ArgumentException("ApiUrl  is required", nameof(verificationConfiguration.ApiUrl));
            
            if (string.IsNullOrWhiteSpace(verificationConfiguration.ServiceType))
                throw new ArgumentException("ServiceType  is required ", nameof(verificationConfiguration.ServiceType));
            if (string.IsNullOrEmpty(verificationConfiguration.ActionType))
                throw new ArgumentException("ActionType is require", nameof(verificationConfiguration.ActionType));

        

            HEADERSEGMENT = new HEADERSEGMENT(verificationConfiguration, getvPanVerificationRequest);
            INQUIRY = new INQUIRY(getvPanVerificationRequest);
        }
    }

    public partial class HEADERSEGMENT
    {
        public HEADERSEGMENT()
        {
        }

        public HEADERSEGMENT(ICrifPanVerificationConfiguration verificationConfiguration,
            IPanVerificationRequest verificationRequest)
        {
            AUTHFLG = verificationConfiguration.AuthFlag;
            //AUTHTITLE = verificationConfiguration.AuthTitle;
            LOSNAME = verificationConfiguration.LosName;
            LOSVERSION = verificationConfiguration.LosVersion;
            LOSVENDER = verificationConfiguration.LosVendor;
            //MEMBERPREOVERRIDE = verificationConfiguration.MemberPreOverride;
            PRODUCTTYP = HEADERSEGMENTPRODUCTTYP.IDENCHECK;
            PRODUCTVER = HEADERSEGMENTPRODUCTVER.Item20;
            REQMBR = verificationConfiguration.MemberId;
            SUBMBRID = verificationConfiguration.SubMemberId;
            REQACTNTYP =
                (HEADERSEGMENTREQACTNTYP) Enum.Parse(typeof(HEADERSEGMENTREQACTNTYP), verificationConfiguration.ActionType);
            REQVOLTYP =
                (HEADERSEGMENTREQVOLTYP)
                Enum.Parse(typeof(HEADERSEGMENTREQVOLTYP), verificationConfiguration.RequestVolumeType);
            INQDTTM =  verificationConfiguration.InquiryDateTime.ToString("dd-MM-yyyy hh:mm:ss");
            TESTFLG = verificationConfiguration.TestFlag;
            RESFRMT = verificationConfiguration.ResponseFormat.ToString();
         //   RESFRMTEMBD = verificationConfiguration.IsEmbededResponse.ToString();
        }
    }


    public partial class INQUIRY
    {
        public INQUIRY()
        {
        }

        public INQUIRY(IPanVerificationRequest verificationRequest)
        {
            if (verificationRequest == null)
                throw new ArgumentNullException(nameof(verificationRequest));
            //if (verificationRequest.Addresses != null)

            //    ADDRESSSEGMENT = new ADDRESSSEGMENT
            //    {
            //        ADDRESS = verificationRequest.Addresses.Select(p => new ADDRESSSEGMENTADDRESS(p)).ToArray()
            //    };
            APPLICANTSEGMENT = new APPLICANTSEGMENT(verificationRequest);
        }
    }


    public partial class ADDRESSSEGMENTADDRESS
    {
        public ADDRESSSEGMENTADDRESS()
        {
        }

        public ADDRESSSEGMENTADDRESS(IAddress addressSegment)
        {
            if (addressSegment != null)
            {
                ADDRESS1 = addressSegment.Address1;
                CITY = addressSegment.City;
                if(!string.IsNullOrEmpty(addressSegment.Pin))
                  PIN = Convert.ToUInt16(addressSegment.Pin);
                STATE = (ADDRESSSEGMENTADDRESSSTATE) addressSegment.State;
                TYPE = (ADDRESSSEGMENTADDRESSTYPE) addressSegment.AddressType;
            }
        }
    }

    public partial class APPLICANTSEGMENT
    {
        public APPLICANTSEGMENT()
        {
        }

        public APPLICANTSEGMENT(IPanVerificationRequest verificationRequest)
        {
            if (verificationRequest == null)
                throw new ArgumentNullException(nameof(verificationRequest));
            DOB = new APPLICANTSEGMENTDOB(Convert.ToDateTime(verificationRequest.Dob).ToString("dd-MM-yyyy"));
            GENDER = verificationRequest.Gender.ToString();
            //   APPLICANTNAME = new APPLICANTSEGMENTAPPLICANTNAME(verificationRequest.ApplicantNames);
            APPLICANTNAME = new APPLICANTSEGMENTAPPLICANTNAME(verificationRequest.Name);
          //  EMAILS = verificationRequest.Emails.ToArray();
            IDS = new APPLICANTSEGMENTIDS(verificationRequest.Pan);
            //PHONES = verificationRequest.ApplicantPhones.Select(p => new APPLICANTSEGMENTPHONE(p)).ToArray();
            //RELATIONS = verificationRequest.ApplicantRelations.Select(p => new APPLICANTSEGMENTRELATION(p)).ToArray();
        }
    }

    public partial class APPLICANTSEGMENTRELATION
    {
        public APPLICANTSEGMENTRELATION()
        {
        }

        public APPLICANTSEGMENTRELATION(IApplicantRelation applicantRelation)
        {
            if (applicantRelation != null)
            {
                NAME = applicantRelation.Name;
                TYPE = (APPLICANTSEGMENTRELATIONTYPE) applicantRelation.RelationType;
            }
        }
    }

    public partial class APPLICANTSEGMENTPHONE
    {
        public APPLICANTSEGMENTPHONE()
        {
        }

        public APPLICANTSEGMENTPHONE(IApplicantPhone applicantPhone)
        {
            if (applicantPhone == null)
                throw new ArgumentNullException(nameof(applicantPhone));
            TELENO = Convert.ToUInt32( applicantPhone.PhoneNo);
            TELENOTYPE = (APPLICANTSEGMENTPHONETELENOTYPE) applicantPhone.PhoneType;
        }
    }

    public partial class APPLICANTSEGMENTIDS
    {
        public APPLICANTSEGMENTIDS()
        {
        }

        public APPLICANTSEGMENTIDS(string pan)
        {
            if (string.IsNullOrEmpty(pan))
                throw new ArgumentException(nameof(pan));
            //DOB = applicantId.Dob;
            PAN = pan;
           // UID = applicantId.Uid;
        }
    }

    public partial class APPLICANTSEGMENTDOB
    {
        public APPLICANTSEGMENTDOB()
        {
        }

        public APPLICANTSEGMENTDOB(string dob)
        {
            DOBDATE = dob;
        }
    }

    public partial class APPLICANTSEGMENTAPPLICANTNAME
    {
        public APPLICANTSEGMENTAPPLICANTNAME()
        {
        }

        public APPLICANTSEGMENTAPPLICANTNAME(IApplicantName applicantName)
        {
            if (applicantName != null)
            {
                if (string.IsNullOrEmpty(applicantName.Name1))
                    throw new ArgumentException("Applicant Name is require", nameof(applicantName.Name1));
                NAME1 = applicantName.Name1;
                NAME2 = applicantName.Name2;
                NAME3 = applicantName.Name3;
                NAME4 = applicantName.Name4;
                NAME5 = applicantName.Name5;
            }
        }
        public APPLICANTSEGMENTAPPLICANTNAME(String applicantName)
        {
            if (applicantName != null)
            {
                if (string.IsNullOrEmpty(applicantName))
                    throw new ArgumentException("Applicant Name is require", nameof(applicantName));
                NAME1 = applicantName;
            
            }
        }
    }
}
