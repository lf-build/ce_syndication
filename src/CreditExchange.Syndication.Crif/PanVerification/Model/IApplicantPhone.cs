﻿namespace CreditExchange.Syndication.Crif.PanVerification.Model
{
    public interface IApplicantPhone
    {
        string PhoneNo { get; set; }
        PhoneType PhoneType { get; set; }
    }
}