﻿namespace CreditExchange.Syndication.Crif.PanVerification.Model
{
    public class ApplicantPhone :IApplicantPhone
    {
        public string PhoneNo { get; set; } 
        public PhoneType PhoneType { get; set; }
    }
}
