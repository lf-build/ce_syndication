﻿

namespace CreditExchange.Syndication.Crif.PanVerification.Model
{
    public class Address :IAddress
    {
        public AddressType AddressType { get; set; }

        public  string Address1 { get; set; }

        public  string City { get; set; }

        public  State State { get; set; }

        public string Pin { get; set; }
    }
}
