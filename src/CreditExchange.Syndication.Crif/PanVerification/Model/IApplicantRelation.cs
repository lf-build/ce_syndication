﻿namespace CreditExchange.Syndication.Crif.PanVerification.Model
{
    public interface IApplicantRelation
    {
        string Name { get; set; }
        RelationType RelationType { get; set; }
    }
}