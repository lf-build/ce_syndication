﻿


namespace CreditExchange.Syndication.Crif.PanVerification.Model
{
    public interface IAddress
    {
        string Address1 { get; set; }
        AddressType AddressType { get; set; } 
        string City { get; set; }
        string Pin { get; set; }
        State State { get; set; }
    }
}