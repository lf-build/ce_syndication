﻿namespace CreditExchange.Syndication.Crif.PanVerification.Model
{
    public class ApplicantRelation :IApplicantRelation
    {
        public string Name { get; set; }
        public RelationType RelationType { get; set; }
    }
}
