﻿using System;

namespace CreditExchange.Syndication.Crif.PanVerification.Request
{
    public class PanVerificationRequest :IPanVerificationRequest
    {
        public  string Name { get; set; }
        public string Dob { get; set; }
        public string Gender { get; set; }
        public string Pan { get; set; }
    }
}
