﻿using System;

namespace CreditExchange.Syndication.Crif.PanVerification.Request
{
    public interface IPanVerificationRequest 
    {
        string Gender { get; set; }
        string Dob { get; set; }
        string Pan { get; set; }
        string Name { get; set; }
    }
}