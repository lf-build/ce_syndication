﻿namespace CreditExchange.Syndication.Crif.PanVerification
{
    public interface ICrifPanVerificationConfiguration  :ICrifConfiguration
    {
        string ServiceType { get; set; }
    }
}