﻿using System.Threading.Tasks;
using CreditExchange.Syndication.Crif.PanVerification.Request;
using CreditExchange.Syndication.Crif.PanVerification.Response;

namespace CreditExchange.Syndication.Crif.PanVerification
{
    public interface ICrifPanVerificationService
    {
        Task<IPanVerificationResponse> VerifyPan(string entityType, string entityId, IPanVerificationRequest panVerificationRequest);
    }
}