﻿using System;
using CreditExchange.Syndication.Crif.PanVerification.Proxy;
using System.Linq;

namespace CreditExchange.Syndication.Crif.PanVerification.Response
{
    public class GetPanVerificationResponse : IPanVerificationResponse
    {
        public GetPanVerificationResponse()
        {
            
        }

        public GetPanVerificationResponse(VERIFICATIONREPORT  verificationreport )
        {
            if (verificationreport?.RESPONSES == null)
                return;

            var panresponse =
                verificationreport.RESPONSES.FirstOrDefault(p => p.REQSERVICETYPE == "PAN");
            var firstOrDefault = panresponse?.SCORES?.FirstOrDefault();
             if(firstOrDefault== null)
             throw new CrifException(panresponse?.DESCRIPTION);
             Score = Convert.ToInt32(firstOrDefault.VALUE);
             Description =Score == 0 ? panresponse.DESCRIPTION : firstOrDefault.DESCRIPTION;
            Status = panresponse?.STATUS;
             if (panresponse?.REMARKS != null) Remarks = panresponse.REMARKS.FirstOrDefault();
        }
        public string Status {get;set;}
        public  int Score { get; set; }
        public string Remarks { get; set; }
        public  string Description { get; set; }
        public  string ReferenceNumber { get; set; }
    }
}
