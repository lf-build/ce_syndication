﻿namespace CreditExchange.Syndication.Crif.PanVerification.Response
{
    public interface IPanVerificationResponse
    {
        string Description { get; set; }
        string Remarks { get; set; }
        int Score { get; set; }
        string ReferenceNumber { get; set; }
        string Status { get; set; }
    }
}