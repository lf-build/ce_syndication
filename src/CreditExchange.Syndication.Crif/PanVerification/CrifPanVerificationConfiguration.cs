﻿namespace CreditExchange.Syndication.Crif.PanVerification
{
    public class CrifPanVerificationConfiguration : CrifConfiguration, ICrifPanVerificationConfiguration
    {
        public CrifPanVerificationConfiguration()
        {
            ProductType = "IDENCHECK";
            ProductVersion = "2.0";
            RequestVolumeType = "INDV";
            ApiUrl = "https://test.highmark.in/Inquiry/Verification/VerificationService";
        }
        public string ServiceType { get; set; } = "PAN";

    }
}
