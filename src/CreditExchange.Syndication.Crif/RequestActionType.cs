namespace CreditExchange.Syndication.Crif
{
    public enum RequestActionType
    {
        Submit,

        Issue,

        ForceNew,

        Reissue,

        Update,

        Upgrade,

        StatusUpdate
    }
}