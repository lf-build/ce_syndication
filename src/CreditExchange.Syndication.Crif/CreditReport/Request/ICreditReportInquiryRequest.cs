﻿using System;
using System.Collections.Generic;
using CreditExchange.Syndication.Crif.CreditReport.SendInquiry;

namespace CreditExchange.Syndication.Crif.CreditReport.Request
{
    public interface ICreditReportInquiryRequest 
    {
        string Name { get; set; }
        string Dob { get; set; }
        Gender Gender { get; set; }
        string Pan { get; set; }
        string Uid { get; set; }
        string RationCard { get; set; }
        string Others { get; set; }
        string Passport { get; set; }
        string VoterId { get; set; }
        string DrivingLicNo { get; set; }
        string MobileNo { get; set; }
        string LandlineNo { get; set; }
        List<IAddressSegment> Addresses { get; set; }
    }
}
