﻿using System;
using System.Collections.Generic;
using CreditExchange.Syndication.Crif.CreditReport.SendInquiry;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.Crif.CreditReport.Request
{
    public class CreditReportInquiryRequest : ICreditReportInquiryRequest
    {
        [JsonConverter(typeof(InterfaceListConverter<IAddressSegment, AddressSegment>))]
        public List<IAddressSegment> Addresses { get; set; }
        public string Name { get; set; }
        public string Dob { get; set; }
        public Gender Gender { get; set; }
        public string Pan { get; set; }
        public string Uid { get; set;}
        public string RationCard { get; set; }

        public string Others { get; set; }
        public string Passport { get; set; }
        public string VoterId { get; set; }
        public string DrivingLicNo { get; set; }
        public string MobileNo { get; set; }
        public string LandlineNo { get; set; }
    }
}
