﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Crif.CreditReport
{
    public interface ICrifCreditReport : IAggregate
    {
         string EntityId { get; set; }  
         string EntityType { get; set; }
         string Inquiryreferencenumber { get; set; }
         string ReportId { get; set; }
        TimeBucket CreatedDate { get; set; }
    }
}
