﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IResponseInquiry
    {
        string Description { get; set; }
        string InquiryUniqueReferenceNumber { get; set; }
        string ResponseType { get; set; }
        string RequestDatetime { get; set; }
        string ResponseDatetime { get; set; }
    }
}