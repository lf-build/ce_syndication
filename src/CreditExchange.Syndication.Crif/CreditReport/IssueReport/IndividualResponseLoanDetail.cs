﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class IndividualResponseLoanDetail : IIndividualResponseLoanDetail
    {
        public IndividualResponseLoanDetail()
        {

        }
        public IndividualResponseLoanDetail(INDVRESPONSESINDVRESPONSELOANDETAIL individualResponseLoanDetail)
        {
            if(individualResponseLoanDetail!=null)
            {
                AccountType = individualResponseLoanDetail.ACCTTYPE;
                Frequency = individualResponseLoanDetail.FREQ;
                Status = individualResponseLoanDetail.STATUS;
                AccountNumber = individualResponseLoanDetail.ACCTNUMBER;
                DisbursedAmount = individualResponseLoanDetail.DISBURSEDAMT;
                CurrentBalance = individualResponseLoanDetail.CURRENTBAL;
               InstallmentAmount = individualResponseLoanDetail.INSTALLMENTAMT;
                OverdueAmount = individualResponseLoanDetail.OVERDUEAMT;
                WriteOffAmount = individualResponseLoanDetail.WRITEOFFAMT;
                WriteOffDate = individualResponseLoanDetail.WRITEOFFDT;
                LoanCycleId = individualResponseLoanDetail.LOANCYCLEID;
                DisbursedDate = individualResponseLoanDetail.DISBURSEDDT;
                ClosedDate = individualResponseLoanDetail.CLOSEDDT;
                RecentDelinqDate = individualResponseLoanDetail.RECENTDELINQDT;
                CombinedPaymentHistory = individualResponseLoanDetail.COMBINEDPAYMENTHISTORY;
                InquiryCount = individualResponseLoanDetail.INQCNT;
                InfoAsOn = individualResponseLoanDetail.INFOASON;
                WorstDelequencyAmount = individualResponseLoanDetail.WORSTDELEQUENCYAMOUNT;
                ActiveBorrowers = individualResponseLoanDetail.ACTIVEBORROWERS;
                NumberOfBorrowers = individualResponseLoanDetail.NOOFBORROWERS;
                Comment = individualResponseLoanDetail.COMMENT;
              }
        }
        public string AccountType { get; set; }
        public string Frequency { get; set; }
        public string Status { get; set; }
        public string AccountNumber { get; set; }
        public string DisbursedAmount { get; set; }
        public string CurrentBalance { get; set; }
        public string InstallmentAmount { get; set; }
        public string OverdueAmount { get; set; }
        public string WriteOffAmount { get; set; }
        public string WriteOffDate { get; set; }
        public string LoanCycleId { get; set; }
        public string DisbursedDate { get; set; }
        public string ClosedDate { get; set; }
        public string RecentDelinqDate { get; set; }
        public string CombinedPaymentHistory { get; set; }
        public string InquiryCount { get; set; }
        public string InfoAsOn { get; set; }
        public string WorstDelequencyAmount { get; set; }
        public string ActiveBorrowers { get; set; }
        public string NumberOfBorrowers { get; set; }
        public string Comment { get; set; }
    }
}
