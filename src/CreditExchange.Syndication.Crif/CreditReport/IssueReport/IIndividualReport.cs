﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IIndividualReport
    {
         IResponseHeader Header { get; set; }
         List<IStatusDetail> StatusDetails { get; set; }
         IRequest Request { get; set; }
         IPersonalInfoVariation PersonalInfoVariation { get; set; }
         List<ISecondaryMatch> SecondaryMatches { get; set; }
         IAccountSummary PrimaryAccountSummary { get; set; }
         IAccountSummary SecondaryAccountSummary { get; set; }
         IAccountDerivedAttribute AccountDerivedAttribute { get; set; }
         List<ILoanDetail> Responses { get; set; }
         IIndividualResponseSummary IndividualResponses { get; set; }
         IGroup GroupResponses { get; set; }
         List<IHistory> InquiryHistory { get; set; }
         List<IComment> Comments { get; set; }
         List<IAlert> Alerts { get; set; }
         List<IScore> Scores { get; set; }
         IPrintableReport Printablereport { get; set; }
         List<IDerivedAttribute> DerivedAttributes { get; set; }
    }
}