﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IStatusDetail
    {
        List<string> Errors { get; set; }
        string Option { get; set; }
        string OptionStatus { get; set; }
    }
}