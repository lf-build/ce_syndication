﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IComment
    {
        string CommentDate { get; set; }
        string CommentText { get; set; }
    }
}