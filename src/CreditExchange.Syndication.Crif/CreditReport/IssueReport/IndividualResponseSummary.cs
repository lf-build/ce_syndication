﻿using System.Collections.Generic;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;
using System.Linq;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class IndividualResponseSummary : IIndividualResponseSummary
    {
        public IndividualResponseSummary()
        {

        }
        public IndividualResponseSummary(INDVRESPONSES individualResponse)
        {
            if(individualResponse!=null)
            {
                if (individualResponse.SUMMARY != null)
                    Summary = new Summary(individualResponse.SUMMARY);
                if (individualResponse.PRIMARYSUMMARY != null)
                    PrimarySummary = new IndividualResponsesPrimarySummary(individualResponse.PRIMARYSUMMARY);
                if (individualResponse.SECONDARYSUMMARY != null)
                    SecondarySummary = new IndividualResponsesSecondarySummary(individualResponse.SECONDARYSUMMARY);
                if (individualResponse.INDVRESPONSELIST != null)
                    IndividualReponseList = individualResponse.INDVRESPONSELIST.Select(p => new IndividualResponse(p)).ToList<IIndividualResponse>();
            }
        }
        [JsonConverter(typeof(InterfaceConverter<ISummary, Summary>))]
        public ISummary Summary { get; set; }

       
        public IndividualResponsesPrimarySummary PrimarySummary { get; set; }

      
        public IndividualResponsesSecondarySummary SecondarySummary { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IIndividualResponse, IIndividualResponse>))]
        public List<IIndividualResponse> IndividualReponseList { get; set; }
    }
}
