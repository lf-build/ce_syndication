﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IRequest
    {
        string AccountOpenDate { get; set; }
        List<string> Addresses { get; set; }
        string Age { get; set; }
        string AgeAsOn { get; set; }
        string Aka { get; set; }
        string Branch { get; set; }
        string CreditInquiryPurposeType { get; set; }
        string CreditInquiryPurposeTypeDescription { get; set; }
        string CreditInquiryStage { get; set; }
        string CreditReportId { get; set; }
        string CreditReportTransactionDatetime { get; set; }
        string CreditRequestType { get; set; }
        string Dob { get; set; }
        List<string> Emails { get; set; }
        string EntityId { get; set; }
        string Father { get; set; }
        string Gender { get; set; }
        List<IId> Ids { get; set; }
        string Kendra { get; set; }
        string LoanAmount { get; set; }
        string LosAppId { get; set; }
        string MemberId { get; set; }
        string Mother { get; set; }
        string Name { get; set; }
        string Ownership { get; set; }
        List<string> Phones { get; set; }
        string Spouse { get; set; }
    }
}