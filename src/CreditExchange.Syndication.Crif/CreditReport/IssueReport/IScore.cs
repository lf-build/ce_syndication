﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IScore
    {
        string ScoreComments { get; set; }
        string ScoreFactors { get; set; }
        string ScoreType { get; set; }
        string ScoreValue { get; set; }
        string ScoreVersion { get; set; }
    }
}