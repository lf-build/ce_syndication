﻿using System.Collections.Generic;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;
using System.Linq;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class DerivedAttribute : IDerivedAttribute
    {
        public DerivedAttribute()
        {

        }
        public DerivedAttribute(DERIVEDATTRIBUTESDERIVEDATTRIBUTE derivedAttribute)
        {
            if(derivedAttribute!=null)
            {
                Name = derivedAttribute.Name;
                Value = derivedAttribute.Value;
                Remark = derivedAttribute.Remark.ToList();
            }
        }
        public string Name { get; set; }

        public string Value { get; set; }

        public List<string> Remark { get; set; }
    }
}
