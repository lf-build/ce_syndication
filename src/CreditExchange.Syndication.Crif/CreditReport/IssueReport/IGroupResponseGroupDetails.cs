﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IGroupResponseGroupDetails
    {
        string GroupId { get; set; }
        string TotalAccounts { get; set; }
        string TotalCurrentBalance { get; set; }
        string TotalDelinqMember { get; set; }
        string TotalDisbursedAmount { get; set; }
        string TotalDpd30 { get; set; }
        string TotalDpd60 { get; set; }
        string TotalDpd90 { get; set; }
        string TotalInstallmentAmount { get; set; }
        string TotalMember { get; set; }
        string TotalOverdueAmount { get; set; }
        string TotalWriteOffs { get; set; }
    }
}