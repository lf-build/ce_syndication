﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IIndividualResponseSummary
    {
        List<IIndividualResponse> IndividualReponseList { get; set; }
        IndividualResponsesPrimarySummary PrimarySummary { get; set; }
        IndividualResponsesSecondarySummary SecondarySummary { get; set; }
        ISummary Summary { get; set; }
    }
}