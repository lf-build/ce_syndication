﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class PrintableReport : IPrintableReport
    {
        public PrintableReport()
        {

        }
        public PrintableReport(PRINTABLEREPORT printableReport)
        {
            if(printableReport!=null)
            {
                Type = printableReport.TYPE;
                Filename = printableReport.FILENAME;
                Content = printableReport.CONTENT;
            }
        }
        public string Type { get; set; }
        public string Filename { get; set; }
        public string Content { get; set; }
    }
}
