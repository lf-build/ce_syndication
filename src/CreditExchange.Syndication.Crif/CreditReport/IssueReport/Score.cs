﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class Score : IScore
    {
        public Score()
        {

        }
        public Score(SCORESSCORE score)
        {
            if(score!=null)
            {
                ScoreType = score.SCORETYPE;
                ScoreValue = score.SCOREVALUE;
                ScoreVersion = score.SCOREVERSION;
                ScoreFactors = score.SCOREFACTORS;
                ScoreComments = score.SCORECOMMENTS;
            }
        }
        public string ScoreType { get; set; }
        public string ScoreValue { get; set; }
        public string ScoreVersion { get; set; }
        public string ScoreFactors { get; set; }
        public string ScoreComments { get; set; }
    }
}
