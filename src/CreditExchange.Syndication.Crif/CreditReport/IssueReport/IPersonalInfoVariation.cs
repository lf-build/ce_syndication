﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IPersonalInfoVariation
    {
        List<IVariation> AddressVariations { get; set; }
        List<IVariation> DateOfBirthVariations { get; set; }
        List<IVariation> DrivingLicenseVariations { get; set; }
        List<IVariation> EmailVariations { get; set; }
        List<IVariation> NameVariations { get; set; }
        List<IVariation> PanVariations { get; set; }
        List<IVariation> PassportVariations { get; set; }
        List<IVariation> PhoneNumberVariations { get; set; }
        List<IVariation> RationCardVariations { get; set; }
        List<IVariation> VoterIdVariations { get; set; }
    }
}