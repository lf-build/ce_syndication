﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IGroupResponsesPrimarySummary
    {
        string MaxWorstDelequency { get; set; }
        string NumberOfActiveAccounts { get; set; }
        string NumberOfClosedAccounts { get; set; }
        string NumberOfDefaultAccounts { get; set; }
        string NumberOfOtherMfis { get; set; }
        string TotalOtherCurrentBalance { get; set; }
        string TotalOtherDisbursedAmount { get; set; }
        string TotalOtherInstallmentAmount { get; set; }
        string TotalOwnCurrentBalance { get; set; }
        string TotalOwnDisbursedAmount { get; set; }
        string TotalOwnInstallmentAmount { get; set; }
        string TotalResponses { get; set; }
    }
}