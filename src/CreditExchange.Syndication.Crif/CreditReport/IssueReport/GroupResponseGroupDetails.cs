﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class GroupResponseGroupDetails : IGroupResponseGroupDetails
    {
        public GroupResponseGroupDetails()
        {

        }
        public GroupResponseGroupDetails(GRPRESPONSESGRPRESPONSEGROUPDETAILS groupResponseGroupDetails)
        {
            if (groupResponseGroupDetails != null)
            {
                GroupId = groupResponseGroupDetails.GROUPID;
                TotalAccounts = groupResponseGroupDetails.TOTACCOUNTS;
                TotalCurrentBalance = groupResponseGroupDetails.TOTCURRENTBAL;
                TotalDelinqMember = groupResponseGroupDetails.TOTDELINQMBR;
                TotalInstallmentAmount = groupResponseGroupDetails.TOTINSTALLMENTAMT;
                TotalOverdueAmount = groupResponseGroupDetails.TOTOVERDUEAMT;
                TotalDisbursedAmount = groupResponseGroupDetails.TOTDISBURSEDAMT;
                TotalMember = groupResponseGroupDetails.TOTMEMBER;
                TotalDpd30 = groupResponseGroupDetails.TOTDPD30;
                TotalDpd60 = groupResponseGroupDetails.TOTDPD60;
                TotalDpd90 = groupResponseGroupDetails.TOTDPD90;
                TotalWriteOffs = groupResponseGroupDetails.TOTWRITEOFFS;
            }
        }
        public string GroupId { get; set; }
        public string TotalDisbursedAmount { get; set; }
        public string TotalCurrentBalance { get; set; }
        public string TotalInstallmentAmount { get; set; }
        public string TotalOverdueAmount { get; set; }
        public string TotalAccounts { get; set; }
        public string TotalMember { get; set; }
        public string TotalDelinqMember { get; set; }
        public string TotalDpd30 { get; set; }
        public string TotalDpd60 { get; set; }
        public string TotalDpd90 { get; set; }
        public string TotalWriteOffs { get; set; }
    }
}
