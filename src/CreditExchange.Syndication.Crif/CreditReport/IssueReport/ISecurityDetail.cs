﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface ISecurityDetail
    {
        string AutomobileType { get; set; }
        string ChassisNumber { get; set; }
        string DateOfValue { get; set; }
        string EngineNumber { get; set; }
        string OwnerName { get; set; }
        string PropertyAddress { get; set; }
        string RegistrationNumber { get; set; }
        string SecurityCharge { get; set; }
        string SecurityType { get; set; }
        string SecurityValue { get; set; }
        string YearOfManufacture { get; set; }
    }
}