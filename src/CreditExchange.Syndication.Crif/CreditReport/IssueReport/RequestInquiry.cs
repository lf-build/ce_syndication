﻿using System;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class RequestInquiry : IRequestInquiry
    {
        public string InquiryUniqueReferenceNumber { get; set; }
        public DateTime RequestDatetime { get; set; }
        public string ReportId { get; set; }
    }
}
