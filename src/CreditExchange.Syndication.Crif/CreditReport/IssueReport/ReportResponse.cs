﻿using System.Collections.Generic;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;
using System.Linq;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class IndividualReport : IIndividualReport
    {
        public IndividualReport()
        {
         

        }
        public IndividualReport(INDVREPORT indvReport)
        {
            if(indvReport!=null)
            {
                if (indvReport.HEADER != null)
                    Header = new ResponseHeader(indvReport.HEADER);
                if (indvReport.STATUSDETAILS != null)
                    StatusDetails = indvReport.STATUSDETAILS.Select(p => new StatusDetail(p)).ToList<IStatusDetail>();
                if (indvReport.REQUEST != null)
                    Request = new Request(indvReport.REQUEST);
                if (indvReport.PERSONALINFOVARIATION != null)
                    PersonalInfoVariation = new PersonalInfoVariation(indvReport.PERSONALINFOVARIATION);
                if (indvReport.SECONDARYMATCHES != null)
                    SecondaryMatches = indvReport.SECONDARYMATCHES.Select(p => new SecondaryMatch(p)).ToList<ISecondaryMatch>();
                if (indvReport.ACCOUNTSSUMMARY?.PRIMARYACCOUNTSSUMMARY != null)
                    PrimaryAccountSummary = new AccountSummary(indvReport.ACCOUNTSSUMMARY.PRIMARYACCOUNTSSUMMARY);
                if (indvReport.ACCOUNTSSUMMARY?.SECONDARYACCOUNTSSUMMARY != null)
                    SecondaryAccountSummary = new AccountSummary(indvReport.ACCOUNTSSUMMARY.SECONDARYACCOUNTSSUMMARY);
                if (indvReport.ACCOUNTSSUMMARY?.ACCDERIVEDATTRIBUTES != null)
                    AccountDerivedAttribute = new AccountDerivedAttribute(indvReport.ACCOUNTSSUMMARY?.ACCDERIVEDATTRIBUTES);
                if (indvReport.RESPONSES != null)
                    Responses = indvReport.RESPONSES.Select(p => new LoanDetail(p.LOANDETAILS)).ToList<ILoanDetail>();
                if (indvReport.GRPRESPONSES != null)
                    GroupResponses = new Group(indvReport.GRPRESPONSES);
                if (indvReport.INQUIRYHISTORY != null)
                    InquiryHistory = indvReport.INQUIRYHISTORY.Select(p => new History(p)).ToList<IHistory>();
                if (indvReport.COMMENTS != null)
                    Comments = indvReport.COMMENTS.Select(p => new Comment(p)).ToList<IComment>();
                if (indvReport.ALERTS != null)
                    Alerts = indvReport.ALERTS.Select(p => new Alert(p)).ToList<IAlert>();
                if (indvReport.SCORES != null)
                    Scores = indvReport.SCORES.Select(p => new Score(p)).ToList<IScore>();
                if (indvReport.PRINTABLEREPORT != null)
                    Printablereport = new PrintableReport(indvReport.PRINTABLEREPORT);
                if (indvReport.DERIVEDATTRIBUTES != null)
                    DerivedAttributes = indvReport.DERIVEDATTRIBUTES.Select(p => new DerivedAttribute(p)).ToList<IDerivedAttribute>();

            }
        }
        [JsonConverter(typeof(InterfaceConverter<IResponseHeader, ResponseHeader>))]
        public IResponseHeader Header { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IStatusDetail, StatusDetail>))]
        public List<IStatusDetail> StatusDetails { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRequest, Request>))]
        public IRequest Request { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPersonalInfoVariation, PersonalInfoVariation>))]
        public IPersonalInfoVariation PersonalInfoVariation { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISecondaryMatch, SecondaryMatch>))]
        public List<ISecondaryMatch> SecondaryMatches { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAccountSummary, AccountSummary>))]
        public IAccountSummary PrimaryAccountSummary { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAccountSummary, AccountSummary>))]
        public IAccountSummary SecondaryAccountSummary { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAccountDerivedAttribute, AccountDerivedAttribute>))]
        public IAccountDerivedAttribute AccountDerivedAttribute { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ILoanDetail, LoanDetail>))]
        public List<ILoanDetail> Responses { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IIndividualResponseSummary, IndividualResponseSummary>))]
        public IIndividualResponseSummary IndividualResponses { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IGroup, Group>))]
        public IGroup GroupResponses { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IHistory, History>))]
        public List<IHistory> InquiryHistory { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IComment, Comment>))]
        public List<IComment> Comments { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IAlert, Alert>))]
        public List<IAlert> Alerts { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IScore, Score>))]
        public List<IScore> Scores { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPrintableReport, PrintableReport>))]
        public IPrintableReport Printablereport { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IDerivedAttribute, DerivedAttribute>))]
        public List<IDerivedAttribute> DerivedAttributes { get; set; }
    }
}
