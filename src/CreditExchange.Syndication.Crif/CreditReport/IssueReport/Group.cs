﻿using System.Collections.Generic;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;
using System.Linq;
using Newtonsoft.Json;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Client;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class Group : IGroup
    {
        public Group()
        {

        }
        public Group(GRPRESPONSES groupResponse)
        {
            if(groupResponse!=null)
            {
                if(groupResponse.SUMMARY!=null)
                Summary = new GroupResponsesSummary(groupResponse.SUMMARY);
                if (groupResponse.PRIMARYSUMMARY != null)
                    PrimarySummary = new GroupResponsesPrimarySummary(groupResponse.PRIMARYSUMMARY);
                if (groupResponse.SECONDARYSUMMARY != null)
                    SecondarySummary = new GroupResponsesSecondarySummary(groupResponse.SECONDARYSUMMARY);
                if (groupResponse.GRPRESPONSELIST != null)
                    GroupResponseList = groupResponse.GRPRESPONSELIST.Select(p => new GroupResponse(p)).ToList<IGroupResponse>();
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IGroupResponsesSummary, GroupResponsesSummary>))]
        public IGroupResponsesSummary Summary { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IGroupResponsesPrimarySummary, GroupResponsesPrimarySummary>))]
        public IGroupResponsesPrimarySummary PrimarySummary { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IGroupResponsesSecondarySummary, GroupResponsesSecondarySummary>))]
        public IGroupResponsesSecondarySummary SecondarySummary { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IGroupResponse, GroupResponse>))]
        public List<IGroupResponse> GroupResponseList { get; set; }
    }
}
