﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class AccountSummary : IAccountSummary
    {
        public AccountSummary()
        {

        }
        public AccountSummary(PRIMARYACCOUNTSSUMMARY primaryaccountSummary)
        {
            if(primaryaccountSummary!=null)
            {
                NumberOfAccounts = primaryaccountSummary.PRIMARYNUMBEROFACCOUNTS;
                ActiveNumberOfAccounts = primaryaccountSummary.PRIMARYACTIVENUMBEROFACCOUNTS;
                OverdueNumberOfAccounts = primaryaccountSummary.PRIMARYOVERDUENUMBEROFACCOUNTS;
                CurrentBalance = primaryaccountSummary.PRIMARYCURRENTBALANCE;
                SanctionedAmount = primaryaccountSummary.PRIMARYDISBURSEDAMOUNT;
                DisbursedAmount = primaryaccountSummary.PRIMARYDISBURSEDAMOUNT;
                SecuredNumberOfAccounts = primaryaccountSummary.PRIMARYSECUREDNUMBEROFACCOUNTS;
                UnsecuredNumberOfAccounts = primaryaccountSummary.PRIMARYSECUREDNUMBEROFACCOUNTS;
                UntaggedNumberOfAccounts = primaryaccountSummary.PRIMARYUNTAGGEDNUMBEROFACCOUNTS;
            }
        }
        public AccountSummary(SECONDARYACCOUNTSSUMMARY secondaryaccountSummary)
        {
            if (secondaryaccountSummary != null)
            {
                NumberOfAccounts = secondaryaccountSummary.SECONDARYNUMBEROFACCOUNTS;
                ActiveNumberOfAccounts = secondaryaccountSummary.SECONDARYACTIVENUMBEROFACCOUNTS;
                OverdueNumberOfAccounts = secondaryaccountSummary.SECONDARYOVERDUENUMBEROFACCOUNTS;
                CurrentBalance = secondaryaccountSummary.SECONDARYCURRENTBALANCE;
                SanctionedAmount = secondaryaccountSummary.SECONDARYSANCTIONEDAMOUNT;
                DisbursedAmount = secondaryaccountSummary.SECONDARYDISBURSEDAMOUNT;
                SecuredNumberOfAccounts = secondaryaccountSummary.SECONDARYSECUREDNUMBEROFACCOUNTS;
                UnsecuredNumberOfAccounts = secondaryaccountSummary.SECONDARYUNSECUREDNUMBEROFACCOUNTS;
                UntaggedNumberOfAccounts = secondaryaccountSummary.SECONDARYUNTAGGEDNUMBEROFACCOUNTS;
            }
        }
        public string NumberOfAccounts { get; set; }
        public string ActiveNumberOfAccounts { get; set; }
        public string OverdueNumberOfAccounts { get; set; }
        public string CurrentBalance { get; set; }
        public string SanctionedAmount { get; set; }
        public string DisbursedAmount { get; set; }
        public string SecuredNumberOfAccounts { get; set; }
        public string UnsecuredNumberOfAccounts { get; set; }
        public string UntaggedNumberOfAccounts { get; set; }
    }
}
