﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
   public interface ILoanDetail 
    {
         string AccountNumber { get; set; }
         string CreditGuarantor { get; set; }
         string AccountType { get; set; }
         string DateReported { get; set; }
         string OwnershipIndicator { get; set; }
         string AccountStatus { get; set; }
         string DisbursedAmount { get; set; }
         string DisbursedDate { get; set; }
         string LastPaymentDate { get; set; }
         string ClosedDate { get; set; }
         string OverdueAmount { get; set; }
         string WriteoffAmount { get; set; }
         string CurrentBalance { get; set; }
         string CreditLimit { get; set; }
         string AccountRemarks { get; set; }
         string SecurityStatus { get; set; }
         string Frequency { get; set; }
         string OriginalTerm { get; set; }
         string TermToMaturity { get; set; }
         string AccountInDispute { get; set; }
         string SettlementAmount { get; set; }
         string CombinedPaymentHistory { get; set; }
         string MatchedType { get; set; }
         string RepaymentTenure { get; set; }
         string SuitFiledWilfulDefault { get; set; }
         string WrittenOffSettledStatus { get; set; }
         string CashLimit { get; set; }
         string ActualPayment { get; set; }
         string InstallmentAmount { get; set; }
         List<ISecurityDetail> SecurityDetails { get; set; }
         ILinkAccount LinkedAccounts { get; set; }
    }
}