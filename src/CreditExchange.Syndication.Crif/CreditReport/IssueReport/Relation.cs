﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class Relation : IRelation
    {
        public Relation()
        {

        }
        public Relation(RELATIONSRELATION relation)
        {
            if(relation!=null)
            {
                Type = relation.TYPE;
                Name = relation.NAME;
            }
        }
        public string Type { get; set; }
        public string Name { get; set; }
    }
}
