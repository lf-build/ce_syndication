﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IAlert
    {
        string AlertDescription { get; set; }
        string AlertType { get; set; }
    }
}