﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IAccountSummary
    {
        string ActiveNumberOfAccounts { get; set; }
        string CurrentBalance { get; set; }
        string DisbursedAmount { get; set; }
        string NumberOfAccounts { get; set; }
        string OverdueNumberOfAccounts { get; set; }
        string SanctionedAmount { get; set; }
        string SecuredNumberOfAccounts { get; set; }
        string UnsecuredNumberOfAccounts { get; set; }
        string UntaggedNumberOfAccounts { get; set; }
    }
}