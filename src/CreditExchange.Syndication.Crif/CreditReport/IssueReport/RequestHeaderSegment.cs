using System;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class RequestHeaderSegment : IRequestHeaderSegment
    {
        public string ProductType { get; set; }
        public string ProductVersion { get; set; }
        public string MemberId { get; set; }
        public string SubMemberId { get; set; }
        public DateTime InquiryDatetime { get; set; }
        public RequestVolumeType RequestVolumeType { get; set; }
        public RequestActionType RequestActionType { get; set; }
        public string TestFlag { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string AuthFlag { get; set; }
        public string AuthTitle { get; set; }
        public string ResponseFormat { get; set; }
        public string MemberPreOverride { get; set; }
        public string ResponseFormatEmbedded { get; set; }
        public string LosName { get; set; }
        public string LosVender { get; set; }
        public string LosVersion { get; set; }
    }
}