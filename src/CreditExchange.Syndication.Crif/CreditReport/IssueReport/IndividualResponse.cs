﻿using System.Collections.Generic;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;
using System.Linq;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class IndividualResponse : IIndividualResponse
    {
        public IndividualResponse()
        {

        }
        public IndividualResponse(INDVRESPONSESINDVRESPONSE individualResponse)
        {
            if(individualResponse!=null)
            {
                MatchedType = individualResponse.MATCHEDTYPE;
                Mfi = individualResponse.MFI;
                Branch = individualResponse.BRANCH;
                Kendra = individualResponse.KENDRA;
                Name = individualResponse.NAME;
                DateOfBirth = individualResponse.DOB;
                Age = individualResponse.AGE;
                AgeAsOn = individualResponse.AGEASON;
                Relations = individualResponse.RELATIONS.Select(p => new Relation(p)).ToList<IRelation>();
                Phones = individualResponse.PHONES.ToList();
                Addresses = individualResponse.ADDRESSES.ToList();
                Ids = individualResponse.IDS.Select(p => new Id(p)).ToList<IId>();
                Emails = individualResponse.EMAILS.ToList();
                CnsmrMemberId = individualResponse.CNSMRMBRID;
                EntityId = individualResponse.ENTITYID;
                GroupCreationDate = individualResponse.GROUPCREATIONDATE;
                InsertDate = individualResponse.INSERTDATE;
                if (individualResponse.GROUPDETAILS != null)
                    GroupDetails = new IndividualResponseGroupDetails(individualResponse.GROUPDETAILS);
                if (individualResponse.LOANDETAIL != null)
                    LoanDetail = new IndividualResponseLoanDetail(individualResponse.LOANDETAIL);
            }
        }
        public string MatchedType { get; set; }
        public string Mfi { get; set; }
        public string MfiId { get; set; }
        public string Branch { get; set; }
        public string Kendra { get; set; }
        public string Name { get; set; }
        public string DateOfBirth { get; set; }
        public string Age { get; set; }
        public string AgeAsOn { get; set; }
        public List<IRelation> Relations { get; set; }
        public List<string> Phones { get; set; }
        public List<string> Addresses { get; set; }
        public  List<IId> Ids { get; set; }
        public List<string> Emails { get; set; }
        public string CnsmrMemberId { get; set; }
        public string EntityId { get; set; }
        public string GroupCreationDate { get; set; }
        public string InsertDate { get; set; }
        public IIndividualResponseGroupDetails GroupDetails { get; set; }
        public IIndividualResponseLoanDetail LoanDetail { get; set; }
    }
}
