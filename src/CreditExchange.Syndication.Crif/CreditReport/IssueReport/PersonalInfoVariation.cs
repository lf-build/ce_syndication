﻿using System.Collections.Generic;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;
using System.Linq;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class PersonalInfoVariation : IPersonalInfoVariation
    {
        public PersonalInfoVariation()
        {

        }
        public PersonalInfoVariation(PERSONALINFOVARIATION personalInfovariation)
        {
            if(personalInfovariation!=null)
            {
                if(personalInfovariation.NAMEVARIATIONS!=null)
                NameVariations = personalInfovariation.NAMEVARIATIONS.Select(p => new Variation(p)).ToList<IVariation>();
                if (personalInfovariation.ADDRESSVARIATIONS != null)
                AddressVariations = personalInfovariation.ADDRESSVARIATIONS.Select(p => new Variation(p)).ToList<IVariation>();
                if (personalInfovariation.PANVARIATIONS != null)
                    PanVariations = personalInfovariation.PANVARIATIONS.Select(p => new Variation(p)).ToList<IVariation>();
                if (personalInfovariation.DRIVINGLICENSEVARIATIONS != null)
                    DrivingLicenseVariations = personalInfovariation.DRIVINGLICENSEVARIATIONS.Select(p => new Variation(p)).ToList<IVariation>();
                if (personalInfovariation.DATEOFBIRTHVARIATIONS != null)
                    DateOfBirthVariations = personalInfovariation.DATEOFBIRTHVARIATIONS.Select(p => new Variation(p)).ToList<IVariation>();
                if (personalInfovariation.VOTERIDVARIATIONS != null)
                    VoterIdVariations = personalInfovariation.VOTERIDVARIATIONS.Select(p => new Variation(p)).ToList<IVariation>();
                if (personalInfovariation.PASSPORTVARIATIONS != null)
                    PassportVariations = personalInfovariation.PASSPORTVARIATIONS.Select(p => new Variation(p)).ToList<IVariation>();
                if (personalInfovariation.PHONENUMBERVARIATIONS != null)
                    PhoneNumberVariations = personalInfovariation.PHONENUMBERVARIATIONS.Select(p => new Variation(p)).ToList<IVariation>();
                if (personalInfovariation.PHONENUMBERVARIATIONS != null)
                    RationCardVariations = personalInfovariation.RATIONCARDVARIATIONS.Select(p => new Variation(p)).ToList<IVariation>();
                if (personalInfovariation.EMAILVARIATIONS != null)
                    EmailVariations = personalInfovariation.EMAILVARIATIONS.Select(p => new Variation(p)).ToList<IVariation>();
             }

         }
        [JsonConverter(typeof(InterfaceListConverter<IVariation, Variation>))]
        public List<IVariation> NameVariations { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IVariation, Variation>))]
        public List<IVariation> AddressVariations { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IVariation, Variation>))]
        public List<IVariation> PanVariations { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IVariation, Variation>))]
        public List<IVariation> DrivingLicenseVariations { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IVariation, Variation>))]
        public List<IVariation> DateOfBirthVariations { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IVariation, Variation>))]
        public List<IVariation> VoterIdVariations { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IVariation, Variation>))]
        public List<IVariation> PassportVariations { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IVariation, Variation>))]
        public List<IVariation> PhoneNumberVariations { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IVariation, Variation>))]
        public List<IVariation> RationCardVariations { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IVariation, Variation>))]
        public List<IVariation> EmailVariations { get; set; }
    }
}
