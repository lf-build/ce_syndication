﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class Comment : IComment
    {
        public Comment()
        {

        }
        public Comment(COMMENTSCOMMENT comment)
        {
            if(comment!=null)
            {
                CommentText = comment.COMMENTTEXT;
                CommentDate = comment.COMMENTDATE;

            }
        }
        public string CommentText { get; set; }
        public string CommentDate { get; set; }
    }
}
