﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class GroupResponsesPrimarySummary : IGroupResponsesPrimarySummary
    {
        public GroupResponsesPrimarySummary()
        {

        }
        public GroupResponsesPrimarySummary(GRPRESPONSESPRIMARYSUMMARY groupResponsesPrimarySummary)
        {
            if(groupResponsesPrimarySummary!=null)
            {
                NumberOfDefaultAccounts = groupResponsesPrimarySummary.NOOFDEFAULTACCOUNTS;
                TotalResponses = groupResponsesPrimarySummary.TOTALRESPONSES;
                NumberOfClosedAccounts = groupResponsesPrimarySummary.NOOFCLOSEDACCOUNTS;
                NumberOfActiveAccounts = groupResponsesPrimarySummary.NOOFACTIVEACCOUNTS;
                NumberOfOtherMfis = groupResponsesPrimarySummary.NOOFOTHERMFIS;
                TotalOtherDisbursedAmount = groupResponsesPrimarySummary.TOTALOTHERDISBURSEDAMOUNT;
                TotalOtherCurrentBalance = groupResponsesPrimarySummary.TOTALOWNCURRENTBALANCE;
                TotalOtherInstallmentAmount = groupResponsesPrimarySummary.TOTALOTHERINSTALLMENTAMOUNT;
                TotalOwnDisbursedAmount = groupResponsesPrimarySummary.TOTALOWNDISBURSEDAMOUNT;
                TotalOwnCurrentBalance = groupResponsesPrimarySummary.TOTALOWNCURRENTBALANCE;
                TotalOwnInstallmentAmount = groupResponsesPrimarySummary.TOTALOWNINSTALLMENTAMOUNT;
                MaxWorstDelequency = groupResponsesPrimarySummary.MAXWORSTDELEQUENCY;
           }
        }
        public string NumberOfDefaultAccounts { get; set; }
        public string TotalResponses { get; set; }
        public string NumberOfClosedAccounts { get; set; }
        public string NumberOfActiveAccounts { get; set; }
        public string NumberOfOtherMfis { get; set; }
        public string TotalOtherDisbursedAmount { get; set; }
        public string TotalOtherCurrentBalance { get; set; }
        public string TotalOtherInstallmentAmount { get; set; }
        public string TotalOwnDisbursedAmount { get; set; }
        public string TotalOwnCurrentBalance { get; set; }
        public string TotalOwnInstallmentAmount { get; set; }
        public string MaxWorstDelequency { get; set; }
    }
}
