﻿using System.Collections.Generic;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;
using System.Linq;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class SecondaryMatch : ISecondaryMatch
    {
        public SecondaryMatch()
        {

        }
        public SecondaryMatch(SECONDARYMATCHESSECONDARYMATCH secondaryMatch)
        {
            if(secondaryMatch!=null)
            {
                Name = secondaryMatch.NAME;
                DateOfBirth = secondaryMatch.DOB;
                Relations = secondaryMatch.RELATIONS.Select(p => new Relation(p)).ToList<IRelation>();
                Phones = secondaryMatch.PHONES.ToList();
                Addresses = secondaryMatch.ADDRESSES.ToList();
                Ids = secondaryMatch.IDS.Select(p => new Id(p)).ToList<IId>();
                Emails = secondaryMatch.EMAILS.ToList();
                LoanDetails = new LoanDetail(secondaryMatch.LOANDETAILS);
            }
        }
        public string Name { get; set; }
        public string DateOfBirth { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IRelation, Relation>))]
        public List<IRelation> Relations { get; set; }
        public List<string> Phones { get; set; }
        public List<string> Addresses { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IId, Id>))]
        public List<IId> Ids { get; set; }
        public List<string> Emails { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ILoanDetail, LoanDetail>))]
        public ILoanDetail LoanDetails { get; set; }
    }
}
