﻿using System.Collections.Generic;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;
using System.Linq;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class StatusDetail : IStatusDetail
    {
        public StatusDetail()
        {

        }
        public StatusDetail(STATUSDETAILSSTATUS statusDetail)
        {
            if(statusDetail!=null)
            {
                Option = statusDetail.OPTION;
                OptionStatus = statusDetail.OPTIONSTATUS;
                Errors = statusDetail.ERRORS?.Select(p => p.ERRORDESCRIPTION).ToList();
            }
        }
        public string Option { get; set; }
        public string OptionStatus { get; set; }
        public List<string> Errors { get; set; }
    }
}
