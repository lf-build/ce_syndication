﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IRelation
    {
        string Name { get; set; }
        string Type { get; set; }
    }
}