﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IPrintableReport
    {
        string Content { get; set; }
        string Filename { get; set; }
        string Type { get; set; }
    }
}