﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IResponseHeader
    {
        string BatchId { get; set; }
        string DateOfIssue { get; set; }
        string DateOfRequest { get; set; }
        string PreparedFor { get; set; }
        string PreparedForId { get; set; }
        string ReportId { get; set; }
    }
}