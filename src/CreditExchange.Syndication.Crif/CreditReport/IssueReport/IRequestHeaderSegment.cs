﻿using System;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IRequestHeaderSegment
    {
        string AuthFlag { get; set; }
        string AuthTitle { get; set; }
        DateTime InquiryDatetime { get; set; }
        string LosName { get; set; }
        string LosVender { get; set; }
        string LosVersion { get; set; }
        string MemberId { get; set; }
        string MemberPreOverride { get; set; }
        string Password { get; set; }
        string ProductType { get; set; }
        string ProductVersion { get; set; }
        RequestActionType RequestActionType { get; set; }
        RequestVolumeType RequestVolumeType { get; set; }
        string ResponseFormat { get; set; }
        string ResponseFormatEmbedded { get; set; }
        string SubMemberId { get; set; }
        string TestFlag { get; set; }
        string UserId { get; set; }
    }
}