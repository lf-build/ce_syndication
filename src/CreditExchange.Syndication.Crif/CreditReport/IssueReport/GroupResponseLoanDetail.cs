﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class GroupResponseLoanDetail : IGroupResponseLoanDetail
    {
        public GroupResponseLoanDetail()
        {

        }
        public GroupResponseLoanDetail(GRPRESPONSESGRPRESPONSELOANDETAIL groupResponseLoanDetail)
        {
            if (groupResponseLoanDetail != null)
            {
                AccountType = groupResponseLoanDetail.ACCTTYPE;
                Frequency = groupResponseLoanDetail.FREQ;
                Status = groupResponseLoanDetail.STATUS;
                AccountNumber = groupResponseLoanDetail.ACCTNUMBER;
                DisbursedAmount = groupResponseLoanDetail.DISBURSEDAMT;
                CurrentBalance = groupResponseLoanDetail.CURRENTBAL;
                InstallmentAmount = groupResponseLoanDetail.INSTALLMENTAMT;
                OverdueAmount = groupResponseLoanDetail.OVERDUEAMT;
                WriteOffAmount = groupResponseLoanDetail.WRITEOFFAMT;
                WriteOffDate = groupResponseLoanDetail.WRITEOFFDT;
                LoanCycleId = groupResponseLoanDetail.LOANCYCLEID;
                DisbursedDate = groupResponseLoanDetail.DISBURSEDDT;
                ClosedDate = groupResponseLoanDetail.CLOSEDDT;
                RecentDelinqDate = groupResponseLoanDetail.RECENTDELINQDT;
                CombinedPaymentHistory = groupResponseLoanDetail.COMBINEDPAYMENTHISTORY;
                InquiryCount = groupResponseLoanDetail.INQCNT;
                InfoAsOn = groupResponseLoanDetail.INFOASON;
                WorstDelequencyAmount = groupResponseLoanDetail.WORSTDELEQUENCYAMOUNT;
                ActiveBorrowers = groupResponseLoanDetail.ACTIVEBORROWERS;
                NumberOfBorrowers = groupResponseLoanDetail.NOOFBORROWERS;
                Comment = groupResponseLoanDetail.COMMENT;
            }
        }
        public string AccountType { get; set; }
        public string Frequency { get; set; }
        public string Status { get; set; }
        public string AccountNumber { get; set; }
        public string DisbursedAmount { get; set; }
        public string CurrentBalance { get; set; }
        public string InstallmentAmount { get; set; }
        public string OverdueAmount { get; set; }
        public string WriteOffAmount { get; set; }
        public string WriteOffDate { get; set; }
        public string LoanCycleId { get; set; }
        public string DisbursedDate { get; set; }
        public string ClosedDate { get; set; }
        public string RecentDelinqDate { get; set; }
        public string CombinedPaymentHistory { get; set; }
        public string InquiryCount { get; set; }
        public string InfoAsOn { get; set; }
        public string WorstDelequencyAmount { get; set; }
        public string ActiveBorrowers { get; set; }
        public string NumberOfBorrowers { get; set; }
        public string Comment { get; set; }
    }
}
