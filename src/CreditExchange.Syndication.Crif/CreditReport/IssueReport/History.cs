﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class History : IHistory
    {
        public History()
        {

        }
        public History(Proxy.Issue.Response.HISTORY history)
        {
            if(history!=null)
            {
                MemberName = history.MEMBERNAME;
                InquiryDate = history.INQUIRYDATE;
                Purpose = history.PURPOSE;
                OwnershipType = history.OWNERSHIPTYPE;
                Amount = history.AMOUNT;
                Remark = history.REMARK;
            }
        }
        public string MemberName { get; set; }
        public string InquiryDate { get; set; }
        public string Purpose { get; set; }
        public string OwnershipType { get; set; }
        public string Amount { get; set; }
        public string Remark { get; set; }
    }
}
