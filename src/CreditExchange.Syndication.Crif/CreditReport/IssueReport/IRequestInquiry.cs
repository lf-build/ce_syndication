﻿using System;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IRequestInquiry
    {
        string InquiryUniqueReferenceNumber { get; set; }
        string ReportId { get; set; }
        DateTime RequestDatetime { get; set; }
    }
}