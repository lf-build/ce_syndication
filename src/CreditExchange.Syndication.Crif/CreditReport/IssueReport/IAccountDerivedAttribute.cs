﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IAccountDerivedAttribute
    {
        string AverageAccountAgeMonth { get; set; }
        string AverageAccountAgeYear { get; set; }
        string InquiriesInLastSixMonths { get; set; }
        string LengthOfCreditHistoryMonth { get; set; }
        string LengthOfCreditHistoryYear { get; set; }
        string NewAccountsInLastSixMonths { get; set; }
        string NewDelinqAccountInLastSixMonths { get; set; }
    }
}