﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IGroupResponseLoanDetail
    {
        string AccountNumber { get; set; }
        string AccountType { get; set; }
        string ActiveBorrowers { get; set; }
        string ClosedDate { get; set; }
        string CombinedPaymentHistory { get; set; }
        string Comment { get; set; }
        string CurrentBalance { get; set; }
        string DisbursedAmount { get; set; }
        string DisbursedDate { get; set; }
        string Frequency { get; set; }
        string InfoAsOn { get; set; }
        string InquiryCount { get; set; }
        string InstallmentAmount { get; set; }
        string LoanCycleId { get; set; }
        string NumberOfBorrowers { get; set; }
        string OverdueAmount { get; set; }
        string RecentDelinqDate { get; set; }
        string Status { get; set; }
        string WorstDelequencyAmount { get; set; }
        string WriteOffAmount { get; set; }
        string WriteOffDate { get; set; }
    }
}