﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class GroupResponsesSecondarySummary : IGroupResponsesSecondarySummary
    {
        public GroupResponsesSecondarySummary()
        {

        }
        public GroupResponsesSecondarySummary(GRPRESPONSESSECONDARYSUMMARY groupResponsesSecondarySummary)
        {
            if (groupResponsesSecondarySummary != null)
            {
                NumberOfDefaultAccounts = groupResponsesSecondarySummary.NOOFDEFAULTACCOUNTS;
                TotalResponses = groupResponsesSecondarySummary.TOTALRESPONSES;
                NumberOfClosedAccounts = groupResponsesSecondarySummary.NOOFCLOSEDACCOUNTS;
                NumberOfActiveAccounts = groupResponsesSecondarySummary.NOOFACTIVEACCOUNTS;
                NumberOfOtherMfis = groupResponsesSecondarySummary.NOOFOTHERMFIS;
                TotalOtherDisbursedAmount = groupResponsesSecondarySummary.TOTALOTHERDISBURSEDAMOUNT;
                TotalOtherCurrentBalance = groupResponsesSecondarySummary.TOTALOWNCURRENTBALANCE;
                TotalOtherInstallmentAmount = groupResponsesSecondarySummary.TOTALOTHERINSTALLMENTAMOUNT;
                TotalOwnDisbursedAmount = groupResponsesSecondarySummary.TOTALOWNDISBURSEDAMOUNT;
                TotalOwnCurrentBalance = groupResponsesSecondarySummary.TOTALOWNCURRENTBALANCE;
                TotalOwnInstallmentAmount = groupResponsesSecondarySummary.TOTALOWNINSTALLMENTAMOUNT;
                MaxWorstDelequency = groupResponsesSecondarySummary.MAXWORSTDELEQUENCY;
            }
        }
        public string NumberOfDefaultAccounts { get; set; }
        public string TotalResponses { get; set; }
        public string NumberOfClosedAccounts { get; set; }
        public string NumberOfActiveAccounts { get; set; }
        public string NumberOfOtherMfis { get; set; }
        public string TotalOtherDisbursedAmount { get; set; }
        public string TotalOtherCurrentBalance { get; set; }
        public string TotalOtherInstallmentAmount { get; set; }
        public string TotalOwnDisbursedAmount { get; set; }
        public string TotalOwnCurrentBalance { get; set; }
        public string TotalOwnInstallmentAmount { get; set; }
        public string MaxWorstDelequency { get; set; }
    }
}
