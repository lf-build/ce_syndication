﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IDerivedAttribute
    {
        string Name { get; set; }
        List<string> Remark { get; set; }
        string Value { get; set; }
    }
}