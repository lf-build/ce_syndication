﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IIndividualResponsesSecondarySummary
    {
         string NumberOfDefaultAccounts { get; set; }
         string TotalResponses { get; set; }
         string NumberOfClosedAccounts { get; set; }
         string NumberOfActiveAccounts { get; set; }
         string NumberOfOtherMfis { get; set; }
         string NumberOfOwnMfis { get; set; }

         string TotalOtherDisbursedAmount { get; set; }
         string TotalOtherCurrentBalance { get; set; }
         string TotalOtherInstallmentAmount { get; set; }
         string TotalOwnDisbursedAmount { get; set; }
         string TotalOwnCurrentBalance { get; set; }
         string TotalOwnInstallmentAmount { get; set; }
         string MaxWorstDelequency { get; set; }
    }
}