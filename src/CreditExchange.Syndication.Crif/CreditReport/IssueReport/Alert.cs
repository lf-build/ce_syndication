﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class Alert : IAlert
    {
        public Alert()
        {

        }
        public Alert(ALERTSALERT alert)
        {
            if(alert!=null)
            {
                AlertType = alert.ALERTTYPE;
                AlertDescription = alert.ALERTDESC;
            }
        }
        public string AlertType { get; set; }
        public string AlertDescription { get; set; }


    }
}
