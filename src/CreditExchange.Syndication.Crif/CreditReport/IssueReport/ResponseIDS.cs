﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class Id : IId
    {
        public Id()
        {

        }
        public Id(IDSID Id)
        {
               if(Id !=null)
            {
                Type = Id.TYPE;
                Value = Id.VALUE;
            }
        }
        
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
