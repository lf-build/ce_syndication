﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IGroupResponsesSummary
    {
         string Status { get; set; }
         string TotalResponses { get; set; }
         string NumberOfDefaultAccounts { get; set; }
         string NumberOfClosedAccounts { get; set; }
         string NumberOfActiveAccounts { get; set; }
         string NumberOfOwnMfis { get; set; }
         string OwnMfiIndicator { get; set; }
         string Errors { get; set; }
         string TotalOwnDisbursedAmount { get; set; }
         string TotalOtherDisbursedAmount { get; set; }
         string TotalOwnCurrentBalance { get; set; }
         string TotalOtherCurrentBalance { get; set; }
         string TotalOwnInstallmentAmount { get; set; }
         string TotalOtherInstallmentAmount { get; set; }
         string MaxWorstDelequency { get; set; }
        string NumberOfOtherMfis { get; set; }
    }
}