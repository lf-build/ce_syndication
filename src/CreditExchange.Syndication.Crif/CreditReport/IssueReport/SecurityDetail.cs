﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class SecurityDetail : ISecurityDetail
    {
        public SecurityDetail()
        {

        }
        public SecurityDetail(SECURITYDETAILSSECURITYDETAIL securityDetail)
        {
            if(securityDetail!=null)
            {
                SecurityType = securityDetail.SECURITYTYPE;
                OwnerName = securityDetail.OWNERNAME;
                SecurityType = securityDetail.SECURITYTYPE;
                DateOfValue = securityDetail.DATEOFVALUE;
                SecurityValue = securityDetail.SECURITYVALUE;
                PropertyAddress = securityDetail.PROPERTYADDRESS;
                AutomobileType = securityDetail.AUTOMOBILETYPE;
                YearOfManufacture = securityDetail.YEAROFMANUFACTURE;
                RegistrationNumber = securityDetail.REGISTRATIONNUMBER;
                EngineNumber = securityDetail.ENGINENUMBER;
                ChassisNumber = securityDetail.CHASSISNUMBER;
            }
        }
        public string SecurityType { get; set; }
        public string OwnerName { get; set; }
        public string SecurityValue { get; set; }
        public string DateOfValue { get; set; }
        public string SecurityCharge { get; set; }
        public string PropertyAddress { get; set; }
        public string AutomobileType { get; set; }
        public string YearOfManufacture { get; set; }
        public string RegistrationNumber { get; set; }
        public string EngineNumber { get; set; }
        public string ChassisNumber { get; set; }
    }
}
