﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IId
    {
        string Type { get; set; }
        string Value { get; set; }
    }
}