﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class Variation : IVariation
    {
        public Variation()
        {
        }

        public Variation(Proxy.Issue.Response.VARIATION variation)
        {
            if (variation != null)
            {
                Value = variation.VALUE;
                ReportedDate = variation.REPORTEDDATE;
            }
        }

        public string Value { get; set; }
        public string ReportedDate { get; set; }
    }
}