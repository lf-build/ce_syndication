﻿using System.Collections.Generic;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;
using System.Linq;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class LinkAccount : ILinkAccount
    {
        public LinkAccount()
        {

        }
        public LinkAccount(LINKEDACCOUNTSACCOUNTDETAILS linkAccount)
        {
            if(linkAccount != null)
            {
                AccountNumber = linkAccount.ACCTNUMBER;
                CreditGuarantor = linkAccount.CREDITGUARANTOR;
                AccountType = linkAccount.ACCTTYPE;
                DateReported = linkAccount.DATEREPORTED;
                OwnershipIndicator = linkAccount.OWNERSHIPIND;
                AccountStatus = linkAccount.ACCOUNTSTATUS;
                DisbursedAmount = linkAccount.DISBURSEDAMT;
                DisbursedDate = linkAccount.DISBURSEDDATE;
                LastPaymentDate = linkAccount.LASTPAYMENTDATE;
                ClosedDate = linkAccount.CLOSEDDATE;
                OverdueAmount = linkAccount.OVERDUEAMT;
                WriteoffAmount = linkAccount.WRITEOFFAMT;
                CurrentBalance = linkAccount.CURRENTBAL;
                CreditLimit = linkAccount.CREDITLIMIT;
                AccountRemarks = linkAccount.ACCOUNTREMARKS;
                SecurityStatus = linkAccount.SECURITYSTATUS;
                Frequency = linkAccount.FREQUENCY;
                OriginalTerm = linkAccount.ORIGINALTERM;
                TermToMaturity = linkAccount.TERMTOMATURITY;
                AccountInDispute = linkAccount.ACCTINDISPUTE;
                SettlementAmount = linkAccount.SETTLEMENTAMT;
                InstallmentAmount = linkAccount.INSTALLMENTAMT;
                SecurityDetails = linkAccount.SECURITYDETAILS.Select(p => new SecurityDetail(p)).ToList<ISecurityDetail>();
                PrincipalWriteOffAmount = linkAccount.PRINCIPALWRITEOFFAMT;
            }
        }
        public string AccountNumber { get; set; }
        public string CreditGuarantor { get; set; }
        public string AccountType { get; set; }
        public string DateReported { get; set; }
        public string OwnershipIndicator { get; set; }
        public string AccountStatus { get; set; }
        public string DisbursedAmount { get; set; }
        public string DisbursedDate { get; set; }
        public string LastPaymentDate { get; set; }
        public string ClosedDate { get; set; }
        public string OverdueAmount { get; set; }
        public string WriteoffAmount { get; set; }
        public string CurrentBalance { get; set; }
        public string CreditLimit { get; set; }
        public string AccountRemarks { get; set; }
        public string SecurityStatus { get; set; }
        public string Frequency { get; set; }
        public string OriginalTerm { get; set; }
        public string TermToMaturity { get; set; }
        public string AccountInDispute { get; set; }
        public string SettlementAmount { get; set; }
     
        public string PrincipalWriteOffAmount { get; set; }
        public string InstallmentAmount { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISecurityDetail, SecurityDetail>))]
        public List<ISecurityDetail> SecurityDetails { get; set; }
    }
}
