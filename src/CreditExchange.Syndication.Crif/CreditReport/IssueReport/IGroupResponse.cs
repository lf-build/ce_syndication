﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IGroupResponse
    {
         string MatchedType { get; set; }
         string Mfi { get; set; }
         string MfiId { get; set; }
         string Branch { get; set; }
         string Kendra { get; set; }
         string Name { get; set; }
         string DateOfBirth { get; set; }
         string Age { get; set; }
         string AgeAsOn { get; set; }
         List<IRelation> Relations { get; set; }
         List<string> Phones { get; set; }
         List<string> Addresses { get; set; }
         List<IId> Ids { get; set; }
         List<string> Emails { get; set; }
         string CnsmrMemberId { get; set; }
         string EntityId { get; set; }
         string GroupCreationDate { get; set; }
         string InsertDate { get; set; }
         IGroupResponseGroupDetails GroupDetails { get; set; }
         IGroupResponseLoanDetail LoanDetail { get; set; }
    }
}