﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IGroup
    {
        List<IGroupResponse> GroupResponseList { get; set; }
        IGroupResponsesPrimarySummary PrimarySummary { get; set; }
        IGroupResponsesSecondarySummary SecondarySummary { get; set; }
        IGroupResponsesSummary Summary { get; set; }
    }
}