﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class GroupResponsesSummary : IGroupResponsesSummary
    {
        public GroupResponsesSummary()
        {

        }
        public GroupResponsesSummary(GRPRESPONSESSUMMARY groupResponsesSummary)
        {
            if(groupResponsesSummary!=null)
            {
                Status = groupResponsesSummary.STATUS;
                TotalResponses = groupResponsesSummary.TOTALRESPONSES;
                NumberOfDefaultAccounts = groupResponsesSummary.NOOFDEFAULTACCOUNTS;
                NumberOfClosedAccounts = groupResponsesSummary.NOOFCLOSEDACCOUNTS;
                NumberOfActiveAccounts = groupResponsesSummary.NOOFACTIVEACCOUNTS;
                NumberOfOwnMfis = groupResponsesSummary.NOOFOWNMFIS;
                OwnMfiIndicator = groupResponsesSummary.OWNMFIINDECATOR;
                Errors = groupResponsesSummary.ERRORS;
                TotalOwnDisbursedAmount = groupResponsesSummary.TOTALOWNDISBURSEDAMOUNT;
                TotalOtherDisbursedAmount = groupResponsesSummary.TOTALOTHERDISBURSEDAMOUNT;
                TotalOwnCurrentBalance = groupResponsesSummary.TOTALOWNCURRENTBALANCE;
                TotalOtherCurrentBalance = groupResponsesSummary.TOTALOWNCURRENTBALANCE;
                TotalOwnInstallmentAmount = groupResponsesSummary.TOTALOWNINSTALLMENTAMOUNT;
                TotalOtherInstallmentAmount = groupResponsesSummary.TOTALOTHERINSTALLMENTAMOUNT;
                MaxWorstDelequency = groupResponsesSummary.MAXWORSTDELEQUENCY;
                NumberOfOtherMfis = groupResponsesSummary.NOOFOTHERMFIS;
    }
        }
        public string Status { get; set; }
        public string TotalResponses { get; set; }
        public string NumberOfDefaultAccounts { get; set; }
        public string NumberOfClosedAccounts { get; set; }
        public string NumberOfActiveAccounts { get; set; }
        public string NumberOfOwnMfis { get; set; }

        public string NumberOfOtherMfis { get; set; }
        public string OwnMfiIndicator { get; set; }
        public string Errors { get; set; }
        public string TotalOwnDisbursedAmount { get; set; }
        public string TotalOtherDisbursedAmount { get; set; }
        public string TotalOwnCurrentBalance { get; set; }
        public string TotalOtherCurrentBalance { get; set; }
        public string TotalOwnInstallmentAmount { get; set; }
        public string TotalOtherInstallmentAmount { get; set; }
        public string MaxWorstDelequency { get; set; }
    }
}
