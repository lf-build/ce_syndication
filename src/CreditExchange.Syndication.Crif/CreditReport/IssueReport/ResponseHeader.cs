﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class ResponseHeader : IResponseHeader
    {
        public ResponseHeader()
        { }
        public ResponseHeader(HEADER header)
        {
            if(header!=null)
            {
                DateOfRequest = header.DATEOFREQUEST;
                DateOfIssue = header.DATEOFISSUE;
                ReportId = header.REPORTID;
                BatchId = header.BATCHID;
                PreparedForId = header.PREPAREDFORID;
                PreparedFor = header.PREPAREDFOR;
            }
        }
        public string DateOfRequest { get; set; }
        public string PreparedFor { get; set; }
        public string PreparedForId { get; set; }
        public string DateOfIssue { get; set; }
        public string ReportId { get; set; }
        public string BatchId { get; set; }
    }
}
