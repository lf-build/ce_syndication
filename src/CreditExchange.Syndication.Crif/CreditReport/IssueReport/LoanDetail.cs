﻿using System.Collections.Generic;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;
using System.Linq;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
  
    public class LoanDetail : ILoanDetail
    {
        public LoanDetail()
        {

        }

        public LoanDetail(LOANDETAILS loanDetail)
        {
            if(loanDetail!=null)
            {
                AccountNumber = loanDetail.ACCTNUMBER;
                CreditGuarantor = loanDetail.CREDITGUARANTOR;
                AccountType = loanDetail.ACCTTYPE;
                DateReported = loanDetail.DATEREPORTED;
                OwnershipIndicator = loanDetail.OWNERSHIPIND;
                AccountStatus = loanDetail.ACCOUNTSTATUS;
                DisbursedAmount = loanDetail.DISBURSEDAMT;
                DisbursedDate = loanDetail.DISBURSEDDT;
                LastPaymentDate = loanDetail.LASTPAYMENTDATE;
                ClosedDate = loanDetail.CLOSEDDATE;
                OverdueAmount = loanDetail.OVERDUEAMT;
                WriteoffAmount = loanDetail.WRITEOFFAMT;
                CurrentBalance = loanDetail.CURRENTBAL;
                CreditLimit = loanDetail.CREDITLIMIT;
                AccountRemarks = loanDetail.ACCOUNTREMARKS;
                SecurityStatus = loanDetail.SECURITYSTATUS;
                Frequency = loanDetail.FREQUENCY;
                OriginalTerm = loanDetail.ORIGINALTERM;
                TermToMaturity = loanDetail.TERMTOMATURITY;
                AccountInDispute = loanDetail.ACCTINDISPUTE;
                SettlementAmount = loanDetail.SETTLEMENTAMT;
                CombinedPaymentHistory = loanDetail.COMBINEDPAYMENTHISTORY;
                MatchedType = loanDetail.MATCHEDTYPE;
                RepaymentTenure = loanDetail.REPAYMENTTENURE;
                SuitFiledWilfulDefault = loanDetail.SUITFILED_WILFULDEFAULT;
                WrittenOffSettledStatus = loanDetail.WRITTENOFF_SETTLEDSTATUS;
                CashLimit = loanDetail.CASHLIMIT;
                ActualPayment = loanDetail.ACTUALPAYMENT;
                InstallmentAmount = loanDetail.INSTALLMENTAMT;
                SecurityDetails = loanDetail.SECURITYDETAILS.Select(p => new SecurityDetail(p)).ToList<ISecurityDetail>();
                LinkedAccounts = new LinkAccount(loanDetail.LINKEDACCOUNTS.ACCOUNTDETAILS);
            }
        }
        public string AccountNumber { get; set; }
        public string CreditGuarantor { get; set; }
        public string AccountType { get; set; }
        public string DateReported { get; set; }
        public string OwnershipIndicator { get; set; }
        public string AccountStatus { get; set; }
        public string DisbursedAmount { get; set; }
        public string DisbursedDate { get; set; }
        public string LastPaymentDate { get; set; }
        public string ClosedDate { get; set; }
        public string OverdueAmount { get; set; }
        public string WriteoffAmount { get; set; }
        public string CurrentBalance { get; set; }
        public string CreditLimit { get; set; }
        public string AccountRemarks { get; set; }
        public string SecurityStatus { get; set; }
        public string Frequency { get; set; }
        public string OriginalTerm { get; set; }
        public string TermToMaturity { get; set; }
        public string AccountInDispute { get; set; }
        public string SettlementAmount { get; set; }
        public string CombinedPaymentHistory { get; set; }
        public string MatchedType { get; set; }
        public string RepaymentTenure { get; set; }
        public string SuitFiledWilfulDefault { get; set; }
        public string WrittenOffSettledStatus { get; set; }
        public string CashLimit { get; set; }
        public string ActualPayment { get; set; }
        public string InstallmentAmount { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISecurityDetail, SecurityDetail>))]
        public List<ISecurityDetail> SecurityDetails { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ILinkAccount, LinkAccount>))]
        public ILinkAccount LinkedAccounts { get; set; }
    }
}
