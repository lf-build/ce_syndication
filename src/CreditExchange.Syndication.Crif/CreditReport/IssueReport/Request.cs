﻿using System.Collections.Generic;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;
using System.Linq;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class Request : IRequest
    {
        public Request()
        {

        }
        public Request(REQUEST request)
        {
            if(request!=null)
            {
                Name = request.NAME;
                Aka = request.AKA;
                Spouse = request.SPOUSE;
                Father = request.FATHER;
                Mother = request.MOTHER;
                Dob = request.DOB;
                Age = request.AGE;
                AgeAsOn = request.AGEASON;
                if (request.IDS != null)
                    Ids = request.IDS.Select(p => new Id(p)).ToList<IId>();
                Gender =  request.GENDER;
                Ownership = request.OWNERSHIP;
                if(request.ADDRESSES!=null)
                Addresses = request.ADDRESSES.ToList();
                if (request.PHONES != null)
                    Phones = request.PHONES.ToList();
                if (request.EMAILS != null)
                    Emails = request.EMAILS.ToList();
                Branch = request.BRANCH;
                Kendra = request.KENDRA;
                MemberId = request.MBRID;
                LosAppId = request.LOSAPPID;
                CreditInquiryPurposeType = request.CREDITINQPURPSTYP;
                CreditInquiryPurposeTypeDescription = request.CREDITINQPURPSTYPDESC;
                CreditInquiryStage = request.CREDITINQUIRYSTAGE;
                CreditRequestType = request.CREDITREQTYP;
                CreditReportId = request.CREDITRPTID;
                CreditReportTransactionDatetime = request.CREDITRPTTRNDTTM;
                AccountOpenDate = request.ACOPENDT;
                LoanAmount = request.LOANAMOUNT;
                EntityId = request.ENTITYID;

            }
        }
        public string Name { get; set; }
        public string Aka { get; set; }
        public string Spouse { get; set; }
        public string Father { get; set; }
        public string Mother { get; set; }
        public string Dob { get; set; }
        public string Age { get; set; }
        public string AgeAsOn { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IId, Id>))]
        public List<IId> Ids { get; set; }
        public string Gender { get; set; }
        public string Ownership { get; set; }
        public List<string>  Addresses { get; set; }
        public List<string> Phones { get; set; }
        public List<string> Emails { get; set; }
        public string Branch { get; set; }
        public string Kendra { get; set; }
        public string MemberId { get; set; }
        public string LosAppId { get; set; }
        public string CreditInquiryPurposeType { get; set; }
        public string CreditInquiryPurposeTypeDescription { get; set; }
        public string CreditInquiryStage { get; set; }
        public string CreditReportId { get; set; }
        public string CreditRequestType { get; set; }
        public string CreditReportTransactionDatetime { get; set; }
        public string AccountOpenDate { get; set; }
        public string LoanAmount { get; set; }
        public string EntityId { get; set; }
    }
}
