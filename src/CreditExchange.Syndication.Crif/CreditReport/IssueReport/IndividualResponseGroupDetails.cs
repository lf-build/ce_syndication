﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class IndividualResponseGroupDetails : IIndividualResponseGroupDetails
    {
        public IndividualResponseGroupDetails()
        {

        }
        public IndividualResponseGroupDetails(INDVRESPONSESINDVRESPONSEGROUPDETAILS individualResponseGroupDetails)
        {
            if(individualResponseGroupDetails!=null)
            {
                GroupId = individualResponseGroupDetails.GROUPID;
                TotalAccounts = individualResponseGroupDetails.TOTACCOUNTS;
                TotalCurrentBalance = individualResponseGroupDetails.TOTCURRENTBAL;
                TotalDelinqMember = individualResponseGroupDetails.TOTDELINQMBR;
                TotalInstallmentAmount = individualResponseGroupDetails.TOTINSTALLMENTAMT;
                TotalOverdueAmount = individualResponseGroupDetails.TOTOVERDUEAMT;
                TotalDisbursedAmount = individualResponseGroupDetails.TOTDISBURSEDAMT;
                TotalMember = individualResponseGroupDetails.TOTMEMBER;
                TotalDpd30 = individualResponseGroupDetails.TOTDPD30;
                TotalDpd60 = individualResponseGroupDetails.TOTDPD60;
                TotalDpd90 = individualResponseGroupDetails.TOTDPD90;
                TotalWriteOffs = individualResponseGroupDetails.TOTWRITEOFFS;
            }
        }
        public string GroupId { get; set; }
        public string TotalDisbursedAmount { get; set; }
        public string TotalCurrentBalance { get; set; }
        public string TotalInstallmentAmount { get; set; }
        public string TotalOverdueAmount { get; set; }
        public string TotalAccounts { get; set; }
        public string TotalMember { get; set; }
        public string TotalDelinqMember { get; set; }
        public string TotalDpd30 { get; set; }
        public string TotalDpd60 { get; set; }
        public string TotalDpd90 { get; set; }
        public string TotalWriteOffs { get; set; }
    }
}
