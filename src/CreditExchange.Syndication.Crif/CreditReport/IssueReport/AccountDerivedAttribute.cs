﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class AccountDerivedAttribute : IAccountDerivedAttribute
    {
        public AccountDerivedAttribute()
        {
        }
        public AccountDerivedAttribute(ACCDERIVEDATTRIBUTES accountDerivedAttribute)
        {
            if(accountDerivedAttribute!=null)
            {
                InquiriesInLastSixMonths = accountDerivedAttribute.INQUIRIESINLASTSIXMONTHS;
                LengthOfCreditHistoryMonth = accountDerivedAttribute.LENGTHOFCREDITHISTORYMONTH;
                LengthOfCreditHistoryYear = accountDerivedAttribute.LENGTHOFCREDITHISTORYYEAR;
                AverageAccountAgeMonth = accountDerivedAttribute.AVERAGEACCOUNTAGEYEAR;
                AverageAccountAgeYear = accountDerivedAttribute.AVERAGEACCOUNTAGEYEAR;
                NewAccountsInLastSixMonths = accountDerivedAttribute.NEWACCOUNTSINLASTSIXMONTHS;
                NewDelinqAccountInLastSixMonths = accountDerivedAttribute.NEWDELINQACCOUNTINLASTSIXMONTHS;
            }
        }
        
        public string InquiriesInLastSixMonths { get; set; }
        public string LengthOfCreditHistoryYear { get; set; }
        public string LengthOfCreditHistoryMonth { get; set; }
        public string AverageAccountAgeYear { get; set; }
        public string AverageAccountAgeMonth { get; set; }
        public string NewAccountsInLastSixMonths { get; set; }
        public string NewDelinqAccountInLastSixMonths { get; set; }
    }
}
