﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IIndividualResponseLoanDetail
    {
         string AccountType { get; set; }
         string Frequency { get; set; }
         string Status { get; set; }
         string AccountNumber { get; set; }
         string DisbursedAmount { get; set; }
         string CurrentBalance { get; set; }
         string InstallmentAmount { get; set; }
         string OverdueAmount { get; set; }
         string WriteOffAmount { get; set; }
         string WriteOffDate { get; set; }
         string LoanCycleId { get; set; }
         string DisbursedDate { get; set; }
         string ClosedDate { get; set; }
         string RecentDelinqDate { get; set; }
         string CombinedPaymentHistory { get; set; }
         string InquiryCount { get; set; }
         string InfoAsOn { get; set; }
         string WorstDelequencyAmount { get; set; }
         string ActiveBorrowers { get; set; }
         string NumberOfBorrowers { get; set; }
         string Comment { get; set; }
    }
}