﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface ISecondaryMatch
    {
        List<string> Addresses { get; set; }
        string DateOfBirth { get; set; }
        List<string> Emails { get; set; }
        List<IId> Ids { get; set; }
        ILoanDetail LoanDetails { get; set; }
        string Name { get; set; }
        List<string> Phones { get; set; }
        List<IRelation> Relations { get; set; }
    }
}