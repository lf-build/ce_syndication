﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IHistory
    {
        string Amount { get; set; }
        string InquiryDate { get; set; }
        string MemberName { get; set; }
        string OwnershipType { get; set; }
        string Purpose { get; set; }
        string Remark { get; set; }
    }
}