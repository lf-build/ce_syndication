﻿namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public interface IVariation
    {
        string ReportedDate { get; set; }
        string Value { get; set; }
    }
}