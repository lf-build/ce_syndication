﻿using System.Collections.Generic;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;
using System.Linq;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class GroupResponse : IGroupResponse
    {
        public GroupResponse()
        {

        }
        public GroupResponse(GRPRESPONSESGRPRESPONSE groupResponse)
        {
            if (groupResponse != null)
            {
                MatchedType = groupResponse.MATCHEDTYPE;
                Mfi = groupResponse.MFI;
                Branch = groupResponse.BRANCH;
                Kendra = groupResponse.KENDRA;
                Name = groupResponse.NAME;
                DateOfBirth = groupResponse.DOB;
                Age = groupResponse.AGE;
                AgeAsOn = groupResponse.AGEASON;
                Relations = groupResponse.RELATIONS.Select(p => new Relation(p)).ToList<IRelation>();
                Phones = groupResponse.PHONES.ToList();
                Addresses = groupResponse.ADDRESSES.ToList();
                Ids = groupResponse.IDS.Select(p => new Id(p)).ToList<IId>();
                Emails = groupResponse.EMAILS.ToList();
                CnsmrMemberId = groupResponse.CNSMRMBRID;
                EntityId = groupResponse.ENTITYID;
                GroupCreationDate = groupResponse.GROUPCREATIONDATE;
                InsertDate = groupResponse.INSERTDATE;
                if (groupResponse.GROUPDETAILS != null)
                    GroupDetails = new GroupResponseGroupDetails(groupResponse.GROUPDETAILS);
                if (groupResponse.LOANDETAIL != null)
                    LoanDetail = new GroupResponseLoanDetail(groupResponse.LOANDETAIL);
            }
        }
        public string MatchedType { get; set; }
        public string Mfi { get; set; }
        public string MfiId { get; set; }
        public string Branch { get; set; }
        public string Kendra { get; set; }
        public string Name { get; set; }
        public string DateOfBirth { get; set; }
        public string Age { get; set; }
        public string AgeAsOn { get; set; }
        public List<IRelation> Relations { get; set; }
        public List<string> Phones { get; set; }
        public List<string> Addresses { get; set; }
        public List<IId> Ids { get; set; }
        public List<string> Emails { get; set; }
        public string CnsmrMemberId { get; set; }
        public string EntityId { get; set; }
        public string GroupCreationDate { get; set; }
        public string InsertDate { get; set; }
        public IGroupResponseGroupDetails GroupDetails { get; set; }
        public IGroupResponseLoanDetail LoanDetail { get; set; }
    }
}
