﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class ResponseInquiry : IResponseInquiry
    {
        public ResponseInquiry()
        {

        }
        public ResponseInquiry(INQUIRY inquiry)
        {
            if(inquiry!=null)
            {
                InquiryUniqueReferenceNumber = inquiry.INQUIRYUNIQUEREFNO;
                ResponseType = inquiry.RESPONSETYPE;
                RequestDatetime = inquiry.REQUESTDTTM;
                Description = inquiry.DESCRIPTION;
            }
        }
        public string InquiryUniqueReferenceNumber { get; set; }
        public string ResponseType { get; set; }
        public string RequestDatetime { get; set; }
        public string ResponseDatetime { get; set; }
        public string Description { get; set; }
    }
}
