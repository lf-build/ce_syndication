﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.IssueReport
{
    public class Summary : ISummary
    {
        public Summary()
        {

        }
        public Summary(INDVRESPONSESSUMMARY summary)
        {
            if(summary!=null)
            {
                Status = summary.STATUS;
                TotalResponses = summary.TOTALRESPONSES;
                NumberOfMfis = summary.NOOFOWNMFIS;
                NumberOfDefaultAccounts = summary.NOOFDEFAULTACCOUNTS;
                NumberOfActiveAccounts = summary.NOOFACTIVEACCOUNTS;
                NumberOfClosedAccounts = summary.NOOFCLOSEDACCOUNTS;
                NumberOfOwnMfis = summary.NOOFOWNMFIS;
                Errors = summary.ERRORS;
                TotalOtherCurrentBalance = summary.TOTALOTHERCURRENTBALANCE;
                TotalOtherDisbursedAmount = summary.TOTALOTHERDISBURSEDAMOUNT;
                TotalOtherInstallmentAmount = summary.TOTALOTHERINSTALLMENTAMOUNT;
                TotalOwnInstallmentAmount = summary.TOTALOWNINSTALLMENTAMOUNT;
                TotalOwnDisbursedAmount = summary.TOTALOWNDISBURSEDAMOUNT;
                TotalOwnCurrentBalance = summary.TOTALOWNCURRENTBALANCE;
                MaxWorstDelequency = summary.MAXWORSTDELEQUENCY;
            }
        }
        public string Status { get; set; }
        public string TotalResponses { get; set; }
        public string NumberOfMfis { get; set; }
        public string NumberOfDefaultAccounts { get; set; }
        public string NumberOfClosedAccounts { get; set; }
        public string NumberOfActiveAccounts { get; set; }
        public string NumberOfOwnMfis { get; set; }
        public string OwnMfiIndicator { get; set; }
        public string Errors { get; set; }
        public string TotalOwnDisbursedAmount { get; set; }
        public string TotalOtherDisbursedAmount { get; set; }
        public string TotalOwnCurrentBalance { get; set; }
        public string TotalOtherCurrentBalance { get; set; }
        public string TotalOwnInstallmentAmount { get; set; }
        public string TotalOtherInstallmentAmount { get; set; }
        public string MaxWorstDelequency { get; set; }
    }
}
