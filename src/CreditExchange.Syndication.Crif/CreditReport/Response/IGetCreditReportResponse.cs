﻿using CreditExchange.Syndication.Crif.CreditReport.IssueReport;

namespace CreditExchange.Syndication.Crif.CreditReport.Response
{
    public interface IGetCreditReportResponse
    {
         IResponseInquiry Inquiries { get; set; }

         IIndividualReport IndividualReponseList { get; set; }

        string ReferenceNumber { get; set; }
    }
}