﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Inquiry.Response;
using System.Linq;

namespace CreditExchange.Syndication.Crif.CreditReport.Response
{
    public class CreditReportInquiryResponse : ICreditReportInquiryResponse
    {
        public CreditReportInquiryResponse()
        {

        }
        public CreditReportInquiryResponse(AcknowledgementResponse acknowledgementResponse )
        {
            if(acknowledgementResponse?.INQUIRYSTATUS!=null)
            {
                ReportId = acknowledgementResponse.INQUIRYSTATUS.FirstOrDefault()?.REPORTID;
                InquiryReferenceNumber = acknowledgementResponse.INQUIRYSTATUS.FirstOrDefault()?.INQUIRYUNIQUEREFNO;
            }
        }
       public string ReportId { get; set; }
       public string InquiryReferenceNumber { get; set; }
       public  string ReferenceNumber { get; set; }
    }
}
