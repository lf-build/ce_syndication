﻿using LendFoundry.Foundation.Client;
using CreditExchange.Syndication.Crif.CreditReport.IssueReport;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.Crif.CreditReport.Response
{
    public class GetCreditReportResponse : IGetCreditReportResponse
    {
        public GetCreditReportResponse()
        {

        }
        public GetCreditReportResponse(ReportResponse issuereportresponse)
        {
            if(issuereportresponse!=null)
            {
                if (issuereportresponse.INQUIRYSTATUS?.INQUIRY != null)
                    Inquiries = new ResponseInquiry(issuereportresponse.INQUIRYSTATUS.INQUIRY);
                if (issuereportresponse.INDVREPORTS?.INDVREPORT != null)
                    IndividualReponseList =new IndividualReport(issuereportresponse.INDVREPORTS.INDVREPORT);
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IResponseInquiry, ResponseInquiry>))]
        public IResponseInquiry Inquiries { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IIndividualReport, IndividualReport>))]
        public IIndividualReport IndividualReponseList { get; set; }

        public string ReferenceNumber { get; set; }
    }
}
