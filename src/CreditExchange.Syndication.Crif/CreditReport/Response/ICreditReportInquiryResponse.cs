﻿namespace CreditExchange.Syndication.Crif.CreditReport.Response
{
    public interface ICreditReportInquiryResponse
    {
        string ReportId { get; set; }
        string InquiryReferenceNumber { get; set; }
        string ReferenceNumber { get; set; }
    }
}