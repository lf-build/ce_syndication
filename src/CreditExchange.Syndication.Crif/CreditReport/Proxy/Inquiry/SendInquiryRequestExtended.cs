﻿using System;
using System.Collections.Generic;
using System.Linq;

using CreditExchange.Syndication.Crif.CreditReport.Request;
using CreditExchange.Syndication.Crif.CreditReport.SendInquiry;

namespace CreditExchange.Syndication.Crif.CreditReport.Proxy.Inquiry
{
    public partial class SendInquiryRequest
    {
        public SendInquiryRequest()
        {
        }

        public SendInquiryRequest(ICrifCreditReportConfiguration crifCreditReportConfiguration, ICreditReportInquiryRequest getSendInquiryRequest)
        {
            HEADERSEGMENT = new HEADERSEGMENT(crifCreditReportConfiguration);
            INQUIRY = new INQUIRY(crifCreditReportConfiguration, getSendInquiryRequest);
        }
    }

    public partial class APPLICANTSEGMENTAPPLICANTNAME
    {
        public APPLICANTSEGMENTAPPLICANTNAME()
        {
        }

        public APPLICANTSEGMENTAPPLICANTNAME(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Name is require", nameof(name));
            NAME1 = name;
            //if (string.IsNullOrWhiteSpace(applicantName.Name1))
            //    throw new ArgumentException("Name1 is required", nameof(applicantName.Name1));
            //NAME1 = applicantName.Name1;
            //if (!string.IsNullOrWhiteSpace(applicantName.Name2))
            //    NAME2 = applicantName.Name2;
            //if (!string.IsNullOrWhiteSpace(applicantName.Name3))
            //    NAME3 = applicantName.Name3;
            //if (!string.IsNullOrWhiteSpace(applicantName.Name4))
            //    NAME4 = applicantName.Name4;
            //if (!string.IsNullOrWhiteSpace(applicantName.Name5))
            //    NAME5 = applicantName.Name3;
        }
    }

    public partial class APPLICANTSEGMENTDOB
    {
        public APPLICANTSEGMENTDOB()
        {
        }

        public APPLICANTSEGMENTDOB(DateTime applicationDob)
        {
            //if (applicationDob == null)
            //    throw new ArgumentNullException(nameof(applicationDob));
            //if ((applicationDob.Age <= 0) && (applicationDob.DateOfBith == null))
            //    throw new ArgumentException("Age or DateofBirth is require", nameof(applicationDob.Age));
            //AGE = applicationDob.Age.ToString();
            DOBDATE = applicationDob.ToString("dd-MM-yyyy");
            //if ((applicationDob.Age > 0) && (applicationDob.AgeAsOn == null))
            //    throw new ArgumentException("Age as on date Require", nameof(applicationDob.AgeAsOn));
            //AGEASON = applicationDob.AgeAsOn.ToString("dd-MM-yyyy");
        }
    }

    public partial class APPLICANTSEGMENTID
    {
        public APPLICANTSEGMENTID()
        {
        }

        public APPLICANTSEGMENTID(IIdentification applicationSegmentId)
        {
            if (applicationSegmentId == null)
                throw new ArgumentNullException(nameof(applicationSegmentId));
            TYPE = (APPLICANTSEGMENTIDTYPE)applicationSegmentId.Type;
            if (string.IsNullOrWhiteSpace(applicationSegmentId.Value))
                throw new ArgumentException(nameof(applicationSegmentId.Value));
            VALUE = applicationSegmentId.Value;
        }
    }

    public partial class APPLICANTSEGMENTKEYPERSON
    {
        public APPLICANTSEGMENTKEYPERSON()
        {
        }

        public APPLICANTSEGMENTKEYPERSON(IKeyPerson applicantSegmentKeyPerson)
        {
            if (applicantSegmentKeyPerson == null)
                throw new ArgumentNullException(nameof(applicantSegmentKeyPerson));
            if (string.IsNullOrWhiteSpace(applicantSegmentKeyPerson.Name))
                throw new ArgumentException("Name is require", nameof(applicantSegmentKeyPerson.Name));
            TYPE = (APPLICANTSEGMENTKEYPERSONTYPE)applicantSegmentKeyPerson.Type;
            NAME = applicantSegmentKeyPerson.Name;
        }
    }

    public partial class APPLICANTSEGMENTNOMINEE
    {
        public APPLICANTSEGMENTNOMINEE()
        {
        }

        public APPLICANTSEGMENTNOMINEE(IApplicantSegmentNominee applicantSegmentNominee)
        {
            if (applicantSegmentNominee == null)
                throw new ArgumentNullException(nameof(applicantSegmentNominee));
            if (string.IsNullOrWhiteSpace(applicantSegmentNominee.Name))
                throw new ArgumentException("Nominee Value is require", nameof(applicantSegmentNominee.Name));
            TYPE = (APPLICANTSEGMENTNOMINEETYPE)applicantSegmentNominee.Type;
            NAME = applicantSegmentNominee.Name;
        }
    }

    public partial class APPLICANTSEGMENTPHONE
    {
        public APPLICANTSEGMENTPHONE()
        {
        }

        public APPLICANTSEGMENTPHONE(IApplicantSegmentPhone applicantSegmentPhone)
        {
            if (applicantSegmentPhone == null)
                throw new ArgumentNullException(nameof(applicantSegmentPhone));
            if (string.IsNullOrWhiteSpace(applicantSegmentPhone.TelephoneNumber))
                throw new ArgumentException("TelephoneNumber is require", nameof(applicantSegmentPhone.TelephoneNumber));
            TELENO = Convert.ToUInt64(applicantSegmentPhone.TelephoneNumber);
            TELENOTYPE = (APPLICANTSEGMENTPHONETELENOTYPE)applicantSegmentPhone.TelephoneNumberType;
        }
    }

    public partial class APPLICANTSEGMENTRELATION
    {
        public APPLICANTSEGMENTRELATION()
        {
        }

        public APPLICANTSEGMENTRELATION(IRelation applicantSegmentRelation)
        {
            if (applicantSegmentRelation == null)
                throw new ArgumentNullException(nameof(applicantSegmentRelation));
            TYPE = (APPLICANTSEGMENTRELATIONTYPE)applicantSegmentRelation.Type;
            if (!string.IsNullOrWhiteSpace(applicantSegmentRelation.Value))
                NAME = applicantSegmentRelation.Value;
        }
    }

    public partial class APPLICANTSEGMENT
    {
        public APPLICANTSEGMENT()
        {
        }

        public APPLICANTSEGMENT(ICreditReportInquiryRequest getSendInquiryRequest)
        {
            if (getSendInquiryRequest == null)
                throw new ArgumentNullException(nameof(getSendInquiryRequest));
            if (string.IsNullOrEmpty(getSendInquiryRequest.Name))
                throw new ArgumentException("Applicant Name is require", nameof(getSendInquiryRequest.Name));
            APPLICANTNAME = new APPLICANTSEGMENTAPPLICANTNAME(getSendInquiryRequest.Name);

            DOB = new APPLICANTSEGMENTDOB(Convert.ToDateTime(getSendInquiryRequest.Dob));
            GENDER = (APPLICANTSEGMENTGENDER)getSendInquiryRequest.Gender;
           var ids = new List<APPLICANTSEGMENTID>();
            if (!string.IsNullOrEmpty(getSendInquiryRequest.Pan))
                ids.Add(new APPLICANTSEGMENTID()
                {
                    TYPE = APPLICANTSEGMENTIDTYPE.ID07,
                    VALUE = getSendInquiryRequest.Pan
                });
            if (!string.IsNullOrEmpty(getSendInquiryRequest.Passport))
                ids.Add(new APPLICANTSEGMENTID()
                {
                    TYPE = APPLICANTSEGMENTIDTYPE.ID01,
                    VALUE = getSendInquiryRequest.Passport
                });
           
            if (!string.IsNullOrEmpty(getSendInquiryRequest.VoterId))
                ids.Add(new APPLICANTSEGMENTID()
                {
                    TYPE = APPLICANTSEGMENTIDTYPE.ID02,
                    VALUE = getSendInquiryRequest.VoterId
                });
            if (!string.IsNullOrEmpty(getSendInquiryRequest.RationCard))
                ids.Add(new APPLICANTSEGMENTID()
                {
                    TYPE = APPLICANTSEGMENTIDTYPE.ID05,
                    VALUE = getSendInquiryRequest.RationCard
                });
            if (!string.IsNullOrEmpty(getSendInquiryRequest.Uid))
                ids.Add(new APPLICANTSEGMENTID()
                {
                    TYPE = APPLICANTSEGMENTIDTYPE.ID03,
                    VALUE = getSendInquiryRequest.Uid
                });
            if (!string.IsNullOrEmpty(getSendInquiryRequest.Others))
                ids.Add(new APPLICANTSEGMENTID()
                {
                    TYPE = APPLICANTSEGMENTIDTYPE.ID04,
                    VALUE = getSendInquiryRequest.Others
                });
            if (!string.IsNullOrEmpty(getSendInquiryRequest.DrivingLicNo))
                ids.Add(new APPLICANTSEGMENTID()
                {
                    TYPE = APPLICANTSEGMENTIDTYPE.ID02,
                    VALUE = getSendInquiryRequest.DrivingLicNo
                });
           
            if (ids.Count > 0)
                IDS = ids.ToArray();
            var phone =new List<APPLICANTSEGMENTPHONE>();
            if (!string.IsNullOrEmpty(getSendInquiryRequest.MobileNo))
                phone.Add(new APPLICANTSEGMENTPHONE()
                {
                    TELENO = Convert.ToUInt64(getSendInquiryRequest.MobileNo),
                    TELENOTYPE = APPLICANTSEGMENTPHONETELENOTYPE.P03
                });
            
            if (!string.IsNullOrEmpty(getSendInquiryRequest.LandlineNo))
                phone.Add(new APPLICANTSEGMENTPHONE()
                {
                    TELENO = Convert.ToUInt64(getSendInquiryRequest.LandlineNo),
                    TELENOTYPE = APPLICANTSEGMENTPHONETELENOTYPE.P01
                });

            if (phone.Count > 0)
                PHONES = phone.ToArray();
            //if (applicantSegment.Ids != null)
            //    IDS = applicantSegment.Ids.Select(p => new APPLICANTSEGMENTID(p)).ToArray();
            //if (applicantSegment.KeyPerson != null)
            //     KEYPERSON = new APPLICANTSEGMENTKEYPERSON(applicantSegment.KeyPerson);
            // if (applicantSegment.Nominee != null)
            //     NOMINEE = new APPLICANTSEGMENTNOMINEE(applicantSegment.Nominee);
            //if (applicantSegment.Phones != null)
            //    PHONES = applicantSegment.Phones.Select(p => new APPLICANTSEGMENTPHONE(p)).ToArray();
            //if (applicantSegment.Relations != null)
            //    RELATIONS = applicantSegment.Relations.Select(p => new APPLICANTSEGMENTRELATION(p)).ToArray();
        }
    }

    public partial class APPLICATIONSEGMENT
    {
        public APPLICATIONSEGMENT()
        {
        }

        public APPLICATIONSEGMENT(ICrifCreditReportConfiguration crifCreditReportConfiguration)
        {
            if (crifCreditReportConfiguration == null)
                throw new ArgumentNullException(nameof(crifCreditReportConfiguration));
            //if (string.IsNullOrWhiteSpace(applicationSegment.InquiryUniqueReferenceNumber))
            //    throw new ArgumentException("Inquiry Unique ReferenceNumber is require",
            //        nameof(applicationSegment.InquiryUniqueReferenceNumber));
            INQUIRYUNIQUEREFNO = Guid.NewGuid().ToString("N");
            //if (string.IsNullOrWhiteSpace(applicationSegment.CreditReportId))
            //    throw new ArgumentException("CreditReportId is require",
            //        nameof(applicationSegment.InquiryUniqueReferenceNumber));
            //if (string.IsNullOrWhiteSpace(reportId))
            //    throw new ArgumentException("CreditReportId is require",
            //           nameof(reportId));
            //    CREDTRPTID = reportId;


            CREDTREQTYP = (APPLICATIONSEGMENTCREDTREQTYP)Enum.Parse(typeof(APPLICATIONSEGMENTCREDTREQTYP), crifCreditReportConfiguration.RequestType);

            CREDTINQPURPSTYP = (APPLICATIONSEGMENTCREDTINQPURPSTYP)Enum.Parse(typeof(APPLICATIONSEGMENTCREDTINQPURPSTYP), crifCreditReportConfiguration.InquiryPurposeType);
            //if (string.IsNullOrWhiteSpace(applicationSegment.CreditInquiryPurposeTypeDescription) && applicationSegment.CreditInquiryPurposeType == "OTHER")
            //    throw new ArgumentException("CreditInquiry Purpose Type Description is require for Other Type",
            //        nameof(applicationSegment.CreditInquiryPurposeTypeDescription));
            CREDTINQPURPSTYPDESC = crifCreditReportConfiguration.InquiryPurposeType;
            CREDITINQUIRYSTAGE = (APPLICATIONSEGMENTCREDITINQUIRYSTAGE)Enum.Parse(typeof(APPLICATIONSEGMENTCREDITINQUIRYSTAGE), crifCreditReportConfiguration.CreditInquiryStage);
            // CREDTRPTTRNDTTM = applicationSegment.CreditReportTransactionDatetime.ToString("dd-MM-yyyy");
            //if (string.IsNullOrWhiteSpace(applicationSegment.MemberId))
            //    throw new ArgumentException("MemberId is require", nameof(applicationSegment.MemberId));
            //MBRID = applicationSegment.MemberId;
            //if (!string.IsNullOrWhiteSpace(applicationSegment.KendraId))
            //    KENDRAID = applicationSegment.KendraId;
            if (!string.IsNullOrWhiteSpace(crifCreditReportConfiguration.BranchId))
                BRANCHID = crifCreditReportConfiguration.BranchId;
            //if (string.IsNullOrWhiteSpace(applicationSegment.LosAppId))
            //    throw new ArgumentException("LosAppId is require", nameof(applicationSegment.LosAppId));
            //LOSAPPID = applicationSegment.LosAppId;
            LOANAMOUNT = string.IsNullOrEmpty(crifCreditReportConfiguration?.LoanAmount) ? "0" : crifCreditReportConfiguration.LoanAmount;
        }
    }

    public partial class ADDRESSSEGMENT
    {
        public ADDRESSSEGMENT()
        {
        }

        public ADDRESSSEGMENT(List<IAddressSegment> addressSegments)
        {
            if (addressSegments == null)
                throw new ArgumentNullException(nameof(addressSegments));
            ADDRESS = addressSegments.Select(p => new ADDRESSSEGMENTADDRESS(p)).ToArray();
        }
    }

    public partial class ADDRESSSEGMENTADDRESS
    {
        public ADDRESSSEGMENTADDRESS()
        {
        }

        public ADDRESSSEGMENTADDRESS(IAddressSegment addressSegment)
        {
            if (addressSegment != null)
            {

                TYPE = (ADDRESSSEGMENTADDRESSTYPE)addressSegment.Type;
                if (string.IsNullOrWhiteSpace(addressSegment.Address))
                    throw new ArgumentException("Address is require", nameof(addressSegment.Address));
                ADDRESS1 = addressSegment.Address;
                if (!string.IsNullOrWhiteSpace(addressSegment.City))
                    CITY = addressSegment.City;
                STATE = (ADDRESSSEGMENTADDRESSSTATE)addressSegment.State;
                if (!string.IsNullOrWhiteSpace(addressSegment.Pin))
                    PIN = Convert.ToUInt32(addressSegment.Pin);
            }
        }
    }

    public partial class INQUIRY
    {
        public INQUIRY()
        {
        }

        public INQUIRY(ICrifCreditReportConfiguration crifCreditReportConfiguration, ICreditReportInquiryRequest getSendInquiryRequest)
        {
            if (getSendInquiryRequest == null)
                throw new ArgumentNullException(nameof(getSendInquiryRequest));

            APPLICANTSEGMENT = new APPLICANTSEGMENT(getSendInquiryRequest);
            APPLICATIONSEGMENT = new APPLICATIONSEGMENT(crifCreditReportConfiguration);
            ADDRESSSEGMENT = getSendInquiryRequest.Addresses.Select(p => new ADDRESSSEGMENTADDRESS(p)).ToArray();
        }
    }

    public partial class HEADERSEGMENTMFI
    {
        public HEADERSEGMENTMFI()
        {

        }
        public HEADERSEGMENTMFI(ICrifCreditReportConfiguration creditReportConfiguration)
        {
            if (creditReportConfiguration != null)
            {
                INDV = creditReportConfiguration.MfiIndividual;
                INDVSpecified = true;
                SCORE = creditReportConfiguration.MfiScore;
                SCORESpecified = true;
                GROUP = creditReportConfiguration.MfiGroup;
                GROUPSpecified = true;
            }
        }
    }

    public partial class HEADERSEGMENTCONSUMER
    {
        public HEADERSEGMENTCONSUMER()
        {
        }

        public HEADERSEGMENTCONSUMER(ICrifCreditReportConfiguration creditReportConfiguration)
        {
            if (creditReportConfiguration != null)
            {
                SCORE = creditReportConfiguration.ConsumerScore;
                SCORESpecified = true;
                INDV = creditReportConfiguration.ConsumerIndividual;
                INDVSpecified = true;
            }
        }
    }

    public partial class HEADERSEGMENT
    {
        public HEADERSEGMENT()
        {
        }

        public HEADERSEGMENT(ICrifCreditReportConfiguration crifCreditReportConfiguration)
        {
            if (crifCreditReportConfiguration == null)
                throw new ArgumentNullException(nameof(crifCreditReportConfiguration));
            if (string.IsNullOrWhiteSpace(crifCreditReportConfiguration.ProductType))
                throw new ArgumentException("ProductType is required", nameof(crifCreditReportConfiguration.ProductType));
            if (string.IsNullOrWhiteSpace(crifCreditReportConfiguration.ProductVersion))
                throw new ArgumentException("ProductVersion is required",
                    nameof(crifCreditReportConfiguration.ProductVersion));
            if (string.IsNullOrWhiteSpace(crifCreditReportConfiguration.MemberId))
                throw new ArgumentException("MemberId is required", nameof(crifCreditReportConfiguration.MemberId));
            if (string.IsNullOrWhiteSpace(crifCreditReportConfiguration.SubMemberId))
                throw new ArgumentException("SubMemberId is required", nameof(crifCreditReportConfiguration.SubMemberId));
            if (string.IsNullOrWhiteSpace(crifCreditReportConfiguration.UserId))
                throw new ArgumentException("UserId is required", nameof(crifCreditReportConfiguration.UserId));
            if (string.IsNullOrWhiteSpace(crifCreditReportConfiguration.Password))
                throw new ArgumentException("Password is required", nameof(crifCreditReportConfiguration.Password));
            if (string.IsNullOrWhiteSpace(crifCreditReportConfiguration.RequestVolumeType))
                throw new ArgumentException("RequestVolumeType is required",
                    nameof(crifCreditReportConfiguration.RequestVolumeType));
            if (string.IsNullOrWhiteSpace(crifCreditReportConfiguration.ApiUrl))
                throw new ArgumentException("ApiUrl  is required", nameof(crifCreditReportConfiguration.ApiUrl));

            if (!string.IsNullOrWhiteSpace(crifCreditReportConfiguration.ProductType))
                PRODUCTTYP =
                    (HEADERSEGMENTPRODUCTTYP)
                    Enum.Parse(typeof(HEADERSEGMENTPRODUCTTYP), crifCreditReportConfiguration.ProductType);
            if (!string.IsNullOrWhiteSpace(crifCreditReportConfiguration.ProductVersion))
                PRODUCTVER = (HEADERSEGMENTPRODUCTVER)Convert.ToInt32(crifCreditReportConfiguration.ProductVersion.Substring(0, 1));
            if (!string.IsNullOrWhiteSpace(crifCreditReportConfiguration.MemberId))
                REQMBR = crifCreditReportConfiguration.MemberId;
            if (!string.IsNullOrWhiteSpace(crifCreditReportConfiguration.SubMemberId))
                SUBMBRID = crifCreditReportConfiguration.SubMemberId;
            //INQDTTM = headerSegment.InquiryDatetime.ToString("dd-MM-yyyy hh:mm:ss");
            REQVOLTYP = (HEADERSEGMENTREQVOLTYP)Enum.Parse(typeof(HEADERSEGMENTREQVOLTYP), crifCreditReportConfiguration.RequestVolumeType);
            REQACTNTYP = (HEADERSEGMENTREQACTNTYP)Enum.Parse(typeof(HEADERSEGMENTREQACTNTYP), crifCreditReportConfiguration.ActionType);
            TESTFLG = crifCreditReportConfiguration.TestFlag;
            if (!string.IsNullOrWhiteSpace(crifCreditReportConfiguration.AuthFlag))
                AUTHFLG = crifCreditReportConfiguration.AuthFlag;
            if (!string.IsNullOrWhiteSpace(crifCreditReportConfiguration.AuthTitle))
                AUTHTITLE = crifCreditReportConfiguration.AuthTitle;
            RESFRMT = crifCreditReportConfiguration.ResponseFormat.ToString();
            if (!string.IsNullOrWhiteSpace(crifCreditReportConfiguration.MemberPreOverride))
                MEMBERPREOVERRIDE = crifCreditReportConfiguration.MemberPreOverride;
            RESFRMTEMBD = crifCreditReportConfiguration.IsEmbededResponse.ToString();
            if (!string.IsNullOrWhiteSpace(crifCreditReportConfiguration.LosName))
                LOSNAME = crifCreditReportConfiguration.LosName;
            if (!string.IsNullOrWhiteSpace(crifCreditReportConfiguration.LosVendor))
                LOSVENDER = crifCreditReportConfiguration.LosVendor;
            if (!string.IsNullOrWhiteSpace(crifCreditReportConfiguration.LosVersion))
                LOSVERSION = crifCreditReportConfiguration.LosVersion;
            MFI = new HEADERSEGMENTMFI(crifCreditReportConfiguration);
            CONSUMER = new HEADERSEGMENTCONSUMER(crifCreditReportConfiguration);
            IOI = crifCreditReportConfiguration.Ioi;
        }
    }
}