﻿/*------------------------------------------------------------------------------
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
------------------------------------------------------------------------------*/

namespace CreditExchange.Syndication.Crif.CreditReport.Proxy.Inquiry.Response
{
    ///
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34283")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute("REPORT-FILE", Namespace = "", IsNullable = false)]
    public partial class AcknowledgementResponse
    {
        private INQUIRY[] iNQUIRYSTATUSField;

        ///
        [System.Xml.Serialization.XmlArrayAttribute("INQUIRY-STATUS")]
        [System.Xml.Serialization.XmlArrayItemAttribute("INQUIRY", IsNullable = false)]
        public INQUIRY[] INQUIRYSTATUS
        {
            get
            {
                return this.iNQUIRYSTATUSField;
            }
            set
            {
                this.iNQUIRYSTATUSField = value;
            }
        }
    }

    ///
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34283")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class INQUIRY
    {
        private string iNQUIRYUNIQUEREFNOField;
        private string mBRIDField;
        private string rEQUESTDTTMField;
        private string rEPORTIDField;
        private string rESPONSEDTTMField;
        private INQUIRYREPONSETYPE rEPONSETYPEField;
        private ERROR[] eRRORSField;

        ///
        [System.Xml.Serialization.XmlElementAttribute("INQUIRY-UNIQUE-REF-NO")]
        public string INQUIRYUNIQUEREFNO
        {
            get
            {
                return this.iNQUIRYUNIQUEREFNOField;
            }
            set
            {
                this.iNQUIRYUNIQUEREFNOField = value;
            }
        }

        ///
        [System.Xml.Serialization.XmlElementAttribute("MBR-ID")]
        public string MBRID
        {
            get
            {
                return this.mBRIDField;
            }
            set
            {
                this.mBRIDField = value;
            }
        }

        ///
        [System.Xml.Serialization.XmlElementAttribute("REQUEST-DT-TM")]
        public string REQUESTDTTM
        {
            get
            {
                return this.rEQUESTDTTMField;
            }
            set
            {
                this.rEQUESTDTTMField = value;
            }
        }

        ///
        [System.Xml.Serialization.XmlElementAttribute("REPORT-ID")]
        public string REPORTID
        {
            get
            {
                return this.rEPORTIDField;
            }
            set
            {
                this.rEPORTIDField = value;
            }
        }

        ///
        [System.Xml.Serialization.XmlElementAttribute("RESPONSE-DT-TM")]
        public string RESPONSEDTTM
        {
            get
            {
                return this.rESPONSEDTTMField;
            }
            set
            {
                this.rESPONSEDTTMField = value;
            }
        }

        ///
        [System.Xml.Serialization.XmlElementAttribute("REPONSE-TYPE")]
        public INQUIRYREPONSETYPE REPONSETYPE
        {
            get
            {
                return this.rEPONSETYPEField;
            }
            set
            {
                this.rEPONSETYPEField = value;
            }
        }

        ///
        [System.Xml.Serialization.XmlArrayItemAttribute("ERROR", IsNullable = false)]
        public ERROR[] ERRORS
        {
            get
            {
                return this.eRRORSField;
            }
            set
            {
                this.eRRORSField = value;
            }
        }
    }

    ///
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34283")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum INQUIRYREPONSETYPE
    {
        ///
        ACKNOWLEDGEMENT,

        ///
        ERROR,
    }

    ///
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34283")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class ERROR
    {
        private string cODEField;
        private string dESCRIPTIONField;

        ///
        public string CODE
        {
            get
            {
                return this.cODEField;
            }
            set
            {
                this.cODEField = value;
            }
        }

        ///
        public string DESCRIPTION
        {
            get
            {
                return this.dESCRIPTIONField;
            }
            set
            {
                this.dESCRIPTIONField = value;
            }
        }
    }
}