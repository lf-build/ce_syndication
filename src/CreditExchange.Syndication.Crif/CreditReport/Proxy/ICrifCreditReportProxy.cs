﻿
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Inquiry;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Inquiry.Response;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue;

using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.Proxy
{
    public interface ICrifCreditReportProxy
    {
        AcknowledgementResponse SendInquiry(SendInquiryRequest inquiryRequest);
        ReportResponse IssueReport(ReportRequest reportRequest);
       
    }
}