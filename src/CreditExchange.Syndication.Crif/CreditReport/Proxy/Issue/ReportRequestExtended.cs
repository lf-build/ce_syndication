﻿using System;
using CreditExchange.Syndication.Crif.CreditReport.IssueReport;
using CreditExchange.Syndication.Crif.CreditReport.Request;
using System.Linq;

namespace CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue
{
    public partial class ReportRequest
    {
        public ReportRequest()
        {

        }
        public ReportRequest(ICrifCreditReportConfiguration creditInformationHighMarkConfiguration, string reportId, string inquiryReferenceNumber)
        {
            if (creditInformationHighMarkConfiguration == null)
                throw new ArgumentNullException(nameof(creditInformationHighMarkConfiguration));
            if (string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.ReportProductType))
                throw new ArgumentException("ProductType is required", nameof(creditInformationHighMarkConfiguration.ReportProductType));
            if (string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.ProductVersion))
                throw new ArgumentException("ProductVersion is required", nameof(creditInformationHighMarkConfiguration.ProductVersion));
            if (string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.MemberId))
                throw new ArgumentException("MemberId is required", nameof(creditInformationHighMarkConfiguration.MemberId));
            if (string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.SubMemberId))
                throw new ArgumentException("SubMemberId is required", nameof(creditInformationHighMarkConfiguration.SubMemberId));
            if (string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.UserId))
                throw new ArgumentException("UserId is required", nameof(creditInformationHighMarkConfiguration.UserId));
            if (string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.Password))
                throw new ArgumentException("Password is required", nameof(creditInformationHighMarkConfiguration.Password));
            if (string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.RequestVolumeType))
                throw new ArgumentException("RequestVolumeType is required", nameof(creditInformationHighMarkConfiguration.RequestVolumeType));
            if (string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.ApiUrl))
                throw new ArgumentException("ApiUrl  is required", nameof(creditInformationHighMarkConfiguration.ApiUrl));
            if (string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.SubMemberId))
                throw new ArgumentException("Sub-MemberId  is required ", nameof(creditInformationHighMarkConfiguration.SubMemberId));

            HEADERSEGMENT = new HEADERSEGMENT(creditInformationHighMarkConfiguration);
            INQUIRY = new[]
            {
                new INQUIRY(creditInformationHighMarkConfiguration,reportId, inquiryReferenceNumber), 
            };

        }

    }

    public partial class HEADERSEGMENT
    {
        public HEADERSEGMENT()
        {

        }
        public HEADERSEGMENT(ICrifCreditReportConfiguration creditInformationHighMarkConfiguration)
        {
            if (creditInformationHighMarkConfiguration == null)
                throw new ArgumentNullException(nameof(creditInformationHighMarkConfiguration));
            if (!string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.ReportProductType))
                PRODUCTTYP = (HEADERSEGMENTPRODUCTTYP)Enum.Parse(typeof(HEADERSEGMENTPRODUCTTYP), creditInformationHighMarkConfiguration.ReportProductType);
            if (!string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.ProductVersion))
                PRODUCTVER = creditInformationHighMarkConfiguration.ProductVersion;
            if (!string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.MemberId))
                REQMBR = creditInformationHighMarkConfiguration.MemberId;
            if (!string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.SubMemberId))
                SUBMBRID = creditInformationHighMarkConfiguration.SubMemberId;
            //if (headerSegment.InquiryDatetime != null)
            //    INQDTTM = headerSegment.InquiryDatetime.ToString("dd-MM-yyyy hh:mm:ss");
            if (creditInformationHighMarkConfiguration.RequestVolumeType != null)
                REQVOLTYP = (HEADERSEGMENTREQVOLTYP)Enum.Parse(typeof(HEADERSEGMENTREQVOLTYP), creditInformationHighMarkConfiguration.RequestVolumeType);
            if (creditInformationHighMarkConfiguration.ActionType != null)
                REQACTNTYP = (HEADERSEGMENTREQACTNTYP)Enum.Parse(typeof(HEADERSEGMENTREQACTNTYP), creditInformationHighMarkConfiguration.ReportActionType);
            if (creditInformationHighMarkConfiguration.TestFlag != null)
                TESTFLG = creditInformationHighMarkConfiguration.TestFlag;
            if (!string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.AuthFlag))
                AUTHFLG = creditInformationHighMarkConfiguration.AuthFlag;
            if (!string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.AuthTitle))
                AUTHTITLE = creditInformationHighMarkConfiguration.AuthTitle;
           // if (!string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.ResponseFormat))
                RESFRMT = creditInformationHighMarkConfiguration.ResponseFormat.ToString();
            if (!string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.MemberPreOverride))
                MEMBERPREOVERRIDE = creditInformationHighMarkConfiguration.MemberPreOverride;
         //   if (!string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.ResponseFormatEmbedded))
                RESFRMTEMBD = creditInformationHighMarkConfiguration.IsEmbededResponse.ToString();
            if (!string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.LosName))
                LOSNAME = creditInformationHighMarkConfiguration.LosName;
            if (!string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.LosVendor))
                LOSVENDER = creditInformationHighMarkConfiguration.LosVendor;
            if (!string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.LosVersion))
                LOSVERSION = creditInformationHighMarkConfiguration.LosVersion;
            if (string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.UserId))
                throw new ArgumentException("UserId is require", nameof(creditInformationHighMarkConfiguration.UserId));
            USERID = creditInformationHighMarkConfiguration.UserId;
            if (string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.Password))
                throw new ArgumentException("Password is require", nameof(creditInformationHighMarkConfiguration.Password));
            PWD = creditInformationHighMarkConfiguration.Password;

        }
    }

    public partial class INQUIRY
    {
        public INQUIRY()
        {

        }
        public INQUIRY(IRequestInquiry inquiry)
        {
            if (inquiry == null)
                throw new ArgumentNullException(nameof(inquiry));
            if (string.IsNullOrWhiteSpace(inquiry.InquiryUniqueReferenceNumber))
                throw new ArgumentException("Inquiry Unique ReferenceNumber is Require", nameof(inquiry.InquiryUniqueReferenceNumber));
            if (inquiry.RequestDatetime == null)
                throw new ArgumentException("Request Datetime is require", nameof(inquiry.RequestDatetime));
            if (string.IsNullOrWhiteSpace(inquiry.ReportId))
                throw new ArgumentException("ReprotId is require", nameof(inquiry.ReportId));
            INQUIRYUNIQUEREFNO = inquiry.InquiryUniqueReferenceNumber;
            REQUESTDTTM = inquiry.RequestDatetime.ToString("dd-MM-yyyy HH:MM:ss");
            REPORTID = inquiry.ReportId;

        }
        public INQUIRY(ICrifCreditReportConfiguration creditInformationHighMarkConfiguration, string reportId, string inquiryReferenceNumber)
        {
            if (creditInformationHighMarkConfiguration == null)
                throw new ArgumentNullException(nameof(creditInformationHighMarkConfiguration));
            if (string.IsNullOrWhiteSpace(creditInformationHighMarkConfiguration.InquiryUniqueReferenceNumber))
                throw new ArgumentException("Inquiry Unique ReferenceNumber is Require", nameof(creditInformationHighMarkConfiguration.InquiryUniqueReferenceNumber));

            if (string.IsNullOrWhiteSpace(reportId))
                throw new ArgumentException("ReprotId is require", nameof(reportId));
            INQUIRYUNIQUEREFNO = inquiryReferenceNumber;
           // REQUESTDTTM = inquiry.RequestDatetime.ToString("dd-MM-yyyy HH:MM:ss");
            REPORTID = reportId;

        }


    }
}
