﻿/*------------------------------------------------------------------------------
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
------------------------------------------------------------------------------*/

namespace CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue
{

    /// 
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34283")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute("REQUEST-REQUEST-FILE", Namespace = "", IsNullable = false)]
    public partial class ReportRequest
    {
        private HEADERSEGMENT hEADERSEGMENTField;
        private INQUIRY[] iNQUIRYField;
        /// 
        [System.Xml.Serialization.XmlElementAttribute("HEADER-SEGMENT")]
        public HEADERSEGMENT HEADERSEGMENT
        {
            get
            {
                return this.hEADERSEGMENTField;
            }
            set
            {
                this.hEADERSEGMENTField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("INQUIRY")]
        public INQUIRY[] INQUIRY
        {
            get
            {
                return this.iNQUIRYField;
            }
            set
            {
                this.iNQUIRYField = value;
            }
        }
    }
    /// 
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34283")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute("HEADER-SEGMENT", Namespace = "", IsNullable = false)]
    public partial class HEADERSEGMENT
    {
        private HEADERSEGMENTPRODUCTTYP pRODUCTTYPField;
        private string pRODUCTVERField;
        private string rEQMBRField;
        private string sUBMBRIDField;
        private string iNQDTTMField;
        private HEADERSEGMENTREQVOLTYP rEQVOLTYPField;
        private HEADERSEGMENTREQACTNTYP rEQACTNTYPField;
        private string tESTFLGField;
        private string uSERIDField;
        private string pWDField;
        private string aUTHFLGField;
        private string aUTHTITLEField;
        private string rESFRMTField;
        private string mEMBERPREOVERRIDEField;
        private string rESFRMTEMBDField;
        private string lOSNAMEField;
        private string lOSVENDERField;
        private string lOSVERSIONField;
        /// 
        [System.Xml.Serialization.XmlElementAttribute("PRODUCT-TYP")]
        public HEADERSEGMENTPRODUCTTYP PRODUCTTYP
        {
            get
            {
                return this.pRODUCTTYPField;
            }
            set
            {
                this.pRODUCTTYPField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("PRODUCT-VER")]
        public string PRODUCTVER
        {
            get
            {
                return this.pRODUCTVERField;
            }
            set
            {
                this.pRODUCTVERField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("REQ-MBR")]
        public string REQMBR
        {
            get
            {
                return this.rEQMBRField;
            }
            set
            {
                this.rEQMBRField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("SUB-MBR-ID")]
        public string SUBMBRID
        {
            get
            {
                return this.sUBMBRIDField;
            }
            set
            {
                this.sUBMBRIDField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("INQ-DT-TM")]
        public string INQDTTM
        {
            get
            {
                return this.iNQDTTMField;
            }
            set
            {
                this.iNQDTTMField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("REQ-VOL-TYP")]
        public HEADERSEGMENTREQVOLTYP REQVOLTYP
        {
            get
            {
                return this.rEQVOLTYPField;
            }
            set
            {
                this.rEQVOLTYPField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("REQ-ACTN-TYP")]
        public HEADERSEGMENTREQACTNTYP REQACTNTYP
        {
            get
            {
                return this.rEQACTNTYPField;
            }
            set
            {
                this.rEQACTNTYPField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("TEST-FLG")]
        public string TESTFLG
        {
            get
            {
                return this.tESTFLGField;
            }
            set
            {
                this.tESTFLGField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("USER-ID")]
        public string USERID
        {
            get
            {
                return this.uSERIDField;
            }
            set
            {
                this.uSERIDField = value;
            }
        }
        /// 
        public string PWD
        {
            get
            {
                return this.pWDField;
            }
            set
            {
                this.pWDField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("AUTH-FLG")]
        public string AUTHFLG
        {
            get
            {
                return this.aUTHFLGField;
            }
            set
            {
                this.aUTHFLGField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("AUTH-TITLE")]
        public string AUTHTITLE
        {
            get
            {
                return this.aUTHTITLEField;
            }
            set
            {
                this.aUTHTITLEField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("RES-FRMT")]
        public string RESFRMT
        {
            get
            {
                return this.rESFRMTField;
            }
            set
            {
                this.rESFRMTField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("MEMBER-PRE-OVERRIDE")]
        public string MEMBERPREOVERRIDE
        {
            get
            {
                return this.mEMBERPREOVERRIDEField;
            }
            set
            {
                this.mEMBERPREOVERRIDEField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("RES-FRMT-EMBD")]
        public string RESFRMTEMBD
        {
            get
            {
                return this.rESFRMTEMBDField;
            }
            set
            {
                this.rESFRMTEMBDField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("LOS-NAME")]
        public string LOSNAME
        {
            get
            {
                return this.lOSNAMEField;
            }
            set
            {
                this.lOSNAMEField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("LOS-VENDER")]
        public string LOSVENDER
        {
            get
            {
                return this.lOSVENDERField;
            }
            set
            {
                this.lOSVENDERField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("LOS-VERSION")]
        public string LOSVERSION
        {
            get
            {
                return this.lOSVERSIONField;
            }
            set
            {
                this.lOSVERSIONField = value;
            }
        }
    }
    /// 
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34283")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum HEADERSEGMENTPRODUCTTYP
    {
        /// 
        BASE_PLUS_REPORT,
    }
    /// 
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34283")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum HEADERSEGMENTREQVOLTYP
    {
        /// 
        INDV,
        /// 
        JOIN,
        /// 
        C01,
        /// 
        C03,
    }
    /// 
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34283")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum HEADERSEGMENTREQACTNTYP
    {
        /// 
        ISSUE,
        /// 
        AT02,
    }
    /// 
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34283")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class INQUIRY
    {
        private string iNQUIRYUNIQUEREFNOField;
        private string rEQUESTDTTMField;
        private string rEPORTIDField;
        /// 
        [System.Xml.Serialization.XmlElementAttribute("INQUIRY-UNIQUE-REF-NO")]
        public string INQUIRYUNIQUEREFNO
        {
            get
            {
                return this.iNQUIRYUNIQUEREFNOField;
            }
            set
            {
                this.iNQUIRYUNIQUEREFNOField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("REQUEST-DT-TM")]
        public string REQUESTDTTM
        {
            get
            {
                return this.rEQUESTDTTMField;
            }
            set
            {
                this.rEQUESTDTTMField = value;
            }
        }
        /// 
        [System.Xml.Serialization.XmlElementAttribute("REPORT-ID")]
        public string REPORTID
        {
            get
            {
                return this.rEPORTIDField;
            }
            set
            {
                this.rEPORTIDField = value;
            }
        }
    }
}