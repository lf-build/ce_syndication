﻿using System;
using System.Net;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Inquiry;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Inquiry.Response;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue.Response;
using RestSharp;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.Crif.CreditReport.Proxy
{
    public class CrifCreditReportProxy : ICrifCreditReportProxy
    {
        public CrifCreditReportProxy(ICrifCreditReportConfiguration configuration, ITenantTime tenantTime,ILogger logger)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (string.IsNullOrWhiteSpace(configuration.UserId))
                throw new ArgumentException(nameof(configuration.UserId));
            if (string.IsNullOrWhiteSpace(configuration.Password))
                throw new ArgumentException(nameof(configuration.Password));
            if (string.IsNullOrWhiteSpace(configuration.ApiUrl))
                throw new ArgumentException(nameof(configuration.ApiUrl));
            if (string.IsNullOrWhiteSpace(configuration.MemberId))
                throw new ArgumentException(nameof(configuration.MemberId));
            if (string.IsNullOrWhiteSpace(configuration.ProductType))
                throw new ArgumentException(nameof(configuration.ProductType));
            if (string.IsNullOrWhiteSpace(configuration.ProductVersion))
                throw new ArgumentException(nameof(configuration.ProductVersion));
            if (string.IsNullOrWhiteSpace(configuration.RequestVolumeType))
                throw new ArgumentException(nameof(configuration.RequestVolumeType));
            Configuration = configuration;
            TenantTime = tenantTime;
            Logger = logger;
        }
        private ILogger Logger { get; set; }
        private  ITenantTime TenantTime { get; set; }
        private ICrifCreditReportConfiguration Configuration { get; }

        public AcknowledgementResponse SendInquiry(SendInquiryRequest inquiryRequest)
        {
            Logger.Info($"[CRIF] SendInquiry method invoked for data  { (inquiryRequest != null ? JsonConvert.SerializeObject(inquiryRequest) : null)}");
            var client = new RestClient(Configuration.ApiUrl);
            var request = new RestRequest(Method.POST);
            inquiryRequest.HEADERSEGMENT.INQDTTM = TenantTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
            var requestxml = XmlSerialization.Serialize(inquiryRequest);

            ServicePointManager.ServerCertificateValidationCallback +=
                (sender, certificate, chain, sslPolicyErrors) => true;
            request.AddHeader("userId", Configuration.UserId);
            request.AddHeader("password", Configuration.Password);
            request.AddHeader("mbrid", Configuration.MemberId);
            request.AddHeader("productType", Configuration.ProductType);
            request.AddHeader("productVersion", Configuration.ProductVersion);
            request.AddHeader("reqVolType", Configuration.RequestVolumeType);
            request.AddHeader("requestXML", requestxml);

            var response = client.Execute(request);

            if (response.ErrorException != null)
            {
                Logger.Error($"[CRIF] Error occured when SendInquiry method was called. Error:", response.ErrorException);
                throw new CrifException("Send Inquiry failed", response.ErrorException);
            }

            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK)
            {
                Logger.Error($"[CRIF] Error occured when SendInquiry method was called. Error:", response.Content);
                throw new CrifException(
                    $"Inquiry failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");
            }

            return XmlSerialization.Deserialize<AcknowledgementResponse>(response.Content);
        }

        public ReportResponse IssueReport(ReportRequest reportRequest)
        {
            Logger.Info($"[CRIF] IssueReport method invoked for data  { (reportRequest != null ? JsonConvert.SerializeObject(reportRequest) : null)}");
            var client = new RestClient(Configuration.ApiUrl);
            var request = new RestRequest(Method.POST);
            var requestxml = XmlSerialization.Serialize(reportRequest);
           
            request.AddHeader("userId", Configuration.UserId);
            request.AddHeader("password", Configuration.Password);
            request.AddHeader("mbrid", Configuration.MemberId);
            request.AddHeader("productType", Configuration.ProductType);
            request.AddHeader("productVersion", Configuration.ProductVersion);
            request.AddHeader("reqVolType", Configuration.RequestVolumeType);
            request.AddHeader("requestXML", requestxml);
            //request.AddHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTExLTE0VDAwOjUxOjE2LjI0MTczNTZaIiwiZXhwIjoiMjAxNi0xMS0xNFQwMDo1MDo1Ni43Njc0NzY0WiIsInN1YiI6ImpvaG5zbWl0aCIsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpbImFtb3J0aXphdGlvbi9zY2hlZHVsZSIsImxvYW5zL2RvY3VtZW50cy91cGxvYWQiXSwiSXNWYWxpZCI6ZmFsc2V9.F_0aDeBgLpNGBiD5IVVAgEN07swGpnbhg8-yTWFzqX0");
            //request.AddHeader("Content-Type", "application/xml");
            ServicePointManager.ServerCertificateValidationCallback +=
                (sender, certificate, chain, sslPolicyErrors) => true;
            var response = client.Execute(request);

            if (response.ErrorException != null)
            {
                Logger.Error($"[CRIF] Error occured when IssueReport method was called. Error:", response.ErrorException);
                throw new CrifException("IssueReport Request failed", response.ErrorException);
            }

            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK)
            {
                Logger.Error($"[CRIF] Error occured when IssueReport method was called. Error:", response.Content);
                throw new CrifException(
                    $"IssueReport Request failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");
            }

            return XmlSerialization.Deserialize<ReportResponse>(response.Content);
        }

      
    }
}