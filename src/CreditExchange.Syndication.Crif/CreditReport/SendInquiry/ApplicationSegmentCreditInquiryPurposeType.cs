namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public enum ApplicationSegmentCreditInquiryPurposeType
    {
        AccountOriginal,

        AccountMaintenance,

        Other
    }
}