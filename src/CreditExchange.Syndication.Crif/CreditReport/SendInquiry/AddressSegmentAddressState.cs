namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public enum AddressSegmentAddressState
    {
        AndhraPradesh,

        ArunachalPradesh,

        Assam,

        Bihar,

        Chattisgarh,

        Goa,

        Gujarat,

        Haryana,

        HimachalPradesh,

        JammuAndKashmir,

        Jharkhand,

        Karnataka,

        Kerala,

        MadhyaPradesh,

        Maharashtra,

        Manipur,

        Meghalaya,

        Mizoram,

        Nagaland,

        Orissa,

        Punjab,

        Rajasthan,

        Sikkim,

        TamilNadu,

        Tripura,

        Uttarakhand,

        UttarPradesh,

        WestBengal,

        AndamanAndNicobar,

        Chandigarh,

        DadarAndNagarHaveli,

        DamanAndDiu,

        Delhi,

        Lakshadweep,

        Pondicherry
    }
}