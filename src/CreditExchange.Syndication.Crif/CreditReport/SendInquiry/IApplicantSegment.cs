﻿using System.Collections.Generic;


namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public interface IApplicantSegment
    {
        IApplicantName ApplicantName { get; set; }
        IApplicantSegmentDob DateOfBirth { get; set; }
        Gender Gender { get; set; }
        bool GenderSpecified { get; set; }
        List<IIdentification> Ids { get; set; }
        IKeyPerson KeyPerson { get; set; }
        IApplicantSegmentNominee Nominee { get; set; }
        List<IApplicantSegmentPhone> Phones { get; set; }
        List<IRelation> Relations { get; set; }
    }
}