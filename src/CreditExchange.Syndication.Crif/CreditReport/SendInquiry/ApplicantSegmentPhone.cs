namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public class ApplicantSegmentPhone : IApplicantSegmentPhone
    {
        public string TelephoneNumber { get; set; }
        public ApplicantSegmentPhoneTelephoneNumberType TelephoneNumberType { get; set; }
    }
}