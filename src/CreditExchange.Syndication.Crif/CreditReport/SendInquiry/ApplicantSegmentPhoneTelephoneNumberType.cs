namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public enum ApplicantSegmentPhoneTelephoneNumberType
    {
        Residence,

        Company,

        Mobile,

        Permanent,

        Foreign,
        P06,

        Other,

        Untagged
    }
}