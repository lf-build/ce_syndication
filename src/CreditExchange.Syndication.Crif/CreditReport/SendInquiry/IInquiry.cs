﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public interface IInquiry
    {
        List<IError> Errors { get; set; }
        string InquiryUniqueReferenceNumber { get; set; }
        string MemberId { get; set; }
        InquiryResponseType ResponseType { get; set; }
        string ReportId { get; set; }
        string RequestDatetime { get; set; }
        string ResponseDatetime { get; set; }
    }
}