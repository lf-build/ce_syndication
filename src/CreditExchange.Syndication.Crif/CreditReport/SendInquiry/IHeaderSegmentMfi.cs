﻿namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public interface IHeaderSegmentMfi
    {
        bool Group { get; set; }
        bool Individual { get; set; }
        bool Score { get; set; }
    
    }
}