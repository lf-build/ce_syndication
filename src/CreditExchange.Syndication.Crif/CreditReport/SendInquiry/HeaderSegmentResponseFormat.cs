namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public enum HeaderSegmentResponseFormat
    {
        Xml,

        XmlHtml,

        XmlPdf
    }
}