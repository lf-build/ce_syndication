namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public enum ApplicationSegmentCreditRequestType
    {
        Individual,

        Joint
    }
}