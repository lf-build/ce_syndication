namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public class HeaderSegmentConsumer : IHeaderSegmentConsumer
    {
        public bool Individual { get; set; }
        public bool Score { get; set; }
        
    }
}