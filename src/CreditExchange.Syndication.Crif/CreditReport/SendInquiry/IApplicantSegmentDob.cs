﻿using System;

namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public interface IApplicantSegmentDob
    {
        int Age { get; set; }
        DateTime AgeAsOn { get; set; }
        DateTime DateOfBith { get; set; }
    }
}