namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public enum HeaderSegmentYesNo
    {
        Y,

        N
    }
}