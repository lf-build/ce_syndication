﻿namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public interface IApplicantSegmentNominee
    {
        string Name { get; set; }
        ApplicantSegmentNomineeType Type { get; set; }
    }
}