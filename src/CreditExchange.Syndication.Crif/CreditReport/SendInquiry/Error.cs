﻿using CreditExchange.Syndication.Crif.CreditReport.Proxy.Inquiry.Response;

namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public class Error : IError
    {
        public Error()
        {

        }
        public Error(ERROR error)
        {
            if(error!=null)
            {
                Code = error.CODE;
                Description = error.DESCRIPTION;
            }
        }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
