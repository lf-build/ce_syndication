﻿namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public interface IError
    {
        string Code { get; set; }
        string Description { get; set; }
    }
}