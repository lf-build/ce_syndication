namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public enum AddressSegmentAddressType
    {
        Residence,

        Company,

        ResidenceCumOffice,

        Permanent,

        Current,

        Foreign,

        Military,

        Other
    }
}