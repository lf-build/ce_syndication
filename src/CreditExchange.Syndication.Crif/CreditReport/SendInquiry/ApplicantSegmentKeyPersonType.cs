namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public enum ApplicantSegmentKeyPersonType
    {
        Father,

        Husband,

        Mother,

        Son,

        Daughter,

        Wife,

        Brother,

        MotherInLaw,

        FatherInLaw,

        DaughterInLaw,

        SisterInLaw,

        SonInLaw,

        BrotherInLaw,

        Other
    }
}