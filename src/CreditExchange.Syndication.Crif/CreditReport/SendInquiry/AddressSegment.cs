namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public class AddressSegment : IAddressSegment
    {
        public AddressType Type { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public State State { get; set; }
        public string Pin { get; set; }
    }
}