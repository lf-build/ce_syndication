namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public enum ApplicationSegmentCreditInquiryStage
    {
        PreScreen,

        PreDisb,

        Uwreview,

        Collection,

        Renewal
    }
}