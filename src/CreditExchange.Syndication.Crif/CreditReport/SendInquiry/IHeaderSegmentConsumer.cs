﻿namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public interface IHeaderSegmentConsumer
    {
        bool Individual { get; set; }
        bool Score { get; set; }
      
    }
}