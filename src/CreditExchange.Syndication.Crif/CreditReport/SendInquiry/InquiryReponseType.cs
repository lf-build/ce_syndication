﻿namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public enum InquiryResponseType
    {
        /// 
        ACKNOWLEDGEMENT,
        /// 
        ERROR,
    }
}
