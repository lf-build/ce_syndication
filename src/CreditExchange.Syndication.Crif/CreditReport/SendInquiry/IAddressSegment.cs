﻿namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public interface IAddressSegment
    {
        string Address { get; set; }
        string City { get; set; }
        string Pin { get; set; }
        State State { get; set; }
        AddressType Type { get; set; }
    }
}