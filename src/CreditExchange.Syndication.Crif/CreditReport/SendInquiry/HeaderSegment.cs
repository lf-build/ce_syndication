using System;

namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{

    public class HeaderSegment : IHeaderSegment
    {
        public DateTime InquiryDatetime { get; set; }
        public string ResponseFormat { get; set; }
        public HeaderSegmentYesNo ResponseFormatEmbedded { get; set; }
        public HeaderSegmentMfi Mfi { get; set; }
        public HeaderSegmentConsumer Consumer { get; set; }
        public bool Ioi { get; set; }
        public RequestActionType RequestActionType { get; set; }
    }
}