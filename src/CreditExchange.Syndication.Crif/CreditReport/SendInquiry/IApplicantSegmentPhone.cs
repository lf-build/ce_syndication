﻿namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public interface IApplicantSegmentPhone
    {
        string TelephoneNumber { get; set; }
        ApplicantSegmentPhoneTelephoneNumberType TelephoneNumberType { get; set; }
    }
}