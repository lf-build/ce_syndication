namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public class ApplicantSegmentNominee : IApplicantSegmentNominee
    {
        public string Name { get; set; }

        public ApplicantSegmentNomineeType Type { get; set; }
    }
}