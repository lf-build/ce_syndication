﻿namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public enum HeaderSegmentTestFlag
    {
        HMTEST,

        HMLIVE
    }
}