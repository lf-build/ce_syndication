using System;

namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public class ApplicantSegmentDob : IApplicantSegmentDob
    {
        public DateTime DateOfBith { get; set; }
        public int Age { get; set; }
        public DateTime AgeAsOn { get; set; }
    }
}