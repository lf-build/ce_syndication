using System.Collections.Generic;
using LendFoundry.Foundation.Client;

using Newtonsoft.Json;

namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public class ApplicantSegment : IApplicantSegment
    {
        [JsonConverter(typeof(InterfaceConverter<IApplicantName, ApplicantSegmentApplicantName>))]
        public IApplicantName ApplicantName { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IApplicantSegmentDob, ApplicantSegmentDob>))]
        public IApplicantSegmentDob DateOfBirth { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IIdentification, Identification>))]
        public List<IIdentification> Ids { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IRelation, Relation>))]
        public List<IRelation> Relations { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IKeyPerson, ApplicantSegmentKeyPerson>))]
        public IKeyPerson KeyPerson { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IApplicantSegmentNominee, ApplicantSegmentNominee>))]
        public IApplicantSegmentNominee Nominee { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IApplicantSegmentPhone, ApplicantSegmentPhone>))]
        public List<IApplicantSegmentPhone> Phones { get; set; }
        public Gender Gender { get; set; }
        public bool GenderSpecified { get; set; }
    }
}