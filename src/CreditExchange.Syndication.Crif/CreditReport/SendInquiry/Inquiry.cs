﻿using System.Collections.Generic;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Inquiry.Response;
using System.Linq;

namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public class Inquiry : IInquiry
    {
        public  Inquiry()
        {
        }
        public Inquiry(INQUIRY inquiry)
        {
            if(inquiry!=null)
            {
                InquiryUniqueReferenceNumber = inquiry.INQUIRYUNIQUEREFNO;
                MemberId = inquiry.MBRID;
                RequestDatetime = inquiry.REQUESTDTTM;
                ResponseDatetime = inquiry.RESPONSEDTTM;
                ReportId = inquiry.REPORTID;
                ResponseType =(InquiryResponseType)inquiry.REPONSETYPE;
                if(inquiry.ERRORS!=null)
                Errors = inquiry.ERRORS.Select(p => new Error(p)).ToList<IError>();
            }
        }

        public string InquiryUniqueReferenceNumber { get; set; }
        public string MemberId { get; set; }
        public string RequestDatetime { get; set; }
        public string ReportId { get; set; }
        public string ResponseDatetime { get; set; }
        public InquiryResponseType ResponseType { get; set; }
        public List<IError> Errors { get; set; }
    }
}
