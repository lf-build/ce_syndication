namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public class HeaderSegmentMfi : IHeaderSegmentMfi
    {
        public bool Individual { get; set; } 
        public bool Score { get; set; } 
        public bool Group { get; set; }
      
    }
}