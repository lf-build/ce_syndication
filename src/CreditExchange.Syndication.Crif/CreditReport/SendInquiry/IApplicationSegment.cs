﻿using System;

namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public interface IApplicationSegment
    {
        string BranchId { get; set; }
        string CreditInquiryPurposeType { get; set; }
        string CreditInquiryPurposeTypeDescription { get; set; }
        string CreditInquiryStage { get; set; }
        string CreditReportId { get; set; }
        DateTime CreditReportTransactionDatetime { get; set; }
        string CreditReportTransactionId { get; set; }
        string CreditRequestType { get; set; }
        string InquiryUniqueReferenceNumber { get; set; }
        string KendraId { get; set; }
        decimal LoanAmount { get; set; }
        string LosAppId { get; set; }
        string MemberId { get; set; }
    }
}