namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public class ApplicantSegmentKeyPerson : IKeyPerson
    {
        public string Name { get; set; }
        public ApplicantSegmentKeyPersonType Type { get; set; }
    }
}