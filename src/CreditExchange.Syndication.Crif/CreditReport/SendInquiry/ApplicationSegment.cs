using System;

namespace CreditExchange.Syndication.Crif.CreditReport.SendInquiry
{
    public class ApplicationSegment : IApplicationSegment
    {
        public string InquiryUniqueReferenceNumber { get; set; }
        public string CreditReportId { get; set; }
        public string CreditRequestType { get; set; } = "INDV";
        public string CreditReportTransactionId { get; set; }
        public string CreditInquiryPurposeType { get; set; } = "ACCT-ORIG";
        public string CreditInquiryPurposeTypeDescription { get; set; }
        public string CreditInquiryStage { get; set; } = "PRE-DISB";
        public DateTime CreditReportTransactionDatetime { get; set; }
        public string MemberId { get; set; }
        public string KendraId { get; set; }
        public string BranchId { get; set; }
        public string LosAppId { get; set; }
        public decimal LoanAmount { get; set; }
    }
}