﻿using System;
using System.Threading.Tasks;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using CreditExchange.Syndication.Crif.CreditReport.Proxy;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Inquiry;
using CreditExchange.Syndication.Crif.CreditReport.Proxy.Issue;
using CreditExchange.Syndication.Crif.CreditReport.Request;
using CreditExchange.Syndication.Crif.CreditReport.Response;
using CreditExchange.Syndication.Crif.Events;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using System.Linq;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.Crif.CreditReport
{
    public class CrifCreditReportService : ICrifCreditReportService
    {
        public CrifCreditReportService(ICrifCreditReportConfiguration configuration, ICrifCreditReportProxy proxy, IEventHubClient eventHub, 
            ILookupService lookup, ICrifCreditReportRepository CrifCreditReportRepository, ITenantTime tenantTime,
            ILogger logger)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));

            Configuration = configuration;
            Proxy = proxy;
            EventHub = eventHub;
            Lookup = lookup;
            TenantTime = tenantTime;
            crifCreditReportRepository = CrifCreditReportRepository;
            Logger = logger;
        }
        private ILogger Logger { get; }
        private IEventHubClient EventHub { get; }
        private ICrifCreditReportConfiguration Configuration { get; }
        private ICrifCreditReportProxy Proxy { get; }
        private ILookupService Lookup { get; }
        private ICrifCreditReportRepository crifCreditReportRepository { get; }
        private ITenantTime TenantTime { get; }

        public static string ServiceName { get; } = "crif-credit-report";
        public async Task<ICreditReportInquiryResponse> SendInquiry(string entityType, string entityId, ICreditReportInquiryRequest sendInquiryRequest)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} can not be null");
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} can not be null");
            entityType = EnsureEntityType(entityType);
            if (sendInquiryRequest == null)
                throw new ArgumentNullException(nameof(sendInquiryRequest ));
            if (string.IsNullOrEmpty(sendInquiryRequest.Name))
            throw new ArgumentException($"{nameof(sendInquiryRequest.Name)} can not be null");
            if (string.IsNullOrEmpty(sendInquiryRequest.RationCard) && string.IsNullOrEmpty(sendInquiryRequest.Others) && string.IsNullOrEmpty(sendInquiryRequest.Uid) && string.IsNullOrEmpty(sendInquiryRequest.Pan) && string.IsNullOrEmpty(sendInquiryRequest.DrivingLicNo) && string.IsNullOrEmpty(sendInquiryRequest.Passport) && string.IsNullOrEmpty(sendInquiryRequest.VoterId))
                throw new ArgumentException("Unique identification is require (VoterId/PassportNo/DrivingLicNo/PAN/UID/RatioCard/DrivingLicenseNo/Others)");
            if (sendInquiryRequest.Addresses == null)
                throw new ArgumentException($"{nameof(sendInquiryRequest.Addresses)} can not be null" );
            Logger.Info($"[CRIF] SendInquiry method invoked for [{entityType}] with id #{entityId} using  data { (sendInquiryRequest != null ? JsonConvert.SerializeObject(sendInquiryRequest) : null)}");
            try
            {
                var proxyRequest = new SendInquiryRequest(Configuration, sendInquiryRequest);
                var response = new CreditReportInquiryResponse(Proxy.SendInquiry(proxyRequest));
                response.ReferenceNumber = response.InquiryReferenceNumber;
                Logger.Info($"[CRIF] Publishing CrifCreditReportInquirySent event for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new CrifCreditReportInquirySent
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = response,
                    Request = sendInquiryRequest,
                    ReferenceNumber = response.InquiryReferenceNumber,
                    Name = ServiceName
                });
                return response;
            }
            catch(CrifException exception)
            {
                Logger.Error("[CRIF] Error occured when SendInquiry method was called. Error: ", exception);
                throw new CrifException(exception.Message);
            }
            
        }

        public async Task<IGetCreditReportResponse> GetReport(string entityType, string entityId, string inquiryReferenceNumber, string reportId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentException($"{nameof(entityId)} can not be null");
                if (string.IsNullOrWhiteSpace(entityType))
                    throw new ArgumentException($"{nameof(entityType)} can not be null");
                entityType = EnsureEntityType(entityType);

                if (string.IsNullOrWhiteSpace(inquiryReferenceNumber))
                       throw new ArgumentException($"{nameof(inquiryReferenceNumber)} cannot be null or whitespace");
                if (string.IsNullOrWhiteSpace(reportId))
                    throw new ArgumentException($"{nameof(reportId)} can not be null or whitespace.");
                Logger.Info($"[CRIF] GetReport method invoked for [{entityType}] with id #{entityId} and inquiryReferenceNumber :{inquiryReferenceNumber} ,reportId :{reportId}");
                var proxyRequest = new ReportRequest(Configuration, reportId, inquiryReferenceNumber);
                var response = new GetCreditReportResponse(Proxy.IssueReport(proxyRequest));
                var referenceNumber = Guid.NewGuid().ToString("N");
                response.ReferenceNumber = referenceNumber;
               
                if (response.Inquiries!= null)
                {
                    return response;
                }
                Logger.Info($"[CRIF] Publishing CrifCreditReportPulled event for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new CrifCreditReportPulled
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = response,
                    Request = new { inquiryReferenceNumber, reportId },
                    ReferenceNumber = referenceNumber,
                    Name = ServiceName
                });

                return response;
            }
            catch (CrifException ex)
            {
                Logger.Info($"[CRIF] Publishing CreditReportPullFail event for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new CreditReportPullFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = new { inquiryReferenceNumber, reportId },
                    ReferenceNumber = null,
                    Name = ServiceName
                });
                Logger.Error("[CRIF] Error occured when GetReport method was called. Error: ", ex);
                throw new CrifException(ex.Message);
            }
        }


        public async Task<IGetCreditReportResponse> GetReport(string entityType, string entityId, ICreditReportInquiryRequest sendInquiryRequest)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} can not be null");
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} can not be null");

            entityType = EnsureEntityType(entityType);
            if (sendInquiryRequest == null)
                throw new ArgumentNullException(nameof(sendInquiryRequest));
            if (string.IsNullOrEmpty(sendInquiryRequest.Name))
                throw new ArgumentException($"{nameof(sendInquiryRequest.Name)} can not be null");
            if (string.IsNullOrEmpty(sendInquiryRequest.Pan) && string.IsNullOrEmpty(sendInquiryRequest.DrivingLicNo) && string.IsNullOrEmpty(sendInquiryRequest.Passport) && string.IsNullOrEmpty(sendInquiryRequest.VoterId))
                throw new ArgumentException("Unique identification is require (VoterId/PassportNo/DrivingLicNo/PAN/UID/RatioCard/DrivingLicenseNo/Others)");

            if (sendInquiryRequest.Addresses == null)
                throw new ArgumentException($"{nameof(sendInquiryRequest.Addresses)} can not be null");
            Logger.Info($"[CRIF] GetReport method invoked for [{entityType}] with id #{entityId} using   data { (sendInquiryRequest != null ? JsonConvert.SerializeObject(sendInquiryRequest) : null)}");
            try
            {
                IGetCreditReportResponse response = null;
                var crifReportInformation = await crifCreditReportRepository.GetCrifReportDetail(entityType, entityId);

                if (crifReportInformation != null)
                {
                    response = await GetReport(entityType, entityId, crifReportInformation.Inquiryreferencenumber, crifReportInformation.ReportId);
                }
                else
                {
                    var creditReportInquiry = await SendInquiry(entityType, entityId, sendInquiryRequest);
                    var crifItem = new CrifCreditReport()
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        ReportId = creditReportInquiry.ReportId,
                        Inquiryreferencenumber = creditReportInquiry.InquiryReferenceNumber,
                        CreatedDate = new TimeBucket(TenantTime.Now.UtcDateTime)
                    };

                    crifCreditReportRepository.Add(crifItem);
                    response = await GetReport(entityType, entityId, creditReportInquiry.InquiryReferenceNumber, creditReportInquiry.ReportId);
                }
                return response;
            }
            catch(CrifException exception)
            {
                Logger.Error("[CRIF] Error occured when GetReport method was called. Error: ", exception);
                throw new CrifException(exception.Message);
            }
        }
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }

    }
}
