﻿namespace CreditExchange.Syndication.Crif.CreditReport
{
    public interface ICrifCreditReportConfiguration : ICrifConfiguration
    {
        IsEmbededResponse IsEmbededResponse { get; set; }
        bool MfiIndividual { get; set; }
        bool MfiScore { get; set; }
        bool MfiGroup { get; set; }
        bool ConsumerIndividual { get; set; }
        bool ConsumerScore { get; set; }
        bool Ioi { get; set; }
        string BranchId { get; set; }
        string InquiryUniqueReferenceNumber { get; set; }
        string RequestType { get; set; }
        string InquiryPurposeType { get; set; }
        string CreditInquiryStage { get; set; }
        string LoanAmount { get; set; }
        string ReportActionType { get; set; }
        string ReportProductType { get; set; }
    }
}