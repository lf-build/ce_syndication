﻿using System.Threading.Tasks;
using CreditExchange.Syndication.Crif.CreditReport.Request;
using CreditExchange.Syndication.Crif.CreditReport.Response;

namespace CreditExchange.Syndication.Crif.CreditReport
{
    public interface ICrifCreditReportService
    {
        Task<IGetCreditReportResponse> GetReport(string entityType, string entityId, ICreditReportInquiryRequest sendInquiryRequest);
        Task<IGetCreditReportResponse> GetReport(string entityType, string entityId, string inquiryReferenceNumber, string reportId);
        Task<ICreditReportInquiryResponse> SendInquiry(string entityType, string entityId, ICreditReportInquiryRequest inquiryRequest);
    }
}