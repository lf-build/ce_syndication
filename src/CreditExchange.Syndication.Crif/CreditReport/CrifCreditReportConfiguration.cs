﻿namespace CreditExchange.Syndication.Crif.CreditReport
{
    
    public class CrifCreditReportConfiguration : CrifConfiguration,  ICrifCreditReportConfiguration
    {

        public CrifCreditReportConfiguration()
        {
            ApiUrl = "https://test.highmark.in/Inquiry/doGet.service/requestResponse";
            ProductType = "INDV";
            ProductVersion = "1.0";
            RequestVolumeType = "INDV";
            TestFlag = "HMTEST";
            AuthFlag = "Y";
            AuthTitle = "USER";
            MemberPreOverride = "N";

        }
        public IsEmbededResponse IsEmbededResponse { get; set; } = IsEmbededResponse.Y;
        public bool MfiIndividual { get; set; } = true;
        public bool MfiScore { get; set; } = true;
        public bool MfiGroup { get; set; } = true;
        public bool ConsumerIndividual { get; set; } = true;
        public bool ConsumerScore { get; set; } = true;
        public bool Ioi { get; set; } = true;
        public string BranchId { get; set; } = "home";
        public string InquiryUniqueReferenceNumber { get; set; } = "006f000026EGk88JJFS99115741";
        public string RequestType { get; set; } = "INDV";

        public string InquiryPurposeType { get; set; } = "ACCT-ORIG";

        public string CreditInquiryStage { get; set; } = "PRE-SCREEN";

        public string LoanAmount { get; set; } = "0";

        public string ReportActionType { get; set; } = "ISSUE";

        public string ReportProductType { get; set; } = "BASE_PLUS_REPORT";

    }
}
