﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Crif.CreditReport
{
    public interface ICrifCreditReportRepository : IRepository<ICrifCreditReport>
    {
        Task<ICrifCreditReport> GetCrifReportDetail(string entityType , string entityId);
    }
}
