﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Crif.CreditReport
{
    public class CrifCreditReport: Aggregate, ICrifCreditReport
    {
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string Inquiryreferencenumber { get; set; }
        public string ReportId { get; set; }
        public TimeBucket CreatedDate { get; set; }
    }
}
