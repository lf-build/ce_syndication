namespace CreditExchange.Syndication.Crif
{
    public enum Gender
    {
        Male,
        Female,
        TransgenderOrOther
    }
}