﻿using System;

namespace CreditExchange.Syndication.Crif
{
    public class CrifConfiguration : ICrifConfiguration
    {
        public  string ApiUrl { get; set; }
        public string MemberId { get; set; }
        public string SubMemberId { get; set; }
        public string Password { get; set; }
        public string ProductType { get; set; }
        public string ProductVersion { get; set; }
        public string RequestVolumeType { get; set; }
        public string TestFlag { get; set; }
        public string AuthFlag { get; set; }
        public string AuthTitle { get; set; }
        public string LosName { get; set; }
        public string LosVendor { get; set; }
        public string LosVersion { get; set; }
        public string MemberPreOverride { get; set; }
        public string UserId { get; set; }
        public string Certificate { get; set; }
        public string ActionType { get; set; }
        public ResponseFormat ResponseFormat { get; set; }
        public DateTime InquiryDateTime { get; set; }
    }
}
