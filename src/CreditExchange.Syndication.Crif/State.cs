﻿namespace CreditExchange.Syndication.Crif
{
    public enum State
    {
        AndhraPradesh,

        ArunachalPradesh,

        Assam,

        Bihar,

        Chattisgarh,

        Goa,

        Gujarat,

        Haryana,

        HimachalPradesh,

        JammuKashmir,

        Jharkhand,

        Karnataka,

        Kerala,

        MadhyaPradesh,

        Maharashtra,

        Manipur,

        Meghalaya,

        Mizoram,

        Nagaland,

        Orissa,

        Punjab,

        Rajasthan,

        Sikkim,

        TamilNadu,

        Tripura,

        Uttarakhand,

        UttarPradesh,

        WestBengal,

        AndamanNicobar,

        Chandigarh,

        DadraandNagarHaveli,

        DamanDiu,

        Delhi,

        Lakshadweep,

        Pondicherry
    }
}
