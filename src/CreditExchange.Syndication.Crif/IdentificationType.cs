namespace CreditExchange.Syndication.Crif
{
    public enum IdentificationType
    {
        Passport,

        VoterIdCard,

        AadharCard,

        Others,

        RationCard,

        DrivingLicense,

        PanCard
    }
}