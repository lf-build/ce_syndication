﻿

namespace CreditExchange.Syndication.Crif
{
    public enum IsEmbededResponse
    {
        Y,
        N
    }
}
