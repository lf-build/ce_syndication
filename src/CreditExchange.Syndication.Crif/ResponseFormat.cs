﻿namespace CreditExchange.Syndication.Crif
{
    public enum ResponseFormat
    {
        Xml,
        Xmlhtml,
        Xmlpdf,
    }
}
