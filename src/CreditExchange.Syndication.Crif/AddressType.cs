﻿namespace CreditExchange.Syndication.Crif
{
    public enum AddressType
    {
        /// <remarks/>
        Residence,

        /// <remarks/>
        Company,

        /// <remarks/>
        ResCumOff,

        /// <remarks/>
        Permanent,

        /// <remarks/>
        Current,

        /// <remarks/>
        Foreign,

        /// <remarks/>
        Military,

        /// <remarks/>
        Other,
    }
}
