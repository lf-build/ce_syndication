﻿using System;

namespace CreditExchange.Syndication.Crif
{
    public interface ICrifConfiguration
    {
        string ApiUrl { get; set; }
        string AuthFlag { get; set; }
        string AuthTitle { get; set; }
        string Certificate { get; set; }
        string LosName { get; set; }
        string LosVendor { get; set; }
        string LosVersion { get; set; }
        string MemberId { get; set; }
        string MemberPreOverride { get; set; }
        string Password { get; set; }
        string ProductType { get; set; }
        string ProductVersion { get; set; }
        string RequestVolumeType { get; set; }
        string SubMemberId { get; set; }
        string TestFlag { get; set; }
        string UserId { get; set; }
        string ActionType { get; set; }
        ResponseFormat ResponseFormat { get; set; }
        DateTime InquiryDateTime { get; set; }
    }
}