﻿namespace CreditExchange.Syndication.Crif
{
    public class ApplicantName :IApplicantName
    {
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string Name3 { get; set; }
        public string Name4 { get; set; }
        public string Name5 { get; set; }

    }
}
