namespace CreditExchange.Syndication.Crif
{
    public enum RelationType
    {
        Father,

        Husband,

        Mother,

        Son,

        Daughter,

        Wife,

        Brother,

        MotherInLaw,

        FatherInLaw,

        DaughterInLaw,

        SisterInLaw,

        SonInLaw,

        BrotherInLaw,

        Other
    }
}