﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Towerdata.Persistence
{
    public class MongoVerifyIpAddressRepository : MongoRepository<IVerifyIpAddress, VerifyIpAddress>, IVerifyIpAddressRepository
    {

        static MongoVerifyIpAddressRepository()
        {
            BsonClassMap.RegisterClassMap<VerifyIpAddress>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.ReportDate).SetSerializer(new DateTimeSerializer(BsonType.Document));
                map.MapMember(m => m.ExpirationDate).SetSerializer(new DateTimeSerializer(BsonType.Document));
                map.MapMember(m => m.Response).SetSerializer(new ObjectSerializer());

                var type = typeof(VerifyIpAddress);
                map.SetDiscriminator($"{type.FullName},{type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }
        public MongoVerifyIpAddressRepository(IMongoConfiguration configuration, ITenantService tenantService, ITenantTime tenantTime):base(tenantService,configuration, "VerifyIpAddress")
        {
            CreateIndexIfNotExists
            (
               indexName: "VerifyIpAddress",
                 index: Builders<IVerifyIpAddress>.IndexKeys.Ascending(i => i.IpAddress)
            );
            CreateIndexIfNotExists
           (
              indexName: "VerifyIpAddress",
                index: Builders<IVerifyIpAddress>.IndexKeys.Ascending(i => i.EntityId)
           );
            CreateIndexIfNotExists
           (
              indexName: "VerifyIpAddress",
                index: Builders<IVerifyIpAddress>.IndexKeys.Ascending(i => i.EntityType)
           );
            CreateIndexIfNotExists
            (
             indexName: "VerifyIpAddress",
               index: Builders<IVerifyIpAddress>.IndexKeys.Ascending(i => i.ExpirationDate)
            );
            TenantTime = tenantTime;

        }
        private ITenantTime TenantTime { get; }


        public List<IVerifyIpAddress> Get(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"#{nameof(entityId)} cannot be null");
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"#{nameof(entityType)} cannot be null");
            var now = TenantTime.Now;

            var VerifyIpAddressReport = Query
                .Where(b => b.EntityType.ToLower() == entityType.ToLower() && b.EntityId == entityId)
                .Where(b => b.ExpirationDate >= now).ToList();


            return VerifyIpAddressReport;
        }
    }
}
