﻿using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using LendFoundry.Foundation.Date;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson;
using LendFoundry.Tenant.Client;

namespace CreditExchange.Towerdata.Persistence
{
    public class MongoGetEmailInformatioRepository : MongoRepository<IGetEmailInformation, GetEmailInformation>, IGetEmailInformationRespository
    {

        static MongoGetEmailInformatioRepository()
        {
            BsonClassMap.RegisterClassMap<GetEmailInformation>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.ReportDate).SetSerializer(new DateTimeSerializer(BsonType.Document));
                map.MapMember(m => m.ExpirationDate).SetSerializer(new DateTimeSerializer(BsonType.Document));
                map.MapMember(m => m.Response).SetSerializer(new ObjectSerializer());

                var type = typeof(GetEmailInformation);
                map.SetDiscriminator($"{type.FullName},{type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }
        public MongoGetEmailInformatioRepository(IMongoConfiguration configuration, ITenantService tenantService, ITenantTime tenantTime):base(tenantService,configuration,"GetEmailInformatio")
        {
            CreateIndexIfNotExists
            (
               indexName: "getemailinformation",
                 index: Builders<IGetEmailInformation>.IndexKeys.Ascending(i => i.Email)
            );
            CreateIndexIfNotExists
           (
              indexName: "getemailinformation",
                index: Builders<IGetEmailInformation>.IndexKeys.Ascending(i => i.EntityId)
           );
            CreateIndexIfNotExists
           (
              indexName: "getemailinformation",
                index: Builders<IGetEmailInformation>.IndexKeys.Ascending(i => i.EntityType)
           );
            CreateIndexIfNotExists
            (
             indexName: "getemailinformation",
               index: Builders<IGetEmailInformation>.IndexKeys.Ascending(i => i.ExpirationDate)
            );
            TenantTime = tenantTime;

        }
        private ITenantTime TenantTime { get; }

      
        public List<IGetEmailInformation> Get(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"#{nameof(entityId)} cannot be null");
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"#{nameof(entityType)} cannot be null");
            var now = TenantTime.Now;

            var GetEmailInformationReport = Query
                .Where(b => b.EntityType.ToLower() == entityType.ToLower() && b.EntityId == entityId)
                .Where(b => b.ExpirationDate >= now).ToList();


            return GetEmailInformationReport;
        }
    }
}
