﻿using CreditExchange.Syndication.Abstractions;

namespace CreditExchange.Syndication
{
    public interface ISyndicationFactory
    {
        ISyndication Create(string syndicationName);
    }
}
