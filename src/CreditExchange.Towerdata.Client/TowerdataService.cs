﻿using CreditExchange.Syndication.TowerData;
using LendFoundry.Foundation.Client;
using RestSharp;
using System.Threading.Tasks;

namespace CreditExchange.Towedata.Client
{
    public class TowerdataService :ITowerDataService
    {
        public TowerdataService(IServiceClient client)
        {
            Client = client;
        }
        private IServiceClient Client { get; }

        public async Task<IEmailInformation> GetEmailInformation(string entityType, string email)
        {
            var restrequest = new RestRequest("/{entityType}/email/{email}/info", Method.GET);
            restrequest.AddUrlSegment("entityType", entityType);
            restrequest.AddUrlSegment("email", email);
            return await Client.ExecuteAsync<EmailInformation>(restrequest);
        }
        public async Task<IEmailVerificationResponse> VerifyEmail(string entityType, string email)
        {
            var restrequest = new RestRequest("/{entityType}/email/{email}/verify", Method.GET);
            restrequest.AddUrlSegment("entityType", entityType);
            restrequest.AddUrlSegment("email", email);
            return await Client.ExecuteAsync<EmailVerificationResponse>(restrequest);
        }
        public async Task<IIpVerificationResponse> VerifyIpAddress(string entityType, string ipAddress)
        {
            var restrequest = new RestRequest("/{entityType}/ip/{ip}/verify", Method.POST);
            restrequest.AddUrlSegment("entityType", entityType);
            restrequest.AddUrlSegment("ip", ipAddress);
            return await Client.ExecuteAsync<IpVerificationResponse>(restrequest);
        }
    }
}
