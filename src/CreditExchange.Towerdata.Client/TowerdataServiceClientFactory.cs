﻿using CreditExchange.Syndication.TowerData;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;

namespace CreditExchange.Towedata.Client
{
    public class TowerdataServiceClientFactory :ITowerdataServiceClientFactory
    {
        public TowerdataServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public ITowerDataService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new TowerdataService(client);
        }

    }
}
