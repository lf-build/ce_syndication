﻿using CreditExchange.Syndication.TowerData;
using LendFoundry.Security.Tokens;

namespace CreditExchange.Towedata.Client
{
    public interface ITowerdataServiceClientFactory
    {
        ITowerDataService Create(ITokenReader reader);
    }
}
