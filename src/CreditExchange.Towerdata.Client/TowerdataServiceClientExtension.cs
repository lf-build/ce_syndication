﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace CreditExchange.Towedata.Client
{
    public static class TowerdataServiceClientExtension
    {
        public static IServiceCollection AddTowerdataService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ITowerdataServiceClientFactory>(p => new TowerdataServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ITowerdataServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
