﻿using System;

namespace CreditExchange.Syndication.Cibil
{
    public class CibilConfiguration : ICibilConfiguration
    {
        public string Url { get; set; } =
            "http://www.test.transuniondecisioncentre.co.in/TU.IDS.ExternalServices_UAT/solutionexecution/ExternalSolutionExecution.svc";
        public string ReturnHTMLUrl { get; set; } = "http://www.test.transuniondecisioncentre.co.in/DC/HTMLConvertersec/HTMLConverter.asmx";
        public string UserId { get; set; } = "CreditExchange_Admin";
        public string Password { get; set; } = "Password@123";
        public string SolutionSetId { get; set; } = "1135";
        public string SolutionSetVersion { get; set; } = "26";
        public string ExecutionMode { get; set; } = "NewWithContext";
        public string EnvironmentType { get; set; } = "U";
        public string InpEnquiryPurpose { get; set; } = "05";
        public string InpScoreType { get; set; } = "04";
        public string IsCirReq { get; set; } = "Y";
        public string IsEverifyReq { get; set; } = "Y";
        public string Authentication { get; set; } = "OnDemand";

        public string IDVMemberCode { get; set; }
        public string ClientID { get; set; } = "21";

        public string ProxyUrl { get; set; } = "";

        public bool UseProxy { get; set; } = true;

        public bool IsSecondaryReportRequired { get; set; } = true;

        public bool SkipCibilBureauFlag
        { get; set; }

        public bool SkipDSTuIDVisionFlag
        { get; set; }

        public bool SkipDSTuNtcFlag
        { get; set; }

        public string ConsumerConsentForUIDAIAuthentication
        { get; set; } = "Y";

        public string Product { get; set; }
        public string CibilRequestBuilderRule { get; set; }

        public string EnvironmentId { get; set; } = "1";
    }
}
