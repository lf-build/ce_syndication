﻿using System;
using System.Runtime.Serialization;

namespace CreditExchange.Syndication.Cibil
{
    [Serializable]
    public class CibilException : Exception
    {
        public CibilException()
        {
        }

        public CibilException(string message) : base(message)
        {
        }

        public CibilException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected CibilException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
