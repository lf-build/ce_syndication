﻿namespace CreditExchange.Syndication.Cibil
{
    public enum AddressCategory
    {
        NotCategorized=00,
        Permanent =01,
        Residence=02,
        Office=03
    }
}