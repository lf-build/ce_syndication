﻿using System;
using System.IO;
using System.Threading.Tasks;
using CreditExchange.Syndication.Cibil.Proxy.Request;
using System.Text;
using CreditExchange.Syndication.Cibil.Proxy.Response;
using LendFoundry.Foundation.Logging;
using Newtonsoft.Json;
using RestSharp;
using System.Net;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security;
using System.Security.Cryptography;
#if DOTNET2
using System.Web;
#else
using RestSharp.Contrib;
#endif
namespace CreditExchange.Syndication.Cibil.Proxy
{
    public class ProxyReport : IProxyReport
    {
        public ProxyReport(ICibilConfiguration cibilConfiguration, ILogger logger)
        {
            if (cibilConfiguration == null)
                throw new ArgumentNullException(nameof(cibilConfiguration));
            if (string.IsNullOrWhiteSpace(cibilConfiguration.Url))
                throw new ArgumentException("Url is required", nameof(cibilConfiguration.Url));
            if (string.IsNullOrWhiteSpace(cibilConfiguration.UserId))
                throw new ArgumentException("UserId is required", nameof(cibilConfiguration.UserId));
            if (string.IsNullOrWhiteSpace(cibilConfiguration.Password))
                throw new ArgumentException("Password is required", nameof(cibilConfiguration.Password));
            if (string.IsNullOrWhiteSpace(cibilConfiguration.Authentication))
                throw new ArgumentException("Authentication is required", nameof(cibilConfiguration.Authentication));
            if (string.IsNullOrWhiteSpace(cibilConfiguration.EnvironmentType))
                throw new ArgumentException("EnvironmentType is required", nameof(cibilConfiguration.EnvironmentType));
            if (string.IsNullOrWhiteSpace(cibilConfiguration.ExecutionMode))
                throw new ArgumentException("ExecutionMode is required", nameof(cibilConfiguration.ExecutionMode));
            if (string.IsNullOrWhiteSpace(cibilConfiguration.InpEnquiryPurpose))
                throw new ArgumentException("InpEnquiryPurpose is required", nameof(cibilConfiguration.InpEnquiryPurpose));
            if (string.IsNullOrWhiteSpace(cibilConfiguration.InpScoreType))
                throw new ArgumentException("InpScoreType is required", nameof(cibilConfiguration.InpScoreType));
            if (string.IsNullOrWhiteSpace(cibilConfiguration.IsCirReq))
                throw new ArgumentException("IsCirReq is required", nameof(cibilConfiguration.IsCirReq));
            if (string.IsNullOrWhiteSpace(cibilConfiguration.IsEverifyReq))
                throw new ArgumentException("IsEverifyReq is required", nameof(cibilConfiguration.IsEverifyReq));
            if (string.IsNullOrWhiteSpace(cibilConfiguration.SolutionSetId))
                throw new ArgumentException("SolutionSetId is required", nameof(cibilConfiguration.SolutionSetId));
            if (string.IsNullOrWhiteSpace(cibilConfiguration.CibilRequestBuilderRule))
                throw new ArgumentException("CibilRequestBuilderRule is required", nameof(cibilConfiguration.SolutionSetVersion));
            CibilConfiguration = cibilConfiguration;
            Logger = logger;

        }
        private ILogger Logger { get; }
        private ICibilConfiguration CibilConfiguration { get; }
        public async Task<XMLResponse> GetCreditReport(DCRequest dcRequest)
        {
            if (dcRequest == null)
                throw new ArgumentNullException(nameof(dcRequest));
           
          
            var dcRequestXml = XmlSerialization.Serialize(dcRequest, "http://transunion.com/dc/extsvc","",false);
            Logger.Info($"[Cibil] GetCreditReport method xmlrequest #{dcRequestXml} ");
            var cibilreportxmlstring = HttpUtility.HtmlEncode(dcRequestXml);
            cibilreportxmlstring = cibilreportxmlstring?.Replace("&amp;", "&").Replace("&quot;","\"");
            var evelopeRequestXml = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body><ExecuteXMLString xmlns=\"http://tempuri.org/\"><request>" +
                                    cibilreportxmlstring + "</request></ExecuteXMLString></s:Body></s:Envelope>";
            var baseUri = new Uri(CibilConfiguration.Url);
            var uri = CibilConfiguration.UseProxy ? new Uri($"{CibilConfiguration.ProxyUrl}{baseUri.PathAndQuery}") : baseUri;
            DCResponse dcResponse =null;
            try
            {
                WebRequest request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "text/xml";
                request.Headers.Add("SOAPAction","http://tempuri.org/IExternalSolutionExecution/ExecuteXMLString");
                string response;
                using (Stream reqeustStream =await request.GetRequestStreamAsync())
                {
                    reqeustStream.Write(Encoding.Default.GetBytes(evelopeRequestXml), 0, evelopeRequestXml.Length);
                    using ( HttpWebResponse webresponse = (HttpWebResponse) request.GetResponse())
                    {
                     
                        using (Stream receiveStream = webresponse.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(receiveStream, Encoding.Default);
                            string content = reader.ReadToEnd();
                            response = WebUtility.HtmlDecode(content
                                .Replace(
                                    "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body><ExecuteXMLStringResponse xmlns=\"http://tempuri.org/\"><ExecuteXMLStringResult>",
                                    "").Replace(
                                    "</ExecuteXMLStringResult></ExecuteXMLStringResponse></s:Body></s:Envelope>", ""));
                            if (HttpStatusCode.OK == webresponse.StatusCode)
                            {
                                dcResponse = XmlSerialization.Deserialize<DCResponse>(response);

                            }
                        }
                    }
   
                }

                return  new XMLResponse(dcResponse, dcRequestXml, response);
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Logger.Error($"[Cibil] Error occured when GetCreditReport method was called. Error:", e);
                    
                    using (Stream data = response.GetResponseStream())
                    {
                        string text = new StreamReader(data).ReadToEnd();
                        return new XMLResponse(null, dcRequestXml, text);
                    }
                }
              
            }
            
           
            
           
          

        }

        public async Task<string> GetHTMLReport(string cibilReport)
        {
            var htmlResponse = "";
            if (cibilReport == null)
                throw new ArgumentNullException(nameof(cibilReport));
            Logger.Info($"[Cibil] GetHTMLReport method invoked for data { (cibilReport != null ? JsonConvert.SerializeObject(cibilReport) : null)}");

            var dcResponse = XmlSerialization.Deserialize<DCResponse>(cibilReport);

            var creditreportEncodedxmlstring = dcResponse?.ContextData?.Field.Where(i => i.Key == "Applicants")?.FirstOrDefault();

            if (!string.IsNullOrEmpty(creditreportEncodedxmlstring?.Text))
            {
                var applicantsxmlstring = HttpUtility.HtmlDecode(SecurityElement.Escape(creditreportEncodedxmlstring?.Text));
                var applicants = XmlSerialization.Deserialize<Response.Applicants>(applicantsxmlstring);
                var bureauResponse = applicants?.Applicant?.DsCibilBureau?.Response?.CibilBureauResponse;
                if (bureauResponse?.IsSucess?.ToUpper() == "True".ToUpper())
                {
                    var htmlreportxmlstring = WebUtility.HtmlEncode(bureauResponse?.BureauResponseXml);

                    var evelopeRequestXml = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body><ReturnHTML xmlns=\"http://tempuri.org/\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"><xml>" +
                                            htmlreportxmlstring + "</xml></ReturnHTML></s:Body></s:Envelope>";
                    try
                    {
                        var baseUri = new Uri(CibilConfiguration.ReturnHTMLUrl);
                       WebRequest request = WebRequest.Create(baseUri);
                       request.Method = "POST";
                       request.ContentType = "text/xml";
                       request.Headers.Add("SOAPAction","http://tempuri.org/ReturnHTML");
                        string response;
                        using (Stream reqeustStream = await request.GetRequestStreamAsync())
                        {
                            reqeustStream.Write(Encoding.Default.GetBytes(evelopeRequestXml), 0, evelopeRequestXml.Length);
                            using ( HttpWebResponse webresponse = (HttpWebResponse) request.GetResponse())
                            {
                     
                                using (Stream receiveStream = webresponse.GetResponseStream())
                                {
                                    StreamReader reader = new StreamReader(receiveStream, Encoding.Default);
                                    string content = reader.ReadToEnd();
                                     response = WebUtility.HtmlDecode(content.Replace("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">", "").
                                        Replace("<soap:Body>", "")
                                        .Replace("</soap:Body>", "").Replace("</soap:Envelope>", ""));
                                    htmlResponse = response.Replace("<ReturnHTMLResponse xmlns=\"http://tempuri.org/\"><ReturnHTMLResult>", "")
                                        .Replace("</ReturnHTMLResult></ReturnHTMLResponse>", "")
                                        .Replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "");
                                   
                                }
                            }
                        }
                    }
                    catch (WebException e)
                    {
                        Logger.Error($"[Cibil] Error occured when GetHTMLReport method was called.", e);
                        throw new CibilException("Not able to build request payload.cibil credit report not found",e);
                    }
                    
                }
                else
                {
                    throw new CibilException("Not able to build request payload.cibil credit report not found");
                }
            }
            return htmlResponse;
        }
    }
}
