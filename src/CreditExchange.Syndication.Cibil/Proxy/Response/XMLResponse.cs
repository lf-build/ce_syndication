﻿using System.Net;

namespace CreditExchange.Syndication.Cibil.Proxy.Response
{
    public class XMLResponse
    {
        public XMLResponse(DCResponse dcResponse,string requestXML,string responseXML)
        {
            DCResponse = dcResponse;
            RequestXML = requestXML;
            ResponseXML =responseXML;
        }
        
        public DCResponse DCResponse { get; set; }
        public string RequestXML { get; set; }
        public string ResponseXML { get; set; }

        public string Status { get; set; }
    }
}
