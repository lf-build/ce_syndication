﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace CreditExchange.Syndication.Cibil.Proxy.Response
{
    [XmlRoot(ElementName = "Authentication")]
    public class Authentication
    {
        public Authentication()
        {

        }
        public Authentication(string status,string token)
        {
            Status = status;
            Token = token;
        }
        [XmlElement(ElementName = "Status")]
        public string Status { get; set; }
        [XmlElement(ElementName = "Token")]
        public string Token { get; set; }
    }

    [XmlRoot(ElementName = "ResponseInfo")]
    public class ResponseInfo
    {
        public ResponseInfo()
        {

        }
        public ResponseInfo(string applicationId,string solutionSetInstanceId,string currentQueue)
        {
            ApplicationId = applicationId;
            SolutionSetInstanceId = solutionSetInstanceId;
            CurrentQueue = currentQueue;
        }
        [XmlElement(ElementName = "ApplicationId")]
        public string ApplicationId { get; set; }
        [XmlElement(ElementName = "SolutionSetInstanceId")]
        public string SolutionSetInstanceId { get; set; }
        [XmlElement(ElementName = "CurrentQueue")]
        public string CurrentQueue { get; set; }
    }

    [XmlRoot(ElementName = "Field")]
    public class Field
    {
        public Field()
        {

        }
        public Field(string key,string text)
        {
            Key = key;
            Text = text;
        }
        [XmlAttribute(AttributeName = "key")]
        public string Key { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "ContextData")]
    public class ContextData
    {
        public ContextData()
        {

        }
        public ContextData(List<Field> field)
        {
            Field = field.Select(i=>new Field(i.Key,i.Text)).ToList();
        }
        [XmlElement(ElementName = "Field")]
        public List<Field> Field { get; set; }
    }

    [XmlRoot(ElementName = "DCResponse")]
    public class DCResponse
    {
        public DCResponse()
        {

        }
        public DCResponse(string status, Authentication authentication, ResponseInfo responseInfo, ContextData contextData)
        {
            Status = status;
            Authentication = new Authentication(authentication.Status, authentication.Token);
            ResponseInfo = new ResponseInfo(responseInfo.ApplicationId, responseInfo.SolutionSetInstanceId, responseInfo.CurrentQueue);
            if(contextData.Field!=null)
            ContextData = new ContextData(contextData.Field);
           // ErrorInfo = new ErrorInfo()
        }
        [XmlElement(ElementName = "Status")]
        public string Status { get; set; }
        [XmlElement(ElementName = "Authentication")]
        public Authentication Authentication { get; set; }
        [XmlElement(ElementName = "ResponseInfo")]
        public ResponseInfo ResponseInfo { get; set; }
        [XmlElement(ElementName = "ContextData")]
        public ContextData ContextData { get; set; }
        [XmlElement(ElementName = "ErrorInfo")]
        public ErrorInfo ErrorInfo { get; set; }
        [XmlElement(ElementName = "ExceptionInfo")]
        public ExceptionInfo ExceptionInfo { get; set; }
    }
    [XmlRoot(ElementName = "ErrorInfo")]
    public class ErrorInfo
    {
        public ErrorInfo()
        {

        }
        public ErrorInfo(string message)
        {
            Message = message;
        }
        [XmlElement(ElementName = "Message")]
        public string Message { get; set; }
    }
    [XmlRoot(ElementName = "ExceptionInfo")]
    public class ExceptionInfo
    {
        public ExceptionInfo()
        {

        }
        public ExceptionInfo(string message,string errorCode,string source,string stackTrace)
        {
            Message = message;
            ErrorCode = errorCode;
            Source = source;
            StackTrace = stackTrace;
           
        }
        [XmlElement(ElementName = "Message")]
        public string Message { get; set; }
        [XmlElement(ElementName = "ErrorCode")]
        public string ErrorCode { get; set; }
        [XmlElement(ElementName = "Source")]
        public string Source { get; set; }
        [XmlElement(ElementName = "StackTrace")]
        public string StackTrace { get; set; }
    }
}
