﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace CreditExchange.Syndication.Cibil.Proxy.Response
{
    [XmlRoot(ElementName = "Milestone")]
    public class Milestone
    {
        [XmlElement(ElementName = "Step")]
        public List<string> Step { get; set; }
    }

    [XmlRoot(ElementName = "Reason")]
    public class Reason
    {
        [XmlElement(ElementName = "Applicant")]
        public string Applicant { get; set; }
        [XmlElement(ElementName = "Code")]
        public string Code { get; set; }
        [XmlElement(ElementName = "Type")]
        public string Type { get; set; }
        [XmlElement(ElementName = "Description")]
        public string Description { get; set; }
    }

    [XmlRoot(ElementName = "InputValReasonCodes")]
    public class InputValReasonCodes
    {
        [XmlElement(ElementName = "Reason")]
        public Reason Reason { get; set; }
    }

    [XmlRoot(ElementName = "ApplicationData")]
    public class ApplicationData
    {

     
        [XmlElement(ElementName = "IsMFTUEF")]
        public string IsMFTUEF { get; set; }
        [XmlElement(ElementName = "Product")]
        public string Product { get; set; }
        
        [XmlElement(ElementName = "ExternalApplicationID")]
        public string ExternalApplicationID { get; set; }
        [XmlElement(ElementName = "EnquiryPurpose")]
        public string EnquiryPurpose { get; set; }
        [XmlElement(ElementName = "EnquiryAmount")]
        public string EnquiryAmount { get; set; }
        [XmlElement(ElementName = "BranchReferenceNo")]
        public string BranchReferenceNo { get; set; }
        [XmlElement(ElementName = "CenterReferenceNo")]
        public string CenterReferenceNo { get; set; }
        [XmlElement(ElementName = "MFIMemberCode")]
        public string MFIMemberCode { get; set; }
        [XmlElement(ElementName = "IDVPDFReport")]
        public string IDVPDFReport { get; set; }
        [XmlElement(ElementName = "MFIPDFReport")]
        public string MFIPDFReport { get; set; }
        [XmlElement(ElementName = "CIBILPDFReport")]
        public string CIBILPDFReport { get; set; }
        [XmlElement(ElementName = "MFIBureauFlag")]
        public string MFIBureauFlag { get; set; }
        [XmlElement(ElementName = "IDVerificationFlag")]
        public string IDVerificationFlag { get; set; }
        [XmlElement(ElementName = "DSTuNtcFlag")]
        public string DSTuNtcFlag { get; set; }
        [XmlElement(ElementName = "CibilBureauFlag")]
        public string CibilBureauFlag { get; set; }
        [XmlElement(ElementName = "GSTStateCode")]
        public string GSTStateCode { get; set; }
        [XmlElement(ElementName = "ConsumerConsentForUIDAIAuthentication")]
        public string ConsumerConsentForUIDAIAuthentication { get; set; }
        [XmlElement(ElementName = "IDVMemberCode")]
        public string IDVMemberCode { get; set; }
        [XmlElement(ElementName = "NTCProductType")]
        public string NTCProductType { get; set; }
        [XmlElement(ElementName = "ReferenceNumber")]
        public string ReferenceNumber { get; set; }
        [XmlElement(ElementName = "Amount")]
        public string Amount { get; set; }
        [XmlElement(ElementName = "Purpose")]
        public string Purpose { get; set; }
        [XmlElement(ElementName = "User")]
        public string User { get; set; }
        [XmlElement(ElementName = "BusinessUnitId")]
        public string BusinessUnitId { get; set; }
        [XmlElement(ElementName = "ApplicationId")]
        public string ApplicationId { get; set; }
        [XmlElement(ElementName = "SolutionSetId")]
        public string SolutionSetId { get; set; }
        [XmlElement(ElementName = "EnvironmentTypeId")]
        public string EnvironmentTypeId { get; set; }
        [XmlElement(ElementName = "EnvironmentType")]
        public string EnvironmentType { get; set; }
        [XmlElement(ElementName = "SkipPreQDEFlag")]
        public string SkipPreQDEFlag { get; set; }
        [XmlElement(ElementName = "SkipEkycFlag")]
        public string SkipEkycFlag { get; set; }
        [XmlElement(ElementName = "SkipVelocityCheckFlag")]
        public string SkipVelocityCheckFlag { get; set; }
        [XmlElement(ElementName = "SkipCibilBureauFlag")]
        public string SkipCibilBureauFlag { get; set; }
        [XmlElement(ElementName = "SkipMultiBureauFlag")]
        public string SkipMultiBureauFlag { get; set; }
        [XmlElement(ElementName = "SkipDsCrifIndFlag")]
        public string SkipDsCrifIndFlag { get; set; }
        [XmlElement(ElementName = "SkipDsEverifyFlag")]
        public string SkipDsEverifyFlag { get; set; }
        [XmlElement(ElementName = "SkipTuVerificationFlag")]
        public string SkipTuVerificationFlag { get; set; }
        [XmlElement(ElementName = "SkipCreditRiskPoliciesFlag")]
        public string SkipCreditRiskPoliciesFlag { get; set; }
        [XmlElement(ElementName = "SkipIndiaInputQueueQdeFlag")]
        public string SkipIndiaInputQueueQdeFlag { get; set; }
        [XmlElement(ElementName = "SkipDSTuNtcFlag")]
        public string SkipDSTuNtcFlag { get; set; }
        [XmlElement(ElementName = "SkipCPVPoliciesQDEFlag")]
        public string SkipCPVPoliciesQDEFlag { get; set; }
        [XmlElement(ElementName = "SkipIDVerificationFlag")]
        public string SkipIDVerificationFlag { get; set; }
        [XmlElement(ElementName = "SkipDSTuIDVisionFlag")]
        public string SkipDSTuIDVisionFlag { get; set; }
        [XmlElement(ElementName = "SkipMFICIRPuller")]
        public string SkipMFICIRPuller { get; set; }
        [XmlElement(ElementName = "SkipQDEValidationQ")]
        public string SkipQDEValidationQ { get; set; }
        [XmlElement(ElementName = "SkipQDEResultsQ")]
        public string SkipQDEResultsQ { get; set; }
        [XmlElement(ElementName = "SkipDocVerificationFlag")]
        public string SkipDocVerificationFlag { get; set; }
        [XmlElement(ElementName = "SkipIndiaInputQueueDdeFlag")]
        public string SkipIndiaInputQueueDdeFlag { get; set; }
        [XmlElement(ElementName = "SkipReferQueue")]
        public string SkipReferQueue { get; set; }
        [XmlElement(ElementName = "Milestone")]
        public Milestone Milestone { get; set; }
        [XmlElement(ElementName = "Start")]
        public string Start { get; set; }
        [XmlElement(ElementName = "InputValReasonCodes")]
        public InputValReasonCodes InputValReasonCodes { get; set; }
    }

}
