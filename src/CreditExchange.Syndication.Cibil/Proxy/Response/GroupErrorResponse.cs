﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace CreditExchange.Syndication.Cibil.Proxy.Response
{

    [XmlRoot(ElementName = "Response")]
    public class GroupErrorResponse
    {
        [XmlElement(ElementName = "ErrorCode")]
        public List<string> ErrorCode { get; set; }
        [XmlElement(ElementName = "ErrorDescription")]
        public List<string> ErrorDescription { get; set; }
    }

    [XmlRoot(ElementName = "ErrorSegment")]
    public class GroupErrorSegment
    {
        [XmlElement(ElementName = "Response")]
        public GroupErrorResponse Response { get; set; }
    }

}
