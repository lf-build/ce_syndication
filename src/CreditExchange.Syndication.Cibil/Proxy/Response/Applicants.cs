﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace CreditExchange.Syndication.Cibil.Proxy.Response
{
    [XmlRoot(ElementName = "Address")]
    public class ApplicantAddress
    {
        [XmlElement(ElementName = "StateCode")]
        public string StateCode { get; set; }
        [XmlElement(ElementName = "ResidenceType")]
        public string ResidenceType { get; set; }
        [XmlElement(ElementName = "PinCode")]
        public string PinCode { get; set; }
        [XmlElement(ElementName = "City")]
        public string City { get; set; }
        [XmlElement(ElementName = "AddressType")]
        public string AddressType { get; set; }
        [XmlElement(ElementName = "AddressLine5")]
        public string AddressLine5 { get; set; }
        [XmlElement(ElementName = "AddressLine4")]
        public string AddressLine4 { get; set; }
        [XmlElement(ElementName = "AddressLine3")]
        public string AddressLine3 { get; set; }
        [XmlElement(ElementName = "AddressLine2")]
        public string AddressLine2 { get; set; }
        [XmlElement(ElementName = "AddressLine1")]
        public string AddressLine1 { get; set; }
        [XmlElement(ElementName = "EnrichedThroughEnquiry")]
        public string EnrichedThroughEnquiry { get; set; }
        [XmlElement(ElementName = "Match")]
        public string Match { get; set; }
        [XmlElement(ElementName = "DateReported")]
        public string DateReported { get; set; }
        [XmlElement(ElementName = "SegmentTag")]
        public string SegmentTag { get; set; }
        [XmlElement(ElementName = "FID")]
        public string FID { get; set; }
        [XmlElement(ElementName = "SNo")]
        public string SNo { get; set; }
        [XmlElement(ElementName = "Address")]
        public string Address { get; set; }
        [XmlElement(ElementName = "AddressCategory")]
        public string AddressCategory { get; set; }
    }

    [XmlRoot(ElementName = "Addresses")]
    public class ApplicantAddresses
    {
        [XmlElement(ElementName = "Address")]
        public List<ApplicantAddress> Address { get; set; }
    }

    [XmlRoot(ElementName = "Telephone")]
    public class Telephone
    {
        [XmlElement(ElementName = "TelephoneType")]
        public string TelephoneType { get; set; }
        [XmlElement(ElementName = "TelephoneNumber")]
        public string TelephoneNumber { get; set; }
        [XmlElement(ElementName = "TelephoneExtension")]
        public string TelephoneExtension { get; set; }
        [XmlElement(ElementName = "DateReported")]
        public string DateReported { get; set; }
        [XmlElement(ElementName = "EnrichedThroughEnquiry")]
        public string EnrichedThroughEnquiry { get; set; }
        [XmlElement(ElementName = "Match")]
        public string Match { get; set; }
        [XmlElement(ElementName = "SegmentTag")]
        public string SegmentTag { get; set; }
        [XmlElement(ElementName = "FID")]
        public string FID { get; set; }
        [XmlElement(ElementName = "SNo")]
        public string SNo { get; set; }
    }

    [XmlRoot(ElementName = "Telephones")]
    public class Telephones
    {
        [XmlElement(ElementName = "Telephone")]
        public List<Telephone> Telephone { get; set; }
    }

    [XmlRoot(ElementName = "Identifier")]
    public class Identifier
    {
        [XmlElement(ElementName = "IdType")]
        public string IdType { get; set; }
        [XmlElement(ElementName = "IdNumber")]
        public string IdNumber { get; set; }
        [XmlElement(ElementName = "DateReported")]
        public string DateReported { get; set; }
        [XmlElement(ElementName = "EnrichedThroughEnquiry")]
        public string EnrichedThroughEnquiry { get; set; }
        [XmlElement(ElementName = "FID")]
        public string FID { get; set; }
        [XmlElement(ElementName = "SNo")]
        public string SNo { get; set; }
    }

    [XmlRoot(ElementName = "Identifiers")]
    public class Identifiers
    {
        [XmlElement(ElementName = "Identifier")]
        public List<Identifier> Identifier { get; set; }
    }

    [XmlRoot(ElementName = "DsCibilBureauData")]
    public class DsCibilBureauData
    {
        [XmlElement(ElementName = "Request")]
        public string Request { get; set; }
    }

    [XmlRoot(ElementName = "DsCibilBureauStatus")]
    public class DsCibilBureauStatus
    {
        [XmlElement(ElementName = "Trail")]
        public string Trail { get; set; }
    }

    [XmlRoot(ElementName = "CibilBureauResponse")]
    public class CibilBureauResponse
    {
        [XmlElement(ElementName = "BureauResponseRaw")]
        public string BureauResponseRaw { get; set; }
        [XmlElement(ElementName = "BureauResponseXml")]
        public string BureauResponseXml { get; set; }
        [XmlElement(ElementName = "SecondaryReportXml")]
        public string SecondaryReportXml { get; set; }
        [XmlElement(ElementName = "IsSucess")]
        public string IsSucess { get; set; }
        [XmlElement(ElementName = "ErrorCode")]
        public string ErrorCode { get; set; }
        [XmlElement(ElementName = "ErrorMessage")]
        public string ErrorMessage { get; set; }
      
    }

    [XmlRoot(ElementName = "Response")]
    public class Response
    {
        [XmlElement(ElementName = "CibilBureauResponse")]
        public CibilBureauResponse CibilBureauResponse { get; set; }
        [XmlElement(ElementName = "IsSuccess")]
        public string IsSuccess { get; set; }
        [XmlElement(ElementName = "ErrorCode")]
        public string ErrorCode { get; set; }
        [XmlElement(ElementName = "ErrorMessage")]
        public string ErrorMessage { get; set; }
        [XmlElement(ElementName = "VerificationResponseXml")]
        public string VerificationResponseXml { get; set; }
        [XmlElement(ElementName = "AadharResponse")]
        public string AadharResponse { get; set; }
    }

    [XmlRoot(ElementName = "DsCibilBureau")]
    public class DsCibilBureau
    {
        [XmlElement(ElementName = "DsCibilBureauData")]
        public DsCibilBureauData DsCibilBureauData { get; set; }
        [XmlElement(ElementName = "DsCibilBureauStatus")]
        public DsCibilBureauStatus DsCibilBureauStatus { get; set; }
        [XmlElement(ElementName = "Response")]
        public Response Response { get; set; }
    }

    [XmlRoot(ElementName = "DsTuVerification")]
    public class DsTuVerification
    {
        [XmlElement(ElementName = "Response")]
        public Response Response { get; set; }
    }

    [XmlRoot(ElementName = "DsAadharEkycOtp")]
    public class DsAadharEkycOtp
    {
        [XmlElement(ElementName = "Response")]
        public Response Response { get; set; }
    }

    [XmlRoot(ElementName = "ExceptionMessage")]
    public class ExceptionMessage
    {
        [XmlElement(ElementName = "PANMessage")]
        public string PANMessage { get; set; }
    }

    [XmlRoot(ElementName = "Match")]
    public class Match
    {
        [XmlElement(ElementName = "ContactabilityAadhaarTelephone1Status")]
        public string ContactabilityAadhaarTelephone1Status { get; set; }
        [XmlElement(ElementName = "ContactabilityAadhaarTelephone1Match")]
        public string ContactabilityAadhaarTelephone1Match { get; set; }
        [XmlElement(ElementName = "AddressAadhaarPermanentStatus")]
        public string AddressAadhaarPermanentStatus { get; set; }
        [XmlElement(ElementName = "AddressAadhaarPermanentMatch")]
        public string AddressAadhaarPermanentMatch { get; set; }
        [XmlElement(ElementName = "AddressAadhaarResidenceStatus")]
        public string AddressAadhaarResidenceStatus { get; set; }
        [XmlElement(ElementName = "AddressAadhaarResidenceMatch")]
        public string AddressAadhaarResidenceMatch { get; set; }
        [XmlElement(ElementName = "IDAadhaarIdentifierStatus")]
        public string IDAadhaarIdentifierStatus { get; set; }
        [XmlElement(ElementName = "IDAadhaarIdentifierMatch")]
        public string IDAadhaarIdentifierMatch { get; set; }
        [XmlElement(ElementName = "IDAadharGenderStatus")]
        public string IDAadharGenderStatus { get; set; }
        [XmlElement(ElementName = "IDAadharGenderMatch")]
        public string IDAadharGenderMatch { get; set; }
        [XmlElement(ElementName = "IDAadharDOBYearStatus")]
        public string IDAadharDOBYearStatus { get; set; }
        [XmlElement(ElementName = "IDAadharDOBYearMatch")]
        public string IDAadharDOBYearMatch { get; set; }
        [XmlElement(ElementName = "IDAadharNameStatus")]
        public string IDAadharNameStatus { get; set; }
        [XmlElement(ElementName = "IDAadharNameMatch")]
        public string IDAadharNameMatch { get; set; }
        [XmlElement(ElementName = "AddressVoterPermanentStatus")]
        public string AddressVoterPermanentStatus { get; set; }
        [XmlElement(ElementName = "AddressVoterPermanentMatch")]
        public string AddressVoterPermanentMatch { get; set; }
        [XmlElement(ElementName = "AddressVoterResidenceStatus")]
        public string AddressVoterResidenceStatus { get; set; }
        [XmlElement(ElementName = "AddressVoterResidenceMatch")]
        public string AddressVoterResidenceMatch { get; set; }
        [XmlElement(ElementName = "IDVoterIdentifierStatus")]
        public string IDVoterIdentifierStatus { get; set; }
        [XmlElement(ElementName = "IDVoterIdentifierMatch")]
        public string IDVoterIdentifierMatch { get; set; }
        [XmlElement(ElementName = "IDVoterGenderStatus")]
        public string IDVoterGenderStatus { get; set; }
        [XmlElement(ElementName = "IDVoterGenderMatch")]
        public string IDVoterGenderMatch { get; set; }
        [XmlElement(ElementName = "IDVoterNameStatus")]
        public string IDVoterNameStatus { get; set; }
        [XmlElement(ElementName = "IDVoterNameMatch")]
        public string IDVoterNameMatch { get; set; }
        [XmlElement(ElementName = "IDNSDLIdentifierStatus")]
        public string IDNSDLIdentifierStatus { get; set; }
        [XmlElement(ElementName = "IDNSDLIdentifierMatch")]
        public string IDNSDLIdentifierMatch { get; set; }
        [XmlElement(ElementName = "IDNSDLNameStatus")]
        public string IDNSDLNameStatus { get; set; }
        [XmlElement(ElementName = "IDNSDLNameMatch")]
        public string IDNSDLNameMatch { get; set; }
        [XmlElement(ElementName = "ContactabilityOfficeNumberStatus")]
        public string ContactabilityOfficeNumberStatus { get; set; }
        [XmlElement(ElementName = "ContactabilityOfficeNumberMatch")]
        public string ContactabilityOfficeNumberMatch { get; set; }
        [XmlElement(ElementName = "ContactabilityResidenceNumberStatus")]
        public string ContactabilityResidenceNumberStatus { get; set; }
        [XmlElement(ElementName = "ContactabilityResidenceNumberMatch")]
        public string ContactabilityResidenceNumberMatch { get; set; }
        [XmlElement(ElementName = "ContactabilityMobileNumberStatus")]
        public string ContactabilityMobileNumberStatus { get; set; }
        [XmlElement(ElementName = "ContactabilityMobileNumberMatch")]
        public string ContactabilityMobileNumberMatch { get; set; }
        [XmlElement(ElementName = "AddressCIBILOfficeStatus")]
        public string AddressCIBILOfficeStatus { get; set; }
        [XmlElement(ElementName = "AddressCIBILOfficeMatch")]
        public string AddressCIBILOfficeMatch { get; set; }
        [XmlElement(ElementName = "AddressCIBILPermanentStatus")]
        public string AddressCIBILPermanentStatus { get; set; }
        [XmlElement(ElementName = "AddressCIBILPermanentMatch")]
        public string AddressCIBILPermanentMatch { get; set; }
        [XmlElement(ElementName = "AddressCIBILResidenceStatus")]
        public string AddressCIBILResidenceStatus { get; set; }
        [XmlElement(ElementName = "AddressCIBILResidenceMatch")]
        public string AddressCIBILResidenceMatch { get; set; }
        [XmlElement(ElementName = "IDCIBILDrivingLicenseIDStatus")]
        public string IDCIBILDrivingLicenseIDStatus { get; set; }
        [XmlElement(ElementName = "IDCIBILDrivingLicenseIDMatch")]
        public string IDCIBILDrivingLicenseIDMatch { get; set; }
        [XmlElement(ElementName = "IDCIBILRationCardIDStatus")]
        public string IDCIBILRationCardIDStatus { get; set; }
        [XmlElement(ElementName = "IDCIBILRationCardIDMatch")]
        public string IDCIBILRationCardIDMatch { get; set; }
        [XmlElement(ElementName = "IDCIBILPassportIDStatus")]
        public string IDCIBILPassportIDStatus { get; set; }
        [XmlElement(ElementName = "IDCIBILPassportIDMatch")]
        public string IDCIBILPassportIDMatch { get; set; }
        [XmlElement(ElementName = "IDCIBILAadhaarIDStatus")]
        public string IDCIBILAadhaarIDStatus { get; set; }
        [XmlElement(ElementName = "IDCIBILAadhaarIDMacth")]
        public string IDCIBILAadhaarIDMacth { get; set; }
        [XmlElement(ElementName = "IDCIBILVoterIDStatus")]
        public string IDCIBILVoterIDStatus { get; set; }
        [XmlElement(ElementName = "IDCIBILVoterIDMatch")]
        public string IDCIBILVoterIDMatch { get; set; }
        [XmlElement(ElementName = "IDCIBILPANStatus")]
        public string IDCIBILPANStatus { get; set; }
        [XmlElement(ElementName = "IDCIBILPANMatch")]
        public string IDCIBILPANMatch { get; set; }
        [XmlElement(ElementName = "IDCIBILGenderStatus")]
        public string IDCIBILGenderStatus { get; set; }
        [XmlElement(ElementName = "IDCIBILGenderMatch")]
        public string IDCIBILGenderMatch { get; set; }
        [XmlElement(ElementName = "IDCIBILDOBStatus")]
        public string IDCIBILDOBStatus { get; set; }
        [XmlElement(ElementName = "IDCIBILDOBMatch")]
        public string IDCIBILDOBMatch { get; set; }
        [XmlElement(ElementName = "IDCIBILNameStatus")]
        public string IDCIBILNameStatus { get; set; }
        [XmlElement(ElementName = "IDCIBILNameMatch")]
        public string IDCIBILNameMatch { get; set; }
        [XmlElement(ElementName = "ExceptionMessage")]
        public ExceptionMessage ExceptionMessage { get; set; }
    }

    [XmlRoot(ElementName = "EnquiryInfo")]
    public class EnquiryInfo
    {
        [XmlElement(ElementName = "MobileNumber")]
        public string MobileNumber { get; set; }
        [XmlElement(ElementName = "PermanentAddressCity")]
        public string PermanentAddressCity { get; set; }
        [XmlElement(ElementName = "PermanentAddressState")]
        public string PermanentAddressState { get; set; }
        [XmlElement(ElementName = "PermanentAddressStateCode")]
        public string PermanentAddressStateCode { get; set; }
        [XmlElement(ElementName = "PermanentAddressPinCode")]
        public string PermanentAddressPinCode { get; set; }
        [XmlElement(ElementName = "PermanentAddressLine")]
        public string PermanentAddressLine { get; set; }
        [XmlElement(ElementName = "PAN")]
        public string PAN { get; set; }
        [XmlElement(ElementName = "EmailID")]
        public string EmailID { get; set; }
        [XmlElement(ElementName = "DateofBirth")]
        public string DateofBirth { get; set; }
        [XmlElement(ElementName = "Gender")]
        public string Gender { get; set; }
        [XmlElement(ElementName = "LastName")]
        public string LastName { get; set; }
        [XmlElement(ElementName = "MiddleName")]
        public string MiddleName { get; set; }
        [XmlElement(ElementName = "FirstName")]
        public string FirstName { get; set; }
    }

    [XmlRoot(ElementName = "CIBILReport")]
    public class CIBILReport
    {
        [XmlElement(ElementName = "Telephones")]
        public Telephones Telephones { get; set; }
        [XmlElement(ElementName = "Addresses")]
        public Addresses Addresses { get; set; }
        [XmlElement(ElementName = "Identifiers")]
        public Identifiers Identifiers { get; set; }
        [XmlElement(ElementName = "FID")]
        public string FID { get; set; }
        [XmlElement(ElementName = "Gender")]
        public string Gender { get; set; }
        [XmlElement(ElementName = "DateOfBirth")]
        public string DateOfBirth { get; set; }
        [XmlElement(ElementName = "EnquiryControlNumber")]
        public string EnquiryControlNumber { get; set; }
        [XmlElement(ElementName = "ConsumerName")]
        public string ConsumerName { get; set; }
    }

    [XmlRoot(ElementName = "VerificationScore")]
    public class VerificationScore
    {
        [XmlElement(ElementName = "IDNameScore")]
        public string IDNameScore { get; set; }
        [XmlElement(ElementName = "IDNameStatus")]
        public string IDNameStatus { get; set; }
        [XmlElement(ElementName = "IDAltNameScore")]
        public string IDAltNameScore { get; set; }
        [XmlElement(ElementName = "IDAltDOBScore")]
        public string IDAltDOBScore { get; set; }
        [XmlElement(ElementName = "IDDOBScore")]
        public string IDDOBScore { get; set; }
        [XmlElement(ElementName = "IDDOBStatus")]
        public string IDDOBStatus { get; set; }
        [XmlElement(ElementName = "IDAltGenderScore")]
        public string IDAltGenderScore { get; set; }
        [XmlElement(ElementName = "IDGenderScore")]
        public string IDGenderScore { get; set; }
        [XmlElement(ElementName = "IDGenderStatus")]
        public string IDGenderStatus { get; set; }
        [XmlElement(ElementName = "IDAltIdentifierScore")]
        public string IDAltIdentifierScore { get; set; }
        [XmlElement(ElementName = "IDIdentifierScore")]
        public string IDIdentifierScore { get; set; }
        [XmlElement(ElementName = "IDIdentifierStatus")]
        public string IDIdentifierStatus { get; set; }
        [XmlElement(ElementName = "AddAltResScore")]
        public string AddAltResScore { get; set; }
        [XmlElement(ElementName = "AddressResidenceScore")]
        public string AddressResidenceScore { get; set; }
        [XmlElement(ElementName = "AddressResidenceStatus")]
        public string AddressResidenceStatus { get; set; }
        [XmlElement(ElementName = "AddAltPerScore")]
        public string AddAltPerScore { get; set; }
        [XmlElement(ElementName = "AddressPermanentScore")]
        public string AddressPermanentScore { get; set; }
        [XmlElement(ElementName = "AddressPermanentStatus")]
        public string AddressPermanentStatus { get; set; }
        [XmlElement(ElementName = "AddressOfficeScore")]
        public string AddressOfficeScore { get; set; }
        [XmlElement(ElementName = "AddressOfficeStatus")]
        public string AddressOfficeStatus { get; set; }
        [XmlElement(ElementName = "ConAltPhoneScore")]
        public string ConAltPhoneScore { get; set; }
        [XmlElement(ElementName = "ContactabilityTelephone1Score")]
        public string ContactabilityTelephone1Score { get; set; }
        [XmlElement(ElementName = "ContactabilityTelephone1Status")]
        public string ContactabilityTelephone1Status { get; set; }
        [XmlElement(ElementName = "ContactabilityTelephone2Score")]
        public string ContactabilityTelephone2Score { get; set; }
        [XmlElement(ElementName = "ContactabilityTelephone2Status")]
        public string ContactabilityTelephone2Status { get; set; }
        [XmlElement(ElementName = "ContactabilityTelephone3Score")]
        public string ContactabilityTelephone3Score { get; set; }
        [XmlElement(ElementName = "ContactabilityTelephone3Status")]
        public string ContactabilityTelephone3Status { get; set; }
        [XmlElement(ElementName = "FinalIdentityScore")]
        public string FinalIdentityScore { get; set; }
        [XmlElement(ElementName = "FinalIdentityStatus")]
        public string FinalIdentityStatus { get; set; }
        [XmlElement(ElementName = "FinalAddressScore")]
        public string FinalAddressScore { get; set; }
        [XmlElement(ElementName = "FinalAddressStatus")]
        public string FinalAddressStatus { get; set; }
        [XmlElement(ElementName = "FinalContactabilityScore")]
        public string FinalContactabilityScore { get; set; }
        [XmlElement(ElementName = "FinalContactabilityStatus")]
        public string FinalContactabilityStatus { get; set; }
        [XmlElement(ElementName = "FinalVerificationScore")]
        public string FinalVerificationScore { get; set; }
        [XmlElement(ElementName = "FinalVerificationStatus")]
        public string FinalVerificationStatus { get; set; }
    }

    [XmlRoot(ElementName = "Responseheader")]
    public class Responseheader
    {
        [XmlElement(ElementName = "ProcessDate")]
        public string ProcessDate { get; set; }
        [XmlElement(ElementName = "ErrorCount")]
        public string ErrorCount { get; set; }
        [XmlElement(ElementName = "NoHitCount")]
        public string NoHitCount { get; set; }
        [XmlElement(ElementName = "HitCount")]
        public string HitCount { get; set; }
    }

    [XmlRoot(ElementName = "CIBILDetect")]
    public class CIBILDetect
    {
        [XmlElement(ElementName = "Responseheader")]
        public Responseheader Responseheader { get; set; }
    }

    [XmlRoot(ElementName = "ProductInfo1month")]
    public class ProductInfo1month
    {
        [XmlElement(ElementName = "NoOfEnquiries")]
        public string NoOfEnquiries { get; set; }
        [XmlElement(ElementName = "ProductType")]
        public string ProductType { get; set; }
        [XmlElement(ElementName = "AvgECRValue")]
        public string AvgECRValue { get; set; }
        [XmlElement(ElementName = "ECRValue")]
        public string ECRValue { get; set; }
        [XmlElement(ElementName = "NoOfTradeLinesOpened")]
        public string NoOfTradeLinesOpened { get; set; }
    }

    [XmlRoot(ElementName = "ProductInfo3months")]
    public class ProductInfo3months
    {
        [XmlElement(ElementName = "NoOfEnquiries")]
        public string NoOfEnquiries { get; set; }
        [XmlElement(ElementName = "ProductType")]
        public string ProductType { get; set; }
        [XmlElement(ElementName = "AvgECRValue")]
        public string AvgECRValue { get; set; }
        [XmlElement(ElementName = "ECRValue")]
        public string ECRValue { get; set; }
        [XmlElement(ElementName = "NoOfTradeLinesOpened")]
        public string NoOfTradeLinesOpened { get; set; }
    }

    [XmlRoot(ElementName = "ProductInfo6months")]
    public class ProductInfo6months
    {
        [XmlElement(ElementName = "NoOfEnquiries")]
        public string NoOfEnquiries { get; set; }
        [XmlElement(ElementName = "ProductType")]
        public string ProductType { get; set; }
        [XmlElement(ElementName = "AvgECRValue")]
        public string AvgECRValue { get; set; }
        [XmlElement(ElementName = "ECRValue")]
        public string ECRValue { get; set; }
        [XmlElement(ElementName = "NoOfTradeLinesOpened")]
        public string NoOfTradeLinesOpened { get; set; }
    }

    [XmlRoot(ElementName = "Velocity")]
    public class Velocity
    {
        [XmlElement(ElementName = "ProductInfo1month")]
        public List<ProductInfo1month> ProductInfo1month { get; set; }
        [XmlElement(ElementName = "ProductInfo3months")]
        public List<ProductInfo3months> ProductInfo3months { get; set; }
        [XmlElement(ElementName = "ProductInfo6months")]
        public List<ProductInfo6months> ProductInfo6months { get; set; }
    }

    [XmlRoot(ElementName = "ECR")]
    public class ECR
    {
        [XmlElement(ElementName = "ProductInfo1month")]
        public List<ProductInfo1month> ProductInfo1month { get; set; }
        [XmlElement(ElementName = "ProductInfo3months")]
        public List<ProductInfo3months> ProductInfo3months { get; set; }
        [XmlElement(ElementName = "ProductInfo6months")]
        public List<ProductInfo6months> ProductInfo6months { get; set; }
    }

    [XmlRoot(ElementName = "AverageECR1month")]
    public class AverageECR1month
    {
        [XmlElement(ElementName = "AVGECR")]
        public string AVGECR { get; set; }
        [XmlElement(ElementName = "ProductType")]
        public string ProductType { get; set; }
    }

    [XmlRoot(ElementName = "AverageECR3months")]
    public class AverageECR3months
    {
        [XmlElement(ElementName = "AVGECR")]
        public string AVGECR { get; set; }
        [XmlElement(ElementName = "ProductType")]
        public string ProductType { get; set; }
    }

    [XmlRoot(ElementName = "AverageECR6months")]
    public class AverageECR6months
    {
        [XmlElement(ElementName = "AVGECR")]
        public string AVGECR { get; set; }
        [XmlElement(ElementName = "ProductType")]
        public string ProductType { get; set; }
    }

    [XmlRoot(ElementName = "AverageECR")]
    public class AverageECR
    {
        [XmlElement(ElementName = "AverageECR1month")]
        public List<AverageECR1month> AverageECR1month { get; set; }
        [XmlElement(ElementName = "AverageECR3months")]
        public List<AverageECR3months> AverageECR3months { get; set; }
        [XmlElement(ElementName = "AverageECR6months")]
        public List<AverageECR6months> AverageECR6months { get; set; }
    }

    [XmlRoot(ElementName = "SuitfiledandWilfuldefault")]
    public class SuitfiledandWilfuldefault
    {
        [XmlElement(ElementName = "Status")]
        public string Status { get; set; }
    }

    [XmlRoot(ElementName = "WilfulDefault")]
    public class WilfulDefault
    {
        [XmlElement(ElementName = "Status")]
        public string Status { get; set; }
    }

    [XmlRoot(ElementName = "SuitFiled")]
    public class SuitFiled
    {
        [XmlElement(ElementName = "Status")]
        public string Status { get; set; }
    }

    [XmlRoot(ElementName = "WilfulDefaultDetails")]
    public class WilfulDefaultDetails
    {
        [XmlElement(ElementName = "SuitfiledandWilfuldefault")]
        public SuitfiledandWilfuldefault SuitfiledandWilfuldefault { get; set; }
        [XmlElement(ElementName = "WilfulDefault")]
        public WilfulDefault WilfulDefault { get; set; }
        [XmlElement(ElementName = "SuitFiled")]
        public SuitFiled SuitFiled { get; set; }
    }
    [XmlRoot(ElementName = "PAN")]
    public class PANAttribute
    {
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "ID")]
        public string ID { get; set; }
    }
    [XmlRoot(ElementName = "CPVAttributes")]
    public class CPVAttributes
    {
        [XmlElement(ElementName = "Match")]
        public Match Match { get; set; }
        [XmlElement(ElementName = "EnquiryInfo")]
        public EnquiryInfo EnquiryInfo { get; set; }
        [XmlElement(ElementName = "CIBILReport")]
        public CIBILReport CIBILReport { get; set; }
        [XmlElement(ElementName = "VerificationScore")]
        public VerificationScore VerificationScore { get; set; }
        [XmlElement(ElementName = "CIBILDetect")]
        public CIBILDetect CIBILDetect { get; set; }
        [XmlElement(ElementName = "Velocity")]
        public Velocity Velocity { get; set; }
        [XmlElement(ElementName = "ECR")]
        public ECR ECR { get; set; }
        [XmlElement(ElementName = "AverageECR")]
        public AverageECR AverageECR { get; set; }
        [XmlElement(ElementName = "WilfulDefaultDetails")]
        public WilfulDefaultDetails WilfulDefaultDetails { get; set; }
        [XmlElement(ElementName = "Pan")]
        public PANAttribute Pan { get; set; }

    }

    [XmlRoot(ElementName = "DsIDVision")]
    public class DsIDVision
    {
        [XmlElement(ElementName = "CPVAttributes")]
        public CPVAttributes CPVAttributes { get; set; }
        [XmlElement(ElementName = "ReturnMessage")]
        public string ReturnMessage { get; set; }
    }

    [XmlRoot(ElementName = "DsTuNtcStatus")]
    public class DsTuNtcStatus
    {
        [XmlElement(ElementName = "Trail")]
        public string Trail { get; set; }
        [XmlElement(ElementName = "IsSuccess")]
        public string IsSuccess { get; set; }
        [XmlElement(ElementName = "ErrorCode")]
        public string ErrorCode { get; set; }
        [XmlElement(ElementName = "ErrorMessage")]
        public string ErrorMessage { get; set; }
        [XmlElement(ElementName = "Score")]
        public string Score { get; set; }
        [XmlElement(ElementName = "RiskBand")]
        public string RiskBand { get; set; }
    }

    [XmlRoot(ElementName = "DsTuNtcData")]
    public class DsTuNtcData
    {
        [XmlElement(ElementName = "Account_Type")]
        public string Account_Type { get; set; }
        [XmlElement(ElementName = "NTC_EnvironmentType")]
        public string NTC_EnvironmentType { get; set; }
        [XmlElement(ElementName = "ExternalApplicationId")]
        public string ExternalApplicationId { get; set; }
        [XmlElement(ElementName = "MemberCode")]
        public string MemberCode { get; set; }
        [XmlElement(ElementName = "Pin")]
        public string Pin { get; set; }
        [XmlElement(ElementName = "DateOfBirth")]
        public string DateOfBirth { get; set; }
        [XmlElement(ElementName = "Gender")]
        public string Gender { get; set; }
        [XmlElement(ElementName = "Occupation_Code")]
        public string Occupation_Code { get; set; }
        [XmlElement(ElementName = "Phone_Type")]
        public string Phone_Type { get; set; }
        [XmlElement(ElementName = "Id_Type")]
        public string Id_Type { get; set; }
        [XmlElement(ElementName = "DL")]
        public string DL { get; set; }
        [XmlElement(ElementName = "NC")]
        public string NC { get; set; }
        [XmlElement(ElementName = "CIR_Report")]
        public string CIR_Report { get; set; }
        [XmlElement(ElementName = "Review_Date")]
        public string Review_Date { get; set; }
        [XmlElement(ElementName = "Validation_Error")]
        public string Validation_Error { get; set; }
    }

    [XmlRoot(ElementName = "DsTuNtc")]
    public class DsTuNtc
    {
        [XmlElement(ElementName = "DsTuNtcStatus")]
        public DsTuNtcStatus DsTuNtcStatus { get; set; }
        [XmlElement(ElementName = "DsTuNtcData")]
        public DsTuNtcData DsTuNtcData { get; set; }
    }

    [XmlRoot(ElementName = "Applicant")]
    public class Applicant
    {
        [XmlElement(ElementName = "Addresses")]
        public ApplicantAddresses Addresses { get; set; }
        [XmlElement(ElementName = "Telephones")]
        public Telephones Telephones { get; set; }
        [XmlElement(ElementName = "Identifiers")]
        public Identifiers Identifiers { get; set; }
        [XmlElement(ElementName = "Gender")]
        public string Gender { get; set; }
        [XmlElement(ElementName = "DateOfBirth")]
        public string DateOfBirth { get; set; }
        [XmlElement(ElementName = "ApplicantLastName")]
        public string ApplicantLastName { get; set; }
        [XmlElement(ElementName = "ApplicantMiddleName")]
        public string ApplicantMiddleName { get; set; }
        [XmlElement(ElementName = "ApplicantFirstName")]
        public string ApplicantFirstName { get; set; }
        [XmlElement(ElementName = "ApplicantType")]
        public string ApplicantType { get; set; }
        [XmlElement(ElementName = "DsCibilBureau")]
        public DsCibilBureau DsCibilBureau { get; set; }
        [XmlElement(ElementName = "DsTuVerification")]
        public DsTuVerification DsTuVerification { get; set; }
        [XmlElement(ElementName = "DsAadharEkycOtp")]
        public DsAadharEkycOtp DsAadharEkycOtp { get; set; }
        [XmlElement(ElementName = "DsIDVision")]
        public DsIDVision DsIDVision { get; set; }
        [XmlElement(ElementName = "DsTuNtc")]
        public DsTuNtc DsTuNtc { get; set; }
    }

    [XmlRoot(ElementName = "Applicants")]
    public class Applicants
    {
        [XmlElement(ElementName = "Applicant")]
        public Applicant Applicant { get; set; }
    }


}
