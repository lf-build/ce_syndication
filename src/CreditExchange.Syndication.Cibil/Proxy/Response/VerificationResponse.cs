﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace CreditExchange.Syndication.Cibil.Proxy.Response
{
  
    [XmlRoot(ElementName = "data")]
    public class Data
    {
        [XmlElement(ElementName = "ClientId")]
        public string ClientId { get; set; }
        [XmlElement(ElementName = "ApplicationId")]
        public string ApplicationId { get; set; }
        [XmlElement(ElementName = "FirstName")]
        public string FirstName { get; set; }
        [XmlElement(ElementName = "MiddleName")]
        public string MiddleName { get; set; }
        [XmlElement(ElementName = "LastName")]
        public string LastName { get; set; }
        [XmlElement(ElementName = "Gender")]
        public string Gender { get; set; }
        [XmlElement(ElementName = "DateofBirth")]
        public string DateofBirth { get; set; }
        [XmlElement(ElementName = "PAN")]
        public string PAN { get; set; }
        [XmlElement(ElementName = "VoterId")]
        public string VoterId { get; set; }
        [XmlElement(ElementName = "UID")]
        public string UID { get; set; }
        [XmlElement(ElementName = "DrivingLicense")]
        public string DrivingLicense { get; set; }
        [XmlElement(ElementName = "RationCard")]
        public string RationCard { get; set; }
        [XmlElement(ElementName = "Passport")]
        public string Passport { get; set; }
        [XmlElement(ElementName = "Telephone1")]
        public string Telephone1 { get; set; }
        [XmlElement(ElementName = "Telephone2")]
        public string Telephone2 { get; set; }
        [XmlElement(ElementName = "Telephone3")]
        public string Telephone3 { get; set; }
        [XmlElement(ElementName = "Telephone4")]
        public string Telephone4 { get; set; }
        [XmlElement(ElementName = "Address1Line")]
        public string Address1Line { get; set; }
        [XmlElement(ElementName = "Address1Pincd")]
        public string Address1Pincd { get; set; }
        [XmlElement(ElementName = "Address1Statecd")]
        public string Address1Statecd { get; set; }
        [XmlElement(ElementName = "Address1State")]
        public string Address1State { get; set; }
        [XmlElement(ElementName = "Add1City")]
        public string Add1City { get; set; }
        [XmlElement(ElementName = "Address2Line")]
        public string Address2Line { get; set; }
        [XmlElement(ElementName = "Address2Pincd")]
        public string Address2Pincd { get; set; }
        [XmlElement(ElementName = "Address2Statecd")]
        public string Address2Statecd { get; set; }
        [XmlElement(ElementName = "Address2State")]
        public string Address2State { get; set; }
    }

    [XmlRoot(ElementName = "Name")]
    public class Name
    {
        [XmlElement(ElementName = "FirstName")]
        public string FirstName { get; set; }
        [XmlElement(ElementName = "MiddleName")]
        public string MiddleName { get; set; }
        [XmlElement(ElementName = "LastName")]
        public string LastName { get; set; }
        [XmlElement(ElementName = "NameMatch")]
        public string NameMatch { get; set; }
    }

    [XmlRoot(ElementName = "Address")]
    public class Address
    {
        [XmlElement(ElementName = "AddressLine1")]
        public string AddressLine1 { get; set; }
        [XmlElement(ElementName = "PinCode")]
        public string PinCode { get; set; }
        [XmlElement(ElementName = "StateCode")]
        public string StateCode { get; set; }
        [XmlElement(ElementName = "Match")]
        public string Match { get; set; }
        [XmlElement(ElementName = "Match1")]
        public string Match1 { get; set; }
        [XmlElement(ElementName = "AddressCategory")]
        public string AddressCategory { get; set; }
        [XmlElement(ElementName = "DateReported")]
        public string DateReported { get; set; }
    }

    [XmlRoot(ElementName = "Addresses")]
    public class Addresses
    {
        [XmlElement(ElementName = "Address")]
        public List<Address> Address { get; set; }
    }

    [XmlRoot(ElementName = "Telephone")]
    public class VerificationResponseTelephone
    {
        [XmlElement(ElementName = "Number")]
        public string Number { get; set; }
        [XmlElement(ElementName = "Type")]
        public string Type { get; set; }
        [XmlElement(ElementName = "Match")]
        public string Match { get; set; }
    }

    [XmlRoot(ElementName = "PhoneNumber")]
    public class PhoneNumber
    {
        [XmlElement(ElementName = "Telephone")]
        public VerificationResponseTelephone Telephone { get; set; }
    }

    [XmlRoot(ElementName = "eKYC")]
    public class EKYC
    {
        [XmlElement(ElementName = "Source")]
        public string Source { get; set; }
        [XmlElement(ElementName = "photo")]
        public string Photo { get; set; }
        [XmlElement(ElementName = "Name")]
        public Name Name { get; set; }
        [XmlElement(ElementName = "Gender")]
        public string Gender { get; set; }
        [XmlElement(ElementName = "GenderMatch")]
        public string GenderMatch { get; set; }
        [XmlElement(ElementName = "DateofBirth")]
        public string DateofBirth { get; set; }
        [XmlElement(ElementName = "DateofBirthMatch")]
        public string DateofBirthMatch { get; set; }
        [XmlElement(ElementName = "PANMatch")]
        public string PANMatch { get; set; }
        [XmlElement(ElementName = "VoterIdMatch")]
        public string VoterIdMatch { get; set; }
        [XmlElement(ElementName = "PassportMatch")]
        public string PassportMatch { get; set; }
        [XmlElement(ElementName = "DrivingLicenseMatch")]
        public string DrivingLicenseMatch { get; set; }
        [XmlElement(ElementName = "AadharNumber")]
        public string AadharNumber { get; set; }
        [XmlElement(ElementName = "AadharNumberMatch")]
        public string AadharNumberMatch { get; set; }
        [XmlElement(ElementName = "RationCardMatch")]
        public string RationCardMatch { get; set; }
        [XmlElement(ElementName = "Addresses")]
        public Addresses Addresses { get; set; }
        [XmlElement(ElementName = "PhoneNumber")]
        public PhoneNumber PhoneNumber { get; set; }
    }

    [XmlRoot(ElementName = "VoterId")]
    public class VoterId
    {
        [XmlElement(ElementName = "Id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "Source")]
        public string Source { get; set; }
        [XmlElement(ElementName = "ErrorCode")]
        public string ErrorCode { get; set; }
        [XmlElement(ElementName = "TimeTrail")]
        public string TimeTrail { get; set; }
    }

    [XmlRoot(ElementName = "AadhaarIdBA")]
    public class AadhaarIdBA
    {
        [XmlElement(ElementName = "Id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "Source")]
        public string Source { get; set; }
        [XmlElement(ElementName = "ErrorCode")]
        public string ErrorCode { get; set; }
        [XmlElement(ElementName = "POI")]
        public string POI { get; set; }
        [XmlElement(ElementName = "POA")]
        public string POA { get; set; }
        [XmlElement(ElementName = "PFA")]
        public string PFA { get; set; }
        [XmlElement(ElementName = "TimeTrail")]
        public string TimeTrail { get; set; }
        [XmlElement(ElementName = "POINameMatch")]
        public string POINameMatch { get; set; }
        [XmlElement(ElementName = "POIGenderMatch")]
        public string POIGenderMatch { get; set; }
        [XmlElement(ElementName = "POIDOBMatch")]
        public string POIDOBMatch { get; set; }
        [XmlElement(ElementName = "POAStateMatch")]
        public string POAStateMatch { get; set; }
        [XmlElement(ElementName = "POAPincodeMatch")]
        public string POAPincodeMatch { get; set; }
    }

    [XmlRoot(ElementName = "DataSource")]
    public class DataSource
    {
        [XmlElement(ElementName = "source")]
        public string Source { get; set; }
        [XmlElement(ElementName = "FirstName")]
        public string FirstName { get; set; }
        [XmlElement(ElementName = "MiddleName")]
        public string MiddleName { get; set; }
        [XmlElement(ElementName = "LastName")]
        public string LastName { get; set; }
        [XmlElement(ElementName = "NameMatch")]
        public string NameMatch { get; set; }
        [XmlElement(ElementName = "Gender")]
        public string Gender { get; set; }
        [XmlElement(ElementName = "GenderMatch")]
        public string GenderMatch { get; set; }
        [XmlElement(ElementName = "DateofBirth")]
        public string DateofBirth { get; set; }
        [XmlElement(ElementName = "DateofBirthMatch")]
        public string DateofBirthMatch { get; set; }
        [XmlElement(ElementName = "PAN")]
        public string PAN { get; set; }
        [XmlElement(ElementName = "PANMatch")]
        public string PANMatch { get; set; }
        [XmlElement(ElementName = "VoterId")]
        public string VoterId { get; set; }
        [XmlElement(ElementName = "VoterIdMatch")]
        public string VoterIdMatch { get; set; }
        [XmlElement(ElementName = "UID")]
        public string UID { get; set; }
        [XmlElement(ElementName = "UIDMatch")]
        public string UIDMatch { get; set; }
        [XmlElement(ElementName = "DrivingLicense")]
        public string DrivingLicense { get; set; }
        [XmlElement(ElementName = "DrivingLicenseMatch")]
        public string DrivingLicenseMatch { get; set; }
        [XmlElement(ElementName = "RationCard")]
        public string RationCard { get; set; }
        [XmlElement(ElementName = "RationCardMatch")]
        public string RationCardMatch { get; set; }
        [XmlElement(ElementName = "Passport")]
        public string Passport { get; set; }
        [XmlElement(ElementName = "PassportMatch")]
        public string PassportMatch { get; set; }
        [XmlElement(ElementName = "Telephone1")]
        public string Telephone1 { get; set; }
        [XmlElement(ElementName = "Telephone1Match")]
        public string Telephone1Match { get; set; }
        [XmlElement(ElementName = "Telephone2")]
        public string Telephone2 { get; set; }
        [XmlElement(ElementName = "Telephone2Match")]
        public string Telephone2Match { get; set; }
        [XmlElement(ElementName = "Telephone3")]
        public string Telephone3 { get; set; }
        [XmlElement(ElementName = "Telephone3Match")]
        public string Telephone3Match { get; set; }
        [XmlElement(ElementName = "Telephone4")]
        public string Telephone4 { get; set; }
        [XmlElement(ElementName = "Telephone4Match")]
        public string Telephone4Match { get; set; }
        [XmlElement(ElementName = "Addresses")]
        public Addresses Address { get; set; }
    }

    [XmlRoot(ElementName = "PAN")]
    public class PAN
    {
        [XmlElement(ElementName = "PANName")]
        public string PANName { get; set; }
        [XmlElement(ElementName = "PANNameMatch")]
        public string PANNameMatch { get; set; }
        [XmlElement(ElementName = "PANNo")]
        public string PANNo { get; set; }
        [XmlElement(ElementName = "PANNoMatch")]
        public string PANNoMatch { get; set; }
    }

    [XmlRoot(ElementName = "CIBIL")]
    public class CIBIL
    {
        [XmlElement(ElementName = "CibilName")]
        public string CibilName { get; set; }
        [XmlElement(ElementName = "CibilNameMatch")]
        public string CibilNameMatch { get; set; }
        [XmlElement(ElementName = "CIBILGender")]
        public string CIBILGender { get; set; }
        [XmlElement(ElementName = "CIBILGenderMatch")]
        public string CIBILGenderMatch { get; set; }
        [XmlElement(ElementName = "CIBILPassport")]
        public string CIBILPassport { get; set; }
        [XmlElement(ElementName = "CIBILPassportMatch")]
        public string CIBILPassportMatch { get; set; }
        [XmlElement(ElementName = "CIBILDrivingLicense")]
        public string CIBILDrivingLicense { get; set; }
        [XmlElement(ElementName = "CIBILDrivingLicenseMatch")]
        public string CIBILDrivingLicenseMatch { get; set; }
        [XmlElement(ElementName = "CIBILAadharNumber")]
        public string CIBILAadharNumber { get; set; }
        [XmlElement(ElementName = "CIBILAadharNumberMatch")]
        public string CIBILAadharNumberMatch { get; set; }
        [XmlElement(ElementName = "CIBILRationCard")]
        public string CIBILRationCard { get; set; }
        [XmlElement(ElementName = "CIBILRationCardMatch")]
        public string CIBILRationCardMatch { get; set; }
        [XmlElement(ElementName = "CIBILVoterID")]
        public string CIBILVoterID { get; set; }
        [XmlElement(ElementName = "CIBILVoterIDMatch")]
        public string CIBILVoterIDMatch { get; set; }
        [XmlElement(ElementName = "CibilAddress1")]
        public string CibilAddress1 { get; set; }
        [XmlElement(ElementName = "CibilAddress1Match")]
        public string CibilAddress1Match { get; set; }
        [XmlElement(ElementName = "CibilAddress2")]
        public string CibilAddress2 { get; set; }
        [XmlElement(ElementName = "CibilAddress2Match")]
        public string CibilAddress2Match { get; set; }
        [XmlElement(ElementName = "CibilPhone1")]
        public string CibilPhone1 { get; set; }
        [XmlElement(ElementName = "CibilPhone1Match")]
        public string CibilPhone1Match { get; set; }
        [XmlElement(ElementName = "CibilPhone2")]
        public string CibilPhone2 { get; set; }
        [XmlElement(ElementName = "CibilPhone2Match")]
        public string CibilPhone2Match { get; set; }
        [XmlElement(ElementName = "CibilPhone3")]
        public string CibilPhone3 { get; set; }
        [XmlElement(ElementName = "CibilPhone3Match")]
        public string CibilPhone3Match { get; set; }
        [XmlElement(ElementName = "CibilPhone4")]
        public string CibilPhone4 { get; set; }
        [XmlElement(ElementName = "CibilPhone4Match")]
        public string CibilPhone4Match { get; set; }
        [XmlElement(ElementName = "CibilDOB")]
        public string CibilDOB { get; set; }
        [XmlElement(ElementName = "CibilDOBMatch")]
        public string CibilDOBMatch { get; set; }
    }

    [XmlRoot(ElementName = "Output")]
    public class Output
    {
        [XmlElement(ElementName = "data")]
        public Data Data { get; set; }
        [XmlElement(ElementName = "eKYC")]
        public EKYC EKYC { get; set; }
        [XmlElement(ElementName = "VoterId")]
        public VoterId VoterId { get; set; }
        [XmlElement(ElementName = "AadhaarIdBA")]
        public AadhaarIdBA AadhaarIdBA { get; set; }
        [XmlElement(ElementName = "DataSource")]
        public DataSource DataSource { get; set; }
        [XmlElement(ElementName = "PAN")]
        public PAN PAN { get; set; }
        [XmlElement(ElementName = "CIBIL")]
        public CIBIL CIBIL { get; set; }
    }
}
