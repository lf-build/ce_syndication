﻿using System.Xml.Serialization;

namespace CreditExchange.Syndication.Cibil.Proxy.Response
{
    [XmlRoot(ElementName = "ReturnHTMLResponse", Namespace = "http://tempuri.org/")]
    public class ReturnHTMLResponse
    {
        [XmlElement(ElementName = "ReturnHTMLResult", Namespace = "http://tempuri.org/")]
        public string ReturnHTMLResult { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }
}
