﻿using CreditExchange.Syndication.Cibil.Request;
using System;

using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace CreditExchange.Syndication.Cibil.Proxy.Request
{
    public partial class DCRequest
    {

        public DCRequest()
        {

        }
        private string WrapInCData(string @string)
        {
            return "<![CDATA[" + @string + "]]>";
        }
        public DCRequest(ICibilConfiguration configuration, IReportRequest reportRequest)
        {
            Authentication = new Authentication(configuration.UserId, configuration.Password, configuration.Authentication);
            RequestInfo = new RequestInfo(configuration.SolutionSetId, configuration.SolutionSetVersion, configuration.ExecutionMode,configuration.EnvironmentId);
            var fieldlist = new List<Field>();
            var applicaitondata = new ApplicationData(configuration, reportRequest);

           var applicants = new Applicants();
            var applicant = new List<Applicant>();
            applicant.Add(new Applicant(reportRequest));
            applicants.Applicant = applicant;

            var applicaitondataxml = XmlSerialization.Serialize(applicaitondata, "", "", true);
            var applicantsxml = XmlSerialization.Serialize(applicants, "", "", true);

            fieldlist.Add(new Field("ApplicationData",WrapInCData(applicaitondataxml)));
            fieldlist.Add(new Field("Applicants", WrapInCData(applicantsxml)));
            fieldlist.Add(new Field("FinalTraceLevel", "2"));
            Fields = new Fields { Field = fieldlist };

        }
        public string Removeunwantedcharacter(string input, string fieldtype = "name")
        {
            if (!string.IsNullOrEmpty(input))
            {
                //string[] diallowchatacter = new string[] { "/", "\"", "(", ")", "[", "]", "{", "}", ">", "<", "#", "@", "~", "!", "$", "%", "^", "&", "*", "=", "|", "?", "+" };
                //for (int i = 0; i < diallowchatacter.Length; i++)
                //{
                //    input = input.Replace(diallowchatacter[i], " ");
                //}
                if (fieldtype == "name")
                {
                    input = Regex.Replace(input, @"[^0-9a-zA-Z]+", "");
                }
                else if (fieldtype == "address")
                {
                    input = Regex.Replace(input, @"[^0-9a-zA-Z]+", " ");
                }
            }

            return input;
        }

    }

    public partial class Authentication
    {
        public Authentication()
        {

        }
        public Authentication(string userId, string password, string type)
        {
            UserId = userId;
            Password = password;
            Type = type;
        }
    }

    public partial class Applicant
    {
        public Applicant()
        {

        }
        public Applicant(IReportRequest request)
        {
            ApplicantFirstName = request.ApplicantFirstName;
            ApplicantLastName = request.ApplicantLastName;
            ApplicantMiddleName = request.ApplicantMiddleName;
            DateOfBirth = request.DateOfBirth.ToString("ddMMyyyy", CultureInfo.InvariantCulture);
            Gender = request.Gender;
            EmailAddress = request.EmailId;
           
            
            var identifier = new List<Identifier>();
            if (!string.IsNullOrEmpty(request.VoterId))
            {
                identifier.Add(new Identifier(){
                    IdNumber = request.VoterId,
                    IdType = "03"
                });
            }
            if(!string.IsNullOrEmpty(request.PanNo))
            {
                identifier.Add(new Identifier()
                {
                    IdNumber = request.PanNo,
                    IdType ="01"
                });
            }
            if (!string.IsNullOrEmpty(request.DrivingLicenseNumber))
            {
                identifier.Add(new Identifier()
                {
                    IdNumber = request.DrivingLicenseNumber,
                    IdType = "04"
                });
            }
            if (!string.IsNullOrEmpty(request.PassportNumber))
            {
                identifier.Add(new Identifier()
                {
                    IdNumber = request.PassportNumber,
                    IdType = "02"
                });
            }
            if (!string.IsNullOrEmpty(request.AdharNumber))
            {
                identifier.Add(new Identifier()
                {
                    IdNumber = request.AdharNumber,
                    IdType = "06"
                });
            }
            Identifiers = new Identifiers();
            Identifiers.Identifier = identifier;
         
            var telephones = new List<Telephone>();
            if (!string.IsNullOrEmpty(request.MobileNo))
            {
                telephones.Add(new Telephone()
                {
                    TelephoneNumber =request.MobileNo,
                    TelephoneType = "01"
                });
            }
            if (!string.IsNullOrEmpty(request.OfficeLandLineNo))
            {
                telephones.Add(new Telephone()
                {
                    TelephoneNumber = request.MobileNo,
                    TelephoneType = "03"
                });
            }
            if (!string.IsNullOrEmpty(request.LandLineNo))
            {
                telephones.Add(new Telephone()
                {
                    TelephoneNumber = request.LandLineNo,
                    TelephoneType = "02"
                });
            }
            Telephones = new Telephones();
            Telephones.Telephone = telephones;
           
            var addresses = new List<Address>();
            if (!string.IsNullOrEmpty(request.Address1Line1))
            {
                addresses.Add(new Address()
                {
                    AddressLine1 = request.Address1Line1,
                    AddressLine2 = request.Address1Line2,
                    AddressLine3 = request.Address1Line3,
                    City = request.Address1City,
                    StateCode = request.Address1State,
                    PinCode = request.Address1Pincode,
                    AddressType = request.Address1AddressCategory,
                    ResidenceType = request.Address1ResidentOccupation
                });
               
            }
            if(!string.IsNullOrEmpty(request.Address2Line1))
            {
                addresses.Add(new Address()
                {
                    AddressLine1 = request.Address2Line1,
                    AddressLine2 = request.Address2Line2,
                    AddressLine3 = request.Address2Line3,
                    City = request.Address2City,
                    StateCode = request.Address2State,
                    PinCode = request.Address2Pincode,
                    AddressType = request.Address2AddressCategory,
                    ResidenceType = request.Address2ResidentOccupation
                });
            }
            Addresses = new Addresses();
            Addresses.Address = addresses;
        }
    }
    public partial class RequestInfo
    {
        public RequestInfo()
        {

        }
        public RequestInfo(string solutionSetId, string solutionSetVersion, string executionMode,string environmentId)
        {
            SolutionSetId = solutionSetId;
            SolutionSetVersion = solutionSetVersion;
            ExecutionMode = executionMode;
            EnvironmentId = environmentId;
        }
    }
    public partial class Identifier
    {
        public Identifier()
        {

        }
        public Identifier(string idType, string idNumber)
        {
            IdType = idType;
            IdNumber = idNumber;
        }
    }
    public partial class ApplicationData
    {
        public ApplicationData()
        {

        }
        public ApplicationData(ICibilConfiguration configuration, IReportRequest reportRequest)
        {
            IDVMemberCode = configuration.IDVMemberCode;
            SkipCibilBureauFlag = configuration.SkipCibilBureauFlag.ToString();
            SkipDSTuIDVisionFlag = configuration.SkipDSTuIDVisionFlag.ToString();
            SkipDSTuNtcFlag = configuration.SkipDSTuNtcFlag.ToString();
            EnquiryAmount = reportRequest.LoanAmount;
            EnquiryPurpose = configuration.InpEnquiryPurpose;
            ExternalApplicationID = "";
            ConsumerConsentForUIDAIAuthentication = configuration.ConsumerConsentForUIDAIAuthentication;
            GSTStateCode = reportRequest.Address1State;
            Purpose = configuration.InpEnquiryPurpose;
            Amount = reportRequest.LoanAmount;
            Product = configuration.Product;
            ReferenceNumber = Guid.NewGuid().ToString("N").ToUpper().Substring(0,14);
        }
    }
    public partial class Identifiers
    {
        public Identifiers()
        {

        }
        public Identifiers(List<Identifier> identifier)
        {
            Identifier = identifier;
        }
    }
    public partial class Telephones
    {
        public Telephones()
        {

        }
        public Telephones(List<Telephone> telephone)
        {
            Telephone = telephone;
        }
    }
    public partial class Telephone
    {
        public Telephone()
        {

        }
        public Telephone(string telephoneType, string telephoneNumber, string telephoneExtension)
        {
            TelephoneType = TelephoneType;
            TelephoneNumber = telephoneExtension;
            TelephoneExtension = TelephoneExtension;
        }
    }
    public partial class Addresses
    {
        public Addresses()
        {
        }
        public Addresses(List<Address> addresses)
        {
            Address = addresses;

            //Address.Add(new Address(reportRequest.Address1Line1, reportRequest.Address1Line2, reportRequest.Address1Line3,
            //    reportRequest.Address1AddressCategory, reportRequest.Address1City, reportRequest.Address1Pincode, reportRequest.Address1ResidentOccupation, reportRequest.Address1State));
            //Address.Add(new Address(reportRequest.Address2Line1, reportRequest.Address2Line2, reportRequest.Address2Line3,
            //    reportRequest.Address2AddressCategory, reportRequest.Address2City, reportRequest.Address2Pincode, reportRequest.Address2ResidentOccupation, reportRequest.Address2State));
        }
    }
    public partial class Address
    {
        public Address()
        {

        }
        public Address(string addressLine1, string addressLine2, string addressLine3, string addressType, string city, string pinCode, string residenceType, string stateCode)
        {
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            AddressLine3 = addressLine3;
            AddressType = addressType;
            AddressType = addressType;
            City = city;
            PinCode = pinCode;
            ResidenceType = residenceType;
            StateCode = stateCode;
        }

    }
    public partial class Field
    {
        public Field()
        {

        }
        public Field(string key, string text)
        {
            Key = key;
            Text = text;
        }
    }
}
