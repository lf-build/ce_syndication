﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Cibil.Proxy.Request
{
    public partial class Envelope
    {
        public Envelope()
        {

        }
        public Envelope(string dcRequest)
        {
            Body = new Body(dcRequest);
        }
    }
    public partial class Body
    {
        public Body()
        {

        }
        public Body(string dcRequest)
        {
            ExecuteXMLString = new ExecuteXMLString(dcRequest);
        }
    }
    public partial class ExecuteXMLString
    {
        public ExecuteXMLString()
        {

        }
        public ExecuteXMLString(string dcRequest)
        {
            Request = dcRequest;
        }
    }
}
