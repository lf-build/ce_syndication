using System.Threading.Tasks;

using CreditExchange.Syndication.Cibil.Proxy.Request;
using CreditExchange.Syndication.Cibil.Proxy.Response;

namespace CreditExchange.Syndication.Cibil.Proxy
{
    public interface IProxyReport
    {
        Task<XMLResponse> GetCreditReport(DCRequest dcRequest);
        Task<string> GetHTMLReport(string cibilReport);
    }
}