﻿using CreditExchange.Syndication.Cibil.Request;
using CreditExchange.Syndication.Cibil.Response;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Cibil
{
    public interface ICibilSyndicationService
    {
        Task<ICibilReport> GetReport(string entityType, string entityId, IReportRequest request);
        Task<string> GetHtmlReport(string entityType, string entityId, string creditreportXML);
    }
}