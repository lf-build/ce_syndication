﻿namespace CreditExchange.Syndication.Cibil.CIR
{
    public interface IIDSegment
    {
        string IDNumber { get; set; }
        string IDType { get; set; }
    }
}