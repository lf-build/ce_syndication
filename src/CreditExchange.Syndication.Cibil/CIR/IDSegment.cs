﻿

namespace CreditExchange.Syndication.Cibil.CIR

{
    public class IDSegment : IIDSegment
    {
        public IDSegment()
        {

        }
        public IDSegment(Proxy.Response.IDSegment idsegment)
        {
            IDType = idsegment.IDType;
            IDNumber = idsegment.IDNumber;
        }

       public string IDType { get; set; }
       public string IDNumber { get; set; }
    }
}
