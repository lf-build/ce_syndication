﻿
using System.Collections.Generic;

namespace CreditExchange.Syndication.Cibil.CIR
{
    public interface ICreditReport
    {
        List<IAccount> Account { get; set; }
        List<IAddressSegment> Address { get; set; }
        string ConsumerName1 { get; set; }
        string ConsumerName2 { get; set; }
        string ConsumerName3 { get; set; }
        string ConsumerName4 { get; set; }
        string ConsumerName5 { get; set; }
        string DateOfBirth { get; set; }
        List<string> Emails { get; set; }
        IEmploymentSegment EmploymentSegment { get; set; }
        List<IEnquiry> Enquiry { get; set; }
        string Gender { get; set; }
        List<IIDSegment> IDSegment { get; set; }
        IScoreSegment ScoreSegment { get; set; }
        List<ITelephoneSegment> TelephoneSegment { get; set; }
        string DateProcessed { get; set; }
        string ECNNumber {get; set;}
    }
}