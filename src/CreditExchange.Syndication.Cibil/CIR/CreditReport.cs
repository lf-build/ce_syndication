﻿using LendFoundry.Foundation.Client;
using CreditExchange.Syndication.Cibil.Proxy.Response;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace CreditExchange.Syndication.Cibil.CIR
{
    public class CreditReport : ICreditReport
    {
        public CreditReport()
        {

        }
        public CreditReport(CibilReport report)
        {
            if (report != null)
            {
                ECNNumber = report?.Header.EnquiryControlNumber;
                DateProcessed = report?.Header.DateProcessed;
                ConsumerName1 = report?.NameSegment?.ConsumerName1;
                ConsumerName2 = report?.NameSegment?.ConsumerName2;
                ConsumerName3 = report?.NameSegment?.ConsumerName3;
                ConsumerName4 = report?.NameSegment?.ConsumerName4;
                ConsumerName5 = report?.NameSegment?.ConsumerName5;
                Gender = report?.NameSegment?.Gender;
                IDSegment = report?.IDSegment?.Select(i => new IDSegment(i)).ToList<IIDSegment>();
                TelephoneSegment = report?.TelephoneSegment?.Select(i => new TelephoneSegment(i)).ToList<ITelephoneSegment>();
                Emails = report?.EmailContactSegment.Select(i => i.EmailID).ToList();
                EmploymentSegment = new EmploymentSegment(report?.EmploymentSegment);
                ScoreSegment = new ScoreSegment(report?.ScoreSegment);
                Address = report?.Address?.Select(i => new AddressSegment(i)).ToList<IAddressSegment>();
                Account = report?.Account?.Select(i => new Account(i.Account_NonSummary_Segment_Fields)).ToList<IAccount>();
                Enquiry = report?.Enquiry.Select(i => new Enquiry(i)).ToList<IEnquiry>();
                
            }
        }
        public CreditReport(AdditionalCreditReport report)
        {
            if (report != null)
            {
                ECNNumber = report?.Header.EnquiryControlNumber;
                DateProcessed = report?.Header.DateProcessed;
                ConsumerName1 = report?.NameSegment?.ConsumerName1;
                ConsumerName2 = report?.NameSegment?.ConsumerName2;
                ConsumerName3 = report?.NameSegment?.ConsumerName3;
                ConsumerName4 = report?.NameSegment?.ConsumerName4;
                ConsumerName5 = report?.NameSegment?.ConsumerName5;
                Gender = report?.NameSegment?.Gender;
                IDSegment = report?.IDSegment?.Select(i => new IDSegment(i)).ToList<IIDSegment>();
                TelephoneSegment = report?.TelephoneSegment?.Select(i => new TelephoneSegment(i)).ToList<ITelephoneSegment>();
                Address = report?.Address?.Select(i => new AddressSegment(i)).ToList<IAddressSegment>();
               
            }
        }

        public string DateProcessed { get; set; }
        public string ConsumerName1 { get; set; }
        public string ConsumerName2 { get; set; }
        public string ConsumerName3 { get; set; }
        public string ConsumerName4 { get; set; }
        public string ConsumerName5 { get; set; }
        public string DateOfBirth { get; set; }
        public string Gender { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IIDSegment, IDSegment>))]
        public List<IIDSegment> IDSegment { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITelephoneSegment, TelephoneSegment>))]
        public List<ITelephoneSegment> TelephoneSegment { get; set; }

        public List<string> Emails { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmploymentSegment, EmploymentSegment>))]
        public IEmploymentSegment EmploymentSegment { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IScoreSegment, ScoreSegment>))]
        public IScoreSegment ScoreSegment { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddressSegment, AddressSegment>))]
        public List<IAddressSegment> Address { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAccount, Account>))]
        public List<IAccount> Account { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IEnquiry, Enquiry>))]
        public List<IEnquiry> Enquiry { get; set; }

        public string ECNNumber {get; set;}
       
    }
}
