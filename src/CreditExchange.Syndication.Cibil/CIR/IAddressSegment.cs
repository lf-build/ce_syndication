﻿namespace CreditExchange.Syndication.Cibil.CIR
{
    public interface IAddressSegment
    {
        string AddressCategory { get; set; }
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string AddressLine3 { get; set; }
        string AddressLine4 { get; set; }
        string AddressLine5 { get; set; }
        string DateReported { get; set; }
        string PinCode { get; set; }
        string ResidenceCode { get; set; }
        string StateCode { get; set; }
    }
}