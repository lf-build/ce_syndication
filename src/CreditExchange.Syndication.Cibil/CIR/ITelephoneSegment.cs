﻿namespace CreditExchange.Syndication.Cibil.CIR
{
    public interface ITelephoneSegment
    {
        string TelephoneNumber { get; set; }
        string TelephoneType { get; set; }
    }
}