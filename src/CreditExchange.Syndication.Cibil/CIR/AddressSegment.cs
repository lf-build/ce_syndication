﻿

namespace CreditExchange.Syndication.Cibil.CIR
{
    public class AddressSegment : IAddressSegment
    {
        public AddressSegment()
        {

        }
        public AddressSegment(Proxy.Response.AddressSegment addresssegment)
        {
            if(addresssegment!=null)
            {
                AddressLine1 = addresssegment.AddressLine1;
                AddressLine2 = addresssegment.AddressLine2;
                AddressLine3 = addresssegment.AddressLine3;
                AddressLine4 = addresssegment.AddressLine4;
                AddressLine5 = addresssegment.AddressLine5;
                StateCode = addresssegment.StateCode;
                PinCode = addresssegment.PinCode;
                AddressCategory = addresssegment.AddressCategory;
                ResidenceCode = addresssegment.ResidenceCode;
                DateReported = addresssegment.DateReported;
            }
        }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string AddressLine5 { get; set; }
        public string StateCode { get; set; }
        public string PinCode { get; set; }
        public string AddressCategory { get; set; }
        public string ResidenceCode { get; set; }
        public string DateReported { get; set; }
    }
}
