﻿namespace CreditExchange.Syndication.Cibil.CIR
{
    public interface IScoreSegment
    {
        string ReasonCode1 { get; set; }
        string ReasonCode2 { get; set; }
        string ReasonCode3 { get; set; }
        string ReasonCode4 { get; set; }
        string ReasonCode5 { get; set; }
        string Score { get; set; }
        string ScoreCardName { get; set; }
        string ScoreCardVersion { get; set; }
        string ScoreDate { get; set; }
        string ScoreName { get; set; }
    }
}