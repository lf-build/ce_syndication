﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Cibil.CIR
{
    public interface IEnquiry
    {
        string DateOfEnquiryFields { get; set; }
        string EnquiringMemberShortName { get; set; }
        string EnquiryPurpose { get; set; }
        string EnquiryAmount { get; set; }
    }
}
