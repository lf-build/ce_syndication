﻿namespace CreditExchange.Syndication.Cibil.CIR
{
    public class EmploymentSegment : IEmploymentSegment
    {
        public EmploymentSegment()
        {

        }
        public EmploymentSegment(Proxy.Response.EmploymentSegment employmentsegment)
        {
            if (employmentsegment != null)
            {
                AccountType = employmentsegment.AccountType;
                DateReportedCertified = employmentsegment.DateReportedCertified;
                OccupationCode = employmentsegment.OccupationCode;
                Income = employmentsegment.Income;
                NetGrossIndicator = employmentsegment.NetGrossIndicator;
                MonthlyAnnualIndicator = employmentsegment.MonthlyAnnualIndicator;
            }
        }
        public string AccountType { get; set; }
        
        public string DateReportedCertified { get; set; }
        
        public string OccupationCode { get; set; }
       public string Income { get; set; }
        public string NetGrossIndicator { get; set; }
        
        public string MonthlyAnnualIndicator { get; set; }
    }
}
