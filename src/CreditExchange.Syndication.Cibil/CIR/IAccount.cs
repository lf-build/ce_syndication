﻿namespace CreditExchange.Syndication.Cibil.CIR
{
    public interface IAccount
    {
        string AccountType { get; set; }
        string ActualPaymentAmount { get; set; }
      
        string CashLimit { get; set; }
        string CreditLimit { get; set; }
        string CurrentBalance { get; set; }
        string DateClosed { get; set; }
        string DateOfLastPayment { get; set; }
        string DateOpenedOrDisbursed { get; set; }
        string DateReportedAndCertified { get; set; }
        string HighCreditOrSanctionedAmount { get; set; }
        string OwenershipIndicator { get; set; }
        string PaymentFrequency { get; set; }
        string PaymentHistory1 { get; set; }
        string PaymentHistory2 { get; set; }
        string PaymentHistory1FieldLength { get; set; }

        string PaymentHistory2FieldLength { get; set; }
        string PaymentHistoryEndDate { get; set; }
        string PaymentHistoryStartDate { get; set; }
        string RateOfInterest { get; set; }
        string SuitFiledOrWilfulDefault { get; set; }

        string WrittenOffAndSettled { get; set; }
        string ReportingMemberShortName { get; set; }
    }
}