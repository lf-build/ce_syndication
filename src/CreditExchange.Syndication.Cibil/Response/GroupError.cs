﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Cibil.Response
{
    public class GroupError : IGroupError
    {
        public GroupError()
        {

        }
        public GroupError(Proxy.Response.GroupErrorSegment groupErrorSegment)
        {
            if (groupErrorSegment?.Response?.ErrorCode?.Count>0)
                ErrorCode = groupErrorSegment.Response.ErrorCode;
            if (groupErrorSegment?.Response?.ErrorDescription?.Count>0)
                ErrorDescription = groupErrorSegment?.Response.ErrorDescription;
        }

        public List<string> ErrorCode
        {
            get;set;
            
        }

        public List<string> ErrorDescription
        {
            get;set;
            
        }
    }
}
