﻿using CreditExchange.Syndication.Cibil.Proxy.Response;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.Cibil.Response
{
    public class CibilReport : ICibilReport
    {
        public CibilReport()
        {

        }
        public CibilReport(XMLResponse xmlResponse)
        {
            if(xmlResponse?.DCResponse!=null)
            Report = new Report(xmlResponse.DCResponse);
            RequestXML = xmlResponse.RequestXML;
            ResponseXML = xmlResponse.ResponseXML;
        }
        [JsonConverter(typeof(InterfaceConverter<IReport, Report>))]
        public IReport Report {  get; set; }

        public string RequestXML{ get; set; }

        public string ResponseXML{ get; set; }
    }
}
