﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Cibil.Response
{
  public  interface IGroupError
    {
        List<string> ErrorCode { get; set; }
        List<string> ErrorDescription { get; set; }
    }
}
