﻿namespace CreditExchange.Syndication.Cibil.Response
{
    public class Error : IError
    {
        public Error()
        {

        }
        public Error(Proxy.Response.Reason errorResponse)
        {
            if(errorResponse!=null)
            {
                ErrorCode = errorResponse.Code;
                ErrorDescription = errorResponse.Description;
            }
        }
        public Error(Proxy.Response.ExceptionInfo exceptionInfo)
        {
            if (exceptionInfo != null)
            {
                ErrorCode = exceptionInfo.ErrorCode;
                ErrorDescription = exceptionInfo.Message;
            }
        }
        public Error(Proxy.Response.ErrorResponse errorResponse)
        {
            if (errorResponse != null)
            {
                ErrorCode = errorResponse.ErrorCode;
                ErrorDescription = errorResponse.ErrorDescription;
            }
        }
        public Error(string errorcode,string errorDescription)
        {
            ErrorCode = errorcode;
            ErrorDescription = errorDescription;
        }
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
    }
}
