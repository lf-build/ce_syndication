﻿namespace CreditExchange.Syndication.Cibil
{
  public enum Gender
    {
        Female=1,
        Male=2,
        Transgender=3
    }
}