﻿using CreditExchange.Syndication.Cibil.Proxy;
using CreditExchange.Syndication.Cibil.Proxy.Request;
using CreditExchange.Syndication.Cibil.Request;
using CreditExchange.Syndication.Cibil.Response;
using System;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Cibil
{
    public class CibilSyndicationService : ICibilSyndicationService
    {
        public CibilSyndicationService(ICibilConfiguration configuration, IProxyReport proxy )
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));

            CibilConfiguration = configuration;
            Proxy = proxy;
           
        }
      
        private ICibilConfiguration CibilConfiguration { get; }
        private IProxyReport Proxy { get; }
     
        public async Task<ICibilReport> GetReport(string entityType, string entityId, IReportRequest request)
        {
           try
            {

                var proxyRequest = new DCRequest(CibilConfiguration, request);
                var proxyResponse = await Proxy.GetCreditReport(proxyRequest);
                var response= new CibilReport(proxyResponse);
       
                 return response;

            }
            catch (CibilException exception)
            {
             throw new CibilException(exception.Message);
            }
                  
        }

        public async Task<string> GetHtmlReport(string entityType, string entityId, string creditreportXML)
        {
            try
            {
             
             return await Proxy.GetHTMLReport(creditreportXML);
            }
            catch(CibilException exception)
            {
                throw new CibilException(exception.Message);
            }
        }


    }
}
