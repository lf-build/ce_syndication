﻿

using LendFoundry.Foundation.Persistence;
using System;

namespace CreditExchange.Syndication.Cibil
{
    public class CibilData : Aggregate,ICibilData
    {
       public string EntityType { get; set; }
       public string EntityId { get; set; }
       public string RequestXML { get; set; }
       public string ResponseXML { get; set; }
       public DateTimeOffset ReportDate { get; set; }

       public string BureauResponseRaw { get; set; }

      public string Status { get; set; } 
    }
}
