﻿namespace CreditExchange.Syndication.Cibil
{
    public interface ICIRHtml
    {
        string report { get; set; }
    }
}