﻿namespace CreditExchange.Syndication.Cibil
{
    public enum TelephoneType
    {
        NotClassified,
        MobilePhone,
        HomePhone,
        OfficePhone
    }
}
