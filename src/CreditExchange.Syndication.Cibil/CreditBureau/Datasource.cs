﻿
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;


namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public class Datasource : IDatasource
    {
        public Datasource()
        {

        }
        public Datasource(Proxy.Response.DataSource datasource)
        {

            if(datasource!=null)
            {
                Source = datasource.Source;
                FirstName = datasource.FirstName;
                MiddleName = datasource.MiddleName;
                LastName = datasource.LastName;
                NameMatch = datasource.NameMatch;
                Gender = datasource.Gender;
                GenderMatch = datasource.GenderMatch;
                DateofBirth = datasource.DateofBirth;
                DateofBirthMatch = datasource.DateofBirthMatch;
                PAN = datasource.PAN;
                PANMatch = datasource.PANMatch;
                VoterId = datasource.VoterId;
                VoterIdMatch = datasource.VoterIdMatch;
                UID = datasource.UID;
                UIDMatch = datasource.UIDMatch;
                DrivingLicense = datasource.DrivingLicense;
                DrivingLicenseMatch = datasource.DrivingLicenseMatch;
                RationCard = datasource.RationCard;
                RationCardMatch = datasource.RationCardMatch;
                Passport = datasource.Passport;
                PassportMatch = datasource.PassportMatch;
                Telephone1 = datasource.Telephone1;
                Telephone1Match = datasource.Telephone1Match;
                Telephone2 = datasource.Telephone2;
                Telephone2Match = datasource.Telephone2Match;
                Telephone3 = datasource.Telephone3;
                Telephone3Match = datasource.Telephone3Match;
                Telephone4 = datasource.Telephone4;
                Telephone4Match = datasource.Telephone4Match;
                Address = datasource.Address.Address?.Select(i => new Address(i)).ToList<IAddress>();
            }
        }
        public string Source { get; set; }
     
        public string FirstName { get; set; }
     
        public string MiddleName { get; set; }
     
        public string LastName { get; set; }
     
        public string NameMatch { get; set; }
     
        public string Gender { get; set; }
     
        public string GenderMatch { get; set; }
     
        public string DateofBirth { get; set; }
     
        public string DateofBirthMatch { get; set; }
     
        public string PAN { get; set; }
     
        public string PANMatch { get; set; }
     
        public string VoterId { get; set; }
     
        public string VoterIdMatch { get; set; }
     
        public string UID { get; set; }
     
        public string UIDMatch { get; set; }
     
        public string DrivingLicense { get; set; }
     
        public string DrivingLicenseMatch { get; set; }
     
        public string RationCard { get; set; }
     
        public string RationCardMatch { get; set; }
     
        public string Passport { get; set; }
     
        public string PassportMatch { get; set; }
     
        public string Telephone1 { get; set; }
     
        public string Telephone1Match { get; set; }
     
        public string Telephone2 { get; set; }
     
        public string Telephone2Match { get; set; }
     
        public string Telephone3 { get; set; }
     
        public string Telephone3Match { get; set; }
     
        public string Telephone4 { get; set; }
     
        public string Telephone4Match { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Address { get; set; }
    }
}
