﻿namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public class AadhaarIdBA : IAadhaarIdBA
    {
        public AadhaarIdBA()
        {
                
        }
        public AadhaarIdBA(Proxy.Response.AadhaarIdBA aadhaar)
        {
            if (aadhaar != null)
            {
                Id = aadhaar.Id;
                Source = aadhaar.Source;
                ErrorCode = aadhaar.ErrorCode;
                POI = aadhaar.POI;
                POA = aadhaar.POA;
                PFA = aadhaar.PFA;
                TimeTrail = aadhaar.TimeTrail;
                POINameMatch = aadhaar.POINameMatch;
                POIGenderMatch = aadhaar.POIGenderMatch;
                POIDOBMatch = aadhaar.POIDOBMatch;
                POAPincodeMatch = aadhaar.POAPincodeMatch;
            }
        }
        public string Id { get; set; }
       
        public string Source { get; set; }
       
        public string ErrorCode { get; set; }
       
        public string POI { get; set; }
       
        public string POA { get; set; }
       
        public string PFA { get; set; }
       
        public string TimeTrail { get; set; }
       
        public string POINameMatch { get; set; }
       
        public string POIGenderMatch { get; set; }
       
        public string POIDOBMatch { get; set; }
       
        public string POAStateMatch { get; set; }
       
        public string POAPincodeMatch { get; set; }
    }
}
