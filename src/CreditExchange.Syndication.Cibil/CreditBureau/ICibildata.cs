﻿namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public interface ICibildata
    {
        string CIBILAadharNumber { get; set; }
        string CIBILAadharNumberMatch { get; set; }
        string CibilAddress1 { get; set; }
        string CibilAddress1Match { get; set; }
        string CibilAddress2 { get; set; }
        string CibilAddress2Match { get; set; }
        string CibilDOB { get; set; }
        string CibilDOBMatch { get; set; }
        string CIBILDrivingLicense { get; set; }
        string CIBILDrivingLicenseMatch { get; set; }
        string CIBILGender { get; set; }
        string CIBILGenderMatch { get; set; }
        string CibilName { get; set; }
        string CibilNameMatch { get; set; }
        string CIBILPassport { get; set; }
        string CIBILPassportMatch { get; set; }
        string CibilPhone1 { get; set; }
        string CibilPhone1Match { get; set; }
        string CibilPhone2 { get; set; }
        string CibilPhone2Match { get; set; }
        string CibilPhone3 { get; set; }
        string CibilPhone3Match { get; set; }
        string CibilPhone4 { get; set; }
        string CibilPhone4Match { get; set; }
        string CIBILRationCard { get; set; }
        string CIBILRationCardMatch { get; set; }
        string CIBILVoterID { get; set; }
        string CIBILVoterIDMatch { get; set; }
    }
}