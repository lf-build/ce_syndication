﻿
namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public class Phone : IPhone
    {
        public Phone()
        {

        }
        public Phone(Proxy.Response.VerificationResponseTelephone phone)
        {
            Number = phone.Number;
            Type = phone.Type;
            Match = phone.Match;
        }
        public string Number { get; set; }
       
        public string Type { get; set; }
       
        public string Match { get; set; }
    }
}
