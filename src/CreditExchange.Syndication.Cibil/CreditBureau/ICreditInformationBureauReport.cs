﻿
namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public interface ICreditInformationBureauReport
    {
         IEkyc Ekyc { get; set; }
         IVoterId VoterId { get; set; }
         IAadhaarIdBA AadhaarIdBA { get; set; }
         IDatasource DataSource { get; set; }
         IPan Pan { get; set; }
         ICibildata Cibildata { get; set; }
    }
}