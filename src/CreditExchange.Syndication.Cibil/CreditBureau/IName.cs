﻿namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public interface IName
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string MiddleName { get; set; }
        string NameMatch { get; set; }
    }
}