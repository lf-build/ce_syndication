﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public class Address : IAddress
    {
        public Address()
        {

        }
        public Address(Proxy.Response.Address address)
        {
            AddressLine1 = address.AddressLine1;
            PinCode = address.PinCode;
            StateCode = address.StateCode;
            Match = address.Match;
            Match1 = address.Match1;
            AddressCategory = address.AddressCategory;
            DateReported = address.DateReported;
        }
        public string AddressLine1 { get; set; }
     
        public string PinCode { get; set; }
     
        public string StateCode { get; set; }
     
        public string Match { get; set; }
     
        public string Match1 { get; set; }
     
        public string AddressCategory { get; set; }
     
        public string DateReported { get; set; }
    }
}
