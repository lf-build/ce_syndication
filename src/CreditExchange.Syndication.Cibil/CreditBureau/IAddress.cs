﻿namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public interface IAddress
    {
        string AddressCategory { get; set; }
        string AddressLine1 { get; set; }
        string DateReported { get; set; }
        string Match { get; set; }
        string Match1 { get; set; }
        string PinCode { get; set; }
        string StateCode { get; set; }
    }
}