﻿namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public interface IPhone
    {
        string Match { get; set; }
        string Number { get; set; }
        string Type { get; set; }
    }
}