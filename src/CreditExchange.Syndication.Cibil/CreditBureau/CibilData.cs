﻿
namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public class Cibildata : ICibildata
    {
        public Cibildata()
        {

        }
        public Cibildata(Proxy.Response.CIBIL cibilresponse)
        {
            if(cibilresponse!=null)
            {
                CibilName = cibilresponse.CibilName;
                CibilNameMatch = cibilresponse.CibilNameMatch;
                CIBILGender = cibilresponse.CIBILGender;
                CIBILGenderMatch = cibilresponse.CIBILGenderMatch;
                CIBILPassport = cibilresponse.CIBILPassport;
                CIBILPassportMatch = cibilresponse.CIBILPassportMatch;
                CIBILVoterID = cibilresponse.CIBILVoterID;
                CIBILVoterIDMatch = cibilresponse.CIBILVoterIDMatch;
                CIBILDrivingLicense = cibilresponse.CIBILDrivingLicense;
                CIBILDrivingLicenseMatch = cibilresponse.CIBILDrivingLicenseMatch;
                CIBILDrivingLicenseMatch = cibilresponse.CIBILDrivingLicenseMatch;
                CIBILAadharNumber = cibilresponse.CIBILAadharNumber;
                CIBILAadharNumberMatch = cibilresponse.CIBILAadharNumberMatch;
                CibilPhone1 = cibilresponse.CibilPhone1;
                CibilPhone1Match = cibilresponse.CibilPhone1Match;
                CibilPhone2 = cibilresponse.CibilPhone2;
                CibilPhone2Match = cibilresponse.CibilPhone2Match;
                CibilPhone3 = cibilresponse.CibilPhone3;
                CibilPhone3Match = cibilresponse.CibilPhone3Match;
                CibilPhone4 = cibilresponse.CibilPhone4;
                CibilPhone4Match = cibilresponse.CibilPhone4Match;
                CibilDOB = cibilresponse.CibilDOB;
                CibilDOBMatch = cibilresponse.CibilDOBMatch;
            }
        }
        public string CibilName { get; set; }
     
        public string CibilNameMatch { get; set; }
     
        public string CIBILGender { get; set; }
     
        public string CIBILGenderMatch { get; set; }
     
        public string CIBILPassport { get; set; }
     
        public string CIBILPassportMatch { get; set; }
     
        public string CIBILDrivingLicense { get; set; }
     
        public string CIBILDrivingLicenseMatch { get; set; }
     
        public string CIBILAadharNumber { get; set; }
     
        public string CIBILAadharNumberMatch { get; set; }
     
        public string CIBILRationCard { get; set; }
     
        public string CIBILRationCardMatch { get; set; }
     
        public string CIBILVoterID { get; set; }
     
        public string CIBILVoterIDMatch { get; set; }
     
        public string CibilAddress1 { get; set; }
     
        public string CibilAddress1Match { get; set; }
     
        public string CibilAddress2 { get; set; }
     
        public string CibilAddress2Match { get; set; }
     
        public string CibilPhone1 { get; set; }
     
        public string CibilPhone1Match { get; set; }
     
        public string CibilPhone2 { get; set; }
     
        public string CibilPhone2Match { get; set; }
     
        public string CibilPhone3 { get; set; }
     
        public string CibilPhone3Match { get; set; }
     
        public string CibilPhone4 { get; set; }
     
        public string CibilPhone4Match { get; set; }
     
        public string CibilDOB { get; set; }
     
        public string CibilDOBMatch { get; set; }
    }
}
