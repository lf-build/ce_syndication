﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public interface IEkyc
    {
        string AadharNumber { get; set; }
        string AadharNumberMatch { get; set; }
        List<IAddress> Addresses { get; set; }
        string DateofBirth { get; set; }
        string DateofBirthMatch { get; set; }
        string DrivingLicenseMatch { get; set; }
        string Gender { get; set; }
        string GenderMatch { get; set; }
        IName Name { get; set; }
        string PANMatch { get; set; }
        string PassportMatch { get; set; }
        IPhone PhoneNumber { get; set; }
        string Photo { get; set; }
        string RationCardMatch { get; set; }
        string Source { get; set; }
        string VoterIdMatch { get; set; }
    }
}