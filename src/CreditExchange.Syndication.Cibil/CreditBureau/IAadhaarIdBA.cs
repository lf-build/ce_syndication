﻿namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public interface IAadhaarIdBA
    {
        string ErrorCode { get; set; }
        string Id { get; set; }
        string PFA { get; set; }
        string POA { get; set; }
        string POAPincodeMatch { get; set; }
        string POAStateMatch { get; set; }
        string POI { get; set; }
        string POIDOBMatch { get; set; }
        string POIGenderMatch { get; set; }
        string POINameMatch { get; set; }
        string Source { get; set; }
        string TimeTrail { get; set; }
    }
}