﻿namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public interface IPan
    {
        string PANName { get; set; }
        string PANNameMatch { get; set; }
        string PANNo { get; set; }
        string PANNoMatch { get; set; }
    }
}