﻿using LendFoundry.Foundation.Client;
using CreditExchange.Syndication.Cibil.Proxy.Response;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public class Ekyc : IEkyc
    {

        public Ekyc(EKYC ekyc)
        {
            Source = ekyc.Source;
            Photo = ekyc.Photo;
            
        }

        public string Source { get; set; }
        
        public string Photo { get; set; }
        
        public IName Name { get; set; }
        
        public string Gender { get; set; }
        
        public string GenderMatch { get; set; }
        
        public string DateofBirth { get; set; }
        
        public string DateofBirthMatch { get; set; }
        
        public string PANMatch { get; set; }
        
        public string VoterIdMatch { get; set; }
      
        public string PassportMatch { get; set; }
      
        public string DrivingLicenseMatch { get; set; }
      
        public string AadharNumber { get; set; }
      
        public string AadharNumberMatch { get; set; }
      
        public string RationCardMatch { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Addresses { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPhone, Phone>))]
        public IPhone PhoneNumber { get; set; }
    }
}
