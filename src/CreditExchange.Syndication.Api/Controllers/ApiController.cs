﻿using CreditExchange.Syndication.Abstractions;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication;
using Microsoft.AspNet.Mvc;
using System;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Api
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(ISyndication syndicationFactory, ILogger logger) : base(logger)
        {
            if (syndicationFactory == null)
                throw new ArgumentNullException(nameof(syndicationFactory));
            SyndicationFactory = syndicationFactory;
        }

        private ISyndication SyndicationFactory { get; }

        [HttpPost("{entitytype}/{entityid}/initiate/{syndicationName}")]
        public async Task<IActionResult> Initiatesyndication(string entityType, string entityId,string syndicationName, [FromBody] Request payload)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {

                    var response = await SyndicationFactory.InitializeSyndication(entityType,entityId, payload);
                    return Ok(response);

                }
                catch (Exception exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
        [HttpPost("{entitytype}/{entityid}/process/{syndicationName}")]
        public async Task<IActionResult> Processsyndication(string entityType, string entityId,string syndicationName, [FromBody] Request payload)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var response = await SyndicationFactory.ProcessSyndication(entityType, entityId, payload);
                    return Ok(response);

                }
                catch (Exception exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
    }
}
