﻿
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using CreditExchange.Syndication.AadharBridge;
using CreditExchange.Syndication.AadharBridge.Proxy;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;

namespace CreditExchange.AadhaarBridge.Api
{
    public class Startup
    {
       // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();

            // interface implements
            services.AddConfigurationService<AadhaarBridgeConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);

            services.AddTransient<IAadhaarBridgeConfiguration>(p =>
            {
                var configuration = p.GetService<IConfigurationService<AadhaarBridgeConfiguration>>().Get();
                configuration.GatewayUrl = $"http://{Settings.AadhaarGateway.Host}:{Settings.AadhaarGateway.Port}";
                return configuration;
            });
            services.AddLookupService(Settings.LookupService.Host, Settings.LookupService.Port);
            services.AddTransient<IAadhaarBridgeProxy, AadhaarBridgeProxy>();
            services.AddTransient<IAadhaarBridgeService, AadhaarBridgeService>();

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseHealthCheck();
        }

      
    }
}
