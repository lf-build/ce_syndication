﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using CreditExchange.Syndication.AadharBridge.Model;
using CreditExchange.Syndication.AadharBridge;
using CreditExchange.Syndication.AadharBridge.Proxy;
using LendFoundry.Foundation.Logging;

namespace CreditExchange.AadharBridge.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IAadhaarBridgeService service,ILogger logger):base(logger)
        {
            Service = service;
        }

        private IAadhaarBridgeService Service { get; }

        [HttpPost("{entityType}/{entityId}/is-aadhaar-valid")]
        public async Task<IActionResult> IsAadharValid(string entityType, string entityId,
            [FromBody] AadhaarAuthRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Service.IsAadharValid(entityType, entityId, request));
                }
                catch (AadhaarBridgeNetworkException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }

            });
        }

        [HttpPost("{entityType}/{entityId}/{aadhaarid}/request-aadhaar-verification")]
        public async Task<IActionResult> GenerateOneTimePassword(string entityType, string entityId, string aadhaarid)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Service.GenerateOneTimePassword(entityType, entityId, aadhaarid));
                }
                catch (AadhaarBridgeNetworkException exception)
                {

                    return ErrorResult.BadRequest(exception.Message);
                }

            });
        }

        [HttpPost("{entityType}/{entityId}/verify-aadhaar")]
        public async Task<IActionResult> VerifyAadhar(string entityType, string entityId,
            [FromBody] AadhaarKycRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Service.VerifyAadhaar(entityType, entityId, request));
                }
                catch (AadhaarBridgeNetworkException exception)
                {

                    return ErrorResult.BadRequest(exception.Message);
                }


            });
        }

    }
}
