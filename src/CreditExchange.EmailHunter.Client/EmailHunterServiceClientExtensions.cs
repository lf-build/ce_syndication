﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace CreditExchange.EmailHunter.Client
{
    public static class EmailHunterServiceClientExtensions
    {
        public static IServiceCollection AddEmailHunterService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IEmailHunterServiceClientFactory>(p => new EmailHunterServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IEmailHunterServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
