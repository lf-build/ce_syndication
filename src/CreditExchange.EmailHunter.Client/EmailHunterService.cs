﻿using CreditExchange.Syndication.EmailHunter;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Services;
using RestSharp;
using System.Threading.Tasks;

namespace CreditExchange.EmailHunter.Client
{
    public class EmailHunterService : IEmailHunterService
    {
        private IServiceClient Client { get; }

        public EmailHunterService(IServiceClient client)
        {
            Client = client;
        }

        public async Task<IDomainSearchResult> DomainSearch(string entityType, string entityId, string domain)
        {
            var request = new RestRequest("search-domain/{entitytype}/{entityid}/{domain}", Method.GET);
            request.AddUrlSegment("domain", domain);
            return await Client.ExecuteAsync<DomainSearchResult>(request);
        }

        public async Task<IEmailCountResult> EmailCount(string entityType, string entityId, string domain)
        {
            var request = new RestRequest("EmailCount/{entitytype}/{entityid}/{domain}", Method.GET);
            request.AddUrlSegment("domain", domain);
            return await Client.ExecuteAsync<EmailCountResult>(request);
        }

        public async Task<IEmailFinderResult> EmailFinder( IEmailFinder emailFinder)
        {
            var request = new RestRequest("search-email", Method.POST);
            request.AddJsonBody(emailFinder);
            return await Client.ExecuteAsync<EmailFinderResult>(request);
        }

        public async Task<IEmailVerificationResult> EmailVerifier(string entityType, string email)
        {
            var request = new RestRequest("verify-email/{entitytype}/{email}", Method.GET);
            request.AddParameter("entitytype", "application", ParameterType.UrlSegment);
            request.AddParameter("email", email, ParameterType.UrlSegment);
            return await Client.ExecuteAsync<EmailVerificationResult>(request);
        }
    }
}
