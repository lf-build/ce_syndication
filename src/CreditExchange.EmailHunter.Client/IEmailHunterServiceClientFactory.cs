﻿using CreditExchange.Syndication.EmailHunter;
using LendFoundry.Security.Tokens;

namespace CreditExchange.EmailHunter.Client
{
    public interface IEmailHunterServiceClientFactory
    {
        IEmailHunterService Create(ITokenReader reader);
    }
}
