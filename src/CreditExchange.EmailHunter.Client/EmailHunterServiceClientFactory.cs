﻿using CreditExchange.Syndication.EmailHunter;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;

namespace CreditExchange.EmailHunter.Client
{
    public class EmailHunterServiceClientFactory : IEmailHunterServiceClientFactory
    {
        public EmailHunterServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }
        
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public IEmailHunterService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new EmailHunterService(client);
        }
    }
}
