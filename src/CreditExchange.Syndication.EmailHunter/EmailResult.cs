﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.EmailHunter
{
    public class EmailResult : IEmailResult
    {
        public string Value { get; set; }
        public EmailType Type { get; set; }
        public int Confidence { get; set; }
        public IEnumerable<ISourceResult> Sources { get; set; }
    }
}