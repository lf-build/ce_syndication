﻿namespace CreditExchange.Syndication.EmailHunter
{
    public class EmailFinder :Source, IEmailFinder
    {
        public string Domain { get; set; }
        public string Company { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}