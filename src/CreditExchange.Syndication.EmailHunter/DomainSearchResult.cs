﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.EmailHunter
{
    public class DomainSearchResult : IDomainSearchResult
    {
        public string Domain { get; set; }
        public int Results { get; set; }
        public bool Webmail { get; set; }
        public string Pattern { get; set; }
        public int Offset { get; set; }
        public IEnumerable<IEmailResult> Emails { get; set; }
    }
}