﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.EmailHunter
{
    public class EmailFinderResult : IEmailFinderResult
    {
        public string Email { get; set; }
        public int Score { get; set; }
        public IEnumerable<ISourceResult> Sources { get; set; }
    }
}