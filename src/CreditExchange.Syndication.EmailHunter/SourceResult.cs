using System;

namespace CreditExchange.Syndication.EmailHunter
{
    public class SourceResult : ISourceResult
    {
        public string Domain { get; set; }
        public string Uri { get; set; }
        public DateTime ExtractedOn { get; set; }
    }
}