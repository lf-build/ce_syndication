namespace CreditExchange.Syndication.EmailHunter
{
    public class EmailCountResult : IEmailCountResult
    {
        public int Count { get; set; }
    }
}