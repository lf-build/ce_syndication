﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.EmailHunter
{
    public class EmailVerificationResult : IEmailVerificationResult
    {
        public string Email { get; set; }
        public string Result { get; set; }
        public int Score { get; set; }
        public bool Regexp { get; set; }
        public bool Gibberish { get; set; }
        public bool Disposable { get; set; }
        public bool Webmail { get; set; }
        public bool MxRecords { get; set; }
        public bool SmtpServer { get; set; }
        public bool SmtpCheck { get; set; }
        public bool AcceptAll { get; set; }
        public IEnumerable<ISourceResult> Sources { get; set; }
    }
}