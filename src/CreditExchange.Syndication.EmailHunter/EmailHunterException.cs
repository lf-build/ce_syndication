﻿using System;
using System.Runtime.Serialization;

namespace CreditExchange.Syndication.EmailHunter
{
    [Serializable]
    public class EmailHunterException : Exception
    {
        public EmailHunterException()
        {
        }

        public EmailHunterException(string message) : base(message)
        {
        }

        public EmailHunterException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EmailHunterException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
