﻿namespace CreditExchange.Syndication.EmailHunter
{
    public interface ISource
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
    }
}