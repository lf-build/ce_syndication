﻿namespace CreditExchange.Syndication.EmailHunter.Configuration
{
    public class EmailHunterRoutes : IEmailHunterRoutes
    {
        public string DomainSearch { get; set; } = "search";
        public string EmailFinder { get; set; } = "generate";
        public string EmailVerification { get; set; } = "verify";
        public string EmailCount { get; set; } = "email-count";
    }
}
