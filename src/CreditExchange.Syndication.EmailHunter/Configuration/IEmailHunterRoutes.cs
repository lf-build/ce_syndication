﻿namespace CreditExchange.Syndication.EmailHunter.Configuration
{
    public interface IEmailHunterRoutes
    {
        string DomainSearch { get; set; }
        string EmailFinder { get; set; }
        string EmailVerification { get; set; }
        string EmailCount { get; set; }
    }
}