﻿using System;

namespace CreditExchange.Syndication.EmailHunter.Configuration
{
    public static class EmailHunterConfigurationExtensions
    {
        public static string BaseUrl(this IEmailHunterConfiguration configuration)
        {
            return new Uri(configuration.ApiUrl + "/" + configuration.ApiVersion).AbsoluteUri;
        }
    }
}
