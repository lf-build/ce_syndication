﻿namespace CreditExchange.Syndication.EmailHunter.Configuration
{
    public class EmailHunterConfiguration : IEmailHunterConfiguration
    {
        public string ApiKey { get; set; } = "a787c4d7ceb14a723e162ed6fabec80ee2f8db9b";
        public string ApiUrl { get; set; } = "https://api.emailhunter.co/";
        public string ApiVersion { get; set; } = "v1";
        public int ExpirationInDays { get; set; }
    }
}
