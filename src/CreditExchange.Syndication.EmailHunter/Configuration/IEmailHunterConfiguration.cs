﻿namespace CreditExchange.Syndication.EmailHunter.Configuration
{
    public interface IEmailHunterConfiguration
    {
        string ApiKey { get; set; }
        string ApiUrl { get; set; }
        string ApiVersion { get; set; }
        int ExpirationInDays { get; set; }
    }
}