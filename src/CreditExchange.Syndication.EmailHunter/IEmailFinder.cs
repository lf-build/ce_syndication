﻿namespace CreditExchange.Syndication.EmailHunter
{
    public interface IEmailFinder :ISource
    {
        string Domain { get; set; }
        string Company { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
    }
}