﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.EmailHunter
{
    public interface IDomainSearchResult
    {
        string Domain { get; set; }
        int Results { get; set; }
        bool Webmail { get; set; }
        string Pattern { get; set; }
        int Offset { get; set; }
        IEnumerable<IEmailResult> Emails { get; set; }
    }
}