﻿using System;

namespace CreditExchange.Syndication.EmailHunter
{
    public interface ISourceResult
    {
        string Domain { get; set; }
        string Uri { get; set; }
        DateTime ExtractedOn { get; set; }
    }
}