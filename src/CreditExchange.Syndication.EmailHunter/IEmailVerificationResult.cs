﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.EmailHunter
{
    public interface IEmailVerificationResult
    {
        string Email { get; set; }
        string Result { get; set; }
        int Score { get; set; }
        bool Regexp { get; set; }
        bool Gibberish { get; set; }
        bool Disposable { get; set; }
        bool Webmail { get; set; }
        bool MxRecords { get; set; }
        bool SmtpServer { get; set; }
        bool SmtpCheck { get; set; }
        bool AcceptAll { get; set; }
        IEnumerable<ISourceResult> Sources { get; set; }
    }
}