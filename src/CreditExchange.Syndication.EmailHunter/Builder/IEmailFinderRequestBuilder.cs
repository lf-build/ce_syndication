﻿using CreditExchange.Syndication.EmailHunter.Request;

namespace CreditExchange.Syndication.EmailHunter.Builder
{
    public interface IEmailFinderRequestBuilder : IRequestBuilder<EmailFinderRequest>
    {
        IEmailFinderRequestBuilder WithDomain(string domain);
        IEmailFinderRequestBuilder WithName(string firstName, string lastName);
        IEmailFinderRequestBuilder WithOptions(string company = null);
    }
}
