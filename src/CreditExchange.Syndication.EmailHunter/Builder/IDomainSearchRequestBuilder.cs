﻿using CreditExchange.Syndication.EmailHunter.Request;

namespace CreditExchange.Syndication.EmailHunter.Builder
{
    public interface IDomainSearchRequestBuilder : IRequestBuilder<DomainSearchRequest>
    {
        IDomainSearchRequestBuilder WithDomain(string domain);
      
    }
}
