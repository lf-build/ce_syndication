﻿using CreditExchange.Syndication.EmailHunter.Request;

namespace CreditExchange.Syndication.EmailHunter.Builder
{
    public interface IEmailCountRequestBuilder : IRequestBuilder<EmailCountRequest>
    {
        IEmailCountRequestBuilder WithDomain(string domain);
    }
}
