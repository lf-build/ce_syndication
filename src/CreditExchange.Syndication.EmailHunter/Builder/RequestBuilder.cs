﻿using System;
using System.Collections.Generic;

namespace CreditExchange.Syndication.EmailHunter.Builder
{
    public abstract class RequestBuilder<T> : IRequestBuilder<T> where T : class, new() 
    {
        protected IList<Action<T>> BuildActions { get; } = new List<Action<T>>(); 
        public virtual T Build()
        {
            var request = new T();

            foreach (var action in BuildActions)
            {
                action(request);
            }

            return request;
        }
    }
}
