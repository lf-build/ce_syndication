﻿using CreditExchange.Syndication.EmailHunter.Request;

namespace CreditExchange.Syndication.EmailHunter.Builder
{
    public class EmailFinderRequestBuilder : RequestBuilder<EmailFinderRequest>, IEmailFinderRequestBuilder
    {
        public IEmailFinderRequestBuilder WithDomain(string domain)
        {
            BuildActions.Add(a => a.Domain = domain);
            return this;
        }

        public IEmailFinderRequestBuilder WithName(string firstName, string lastName)
        {
            BuildActions.Add(a => a.FirstName = firstName);
            BuildActions.Add(a => a.LastName = lastName);
            return this;
        }

        public IEmailFinderRequestBuilder WithOptions(string company = null)
        {
            BuildActions.Add(a => a.Company = company);
            return this;
        }
    }
}