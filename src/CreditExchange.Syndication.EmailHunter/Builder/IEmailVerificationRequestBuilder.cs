﻿using CreditExchange.Syndication.EmailHunter.Request;

namespace CreditExchange.Syndication.EmailHunter.Builder
{
    public interface IEmailVerificationRequestBuilder : IRequestBuilder<EmailVerificationRequest>
    {
        IEmailVerificationRequestBuilder WithEmail(string email);
    }

    public class EmailVerificationRequestBuilder : RequestBuilder<EmailVerificationRequest>, IEmailVerificationRequestBuilder
    {
        public IEmailVerificationRequestBuilder WithEmail(string email)
        {
            BuildActions.Add(a => a.Email = email);
            return this;
        }
    }
}
