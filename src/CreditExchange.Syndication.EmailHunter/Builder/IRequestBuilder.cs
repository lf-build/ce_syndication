﻿namespace CreditExchange.Syndication.EmailHunter.Builder
{
    public interface IRequestBuilder<out T> where T : class
    {
        T Build();
    }
}
