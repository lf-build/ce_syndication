﻿using CreditExchange.Syndication.EmailHunter.Request;

namespace CreditExchange.Syndication.EmailHunter.Builder
{
    public class DomainSearchRequestBuilder : RequestBuilder<DomainSearchRequest>, IDomainSearchRequestBuilder
    {
        public IDomainSearchRequestBuilder WithDomain(string domain)
        {
            BuildActions.Add(a => a.Domain = domain);
            return this;
        }
    }
}