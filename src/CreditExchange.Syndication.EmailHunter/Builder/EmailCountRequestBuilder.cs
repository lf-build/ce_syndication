using CreditExchange.Syndication.EmailHunter.Request;

namespace CreditExchange.Syndication.EmailHunter.Builder
{
    public class EmailCountRequestBuilder : RequestBuilder<EmailCountRequest>, IEmailCountRequestBuilder
    {
        public IEmailCountRequestBuilder WithDomain(string domain)
        {
            BuildActions.Add(a => a.Domain = domain);
            return this;
        }
    }
}