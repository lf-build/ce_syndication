﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.EmailHunter.Response
{
    public interface IEmailFinderResponse
    {
        string Status { get; set; }
        string Email { get; set; }
        int? Score { get; set; }
        IEnumerable<ISourceResponse> Sources { get; set; }
    }
}