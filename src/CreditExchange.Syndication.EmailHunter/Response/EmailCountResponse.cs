﻿namespace CreditExchange.Syndication.EmailHunter.Response
{
    public class EmailCountResponse : IEmailCountResponse
    {
        public string Status { get; set; }
        public int Count { get; set; }
    }
}
