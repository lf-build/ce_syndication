﻿using System.Collections.Generic;
using CreditExchange.Syndication.EmailHunter.Infrastructure;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.EmailHunter.Response
{
    public class DomainSearchResponse : IDomainSearchResponse
    {
        public string Status { get; set; }
        public string Domain { get; set; }
        public int Results { get; set; }
        public bool Webmail { get; set; }
        public string Pattern { get; set; }
        public int Offset { get; set; }
        [JsonConverter(typeof(ConcreteTypeConverter<IEnumerable<EmailResponse>>))]
        public IEnumerable<IEmailResponse> Emails { get; set; }
    }
}
