﻿using System.Collections.Generic;
using CreditExchange.Syndication.EmailHunter.Infrastructure;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.EmailHunter.Response
{
    public class EmailResponse : IEmailResponse
    {
        public string Value { get; set; }
        public EmailType Type { get; set; }
        public int Confidence { get; set; }
        [JsonConverter(typeof(ConcreteTypeConverter<IEnumerable<SourceResponse>>))]
        public IEnumerable<ISourceResponse> Sources { get; set; }
    }
}
