﻿namespace CreditExchange.Syndication.EmailHunter.Response
{
    public interface IEmailCountResponse
    {
        string Status { get; set; }
        int Count { get; set; }
    }
}