﻿using System.Collections.Generic;
using CreditExchange.Syndication.EmailHunter.Infrastructure;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.EmailHunter.Response
{
    public class EmailVerificationResponse : IEmailVerificationResponse
    {
        public string Status { get; set; }
        public string Email { get; set; }
        public string Result { get; set; }
        public int Score { get; set; }
        public bool Regexp { get; set; }
        public bool Gibberish { get; set; }
        public bool Disposable { get; set; }
        public bool Webmail { get; set; }
        [JsonProperty("mx_records")]
        public bool MxRecords { get; set; }
        [JsonProperty("smtp_server")]
        public bool SmtpServer { get; set; }
        [JsonProperty("smtp_check")]
        public bool SmtpCheck { get; set; }
        [JsonProperty("accept_all")]
        public bool AcceptAll { get; set; }
        [JsonConverter(typeof(ConcreteTypeConverter<IEnumerable<SourceResponse>>))]
        public IEnumerable<ISourceResponse> Sources { get; set; }
    }
}
