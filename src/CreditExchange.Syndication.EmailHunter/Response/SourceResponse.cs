﻿using System;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.EmailHunter.Response
{
    public class SourceResponse : ISourceResponse
    {
        public string Domain { get; set; }
        public string Uri { get; set; }
        [JsonProperty("extracted_on")]
        public DateTime ExtractedOn { get; set; }
    }
}
