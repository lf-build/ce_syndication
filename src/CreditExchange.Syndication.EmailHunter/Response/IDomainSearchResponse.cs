using System.Collections.Generic;

namespace CreditExchange.Syndication.EmailHunter.Response
{
    public interface IDomainSearchResponse
    {
        string Status { get; set; }
        string Domain { get; set; }
        int Results { get; set; }
        bool Webmail { get; set; }
        string Pattern { get; set; }
        int Offset { get; set; }
        IEnumerable<IEmailResponse> Emails { get; set; }
    }
}