﻿using System.Collections.Generic;
using CreditExchange.Syndication.EmailHunter.Infrastructure;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.EmailHunter.Response
{
    public class EmailFinderResponse : IEmailFinderResponse
    {
        public string Status { get; set; }
        public string Email { get; set; }
        public int? Score { get; set; }
        [JsonConverter(typeof(ConcreteTypeConverter<IEnumerable<SourceResponse>>))]
        public IEnumerable<ISourceResponse> Sources { get; set; }
    }
}
