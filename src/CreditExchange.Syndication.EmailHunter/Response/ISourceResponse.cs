﻿using System;

namespace CreditExchange.Syndication.EmailHunter.Response
{
    public interface ISourceResponse
    {
        string Domain { get; set; }
        string Uri { get; set; }
        DateTime ExtractedOn { get; set; }
    }
}