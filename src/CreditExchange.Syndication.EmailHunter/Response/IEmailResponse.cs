using System.Collections.Generic;

namespace CreditExchange.Syndication.EmailHunter.Response
{
    public interface IEmailResponse
    {
        string Value { get; set; }
        EmailType Type { get; set; }
        int Confidence { get; set; }
        IEnumerable<ISourceResponse> Sources { get; set; }
    }
}