namespace CreditExchange.Syndication.EmailHunter.Request
{
    public interface IEmailVerificationRequest
    {
        string Email { get; set; }
    }
}