﻿namespace CreditExchange.Syndication.EmailHunter.Request
{
    public interface IEmailFinderRequest
    {
        string Domain { get; set; }
        string Company { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
    }
}