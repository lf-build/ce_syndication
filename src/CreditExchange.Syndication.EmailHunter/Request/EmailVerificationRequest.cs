﻿namespace CreditExchange.Syndication.EmailHunter.Request
{
    public class EmailVerificationRequest : IEmailVerificationRequest
    {
        public string Email { get; set; }
    }
}