﻿namespace CreditExchange.Syndication.EmailHunter.Request
{
    public interface IEmailCountRequest
    {
        string Domain { get; set; }
    }
}