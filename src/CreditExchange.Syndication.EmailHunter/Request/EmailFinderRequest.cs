﻿namespace CreditExchange.Syndication.EmailHunter.Request
{
    public class EmailFinderRequest : IEmailFinderRequest
    {
        public string Domain { get; set; }
        public string Company { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
