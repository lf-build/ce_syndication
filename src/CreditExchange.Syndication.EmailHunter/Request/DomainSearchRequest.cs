﻿namespace CreditExchange.Syndication.EmailHunter.Request
{
    public class DomainSearchRequest : IDomainSearchRequest
    {
    
        public string Domain { get; set; }
        
    }
}
