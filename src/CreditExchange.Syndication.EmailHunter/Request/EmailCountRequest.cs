﻿namespace CreditExchange.Syndication.EmailHunter.Request
{
    public class EmailCountRequest : IEmailCountRequest
    {
        public string Domain { get; set; }
    }
}
