﻿using System;
using System.Threading.Tasks;
using CreditExchange.Syndication.EmailHunter.Builder;
using CreditExchange.Syndication.EmailHunter.Client;
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.EmailHunter.Request;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using System.Linq;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.EmailHunter
{
    public class EmailHunterService : IEmailHunterService
    {
        private IEmailHunterProxy EmailHunterProxy { get; }
        private ILookupService Lookup { get; }
        private ILogger Logger { get; }
        public EmailHunterService(IEmailHunterProxy proxy, ILogger logger, ILookupService lookup)
        {
            EmailHunterProxy = proxy;
            Lookup = lookup;
            Logger = logger;
        }
      
        public async Task<IDomainSearchResult> DomainSearch(string entityType, string entityId, string domain)
        {   
         if (string.IsNullOrWhiteSpace(domain))
                throw new InvalidArgumentException($"#{nameof(domain)} cannot be null", nameof(domain));
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException(entityId);
            entityType = EnsureEntityType(entityType);
            Logger.Info($"[Email Hunter] DomainSearch method invoked for [{entityType}] with id #{entityId} using  Domain  {domain}");
            try
            {
                var domainSearchResponse = await EmailHunterProxy.DomainSearch(new DomainSearchRequest { Domain = domain });
                return domainSearchResponse.ToDomainSearch();
            }
            catch(EmailHunterException exception)
            {
                Logger.Error("[Email Hunter] Error occured when DomainSearch method was called. Error: ", exception);
                throw new EmailHunterException(exception.Message);
            }
        }
     
        public async Task<IEmailFinderResult> EmailFinder(IEmailFinder emailFinder)
        {
            if (emailFinder == null)
                throw new ArgumentNullException(nameof(emailFinder));
           if (string.IsNullOrWhiteSpace(emailFinder.Domain) && string.IsNullOrWhiteSpace(emailFinder.Company))
               throw new ArgumentException("Either domain or company name is required");
           if (string.IsNullOrWhiteSpace(emailFinder.FirstName))
               throw new ArgumentException("FirstName is require");
           if (string.IsNullOrWhiteSpace(emailFinder.LastName))
               throw new ArgumentException("LastName is require");
            if (string.IsNullOrWhiteSpace(emailFinder.EntityType))
                throw new ArgumentException(nameof(emailFinder.EntityType));
            if (string.IsNullOrWhiteSpace(emailFinder.EntityId))
                throw new ArgumentException(nameof(emailFinder.EntityId));
            emailFinder.EntityType = EnsureEntityType(emailFinder.EntityType);
            Logger.Info($"[Email Hunter] DomainSearch method invoked for data { (emailFinder != null ? JsonConvert.SerializeObject(emailFinder) : null)}");
            try
            {
                var emailFinderRequest = new EmailFinderRequestBuilder()
                    .WithDomain(emailFinder.Domain)
                    .WithName(emailFinder.FirstName, emailFinder.LastName)
                    .WithOptions(emailFinder.Company)
                    .Build();

                var emailFinderResponse = await EmailHunterProxy.EmailFinder(emailFinderRequest);
                return emailFinderResponse.ToEmailFinder();
            }
            catch(EmailHunterException exception)
            {
                Logger.Error("[Email Hunter] Error occured when DomainSearch method was called. Error: ", exception);
                throw new EmailHunterException(exception.Message);
            }
        }

        public async Task<IEmailVerificationResult> EmailVerifier(string entityType, string email)
        {
           
            if (string.IsNullOrWhiteSpace(email))
                throw new InvalidArgumentException($"#{nameof(email)} cannot be null", nameof(email));
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException(nameof(entityType));
           
            entityType = EnsureEntityType(entityType);
            var emailVerificationRequest = new EmailVerificationRequestBuilder()
                .WithEmail(email)
                .Build();
           
            var emailVerificationResponse = await EmailHunterProxy.EmailVerifier(emailVerificationRequest);
            
            return emailVerificationResponse.ToEmailVerification();
        }

        public async Task<IEmailCountResult> EmailCount(string entityType, string entityId, string domain)
        {
           
            if (string.IsNullOrWhiteSpace(domain))
               throw new ArgumentException(nameof(domain));
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException(entityId);
            entityType = EnsureEntityType(entityType);
            var emailCountRequest = new EmailCountRequestBuilder()
                .WithDomain(domain)
                .Build();
           
            var emailCountResponse = await EmailHunterProxy.EmailCount(emailCountRequest);
            if (emailCountResponse == null)
                throw new NotFoundException("domain search Report not found.");
       
            return emailCountResponse.ToEmailCount();
        }

        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }

    }
}