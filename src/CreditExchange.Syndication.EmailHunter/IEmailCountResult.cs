﻿namespace CreditExchange.Syndication.EmailHunter
{
    public interface IEmailCountResult
    {
        int Count { get; set; }
    }
}