﻿using System.Threading.Tasks;
using CreditExchange.Syndication.EmailHunter.Request;
using CreditExchange.Syndication.EmailHunter.Response;

namespace CreditExchange.Syndication.EmailHunter.Client
{
    public interface IEmailHunterProxy
    {
        Task<IDomainSearchResponse> DomainSearch(IDomainSearchRequest domainSearch);
        Task<IEmailFinderResponse> EmailFinder(IEmailFinderRequest emailFinder);
        Task<IEmailVerificationResponse> EmailVerifier(IEmailVerificationRequest emailVerification);
        Task<IEmailCountResponse> EmailCount(IEmailCountRequest emailCount);
    }
}
