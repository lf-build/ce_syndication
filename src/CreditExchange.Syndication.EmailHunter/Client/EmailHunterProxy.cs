﻿using System;
using System.Net;
using System.Threading.Tasks;
using CreditExchange.Syndication.EmailHunter.Configuration;
using CreditExchange.Syndication.EmailHunter.Request;
using CreditExchange.Syndication.EmailHunter.Response;
using Newtonsoft.Json;
using RestSharp;

namespace CreditExchange.Syndication.EmailHunter.Client
{
    public class EmailHunterProxy : IEmailHunterProxy
    {
        private IEmailHunterConfiguration Configuration { get; }
        private IEmailHunterRoutes Routes { get; }

        public EmailHunterProxy() : this(new EmailHunterConfiguration(), new EmailHunterRoutes())
        {
            
        }

        public EmailHunterProxy(IEmailHunterConfiguration configuration) : this(configuration, new EmailHunterRoutes())
        {
            
        }

        public EmailHunterProxy(IEmailHunterConfiguration configuration, IEmailHunterRoutes routes)
        {
            ValidateConfigurationAndRoutes(configuration, routes);

            Configuration = configuration;
            Routes = routes;
        }

        private static void ValidateConfigurationAndRoutes(IEmailHunterConfiguration configuration, IEmailHunterRoutes routes)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            if (routes == null)
            {
                throw new ArgumentNullException(nameof(routes));
            }

            if (string.IsNullOrWhiteSpace(configuration.ApiKey))
            {
                throw new ArgumentException(nameof(configuration.ApiKey));
            }
            if (string.IsNullOrWhiteSpace(configuration.ApiVersion))
            {
                throw new ArgumentException(nameof(configuration.ApiVersion));
            }

            if (string.IsNullOrWhiteSpace(routes.DomainSearch))
            {
                throw new ArgumentException(nameof(routes.DomainSearch));
            }

            if (string.IsNullOrWhiteSpace(routes.EmailFinder))
            {
                throw new ArgumentException(nameof(routes.EmailFinder));
            }

            if (string.IsNullOrWhiteSpace(routes.EmailVerification))
            {
                throw new ArgumentException(nameof(routes.EmailVerification));
            }

            if (string.IsNullOrWhiteSpace(routes.EmailCount))
            {
                throw new ArgumentException(nameof(routes.EmailCount));
            }
        }

        private static T ValidateAndConvertResponse<T>(IRestResponse response)
        {
            if (response.ErrorException != null)
                throw new EmailHunterException("EmailHunter request failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK)
                throw new EmailHunterException(
                    $"EmailHunter request failed. Status {response.ResponseStatus}. Response {response.Content ?? ""}");

            return JsonConvert.DeserializeObject<T>(response.Content);
        }

        public async Task<IDomainSearchResponse> DomainSearch(IDomainSearchRequest domainSearch)
        {
            var client = new RestClient(Configuration.BaseUrl());
            var request = new RestRequest(Routes.DomainSearch, Method.GET);

            request.AddQueryParameter("domain", domainSearch.Domain);
            //request.AddQueryParameter("company", domainSearch.Company);
            //request.AddQueryParameter("offset", domainSearch.Offset.ToString());

            //if (domainSearch.Type != EmailType.None)
            //{
            //    request.AddQueryParameter("type", domainSearch.Type.ToString().ToLower());

            //}

            request.AddQueryParameter("api_key", Configuration.ApiKey);

            var response = await client.ExecuteTaskAsync(request);

            return ValidateAndConvertResponse<DomainSearchResponse>(response);
        }

        public async Task<IEmailFinderResponse> EmailFinder(IEmailFinderRequest emailFinder)
        {
            var client = new RestClient(Configuration.BaseUrl());
            var request = new RestRequest(Routes.EmailFinder, Method.GET);

            request.AddQueryParameter("domain", emailFinder.Domain);
            request.AddQueryParameter("company", emailFinder.Company);
            request.AddQueryParameter("first_name", emailFinder.FirstName);
            request.AddQueryParameter("last_name", emailFinder.LastName);
            request.AddQueryParameter("api_key", Configuration.ApiKey);

            var response = await client.ExecuteTaskAsync(request);

            return ValidateAndConvertResponse<EmailFinderResponse>(response);
        }

        public async Task<IEmailVerificationResponse> EmailVerifier(IEmailVerificationRequest emailVerification)
        {
            var client = new RestClient(Configuration.BaseUrl());
            var request = new RestRequest(Routes.EmailVerification, Method.GET);

            request.AddQueryParameter("email", emailVerification.Email);
            request.AddQueryParameter("api_key", Configuration.ApiKey);

            var response = await client.ExecuteTaskAsync(request);

            return ValidateAndConvertResponse<EmailVerificationResponse>(response);
        }

        public async Task<IEmailCountResponse> EmailCount(IEmailCountRequest emailCount)
        {
            var client = new RestClient(Configuration.BaseUrl());
            var request = new RestRequest(Routes.EmailCount, Method.GET);

            request.AddQueryParameter("domain", emailCount.Domain);
            request.AddQueryParameter("api_key", Configuration.ApiKey);

            var response = await client.ExecuteTaskAsync(request);

            return ValidateAndConvertResponse<EmailCountResponse>(response);
        }
    }
}