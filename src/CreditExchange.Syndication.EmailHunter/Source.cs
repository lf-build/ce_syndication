﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.EmailHunter
{
    public class Source
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
    }
}
