﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.EmailHunter
{
    public interface IEmailResult
    {
        string Value { get; set; }
        EmailType Type { get; set; }
        int Confidence { get; set; }
        IEnumerable<ISourceResult> Sources { get; set; }
    }
}