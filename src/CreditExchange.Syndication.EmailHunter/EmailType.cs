﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CreditExchange.Syndication.EmailHunter
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum EmailType
    {
        None,
        Generic,
        Personal
    }
}
