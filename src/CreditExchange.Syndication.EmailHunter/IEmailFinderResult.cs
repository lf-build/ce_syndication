﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.EmailHunter
{
    public interface IEmailFinderResult
    {
        string Email { get; set; }
        int Score { get; set; }
        IEnumerable<ISourceResult> Sources { get; set; }
    }
}