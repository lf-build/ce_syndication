﻿using System.Collections.Generic;
using System.Linq;
using CreditExchange.Syndication.EmailHunter.Response;

namespace CreditExchange.Syndication.EmailHunter
{
    public static class EmailHunterExtensions
    {
        public static IDomainSearchResult ToDomainSearch(this IDomainSearchResponse domainSearch)
        {
            return new DomainSearchResult
            {
                Domain = domainSearch.Domain,
                Offset = domainSearch.Offset,
                Pattern = domainSearch.Pattern,
                Results = domainSearch.Results,
                Webmail = domainSearch.Webmail,
                Emails = domainSearch.Emails.ToEmails()
            };
        }

        public static IEmailFinderResult ToEmailFinder(this IEmailFinderResponse emailFinder)
        {
            return new EmailFinderResult
            {
                Email = emailFinder.Email,
                Score = emailFinder.Score == null ?0:emailFinder.Score.Value,
                Sources = emailFinder.Sources.ToSources()
            };
        }

        public static IEmailVerificationResult ToEmailVerification(this IEmailVerificationResponse emailVerification)
        {
            return new EmailVerificationResult
            {
                Email = emailVerification.Email,
                Score = emailVerification.Score,
                Disposable = emailVerification.Disposable,
                AcceptAll = emailVerification.AcceptAll,
                MxRecords = emailVerification.MxRecords,
                Regexp = emailVerification.Regexp,
                Result = emailVerification.Result,
                SmtpCheck = emailVerification.SmtpCheck,
                SmtpServer = emailVerification.SmtpServer,
                Gibberish = emailVerification.Gibberish,
                Webmail = emailVerification.Webmail,
                Sources = emailVerification.Sources.ToSources()
            };
        }

        public static IEmailCountResult ToEmailCount(this IEmailCountResponse emailCount)
        {
            return new EmailCountResult
            {
                Count = emailCount.Count
            };
        }

        public static IEmailResult ToEmail(this IEmailResponse email)
        {
            return new EmailResult
            {
                Value = email.Value,
                Confidence = email.Confidence,
                Type = email.Type,
                Sources = email.Sources.ToSources()
            };
        }

        public static IEnumerable<IEmailResult> ToEmails(this IEnumerable<IEmailResponse> emails)
        {
            return emails.Select(email => email.ToEmail());
        } 

        public static IEnumerable<ISourceResult> ToSources(this IEnumerable<ISourceResponse> sources)
        {
            return sources.Select(source => source.ToSource());
        }

        public static ISourceResult ToSource(this ISourceResponse source)
        {
            return new SourceResult
            {
                Domain = source.Domain,
                Uri = source.Uri,
                ExtractedOn = source.ExtractedOn
            };
        }
    }
}
