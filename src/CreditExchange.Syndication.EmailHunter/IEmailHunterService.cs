﻿using System.Threading.Tasks;

namespace CreditExchange.Syndication.EmailHunter
{
    public interface IEmailHunterService
    {
        Task<IDomainSearchResult> DomainSearch(string entityType, string entityId, string domain);
        Task<IEmailFinderResult> EmailFinder(IEmailFinder emailFinder);
        Task<IEmailVerificationResult> EmailVerifier(string entityType, string email);
        Task<IEmailCountResult> EmailCount(string entityType, string entityId, string domain);
    }
}
