﻿using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Zumigo.Client
{
    public interface IZumigoServiceClientFactory
    {
        IZumigoService Create(ITokenReader reader);
    }
}
