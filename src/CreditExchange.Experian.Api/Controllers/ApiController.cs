﻿using CreditExchange.Syndication.Experian;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
#if DOTNET2
using  Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.Threading.Tasks;

namespace CreditExchange.Experian.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IExperianService experianService, ILogger logger) : base(logger)
        {
            if (experianService == null)
                throw new ArgumentNullException(nameof(experianService));

            ExperianService = experianService;

        }
        private IExperianService ExperianService;

        [HttpPost("{entitytype}/{entityid}")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.Experian.IAbridgedReportResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetExperianReport(string entitytype, string entityid,
          [FromBody] SingleActionRequest request)
        {

            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await ExperianService.GetReport(entitytype, entityid, request));
                }
                catch (ExperianException exception)
                {
                    Logger.Error($"[Experian] Error occured when Fetch Report endpoint was called for [{entitytype}] with id #{entityid}", exception);
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
    }
}
