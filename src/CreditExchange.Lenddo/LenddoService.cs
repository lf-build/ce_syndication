﻿using System;
using System.Threading.Tasks;
using CreditExchange.Lenddo.Abstractions;
using CreditExchange.Syndication.Lenddo;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Logging;
using CreditExchange.Syndication.Lenddo.Events;
using LendFoundry.Foundation.Services;
using System.Linq;

namespace CreditExchange.Lenddo
{
    public class LenddoService : ILenddoService
    {
        public LenddoService(ILenddoSyndicationService lenddoSydicationService, IEventHubClient eventHub, ILookupService lookup, ILogger logger, ILenddoRepository lenddoRepository)
        {
            if (lenddoSydicationService == null)
                throw new ArgumentNullException(nameof(lenddoSydicationService));
            EventHub = eventHub;
            Lookup = lookup;
            Logger = logger;
            LenddoSydicationService = lenddoSydicationService;
            LenddoRepository = lenddoRepository;
        }
        public static string ServiceName { get; } = "lenddo";
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; } 
        private ILogger Logger { get; }
        private ILenddoSyndicationService LenddoSydicationService { get; }
        private ILenddoRepository LenddoRepository { get; }
        public async Task<IGetApplicationScoreResponse> GetClientScore(string entityType, string entityId, string clientId)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(clientId))
                throw new ArgumentException("clientId cannot be null or whitespace.", nameof(clientId));
            //var lenddoRepository = await LenddoRepository.GetByEntityId(entityType, entityId);
            //if (lenddoRepository != null)
            //{
            //    if(!lenddoRepository.Islinked)
            //    {
            //        throw new LenddoException("NOT_FOUND");
            //    }
            //}
            //else
            //{
            //    throw new LenddoException("NOT_FOUND");
            //}

            var referencenumber = Guid.NewGuid().ToString("N");
            Logger.Info($"[Lenddo] GetClientVerification method invoked for [{entityType}] with id #{entityId} using  clientId : {clientId}");
            var response = await Task.Run(() => LenddoSydicationService.GetClientScore(entityType, entityId,clientId));
            Logger.Info($"[Lenddo] Publishing LenddoClientVerificationRequested event for [{entityType}] with id #{entityId}");
            await EventHub.Publish(new LenddoSocialScoreRequested
            {    
                EntityId = entityId,
                EntityType = entityType,
                Response = response,
                Request = clientId,
                ReferenceNumber = referencenumber,
                Name = ServiceName

            });
            return response;
        }

        public async Task<IGetApplicationVerificationResponse> GetClientVerification(string entityType, string entityId, string clientId)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(clientId))
                throw new ArgumentException("clientId cannot be null or whitespace.", nameof(clientId));
            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                Logger.Info($"[Lenddo] GetClientScore method invoked for [{entityType}] with id #{entityId} using  clientId : {clientId}");
                var response = await Task.Run(() => LenddoSydicationService.GetClientVerification(entityType, entityId, clientId));
                
                Logger.Info($"[Lenddo] Publishing LenddoSocialScoreRequested event for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new LenddoClientVerificationRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = response,
                    Request = clientId,
                    ReferenceNumber = referencenumber,
                    Name = ServiceName
                });
                return response;
            }
            catch (LenddoException exception)
            {
                Logger.Info($"[Lenddo] Publishing LenddoSocialScoreFail event for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new LenddoSocialScoreFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = clientId,
                    ReferenceNumber = null,
                    Name = ServiceName
                });
                Logger.Error("[Lenddo] Error occured when GetClientScore method was called. Error: ", exception);
                throw new LenddoException(exception.Message);// new Exception(exception.Message);
            }
        }
        public async Task InitiateSocialLinking(string entityType, string entityId)
        {
            entityType = EnsureEntityType(entityType);
            var referencenumber = Guid.NewGuid().ToString("N");
            Logger.Info($"[Lenddo] InitiateSocialLinking method invoked for [{entityType}] with id #{entityId}");
            var lenddoRepository = await LenddoRepository.GetByEntityId(entityType, entityId);
            if(lenddoRepository!=null)
            {
                lenddoRepository.Islinked = true;
                LenddoRepository.Update(lenddoRepository);
            }
            else
            {
                LenddoData data = new LenddoData();
                data.Islinked = true;
                data.EntityId = entityId;
                data.EntityType = entityType;
                LenddoRepository.Add(data);
            }
 

            Logger.Info($"[Lenddo] Publishing LenddoSocialLinkingInitiated event for [{entityType}] with id #{entityId}");
            await EventHub.Publish(new LenddoSocialLinkingInitiated
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = entityId,
                Request = entityId,
                ReferenceNumber = referencenumber,
                Name = ServiceName

            });
           
        }
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}
