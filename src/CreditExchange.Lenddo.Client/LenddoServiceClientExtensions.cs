﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace CreditExchange.Lenddo.Client
{
    public static class LenddoServiceClientExtensions
    {
        public static IServiceCollection AddLenddoService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ILenddoServiceClientFactory>(p => new LenddoServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ILenddoServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
