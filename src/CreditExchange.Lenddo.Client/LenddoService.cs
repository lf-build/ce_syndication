﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using CreditExchange.Syndication.Lenddo;
using RestSharp;
using CreditExchange.Lenddo.Abstractions;

namespace CreditExchange.Lenddo.Client
{
    public class LenddoService : ILenddoService
    {
        public LenddoService(IServiceClient client)
        {
            Client = client;
        }
        private IServiceClient Client { get; }

        public async Task<IGetApplicationVerificationResponse> GetClientVerification(string entityType, string entityId, string clientId)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/{clientid}/verification", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddUrlSegment("clientid", clientId);
            return await Client.ExecuteAsync<GetApplicationVerificationResponse>(request);
        }


        public async Task<IGetApplicationScoreResponse> GetClientScore(string entityType, string entityId, string clientId)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/{clientId}/score", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddUrlSegment("clientid", clientId);
            return await Client.ExecuteAsync<GetApplicationScoreResponse>(request);
        }

    }
}
