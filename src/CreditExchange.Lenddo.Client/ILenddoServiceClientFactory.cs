﻿using LendFoundry.Security.Tokens;
using CreditExchange.Syndication.Lenddo;
using CreditExchange.Lenddo.Abstractions;

namespace CreditExchange.Lenddo.Client
{
   public   interface ILenddoServiceClientFactory
    {
        ILenddoService Create(ITokenReader reader);
    }
}
