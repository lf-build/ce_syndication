﻿using System;
using LendFoundry.Security.Tokens;
using CreditExchange.Lenddo.Abstractions;
using LendFoundry.Foundation.Client;

namespace CreditExchange.Lenddo.Client
{
    public class LenddoServiceClientFactory : ILenddoServiceClientFactory
    {
        public LenddoServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public ILenddoService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new LenddoService(client);
        }
    }
}
