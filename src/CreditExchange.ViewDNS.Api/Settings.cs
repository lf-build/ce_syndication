﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.ViewDNS.Api
{
    /// <summary>
    /// Settings class
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// Gets the name of the service.
        /// </summary>
        /// <value>
        /// The name of the service.
        /// </value>
        public static string ServiceName { get; } = "view-dns";
        /// <summary>
        /// Gets the prefix.
        /// </summary>
        /// <value>
        /// The prefix.
        /// </value>
        private static string Prefix { get; } = ServiceName.ToUpper();
        /// <summary>
        /// Gets the event hub.
        /// </summary>
        /// <value>
        /// The event hub.
        /// </value>
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");
        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");
        /// <summary>
        /// Gets the company.
        /// </summary>
        /// <value>
        /// The company.
        /// </value>
        public static ServiceSettings Company { get; } = new ServiceSettings($"{Prefix}_COMPANY", "company-db");
        /// <summary>
        /// Gets the lookup service.
        /// </summary>
        /// <value>
        /// The lookup service.
        /// </value>
        public static ServiceSettings LookupService { get; } = new ServiceSettings($"{Prefix}_LOOKUPSERVICE", "lookup-service");
        /// <summary>
        /// Gets the email hunter service.
        /// </summary>
        /// <value>
        /// The email hunter service.
        /// </value>
        //public static ServiceSettings EmailHunterService { get; } = new ServiceSettings($"{Prefix}_EMAIL-HUNTER", "syndication-email-hunter");
        /// <summary>
        /// Gets the email verification service.
        /// </summary>
        /// <value>
        /// The email verification service.
        /// </value>
        public static ServiceSettings EmailVerificationService { get; } = new ServiceSettings($"{Prefix}_EMAIL-VERIFICATION", "email-verification");
        /// <summary>
        /// Gets the data attributes.
        /// </summary>
        /// <value>
        /// The data attributes.
        /// </value>
        public static ServiceSettings DataAttributes { get; } = new ServiceSettings($"{Prefix}_DATAATTRIBUTES_HOST", "data-attributes", $"{Prefix}_DATAATTRIBUTES_PORT");
        public static ServiceSettings DecisionEngine { get; } = new ServiceSettings($"{Prefix}_DECISIONENGINE_HOST", "decision-engine", $"{Prefix}_DECISIONENGINE_PORT");
        //public static ServiceSettings TowerData { get; } = new ServiceSettings($"{Prefix}_TOWERDATA_HOST", "syndication-towerdata", $"{Prefix}_TOWERDATA_PORT");
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
    }
}
