﻿using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.ViewDNS;
#if DOTNET2
using  Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;

namespace CreditExchange.ViewDNS.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Gets the service.
        /// </summary>
        /// <value>
        /// The service.
        /// </value>
        private IViewDnsService Service { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiController"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="logger"></param>
        public ApiController(IViewDnsService service, ILogger logger):base (logger)
        {
            Service = service;
            Logger = logger;
        }

        private ILogger Logger { get; }

        /// <summary>
        /// Gets the employee verification.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="companyName">Name of the company.</param>
        /// <param name="cin">The cin.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/{emailAddress}/{companyName}/{cin}")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.ViewDNS.Response.IEmployeeVerification), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> VerifyEmployment(string entityType, string entityId, string emailAddress, string companyName, string cin)
        {
            Logger.Info($"[ViewDNS] VerifyEmployment endpoint invoked for [{entityType}] with id #{entityId} with data - email address#{emailAddress}, company name #{companyName}, cin #{cin}");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var decodedString = System.Net.WebUtility.UrlDecode(companyName);
                    var decodedEmailAddress = System.Net.WebUtility.UrlDecode(emailAddress);
                    var response = await Service.VerifyEmployment(entityType, entityId, decodedEmailAddress, decodedString, cin);
                    return Ok(response);
                }
                catch (ViewDnsException exception)
                {
                    Logger.Error($"[ViewDNS] Error occured when VerifyEmployment endpoint was called for [{entityType}] with id #{entityId} with data - email address#{emailAddress}, company name #{companyName}, cin #{cin}", exception);
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
    }
}
