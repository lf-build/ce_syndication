﻿using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.ViewDNS;
using LendFoundry.Configuration.Client;
using CreditExchange.Syndication.ViewDNS.Proxy;
using LendFoundry.EventHub.Client;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup.Client;
using CreditExchange.CompanyDb.Client;
using LendFoundry.EmailVerification.Client;
using LendFoundry.DataAttributes.Client;
//using CreditExchange.Towedata.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration;
//using CreditExchange.EmailHunter.Client;

namespace CreditExchange.ViewDNS.Api
{
    /// <summary>
    /// Startup class
    /// </summary>
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container
        /// <summary>
        /// Configures the services.
        /// </summary>
        /// <param name="services">The services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "ViewDNS"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "CreditExchange.ViewDNS.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();

            // interface implements
            services.AddConfigurationService<ViewDnsConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddCompanyService(Settings.Company.Host, Settings.Company.Port);
            services.AddLookupService(Settings.LookupService.Host, Settings.LookupService.Port);
            services.AddDecisionEngine(Settings.DecisionEngine.Host, Settings.DecisionEngine.Port);
            //services.AddTowerdataService(Settings.TowerData.Host, Settings.TowerData.Port);
            //services.AddEmailHunterService(Settings.EmailHunterService.Host, Settings.EmailHunterService.Port);
            services.AddEmailVerificationService(Settings.EmailVerificationService.Host, Settings.EmailVerificationService.Port);
            services.AddDataAttributes(Settings.DataAttributes.Host, Settings.DataAttributes.Port);

            // Configuration factory
            services.AddTransient<IViewDnsConfiguration>(p =>
            {
                var configuration = p.GetService<IConfigurationService<ViewDnsConfiguration>>().Get();
                return configuration;
            });

            services.AddTransient<IViewDnsService, ViewDnsService>();
            services.AddTransient<IViewDnsProxy, ViewDnsProxy>();
            services.AddTransient<IDomainMatching, DomainMatching>();
            services.AddTransient<IFilterDomain, FilterDomain>();
            services.AddTransient<ICompanyNameGenerator, CompanyNameGenerator>();
         
            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        /// <summary>
        /// Configures the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="env">The env.</param>
        /// <param name="loggerFactory">The logger factory.</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseHealthCheck();
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ViewDNS Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
        }

    }
}
