﻿using CreditExchange.Syndication.YodleeYsl.Request;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Lookup;
using System.Linq;

using CreditExchange.Syndication.YodleeYsl;
using CreditExchange.Syndication.YodleeYsl.Proxy;
using CreditExchange.Syndication.YodleeYsl.Response;
using CreditExchange.YodleeYsl.Abstractions;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using Newtonsoft.Json;

namespace CreditExchange.YodleeYsl
{
    public class YodleeYslService : IYodleeYslService
    {
        public YodleeYslService(IYodleeYslRestProxy proxy, IYodleeRepository yodleeRepository, IEventHubClient eventHub, ILookupService lookup, ITenantTime tenantTime, ILogger logger)
        {
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));
            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));
            EventHub = eventHub;
            YodleeRepositoy = yodleeRepository;
            Proxy = proxy;
            Lookup = lookup;
            TenantTime = tenantTime;
            Logger = logger;
        }
        private ILookupService Lookup { get; }
        private IYodleeRepository YodleeRepositoy { get; set; }
        private IYodleeYslRestProxy Proxy { get; }
        private IEventHubClient EventHub { get; }
      
        private ITenantTime TenantTime { get; }
        private ILogger Logger { get; }
        public async Task<ICobrandLoginResponse> LoginForCobrand(string entityType,string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            ICobrandLoginResponse response = new CobrandLoginResponse();
            entityType = EnsureEntityType(entityType);
            try
            {

                Logger.Info($"[yodlee] LoginForCobrand method invoked for [{entityType}] with id #{entityId}");
                response = await Task.Run(()=> Proxy.LoginForCobrand());
            var cobsession = await YodleeRepositoy.GetByEntityId(entityType, entityId);
            var yodleedata = new YodleeData() { EntityType = entityType, EntityId = entityId,
                CobSession = new CobrandSession() { ApplicationId = response.ApplicationId,
                    CobrandId = response.CobrandId, CobSessionId = response.Session.CobSession, Locale = response.Locale },
                StartDate = new TimeBucket(TenantTime.Now)
            };
            if (cobsession == null)
                YodleeRepositoy.Add(yodleedata);
            else
            {
                cobsession.CobSession.ApplicationId = response.ApplicationId;
                cobsession.CobSession.CobrandId = response.CobrandId;
                cobsession.CobSession.CobSessionId = response.Session.CobSession;
                cobsession.CobSession.Locale = response.Locale;
                YodleeRepositoy.Update(cobsession);
            }
            var referencenumber = Guid.NewGuid().ToString("N");
            Logger.Info($"[yodlee] Publishing CobrandTokenRequested event for [{entityType}] with id #{entityId}");
            await EventHub.Publish(new CobrandTokenRequested
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = response,
                Request = cobsession,
                ReferenceNumber = referencenumber,
                Name ="yodlee"
                
            });
            }
            catch (YodleeException exception)
            {
                Logger.Info($"[yodlee] Publishing ConbrandLoginFail event for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new ConbrandLoginFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = entityId,
                    ReferenceNumber = null,
                    Name = "yodlee"
                });
                Logger.Error("[yodlee] Error occured when LoginForCobrand method was called. Error: ", exception);
                throw new YodleeException(exception.Message);
            }
            return response;
        }

        

        public async Task<IUserRegistrationResponse> RegisterUser(string entityType,string entityId, IUserRegistrationRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            IUserRegistrationResponse response = new UserRegistrationResponse();
            entityType = EnsureEntityType(entityType);
            var sessiondata = await YodleeRepositoy.GetByEntityId(entityType, entityId);
            if (sessiondata.CobSession == null)
            {
                throw new NotFoundException("Cobrand Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            try
            {
                Logger.Info($"[yodlee] RegisterUser method invoked for [{entityType}] with id #{entityId} and  data { (request != null ? JsonConvert.SerializeObject(request) : null)}");
                response = await Task.Run(() => Proxy.RegisterUser(sessiondata.CobSession.CobSessionId, request));
                sessiondata.UserData = new UserData() { LoginName = request.User.LoginName, Password = request.User.Password, Email = request.User.Email };
                YodleeRepositoy.Update(sessiondata);
                var referencenumber = Guid.NewGuid().ToString("N");
                Logger.Info($"[yodlee] Publishing UserRegistrationRequested event for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new UserRegistrationRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = response,
                    Request = request,
                    ReferenceNumber = referencenumber,
                    Name = "yodlee"

                });
            }
            catch (YodleeException exception)
            {
                Logger.Info($"[yodlee] Publishing UserRegistrationFail event for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new UserRegistrationFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = entityId,
                    ReferenceNumber = null,
                    Name = "yodlee"
                });
                Logger.Error("[yodlee] Error occured when RegisterUser method was called. Error: ", exception);
                throw new YodleeException(exception.Message);
            }
          
            return response;
        }
        public async Task<IUserLoginResponse> LoginUser(string entityType,string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            IUserLoginResponse usersession = null;
            entityType = EnsureEntityType(entityType);
            IYodleeData sessiondata = await YodleeRepositoy.GetByEntityId(entityType, entityId);
            if (sessiondata?.CobSession == null)
                 throw new NotFoundException("Cobrand Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            try
            {
                if (sessiondata.IsRegistered)
                {
                    return await GetUserSession(entityType, entityId);
                }
                else
                {
                    //string loginName = (entityId.Length < 4 ? "0000" + entityId : entityId);
                    var referencenumber = Guid.NewGuid().ToString("N");
                    string loginName = entityId + "-" + referencenumber;
                    string password = "Application@" + (entityId.Length < 3 ? "00" + entityId : entityId.Substring(entityId.Length - 3, 3));
                    string email = entityId + "@application.com";
                    IUserRegistrationRequest request = new UserRegistrationRequest(loginName, password, email);
                    var response = await Task.Run(() => Proxy.RegisterUser(sessiondata.CobSession.CobSessionId, request));
                    if (response != null)
                    {
                        // YodleeRepositoy.Update(sessiondata);

                        usersession = await Task.Run(() => Proxy.LoginUser(sessiondata.CobSession.CobSessionId, loginName, password));
                        if (usersession!=null)
                        {
                            Logger.Info($"[yodlee] Publishing UserLoginRequested event for [{entityType}] with id #{entityId}");
                            await EventHub.Publish(new UserLoginRequested
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = response,
                                Request = request,
                                ReferenceNumber = referencenumber,
                                Name = "yodlee"

                            });
                            sessiondata.IsRegistered = true;
                            sessiondata.UserData = new UserData() { LoginName = loginName, Password = password, Email = email };

                            sessiondata.UserSession = response.User.Session.UserSession;
                            YodleeRepositoy.Update(sessiondata);
                        }
                       
                    }

                }
            }
            catch(YodleeException exception)
            {
                Logger.Info($"[yodlee] Publishing UserRegistrationFail event for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new UserRegistrationFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = entityId,
                    ReferenceNumber = null,
                    Name = "yodlee"
                });
                Logger.Error("[yodlee] Error occured when LoginUser method was called. Error: ", exception);
                throw new YodleeException(exception.Message);
            }
            return usersession;
        }
        public async Task<IUserLoginResponse> GetUserSession(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            entityType = EnsureEntityType(entityType);
            IYodleeData  sessiondata = await YodleeRepositoy.GetByEntityId(entityType, entityId);
            if (sessiondata.CobSession == null)
            {
                throw new NotFoundException("Cobrand Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            var response = await Task.Run(()=> Proxy.LoginUser(sessiondata.CobSession.CobSessionId,sessiondata?.UserData?.LoginName,sessiondata?.UserData?.Password));
            sessiondata.UserSession = response.User.Session.UserSession;
            YodleeRepositoy.Update(sessiondata);
            return response;
        }

        public async Task<ISearchBankResponse> SearchBank(string entityType,string entityId, string searchName)
        {
             if (string.IsNullOrWhiteSpace(searchName))
                throw new ArgumentNullException(nameof(searchName));
            entityType = EnsureEntityType(entityType);
            var sessiondata = await YodleeRepositoy.GetByEntityId(entityType, entityId);
            if (sessiondata.CobSession == null)
            {
                throw new NotFoundException("Cobrand Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            var response = Proxy.SearchBank(sessiondata.CobSession.CobSessionId, searchName);

            return response;
        }

        public async Task<string> RemoveLinkAccount(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            var sessiondata = await YodleeRepositoy.GetByEntityId(entityType, entityId);
            if (sessiondata?.CobSession == null)
            {
                throw new NotFoundException("Cobrand Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            return Proxy.RemoveSiteAccount(sessiondata.CobSession.CobSessionId, sessiondata.UserSession, sessiondata.ProviderAccountId);


        }

        public async Task<IBankLoginResponse> GetBankLoginForm(string entityType, string entityId, string providerId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(providerId))
                throw new ArgumentNullException(nameof(providerId));
            entityType = EnsureEntityType(entityType);
            var sessiondata = await YodleeRepositoy.GetByEntityId(entityType, entityId);
            if (sessiondata?.CobSession == null)
            {
                throw new NotFoundException("Cobrand Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            return Proxy.GetBankLoginForm(sessiondata.CobSession.CobSessionId, providerId);
        }

        public async Task<IProviderAccountRefreshStatus> PostBankLoginForm(string entityType, string entityId, string providerId, IUpdateBankLogin loginForm)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(providerId))
                throw new ArgumentNullException(nameof(providerId));
            if (loginForm == null)
                throw new ArgumentNullException(nameof(loginForm));
            var sessiondata = await YodleeRepositoy.GetByEntityId(entityType, entityId);
            if (sessiondata?.CobSession == null)
            {
                throw new NotFoundException("Cobrand Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            if(string.IsNullOrEmpty(sessiondata?.UserSession))
            {
                throw new NotFoundException("User Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            return await Task.Run(()=> Proxy.PostBankLoginForm(sessiondata.CobSession.CobSessionId,sessiondata.UserSession, providerId, loginForm));
        }

        public string UpdateBankLoginForm(string cobrandSessionId, string userSessionId, string providerAccountId, IUpdateBankLogin loginForm)
        {

            if (string.IsNullOrWhiteSpace(cobrandSessionId))
                throw new ArgumentNullException(nameof(cobrandSessionId));
            if (string.IsNullOrWhiteSpace(userSessionId))
                throw new ArgumentNullException(nameof(userSessionId));
            if (string.IsNullOrWhiteSpace(providerAccountId))
                throw new ArgumentNullException(nameof(providerAccountId));
            if (loginForm == null)
                throw new ArgumentNullException(nameof(loginForm));

            return Proxy.UpdateBankLoginForm(cobrandSessionId, userSessionId, providerAccountId, loginForm);
        }

        public IMfaRefreshResponse GetMfaStatusWithRefreshInformation(string cobrandSessionId, string userSessionId, string providerAccountId)
        {
            if (string.IsNullOrWhiteSpace(cobrandSessionId))
                throw new ArgumentNullException(nameof(cobrandSessionId));
            if (string.IsNullOrWhiteSpace(userSessionId))
                throw new ArgumentNullException(nameof(userSessionId));
            if (string.IsNullOrWhiteSpace(providerAccountId))
                throw new ArgumentNullException(nameof(providerAccountId));

            return Proxy.GetMfaStatusWithRefreshInformation(cobrandSessionId, userSessionId, providerAccountId);
        }

        public string SendMfaDetails(string cobrandSessionId, string userSessionId, string providerAccountId, dynamic mfaChallenge)
        {
            if (string.IsNullOrWhiteSpace(cobrandSessionId))
                throw new ArgumentNullException(nameof(cobrandSessionId));
            if (string.IsNullOrWhiteSpace(userSessionId))
                throw new ArgumentNullException(nameof(userSessionId));
            if (string.IsNullOrWhiteSpace(providerAccountId))
                throw new ArgumentNullException(nameof(providerAccountId));
            if (mfaChallenge == null)
                throw new ArgumentNullException(nameof(mfaChallenge));

            return Proxy.SendMfaDetails(cobrandSessionId, userSessionId, providerAccountId, mfaChallenge);
        }

        public IFinalRefreshResponse GetRefreshStatus(string cobrandSessionId, string userSessionId, string providerAccountId)
        {
            if (string.IsNullOrWhiteSpace(cobrandSessionId))
                throw new ArgumentNullException(nameof(cobrandSessionId));
            if (string.IsNullOrWhiteSpace(userSessionId))
                throw new ArgumentNullException(nameof(userSessionId));
            if (string.IsNullOrWhiteSpace(providerAccountId))
                throw new ArgumentNullException(nameof(providerAccountId));

            return Proxy.GetRefreshStatus(cobrandSessionId, userSessionId, providerAccountId);
        }
        public string StartRefreshStatus(string cobrandSessionId, string userSessionId, string providerAccountId)
        {
            if (string.IsNullOrWhiteSpace(cobrandSessionId))
                throw new ArgumentNullException(nameof(cobrandSessionId));
            if (string.IsNullOrWhiteSpace(userSessionId))
                throw new ArgumentNullException(nameof(userSessionId));
            if (string.IsNullOrWhiteSpace(providerAccountId))
                throw new ArgumentNullException(nameof(providerAccountId));

            return Proxy.StartSiteRefresh(cobrandSessionId, userSessionId, providerAccountId);
        }
        public async Task<IList<IItemSummaries>> GetItemSummaries(string entityType, string entityId)
        {

            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            entityType = EnsureEntityType(entityType);
            IList<IItemSummaries> result = new List<IItemSummaries>();
            var sessiondata = await YodleeRepositoy.GetByEntityId(entityType, entityId);
            if (sessiondata.CobSession == null)
            {
                throw new NotFoundException("Cobrand Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            if (string.IsNullOrEmpty(sessiondata.UserSession))
            {
                throw new NotFoundException("User Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                Logger.Info($"[yodlee] GetItemSummaries method invoked for [{entityType}] with id #{entityId} ");
                result = Proxy.GetItemSummaries(sessiondata.CobSession.CobSessionId, sessiondata.UserSession, sessiondata.ProviderAccountId);
                var accountData = result.Select(i => i.ItemData.Accounts.Where(j => j.AcctType == "savings").Select(s => new ItemAccount()
                {
                    ItemAccountId = s.ItemAccountId.ToString(),
                    ItemId = i.ItemId.ToString(),
                    BankAccountNo = s.AccountNumber,
                    BankAccountName = s.AccountName

                })).FirstOrDefault();
                //sessiondata.BankAccountName = accountData.FirstOrDefault().BankAccountName;
                //sessiondata.BankAccountNo = accountData.FirstOrDefault().BankAccountNo;
                sessiondata.ItemAccountId = accountData.FirstOrDefault().ItemAccountId;
                sessiondata.ItemId = accountData.FirstOrDefault().ItemId;
                YodleeRepositoy.Update(sessiondata);
                Logger.Info($"[yodlee] Publishing UserLoginRequested event for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new ItemSummaryRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = entityId,
                    ReferenceNumber = referencenumber,
                    Name = "yodlee"

                });
            }
            catch (YodleeException exception)
            {
                Logger.Info($"[yodlee] Publishing ItemSummaryCallFail event for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new ItemSummaryCallFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = entityId,
                    ReferenceNumber = null,
                    Name = "yodlee"
                });
                Logger.Error("[yodlee] Error occured when GetItemSummaries method was called. Error: ", exception);
                throw new YodleeException(exception.Message);
            }
            return result;


        }

        public async Task<IFinalReport> FetchReport(string entityType, string entityId)
        {
        
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            entityType = EnsureEntityType(entityType);
            IFinalReport response = new FinalReport();
            var sessiondata = await YodleeRepositoy.GetByEntityId(entityType, entityId);
            if (sessiondata?.CobSession == null)
            {
                throw new NotFoundException("Cobrand Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            if (string.IsNullOrEmpty(sessiondata?.UserSession))
            {
                throw new NotFoundException("User Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            try
            {
                Logger.Info($"[yodlee] FetchReport method invoked for [{entityType}] with id #{entityId} ");
                response = Proxy.FetchReport(sessiondata.CobSession.CobSessionId, sessiondata.UserSession, sessiondata.ItemId, sessiondata.ItemAccountId);
                sessiondata.BankAccountName = response?.MetaData?.AccountHolder;
                sessiondata.BankAccountNo = response?.MetaData?.AccountNumber;
                YodleeRepositoy.Update(sessiondata);
                Logger.Info($"[yodlee] Publishing YodleeCashFlowReportPulled event for [{entityType}] with id #{entityId}");
                var referencenumber = Guid.NewGuid().ToString("N");
                response.ReferenceNumber = referencenumber;
                await EventHub.Publish(new YodleeCashFlowReportPulled
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = response,
                    Request = sessiondata,
                    ReferenceNumber = referencenumber,
                    Name = "yodlee"
                });
            }
            catch (YodleeException exception)
            {
                Logger.Info($"[yodlee] Publishing FetchReport event for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new YodleeCashFlowReportPullFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = entityId,
                    ReferenceNumber = null,
                    Name = "yodlee"
                });
                Logger.Error("[yodlee] Error occured when GetItemSummaries method was called. Error: ", exception);
                throw new YodleeException(exception.Message);
            }
           
            return response;
        }
        public async Task<IFinalReport> GetReport(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            entityType = EnsureEntityType(entityType);
            Logger.Info($"[yodlee] GetReport method invoked for [{entityType}] with id #{entityId} ");
            var sessiondata = await YodleeRepositoy.GetByEntityId(entityType, entityId);
            IFinalReport response = null;
            if (sessiondata?.CobSession == null)
            {
                throw new NotFoundException("Cobrand Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            if (string.IsNullOrEmpty(sessiondata?.UserSession))
            {
                throw new NotFoundException("User Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
         

            Logger.Info($"[yodlee] GetUserBankAccounts method invoked from GetReport for [{entityType}] with id #{entityId} ");
            var getAccountDetail =await GetUserBankAccounts(entityType, entityId);
              if(getAccountDetail!=null)
                  {
                Logger.Info($"[yodlee] GetItemSummaries method invoked from GetReport for [{entityType}] with id #{entityId} ");
                var getItemSummary =await GetItemSummaries(entityType, entityId);
                      if (getItemSummary != null)
                      {
                         sessiondata = await YodleeRepositoy.GetByEntityId(entityType, entityId);
                    Logger.Info($"[yodlee] FetchReport method invoked from GetReport for [{entityType}] with id #{entityId} ");
                    response = await Task.Run(() => FetchReport(entityType,entityId));
                        }
                    }
               
            
            if (sessiondata != null)
            {
               sessiondata.ReportPulleddDate = new TimeBucket(TenantTime.Now);
            }
            YodleeRepositoy.Update(sessiondata);
          
        
            return response;
        }
        public async Task<Syndication.YodleeYsl.Response.IFastlinkAccessTokenResponse> GetFastlinkAccessToken(string entityType, string entityId)
        {
            //entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            entityType = EnsureEntityType(entityType);
            Syndication.YodleeYsl.Response.IFastlinkAccessTokenResponse result = new Syndication.YodleeYsl.Response.FastlinkAccessTokenResponse();
            var sessiondata = await YodleeRepositoy.GetByEntityId(entityType, entityId);
            if (sessiondata?.CobSession == null)
            {
                throw new NotFoundException("Cobrand Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            if (string.IsNullOrEmpty(sessiondata?.UserSession))
            {
                throw new NotFoundException("User Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            var request = new Syndication.YodleeYsl.Request.FastlinkAccessTokenRequest() { CobrandToken = sessiondata.CobSession.CobSessionId, UserSessionToken = sessiondata.UserSession };
            try
            {
                Logger.Info($"[yodlee] GetFastlinkAccessToken method invoked from GetReport for [{entityType}] with id #{entityId} ");
                var proxyrequest = new Syndication.YodleeYsl.Proxy.FastlinkAccessTokenRequest(request);
                var response = await Task.Run(() => Proxy.GetFastlinkAccessToken(proxyrequest));
                var referencenumber = Guid.NewGuid().ToString("N");
                result = new Syndication.YodleeYsl.Response.FastlinkAccessTokenResponse(response);
                sessiondata.FastLinkAccessToken = result?.AccessTokenData?.AccessTokens?.FirstOrDefault()?.Value;
                YodleeRepositoy.Update(sessiondata);
                Logger.Info($"[yodlee] Publishing FastlinkAccessTokenRequested event for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new FastlinkAccessTokenRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = request,
                    ReferenceNumber = referencenumber,
                    Name = "yodlee"
                });
            }
            catch (YodleeException exception)
            {
                Logger.Info($"[yodlee] Publishing FastlinkAccessTokenRequestFail event for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new FastlinkAccessTokenRequestFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = entityId,
                    ReferenceNumber = null,
                    Name = "yodlee"
                });
                Logger.Error("[yodlee] Error occured when GetFastlinkAccessToken method was called. Error: ", exception);
                throw new YodleeException(exception.Message);
            }
           
            return result;
        }
        public async Task<IGetFastLinkUrlResponse> GetFalstLinkUrl(string entityType, string entityId, FastLinkUrlRequest fastlinkReuest)
        {
            //entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (fastlinkReuest == null)
                throw new ArgumentNullException(nameof(fastlinkReuest));
            var referencenumber = Guid.NewGuid().ToString("N");
            var request = new FastLinkUrlRequest(fastlinkReuest);
            var sessiondata = await YodleeRepositoy.GetByEntityId(entityType, entityId);
            if (sessiondata?.CobSession == null)
            {
                throw new NotFoundException("Cobrand Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            if (string.IsNullOrEmpty(sessiondata?.UserSession))
            {
                throw new NotFoundException("User Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            var response = await Task.Run(() => Proxy.GetFalstLinkUrl(sessiondata.FastLinkAccessToken,sessiondata.UserSession,  request));
            await EventHub.Publish(new FalstLinkUrlRequested
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = response,
                Request = fastlinkReuest,
                ReferenceNumber = referencenumber,
                Name = "yodlee"
            });
            return new GetFastLinkUrlResponse(response);
        }
        public async Task<Syndication.YodleeYsl.Response.IGetBankAccountResponse> GetUserBankAccounts(string entityType, string entityId)
        {
            //entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            var sessiondata = await YodleeRepositoy.GetByEntityId(entityType, entityId);
            if (sessiondata?.CobSession == null)
            {
                throw new NotFoundException("Cobrand Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            if (string.IsNullOrEmpty(sessiondata.UserSession))
            {
                throw new NotFoundException("User Session is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            var referencenumber = Guid.NewGuid().ToString("N");
            var response = await Task.Run(() => Proxy.GetUserBankAccounts(sessiondata.CobSession.CobSessionId,sessiondata.UserSession));
            var result = new Syndication.YodleeYsl.Response.GetBankAccountResponse(response);
            if (result != null)
            {
                sessiondata.ProviderAccountId = result?.Accounts?.FirstOrDefault()?.ProviderAccountId.ToString();
                YodleeRepositoy.Update(sessiondata);
            }
            await EventHub.Publish(new UserBankAccountDetailRequested
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = result,
                Request = sessiondata,
                ReferenceNumber = referencenumber,
                Name = "yodlee"
            });
            return result;
        }
        public async Task<IYodleeData> GetStatus(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            var sessiondata = await YodleeRepositoy.GetByEntityId(entityType, entityId);
            return sessiondata;
        }
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}
