﻿

using System.Threading.Tasks;
using CreditExchange.Syndication.Crif.CreditReport;
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace CreditExchange.Crif.CreditReport.Persistence
{
    public class CrifCreditReportRepository : MongoRepository<ICrifCreditReport, CrifCreditReport>, ICrifCreditReportRepository
    {
        static CrifCreditReportRepository()
        {
            BsonClassMap.RegisterClassMap<CrifCreditReport>(map =>
            {
                map.AutoMap();
                var type = typeof(CrifCreditReport);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public CrifCreditReportRepository(LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "crif-credit-report")
        {
            CreateIndexIfNotExists("crif-credit-report",
                Builders<ICrifCreditReport>.IndexKeys.Ascending(i => i.EntityType).Ascending(i => i.EntityId));
        }

        public async Task<ICrifCreditReport> GetCrifReportDetail( string entityType ,string entityId)
        {
            return await Query.FirstOrDefaultAsync(a => a.EntityType == entityType && a.EntityId == entityId);
        }

    }
}
