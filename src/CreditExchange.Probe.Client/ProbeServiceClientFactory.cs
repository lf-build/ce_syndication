﻿using System;
using LendFoundry.Security.Tokens;
using CreditExchange.Syndication.Probe;
using LendFoundry.Foundation.Client;

namespace CreditExchange.Probe.Client
{
    public class ProbeServiceClientFactory :IProbeServiceClientFactory
    {
        public ProbeServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public IProbeService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new ProbeService(client);
        }
    }
}
