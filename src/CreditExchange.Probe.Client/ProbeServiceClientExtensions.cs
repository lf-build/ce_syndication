﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace CreditExchange.Probe.Client
{
    public static class ProbeServiceClientExtensions
    {
        public static IServiceCollection AddProbeService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IProbeServiceClientFactory>(p => new ProbeServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IProbeServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
