﻿using System.Threading.Tasks;
using CreditExchange.Syndication.Probe;
using CreditExchange.Syndication.Probe.Response;
using RestSharp;
using LendFoundry.Foundation.Client;

namespace CreditExchange.Probe.Client
{
    public class ProbeService :IProbeService
    {
        public ProbeService(IServiceClient client)
        {
            Client = client;
        }
        private IServiceClient Client { get; }

        public async Task<IGetFinancialDetailResponse> GetFinancialDetail(string entityType,string entityId, string cin)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/{cin}/finance", Method.GET);
            request.AddUrlSegment("cin", cin);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<FinancialDetailResponse>(request);
        }
    }
}
