﻿using LendFoundry.Security.Tokens;
using CreditExchange.Syndication.Probe;

namespace CreditExchange.Probe.Client
{
   public interface IProbeServiceClientFactory
    {
        IProbeService Create(ITokenReader reader);
    }
}
