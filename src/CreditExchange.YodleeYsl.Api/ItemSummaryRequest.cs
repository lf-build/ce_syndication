﻿namespace CreditExchange.YodleeYsl.Api
{
    public class ItemSummaryRequest
    {
        public string CobrandSessionId { get; set; }
        public string UserSessionId { get; set; }
        public string ProviderAccountId { get; set; }
    }
}
