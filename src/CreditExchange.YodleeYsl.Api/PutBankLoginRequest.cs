﻿using CreditExchange.Syndication.YodleeYsl;

namespace CreditExchange.YodleeYsl.Api
{
    public class PutBankLoginRequest
    {
        public string CobrandSessionId { get; set; }
        public string UserSessionId { get; set; }
        public string ProviderAccountId { get; set; }
        public UpdateBankLogin LoginForm { get; set; }
    }
}
