﻿using System;
using LendFoundry.Foundation.Services;
#if DOTNET2
using  Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System.Threading.Tasks;
using CreditExchange.Syndication.YodleeYsl;
using CreditExchange.Syndication.YodleeYsl.Request;
using LendFoundry.Foundation.Logging;
using CreditExchange.YodleeYsl.Abstractions;
using Newtonsoft.Json;

namespace CreditExchange.YodleeYsl.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IYodleeYslService service, ILogger logger) : base(logger)
        {
            if (service == null)
                throw new ArgumentNullException(nameof(service));

            YodleeService = service;
        }

        private IYodleeYslService YodleeService { get; }

        [HttpPost("{entityType}/{entityId}/cobrand-login")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.YodleeYsl.ICobrandLoginResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> CobrandLogin(string entityType,string entityId)
        {
            Logger.Info($"[YODLEE] CobrandLogin endpoint invoked for [{entityType}] with id #{entityId} ");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await YodleeService.LoginForCobrand(entityType, entityId));
                }
                catch (YodleeException exception)
                {
                    Logger.Error($"[YODLEE] Error occured when CobrandLogin endpoint was called for [{entityType}] with id #{entityId} ", exception);
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
          
        }

        [HttpPost("{entityType}/{entityId}/register")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.YodleeYsl.IUserRegistrationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> RegisterUser(string entityType, string entityId, UserRegistrationRequest request)
        {
            Logger.Info($"[YODLEE] RegisterUser endpoint invoked for [{entityType}] with id #{entityId} with  data { (request != null ? JsonConvert.SerializeObject(request) : null)} ");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await YodleeService.RegisterUser(entityType, entityId, request));
                }
                catch (YodleeException exception)
                {
                    Logger.Error($"[YODLEE] Error occured when RegisterUser endpoint was called for [{entityType}] with id #{entityId} ", exception);
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
         
        }

        [HttpPost("{entityType}/{entityId}/user-login")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.YodleeYsl.IUserLoginResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> UserLogin(string entityType, string entityId)
        {
            Logger.Info($"[YODLEE] UserLogin endpoint invoked for [{entityType}] with id #{entityId} ");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await YodleeService.LoginUser(entityType, entityId));
                }
                catch (YodleeException exception)
                {
                    Logger.Error($"[YODLEE] Error occured when UserLogin endpoint was called for [{entityType}] with id #{entityId} ", exception);
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
           
        }

        [HttpGet("{entityType}/{entityId}/{searchName}/search-bank")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.YodleeYsl.ISearchBankResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> SearchBank(string entityType, string entityId,string searchName)
        {
             return await ExecuteAsync(() => Task.FromResult<IActionResult>(
                Ok(YodleeService.SearchBank(entityType, entityId, searchName))));
        }

        [HttpGet("{entityType}/{entityId}/{providerId}/submit-bank-login")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.YodleeYsl.IBankLoginResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetBankLogin(string entityType, string entityId,string providerId)
        {
              return await ExecuteAsync(() => Task.FromResult<IActionResult>(
                Ok(YodleeService.GetBankLoginForm(entityType, entityId,providerId))));
        }

        [HttpPost("postlogin")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.YodleeYsl.IProviderAccountRefreshStatus), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> PostBankLogin([FromBody] PostBankLoginRequest postLoginRequest)
        {
            if (postLoginRequest == null)
                return new BadRequestResult();

            return await ExecuteAsync(() => Task.FromResult<IActionResult>(
                Ok(YodleeService.PostBankLoginForm(postLoginRequest.CobrandSessionId, postLoginRequest.UserSessionId, postLoginRequest.ProviderId,
                    postLoginRequest.LoginForm))));
        }

        [HttpPost("putlogin")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.YodleeYsl.IProviderAccountRefreshStatus), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> PutBankLogin([FromBody] PutBankLoginRequest putLoginRequest)
        {
            if (putLoginRequest == null)
                return new BadRequestResult();

            return await ExecuteAsync(() => Task.FromResult<IActionResult>(
                Ok(YodleeService.PostBankLoginForm(putLoginRequest.CobrandSessionId, putLoginRequest.UserSessionId, putLoginRequest.ProviderAccountId,
                    putLoginRequest.LoginForm))));
        }

        [HttpPost("mfastatus")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.YodleeYsl.IMfaRefreshResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetMfaStatus([FromBody] MfaStatusRequest mfaStatusRequest)
        {
            if (mfaStatusRequest == null)
                return new BadRequestResult();

            return await ExecuteAsync(() => Task.FromResult<IActionResult>(
                Ok(YodleeService.GetMfaStatusWithRefreshInformation(mfaStatusRequest.CobrandSessionId, mfaStatusRequest.UserSessionId, 
                    mfaStatusRequest.ProviderAccountId))));
        }

        [HttpPost("sendmfa")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> SendMfaStatus([FromBody] PostMfaRequest postMfaRequest)
        {
            if (postMfaRequest == null)
                return new BadRequestResult();

            return await ExecuteAsync(() => Task.FromResult<IActionResult>(
                Ok(YodleeService.SendMfaDetails(postMfaRequest.CobrandSessionId, postMfaRequest.UserSessionId,
                    postMfaRequest.ProviderAccountId, postMfaRequest.MfaChallenge))));
        }

        [HttpPost("refreshstatus")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.YodleeYsl.IFinalRefreshResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetRefreshStatus([FromBody] RefreshRequest refreshRequest)
        {
            if (refreshRequest == null)
                return new BadRequestResult();

            return await ExecuteAsync(() => Task.FromResult<IActionResult>(
                Ok(YodleeService.GetRefreshStatus(refreshRequest.CobrandSessionId, refreshRequest.UserSessionId,
                    refreshRequest.ProviderAccountId))));
        }

        [HttpGet("{entityType}/{entityId}/itemsummary")]
#if DOTNET2
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetItemSummaries(string entityType, string entityId)
        {
            Logger.Info($"[YODLEE] GetItemSummaries endpoint invoked for [{entityType}] with id #{entityId} ");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await YodleeService.GetItemSummaries(entityType, entityId));
                }
                catch (YodleeException exception)
                {
                    Logger.Error($"[YODLEE] Error occured when GetItemSummaries endpoint was called for [{entityType}] with id #{entityId} ", exception);
                    return ErrorResult.BadRequest(exception.Description);
                }
            });

        }


        [HttpGet("{entityType}/{entityId}/finalreport")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.YodleeYsl.IFinalReport), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetFinalReport(string entityType, string entityId)
        {
            Logger.Info($"[YODLEE] GetFinalReport endpoint invoked for [{entityType}] with id #{entityId} ");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(() => YodleeService.FetchReport(entityType, entityId)));
                }
                catch (YodleeException exception)
                {
                    Logger.Error($"[YODLEE] Error occured when GetFinalReport endpoint was called for [{entityType}] with id #{entityId} ", exception);
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
        }
                
        
        [HttpPost("startrefreshstatus")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> Startrefreshstatus([FromBody]  RefreshRequest refreshRequest)
        {
            if (refreshRequest == null)
                return new BadRequestResult();

            return await ExecuteAsync(() => Task.FromResult<IActionResult>(
                Ok(YodleeService.StartRefreshStatus(refreshRequest.CobrandSessionId, refreshRequest.UserSessionId,
                    refreshRequest.ProviderAccountId))));
        }
        [HttpGet("{entityType}/{entityid}/fastlink-access-token")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.YodleeYsl.Response.IFastlinkAccessTokenResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetFastlinkAccessToken(string entityType, string entityId)
        {
            Logger.Info($"[YODLEE] GetFastlinkAccessToken endpoint invoked for [{entityType}] with id #{entityId} ");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await  YodleeService.GetFastlinkAccessToken(entityType, entityId));
                }
                catch (YodleeException exception)
                {
                    Logger.Error($"[YODLEE] Error occured when GetFastlinkAccessToken endpoint was called for [{entityType}] with id #{entityId} ", exception);
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
        }
        [HttpPost("{entityType}/{entityid}/fastlink-url")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.YodleeYsl.IGetFastLinkUrlResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetFalstLinkUrl(string entityType, string entityId, [FromBody]FastLinkUrlRequest request)
        {
            Logger.Info($"[YODLEE] GetFalstLinkUrl endpoint invoked for [{entityType}] with id #{entityId} ");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await  YodleeService.GetFalstLinkUrl(entityType, entityId, request));
                }
                catch (YodleeException exception)
                {
                    Logger.Error($"[YODLEE] Error occured when GetFalstLinkUrl endpoint was called for [{entityType}] with id #{entityId} ", exception);
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
        }
        [HttpGet("{entityType}/{entityId}/user-bank-accounts")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.YodleeYsl.Response.IGetBankAccountResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetUserBankAccounts(string entityType, string entityId)
        {
            Logger.Info($"[YODLEE] GetUserBankAccounts endpoint invoked for [{entityType}] with id #{entityId} ");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await YodleeService.GetUserBankAccounts(entityType, entityId));
                }
                catch (YodleeException exception)
                {
                    Logger.Error($"[YODLEE] Error occured when GetUserBankAccounts endpoint was called for [{entityType}] with id #{entityId} ", exception);
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
        }
        [HttpGet("{entityType}/{entityId}/getreport")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.YodleeYsl.IFinalReport), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetReport(string entityType, string entityId)
        {
            Logger.Info($"[YODLEE] GetReport endpoint invoked for [{entityType}] with id #{entityId} ");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await YodleeService.GetReport(entityType, entityId));
                }
                catch (YodleeException exception)
                {
                    Logger.Error($"[YODLEE] Error occured when GetReport endpoint was called for [{entityType}] with id #{entityId} ", exception);
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
        }

        [HttpDelete("{entityType}/{entityId}/link-account")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> RemoveLinkAccount(string entityType, string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await YodleeService.RemoveLinkAccount(entityType, entityId));
                }
                catch (YodleeException exception)
                {
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
        }

        [HttpGet("{entityType}/{entityId}/status")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.YodleeYsl.Abstractions.IYodleeData), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetStatus(string entityType, string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await YodleeService.GetStatus(entityType, entityId));
                }
                catch (YodleeException exception)
                {
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
        }
    }
}
