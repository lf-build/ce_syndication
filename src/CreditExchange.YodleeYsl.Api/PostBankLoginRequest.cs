﻿using CreditExchange.Syndication.YodleeYsl;

namespace CreditExchange.YodleeYsl.Api
{
    public class PostBankLoginRequest
    {
        public string CobrandSessionId { get; set; }
        public string UserSessionId { get; set; }
        public string ProviderId { get; set; }
        public UpdateBankLogin LoginForm { get; set; }
    }
}
