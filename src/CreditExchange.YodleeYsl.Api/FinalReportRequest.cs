﻿namespace CreditExchange.YodleeYsl.Api
{
    public class FinalReportRequest
    {
        public string CobrandSessionId { get; set; }
        public string UserSessionId { get; set; }
        public string ItemId { get; set; }
        public string ItemAccountId { get; set; }
    }
}
