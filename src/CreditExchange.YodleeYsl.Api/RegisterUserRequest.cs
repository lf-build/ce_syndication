﻿using CreditExchange.Syndication.YodleeYsl;

namespace CreditExchange.YodleeYsl.Api
{
    public class RegisterUserRequest
    {
        public UserRegistrationRequest UserRegistrationRequest { get; set; }
    }
}
