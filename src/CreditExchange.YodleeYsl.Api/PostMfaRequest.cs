﻿namespace CreditExchange.YodleeYsl.Api
{
    public class PostMfaRequest
    {
        public string CobrandSessionId { get; set; }
        public string UserSessionId { get; set; }
        public string ProviderAccountId { get; set; }
        public dynamic MfaChallenge { get; set; }
    }
}
