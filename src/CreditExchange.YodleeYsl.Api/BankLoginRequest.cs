﻿namespace CreditExchange.YodleeYsl.Api
{
    public class BankLoginRequest
    {
        public string CobrandSessionId { get; set; }
        public string ProviderId { get; set; }
    }
}
