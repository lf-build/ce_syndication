﻿namespace CreditExchange.YodleeYsl.Api
{
    public class SearchBankRequest
    {
        public string CobrandSessionId { get; set; }
        public string SearchName { get; set; }
    }
}
