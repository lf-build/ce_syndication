using System.Threading.Tasks;

using LendFoundry.Syndication.Cibil.Proxy.Request;
using LendFoundry.Syndication.Cibil.Proxy.Response;

namespace LendFoundry.Syndication.Cibil.Proxy
{
    public interface IProxyReport
    {
        Task<DCResponse> GetCreditReport(DCRequest dcRequest);
    }
}