﻿using System.Threading.Tasks;
using CreditExchange.Syndication.Cibil.Response;
using CreditExchange.Syndication.Cibil.Request;
using CreditExchange.Syndication.Cibil;

namespace CreditExchange.Cibil
{
    public interface ICibilReportService
    {
        Task<IReport> GetReport(string entityType, string entityId, IReportRequest request);
        Task<ICIRHtml> GetHtmlReport(string entityType, string entityId);
        Task<ICibilData> GetCibilData(string entityType, string entityId);
    }
}
