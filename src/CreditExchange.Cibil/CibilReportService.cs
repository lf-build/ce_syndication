﻿using CreditExchange.Cibil.Persistence;
using CreditExchange.Syndication.Cibil;
using CreditExchange.Syndication.Cibil.Request;
using CreditExchange.Syndication.Cibil.Response;
using LendFoundry.Clients.DecisionEngine;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Cibil
{
    public class CibilReportService :ICibilReportService
    {
        public CibilReportService(IEventHubClient eventHub, ILookupService lookup, ILogger logger, ICibilSyndicationService cibilsyndicaiton,
            ICibilRepository cibilRespository, IDecisionEngineService decisionEngineService,ICibilConfiguration configuration)
        {
            EventHub = eventHub;
            Lookup = lookup;
            Logger = logger;
            if (cibilsyndicaiton == null)
                throw new ArgumentNullException(nameof(cibilsyndicaiton));
            CibilSyndication = cibilsyndicaiton;
            if (cibilRespository == null)
                throw new ArgumentNullException(nameof(cibilRespository));
            CibilRepository = cibilRespository;
            DecisionEngineService = decisionEngineService;
            Configuration = configuration;
        }
        private IEventHubClient EventHub { get; }
        public static string ServiceName { get; } = "cibil";
        private ILookupService Lookup { get; }
        private ILogger Logger { get; }
        private ICibilSyndicationService CibilSyndication { get; }
        private ICibilRepository CibilRepository { get; }
        private IDecisionEngineService DecisionEngineService { get; }
        private ICibilConfiguration Configuration { get; }
        public void ValidateRequest(IReportRequest request)
        {
            if (string.IsNullOrEmpty(request.Gender))
                throw new ArgumentException("Gender is require");
            if (string.IsNullOrEmpty(request.DateOfBirth.ToString()))
                throw new ArgumentException("DateOfBirth is require");
            if (string.IsNullOrEmpty(request.ApplicantFirstName))
                throw new ArgumentException("ApplicantFirstName is require");
            if (string.IsNullOrEmpty(request.ApplicantLastName))
                throw new ArgumentException("ApplicantFirstName is require");
            if (string.IsNullOrEmpty(request.Address1Line1))
                throw new ArgumentException("Address1 Line1 is require");
            if (string.IsNullOrEmpty(request.Address1State))
                throw new ArgumentException("Address1 State is require");
            
            if (string.IsNullOrEmpty(request.Address1Pincode))
                throw new ArgumentException("Address1 Pincode is require");
            if (string.IsNullOrEmpty(request.Address1ResidentOccupation))
                throw new ArgumentException("Address1 ResidentOccupation is require");
            if (string.IsNullOrEmpty(request.Address1AddressCategory))
                throw new ArgumentException("Address1 Category is require");

        }
        public async Task<IReport> GetReport(string entityType, string entityId, IReportRequest request)
        {

            if (request == null)
                throw new ArgumentException("Invalid Request Payload");
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentException("EnityType is require");
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentException("EntityId is require");
            ValidateRequest(request);
           
            var referenceNumber = Guid.NewGuid().ToString("N");
           
            try
            {
                entityType = EnsureEntityType(entityType);
                Logger.Info($"[Cibil] GetReport method invoked for [{entityType}] with id #{entityId}");
                var cibilrequest = GetCibilRequest(request);
                var response = await CibilSyndication.GetReport(entityType, entityId, cibilrequest);
              
                if (response != null)
                {

                    var ErrorDescription = "";
                    var cibildata = await CibilRepository.GetByEntityId(entityType, entityId);
                   
                    if (response?.Report?.ValidationError != null)
                        ErrorDescription = "response.Report.ValidationError.ErrorDescription";
                    if (response?.Report?.ExceptionInfo != null)
                        ErrorDescription= response.Report.ExceptionInfo.ErrorDescription;
                    if (response?.Report?.CreditInfomationReport == null)
                        ErrorDescription = "Cibil CreditReport Response not found";
                    if (cibildata != null)
                    {
                        cibildata.ReportDate = DateTimeOffset.Now;
                        cibildata.RequestXML = response.RequestXML;
                        cibildata.ResponseXML = response.ResponseXML;
                        if (string.IsNullOrEmpty(ErrorDescription))
                            cibildata.Status = "success";
                        else
                            cibildata.Status = "failed";
                        cibildata.BureauResponseRaw = response?.Report?.BureauResponseRaw;
                        CibilRepository.Update(cibildata);
                    }
                    else
                    {
                        cibildata = new CibilData() { EntityType = entityType, EntityId = entityId, RequestXML = response.RequestXML, ResponseXML = response.ResponseXML, ReportDate = DateTimeOffset.Now, BureauResponseRaw = response?.Report?.BureauResponseRaw };
                        CibilRepository.Add(cibildata);
                    }
                    Logger.Info($"[Cibil] Publishing cibilreportpulled event for [{entityType}] with id #{entityId}");
                    if (cibildata.Status == "success")
                    {
                        var eventresponse = response?.Report;
                        if (!string.IsNullOrEmpty(eventresponse?.BureauResponseRaw))
                            eventresponse.BureauResponseRaw = "";
                        await EventHub.Publish(new cibilreportpulled
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            Response = eventresponse,
                            Request = request,
                            ReferenceNumber = referenceNumber,
                            Name = ServiceName
                        });
                        await EventHub.Publish(new GetCibilHtmlReportTriggered
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            ReferenceNumber = referenceNumber,
                            Name = ServiceName
                        });
                    }
                    else
                    {
                        throw new CibilException(ErrorDescription);
                    }
                }
                return response.Report;
            }
            catch (CibilException exception)
            {
                Logger.Info($"[Cibil] Publishing CibilReportPullFail event for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new CibilReportPullFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = request,
                    ReferenceNumber = referenceNumber,
                    Name = ServiceName
                });
                Logger.Error("[Cibil] Error occured when GetReport method was called. Error: ", exception);
                throw new CibilException(exception.Message);
            }
        }
        public async Task<ICIRHtml> GetHtmlReport(string entityType, string entityId)
        {
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentException("EnityType is require", nameof(entityType));
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            entityType = EnsureEntityType(entityType);
            var referenceNumber = Guid.NewGuid().ToString("N");
            try
            {
                var cibildata = await CibilRepository.GetByEntityId(entityType, entityId);
                if (cibildata == null)
                    throw new CibilException($"Credit Report XML not Found  for [{entityType}] with id #{entityId} ");

                var response= await CibilSyndication.GetHtmlReport(entityType, entityId,cibildata?.ResponseXML);
                await EventHub.Publish(new CreditReportHtmlPulled
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = "",
                    Request = entityId,
                    ReferenceNumber = referenceNumber,
                    Name = ServiceName
                });
                return new CIRHtml {   report = response  };
            }
            catch (CibilException exception)
            {
                Logger.Info($"[Cibil] Publishing GetHtmlReport event for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new CreditReportHtmlPullFailed
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = entityId,
                    ReferenceNumber = referenceNumber,
                    Name = ServiceName
                });
                Logger.Error("[Cibil] Error occured when GetHtmlReport method was called. Error: ", exception);
                throw new CibilException(exception.Message);
            }
        }
        private IReportRequest GetCibilRequest(IReportRequest request)
        {
             object executionResult = DecisionEngineService.Execute<dynamic, dynamic>(
                      Configuration.CibilRequestBuilderRule,
                       new { payload = request });

                if (executionResult != null)
                {
                    var cibilrequest = JsonConvert.DeserializeObject<ReportRequest>(executionResult.ToString());
                    return cibilrequest;
                }

            return null;
        }

        public async Task<ICibilData> GetCibilData(string entityType, string entityId)
        {
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentNullException("EnityType is require", nameof(entityType));

            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentNullException("EntityId is require", nameof(entityId));

            entityType = EnsureEntityType(entityType);

            var cibildata = await CibilRepository.GetByEntityId(entityType, entityId);
            if (cibildata == null)
                throw new CibilException($"Credit Report XML not Found  for [{entityType}] with id #{entityId} ");

            return cibildata;
        }


        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;

        }
    }
}
