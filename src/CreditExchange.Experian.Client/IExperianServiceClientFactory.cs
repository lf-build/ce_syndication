﻿using LendFoundry.Security.Tokens;

namespace CreditExchange.Experian.Client
{
    public interface IExperianServiceClientFactory
    {
        IExperianService Create(ITokenReader reader);
    }
}