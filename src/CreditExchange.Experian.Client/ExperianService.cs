﻿using System;
using System.Threading.Tasks;
using CreditExchange.Syndication.Experian;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using RestSharp;

namespace CreditExchange.Experian.Client
{
    public class ExperianService : IExperianService
    {
        public ExperianService(IServiceClient client)
        {
            Client = client;
        }
        private ILogger Logger { get; set; }
        private IServiceClient Client {get;set;}
        public async Task<IAbridgedReportResponse> GetReport(string entityType, string entityId, ISingleActionRequest request)
        {
            var restrequest = new RestRequest("/{entitytype}/{entityid}", Method.POST);
            restrequest.AddUrlSegment("entitytype", entityType);
            restrequest.AddUrlSegment("entityid", entityId);
            restrequest.AddJsonBody(request);
            return await Client.ExecuteAsync<AbridgedReportResponse>(restrequest);
        }

        public Task<ISingleActionResponse> InvokeSingleAction(string entityType, string entityId, ISingleActionRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
