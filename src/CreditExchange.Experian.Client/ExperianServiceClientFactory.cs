﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;

namespace CreditExchange.Experian.Client
{
    public class ExperianServiceClientFactory :IExperianServiceClientFactory
    {
        public ExperianServiceClientFactory(IServiceProvider provider,string endpoint,int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }
        private IServiceProvider  Provider { get; set; }
        private string Endpoint { get; set; }

        private int Port { get; set; }

        public IExperianService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new ExperianService(client);
        }
    }
}
