﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace CreditExchange.Experian.Client
{
    public static class ExperianServiceClientExtension
    {
        public static IServiceCollection AddExperianService(this IServiceCollection services,string endpoint,int port)
        {
            services.AddTransient<IExperianServiceClientFactory>(p => new ExperianServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IExperianServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
            
    }
}
