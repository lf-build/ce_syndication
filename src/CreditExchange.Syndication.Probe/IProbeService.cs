﻿using System.Threading.Tasks;
using CreditExchange.Syndication.Probe.Response;

namespace CreditExchange.Syndication.Probe
{
    public interface IProbeService
    {
        //Task<IGetAuthorisedSignatoryDetail>  GetAuthorisedSignatoryDetail(IGetCompanyDetailRequest companyDetailRequest);
        //Task<IGetChargesResponse>  GetChargesDetail(IGetCompanyDetailRequest companyDetailRequest);
        //Task<IGetCompanyDetailResponse> GetCompanyDetail(IGetCompanyDetailRequest companyDetailRequest);
        //Task<IGetFinancialDataStatusResponse> GetFinancialDataStatus(IGetCompanyDetailRequest companyDetailRequest);
        Task<IGetFinancialDetailResponse> GetFinancialDetail(string entityType, string entityId, string cin);
        //Task<IGetSearchAuthorizedSignatoryResponse> SearchAuthorizedSignatory(IGetSearchRequest searchRequest);
        //Task<IGetSearchCompanyResponse> SearchCompany(IGetSearchRequest searchRequest);
    }
}