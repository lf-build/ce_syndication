﻿namespace CreditExchange.Syndication.Probe
{
    public interface IProbeServiceConfiguration
    {
        string ServiceUrls { get; set; }
        string ApiKey { get; set; }
        string ApiVersion { get; set; }

        int Limit { get; set; }
        int Offset { get; set; }
    }
}