﻿using CreditExchange.Syndication.Probe.Proxy;
using CreditExchange.Syndication.Probe.SearchResponse;
using System.Collections.Generic;
using System.Linq;

namespace CreditExchange.Syndication.Probe.Response
{
    public class GetSearchAuthorizedSignatoryResponse : IGetSearchAuthorizedSignatoryResponse
    {
        public GetSearchAuthorizedSignatoryResponse(AuthorizedSignatorySearchResponse authorizedSignatorySearchResponse)
        {
            if (authorizedSignatorySearchResponse?.data?.authorizedsignatories != null)
                AuthorizedSignatories = authorizedSignatorySearchResponse.data.authorizedsignatories.Select(p => new SearchAuthorizedSignatory(p)).ToList<ISearchAuthorizedSignatory>();
        }

        public List<ISearchAuthorizedSignatory> AuthorizedSignatories { get; set; }

        public string ReferenceNumber { get; set; }
    }
}