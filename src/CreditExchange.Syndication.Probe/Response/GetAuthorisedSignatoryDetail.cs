﻿using CreditExchange.Syndication.Probe.CompanyResponse;
using CreditExchange.Syndication.Probe.Proxy;
using System.Collections.Generic;
using System.Linq;

namespace CreditExchange.Syndication.Probe.Response
{
    public class GetAuthorisedSignatoryDetail : IGetAuthorisedSignatoryDetail
    {
        public GetAuthorisedSignatoryDetail(AuthorizedSignatoryResponse authorizedSignatoryResponse)
        {
            if (authorizedSignatoryResponse?.data?.authorizedsignatories != null)
                AthorisedSignatories = authorizedSignatoryResponse.data.authorizedsignatories.Select(p => new AuthorizedSignatoryDetail(p)).ToList<IAuthorizedSignatoryDetail>();
        }

        public List<IAuthorizedSignatoryDetail> AthorisedSignatories { get; set; }
        public string ReferenceNumber { get; set; }
    }
}