﻿using CreditExchange.Syndication.Probe.CompanyResponse;
using System.Collections.Generic;

namespace CreditExchange.Syndication.Probe.Response
{
    public interface IGetAuthorisedSignatoryDetail
    {
        List<IAuthorizedSignatoryDetail> AthorisedSignatories { get; set; }

        string ReferenceNumber { get; set; }
    }
}