﻿using CreditExchange.Syndication.Probe.CompanyResponse;

namespace CreditExchange.Syndication.Probe.Response
{
    public interface IGetCompanyDetailResponse
    {
        ICompanyDetail Company { get; set; }

        string ReferenceNumber { get; set; }

    }
}