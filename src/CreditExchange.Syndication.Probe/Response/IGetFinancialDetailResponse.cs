﻿using CreditExchange.Syndication.Probe.CompanyResponse;
using System.Collections.Generic;

namespace CreditExchange.Syndication.Probe.Response
{
    public interface IGetFinancialDetailResponse
    {
        List<IFinancialDetail> FinancialDetails { get; set; }
        string ReferenceNumber { get; set; }
    }
}