﻿using CreditExchange.Syndication.Probe.CompanyResponse;
using CreditExchange.Syndication.Probe.Proxy;

namespace CreditExchange.Syndication.Probe.Response
{
    public class GetCompanyDetailResponse : IGetCompanyDetailResponse
    {
        public GetCompanyDetailResponse(CompanyDetailResponse companyDetailResponse)
        {
            if (companyDetailResponse?.data?.company != null)
                Company = new CompanyDetail(companyDetailResponse.data.company);
        }

        public ICompanyDetail Company { get; set; }

        public string ReferenceNumber { get; set; }
    }
}