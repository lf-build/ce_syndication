﻿using CreditExchange.Syndication.Probe.CompanyResponse;
using CreditExchange.Syndication.Probe.Proxy;

namespace CreditExchange.Syndication.Probe.Response
{
    public class GetFinancialDataStatusResponse : IGetFinancialDataStatusResponse
    {
        public GetFinancialDataStatusResponse(FinancialDataStatusResponse financialdatastatusResponse)
        {
            if (financialdatastatusResponse?.data?.datastatus != null)
                Status = new FinancialDataStatus(financialdatastatusResponse.data.datastatus);
        }

        public IFinancialDataStatus Status { get; set; }

        public string ReferenceNumber { get; set; }
    }
}