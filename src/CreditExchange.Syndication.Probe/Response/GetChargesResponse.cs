﻿using CreditExchange.Syndication.Probe.CompanyResponse;
using CreditExchange.Syndication.Probe.Proxy;
using System.Collections.Generic;
using System.Linq;

namespace CreditExchange.Syndication.Probe.Response
{
    public class GetChargesResponse : IGetChargesResponse
    {
        public GetChargesResponse(ChargesResponse chagesResponse)
        {
            if (chagesResponse?.data?.charges != null)
                Charges = chagesResponse.data.charges.Select(p => new ChargesDetail(p)).ToList<IChargesDetail>();
        }

        public List<IChargesDetail> Charges { get; set; }

        public string ReferenceNumber { get; set; } 
    }
}