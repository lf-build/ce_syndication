﻿using CreditExchange.Syndication.Probe.CompanyResponse;
using System.Collections.Generic;

namespace CreditExchange.Syndication.Probe.Response
{
    public interface IGetChargesResponse
    {
        List<IChargesDetail> Charges { get; set; }

        string ReferenceNumber { get; set; }
    }
}