﻿using CreditExchange.Syndication.Probe.Proxy;
using CreditExchange.Syndication.Probe.SearchResponse;
using System.Collections.Generic;
using System.Linq;

namespace CreditExchange.Syndication.Probe.Response
{
    public class GetSearchCompanyResponse : IGetSearchCompanyResponse
    {
        public GetSearchCompanyResponse(CompanySearchResponse searchResponse)
        {
            if (searchResponse?.data?.companies != null)
                Companies = searchResponse.data.companies.Select(p => new SearchCompanyResponse(p)).ToList<ISearchCompanyResponse>();
        }

        public List<ISearchCompanyResponse> Companies { get; set; }

        public string ReferenceNumber { get; set; }
    }
}