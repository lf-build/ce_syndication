﻿using LendFoundry.Foundation.Client;
using CreditExchange.Syndication.Probe.CompanyResponse;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace CreditExchange.Syndication.Probe.Response
{
    public class FinancialDetailResponse : IGetFinancialDetailResponse
    {
        public FinancialDetailResponse()
        {
        }

        public FinancialDetailResponse(Proxy.FinancialDetailResponse financialDetailResponse)
        {
            if (financialDetailResponse?.data?.financials != null)
                FinancialDetails = financialDetailResponse.data.financials.Select(p => new FinancialDetail(p)).ToList<IFinancialDetail>();
        }

        [JsonConverter(typeof(InterfaceListConverter<IFinancialDetail, FinancialDetail>))]
        public List<IFinancialDetail> FinancialDetails { get; set; }
        public  string ReferenceNumber { get; set; }
    }
}