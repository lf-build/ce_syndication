﻿using CreditExchange.Syndication.Probe.SearchResponse;
using System.Collections.Generic;

namespace CreditExchange.Syndication.Probe.Response
{
    public interface IGetSearchAuthorizedSignatoryResponse
    {
        List<ISearchAuthorizedSignatory> AuthorizedSignatories { get; set; }

        string ReferenceNumber { get; set; }
    }
}