﻿using CreditExchange.Syndication.Probe.CompanyResponse;

namespace CreditExchange.Syndication.Probe.Response
{
    public interface IGetFinancialDataStatusResponse
    {
        IFinancialDataStatus Status { get; set; }

        string ReferenceNumber { get; set; }
        
    }
}