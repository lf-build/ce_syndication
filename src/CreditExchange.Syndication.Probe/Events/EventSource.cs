﻿namespace CreditExchange.Syndication.Probe.Events
{
    public class EventSource
    {
        public string ReferenceNumber { get; set; }
        public object Request { get; set; }
        public object Response { get; set; }
    }
}
