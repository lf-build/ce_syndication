﻿using LendFoundry.SyndicationStore.Events;

namespace CreditExchange.Syndication.Probe.Events
{
    public class FinancialDetailRequestedFail : SyndicationCalledEvent
    {
    }
}
