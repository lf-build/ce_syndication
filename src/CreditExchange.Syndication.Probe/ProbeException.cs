﻿using System;
using System.Runtime.Serialization;

namespace CreditExchange.Syndication.Probe
{
    [Serializable]
    public class ProbeException : Exception
    {
        public ProbeException()
        {
        }

        public ProbeException(string message) : base(message)
        {
        }

        public ProbeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ProbeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}