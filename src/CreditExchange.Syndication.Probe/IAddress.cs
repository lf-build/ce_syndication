﻿namespace CreditExchange.Syndication.Probe
{
    public interface IAddress
    {
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string City { get; set; }
        string PinCode { get; set; }
        string State { get; set; }
    }
}