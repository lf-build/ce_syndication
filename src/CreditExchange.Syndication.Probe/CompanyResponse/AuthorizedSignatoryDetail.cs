﻿using CreditExchange.Syndication.Probe.Proxy;

namespace CreditExchange.Syndication.Probe.CompanyResponse
{
    public class AuthorizedSignatoryDetail : IAuthorizedSignatoryDetail
    {
        public AuthorizedSignatoryDetail(AuthorizedSignatoriesDetail authorizedSignatory)
        {
            if (authorizedSignatory != null)
            {
                Name = authorizedSignatory.name;
                Pan = authorizedSignatory.pan;
                Din = authorizedSignatory.din;
                DateOfBirth = authorizedSignatory.date_of_birth;
                Age = authorizedSignatory.age;
                DateOfAppointment = authorizedSignatory.date_of_appointment;
                DateOfAppointmentForCurrentDesignation = authorizedSignatory.date_of_appointment_for_current_designation;
                DateOfCessation = authorizedSignatory.date_of_cessation;
            }
        }

        public string Name { get; set; }
        public string Pan { get; set; }
        public string Din { get; set; }
        public string DateOfBirth { get; set; }
        public int Age { get; set; }
        public string DateOfAppointment { get; set; }
        public string DateOfAppointmentForCurrentDesignation { get; set; }
        public string DateOfCessation { get; set; }
    }
}