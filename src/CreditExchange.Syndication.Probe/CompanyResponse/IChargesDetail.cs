﻿namespace CreditExchange.Syndication.Probe.CompanyResponse
{
    public interface IChargesDetail
    {
        double Amount { get; set; }
        string Date { get; set; }
        string HolderName { get; set; }
        string Type { get; set; }
    }
}