﻿using CreditExchange.Syndication.Probe.Proxy;

namespace CreditExchange.Syndication.Probe.CompanyResponse
{
    public class ChargesDetail : IChargesDetail
    {
        public ChargesDetail(Charge charge)
        {
            if (charge != null)
            {
                Amount = charge.amount;
                Date = charge.date;
                HolderName = charge.holder_name;
                Type = charge.type;
            }
        }

        public double Amount { get; set; }
        public string Date { get; set; }
        public string HolderName { get; set; }
        public string Type { get; set; }
    }
}