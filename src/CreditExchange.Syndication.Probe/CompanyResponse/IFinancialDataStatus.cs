﻿namespace CreditExchange.Syndication.Probe.CompanyResponse
{
    public interface IFinancialDataStatus
    {
        string LastFinYearEnd { get; set; }
        string LastUpdated { get; set; }
    }
}