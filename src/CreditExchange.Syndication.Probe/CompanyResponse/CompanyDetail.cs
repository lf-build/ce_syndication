﻿using CreditExchange.Syndication.Probe.Proxy;

namespace CreditExchange.Syndication.Probe.CompanyResponse
{
    public class CompanyDetail : ICompanyDetail
    {
        public CompanyDetail(Company company)
        {
            if (company != null)
            {
                AuthorizedCapital = company.authorized_capital;
                Cin = company.cin;
                EfilingStatus = company.efiling_status;
                IncorporationDate = company.incorporation_date;
                LegalName = company.legal_name;
                PaidUpCapital = company.paid_up_capital;
                SumOfCharges = company.sum_of_charges;
                if (company.registered_address != null)
                    RegisteredAddress = new Address(company.registered_address);
                Classification = company.classification;
                Status = company.status;
                NextCin = company.next_cin;
                LastAgmDate = company.last_agm_date;
                LastFilingDate = company.last_filing_date;
            }
        }

        public int AuthorizedCapital { get; set; }
        public string Cin { get; set; }
        public string EfilingStatus { get; set; }
        public string IncorporationDate { get; set; }
        public string LegalName { get; set; }
        public int PaidUpCapital { get; set; }
        public int SumOfCharges { get; set; }

        public IAddress RegisteredAddress { get; set; }
        public string Classification { get; set; }
        public string Status { get; set; }
        public string NextCin { get; set; }
        public string LastAgmDate { get; set; }
        public string LastFilingDate { get; set; }
    }
}