﻿namespace CreditExchange.Syndication.Probe.CompanyResponse
{
    public interface IFinancialDetail
    {
        double CapitalWip { get; set; }
        double CashAndBankBalances { get; set; }
        double Depreciation { get; set; }
        double ExceptionalItemsBeforeTax { get; set; }
        double? IncomeTax { get; set; }
        double Interest { get; set; }
        double Inventories { get; set; }
        double Investments { get; set; }
        double LongTermBorrowings { get; set; }
        string MinorityInterestAndProfitFromAssociatesAndJointVentures { get; set; }
        string Nature { get; set; }
        double NetFixedAssets { get; set; }
        double OperatingCost { get; set; }
        double OperatingProfit { get; set; }
        double OtheAssets { get; set; }
        double OtherCurrentLiabilitiesAndProvisions { get; set; }
        double OtherIncome { get; set; }
        double OtherLongTermLiabilitiesAndProvisions { get; set; }
        double Others { get; set; }
        string PnlVersion { get; set; }
        double ProfitAfterTax { get; set; }
        double ProfitAfterTaxButBeforeExceptionalItemsAfterTax { get; set; }
        double ProfitBeforeInterestAndTax { get; set; }
        double ProfitBeforeTax { get; set; }
        double ProfitBeforeTaxAndExceptionalItemsBeforeTax { get; set; }
        double ReservesAndSurplus { get; set; }
        double Revenue { get; set; }
        double ShareCapital { get; set; }
        double ShortTermBorrowings { get; set; }
        string StatedOn { get; set; }
        double TotalAssets { get; set; }
        double TotalEquity { get; set; }
        double TotalEquityAndLiabilities { get; set; }
        double TradePayables { get; set; }
        double TradeReceivables { get; set; }
        string Year { get; set; }
    }
}