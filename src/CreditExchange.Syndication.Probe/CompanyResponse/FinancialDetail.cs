﻿using System;
using CreditExchange.Syndication.Probe.Proxy;

namespace CreditExchange.Syndication.Probe.CompanyResponse
{
    public class FinancialDetail : IFinancialDetail
    {
        public FinancialDetail()
        {
        }

        public FinancialDetail(Financial financial)
        {
            if (financial != null)
            {
                Year = financial.year;
                Nature = financial.nature;
                StatedOn = financial.stated_on;
                CapitalWip = Convert.ToDouble( financial.capital_wip);
                CashAndBankBalances = Convert.ToDouble(financial.cash_and_bank_balances);
                Investments = Convert.ToDouble( financial.investments);
                Inventories = Convert.ToDouble( financial.inventories);
                NetFixedAssets = Convert.ToDouble( financial.net_fixed_assets);
                OtheAssets = Convert.ToDouble( financial.other_assets);
                TotalAssets = Convert.ToDouble( financial.total_assets);
                TradeReceivables = Convert.ToDouble( financial.trade_receivables);
                ShareCapital = Convert.ToDouble( financial.share_capital);
                ReservesAndSurplus = Convert.ToDouble( financial.reserves_and_surplus);
                Others = Convert.ToDouble( financial.others);
                LongTermBorrowings = Convert.ToDouble( financial.long_term_borrowings);
                OtherCurrentLiabilitiesAndProvisions = Convert.ToDouble( financial.other_current_liabilities_and_provisions);
                OtherLongTermLiabilitiesAndProvisions = Convert.ToDouble( financial.other_long_term_liabilities_and_provisions);
                TradePayables = Convert.ToDouble( financial.trade_payables);
                ShortTermBorrowings = Convert.ToDouble( financial.short_term_borrowings);
                TotalEquity = Convert.ToDouble( financial.total_equity);
                TotalEquityAndLiabilities = Convert.ToDouble( financial.total_equity_and_liabilities);
                Depreciation = Convert.ToDouble( financial.depreciation);
                IncomeTax = Convert.ToDouble(financial.income_tax);
                Interest = Convert.ToDouble( financial.interest);
                OperatingCost = Convert.ToDouble(financial.operating_cost);
                OperatingProfit = Convert.ToDouble(financial.operating_profit);
                OtherIncome = Convert.ToDouble(financial.other_income);
                ProfitBeforeInterestAndTax = Convert.ToDouble(financial.profit_before_interest_and_tax);
                ProfitBeforeTax = Convert.ToDouble(financial.profit_before_tax);
                Revenue = Convert.ToDouble( financial.revenue);
                ExceptionalItemsBeforeTax = Convert.ToDouble(financial.exceptional_items_before_tax);
                MinorityInterestAndProfitFromAssociatesAndJointVentures =Convert.ToString(financial.minority_interest_and_profit_from_associates_and_joint_ventures);
                ProfitAfterTaxButBeforeExceptionalItemsAfterTax = Convert.ToDouble( financial.profit_after_tax_but_before_exceptional_items_after_tax);
                ProfitBeforeTaxAndExceptionalItemsBeforeTax = Convert.ToDouble( financial.profit_before_tax_and_exceptional_items_before_tax);
                ProfitAfterTax = Convert.ToDouble( financial.profit_after_tax);
                PnlVersion = financial.pnl_version;
            }
        }

        public string Year { get; set; }
        public string Nature { get; set; }
        public string StatedOn { get; set; }
        public double CapitalWip { get; set; }
        public double CashAndBankBalances { get; set; }
        public double Investments { get; set; }
        public double Inventories { get; set; }
        public double NetFixedAssets { get; set; }
        public double OtheAssets { get; set; }
        public double TotalAssets { get; set; }
        public double TradeReceivables { get; set; }
        public double ShareCapital { get; set; }
        public double ReservesAndSurplus { get; set; }
        public double Others { get; set; }
        public double LongTermBorrowings { get; set; }
        public double OtherCurrentLiabilitiesAndProvisions { get; set; }
        public double OtherLongTermLiabilitiesAndProvisions { get; set; }
        public double TradePayables { get; set; }
        public double ShortTermBorrowings { get; set; }
        public double TotalEquity { get; set; }
        public double TotalEquityAndLiabilities { get; set; }
        public double Depreciation { get; set; }
        public double? IncomeTax { get; set; }
        public double Interest { get; set; }
        public double OperatingCost { get; set; }
        public double OperatingProfit { get; set; }
        public double OtherIncome { get; set; }
        public double ProfitBeforeInterestAndTax { get; set; }
        public double ProfitBeforeTax { get; set; }
        public double Revenue { get; set; }
        public double ExceptionalItemsBeforeTax { get; set; }
        public string MinorityInterestAndProfitFromAssociatesAndJointVentures { get; set; }
        public double ProfitAfterTaxButBeforeExceptionalItemsAfterTax { get; set; }
        public double ProfitBeforeTaxAndExceptionalItemsBeforeTax { get; set; }
        public double ProfitAfterTax { get; set; }
        public string PnlVersion { get; set; }
    }
}