﻿namespace CreditExchange.Syndication.Probe.CompanyResponse
{
    public interface IAuthorizedSignatoryDetail
    {
        int Age { get; set; }
        string DateOfAppointment { get; set; }
        string DateOfAppointmentForCurrentDesignation { get; set; }
        string DateOfBirth { get; set; }
        string DateOfCessation { get; set; }
        string Din { get; set; }
        string Name { get; set; }
        string Pan { get; set; }
    }
}