﻿namespace CreditExchange.Syndication.Probe
{
    public class ProbeServiceConfiguration : IProbeServiceConfiguration
    {
        public string ApiVersion { get; set; } = "1.1";

        public string ApiKey { get; set; } = "ULexyqL8Ke6yBEj2rcTwO2VIlOP6oiZT3k5AtaYc";

        public string ServiceUrls { get; set; } = "https://api.probe42.in/probe_lite/";

        public int Limit { get; set; } = 25;
        public int Offset { get; set; } = 0;
    }
}