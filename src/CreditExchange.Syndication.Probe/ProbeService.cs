﻿using CreditExchange.Syndication.Probe.Proxy;
using CreditExchange.Syndication.Probe.Response;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using CreditExchange.Syndication.Probe.Events;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using System.Linq;
using Newtonsoft.Json;
using CreditExchange.Probe.Persistence;

namespace CreditExchange.Syndication.Probe
{

    public class ProbeService : IProbeService
    {
        public ProbeService(IProbeServiceConfiguration configurationservice, IProbeProxy probeproxy, IEventHubClient eventHub, ILookupService lookup, IProbeRepository probeRepository)
        {
            if (configurationservice == null)
                throw new ArgumentNullException(nameof(configurationservice));
            if (probeproxy == null)
                throw new ArgumentNullException(nameof(probeproxy));
            if (string.IsNullOrWhiteSpace(configurationservice.ServiceUrls))
                throw new ArgumentException("Url is required", nameof(configurationservice.ServiceUrls));
            if (string.IsNullOrWhiteSpace(configurationservice.ApiKey))
                throw new ArgumentException("ApiKey is required", nameof(configurationservice.ApiKey));
            if (string.IsNullOrWhiteSpace(configurationservice.ApiVersion))
                throw new ArgumentException("ApiVersion is required", nameof(configurationservice.ApiVersion));
            ProbeServiceConfiguration = configurationservice;
            ProbeProxy = probeproxy;
            EventHub = eventHub;
            Lookup = lookup;
            ProbeRepository = probeRepository;
        }
        public static string ServiceName { get; } = "probe";

        private IProbeRepository ProbeRepository { get; }

        private IProbeServiceConfiguration ProbeServiceConfiguration { get; }
        private IProbeProxy ProbeProxy { get; }
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }
        //public async Task<IGetSearchCompanyResponse> SearchCompany(string filter, string value)
        //{

        //    if (string.IsNullOrWhiteSpace(filter))
        //        throw new ArgumentException("Filter is require", nameof(filter));
        //    if (string.IsNullOrWhiteSpace(value))
        //        throw new ArgumentException("Value is require", nameof(value));

        //    var searchrequestproxy = new SearchRequest(filter,value);
        //    var searchproxyResponse =await ProbeProxy.SearchCompany(searchrequestproxy);
        //    var response = new GetSearchCompanyResponse(searchproxyResponse);
        //    var referencenumber = Guid.NewGuid().ToString("N");
        //    await EventHub.Publish(new CompanySearched
        //    {
        //        ReferenceNumber = referencenumber,
        //        Response = response,
        //        Request = new { filter, value }
        //    });
        //    response.ReferenceNumber = referencenumber;
        //    return response;
        //}

        //public async Task<IGetSearchAuthorizedSignatoryResponse> SearchAuthorizedSignatory(string filter, string value)
        //{

        //    if (string.IsNullOrWhiteSpace(filter))
        //        throw new ArgumentException("Filter is require", nameof(filter));
        //    if (string.IsNullOrWhiteSpace(value))
        //        throw new ArgumentException("Value is require", nameof(value));
        //    var searchrequestproxy = new SearchRequest(filter, value);
        //    var searchproxyResponse = await ProbeProxy.SearchAuthorizedSignatory(searchrequestproxy);
        //    var response = new GetSearchAuthorizedSignatoryResponse(searchproxyResponse);
        //    var referencenumber = Guid.NewGuid().ToString("N");
        //    await EventHub.Publish(new AuthorizedSignatorySearched
        //    {
        //        ReferenceNumber = referencenumber,
        //        Response = response,
        //        Request = new {filter, value}
        //    });
        //    response.ReferenceNumber = referencenumber;
        //    return response;
        //}

        //public async Task<IGetCompanyDetailResponse> GetCompanyDetail(string cin)
        //{
        //    if (string.IsNullOrWhiteSpace(cin))
        //        throw new ArgumentException("Cin is require", nameof(cin));
        //    if(!(new Regex(@"[A-Z]{1}[0-9]{5}[A-Z]{2}[0-9]{4}[A-Z]{3}[0-9]{6}").IsMatch(cin)))
        //        throw new ArgumentException("Invalid Cin format", nameof(cin));
        //    var companyDetailrequestproxy = new DetailRequest(ProbeServiceConfiguration, cin);
        //    var companyDetailproxyResponse = await ProbeProxy.GetCompanyDetail(companyDetailrequestproxy);
        //     var response = new GetCompanyDetailResponse(companyDetailproxyResponse);
        //    var referencenumber = Guid.NewGuid().ToString("N");
        //    await EventHub.Publish(new CompanyDetailRequested
        //    {
        //        ReferenceNumber = referencenumber,
        //        Response = response,
        //        Request = new { cin }
        //    });
        //    response.ReferenceNumber = referencenumber;
        //    return response;
        //}

        //public async Task<IGetAuthorisedSignatoryDetail> GetAuthorisedSignatoryDetail(string cin)
        //{
        //    if (string.IsNullOrWhiteSpace(cin))
        //        throw new ArgumentException("Cin is require", nameof(cin));
        //    if (!(new Regex(@"[A-Z]{1}[0-9]{5}[A-Z]{2}[0-9]{4}[A-Z]{3}[0-9]{6}").IsMatch(cin)))
        //        throw new ArgumentException("Invalid Cin format", nameof(cin));
        //    var authorisedSignatoryDetailrequest = new DetailRequest(ProbeServiceConfiguration, cin);
        //    var authorisedSignatoryDetailResponse = await ProbeProxy.GetAuthorizedSignatoryDetail(authorisedSignatoryDetailrequest);
        //    var response = new GetAuthorisedSignatoryDetail(authorisedSignatoryDetailResponse);
        //    var referencenumber = Guid.NewGuid().ToString("N");
        //    await EventHub.Publish(new AuthorisedSignatoryDetailRequested
        //    {
        //        ReferenceNumber = referencenumber,
        //        Response = response,
        //        Request = new { cin }
        //    });
        //    response.ReferenceNumber = referencenumber;
        //    return response;
        //}

        //public async Task<IGetChargesResponse> GetChargesDetail(string cin)
        //{

        //    if (string.IsNullOrWhiteSpace(cin))
        //        throw new ArgumentException("Cin is require", nameof(cin));
        //    if (!(new Regex(@"[A-Z]{1}[0-9]{5}[A-Z]{2}[0-9]{4}[A-Z]{3}[0-9]{6}").IsMatch(cin)))
        //        throw new ArgumentException("Invalid Cin format", nameof(cin));
        //    var chargesrequestproxy = new DetailRequest(ProbeServiceConfiguration, cin);
        //    var chargesproxyResponse = await ProbeProxy.GetChargesDetail(chargesrequestproxy);
        //    var response = new GetChargesResponse(chargesproxyResponse);
        //    var referencenumber = Guid.NewGuid().ToString("N");
        //    await EventHub.Publish(new ChargesDetailRequested
        //    {
        //        ReferenceNumber = referencenumber,
        //        Response = response,
        //        Request = new { cin }
        //    });
        //    response.ReferenceNumber = referencenumber;
        //    return response;
        //}

        //public async Task<IGetFinancialDataStatusResponse> GetFinancialDataStatus(string cin)
        //{
        //    if (string.IsNullOrWhiteSpace(cin))
        //        throw new ArgumentException("Cin is require", nameof(cin));
        //    if (!(new Regex(@"[A-Z]{1}[0-9]{5}[A-Z]{2}[0-9]{4}[A-Z]{3}[0-9]{6}").IsMatch(cin)))
        //        throw new ArgumentException("Invalid Cin format", nameof(cin));
        //    var financialDataStatusProxyRequest = new DetailRequest(ProbeServiceConfiguration, cin);
        //    var financialDataStatusProxyResponse = await ProbeProxy.GetFinancialDataStatus(financialDataStatusProxyRequest);
        //    var  response = new GetFinancialDataStatusResponse(financialDataStatusProxyResponse);
        //    var referencenumber = Guid.NewGuid().ToString("N");

        //    await EventHub.Publish(new FinancialDataStatusRequested
        //    {
        //        ReferenceNumber = referencenumber,
        //        Response = response,
        //        Request = new { cin }
        //    });

        //    response.ReferenceNumber = referencenumber;
        //    return response;
        //}

        public async Task<IGetFinancialDetailResponse> GetFinancialDetail(string entityType, string entityId, string cin)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("EntityType is require", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            if (string.IsNullOrWhiteSpace(cin))
                throw new ArgumentException("Cin is require", nameof(cin));
            if (!(new Regex(@"[A-Z]{1}[0-9]{5}[A-Z]{2}[0-9]{4}[A-Z]{3}[0-9]{6}").IsMatch(cin)))
                throw new ArgumentException("Invalid Cin format", nameof(cin));
            string proxyResponsestring = "";
            IGetFinancialDetailResponse response = null;
            try
            {
                entityType = EnsureEntityType(entityType);
               
                var financialDetailrequestproxy = new DetailRequest(ProbeServiceConfiguration, cin);
                proxyResponsestring = await ProbeProxy.GetFinancialDetail(financialDetailrequestproxy);
                response = new Response.FinancialDetailResponse(JsonConvert.DeserializeObject<Proxy.FinancialDetailResponse>(proxyResponsestring));
                if (response?.FinancialDetails == null)
                    throw new ProbeException("Probe Company Financial Report Call Failed");
                if (response?.FinancialDetails != null)
                {
                    var referencenumber = Guid.NewGuid().ToString("N");
                    await EventHub.Publish(new FinancialDetailRequested
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        ReferenceNumber = referencenumber,
                        Response = response,
                        Request = new { cin },
                        Name = ServiceName
                    });
                    response.ReferenceNumber = referencenumber;
                }
               

                return response;
            }
            catch (ProbeException exception)
            {
                await EventHub.Publish(new FinancialDetailRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = null,
                    Response = exception.Message,
                    Request = new { cin },
                    Name = ServiceName
                });
                throw new ProbeException(exception.Message);
            }
            finally
            {
                var proberepo = ProbeRepository.GetByEntityId(entityType, entityId, "companyfinancials").Result;
                if (proberepo != null)
                {
                    proberepo.EntityId = entityId;
                    proberepo.EntityType = entityType;
                    proberepo.Request = cin;
                    proberepo.Response = proxyResponsestring;
                    proberepo.ProcessDate = DateTime.Now;
                    proberepo.Api = "companyfinancials";
                    if (response?.FinancialDetails == null)
                        proberepo.Status = "failed";
                    else
                        proberepo.Status = "success";
                    ProbeRepository.Update(proberepo);
                }
                else
                {
                    var probedata = new ProbeData();
                    probedata.EntityId = entityId;
                    probedata.EntityType = entityType;
                    probedata.Request = cin;
                    probedata.Api = "companyfinancials";
                    probedata.Response = proxyResponsestring;
                    probedata.ProcessDate = DateTime.Now;
                    if (response?.FinancialDetails == null)
                        probedata.Status = "failed";
                    else
                        probedata.Status = "success";
                    ProbeRepository.Add(probedata);
                }
            }
        }
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}
