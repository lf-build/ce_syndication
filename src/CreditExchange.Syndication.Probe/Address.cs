﻿using CreditExchange.Syndication.Probe.Proxy;

namespace CreditExchange.Syndication.Probe
{
    public class Address : IAddress
    {
        public Address(AuthorizedSignatoryAddress address)
        {
            if (address != null)
            {
                AddressLine1 = address.address_line1;
                AddressLine2 = address.address_line2;
                City = address.city;
                PinCode = address.pincode;
                State = address.state;
            }
        }

        public Address(Registered_Address address)
        {
            if (address != null)
            {
                AddressLine1 = address.address_line1;
                AddressLine2 = address.address_line2;
                City = address.city;
                PinCode = address.pincode;
                State = address.state;
            }
        }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string PinCode { get; set; }
        public string State { get; set; }
    }
}