﻿namespace CreditExchange.Syndication.Probe.Proxy
{
    public partial class CompanySearchResponse
    {
        public SearchMetadata meta { get; set; }
        public CompanyData data { get; set; }
    }

    public class CompanyData
    {
        public SearchCompany[] companies { get; set; }
        public bool hasMore { get; set; }
        public int totalCount { get; set; }
    }
}