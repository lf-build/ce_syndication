﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.Probe.Proxy
{
    public class CompanyDetailResponse
    {
        [JsonProperty("meta")]
        public Metadata meta { get; set; }

        [JsonProperty("data")]
        public CompanyDetailData data { get; set; }
    }

    public class CompanyDetailData
    {
        [JsonProperty("company")]
        public Company company { get; set; }
    }

    public class Company
    {
        public int authorized_capital { get; set; }
        public string cin { get; set; }
        public string efiling_status { get; set; }
        public string incorporation_date { get; set; }
        public string legal_name { get; set; }
        public int paid_up_capital { get; set; }
        public int sum_of_charges { get; set; }

        [JsonProperty("registered_address")]
        public Registered_Address registered_address { get; set; }

        public string classification { get; set; }
        public string status { get; set; }
        public string next_cin { get; set; }
        public string last_agm_date { get; set; }
        public string last_filing_date { get; set; }
    }

    public class Registered_Address
    {
        public string address_line1 { get; set; }
        public string address_line2 { get; set; }
        public string city { get; set; }
        public string pincode { get; set; }
        public string state { get; set; }
    }
}