﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.Probe.Proxy
{
    public class AuthorizedSignatorySearchResponse
    {
        [JsonProperty("meta")]
        public SearchMetadata meta { get; set; }
        [JsonProperty("data")]
        public AuthorizedSignatoryData data { get; set; }
    }
   
    public class AuthorizedSignatoryData
    {
        [JsonProperty("authorized-signatories")]
        public AuthorizedSignatories[] authorizedsignatories { get; set; }
        [JsonProperty("hasMore")]
        public bool hasMore { get; set; }
        [JsonProperty("totalCount")]
        public int totalCount { get; set; }
    }

    public class AuthorizedSignatories
    {
        public string name { get; set; }
        public string pan { get; set; }
        public string din { get; set; }
        public string date_of_birth { get; set; }
        public int age { get; set; }
        public string nationality { get; set; }
        [JsonProperty("address")]
        public AuthorizedSignatoryAddress address { get; set; }
        [JsonProperty("companies")]
        public SearchCompany[] companies { get; set; }
    }

    public class AuthorizedSignatoryAddress
    {
        public string address_line1 { get; set; }
        public string address_line2 { get; set; }
        public string city { get; set; }
        public string pincode { get; set; }
        public string state { get; set; }
    }
}