﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.Probe.Proxy
{
    public class AuthorizedSignatoryResponse
    {
        [JsonProperty("meta")]
        public Metadata meta { get; set; }

        [JsonProperty("data")]
        public CompanyAuthorizedSignatories data { get; set; }
    }

    public class CompanyAuthorizedSignatories
    {
        [JsonProperty("authorized-signatories")]
        
        public AuthorizedSignatoriesDetail[] authorizedsignatories { get; set; }
    }

    public class AuthorizedSignatoriesDetail
    {
        public string name { get; set; }
        public string pan { get; set; }
        public string din { get; set; }
        public string date_of_birth { get; set; }
        public int age { get; set; }
        public string date_of_appointment { get; set; }
        public string date_of_appointment_for_current_designation { get; set; }
        public string date_of_cessation { get; set; }
    }
}