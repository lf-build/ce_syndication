﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.Probe.Proxy
{
    public class FinancialDetailResponse
    {
        [JsonProperty("meta")]
        public Metadata meta { get; set; }
        [JsonProperty("data")]
        public FinancialData data { get; set; }
    }

     
  

    public class FinancialData
    {
        [JsonProperty("financials")]
        
        public Financial[] financials { get; set; }
    }

    public class Financial
    {
        public string year { get; set; }
        public string nature { get; set; }
        public string stated_on { get; set; }
        public double? capital_wip { get; set; }
        public double? cash_and_bank_balances { get; set; }
        public double? investments { get; set; }
        public double? inventories { get; set; }
        public double? net_fixed_assets { get; set; }
        public double? other_assets { get; set; }
        public double? total_assets { get; set; }
        public double? trade_receivables { get; set; }
        public double? share_capital { get; set; }
        public double? reserves_and_surplus { get; set; }
        public double? others { get; set; }
        public double? long_term_borrowings { get; set; }
        public double? other_current_liabilities_and_provisions { get; set; }
        public double? other_long_term_liabilities_and_provisions { get; set; }
        public double? trade_payables { get; set; }
        public double? short_term_borrowings { get; set; }
        public double? total_equity { get; set; }
        public double? total_equity_and_liabilities { get; set; }
        public double? depreciation { get; set; }
        public double? income_tax { get; set; }
        public double? interest { get; set; }
        public double?  operating_cost { get; set; }
        public double? operating_profit { get; set; }
        public double? other_income { get; set; }
        public double? profit_before_interest_and_tax { get; set; }
        public double? profit_before_tax { get; set; }
        public double? revenue { get; set; }
        public double? exceptional_item_before_tax { get; set; }
        public double? exceptional_items_before_tax { get; set; }
        public double? minority_interest_and_profit_from_associates_and_joint_ventures { get; set; }
        public double? profit_after_tax_but_before_exceptional_items_after_tax { get; set; }
        public double? profit_before_tax_and_exceptional_items_before_tax { get; set; }
        public double? profit_after_tax { get; set; }
        public string pnl_version { get; set; }
        public double? net_income { get; set; }
    }

}
