﻿namespace CreditExchange.Syndication.Probe.Proxy
{
    public partial class SearchRequest
    {
      public string Filter { get; set; }

      public  string Value { get; set; }

    }
}