﻿using System;

namespace CreditExchange.Syndication.Probe.Proxy
{
    public partial class DetailRequest
    {
        public DetailRequest()
        {
        }

        public DetailRequest(IProbeServiceConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (string.IsNullOrWhiteSpace(configuration.ApiKey))
                throw new ArgumentException("Api Key is require", nameof(configuration.ApiKey));
            if (string.IsNullOrWhiteSpace(configuration.ApiVersion))
                throw new ArgumentException("Api Version is require", nameof(configuration.ApiVersion));
            if (string.IsNullOrWhiteSpace(configuration.ServiceUrls))
                throw new ArgumentException("Api Url is require", nameof(configuration.ServiceUrls));
        }

        public DetailRequest(IProbeServiceConfiguration configuration,string cin) : this(configuration)
        {
            if (string.IsNullOrWhiteSpace(cin))
                throw new ArgumentException("Cin is require", nameof(cin));
            Cin = cin;
        }
    }
}