﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.Probe.Proxy
{
    public class SearchMetadata
    {
        [JsonProperty("api-version")]
        public string apiversion { get; set; }
    }
}