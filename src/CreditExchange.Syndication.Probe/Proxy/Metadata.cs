﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.Probe.Proxy
{
    public class Metadata
    {
        [JsonProperty("api-version")]
        public string apiversion { get; set; }

        public string last_updated { get; set; }
    }
}