﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.Probe.Proxy
{
    public class FinancialDataStatusResponse
    {
        [JsonProperty("meta")]
        public SearchMetadata meta { get; set; }
        [JsonProperty("data")]
        public FinancialDataData data { get; set; }
    }

    public class FinancialDataData
    {
        [JsonProperty("data-status")]
        public FinancialDataDataStatus datastatus { get; set; }
    }

    public class FinancialDataDataStatus
    {
        public string last_fin_year_end { get; set; }
        public string last_updated { get; set; }
    }
}