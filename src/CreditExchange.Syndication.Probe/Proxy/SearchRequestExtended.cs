﻿using System;

namespace CreditExchange.Syndication.Probe.Proxy
{
    public partial class SearchRequest
    {
        public SearchRequest()
        {
        }

        public SearchRequest(IProbeServiceConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (string.IsNullOrWhiteSpace(configuration.ApiKey))
                throw new ArgumentException("Api Key is require", nameof(configuration.ApiKey));
            if (string.IsNullOrWhiteSpace(configuration.ApiVersion))
                throw new ArgumentException("Api Version is require", nameof(configuration.ApiVersion));
            if (string.IsNullOrWhiteSpace(configuration.ServiceUrls))
                throw new ArgumentException("Api Url is require", nameof(configuration.ServiceUrls));
        }

        public SearchRequest(string filter, string value) 
        {
            Filter = filter;
            Value = value;
        }
    }
}