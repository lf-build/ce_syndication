﻿using LendFoundry.Foundation.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Probe.Proxy
{
    public class ProbeProxy : IProbeProxy
    {
        private IProbeServiceConfiguration ProbeServiceConfiguration { get; }
        private ILogger Logger { get; }
        public ProbeProxy(IProbeServiceConfiguration probeserviceconfiguration, ILogger logger)
        {
            if (probeserviceconfiguration == null)
                throw new ArgumentNullException(nameof(probeserviceconfiguration));
            if (string.IsNullOrWhiteSpace(probeserviceconfiguration.ServiceUrls))
                throw new ArgumentException("Url is required", nameof(probeserviceconfiguration.ServiceUrls));
            if (string.IsNullOrWhiteSpace(probeserviceconfiguration.ApiKey))
                throw new ArgumentException("ApiKey is required", nameof(probeserviceconfiguration.ApiKey));
            if (string.IsNullOrWhiteSpace(probeserviceconfiguration.ApiVersion))
                throw new ArgumentException("ApiVersion is required", nameof(probeserviceconfiguration.ApiVersion));
            ProbeServiceConfiguration = probeserviceconfiguration;
            Logger = logger;
        }

        public async Task<string> SearchCompany(SearchRequest searchrequest)
        {
            if (searchrequest == null)
                throw new ArgumentNullException(nameof(searchrequest));
            var client = new RestClient(ProbeServiceConfiguration.ServiceUrls + "companies");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("x-api-key", ProbeServiceConfiguration.ApiKey);
            request.AddHeader("x-api-version", ProbeServiceConfiguration.ApiVersion);
            request.AddParameter("limit", ProbeServiceConfiguration.Limit);
            request.AddParameter("filters", "%7B%22" + searchrequest.Filter+ "%22%3A%22"+searchrequest.Value+ "%22%7D");
            request.AddParameter("offset", ProbeServiceConfiguration.Offset);
            var objectResponse =await ExecuteRequestAsync<CompanySearchResponse>(client, request);
            Logger.Info($"[Probe] SearchCompany method response: ",objectResponse);
            return objectResponse;
        }

        public async Task<string> SearchAuthorizedSignatory(SearchRequest searchrequest)
        {
            if (searchrequest == null)
                throw new ArgumentNullException(nameof(searchrequest));
            var client = new RestClient(ProbeServiceConfiguration.ServiceUrls + "authorized-signatories/");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("x-api-key", ProbeServiceConfiguration.ApiKey);
            request.AddHeader("x-api-version", ProbeServiceConfiguration.ApiVersion);
            request.AddParameter("limit", ProbeServiceConfiguration.Limit);
            request.AddParameter("filters", "%7B%22" + searchrequest.Filter + "%22%3A%22" + searchrequest.Value + "%22%7D");
            request.AddParameter("offset", ProbeServiceConfiguration.Offset);
            var objectResponse = await ExecuteRequestAsync<AuthorizedSignatorySearchResponse>(client, request);
            Logger.Info($"[Probe] SearchAuthorizedSignatory method response: ", objectResponse);
            return objectResponse;
        }


        public async Task<string> GetCompanyDetail(DetailRequest companydetailrequest)
        {
            if (companydetailrequest == null)
                throw new ArgumentNullException(nameof(companydetailrequest));
            var client = new RestClient(ProbeServiceConfiguration.ServiceUrls + "companies/");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("x-api-key", ProbeServiceConfiguration.ApiKey);
            request.AddHeader("x-api-version", ProbeServiceConfiguration.ApiVersion);
            request.AddParameter("cin", companydetailrequest.Cin);
            
            var objectResponse = await ExecuteRequestAsync<CompanyDetailResponse>(client, request);
            Logger.Info($"[Probe] GetCompanyDetail method response: ", objectResponse);
            return objectResponse;
        }

        public async Task<string> GetAuthorizedSignatoryDetail(DetailRequest companydetailrequest)
        {
            if (companydetailrequest == null)
                throw new ArgumentNullException(nameof(companydetailrequest));
            var client = new RestClient(ProbeServiceConfiguration.ServiceUrls + "companies/"+companydetailrequest.Cin+ "/authorized-signatories");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("x-api-key", ProbeServiceConfiguration.ApiKey);
            request.AddHeader("x-api-version", ProbeServiceConfiguration.ApiVersion);
            var objectResponse = await ExecuteRequestAsync<AuthorizedSignatoryResponse>(client, request);
            Logger.Info($"[Probe] GetAuthorizedSignatoryDetail method response: ", objectResponse);
            return objectResponse;
        }

        public async Task<string> GetChargesDetail(DetailRequest companydetailrequest)
        {
            if (companydetailrequest == null)
                throw new ArgumentNullException(nameof(companydetailrequest));
            var client = new RestClient(ProbeServiceConfiguration.ServiceUrls + "companies/" + companydetailrequest.Cin + "/charges");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("x-api-key", ProbeServiceConfiguration.ApiKey);
            request.AddHeader("x-api-version", ProbeServiceConfiguration.ApiVersion);
            var objectResponse = await ExecuteRequestAsync<ChargesResponse>(client, request);
            Logger.Info($"[Probe] GetChargesDetail method response: ", objectResponse);
            return objectResponse;
        }

        public async Task<string> GetFinancialDataStatus(DetailRequest companydetailrequest)
        {
            if (companydetailrequest == null)
                throw new ArgumentNullException(nameof(companydetailrequest));
            var client = new RestClient(ProbeServiceConfiguration.ServiceUrls + "companies/" + companydetailrequest.Cin + "/financial-datastatus");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("x-api-key", ProbeServiceConfiguration.ApiKey);
            request.AddHeader("x-api-version", ProbeServiceConfiguration.ApiVersion);
            var objectResponse = await ExecuteRequestAsync<FinancialDataStatusResponse>(client, request);
            Logger.Info($"[Probe] GetFinancialDataStatus method response: ", objectResponse);
            return objectResponse;
        }
        public async Task<string> GetFinancialDetail(DetailRequest companydetailrequest)
        {
            if (companydetailrequest == null)
                throw new ArgumentNullException(nameof(companydetailrequest));
            var client = new RestClient(ProbeServiceConfiguration.ServiceUrls + "companies/" + companydetailrequest.Cin + "/financials");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("x-api-key", ProbeServiceConfiguration.ApiKey);
            request.AddHeader("x-api-version", ProbeServiceConfiguration.ApiVersion);
            var objectResponse = await ExecuteRequestAsync<FinancialDetailResponse>(client, request);
            Logger.Info($"[Probe] GetFinancialDetail method response: ", new { objectResponse});
            return objectResponse;
        }
        private async Task<string> ExecuteRequestAsync<T>(IRestClient client, IRestRequest request) where T : class
        {
            var response =await client.ExecuteTaskAsync(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new ProbeException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new ProbeException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
                    throw new ProbeException(
                        $"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");
            }

            return response.Content;
        }
    }
}
