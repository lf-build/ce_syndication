﻿using System.Threading.Tasks;

namespace CreditExchange.Syndication.Probe.Proxy
{
    public interface IProbeProxy
    {
       Task<string> GetAuthorizedSignatoryDetail(DetailRequest companydetailrequest);

      Task<string> GetChargesDetail(DetailRequest companydetailrequest);

       Task<string> GetCompanyDetail(DetailRequest companydetailrequest);

       Task<string> GetFinancialDataStatus(DetailRequest companydetailrequest);

        Task<string> GetFinancialDetail(DetailRequest companydetailrequest);

        Task<string> SearchAuthorizedSignatory(SearchRequest searchrequest);

        Task<string> SearchCompany(SearchRequest searchrequest);
    }
}