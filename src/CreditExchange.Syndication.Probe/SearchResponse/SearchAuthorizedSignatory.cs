﻿using CreditExchange.Syndication.Probe.Proxy;
using System.Collections.Generic;
using System.Linq;

namespace CreditExchange.Syndication.Probe.SearchResponse
{
    public class SearchAuthorizedSignatory : ISearchAuthorizedSignatory
    {
        public SearchAuthorizedSignatory(AuthorizedSignatories authorizedSingnatory)
        {
            if (authorizedSingnatory != null)
            {
                Name = authorizedSingnatory.name;
                Pan = authorizedSingnatory.pan;
                Din = authorizedSingnatory.din;
                DateOfBirth = authorizedSingnatory.date_of_birth;
                Age = authorizedSingnatory.age;
                Nationality = authorizedSingnatory.nationality;
                if (authorizedSingnatory.address != null)
                    Address = new Address(authorizedSingnatory.address);
                if (authorizedSingnatory.companies != null)
                    Companies = authorizedSingnatory.companies.Select(p => new SearchCompanyResponse(p)).ToList<ISearchCompanyResponse>();
            }
        }

        public string Name { get; set; }
        public string Pan { get; set; }
        public string Din { get; set; }
        public string DateOfBirth { get; set; }
        public int Age { get; set; }
        public string Nationality { get; set; }
        public IAddress Address { get; set; }
        public List<ISearchCompanyResponse> Companies { get; set; }
    }
}