﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Probe.SearchResponse
{
    public interface ISearchAuthorizedSignatory
    {
        IAddress Address { get; set; }
        int Age { get; set; }
        List<ISearchCompanyResponse> Companies { get; set; }
        string DateOfBirth { get; set; }
        string Din { get; set; }
        string Name { get; set; }
        string Nationality { get; set; }
        string Pan { get; set; }
    }
}