﻿namespace CreditExchange.Syndication.Probe.SearchResponse
{
    public interface ISearchCompanyResponse
    {
        string Cin { get; set; }
        string LegalName { get; set; }
    }
}