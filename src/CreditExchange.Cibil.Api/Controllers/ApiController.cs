﻿﻿#if DOTNET2
using  Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.Cibil;
using System;
using System.Threading.Tasks;
using CreditExchange.Syndication.Cibil.Request;
using LendFoundry.Foundation.Logging;
using Newtonsoft.Json;

namespace CreditExchange.Cibil.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(ICibilReportService cibilReportService,ILogger logger):base(logger)
        {
            if (cibilReportService == null)
                throw new ArgumentNullException(nameof(cibilReportService));

            CibilReportService = cibilReportService;
           
        }
        private ICibilReportService CibilReportService { get; }




        [HttpPost("{entitytype}/{entityid}")]
        #if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.Cibil.Response.IReport), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetCibilReport(string entitytype, string entityid,
           [FromBody] ReportRequest request)
        {

            Logger.Info($"[Cibil] Fetch Report endpoint invoked for [{entitytype}] with id #{entityid} with  data { (request != null ? JsonConvert.SerializeObject(request) : null)}");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await CibilReportService.GetReport(entitytype, entityid, request));
                }
                catch (CibilException exception)
                {
                    Logger.Error($"[Cibil] Error occured when Fetch Report endpoint was called for [{entitytype}] with id #{entityid} with  data { (request != null ? JsonConvert.SerializeObject(request) : null)}", exception);
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
        [HttpGet("{entitytype}/{entityid}/htmlreport")]
        #if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.Cibil.ICIRHtml), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetHtmlReport(string entitytype, string entityid)
        {
            Logger.Info($"[Cibil] Fetch HtmlReport endpoint invoked for [{entitytype}] with id #{entityid}");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await CibilReportService.GetHtmlReport(entitytype, entityid));
                }
                catch (CibilException exception)
                {
                    Logger.Error($"[Cibil] Error occured when Fetch HtmlReport endpoint was called for [{entitytype}] with id #{entityid} ", exception);
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        [HttpGet("{entitytype}/{entityid}/cibildata")]
        #if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.Cibil.ICibilData), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetCibilData(string entitytype, string entityid)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await CibilReportService.GetCibilData(entitytype, entityid));
            });
        }
    }
}
