﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Cibil.Api
{
    public class Settings
    {
        public static string ServiceName { get; } = "cibil";

        private static string Prefix { get; } = ServiceName.ToUpper();
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");
        public static ServiceSettings TlsProxy { get; } = new ServiceSettings($"{Prefix}_TLSPROXY", "tlsproxy");
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");
        public static ServiceSettings LookupService { get; } = new ServiceSettings($"{Prefix}_LOOKUPSERVICE", "lookup-service");
        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "cibilData");
        public static ServiceSettings DecisionEngine { get; } = new ServiceSettings($"{Prefix}_DECISIONENGINE_HOST", "decision-engine", $"{Prefix}_DECISIONENGINE_PORT");
    }
}
