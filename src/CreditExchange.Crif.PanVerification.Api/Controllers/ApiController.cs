﻿using System;
using System.Threading.Tasks;
#if DOTNET2
using  Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.Crif;
using CreditExchange.Syndication.Crif.PanVerification.Request;
using CreditExchange.Syndication.Crif.PanVerification;
using LendFoundry.Foundation.Logging;
using Newtonsoft.Json;

namespace CreditExchange.Crif.PanVerification.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(ICrifPanVerificationService crifVerificationService,ILogger logger):base(logger)
        {
            if (crifVerificationService == null)
                throw new ArgumentNullException(nameof(crifVerificationService));

            CrifVerificationService = crifVerificationService;

        }

        private ICrifPanVerificationService CrifVerificationService { get; }

        [HttpPost("{entitytype}/{entityid}")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.Crif.PanVerification.Response.IPanVerificationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> VerifyPan(string entitytype, string entityid,
            [FromBody] PanVerificationRequest panverificationrequest)
        {
            Logger.Info($"[CRIF] VerifyPan endpoint invoked for [{entitytype}] with id #{entityid} with  data { (panverificationrequest != null ? JsonConvert.SerializeObject(panverificationrequest) : null)}");
            return await ExecuteAsync(
                async () =>
                {
                    try
                    {
                        return Ok(await CrifVerificationService.VerifyPan(entitytype, entityid, panverificationrequest));
                    }
                    catch (CrifException exception)
                    {
                        Logger.Error($"[CRIF] Error occured when VerifyPan endpoint was called for [{entitytype}] with id #{entityid} with  data { (panverificationrequest != null ? JsonConvert.SerializeObject(panverificationrequest) : null)}", exception);
                        return ErrorResult.BadRequest(exception.Message);
                    }
                });
        }
    }
}
