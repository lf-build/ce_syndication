﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Crif.PanVerification.Api
{
    public class Settings
    {
        public static string ServiceName { get; } = "crif-pan-verification";
        private static string Prefix { get; } = ServiceName.ToUpper();
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");
        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");
        public static ServiceSettings LookupService { get; } = new ServiceSettings($"{Prefix}_LOOKUPSERVICE", "lookup-service");
    }
}
