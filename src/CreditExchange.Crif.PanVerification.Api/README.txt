##### Availalbe End Points:

1. Verify Pan
   ```
   - POST ({entitytype}/{entityid})
   - Body {  "name": "NITIN JAIN", "gender":"Male", "dob":"2000-09-08",  "pan":"AFUPJ7365N",}  
   ``` 
 
#**Configurations**

1. {{default_host}}:{{configuration_port}}/crif-pan-verification
```
{
    "LosName": "LosName",
    "Certificate": "MIIEZzCCA0+gAwIBAgIJAJ7Fg6eL2STKMA0GCSqGSIb3DQEBBQUAMIHJMQswCQYDVQQGEwJJTjEUMBIGA1UECAwLTWFoYXJhc2h0cmExKTAnBgNVBAcMIDQwMiwgU2hlaWwgRXN0YXRlLCAxNTgsIENTVCBSb2FkMRkwFwYDVQQKDBB0ZXN0LmhpZ2htYXJrLmluMRMwEQYDVQQLDAogSGlnaCBNYXJrMRkwFwYDVQQDDBB0ZXN0LmhpZ2htYXJrLmluMS4wLAYJKoZIhvcNAQkBFh92aW5heS5jaG91ZGhhcnlAY3JpZmhpZ2htYXJrLmluMB4XDTE1MTEwMjExMzIxM1oXDTE3MTEwMTExMzIxM1owgckxCzAJBgNVBAYTAklOMRQwEgYDVQQIDAtNYWhhcmFzaHRyYTEpMCcGA1UEBwwgNDAyLCBTaGVpbCBFc3RhdGUsIDE1OCwgQ1NUIFJvYWQxGTAXBgNVBAoMEHRlc3QuaGlnaG1hcmsuaW4xEzARBgNVBAsMCiBIaWdoIE1hcmsxGTAXBgNVBAMMEHRlc3QuaGlnaG1hcmsuaW4xLjAsBgkqhkiG9w0BCQEWH3ZpbmF5LmNob3VkaGFyeUBjcmlmaGlnaG1hcmsuaW4wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDDcACUQjYbIuvE3yWOcX69cN720kzl294UflD/6xNy4DsLGkWQCTgCBR6q6qV/41jedNmksJsAN56FpIVOJvXvH+IeyiVNaFDB5AUhu6CTpmKAMuTwuMYH5cjieKmNf+VZSlD27GiApvrTbIRwKrNf+NaX9zqG5pEoAPy2CxJUUBrpiPsILcLv7VYzazEPVfKDXiyDbaTymp01yfQHfiaQPKC5nMeduX2Qf3qECT6wNHlwBihnKjIDDNOJ7n5Q3bwUIJA39qs8IJyOLbw3Xz9N4eYH0g2hgss42DBN+s5bhcndEiA8UanWQR2KN2rgyu8cIQxfljgyr2D8xj5y7ud1AgMBAAGjUDBOMB0GA1UdDgQWBBQfZnZWajW80PDuU3yiE8IbDe7+uDAfBgNVHSMEGDAWgBQfZnZWajW80PDuU3yiE8IbDe7+uDAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBAQAY7djP9kk8t0gfok2nE0tgInyEIhlhJzrrV9L37a0rCs/coXPLIJf3/AT97vyjic/lXxMdol58/EBQR7fcQIvVubfxKwnxaVF+evlvwUGTu/iU2y37FbKDQTaMTmnR9Pv/o3syvV6CJeyXHZxN5TxLyhsd+sKcrhdakbDWoSlZHa7Tjs8Om8aPiWGz3DgxmyrGXiGfT4I+YUfX2uisc/nm65sZ70ClCby7YouJoOuRWWKff2W6x60JoVHAbXcskba8oiHnBkufQ8piHejvEnMFwhF+dPGifyITmOEK3FVcto3CVBHavmsRIqjqJqOR3UsMfth7LygxAKJRf8OgctCE",
    "Password": "8F4EFE649A06987F6D8697B30B72D55284AD6DF3",
    "UserId": "uat@greatmeera.in",
    "MemberId": "NBF0000119",
    "ProductType": "IDENCHECK",
    "TestFlag": "HMTEST",
    "LosVendor": "LosVendor",
    "MemberPreOverride": "N",
    "AuthTitle": "USER",
    "AuthFlag": "Y",
    "RequestVolumeType": "INDV",
    "LosVersion": "LosVersion",
    "SubMemberId": "GREAT MEERA FINLEASE",
    "ApiUrl": "https://test.highmark.in/Inquiry/Verification/VerificationService",
    "ProductVersion": "2.0",
    "ActionType":"SUBMIT",
    "ResponseFormat":"XML",
    "InquiryDateTime":"2000-09-08T12:42:17.9254896+05:30"
  }
```
