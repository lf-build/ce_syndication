﻿using CreditExchange.Syndication.ViewDNS.Proxy;
using CreditExchange.Syndication.ViewDNS.Response;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using CreditExchange.Syndication.ViewDNS.Events;

using CreditExchange.CompanyDb.Abstractions;
using LendFoundry.DataAttributes;
using LendFoundry.EmailVerification;
//using CreditExchange.Syndication.EmailHunter;
//using CreditExchange.Syndication.TowerData;
using LendFoundry.Clients.DecisionEngine;
//using CreditExchange.Syndication.EmailHunter.Configuration;
using System.Globalization;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using System.Linq;
using Newtonsoft.Json.Linq;
using Microsoft.CSharp.RuntimeBinder;

namespace CreditExchange.Syndication.ViewDNS
{
    /// <summary>
    /// View Dns service class
    /// </summary>
    /// <seealso cref="CreditExchange.Syndication.ViewDNS.IViewDnsService" />
    public class ViewDnsService : IViewDnsService
    {
#region Variables

        /// <summary>
        /// Gets the view DNS proxy.
        /// </summary>
        /// <value>
        /// The view DNS proxy.
        /// </value>
        private IViewDnsProxy ViewDnsProxy { get; }

        /// <summary>
        /// Gets the event hub.
        /// </summary>
        /// <value>
        /// The event hub.
        /// </value>
        private IEventHubClient EventHub { get; }

        //private ITowerDataService TowerDataService { get; }
        /// <summary>
        /// Gets the company detail.
        /// </summary>
        /// <value>
        /// The company detail.
        /// </value>
        private ICompanyService CompanyService { get; }

        /// <summary>
        /// Gets the email hunter service.
        /// </summary>
        /// <value>
        /// The email hunter service.
        /// </value>
        private IDecisionEngineService DecisionEngineService { get; }

        //private IEmailHunterService EmailHunterService { get; }
        /// <summary>
        /// Gets the entity validator.
        /// </summary>
        /// <value>
        /// The entity validator.
        /// </value>
   

        /// <summary>
        /// Gets the filter domain.
        /// </summary>
        /// <value>
        /// The filter domain.
        /// </value>
        private IFilterDomain FilterDomain { get; }

        /// <summary>
        /// Gets the email verification service.
        /// </summary>
        /// <value>
        /// The email verification service.
        /// </value>
        private IEmailVerificationService EmailVerificationService { get; }

        /// <summary>
        /// Gets the data attributes engine.
        /// </summary>
        /// <value>
        /// The data attributes engine.
        /// </value>
        private IDataAttributesEngine DataAttributesEngine { get; }


        private IViewDnsConfiguration ViewDnsConfiguration { get; }

        /// <summary>
        /// Gets the name of the service.
        /// </summary>
        /// <value>
        /// The name of the service.
        /// </value>
        public static string ServiceName { get; } = "view-dns";

#endregion
        private ILookupService Lookup { get; }
#region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewDnsService" /> class.
        /// </summary>
        /// <param name="viewDnsProxy">The view DNS proxy.</param>
        /// <param name="companyService">The company service.</param>
        /// <param name="emailHunterService">The email hunter service.</param>
        /// <param name="filterDomain">The filter domain.</param>
        /// <param name="eventHub">The event hub.</param>
        /// <param name="entityValidator">The entity validator.</param>
        /// <param name="emailVerificationService">The email verification service.</param>
        /// <param name="dataAttributesEngine">The data attributes engine.</param>
        /// <exception cref="System.ArgumentNullException">viewDnsProxy</exception>
        //public ViewDnsService(IViewDnsProxy viewDnsProxy, ICompanyService companyService, IViewDnsConfiguration viewDnsConfiguration, IEmailHunterService emailHunterService, 
        //    IDecisionEngineService decisionEngineService, ITowerDataService towerDataService, IFilterDomain filterDomain, IEventHubClient eventHub,
        //    IEmailVerificationService emailVerificationService, IDataAttributesEngine dataAttributesEngine, ILookupService lookup)
        //{
        //    if (viewDnsProxy == null)
        //        throw new ArgumentNullException(nameof(viewDnsProxy));
        //    DecisionEngineService = decisionEngineService;
        //    ViewDnsProxy = viewDnsProxy;
        //    CompanyService = companyService;
        //    TowerDataService = towerDataService;
        //    FilterDomain = filterDomain;
        //    EventHub = eventHub;
        //    ViewDnsConfiguration = viewDnsConfiguration;
        //    Lookup = lookup;
        //    //EmailHunterService = emailHunterService;
        //    EmailVerificationService = emailVerificationService;
        //    DataAttributesEngine = dataAttributesEngine;
        //}
        public ViewDnsService(IViewDnsProxy viewDnsProxy, ICompanyService companyService, IViewDnsConfiguration viewDnsConfiguration, 
            IDecisionEngineService decisionEngineService,  IFilterDomain filterDomain, IEventHubClient eventHub,
            IEmailVerificationService emailVerificationService, IDataAttributesEngine dataAttributesEngine, ILookupService lookup)
        {
            if (viewDnsProxy == null)
                throw new ArgumentNullException(nameof(viewDnsProxy));
            DecisionEngineService = decisionEngineService;
            ViewDnsProxy = viewDnsProxy;
            CompanyService = companyService;
            
            FilterDomain = filterDomain;
            EventHub = eventHub;
            ViewDnsConfiguration = viewDnsConfiguration;
            Lookup = lookup;
            
            EmailVerificationService = emailVerificationService;
            DataAttributesEngine = dataAttributesEngine;
        }
#endregion

#region Service

        /// <summary>
        /// Gets the employee verification.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Email is require - emailAddress
        /// or
        /// Invalid email address - emailAddress</exception>
        public async Task<IEmployeeVerification> VerifyEmployment(string entityType, string entityId, string emailAddress, string companyName, string cin)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(entityType))
                    throw new ArgumentException("Entity type is require", nameof(entityType));
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentException("Entity identifier is require", nameof(entityId));
                entityType = EnsureEntityType(entityType);

                if (string.IsNullOrWhiteSpace(emailAddress))
                    throw new ArgumentException("Email address is require", nameof(emailAddress));
                if (string.IsNullOrWhiteSpace(companyName))
                    throw new ArgumentException("Company name is require", nameof(companyName));
                if (string.IsNullOrWhiteSpace(cin))
                    throw new ArgumentException("Company id is require", nameof(cin));

                if (FilterDomain.IsValidEmail(emailAddress) == false)
                    throw new ArgumentException("Invalid email address", nameof(emailAddress));

                var domain = FilterDomain.GetDomainFromEmailAddress(emailAddress);

                if (string.IsNullOrWhiteSpace(domain))
                    throw new ArgumentException("Invalid email address", nameof(emailAddress));

                var company = await CompanyService.GetCompanyWithDirectors(entityType, entityId, cin);
                if (company?.Company == null)
                    return await GetEmployeeVerifyResponse(false, "None", emailAddress, companyName, cin, entityType, entityId);

                var employeeVerification = new EmployeeVerification
                {
                    ReferenceNumber = Guid.NewGuid().ToString("N"),
                    DomainMatched = false,
                    VerificationMethod = "None"
                };

                //VerifyDomain verifyDomain = new EmailHunterDomainVerification(employeeVerification, ViewDnsConfiguration, EmailHunterService, DecisionEngineService, TowerDataService, company, emailAddress, domain, companyName, ViewDnsProxy);
                VerifyDomain verifyDomain = new EmailHunterDomainVerification(employeeVerification, ViewDnsConfiguration,  DecisionEngineService,  company, emailAddress, domain, companyName, ViewDnsProxy);
                await verifyDomain.ExecuteStep();

                if (employeeVerification.DomainMatched)
                {
                    await InitiateEmploymentVerification(entityType, entityId, emailAddress);
                }

                await EventHub.Publish(new DomainSearched
                {
                    EntityType = entityType,
                    EntityId = entityId,
                    Response = employeeVerification,
                    Request = new { emailAddress, companyName, cin },
                    ReferenceNumber = employeeVerification.ReferenceNumber,
                    Name = ServiceName
                });

                return employeeVerification;
            }
            catch (Exception ex)
            {
                await EventHub.Publish(new DomainSearchedFailed
                {
                    EntityType = entityType,
                    EntityId = entityId,
                    Response = ex.Message,
                    Request = new { emailAddress, companyName, cin },
                    ReferenceNumber = null,
                    Name = ServiceName
                });
                throw new ViewDnsException(ex.Message);
            }
        }

#endregion

#region Extra methods

        /// <summary>
        /// Gets the employee verify response.
        /// </summary>
        /// <param name="response">if set to <c>true</c> [response].</param>
        /// <param name="cin"></param>
        /// <param name="entitytype">The entitytype.</param>
        /// <param name="entityid">The entityid.</param>
        /// <param name="verificationMethod"></param>
        /// <param name="emailAddress"></param>
        /// <param name="companyName"></param>
        /// <returns></returns>
        private async Task<IEmployeeVerification> GetEmployeeVerifyResponse(bool response, string verificationMethod, string emailAddress, string companyName, string cin, string entitytype, string entityid)
        {
            IEmployeeVerification employeeVerifyResponse = new EmployeeVerification(response, verificationMethod);
            employeeVerifyResponse.ReferenceNumber = Guid.NewGuid().ToString("N");
            await EventHub.Publish(new DomainSearched
            {
                EntityType = entitytype,
                EntityId = entityid,
                Response = employeeVerifyResponse,
                Request = new { emailAddress, companyName, cin },
                ReferenceNumber = employeeVerifyResponse.ReferenceNumber,
                Name = ServiceName
            });
            return employeeVerifyResponse;
        }

        /// <summary>
        /// Initiates the employment verification.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="emailAddress">The email address.</param>
        /// <returns></returns>
        /// <exception cref="System.InvalidOperationException">Data attributes not found for application</exception>
        private async Task InitiateEmploymentVerification(string entityType, string entityId, string emailAddress)
        {
            var attributes = await DataAttributesEngine.GetAttribute("application", entityId, "application");
            //JArray attributes = (JArray)await DataAttributesEngine.GetAttribute("application", entityId, "application");
            string firstName = string.Empty;
            if (attributes != null )
            {
                firstName = GetOnlyFirstName(attributes);
            }

            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            var emailVerificationRequest = new EmailVerificationRequest
            {
                Data = new { },
                Email = emailAddress,
                Name = textInfo.ToTitleCase(firstName.ToString() ?? string.Empty),
                Source = "borrower"
            };
            await EmailVerificationService.InitiateEmailVerification(entityType, entityId, emailVerificationRequest);
        }
        private static string GetFirstName(dynamic data)
        {
            Func<dynamic> resultProperty = () => data[0].firstName;

            return HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
        }
        private static string GetOnlyFirstName(dynamic data)
        {
            Func<dynamic> resultProperty = () => data.firstName;

            return HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
        }
        private static string GetFLastName(dynamic data)
        {
            Func<dynamic> resultProperty = () => data[0].lastName;

            return HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
        }
        private static bool HasProperty<T>(Func<T> property)
        {
            try
            {
                property();
                return true;
            }
            catch (RuntimeBinderException)
            {
                return false;
            }
        }

        private static T GetValue<T>(Func<T> property)
        {
            return property();
        }

#endregion
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}
