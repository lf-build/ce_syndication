﻿namespace CreditExchange.Syndication.ViewDNS
{
    /// <summary>
    /// Company Name Generator class
    /// </summary>
    /// <seealso cref="CreditExchange.Syndication.ViewDNS.ICompanyNameGenerator" />
    public class CompanyNameGenerator : ICompanyNameGenerator
    {
        /// <summary>
        /// The replacement words
        /// </summary>
        private string[] replacementWords = new string[] { "Private", "PRIVATE", "Pvt", "PVT", "Limited", "LIMITED", "Ltd", "LTD", "Inc", "INC", ".", ",", "Org", "ORG", "Organization", "ORGANIZATION", "llp", "Llp", "LLP", "&" };

        /// <summary>
        /// Gets the name of the only company.
        /// </summary>
        /// <param name="companyName">Name of the company.</param>
        /// <returns></returns>
        public string RemoveUnWantedCharactersFromCompanyName(string companyName)
        {
            var onlyCompanyName = companyName;
            foreach (var word in replacementWords)
            {
                onlyCompanyName = onlyCompanyName.Replace(word, "");
            }

            return onlyCompanyName.Trim();
        }
    }
}
