﻿using System;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace CreditExchange.Syndication.ViewDNS
{
    /// <summary>
    /// Filter domain class
    /// </summary>
    /// <seealso cref="CreditExchange.Syndication.ViewDNS.IFilterDomain" />
    public class FilterDomain : IFilterDomain
    {
        /// <summary>
        /// Gets the domain from email address.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <returns></returns>
        public string GetDomainFromEmailAddress(string emailAddress)
        {
            return string.IsNullOrWhiteSpace(emailAddress) ? string.Empty : emailAddress.Split('@')[1];
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <param name="emailaddress">The emailaddress.</param>
        /// <returns>
        ///   <c>true</c> if the specified emailaddress is valid; otherwise, <c>false</c>.
        /// </returns>
        public bool IsValidEmail(string emailaddress)
        {
            return Regex.IsMatch(emailaddress, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        }
    }
}
