﻿using CreditExchange.Syndication.ViewDNS.Response;

namespace CreditExchange.Syndication.ViewDNS
{
    /// <summary>
    /// Domain Matching interface
    /// </summary>
    public interface IDomainMatching
    {
        /// <summary>
        /// Verifies the domain matched.
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <param name="reverseWhoisResponse">The reverse whois response.</param>
        /// <returns></returns>
        bool VerifyDomainMatched(string domain, IGetReverseWhoisDetails reverseWhoisResponse);
    }
}
