﻿using System.Threading.Tasks;

namespace CreditExchange.Syndication.ViewDNS
{
    public abstract class VerifyDomain
    {
        protected VerifyDomain NextStep;

        public void SetNextStep(VerifyDomain nextStep)
        {
            NextStep = nextStep;
        }

        public abstract Task ExecuteStep();
    }
}
