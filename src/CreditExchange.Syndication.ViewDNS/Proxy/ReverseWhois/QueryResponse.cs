﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.ViewDNS.Proxy.ReverseWhois
{
    /// <summary>
    /// Root object class
    /// </summary>
    public class QueryResponse
    {
        /// <summary>
        /// Gets or sets the query.
        /// </summary>
        /// <value>
        /// The query.
        /// </value>
        [JsonProperty("query")]
        public Query Query { get; set; }
        /// <summary>
        /// Gets or sets the response.
        /// </summary>
        /// <value>
        /// The response.
        /// </value>
        [JsonProperty("response")]
        public Response Response { get; set; }
    }
}
