﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.ViewDNS.Proxy.ReverseWhois
{
    /// <summary>
    /// Query class
    /// </summary>
    public class Query
    {
        /// <summary>
        /// Gets or sets the tool.
        /// </summary>
        /// <value>
        /// The tool.
        /// </value>
        [JsonProperty("tool")]
        public string Tool { get; set; }
        /// <summary>
        /// Gets or sets the nameserver.
        /// </summary>
        /// <value>
        /// The nameserver.
        /// </value>
        [JsonProperty("nameserver")]
        public string NameServer { get; set; }
    }
}
