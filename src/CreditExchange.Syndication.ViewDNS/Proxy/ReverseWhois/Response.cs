﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.ViewDNS.Proxy.ReverseWhois
{
    /// <summary>
    /// Response class
    /// </summary>
    public class Response
    {
        /// <summary>
        /// Gets or sets the result count.
        /// </summary>
        /// <value>
        /// The result count.
        /// </value>
        [JsonProperty("result_count")]
        public int ResultCount { get; set; }
        /// <summary>
        /// Gets or sets the total pages.
        /// </summary>
        /// <value>
        /// The total pages.
        /// </value>
        [JsonProperty("total_pages")]
        public string TotalPages { get; set; }
        /// <summary>
        /// Gets or sets the current page.
        /// </summary>
        /// <value>
        /// The current page.
        /// </value>
        [JsonProperty("current_page")]
        public string CurrentPage { get; set; }
        /// <summary>
        /// Gets or sets the matches.
        /// </summary>
        /// <value>
        /// The matches.
        /// </value>
        [JsonProperty("matches")]
        public Match[] Matches { get; set; }
    }
}
