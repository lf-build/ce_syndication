﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.ViewDNS.Proxy.ReverseWhois
{
    /// <summary>
    /// Match class
    /// </summary>
    public class Match
    {
        /// <summary>
        /// Gets or sets the domain.
        /// </summary>
        /// <value>
        /// The domain.
        /// </value>
        [JsonProperty("domain")]
        public string Domain { get; set; }
        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        [JsonProperty("created_date")]
        public string CreatedDate { get; set; }
        /// <summary>
        /// Gets or sets the registrar.
        /// </summary>
        /// <value>
        /// The registrar.
        /// </value>
        [JsonProperty("registrar")]
        public string Registrar { get; set; }
    }
}
