﻿using CreditExchange.Syndication.ViewDNS.Proxy.ReverseWhois;
using LendFoundry.Foundation.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.ViewDNS.Proxy
{
    /// <summary>
    /// View Dns proxy class
    /// </summary>
    public class ViewDnsProxy : IViewDnsProxy
    {
        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        private IViewDnsConfiguration Configuration { get; }
        private ILogger Logger { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewDnsProxy"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <exception cref="System.ArgumentNullException">
        /// configuration
        /// or
        /// ApiBaseUrl
        /// or
        /// ApiKey
        /// or
        /// ApiOutputType
        /// </exception>
        public ViewDnsProxy(IViewDnsConfiguration configuration, ILogger logger)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (string.IsNullOrWhiteSpace(configuration.ApiBaseUrl))
                throw new ArgumentNullException(nameof(configuration.ApiBaseUrl));

            if (string.IsNullOrWhiteSpace(configuration.ApiKey))
                throw new ArgumentNullException(nameof(configuration.ApiKey));

            if (string.IsNullOrWhiteSpace(configuration.ApiOutputType))
                throw new ArgumentNullException(nameof(configuration.ApiOutputType));

            if (string.IsNullOrWhiteSpace(configuration.ReverseWhoisUrl))
                throw new ArgumentNullException(nameof(configuration.ReverseWhoisUrl));

            Configuration = configuration;
            Logger = logger;
        }

        /// <summary>
        /// Gets the name of the domain details by company.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">Value cannot be null or whitespace. - name</exception>
        public async Task<QueryResponse> GetDomainDetails(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name),"Value cannot be null or whitespace." );

            var client = new RestClient(Configuration.ApiBaseUrl + "/" + Configuration.ReverseWhoisUrl);
            var request = new RestRequest(Method.GET);
            request.AddParameter("q", name,ParameterType.QueryString);
            request.AddParameter("apikey", Configuration.ApiKey, ParameterType.QueryString);
            request.AddParameter("output", Configuration.ApiOutputType, ParameterType.QueryString);
            request.AddHeader("Accept", "application/json");
            var objectResponse = await ExecuteRequestAsync<QueryResponse>(client, request);
            Logger.Info($"[ViewDNS] GetDomainDetails method response: ", objectResponse);
            return objectResponse;
        }

        /// <summary>
        /// Executes the request asynchronous.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="client">The client.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">response</exception>
        /// <exception cref="CreditExchange.Syndication.ViewDNS.ViewDnsException">
        /// Service call failed
        /// or
        /// or
        /// </exception>
        private async Task<T> ExecuteRequestAsync<T>(IRestClient client, IRestRequest request) where T : class
        {
            var response = await client.ExecuteTaskAsync(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new ViewDnsException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new ViewDnsException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");
            
            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
                    throw new ViewDnsException(
                        $"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");

                if (response.StatusCode.ToString().ToLower() == "notfound")
                    throw new ViewDnsException($"Service call failed. Status {response.ResponseStatus}. Status Code: {response.StatusCode}. Response: {response.StatusDescription ?? ""}");
            }
            return JsonConvert.DeserializeObject<T>(response.Content);
        }
    }
}
