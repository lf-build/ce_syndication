﻿using CreditExchange.Syndication.ViewDNS.Proxy.ReverseWhois;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.ViewDNS.Proxy
{
    /// <summary>
    /// View Dns proxy interface
    /// </summary>
    public interface IViewDnsProxy
    {
        /// <summary>
        /// Gets the name of the domain details by company.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Value cannot be null or whitespace. - companyName</exception>
        Task<QueryResponse> GetDomainDetails(string name);
    }
}
