﻿using System.Threading.Tasks;

namespace CreditExchange.Syndication.ViewDNS
{
    /// <summary>
    /// View dns interface
    /// </summary>
    public interface IViewDnsService
    {
        /// <summary>
        /// Gets the employee verification.
        /// </summary>
        /// <param name="entitytype">The entitytype.</param>
        /// <param name="entityid">The entityid.</param>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="companyName">Name of the company.</param>
        /// <param name="cin">The cin.</param>
        /// <returns></returns>
        Task<Response.IEmployeeVerification> VerifyEmployment(string entityType, string entityId, string emailAddress, string companyName, string cin);
    }
}
