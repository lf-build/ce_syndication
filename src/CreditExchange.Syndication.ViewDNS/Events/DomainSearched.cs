﻿using LendFoundry.SyndicationStore.Events;

namespace CreditExchange.Syndication.ViewDNS.Events
{
    /// <summary>
    /// Domain searched event class
    /// </summary>
    /// <seealso cref="EventSource" />
    public class DomainSearched : SyndicationCalledEvent
    {
    }
}
