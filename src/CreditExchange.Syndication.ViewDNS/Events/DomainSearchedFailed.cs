﻿using LendFoundry.SyndicationStore.Events;

namespace CreditExchange.Syndication.ViewDNS.Events
{
    public class DomainSearchedFailed : SyndicationCalledEvent
    {
    }
}
