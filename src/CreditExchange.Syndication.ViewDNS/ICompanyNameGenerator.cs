﻿namespace CreditExchange.Syndication.ViewDNS
{
    /// <summary>
    /// Company name generator interface
    /// </summary>
    public interface ICompanyNameGenerator
    {
        /// <summary>
        /// Removes the name of the un wanted characters from company.
        /// </summary>
        /// <param name="companyName">Name of the company.</param>
        /// <returns></returns>
        string RemoveUnWantedCharactersFromCompanyName(string companyName);
    }
}
