﻿namespace CreditExchange.Syndication.ViewDNS.Request
{
    /// <summary>
    /// Employee verify request interface
    /// </summary>
    public interface IEmployeeVerification
    {
        /// <summary>
        /// Gets or sets the email address.
        /// </summary>
        /// <value>
        /// The email address.
        /// </value>
        string EmailAddress { get; set; }
        /// <summary>
        /// Gets or sets the name of the company.
        /// </summary>
        /// <value>
        /// The name of the company.
        /// </value>
        string CompanyName { get; set; }
        /// <summary>
        /// Gets or sets the cin.
        /// </summary>
        /// <value>
        /// The cin.
        /// </value>
        string Cin { get; set; }
    }
}
