﻿namespace CreditExchange.Syndication.ViewDNS.Request
{
    /// <summary>
    /// Employee verify request class
    /// </summary>
    /// <seealso cref="IEmployeeVerification" />
    public class EmployeeVerification : IEmployeeVerification
    {
        /// <summary>
        /// Gets or sets the cin.
        /// </summary>
        /// <value>
        /// The cin.
        /// </value>
        public string Cin { get;set; }

        /// <summary>
        /// Gets or sets the name of the company.
        /// </summary>
        /// <value>
        /// The name of the company.
        /// </value>
        public string CompanyName{ get;set; }

        /// <summary>
        /// Gets or sets the email address.
        /// </summary>
        /// <value>
        /// The email address.
        /// </value>
        public string EmailAddress{ get;set; }
    }
}
