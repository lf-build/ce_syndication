﻿namespace CreditExchange.Syndication.ViewDNS
{
    /// <summary>
    /// View dns configuration class
    /// </summary>
    public class ViewDnsConfiguration : IViewDnsConfiguration
    {
        /// <summary>
        /// Gets or sets the API key.
        /// </summary>
        /// <value>
        /// The API key.
        /// </value>
        public string ApiKey { get; set; }
        /// <summary>
        /// Gets or sets the API base URL.
        /// </summary>
        /// <value>
        /// The API base URL.
        /// </value>
        public string ApiBaseUrl { get; set; }
        /// <summary>
        /// Gets or sets the type of the API output.
        /// </summary>
        /// <value>
        /// The type of the API output.
        /// </value>
        public string ApiOutputType { get; set; }
        /// <summary>
        /// Gets or sets the reverse whois URL.
        /// </summary>
        /// <value>
        /// The reverse whois URL.
        /// </value>
        public string ReverseWhoisUrl { get; set; }
      
        public string EmailValidationProvider { get; set; }
    }
}
