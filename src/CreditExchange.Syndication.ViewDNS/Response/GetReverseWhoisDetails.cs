﻿using LendFoundry.Foundation.Client;
using CreditExchange.Syndication.ViewDNS.Proxy.ReverseWhois;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace CreditExchange.Syndication.ViewDNS.Response
{
    /// <summary>
    /// Get reverse whois details class
    /// </summary>
    /// <seealso cref="CreditExchange.Syndication.ViewDNS.Response.IGetReverseWhoisDetails" />
    public class GetReverseWhoisDetails : IGetReverseWhoisDetails
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetReverseWhoisDetails"/> class.
        /// </summary>
        /// <param name="reverseWhoisResponse">The reverse whois response.</param>
        public GetReverseWhoisDetails(QueryResponse reverseWhoisResponse)
        {
            if (reverseWhoisResponse != null && reverseWhoisResponse.Response != null)
            {
                ResultCount = reverseWhoisResponse.Response.ResultCount;
                if(reverseWhoisResponse.Response.Matches != null)
                {
                    ReverseWhoisMatch = reverseWhoisResponse.Response.Matches.Select(p=>new GetReverseWhoisMatch(p)).ToList<IGetReverseWhoisMatch>();
                }
            }
        }

        /// <summary>
        /// Gets or sets the reverse whois match.
        /// </summary>
        /// <value>
        /// The reverse whois match.
        /// </value>
        [JsonConverter(typeof(InterfaceListConverter<IGetReverseWhoisMatch, GetReverseWhoisMatch>))]
        public List<IGetReverseWhoisMatch> ReverseWhoisMatch { get; set; }
        /// <summary>
        /// Gets or sets the result count.
        /// </summary>
        /// <value>
        /// The result count.
        /// </value>
        public int ResultCount { get; set; }
        /// <summary>
        /// Gets or sets the reference number.
        /// </summary>
        /// <value>
        /// The reference number.
        /// </value>
        public string ReferenceNumber { get; set; }
    }
}
