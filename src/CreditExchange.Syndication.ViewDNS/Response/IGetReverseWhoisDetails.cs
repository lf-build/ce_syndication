﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.ViewDNS.Response
{
    /// <summary>
    /// Get reverse whois details interface
    /// </summary>
    public interface IGetReverseWhoisDetails
    {
        /// <summary>
        /// Gets or sets the reverse whois match.
        /// </summary>
        /// <value>
        /// The reverse whois match.
        /// </value>
        List<IGetReverseWhoisMatch> ReverseWhoisMatch { get; set; }
        /// <summary>
        /// Gets or sets the result count.
        /// </summary>
        /// <value>
        /// The result count.
        /// </value>
        int ResultCount { get; set; }
        /// <summary>
        /// Gets or sets the reference number.
        /// </summary>
        /// <value>
        /// The reference number.
        /// </value>
        string ReferenceNumber { get; set; }
    }
}
