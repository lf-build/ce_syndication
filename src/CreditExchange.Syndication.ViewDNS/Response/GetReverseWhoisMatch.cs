﻿using CreditExchange.Syndication.ViewDNS.Proxy;
using CreditExchange.Syndication.ViewDNS.Proxy.ReverseWhois;

namespace CreditExchange.Syndication.ViewDNS.Response
{
    /// <summary>
    /// Get reverse whois matches class
    /// </summary>
    /// <seealso cref="IGetReverseWhoisMatch" />
    public class GetReverseWhoisMatch : IGetReverseWhoisMatch
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetReverseWhoisMatch"/> class.
        /// </summary>
        /// <param name="reverseWhoisMatch">The reverse whois match.</param>
        public GetReverseWhoisMatch(Match reverseWhoisMatch)
        {
            if(reverseWhoisMatch != null)
            {
                Domain = reverseWhoisMatch.Domain;
                CreatedDate = reverseWhoisMatch.CreatedDate;
                Registrar = reverseWhoisMatch.Registrar;
            }
        }

        /// <summary>
        /// Gets or sets the domain.
        /// </summary>
        /// <value>
        /// The domain.
        /// </value>
        public string Domain { get; set; }
        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        public string CreatedDate { get; set; }
        /// <summary>
        /// Gets or sets the registrar.
        /// </summary>
        /// <value>
        /// The registrar.
        /// </value>
        public string Registrar { get; set; }
    }
}