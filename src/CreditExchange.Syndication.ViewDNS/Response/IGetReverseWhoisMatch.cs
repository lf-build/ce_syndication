﻿namespace CreditExchange.Syndication.ViewDNS.Response
{
    /// <summary>
    /// Get reverse whois matches interface 
    /// </summary>
    public interface IGetReverseWhoisMatch
    {
        /// <summary>
        /// Gets or sets the domain.
        /// </summary>
        /// <value>
        /// The domain.
        /// </value>
        string Domain { get; set; }
        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        string CreatedDate { get; set; }
        /// <summary>
        /// Gets or sets the registrar.
        /// </summary>
        /// <value>
        /// The registrar.
        /// </value>
        string Registrar { get; set; }
    }
}