﻿namespace CreditExchange.Syndication.ViewDNS.Response
{
    /// <summary>
    /// Employee verify response class
    /// </summary>
    /// <seealso cref="IEmployeeVerification" />
    public class EmployeeVerification : IEmployeeVerification
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeVerification" /> class.
        /// </summary>
        /// <param name="domainMatched">if set to <c>true</c> [domain matched].</param>
        /// <param name="verificationMatched">The verification matched.</param>
        public EmployeeVerification(bool domainMatched, string verificationMatched)
        {
            DomainMatched = domainMatched;
            VerificationMethod = verificationMatched;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeVerification"/> class.
        /// </summary>
        public EmployeeVerification()
        {

        }

        /// <summary>
        /// Gets or sets the reference number.
        /// </summary>
        /// <value>
        /// The reference number.
        /// </value>
        public string ReferenceNumber { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [domain matched].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [domain matched]; otherwise, <c>false</c>.
        /// </value>
        public bool DomainMatched { get; set; }
        /// <summary>
        /// Gets or sets the verification method.
        /// </summary>
        /// <value>
        /// The verification method.
        /// </value>
        public string VerificationMethod { get; set; }
    }
}
