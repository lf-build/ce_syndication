﻿namespace CreditExchange.Syndication.ViewDNS.Response
{
    /// <summary>
    /// Employee verify response interface
    /// </summary>
    public interface IEmployeeVerification
    {
        /// <summary>
        /// Gets or sets the reference number.
        /// </summary>
        /// <value>
        /// The reference number.
        /// </value>
        string ReferenceNumber { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [domain matched].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [domain matched]; otherwise, <c>false</c>.
        /// </value>
        bool DomainMatched { get; set; }
    }
}
