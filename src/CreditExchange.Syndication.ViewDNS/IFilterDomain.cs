﻿namespace CreditExchange.Syndication.ViewDNS
{
    /// <summary>
    /// Filter domain interface
    /// </summary>
    public interface IFilterDomain
    {
        /// <summary>
        /// Gets the domain from email address.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <returns></returns>
        string GetDomainFromEmailAddress(string emailAddress);
        /// <summary>
        /// Determines whether [is valid email] [the specified emailaddress].
        /// </summary>
        /// <param name="emailaddress">The emailaddress.</param>
        /// <returns>
        ///   <c>true</c> if [is valid email] [the specified emailaddress]; otherwise, <c>false</c>.
        /// </returns>
        bool IsValidEmail(string emailaddress);
    }
}