using System;

namespace CreditExchange.Syndication.ViewDNS
{
    public interface IRecoverabilityPolicy
    {
        bool CanBeRecoveredFrom(Exception exception);
    }
}