﻿using System;
using System.Collections.Generic;

namespace CreditExchange.Syndication.ViewDNS
{
    public class CaseInsensitiveDictionary<TValue> : Dictionary<string, TValue>
    {
        public CaseInsensitiveDictionary() : base(StringComparer.OrdinalIgnoreCase)
        {
        }

        public CaseInsensitiveDictionary(CaseInsensitiveDictionary<TValue> values) : base(values, StringComparer.OrdinalIgnoreCase)
        {
        }

        public CaseInsensitiveDictionary(Dictionary<string, TValue> values) : base(values, StringComparer.OrdinalIgnoreCase)
        {
        }
    }
}
