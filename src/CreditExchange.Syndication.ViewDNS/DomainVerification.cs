﻿using CreditExchange.CompanyDb.Abstractions;
//using CreditExchange.Syndication.TowerData;
using CreditExchange.Syndication.ViewDNS.Proxy;
using CreditExchange.Syndication.ViewDNS.Response;
using LendFoundry.Clients.DecisionEngine;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.ViewDNS
{
    public sealed class EmailHunterDomainVerification : VerifyDomain
    {
        private readonly string _domain;
        private readonly CompanyWithDirectors _company;
        private EmployeeVerification _employeeVerification;
        private readonly string _companyName;
        private readonly IViewDnsProxy _viewDnsProxy;
        //private readonly ITowerDataService _towerDataService;
        //private readonly IEmailHunterService _emailHunterService;
        private readonly IDecisionEngineService _decisionEngineService;
        private IViewDnsConfiguration _viewDnsConfiguration { get; }
        private readonly string _emailAddress;

        //public EmailHunterDomainVerification(EmployeeVerification employeeVerification, IViewDnsConfiguration viewDnsConfiguration, IEmailHunterService emailHunterService, IDecisionEngineService decisionEngineService, ITowerDataService towerDataService, CompanyWithDirectors company, string emailAddress, string domain, string companyName, IViewDnsProxy viewDnsProxy)
        //{
        //    _employeeVerification = employeeVerification;
        //    //_towerDataService = towerDataService;
        //    //_emailHunterService = emailHunterService;
        //    _decisionEngineService = decisionEngineService;
        //    _company = company;
        //    _emailAddress = emailAddress;
        //    _domain = domain;
        //    _companyName = companyName;
        //    _viewDnsProxy = viewDnsProxy;
        //    _viewDnsConfiguration = viewDnsConfiguration;
        //}
        public EmailHunterDomainVerification(EmployeeVerification employeeVerification, IViewDnsConfiguration viewDnsConfiguration, IDecisionEngineService decisionEngineService, CompanyWithDirectors company, string emailAddress, string domain, string companyName, IViewDnsProxy viewDnsProxy)
        {
            _employeeVerification = employeeVerification;
            //_towerDataService = towerDataService;
            //_emailHunterService = emailHunterService;
            _decisionEngineService = decisionEngineService;
            _company = company;
            _emailAddress = emailAddress;
            _domain = domain;
            _companyName = companyName;
            _viewDnsProxy = viewDnsProxy;
            _viewDnsConfiguration = viewDnsConfiguration;
        }
        public override async Task ExecuteStep()
        {

            //if (!string.IsNullOrWhiteSpace(_viewDnsConfiguration.EmailValidationProvider) && _viewDnsConfiguration.EmailValidationProvider.ToLower() == "emailhunter")
            //{
            //    if (_emailHunterService == null)
            //        return;

            //    IEmailVerificationResult result = null;
            //    FaultRetry.RunWithAlwaysRetry(() =>
            //    {
            //        result = _emailHunterService.EmailVerifier("application", _emailAddress).Result;
            //    });

            //    if (result == null)
            //        return;

            //    if (result.Regexp && !result.Gibberish && !result.Disposable && !result.Webmail && result.MxRecords && result.SmtpServer && result.SmtpCheck)
            //    {
            //        SetNextStep(new CompanyDomainVerification(_employeeVerification, _company, _domain, _companyName, _viewDnsProxy));
            //    }
            //}
            //else if (!string.IsNullOrWhiteSpace(_viewDnsConfiguration.EmailValidationProvider) && _viewDnsConfiguration.EmailValidationProvider.ToLower() == "towerdata")
            //{
            //    if (_towerDataService == null)
            //        return;

            //    IEmailVerificationResponse result = null;
            //    var emailResult = string.Empty;
            //    result = await _towerDataService.VerifyEmail("application", _emailAddress);

            //    if (result == null)
            //        return;

            //    if (result != null)
            //    {
            //        object executionResult = _decisionEngineService.Execute<dynamic, dynamic>(
            //               "verification_rule_towerdataemail",
            //               new { payload = result });

            //        if (executionResult != null)
            //        {
            //            emailResult = GetResultValue(executionResult);
            //        }
            //        if (emailResult != null)
            //        {
            //            if (emailResult.ToLower() == "true")
            //            {
            //                SetNextStep(new CompanyDomainVerification(_employeeVerification, _company, _domain, _companyName, _viewDnsProxy));
            //            }
            //            else
            //                return;
            //        }
            //    }
            //}
            //else
            //{
                SetNextStep(new CompanyDomainVerification(_employeeVerification, _company, _domain, _companyName, _viewDnsProxy));
            //}

        }

        private static string GetResultValue(dynamic data)
        {
            Func<dynamic> resultProperty = () => data;

            return HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
        }
        private static bool HasProperty<T>(Func<T> property)
        {
            try
            {
                property();
                return true;
            }
            catch (RuntimeBinderException)
            {
                return false;
            }
        }

        private static T GetValue<T>(Func<T> property)
        {
            return property();
        }


    }

    public sealed class CompanyDomainVerification : VerifyDomain
    {
        private readonly string _domain;
        private readonly CompanyWithDirectors _company;
        private EmployeeVerification _employeeVerification;
        private readonly string _companyName;
        private readonly IViewDnsProxy _viewDnsProxy;

        public CompanyDomainVerification(EmployeeVerification employeeVerification, CompanyWithDirectors company, string domain, string companyName, IViewDnsProxy viewDnsProxy)
        {
            _employeeVerification = employeeVerification;
            _company = company;
            _domain = domain;
            _companyName = companyName;
            _viewDnsProxy = viewDnsProxy;
            ExecuteStep().Wait();
        }

        public override async Task ExecuteStep()
        {
            if (_company == null || _company.Company == null)
                return;

            if (string.Equals(_domain, _company.Company.Domain, StringComparison.InvariantCultureIgnoreCase))
            {
                _employeeVerification.DomainMatched = true;
                _employeeVerification.VerificationMethod = "Company Database";
                _employeeVerification.ReferenceNumber = Guid.NewGuid().ToString("N");
                return;
            }
            SetNextStep(new ExternalCompanyDomainVerification(_employeeVerification, new CompanyNameGenerator(), _viewDnsProxy, new DomainMatching(), _domain, _companyName, _company));
        }
    }

    public sealed class ExternalCompanyDomainVerification : VerifyDomain
    {
        private readonly ICompanyNameGenerator _companyNameGenerator;
        private readonly IViewDnsProxy _viewDnsProxy;
        private readonly string _companyName;
        private readonly IDomainMatching _domainMatching;
        private readonly string _domain;
        private EmployeeVerification _employeeVerification;
        private readonly CompanyWithDirectors _company;

        public ExternalCompanyDomainVerification(EmployeeVerification employeeVerification, ICompanyNameGenerator companyNameGenerator, IViewDnsProxy viewDnsProxy, IDomainMatching domainMatching, string domain, string companyName, CompanyWithDirectors company)
        {
            _employeeVerification = employeeVerification;
            _companyNameGenerator = companyNameGenerator;
            _viewDnsProxy = viewDnsProxy;
            _domainMatching = domainMatching;
            _domain = domain;
            _companyName = companyName;
            _company = company;
            ExecuteStep().Wait();
        }

        public override async Task ExecuteStep()
        {
            var trimmedCompanyName = _companyNameGenerator.RemoveUnWantedCharactersFromCompanyName(_companyName);
            var reversewhoisproxyResponse = await _viewDnsProxy.GetDomainDetails(trimmedCompanyName);
            var response = new GetReverseWhoisDetails(reversewhoisproxyResponse);
            var match = _domainMatching.VerifyDomainMatched(_domain, response);
            if (match)
            {
                _employeeVerification.DomainMatched = true;
                _employeeVerification.VerificationMethod = "Company Name";
                _employeeVerification.ReferenceNumber = Guid.NewGuid().ToString("N");
                return;
            }
            SetNextStep(new ExternalDirectorDomainVerification(_employeeVerification, _viewDnsProxy, _domainMatching, _domain, _companyName, _company));
        }
    }

    public sealed class ExternalDirectorDomainVerification : VerifyDomain
    {
        private EmployeeVerification _employeeVerification;
        private readonly IViewDnsProxy _viewDnsProxy;
        private readonly string _companyName;
        private readonly IDomainMatching _domainMatching;
        private readonly string _domain;
        private readonly CompanyWithDirectors _company;

        public ExternalDirectorDomainVerification(EmployeeVerification employeeVerification, IViewDnsProxy viewDnsProxy, IDomainMatching domainMatching, string domain, string companyName, CompanyWithDirectors company)
        {
            _employeeVerification = employeeVerification;
            _viewDnsProxy = viewDnsProxy;
            _domainMatching = domainMatching;
            _domain = domain;
            _companyName = companyName;
            _company = company;
            ExecuteStep().Wait();
        }

        public override async Task ExecuteStep()
        {
            if (_company == null || _company.Directors == null || _company.Directors.Any() == false)
                return;

            foreach (var companyDirector in _company.Directors)
            {
                var reversewhoisproxyResponse = await _viewDnsProxy.GetDomainDetails(companyDirector.Name);
                var response = new GetReverseWhoisDetails(reversewhoisproxyResponse);
                var matched = _domainMatching.VerifyDomainMatched(_domain, response);
                if (matched)
                {
                    _employeeVerification.DomainMatched = true;
                    _employeeVerification.VerificationMethod = "Company Director";
                    _employeeVerification.ReferenceNumber = Guid.NewGuid().ToString("N");
                    return;
                }
            }
        }



    }
}
