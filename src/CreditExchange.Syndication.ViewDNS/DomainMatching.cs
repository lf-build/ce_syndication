﻿using CreditExchange.Syndication.ViewDNS.Response;
using System;
using System.Linq;

namespace CreditExchange.Syndication.ViewDNS
{
    /// <summary>
    /// Domain Matching class
    /// </summary>
    /// <seealso cref="IDomainMatching" />
    public class DomainMatching : IDomainMatching
    {
        /// <summary>
        /// Verifies the domain matched.
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <param name="reverseWhoisResponse">The reverse whois response.</param>
        /// <returns></returns>
        public bool VerifyDomainMatched(string domain, IGetReverseWhoisDetails reverseWhoisResponse)
        {
            var isDomainMatched = false;
            if (reverseWhoisResponse != null && reverseWhoisResponse.ResultCount > 0)
            {
                isDomainMatched = reverseWhoisResponse.ReverseWhoisMatch.Any(c => string.Equals(c.Domain, domain, StringComparison.InvariantCultureIgnoreCase));
            }
            return isDomainMatched;
        }
    }
}
