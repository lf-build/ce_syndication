﻿using CreditExchange.YodleeYsl.Abstractions;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Threading.Tasks;

namespace CreditExchange.YodleeYsl.Persistence
{
    public class YodleeRepository : MongoRepository<IYodleeData, YodleeData>, IYodleeRepository
    {
        static YodleeRepository()
        {
            BsonClassMap.RegisterClassMap<YodleeData>(map =>
            {
                map.AutoMap();
                map.MapProperty(a => a.CobSession).SetIgnoreIfDefault(true);
                var type = typeof(YodleeData);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<CobrandSession>(map =>
            {
                map.AutoMap();
                var type = typeof(CobrandSession);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<UserData>(map =>
            {
                map.AutoMap();
                var type = typeof(UserData);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });
        }

        public YodleeRepository(LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "yodleedata")
        {
            CreateIndexIfNotExists("yodleedata",
                Builders<IYodleeData>.IndexKeys.Ascending(i => i.EntityType).Ascending(i => i.EntityId));
        }


        public async Task<IYodleeData> GetByEntityId(string entityType, string entityId)
        {
            return await Query.FirstOrDefaultAsync(a => a.EntityType == entityType && a.EntityId == entityId);
        }
    }
}
