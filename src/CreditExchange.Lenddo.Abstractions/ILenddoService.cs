﻿using CreditExchange.Syndication.Lenddo;
using System.Threading.Tasks;

namespace CreditExchange.Lenddo.Abstractions
{
    public  interface ILenddoService
    {
        Task<IGetApplicationVerificationResponse> GetClientVerification(string entityType, string entityId, string clientId);
        Task<IGetApplicationScoreResponse> GetClientScore(string entityType, string entityId, string clientId);
    }
}
