﻿using CreditExchange.Syndication.Lenddo;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace CreditExchange.Lenddo.Abstractions
{
    public interface ILenddoRepository :IRepository<ILenddoData>
    {
        Task<ILenddoData> GetByEntityId(string entityType, string entityId);
    }
}
