﻿using LendFoundry.Security.Tokens;
using CreditExchange.Syndication.Crif.PanVerification;

namespace CreditExchange.Crif.PanVerification.Client
{
    public interface ICrifPanVerificationServiceFactory
    {
        ICrifPanVerificationService Create(ITokenReader reader);
    }
}