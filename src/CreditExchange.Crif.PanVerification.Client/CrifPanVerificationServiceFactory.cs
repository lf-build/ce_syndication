﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using CreditExchange.Syndication.Crif.PanVerification;

namespace CreditExchange.Crif.PanVerification.Client
{
    public class CrifPanVerificationServiceFactory : ICrifPanVerificationServiceFactory
    {
        public CrifPanVerificationServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public ICrifPanVerificationService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new CrifPanVerificationService(client);
        }

    }
}
