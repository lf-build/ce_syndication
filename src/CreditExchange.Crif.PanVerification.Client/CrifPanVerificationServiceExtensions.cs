﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace CreditExchange.Crif.PanVerification.Client
{
    public static class CrifPanVerificationServiceExtensions
    {
        public static IServiceCollection AddCrifPanVerificationServiceExtensions(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<ICrifPanVerificationServiceFactory>(p => new CrifPanVerificationServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ICrifPanVerificationServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
