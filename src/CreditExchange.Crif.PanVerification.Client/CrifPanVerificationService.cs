﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using CreditExchange.Syndication.Crif.PanVerification;
using CreditExchange.Syndication.Crif.PanVerification.Request;
using CreditExchange.Syndication.Crif.PanVerification.Response;
using RestSharp;


namespace CreditExchange.Crif.PanVerification.Client
{
    public class CrifPanVerificationService : ICrifPanVerificationService
    {
        public CrifPanVerificationService(IServiceClient client)
        {
            Client = client;
        }
        private IServiceClient Client { get; }

        public async Task<IPanVerificationResponse> VerifyPan(string entityType, string entityId,IPanVerificationRequest panVerificationRequest)
        {
            var request = new RestRequest("/{entityType}/{entityId}", Method.POST);
            request.AddJsonBody(panVerificationRequest);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            return await Client.ExecuteAsync<GetPanVerificationResponse>(request);
        }


    }
}
