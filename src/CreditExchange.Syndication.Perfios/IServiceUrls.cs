﻿namespace CreditExchange.Syndication.Perfios
{
    public interface IServiceUrls
    {
        string CompleteTransactionUrl { get; set; }
        string DeletedataUrl { get; set; }
        string ListofsupportedInstitutionsUrl { get; set; }
        string RequestReviewofAPerfiosTransactionUrl { get; set; }
        string RetrieveReportsandStatementsUrl { get; set; }
        string StartTransactionUrl { get; set; }
        string TransactionStatusUrl { get; set; }
        string UploadStatementUrl { get; set; }
        string StartNetLinkingProcessUrl { get; set; }
    }
}