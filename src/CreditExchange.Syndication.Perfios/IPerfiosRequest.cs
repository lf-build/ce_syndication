﻿namespace CreditExchange.Syndication.Perfios
{
    public interface IPerfiosInitializationRequest
   {
         string InstitutionId { get; set; }
         string YearMonthFrom { get; set; }
         string YearMonthTo { get; set; }
         byte[] fileBytes { get; set; }
         string password { get; set; }
    }
}
