﻿using LendFoundry.Foundation.Persistence;
using System;

namespace CreditExchange.Syndication.Perfios
{
    public class PerfiosData : Aggregate, IPerfiosData
    {

        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string PerfiosTransactionId { get; set; }
        public string Referencenumber { get; set; }
        public string TransactionId { get; set; }
        public byte[] File { get; set; }
        public bool IsForm26ASInfoAvailable { get; set; }
        public string Password { get; set; }
        
        public string XMLResponse { get; set; }
        public string XMLRequest { get; set; }
        public DateTimeOffset ProcessDate { get; set; }
        public string Status { get; set; }
    }
}
