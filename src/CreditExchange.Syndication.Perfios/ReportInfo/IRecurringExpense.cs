﻿using System.Collections.Generic;
using CreditExchange.Syndication.Perfios.Response;

namespace CreditExchange.Syndication.Perfios.ReportInfo
{
    public interface IRecurringExpense
    {
        string Category { get; set; }
        List<IMonthwiseBreakup> MonthwiseBreakups { get; set; }
        List<ITranasactionInfo> Tranasactions { get; set; }
    }
}