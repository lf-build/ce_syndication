﻿using CreditExchange.Syndication.Perfios.Response;
using System.Collections.Generic;

namespace CreditExchange.Syndication.Perfios.ReportInfo
{
    public  interface IForm26ASInfo
    {
         string Name { get; set; }
         string Address { get; set; }
         string Dob { get; set; }
         string Pan { get; set; }
         string Panstatus { get; set; }

        List<ITDSDetail> TDSDetail { get; set; }
    }
}
