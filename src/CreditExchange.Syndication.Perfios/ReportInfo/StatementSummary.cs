﻿namespace CreditExchange.Syndication.Perfios.ReportInfo
{
    public class StatementSummary :IStatementSummary
    {
        public StatementSummary()
        {

        }
        public StatementSummary(Proxy.Response.SummaryInfo summaryInfo)
        {
            if (summaryInfo != null)
            {
                AvgBalanceOf6Dates = summaryInfo.avgBalanceOf6Dates.ToString();
                AvgDailyBalance = summaryInfo.avgDailyBalance.ToString();
                AvgInvestmentExpensePerMonth = summaryInfo.avgInvestmentExpensePerMonth.ToString();
                AvgInvestmentIncomePerMonth = summaryInfo.avgInvestmentIncomePerMonth.ToString();
                AvgMonthlyExpense = summaryInfo.avgMonthlyExpense.ToString();
                AvgMonthlyIncome = summaryInfo.avgMonthlyIncome.ToString();
                AvgMonthlySurplus = summaryInfo.avgMonthlySurplus.ToString();
                ClosingBalance = summaryInfo.closingBalance.ToString();
                ExpenseToIncomeRatio = summaryInfo.expenseToIncomeRatio.ToString();
                HighestIncomeInAMonth = summaryInfo.highestIncomeInAMonth.ToString();
                HighestInvestmentExpenseInAMonth = summaryInfo.highestInvestmentExpenseInAMonth.ToString();
                HighestInvestmentIncomeInAMonth = summaryInfo.highestInvestmentIncomeInAMonth.ToString();
                HighestSavingsInAMonth = summaryInfo.highestSavingsInAMonth.ToString();
                InwardChqBounces = summaryInfo.inwardChqBounces.ToString();
                InwardEcsBounces = summaryInfo.inwardECSBounces.ToString();
                InwardOtherBounces = summaryInfo.inwardOtherBounces.ToString();
                LastMonthExpense = summaryInfo.lastMonthExpense.ToString();
                LastMonthIncome = summaryInfo.lastMonthIncome.ToString();
                LastMonthInvestmentExpense = summaryInfo.lastMonthInvestmentExpense.ToString();
                LastMonthInvestmentIncome = summaryInfo.lastMonthInvestmentIncome.ToString();
                LastMonthSavings = summaryInfo.lastMonthSavings.ToString();
                LowestExpenseInAMonth = summaryInfo.lowestExpenseInAMonth.ToString();
                LowestIncomeInAMonth = summaryInfo.lowestIncomeInAMonth.ToString();
                LowestInvestmentExpenseInAMonth = summaryInfo.lowestInvestmentExpenseInAMonth.ToString();
                LowestInvestmentIncomeInAMonth = summaryInfo.lowestInvestmentIncomeInAMonth.ToString();
                LowestSavingsInAMonth = summaryInfo.lowestSavingsInAMonth.ToString();
                MaxBalance = summaryInfo.maxBalance.ToString();
                MaxCredit = summaryInfo.maxCredit.ToString();
                MaxDebit = summaryInfo.maxDebit.ToString();
                MinBalance = summaryInfo.minBalance.ToString();
                MinCredit = summaryInfo.minCredit.ToString();
                MinDebit = summaryInfo.minDebit.ToString();
                NumberOfTransactions = summaryInfo.numberOfTransactions.ToString();
                OpeningBalance = summaryInfo.openingBalance.ToString();
                OutwardChqBounces = summaryInfo.outwardChqBounces.ToString();
                OutwardEcsBounces = summaryInfo.outwardECSBounces.ToString();
                OutwardOtherBounces = summaryInfo.outwardOtherBounces.ToString();
                SalaryCredits = summaryInfo.salaryCredits.ToString();
            }
        }
        public string AvgBalanceOf6Dates { get; set; }
     
        public string AvgDailyBalance { get; set; }
       
        public string AvgInvestmentExpensePerMonth { get; set; }
   
        public string AvgInvestmentIncomePerMonth { get; set; }
      
        public string AvgMonthlyExpense { get; set; }
       
        public string AvgMonthlyIncome { get; set; }
       
        public string AvgMonthlySurplus { get; set; }
     
        public string ClosingBalance { get; set; }
     
        public string ExpenseToIncomeRatio { get; set; }
      
        public string HighestExpenseInAMonth { get; set; }
      
        public string HighestIncomeInAMonth { get; set; }
       
        public string HighestInvestmentExpenseInAMonth { get; set; }
       
        public string HighestInvestmentIncomeInAMonth { get; set; }
       
        public string HighestSavingsInAMonth { get; set; }
     
        public string InwardChqBounces { get; set; }
     
        public string InwardEcsBounces { get; set; }
      
        public string InwardOtherBounces { get; set; }
       
        public string LastMonthExpense { get; set; }
       
        public string LastMonthIncome { get; set; }
       
        public string LastMonthInvestmentExpense { get; set; }
      
        public string LastMonthInvestmentIncome { get; set; }
     
        public string LastMonthSavings { get; set; }
      
        public string LowestExpenseInAMonth { get; set; }
    
        public string LowestIncomeInAMonth { get; set; }
       
        public string LowestInvestmentExpenseInAMonth { get; set; }
      
        public string LowestInvestmentIncomeInAMonth { get; set; }
      
        public string LowestSavingsInAMonth { get; set; }
       
        public string MaxBalance { get; set; }
      
        public string MaxCredit { get; set; }
      
        public string MaxDebit { get; set; }
      
        public string MinBalance { get; set; }
       
        public string MinCredit { get; set; }
       
        public string MinDebit { get; set; }
    
        public string NumberOfTransactions { get; set; }
   
        public string OpeningBalance { get; set; }
       
        public string OutwardChqBounces { get; set; }
        
        public string OutwardEcsBounces { get; set; }
    
        public string OutwardOtherBounces { get; set; }
      
        public string SalaryCredits { get; set; }
    }
}
