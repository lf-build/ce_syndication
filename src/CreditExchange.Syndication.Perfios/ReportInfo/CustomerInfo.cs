﻿namespace CreditExchange.Syndication.Perfios.ReportInfo
{
    public class CustomerInfo : ICustomerInfo
    {
        public  CustomerInfo()
        {
        }
        public CustomerInfo(Proxy.Response.Data reportResponse)
        {
          
            if(reportResponse?.CustomerInfo!= null)
            {
                Name = reportResponse.CustomerInfo.name;
                Address = reportResponse.CustomerInfo.address;
                Landline = reportResponse.CustomerInfo.landline;
                Mobile = reportResponse.CustomerInfo.mobile;
                Email = reportResponse.CustomerInfo.email;
                Pan = reportResponse.CustomerInfo.pan;
                SourceOfData = reportResponse.CustomerInfo.sourceOfData;
            }
         }

        public string Name { get; set; }
        public string Address { get; set; }
        public string Landline { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Pan { get; set; }
        public string SourceOfData { get; set; }

    }
}
