﻿using System.Collections.Generic;
using System.Linq;
using CreditExchange.Syndication.Perfios.Response;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace CreditExchange.Syndication.Perfios.ReportInfo
{
    public class RecurringExpense :IRecurringExpense
    {
        public RecurringExpense()
        {

        }
        public RecurringExpense(Proxy.Response.RecurringExpensesRecurringExpense recurringExpense)
        {
            if (recurringExpense != null)
            {
                Category = recurringExpense.category;
                if(recurringExpense?.MonthwiseBreakup!=null)
                MonthwiseBreakups =
                    recurringExpense.MonthwiseBreakup.Select(p => new MonthwiseBreakup(p))
                        .ToList<IMonthwiseBreakup>();
                if (recurringExpense?.Xns != null)
                    Tranasactions =
                    recurringExpense.Xns.Select(p => new TranasactionInfo(p))
                        .ToList<ITranasactionInfo>();
            }
        }
        public string Category { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IMonthwiseBreakup, MonthwiseBreakup>))]
        public List<IMonthwiseBreakup> MonthwiseBreakups { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITranasactionInfo, TranasactionInfo>))]
        public List<ITranasactionInfo> Tranasactions { get; set; }
    }
}
