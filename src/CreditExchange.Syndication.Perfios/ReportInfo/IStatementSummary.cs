﻿namespace CreditExchange.Syndication.Perfios.ReportInfo
{
    public interface IStatementSummary
    {
        string AvgBalanceOf6Dates { get; set; }
        string AvgDailyBalance { get; set; }
        string AvgInvestmentExpensePerMonth { get; set; }
        string AvgInvestmentIncomePerMonth { get; set; }
        string AvgMonthlyExpense { get; set; }
        string AvgMonthlyIncome { get; set; }
        string AvgMonthlySurplus { get; set; }
        string ClosingBalance { get; set; }
        string ExpenseToIncomeRatio { get; set; }
        string HighestExpenseInAMonth { get; set; }
        string HighestIncomeInAMonth { get; set; }
        string HighestInvestmentExpenseInAMonth { get; set; }
        string HighestInvestmentIncomeInAMonth { get; set; }
        string HighestSavingsInAMonth { get; set; }
        string InwardChqBounces { get; set; }
        string InwardEcsBounces { get; set; }
        string InwardOtherBounces { get; set; }
        string LastMonthExpense { get; set; }
        string LastMonthIncome { get; set; }
        string LastMonthInvestmentExpense { get; set; }
        string LastMonthInvestmentIncome { get; set; }
        string LastMonthSavings { get; set; }
        string LowestExpenseInAMonth { get; set; }
        string LowestIncomeInAMonth { get; set; }
        string LowestInvestmentExpenseInAMonth { get; set; }
        string LowestInvestmentIncomeInAMonth { get; set; }
        string LowestSavingsInAMonth { get; set; }
        string MaxBalance { get; set; }
        string MaxCredit { get; set; }
        string MaxDebit { get; set; }
        string MinBalance { get; set; }
        string MinCredit { get; set; }
        string MinDebit { get; set; }
        string NumberOfTransactions { get; set; }
        string OpeningBalance { get; set; }
        string OutwardChqBounces { get; set; }
        string OutwardEcsBounces { get; set; }
        string OutwardOtherBounces { get; set; }
        string SalaryCredits { get; set; }
    }
}