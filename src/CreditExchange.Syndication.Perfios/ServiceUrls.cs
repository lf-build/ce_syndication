﻿namespace CreditExchange.Syndication.Perfios
{
    public class ServiceUrls :IServiceUrls
    {
        public string StartNetLinkingProcessUrl { get; set; } = "https://demo.perfios.com/KuberaVault/insights/start";
        public string StartTransactionUrl { get; set; } = "https://demo.perfios.com/KuberaVault/insights/api/statement/start";
        public string UploadStatementUrl { get; set; } = "https://demo.perfios.com/KuberaVault/insights/api/statement/upload";
        public string CompleteTransactionUrl { get; set; } = "https://demo.perfios.com/KuberaVault/insights/api/statement/done";
        public string TransactionStatusUrl { get; set; } = "https://demo.perfios.com/KuberaVault/insights/txnstatus";
        public string RetrieveReportsandStatementsUrl { get; set; } = "https://demo.perfios.com/KuberaVault/insights/retrieve";
        public string DeletedataUrl { get; set; } = "https://demo.perfios.com/KuberaVault/insights/delete";
        public string RequestReviewofAPerfiosTransactionUrl { get; set; } = "https://demo.perfios.com/KuberaVault/insights/review";
        public string ListofsupportedInstitutionsUrl { get; set; } = "https://demo.perfios.com/KuberaVault/insights/institutions";
    }
}
