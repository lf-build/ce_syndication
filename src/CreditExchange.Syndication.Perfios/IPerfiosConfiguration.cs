﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Perfios
{
    public interface IPerfiosConfiguration
    {
        string ApiVersion { get; set; }
        IServiceUrls ServiceUrls { get; set; }
        string VendorId { get; set; }
        string PrivateKey { get; set; }
        string Destination { get; set; }
        string AcceptancePolicy { get; set; }
         string LoanType { get; set; } 
        string LoanDuration { get; set; } 
         string LoanAmount { get; set; } 
        string ProxyUrl { get; set; }
        bool UseProxy { get; set; }
        string PublicKey { get; set; }
        string  NetLinkForm { get; set; }
        //List<string> RetryErrorCode { get; set; }
    }
}