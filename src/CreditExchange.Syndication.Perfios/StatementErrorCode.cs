﻿namespace CreditExchange.Syndication.Perfios
{
    public class StatementErrorCode
    {
        public string ErrorCode { get; set; }
        public string Description { get; set; }
    }
}
