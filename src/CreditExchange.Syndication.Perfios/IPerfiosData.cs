﻿using LendFoundry.Foundation.Persistence;
using System;

namespace CreditExchange.Syndication.Perfios
{
   public  interface IPerfiosData : IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        string PerfiosTransactionId { get; set; }
        string Referencenumber { get; set; }

        string TransactionId { get; set; }

        bool IsForm26ASInfoAvailable { get; set; }

        byte[] File { get; set; }

        string Password { get; set; }
        string XMLResponse { get; set; }
        string XMLRequest { get; set; }
       
        DateTimeOffset ProcessDate { get; set; }
        string Status { get; set; }
    }
}
