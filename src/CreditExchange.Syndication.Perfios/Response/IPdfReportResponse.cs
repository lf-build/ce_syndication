﻿namespace CreditExchange.Syndication.Perfios.Response
{
    public interface IPdfReportResponse
    {
        string ReferenceNumber { get; set; }
        byte[] Report { get; set; }
    }
}