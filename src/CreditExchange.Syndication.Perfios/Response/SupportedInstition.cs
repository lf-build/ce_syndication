﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace CreditExchange.Syndication.Perfios.Response
{
    public class SupportedInstition : ISupportedInstition
    {
        public SupportedInstition(Proxy.Response.SupportedInstitutions supportedInstitution)
        {
            if(supportedInstitution!=null)
            {
                Instiutions = supportedInstitution.Institution.Select(p => new Institution(p)).ToList<IInstitution>();
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<IInstitution, Institution>))]
        public List<IInstitution> Instiutions { get; set; }
        public string ReferenceNumber { get; set; }
    }
}
