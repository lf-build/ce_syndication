﻿namespace CreditExchange.Syndication.Perfios.Response
{
    public class TDS :ITDS
    {
        public TDS()
        {

        }
        public TDS(Proxy.Response.TDS tds)
        {
            if (tds != null)
            {
                Tds = tds.tds;
                Section = tds.section;
                Deductor = tds.deductor;
                Date = tds.date;
                Amount = tds.amount;
                Tan = tds.tan;
            }
        }
        public string Tds { get; set; }
        public string Section { get; set; }
        public string Deductor { get; set; }
        public string Date { get; set; }
        public string Amount { get; set; }
        public string Tan { get; set; }
    }
}
