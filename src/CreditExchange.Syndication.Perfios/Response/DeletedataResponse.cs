﻿namespace CreditExchange.Syndication.Perfios.Response
{
    public class DeletedataResponse : IDeletedataResponse
    {
        public string ReferenceNumber { get; set; }
        public string Status { get; set; }
    }
}
