﻿namespace CreditExchange.Syndication.Perfios.Response
{
    public interface IReportResponse
    {
        string XMLResponseStr { get; set; }
        IAnalysisReport AnalysisReport { get; set; }
        string XMLRequestStr { get; set; }
    }
}