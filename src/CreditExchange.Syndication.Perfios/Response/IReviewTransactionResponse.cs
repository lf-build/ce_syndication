﻿namespace CreditExchange.Syndication.Perfios.Response
{
    public interface IReviewTransactionResponse
    {
        string ReferenceNumber { get; set; }
        string Status { get; set; }
    }
}