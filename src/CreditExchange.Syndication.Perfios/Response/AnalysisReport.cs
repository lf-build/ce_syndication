﻿using CreditExchange.Syndication.Perfios.ReportInfo;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using CustomerInfo = CreditExchange.Syndication.Perfios.ReportInfo.CustomerInfo;
using RecurringExpense = CreditExchange.Syndication.Perfios.ReportInfo.RecurringExpense;
using System.Linq;

namespace CreditExchange.Syndication.Perfios.Response
{
    public class AnalysisReport :IAnalysisReport
    {
        public AnalysisReport()
        {

        }
        public AnalysisReport(Proxy.Response.Data reportResponse)
        {

            if (reportResponse != null)
            {
                if (reportResponse.Account?.Xns != null)
                    Transactions = reportResponse.Account.Xns.Select(p => new TranasactionInfo(p)).ToList<ITranasactionInfo>();
                CustomerInfo = new CustomerInfo(reportResponse);
                if (reportResponse?.Account != null)
                {
                    Bank = reportResponse.Account.bank;
                    AccountNo = reportResponse.Account.accountNo.ToString();
                    AccountType = reportResponse.Account.accountType;
                    IFSCCode = reportResponse.Account.ifscCode;
                    Location = reportResponse.Account.location;
                    MICRCode = reportResponse.Account.micrCode;
                }
                if (reportResponse?.CustomerInfo != null)
                {
                    StartDate = reportResponse.CustomerInfo.startDate.ToString();
                    EndDate = reportResponse.CustomerInfo.endDate.ToString();
                    DurationInMonths = reportResponse.CustomerInfo.durationInMonths.ToString();
                }
                if (reportResponse?.SummaryInfo != null)
                    StatementSummary = new StatementSummary(reportResponse.SummaryInfo);
                if (reportResponse?.RecurringExpenses?.KnownTypes != null)
                    RecurringExpenses =
                        reportResponse.RecurringExpenses.KnownTypes.Select(p => new RecurringExpense(p))
                            .ToList<IRecurringExpense>();
                if (reportResponse?.Form26ASInfo != null)
                    Form26ASInfo = new Form26ASInfo(reportResponse.Form26ASInfo);

            }
        }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string DurationInMonths { get; set; }
        public string Bank { get; set; }
        public string AccountNo { get; set; }
        public string AccountType { get; set; }

        public string IFSCCode { get; set; }

        public string MICRCode { get; set; }
        public string Location { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ICustomerInfo, CustomerInfo>))]
        public ICustomerInfo CustomerInfo { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IStatementSummary, StatementSummary>))]
        public IStatementSummary StatementSummary { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITranasactionInfo, TranasactionInfo>))]
        public List<ITranasactionInfo> Transactions { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IRecurringExpense, RecurringExpense>))]
        public List<IRecurringExpense> RecurringExpenses { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IForm26ASInfo, Form26ASInfo>))]
        public IForm26ASInfo Form26ASInfo { get; set; }
        public string ReferenceNumber { get; set; }
        public string StatementPassword { get; set; }
    }
}
