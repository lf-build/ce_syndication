﻿using System.Collections.Generic;
using System.Linq;

namespace CreditExchange.Syndication.Perfios.Response
{
    public class TransactionStatusResponse : ITransactionStatusResponse
    {
        public TransactionStatusResponse(Proxy.Response.TransactionStatusResponse transactionStatusResponse)
        {
            if (transactionStatusResponse != null)
            {
                if (transactionStatusResponse?.Part!=null)
                {
                    Part = transactionStatusResponse.Part.Select(p => new Part(p)).ToList<IPart>();
                }
           
                Parts = transactionStatusResponse.Parts;
                Processing = transactionStatusResponse.Processing;
                Files = transactionStatusResponse.Files;
             }
        }


        public List<IPart> Part { get; set; }
        public string Parts { get; set; }
        public string Processing { get; set; }
        public string Files { get; set; }

       public string ReferenceNumber { get; set; }
    }

   
    public class Part :IPart
    {
        public Part(Proxy.Response.Part part)
        {
            Status = part.Status;
            ErrorCode = part.ErrorCode;
            Reason = part.Reason;
            PerfiosTransactionId = part.PerfiosTransactionId;
        }
        public string PerfiosTransactionId { get; set; }
   
        public string Status { get; set; }
   
        public string ErrorCode { get; set; }
   
        public string Reason { get; set; }
    }
}
