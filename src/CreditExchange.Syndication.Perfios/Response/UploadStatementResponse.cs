﻿namespace CreditExchange.Syndication.Perfios.Response
{
    public class UploadStatementResponse : IUploadStatementResponse
    {
        public UploadStatementResponse(Proxy.Response.UploadStatementResponse uploadResponse)
        {
            if (uploadResponse != null)
                StatementId = uploadResponse.StatementId;
        }
        public string StatementId { get; set; }

        public string ReferenceNumber { get; set; }
    }
}
