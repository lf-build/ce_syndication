﻿namespace CreditExchange.Syndication.Perfios.Response
{
    public  interface ITDS
    {
         string Tds { get; set; }
         string Section { get; set; }
         string Deductor { get; set; }
         string Date { get; set; }
         string Amount { get; set; }
         string Tan { get; set; }
    }
}
