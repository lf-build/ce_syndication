﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Perfios
{
   public interface IErrorResponse
    {
        string Code { get; set; }
        string Message { get; set; }
        string StatementErrorCode { get; set; }
    }
}
