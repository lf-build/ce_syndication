﻿namespace CreditExchange.Syndication.Perfios.Request
{
    public interface ICompleteTransactionRequest :ISource
    {
        string PerfiosTransactionId { get; set; }
    }
}