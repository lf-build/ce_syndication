﻿namespace CreditExchange.Syndication.Perfios.Request
{
    public class ReviewTransactionRequest :Source, IReviewTransactionRequest
    {
        public string PerfiosTransactionId { get; set; }
    }
}
