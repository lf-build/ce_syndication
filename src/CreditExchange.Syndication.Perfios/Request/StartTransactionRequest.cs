﻿namespace CreditExchange.Syndication.Perfios.Request
{
    public class StartTransactionRequest :Source, IStartTransactionRequest
    {
        public  StartTransactionRequest( )
        {
            
        }
        public StartTransactionRequest(IPerfiosInitializationRequest perfiosRequest)
        {
            InstitutionId = perfiosRequest.InstitutionId;
            YearMonthFrom = perfiosRequest.YearMonthFrom;
            YearMonthTo = perfiosRequest.YearMonthTo;
        }
        public string InstitutionId { get; set; }
        public string YearMonthFrom { get; set; }
        public string YearMonthTo { get; set; }
    }
}
