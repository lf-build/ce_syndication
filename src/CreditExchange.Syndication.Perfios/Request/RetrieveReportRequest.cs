﻿namespace CreditExchange.Syndication.Perfios.Request
{
    public class RetrieveReportRequest : Source, IRetrieveReportRequest
    {
        public string PerfiosTransactionId { get; set; }

        public string TransactionId { get; set; }
    }
}
