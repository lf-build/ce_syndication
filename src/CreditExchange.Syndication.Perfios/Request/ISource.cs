﻿namespace CreditExchange.Syndication.Perfios.Request
{
  public interface ISource
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
    }
}
