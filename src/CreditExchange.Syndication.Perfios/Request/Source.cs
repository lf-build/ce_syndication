﻿namespace CreditExchange.Syndication.Perfios.Request
{
    public class Source : ISource
    {
        public string EntityId { get;set;}
        public string EntityType { get; set; }
     
    }
}