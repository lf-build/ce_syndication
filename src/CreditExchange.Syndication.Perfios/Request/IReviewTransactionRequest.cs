﻿namespace CreditExchange.Syndication.Perfios.Request
{
    public interface IReviewTransactionRequest :ISource
    {
        string PerfiosTransactionId { get; set; }
    }
}