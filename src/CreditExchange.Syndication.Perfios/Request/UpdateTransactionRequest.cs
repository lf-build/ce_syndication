﻿namespace CreditExchange.Syndication.Perfios.Request
{
    public class UpdateTransactionRequest :IUpdateTransactionRequest
    {
        public string PerfiosTransactionId { get; set; }
        public string Referencenumber { get; set; }
        public string TransactionId { get; set; }
        public byte[] File { get; set; }
        public bool IsForm26ASInfoAvailable { get; set; }
        public string Password { get; set; }
        public string XMLResponse { get; set; }
    }
}
