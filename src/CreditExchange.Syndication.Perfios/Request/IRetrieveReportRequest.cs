﻿namespace CreditExchange.Syndication.Perfios.Request
{
    public interface IRetrieveReportRequest :ISource
    {
        string PerfiosTransactionId { get; set; }

        string TransactionId { get; set; }
    }
}