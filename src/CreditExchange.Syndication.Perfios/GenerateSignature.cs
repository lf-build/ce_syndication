﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Encodings;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Utilities.Encoders;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CreditExchange.Syndication.Perfios
{
    public static class GenerateSignature
    {
        public static string RsaEncrypt(string rawText, string privateKey)
        {
            string digest = Sha1MakeDigest(rawText);

            var bytesToEncrypt = Encoding.UTF8.GetBytes(digest);

            var encryptEngine = new Pkcs1Encoding(new RsaEngine());

            using (var txtreader = new StringReader(privateKey))
            {
                var keyPair = (AsymmetricCipherKeyPair)new PemReader(txtreader).ReadObject();

                encryptEngine.Init(true, keyPair.Private);
            }

            byte[] encoded = Hex.Encode(encryptEngine.ProcessBlock(bytesToEncrypt, 0, bytesToEncrypt.Length));
            return Encoding.UTF8.GetString(encoded);
        }
        public static string PasswordEncrypt(string rawText, string publickey)
        {

          

            var bytesToEncrypt = Encoding.UTF8.GetBytes(rawText);

            var encryptEngine = new Pkcs1Encoding(new RsaEngine());

            using (var txtreader = new StringReader(publickey))
            {
                var keyPair = (AsymmetricKeyParameter)new PemReader(txtreader).ReadObject();

                encryptEngine.Init(true, keyPair);
            }

            byte[] encoded = Hex.Encode(encryptEngine.ProcessBlock(bytesToEncrypt, 0, bytesToEncrypt.Length));
            return Encoding.UTF8.GetString(encoded);
        }
        public static string Sha1MakeDigest(string input)
        {
            var sha1 = SHA1.Create();
            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            byte[] outputBytes = sha1.ComputeHash(inputBytes);
            byte[] encoded = Hex.Encode(outputBytes);
            return Encoding.UTF8.GetString(encoded);
        }
    }
}
