﻿using CreditExchange.Syndication.Perfios.Request;
using System;

namespace CreditExchange.Syndication.Perfios.Proxy.Request
{
    public partial class  StartProcessRequest
    {
        public StartProcessRequest()
        {

        }
        public StartProcessRequest(IPerfiosConfiguration perfiosConfiguration, IStartProcessRequest startProcessReuest)
        {
            if (perfiosConfiguration == null)
                throw new ArgumentNullException(nameof(perfiosConfiguration));
            if (startProcessReuest == null)
                throw new ArgumentNullException(nameof(startProcessReuest));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.ServiceUrls.StartTransactionUrl))
                throw new ArgumentException("StartTransactionURL is required", nameof(perfiosConfiguration.ServiceUrls.StartTransactionUrl));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.ApiVersion))
                throw new ArgumentException("ApiVersion is required", nameof(perfiosConfiguration.ApiVersion));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.VendorId))
                throw new ArgumentException("VendorId is required", nameof(perfiosConfiguration.VendorId));

            if (string.IsNullOrWhiteSpace(startProcessReuest.EmailId))
                throw new ArgumentException("EmailId is required", nameof(startProcessReuest.EmailId));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.Destination))
                throw new ArgumentException("Destination  is required", nameof(perfiosConfiguration.Destination));
        

            ApiVersion = perfiosConfiguration.ApiVersion;
            VendorId = perfiosConfiguration.VendorId;
            TxnId = startProcessReuest.TransactionId;
            YearMonthFrom = startProcessReuest.YearMonthFrom;
            YearMonthTo = startProcessReuest.YearMonthTo;
            AcceptancePolicy = perfiosConfiguration.AcceptancePolicy;
            EmailId = startProcessReuest.EmailId;
            DOB = startProcessReuest.DOB;
            ReturnUrl = startProcessReuest.ReturnUrl;
            Destination = perfiosConfiguration.Destination;
            }
    }
}
