﻿using System;

namespace CreditExchange.Syndication.Perfios.Proxy.Request
{
    public partial class TransactionStatusRequest
    {
        public TransactionStatusRequest()
        {

        }
        public TransactionStatusRequest(IPerfiosConfiguration perfiosConfiguration, string transactionId)
        {
            if (perfiosConfiguration == null)
                throw new ArgumentNullException(nameof(perfiosConfiguration));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.ServiceUrls.TransactionStatusUrl))
                throw new ArgumentException("TransactionStatusURL is required", nameof(perfiosConfiguration.ServiceUrls.TransactionStatusUrl));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.ApiVersion))
                throw new ArgumentException("ApiVersion is required", nameof(perfiosConfiguration.ApiVersion));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.VendorId))
                throw new ArgumentException("VendorId is required", nameof(perfiosConfiguration.VendorId));
            if (string.IsNullOrWhiteSpace(transactionId))
                throw new ArgumentException("TransactionId is required", nameof(transactionId));
            ApiVersion = perfiosConfiguration.ApiVersion;
            TxnId = transactionId;
            VendorId = perfiosConfiguration.VendorId;
        }
    }
}
