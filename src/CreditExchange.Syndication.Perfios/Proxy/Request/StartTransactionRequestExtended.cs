﻿using CreditExchange.Syndication.Perfios.Request;
using System;

namespace CreditExchange.Syndication.Perfios.Proxy.Request
{
    public partial class StartTransactionRequest
    {
        public StartTransactionRequest()
        {

        }
        public StartTransactionRequest(IPerfiosConfiguration perfiosConfiguration,IStartTransactionRequest startTransacationReuest)
        {
            if (perfiosConfiguration == null)
                throw new ArgumentNullException(nameof(perfiosConfiguration));
            if (startTransacationReuest == null)
                throw new ArgumentNullException(nameof(startTransacationReuest));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.ServiceUrls.StartTransactionUrl))
                throw new ArgumentException("StartTransactionURL is required", nameof(perfiosConfiguration.ServiceUrls.StartTransactionUrl));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.ApiVersion))
                throw new ArgumentException("ApiVersion is required", nameof(perfiosConfiguration.ApiVersion));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.VendorId))
                throw new ArgumentException("VendorId is required", nameof(perfiosConfiguration.VendorId));
           
            if (string.IsNullOrWhiteSpace(startTransacationReuest.InstitutionId))
                throw new ArgumentException("InstitutionId is required", nameof(startTransacationReuest.InstitutionId));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.LoanDuration))
                throw new ArgumentException("LoanDuration  must be greater than or equal to 1", nameof(perfiosConfiguration.LoanDuration));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.LoanType))
                throw new ArgumentException("LoanType is require", nameof(perfiosConfiguration.LoanType));

            ApiVersion = perfiosConfiguration.ApiVersion;
            VendorId = perfiosConfiguration.VendorId;
            TxnId = Guid.NewGuid().ToString("N");
            InstitutionId = startTransacationReuest.InstitutionId;
            LoanAmount = perfiosConfiguration.LoanAmount;
            LoanDuration = perfiosConfiguration.LoanDuration;
            LoanType = perfiosConfiguration.LoanType;
            YearMonthFrom = startTransacationReuest.YearMonthFrom;
            YearMonthTo = startTransacationReuest.YearMonthTo;
            AcceptancePolicy = perfiosConfiguration.AcceptancePolicy;
        }
    }
}
