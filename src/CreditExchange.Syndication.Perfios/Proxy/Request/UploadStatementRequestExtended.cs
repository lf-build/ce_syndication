﻿using System;

namespace CreditExchange.Syndication.Perfios.Proxy.Request
{
    public partial class UploadStatementRequest
    {
        public UploadStatementRequest()
        {

        }
        public UploadStatementRequest(IPerfiosConfiguration perfiosConfiguration, byte[] filBytes,string password)
        {
            if (perfiosConfiguration == null)
                throw new ArgumentNullException(nameof(perfiosConfiguration));
           if (string.IsNullOrWhiteSpace(perfiosConfiguration.ServiceUrls.UploadStatementUrl))
                throw new ArgumentException("UploadStatementURL is required", nameof(perfiosConfiguration.ServiceUrls.UploadStatementUrl));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.VendorId))
                throw new ArgumentException("VendorId is required", nameof(perfiosConfiguration.VendorId));
            if (filBytes == null)
                throw new ArgumentException("File is required", nameof(filBytes));
            VendorId = perfiosConfiguration.VendorId;
            File = filBytes;
            Password = password;
        }
    }
}
