﻿using System.Xml.Serialization;

namespace CreditExchange.Syndication.Perfios.Proxy.Request
{
    [XmlRoot(ElementName = "payload")]
    public partial class StartProcessRequest
    {
        [XmlElement(ElementName = "apiVersion")]
        public string ApiVersion { get; set; }
        [XmlElement(ElementName = "vendorId")]
        public string VendorId { get; set; }
        [XmlElement(ElementName = "txnId")]
        public string TxnId { get; set; }
        [XmlElement(ElementName = "destination")]
        public string Destination { get; set; }
        [XmlElement(ElementName = "emailId")]
        public string EmailId { get; set; }
        [XmlElement(ElementName = "yearMonthFrom")]
        public string YearMonthFrom { get; set; }
        [XmlElement(ElementName = "yearMonthTo")]
        public string YearMonthTo { get; set; }
        [XmlElement(ElementName = "form26ASDOB")]
        public string DOB { get; set; }
        [XmlElement(ElementName = "returnUrl")]
        public string ReturnUrl { get; set; }

        [XmlElement(ElementName = "acceptancePolicy")]
        public string AcceptancePolicy { get; set; }
    }
}
