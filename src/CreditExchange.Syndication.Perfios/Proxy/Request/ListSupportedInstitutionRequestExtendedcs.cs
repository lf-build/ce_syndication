﻿using System;

namespace CreditExchange.Syndication.Perfios.Proxy.Request
{
    public partial class ListSupportedInstitutionRequest
    {
        public ListSupportedInstitutionRequest()
        {

        }
        public ListSupportedInstitutionRequest(IPerfiosConfiguration perfiosConfiguration)
        {
            if (perfiosConfiguration == null)
                throw new ArgumentNullException(nameof(perfiosConfiguration));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.ServiceUrls.ListofsupportedInstitutionsUrl))
                throw new ArgumentException("ListofsupportedInstitutionsURL is required", nameof(perfiosConfiguration.ServiceUrls.ListofsupportedInstitutionsUrl));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.ApiVersion))
                throw new ArgumentException("ApiVersion is required", nameof(perfiosConfiguration.ApiVersion));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.VendorId))
                throw new ArgumentException("VendorId is required", nameof(perfiosConfiguration.VendorId));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.Destination))
                throw new ArgumentException("Destination is required", nameof(perfiosConfiguration.Destination));
            ApiVersion = perfiosConfiguration.ApiVersion;
            VendorId = perfiosConfiguration.VendorId;
            Destination = perfiosConfiguration.Destination;
        }
    }
}
