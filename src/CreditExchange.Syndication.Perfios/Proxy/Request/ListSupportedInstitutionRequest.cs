﻿using System.Xml.Serialization;

namespace CreditExchange.Syndication.Perfios.Proxy.Request
{

    [XmlRoot(ElementName = "payload")]
    public partial class ListSupportedInstitutionRequest
    {
        [XmlElement(ElementName = "apiVersion")]
        public string ApiVersion { get; set; } 
        [XmlElement(ElementName = "destination")]
        public string Destination { get; set; } 
        [XmlElement(ElementName = "vendorId")]
        public string VendorId { get; set; }
    }
}
