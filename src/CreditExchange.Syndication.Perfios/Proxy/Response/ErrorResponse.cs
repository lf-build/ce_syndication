﻿using System.Xml.Serialization;

namespace CreditExchange.Syndication.Perfios.Proxy.Response
{
   
    [XmlRoot(ElementName = "Error")]
    public class ErrorResponse
    {
        [XmlElement(ElementName = "code")]
        public string Code { get; set; }
        [XmlElement(ElementName = "message")]
        public string Message { get; set; }
        [XmlElement(ElementName = "statementErrorCode")]
        public string StatementErrorCode { get; set; }
    }
}
