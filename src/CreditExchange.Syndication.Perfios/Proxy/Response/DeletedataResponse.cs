﻿using System.Xml.Serialization;

namespace CreditExchange.Syndication.Perfios.Proxy.Response
{

    [XmlRoot(ElementName = "Status")]
    public class DeletedataResponse
    {
        [XmlText]
        public string Text { get; set; }
    }
}
