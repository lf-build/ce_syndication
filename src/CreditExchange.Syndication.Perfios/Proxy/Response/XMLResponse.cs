﻿namespace CreditExchange.Syndication.Perfios.Proxy.Response
{


    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://www.perfios.com/PIR")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://www.perfios.com/PIR", IsNullable = false)]
    public partial class Data
    {

        private CustomerInfo customerInfoField;

        private SummaryInfo summaryInfoField;

        private RecurringExpenses recurringExpensesField;

        private Account accountField;

        private Form26ASInfo form26ASInfoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public Form26ASInfo Form26ASInfo
        {
            get
            {
                return this.form26ASInfoField;
            }
            set
            {
                this.form26ASInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public CustomerInfo CustomerInfo
        {
            get
            {
                return this.customerInfoField;
            }
            set
            {
                this.customerInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public SummaryInfo SummaryInfo
        {
            get
            {
                return this.summaryInfoField;
            }
            set
            {
                this.summaryInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public RecurringExpenses RecurringExpenses
        {
            get
            {
                return this.recurringExpensesField;
            }
            set
            {
                this.recurringExpensesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public Account Account
        {
            get
            {
                return this.accountField;
            }
            set
            {
                this.accountField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class CustomerInfo
    {

        private string nameField;

        private string addressField;

        private string landlineField;

        private string mobileField;

        private string emailField;

        private string panField;

        private string perfiosTransactionIdField;

        private string customerTransactionIdField;

        private string sourceOfDataField;

        private System.DateTime startDateField;

        private System.DateTime endDateField;

        private string durationInMonthsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string landline
        {
            get
            {
                return this.landlineField;
            }
            set
            {
                this.landlineField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string mobile
        {
            get
            {
                return this.mobileField;
            }
            set
            {
                this.mobileField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string pan
        {
            get
            {
                return this.panField;
            }
            set
            {
                this.panField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string perfiosTransactionId
        {
            get
            {
                return this.perfiosTransactionIdField;
            }
            set
            {
                this.perfiosTransactionIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string customerTransactionId
        {
            get
            {
                return this.customerTransactionIdField;
            }
            set
            {
                this.customerTransactionIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string sourceOfData
        {
            get
            {
                return this.sourceOfDataField;
            }
            set
            {
                this.sourceOfDataField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime startDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime endDate
        {
            get
            {
                return this.endDateField;
            }
            set
            {
                this.endDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string durationInMonths
        {
            get
            {
                return this.durationInMonthsField;
            }
            set
            {
                this.durationInMonthsField = value;
            }
        }
    }
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class PersonalDetails
    {
        private string nameField;
        private string addressField;
        private string dobField;
        private string panField;
        private string panStatusField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string dob
        {
            get
            {
                return this.dobField;
            }
            set
            {
                this.dobField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string pan
        {
            get
            {
                return this.panField;
            }
            set
            {
                this.panField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string panStatus
        {
            get
            {
                return this.panStatusField;
            }
            set
            {
                this.panStatusField = value;
            }
        }
    }
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class TDS
    {
        private string tdsField;
        private string sectionField;
        private string deductorField;
        private string dateField;
        private string amountField;
        private string tanField;
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string tds
        {
            get
            {
                return this.tdsField;
            }
            set
            {
                this.tdsField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string section
        {
            get
            {
                return this.sectionField;
            }
            set
            {
                this.sectionField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string deductor
        {
            get
            {
                return this.deductorField;
            }
            set
            {
                this.deductorField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string tan
        {
            get
            {
                return this.tanField;
            }
            set
            {
                this.tanField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class SummaryInfo
    {

        private string avgBalanceOf6DatesField;

        private string avgDailyBalanceField;

        private string avgInvestmentExpensePerMonthField;

        private string avgInvestmentIncomePerMonthField;

        private string avgMonthlyExpenseField;

        private string avgMonthlyIncomeField;

        private string avgMonthlySurplusField;

        private string closingBalanceField;

        private string expenseToIncomeRatioField;

        private string highestExpenseInAMonthField;

        private string highestIncomeInAMonthField;

        private string highestInvestmentExpenseInAMonthField;

        private string highestInvestmentIncomeInAMonthField;

        private string highestSavingsInAMonthField;

        private string inwardChqBouncesField;

        private string inwardECSBouncesField;

        private string inwardOtherBouncesField;

        private string lastMonthExpenseField;

        private string lastMonthIncomeField;

        private string lastMonthInvestmentExpenseField;

        private string lastMonthInvestmentIncomeField;

        private string lastMonthSavingsField;

        private string lowestExpenseInAMonthField;

        private string lowestIncomeInAMonthField;

        private string lowestInvestmentExpenseInAMonthField;

        private string lowestInvestmentIncomeInAMonthField;

        private string lowestSavingsInAMonthField;

        private string maxBalanceField;

        private string maxCreditField;

        private string maxDebitField;

        private string minBalanceField;

        private string minCreditField;

        private string minDebitField;

        private string numberOfTransactionsField;

        private string openingBalanceField;

        private string outwardChqBouncesField;

        private string outwardECSBouncesField;

        private string outwardOtherBouncesField;

        private string salaryCreditsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string avgBalanceOf6Dates
        {
            get
            {
                return this.avgBalanceOf6DatesField;
            }
            set
            {
                this.avgBalanceOf6DatesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string avgDailyBalance
        {
            get
            {
                return this.avgDailyBalanceField;
            }
            set
            {
                this.avgDailyBalanceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string avgInvestmentExpensePerMonth
        {
            get
            {
                return this.avgInvestmentExpensePerMonthField;
            }
            set
            {
                this.avgInvestmentExpensePerMonthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string avgInvestmentIncomePerMonth
        {
            get
            {
                return this.avgInvestmentIncomePerMonthField;
            }
            set
            {
                this.avgInvestmentIncomePerMonthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string avgMonthlyExpense
        {
            get
            {
                return this.avgMonthlyExpenseField;
            }
            set
            {
                this.avgMonthlyExpenseField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string avgMonthlyIncome
        {
            get
            {
                return this.avgMonthlyIncomeField;
            }
            set
            {
                this.avgMonthlyIncomeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string avgMonthlySurplus
        {
            get
            {
                return this.avgMonthlySurplusField;
            }
            set
            {
                this.avgMonthlySurplusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string closingBalance
        {
            get
            {
                return this.closingBalanceField;
            }
            set
            {
                this.closingBalanceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string expenseToIncomeRatio
        {
            get
            {
                return this.expenseToIncomeRatioField;
            }
            set
            {
                this.expenseToIncomeRatioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string highestExpenseInAMonth
        {
            get
            {
                return this.highestExpenseInAMonthField;
            }
            set
            {
                this.highestExpenseInAMonthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string highestIncomeInAMonth
        {
            get
            {
                return this.highestIncomeInAMonthField;
            }
            set
            {
                this.highestIncomeInAMonthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string highestInvestmentExpenseInAMonth
        {
            get
            {
                return this.highestInvestmentExpenseInAMonthField;
            }
            set
            {
                this.highestInvestmentExpenseInAMonthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string highestInvestmentIncomeInAMonth
        {
            get
            {
                return this.highestInvestmentIncomeInAMonthField;
            }
            set
            {
                this.highestInvestmentIncomeInAMonthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string highestSavingsInAMonth
        {
            get
            {
                return this.highestSavingsInAMonthField;
            }
            set
            {
                this.highestSavingsInAMonthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string inwardChqBounces
        {
            get
            {
                return this.inwardChqBouncesField;
            }
            set
            {
                this.inwardChqBouncesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string inwardECSBounces
        {
            get
            {
                return this.inwardECSBouncesField;
            }
            set
            {
                this.inwardECSBouncesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string inwardOtherBounces
        {
            get
            {
                return this.inwardOtherBouncesField;
            }
            set
            {
                this.inwardOtherBouncesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lastMonthExpense
        {
            get
            {
                return this.lastMonthExpenseField;
            }
            set
            {
                this.lastMonthExpenseField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lastMonthIncome
        {
            get
            {
                return this.lastMonthIncomeField;
            }
            set
            {
                this.lastMonthIncomeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lastMonthInvestmentExpense
        {
            get
            {
                return this.lastMonthInvestmentExpenseField;
            }
            set
            {
                this.lastMonthInvestmentExpenseField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lastMonthInvestmentIncome
        {
            get
            {
                return this.lastMonthInvestmentIncomeField;
            }
            set
            {
                this.lastMonthInvestmentIncomeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lastMonthSavings
        {
            get
            {
                return this.lastMonthSavingsField;
            }
            set
            {
                this.lastMonthSavingsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lowestExpenseInAMonth
        {
            get
            {
                return this.lowestExpenseInAMonthField;
            }
            set
            {
                this.lowestExpenseInAMonthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lowestIncomeInAMonth
        {
            get
            {
                return this.lowestIncomeInAMonthField;
            }
            set
            {
                this.lowestIncomeInAMonthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lowestInvestmentExpenseInAMonth
        {
            get
            {
                return this.lowestInvestmentExpenseInAMonthField;
            }
            set
            {
                this.lowestInvestmentExpenseInAMonthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lowestInvestmentIncomeInAMonth
        {
            get
            {
                return this.lowestInvestmentIncomeInAMonthField;
            }
            set
            {
                this.lowestInvestmentIncomeInAMonthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lowestSavingsInAMonth
        {
            get
            {
                return this.lowestSavingsInAMonthField;
            }
            set
            {
                this.lowestSavingsInAMonthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string maxBalance
        {
            get
            {
                return this.maxBalanceField;
            }
            set
            {
                this.maxBalanceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string maxCredit
        {
            get
            {
                return this.maxCreditField;
            }
            set
            {
                this.maxCreditField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string maxDebit
        {
            get
            {
                return this.maxDebitField;
            }
            set
            {
                this.maxDebitField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string minBalance
        {
            get
            {
                return this.minBalanceField;
            }
            set
            {
                this.minBalanceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string minCredit
        {
            get
            {
                return this.minCreditField;
            }
            set
            {
                this.minCreditField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string minDebit
        {
            get
            {
                return this.minDebitField;
            }
            set
            {
                this.minDebitField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string numberOfTransactions
        {
            get
            {
                return this.numberOfTransactionsField;
            }
            set
            {
                this.numberOfTransactionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string openingBalance
        {
            get
            {
                return this.openingBalanceField;
            }
            set
            {
                this.openingBalanceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string outwardChqBounces
        {
            get
            {
                return this.outwardChqBouncesField;
            }
            set
            {
                this.outwardChqBouncesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string outwardECSBounces
        {
            get
            {
                return this.outwardECSBouncesField;
            }
            set
            {
                this.outwardECSBouncesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string outwardOtherBounces
        {
            get
            {
                return this.outwardOtherBouncesField;
            }
            set
            {
                this.outwardOtherBouncesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string salaryCredits
        {
            get
            {
                return this.salaryCreditsField;
            }
            set
            {
                this.salaryCreditsField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class RecurringExpenses
    {

        private RecurringExpensesRecurringExpense[] knownTypesField;

        private RecurringExpensesUTXn[] unknownTypesField;

        private RecurringExpensesRAXn[] recurringAmountsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("RecurringExpense", IsNullable = false)]
        public RecurringExpensesRecurringExpense[] KnownTypes
        {
            get
            {
                return this.knownTypesField;
            }
            set
            {
                this.knownTypesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("UTXn", IsNullable = false)]
        public RecurringExpensesUTXn[] UnknownTypes
        {
            get
            {
                return this.unknownTypesField;
            }
            set
            {
                this.unknownTypesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("RAXn", IsNullable = false)]
        public RecurringExpensesRAXn[] RecurringAmounts
        {
            get
            {
                return this.recurringAmountsField;
            }
            set
            {
                this.recurringAmountsField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class RecurringExpensesRecurringExpense
    {

        private RecurringExpensesRecurringExpenseTotalPerMonth[] monthwiseBreakupField;

        private RecurringExpensesRecurringExpenseKTXn[] xnsField;

        private string categoryField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("TotalPerMonth", IsNullable = false)]
        public RecurringExpensesRecurringExpenseTotalPerMonth[] MonthwiseBreakup
        {
            get
            {
                return this.monthwiseBreakupField;
            }
            set
            {
                this.monthwiseBreakupField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("KTXn", IsNullable = false)]
        public RecurringExpensesRecurringExpenseKTXn[] Xns
        {
            get
            {
                return this.xnsField;
            }
            set
            {
                this.xnsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string category
        {
            get
            {
                return this.categoryField;
            }
            set
            {
                this.categoryField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class RecurringExpensesRecurringExpenseTotalPerMonth
    {

        private string monthField;

        private string amountField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "gYearMonth")]
        public string month
        {
            get
            {
                return this.monthField;
            }
            set
            {
                this.monthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class RecurringExpensesRecurringExpenseKTXn
    {

        private System.DateTime dateField;

        private string chqNoField;

        private string narrationField;

        private string amountField;

        private string accountField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string chqNo
        {
            get
            {
                return this.chqNoField;
            }
            set
            {
                this.chqNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string narration
        {
            get
            {
                return this.narrationField;
            }
            set
            {
                this.narrationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string account
        {
            get
            {
                return this.accountField;
            }
            set
            {
                this.accountField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class RecurringExpensesUTXn
    {

        private string groupNoField;

        private System.DateTime dateField;

        private string chqNoField;

        private string narrationField;

        private string amountField;

        private string bankField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string groupNo
        {
            get
            {
                return this.groupNoField;
            }
            set
            {
                this.groupNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string chqNo
        {
            get
            {
                return this.chqNoField;
            }
            set
            {
                this.chqNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string narration
        {
            get
            {
                return this.narrationField;
            }
            set
            {
                this.narrationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string bank
        {
            get
            {
                return this.bankField;
            }
            set
            {
                this.bankField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class RecurringExpensesRAXn
    {

        private string groupNoField;

        private string slNoField;

        private string categoryField;

        private System.DateTime dateField;

        private string chqNoField;

        private string narrationField;

        private string amountField;

        private string bankField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string groupNo
        {
            get
            {
                return this.groupNoField;
            }
            set
            {
                this.groupNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string slNo
        {
            get
            {
                return this.slNoField;
            }
            set
            {
                this.slNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string category
        {
            get
            {
                return this.categoryField;
            }
            set
            {
                this.categoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string chqNo
        {
            get
            {
                return this.chqNoField;
            }
            set
            {
                this.chqNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string narration
        {
            get
            {
                return this.narrationField;
            }
            set
            {
                this.narrationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string bank
        {
            get
            {
                return this.bankField;
            }
            set
            {
                this.bankField = value;
            }
        }
    }
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class TDSs
    {
        private TDS[] tDSField;
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("TDS", IsNullable = false)]
        public TDS[] TDS
        {
            get
            {
                return this.tDSField;
            }
            set
            {
                this.tDSField = value;
            }
        }
       
    }
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class TDSDetails
    {
        private TDSs tDSsField;
        private string fyField;
        private string ayField;
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public TDSs TDSs
        {
            get
            {
                return this.tDSsField;
            }
            set
            {
                this.tDSsField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string fy
        {
            get
            {
                return this.ayField;
            }
            set
            {
                this.ayField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ay
        {
            get
            {
                return this.ayField;
            }
            set
            {
                this.ayField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class NonTDSDetails
    {
      
        private string fyField;
        private string ayField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string fy
        {
            get
            {
                return this.ayField;
            }
            set
            {
                this.ayField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ay
        {
            get
            {
                return this.ayField;
            }
            set
            {
                this.ayField = value;
            }
        }
    }
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Form26ASInfo
    {

        private PersonalDetails personalDetailsField;
        private TDSDetails[] tDSDetailsField;
        private NonTDSDetails[] nonTDSDetailsField;
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public PersonalDetails PersonalDetails
        {
            get
            {
                return this.personalDetailsField;
            }
            set
            {
                this.personalDetailsField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("TDSDetails", IsNullable = false)]
        public TDSDetails[] TDSDetails
        {
            get
            {
                return this.tDSDetailsField;
            }
            set
            {
                this.tDSDetailsField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("NonTDSDetails", IsNullable = false)]
        public NonTDSDetails[] NonTDSDetails
        {
            get
            {
                return this.nonTDSDetailsField;
            }
            set
            {
                this.nonTDSDetailsField = value;
            }
        }
    }
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Account
    {

        private AccountXN[] xnsField;

        private string bankField;

        private string instIdField;

        private string accountNoField;

        private string accountTypeField;

        private string ifscCodeField;
        private string locationField;

        private string micrCodeFiled;
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Xn", IsNullable = false)]
        public AccountXN[] Xns
        {
            get
            {
                return this.xnsField;
            }
            set
            {
                this.xnsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string bank
        {
            get
            {
                return this.bankField;
            }
            set
            {
                this.bankField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string instId
        {
            get
            {
                return this.instIdField;
            }
            set
            {
                this.instIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string accountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string accountType
        {
            get
            {
                return this.accountTypeField;
            }
            set
            {
                this.accountTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ifscCode
        {
            get
            {
                return this.ifscCodeField;
            }
            set
            {
                this.ifscCodeField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string location
        {
            get
            {
                return this.locationField;
            }
            set
            {
                this.locationField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string micrCode
        {
            get
            {
                return this.micrCodeFiled;
            }
            set
            {
                this.micrCodeFiled = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AccountXN
    {

        private System.DateTime dateField;

        private string chqNoField;

        private string narrationField;

        private string amountField;

        private string categoryField;

        private string balanceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string chqNo
        {
            get
            {
                return this.chqNoField;
            }
            set
            {
                this.chqNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string narration
        {
            get
            {
                return this.narrationField;
            }
            set
            {
                this.narrationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string category
        {
            get
            {
                return this.categoryField;
            }
            set
            {
                this.categoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string balance
        {
            get
            {
                return this.balanceField;
            }
            set
            {
                this.balanceField = value;
            }
        }
    }


}
