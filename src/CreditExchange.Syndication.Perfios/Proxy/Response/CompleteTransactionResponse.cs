﻿using System.Xml.Serialization;

namespace CreditExchange.Syndication.Perfios.Proxy.Response
{
  
    [XmlRoot(ElementName = "Success")]
    public class CompleteTransactionResponse
    {
        [XmlElement(ElementName = "message")]
        public string Message { get; set; }
    }
}
