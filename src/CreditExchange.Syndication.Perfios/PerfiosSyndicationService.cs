﻿using CreditExchange.Syndication.Perfios.Proxy;
using CreditExchange.Syndication.Perfios.Request;
using CreditExchange.Syndication.Perfios.Response;
using System;
using System.Threading.Tasks;
namespace CreditExchange.Syndication.Perfios
{
    public class PerfiosSyndicationService : IPerfiosSyndicationService
    {
        public PerfiosSyndicationService(IPerfiosConfiguration perfiosConfiguration,IPerfiosProxy perfiosProxy)
        {
            if (perfiosConfiguration == null)
                throw new ArgumentNullException(nameof(perfiosConfiguration));
            if (perfiosConfiguration.ApiVersion == null)
                throw new ArgumentException("ApiVersion is require", nameof(perfiosConfiguration.ApiVersion));
            if (perfiosConfiguration.VendorId == null)
                throw new ArgumentException("VendorId is require", nameof(perfiosConfiguration.VendorId));
            if (perfiosConfiguration.PrivateKey == null)
                throw new ArgumentException("PrivateKey is require", nameof(perfiosConfiguration.PrivateKey));
            if (perfiosProxy == null)
                throw new ArgumentNullException(nameof(perfiosProxy));

            PerfiosConfiguration = perfiosConfiguration;
            PerfiosProxy = perfiosProxy;
           
        }

     
        private IPerfiosConfiguration PerfiosConfiguration { get; }
        private IPerfiosProxy PerfiosProxy { get; }
        public async Task<string> StartProcessForm(IStartProcessRequest startProcessRequest)
        {

            var request = new Proxy.Request.StartProcessRequest(PerfiosConfiguration, startProcessRequest);
           return await PerfiosProxy.GenerateNetlinkingForm(request);
            //var referencenumber = Guid.NewGuid().ToString("N");
           

        }
        public async Task<IStartTransactionResponse> StartTransaction(IStartTransactionRequest startTransactionRequest)
        {
          
            var request = new Proxy.Request.StartTransactionRequest(PerfiosConfiguration, startTransactionRequest);
            var apiresponse =await PerfiosProxy.StartTransaction(request);
            //var referencenumber = Guid.NewGuid().ToString("N");
            var response = new StartTransactionResponse(apiresponse) { Referencenumber = request.TxnId, TransactionId = request.TxnId};
          
            return response;

        }
        public async Task<Proxy.Response.CompleteTransactionResponse> CompleteTranscation(string perfiosTransactionId)
        {          
            if (string.IsNullOrWhiteSpace(perfiosTransactionId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(perfiosTransactionId));

            var request = new Proxy.Request.CompleteTransactionRequest(PerfiosConfiguration, perfiosTransactionId);
            var apiresponse = await  PerfiosProxy.CompleteTransaction(request);
            return apiresponse;
        }
        public async Task<IReportResponse> RetrieveXmlReport(string perfiosTransactionId, string transactionId)
        {
                             
                if (string.IsNullOrWhiteSpace(perfiosTransactionId))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(perfiosTransactionId));
                if (string.IsNullOrWhiteSpace(transactionId))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(transactionId));

                var request = new Proxy.Request.RetrieveReportRequest(PerfiosConfiguration, perfiosTransactionId, transactionId);
                var proxyresponse = await PerfiosProxy.RetrieveReportXml(request);               
                var response = new ReportResponse(proxyresponse);               
                return response;
          
        }
        public async Task<IPdfReportResponse> RetrievePdfReport(string perfiosTransactionId, string transactionId)
        {
            if (string.IsNullOrWhiteSpace(perfiosTransactionId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(perfiosTransactionId));
            if (string.IsNullOrWhiteSpace(transactionId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(transactionId));

            var request = new Proxy.Request.RetrieveReportRequest(PerfiosConfiguration, perfiosTransactionId,transactionId);
            var proxyresponse= await PerfiosProxy.RetrieveReportPdf(request);
            var referencenumber = Guid.NewGuid().ToString("N");
            var response = new PdfReportResponse() { Report = proxyresponse, ReferenceNumber = referencenumber };
            return response;
        }
        public async Task<ITransactionStatusResponse> TransactionStatus(string transactionId)
        {
         
            if (string.IsNullOrWhiteSpace(transactionId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(transactionId));

            var request = new Proxy.Request.TransactionStatusRequest(PerfiosConfiguration, transactionId);
            var proxyresponse =await PerfiosProxy.TransactionStatus(request);          
            var response = new TransactionStatusResponse(proxyresponse);
            return response;
        }
        public async Task<IUploadStatementResponse> UploadStatement( string perfiosTransactionId, byte[] fileBytes,string password)
        {
          
            if (string.IsNullOrWhiteSpace(perfiosTransactionId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(perfiosTransactionId));

            var request = new Proxy.Request.UploadStatementRequest(PerfiosConfiguration, fileBytes,password);
            request.PerfiosTransactionId = perfiosTransactionId;
            var proxyresponse =await PerfiosProxy.UploadStatement(request);          
            var response = new UploadStatementResponse(proxyresponse);                      
            return response;
        }

        public async Task<ISupportedInstition> SupportedInstition()
        {
            var request = new Proxy.Request.ListSupportedInstitutionRequest(PerfiosConfiguration);
            var proxyresponse = await PerfiosProxy.ListsupportedInstitutions(request);           
            var response = new SupportedInstition(proxyresponse);           
            return response;
        }
        public async Task<IReviewTransactionResponse> ReviewTransaction(IReviewTransactionRequest reviewTransactionRequest)
        {
          
            var request = new Proxy.Request.ReviewTransactionRequest(PerfiosConfiguration, reviewTransactionRequest);
            var proxyresponse =await PerfiosProxy.ReviewTransaction(request);
            var referencenumber = Guid.NewGuid().ToString("N");
            var response = new ReviewTransactionResponse() {ReferenceNumber = referencenumber,Status = proxyresponse.Text };          
            return response;
        }
        public async Task<IDeletedataResponse> Deletedata(IDeletedataRequest deletedataRequest)
        {
           
            var request = new Proxy.Request.DeletedataRequest(PerfiosConfiguration, deletedataRequest);
            var proxyresponse =await PerfiosProxy.Deletedata(request);
            var referencenumber = Guid.NewGuid().ToString("N");
            var response = new DeletedataResponse() { ReferenceNumber = referencenumber, Status = proxyresponse.Text };
          
            return response;
        }

    }
}
