﻿using CreditExchange.Syndication.Cibil;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace CreditExchange.Cibil.Persistence
{
    public interface ICibilRepository : IRepository<ICibilData>
    {
        Task<ICibilData> GetByEntityId(string entityType, string entityId);
    }
}
