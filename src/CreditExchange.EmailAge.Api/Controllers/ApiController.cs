﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.EmailAge;
using Microsoft.AspNet.Mvc;
using LendFoundry.Foundation.Logging;

namespace CreditExchange.EmailAge.Api.Controllers
{

    public class ApiController : ExtendedController
    {
        public ApiController(IEmailAgeService service,ILogger logger):base(logger)
        {
            Service = service;
        }
        private IEmailAgeService Service { get; }

        [HttpGet("{entitytype}/{entityid}/{email}/verify-email")]
        public async Task<IActionResult> VerifyEmail(string entityType, string entityId, string  email)
        {
            return await ExecuteAsync(async () => Ok(await Service.VerifyEmail(entityType, entityId, email)));
        }
       
    }
}
