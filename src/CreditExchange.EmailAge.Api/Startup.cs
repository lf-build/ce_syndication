﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using CreditExchange.Syndication.EmailAge;
using CreditExchange.Syndication.EmailAge.Proxy;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Lookup.Client;

namespace CreditExchange.EmailAge.Api
{
    public class Startup
    {
      // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();

            // interface implements
            services.AddConfigurationService<EmailAgeConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);

            // Configuration factory
            //services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<CreditInformationHighMarkConfiguration>>().Get());

            services.AddTransient<IEmailAgeConfiguration>(p =>
            {
                var configuration = p.GetService<IConfigurationService<EmailAgeConfiguration>>().Get();
                return configuration ;
            });
            services.AddLookupService(Settings.LookupService.Host, Settings.LookupService.Port);
            services.AddTransient<IEmailAgeProxy, EmailAgeProxy>();
            services.AddTransient<IEmailAgeService, EmailAgeService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseHealthCheck();
        }

       
    }
}
