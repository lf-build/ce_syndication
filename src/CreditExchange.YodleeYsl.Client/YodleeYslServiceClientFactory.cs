﻿using CreditExchange.YodleeYsl.Abstractions;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;

namespace CreditExchange.YodleeYsl.Client
{
    public class YodleeYslServiceClientFactory :IYodleeYslServiceClientFactory
    {
        public YodleeYslServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public IYodleeYslService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new YodleeYslService(client);
        }
    }
}
