﻿using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace CreditExchange.YodleeYsl.Client
{
    public static class YodleeYslServiceClientExtension
    {
        public static IServiceCollection AddYodleeYslService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IYodleeYslServiceClientFactory>(p => new YodleeYslServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IYodleeYslServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
