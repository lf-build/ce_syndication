﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CreditExchange.Syndication.YodleeYsl;
using CreditExchange.Syndication.YodleeYsl.Request;
using CreditExchange.Syndication.YodleeYsl.Response;
using RestSharp;
using LendFoundry.Foundation.Client;
using CreditExchange.YodleeYsl.Abstractions;

namespace CreditExchange.YodleeYsl.Client
{
    public class YodleeYslService : IYodleeYslService
    {
        public YodleeYslService(IServiceClient client)
        {
            Client = client;
        }
        private IServiceClient Client { get; }
        public async Task<IFinalReport> FetchReport(string entityType, string entityId)
        {
            var request = new RestRequest("/{entityType}/{entityId}/finalreport", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);

            return await Client.ExecuteAsync<FinalReport>(request);
        }

        public Task<IBankLoginResponse> GetBankLoginForm(string entityType, string entityId, string providerId)
        {
            throw new NotImplementedException();
        }

        public async Task<IGetFastLinkUrlResponse> GetFalstLinkUrl(string entityType, string entityId, FastLinkUrlRequest fastlinkReuest)
        {
            var request = new RestRequest("/{entityType}/{entityid}/fastlink-url", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(fastlinkReuest);
            return await Client.ExecuteAsync<GetFastLinkUrlResponse>(request);
        }


        public async Task<IFastlinkAccessTokenResponse> GetFastlinkAccessToken(string entityType, string entityId)
        {
            var request = new RestRequest("/{entityType}/{entityid}/fastlink-access-token", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            
            return await Client.ExecuteAsync<FastlinkAccessTokenResponse>(request);
        }

        public async Task<IList<IItemSummaries>> GetItemSummaries(string entityType, string entityId)
        {
            var request = new RestRequest("/{entityType}/{entityId}/itemsummary", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            return await Client.ExecuteAsync<List<IItemSummaries>>(request);
        }



        public async Task<IGetBankAccountResponse> GetUserBankAccounts(string entityType, string entityId)
        {
            var request = new RestRequest("{entityType}/{entityid}/user-bank-accounts", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);

            return await Client.ExecuteAsync<GetBankAccountResponse>(request);
        }
        public IMfaRefreshResponse GetMfaStatusWithRefreshInformation(string cobrandSessionId, string userSessionId, string providerAccountId)
        {
            throw new NotImplementedException();
        }

        public IFinalRefreshResponse GetRefreshStatus(string cobrandSessionId, string userSessionId, string providerAccountId)
        {
            throw new NotImplementedException();
        }
        public async Task<ICobrandLoginResponse> LoginForCobrand(string entityType, string entityId)
        {
            var restRequest = new RestRequest("{entityType}/{entityId}/cobrand-login", Method.POST);
            restRequest.AddUrlSegment("entityType", entityType);
            restRequest.AddUrlSegment("entityId", entityId);
            
            return await Client.ExecuteAsync<CobrandLoginResponse>(restRequest);
        }

        public async Task<IUserLoginResponse> LoginUser(string entityType, string entityId)
        {
            var restRequest = new RestRequest("{entityType}/{entityId}/user-login", Method.POST);
            restRequest.AddUrlSegment("entityType", entityType);
            restRequest.AddUrlSegment("entityId", entityId);
            
            return await Client.ExecuteAsync<UserLoginResponse>(restRequest);
        }

        public Task<IProviderAccountRefreshStatus> PostBankLoginForm(string entityType, string entityId, string providerId, IUpdateBankLogin loginForm)
        {
            throw new NotImplementedException();
        }

        public async Task<IUserRegistrationResponse> RegisterUser(string entityType, string entityId, IUserRegistrationRequest request)
        {
            var restRequest = new RestRequest("{entityType}/{entityId}/register", Method.POST);
            restRequest.AddUrlSegment("entityType", entityType);
            restRequest.AddUrlSegment("entityId", entityId);
            restRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<UserRegistrationResponse>(restRequest);
        }

        public Task<ISearchBankResponse> SearchBank(string entityType, string entityId, string searchName)
        {
            throw new NotImplementedException();
        }

        public string SendMfaDetails(string cobrandSessionId, string userSessionId, string providerAccountId, dynamic mfaChallenge)
        {
            throw new NotImplementedException();
        }

        public string StartRefreshStatus(string cobrandSessionId, string userSessionId, string providerAccountId)
        {
            throw new NotImplementedException();
        }

        public async Task<IFinalReport> GetReport(string entityType, string entityId)
        {
            var request = new RestRequest("/{entityType}/{entityId}/getreport", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);

            return await Client.ExecuteAsync<FinalReport>(request);
        }

        public async Task<string> RemoveLinkAccount(string entityType, string entityId)
        {
            var request = new RestRequest("/{entityType}/{entityId}/delete-link-Account", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);

            return await Client.ExecuteAsync<string>(request);
        }

        public async Task<IYodleeData> GetStatus(string entityType, string entityId)
        {
            var request = new RestRequest("/{entityType}/{entityId}/status", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);

            return await Client.ExecuteAsync<YodleeData>(request);
        }
    }
}
