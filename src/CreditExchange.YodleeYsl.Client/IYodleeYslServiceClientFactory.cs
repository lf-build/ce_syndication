﻿using CreditExchange.YodleeYsl.Abstractions;
using LendFoundry.Security.Tokens;

namespace CreditExchange.YodleeYsl.Client
{
    public interface IYodleeYslServiceClientFactory
    {
        IYodleeYslService Create(ITokenReader reader);
    }
}
