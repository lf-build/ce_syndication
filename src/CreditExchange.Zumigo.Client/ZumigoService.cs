﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using CreditExchange.Syndication.Zumigo.Events;
using CreditExchange.Syndication.Zumigo.Request;
using CreditExchange.Syndication.Zumigo.Response;
using RestSharp;
using CreditExchange.Syndication.Zumigo;
using System;
using System.Collections.Generic;

namespace CreditExchange.Zumigo.Client
{
    public class ZumigoService : IZumigoService
    {
        public ZumigoService(IServiceClient client)
        {
            Client = client;
        }
        private IServiceClient Client { get; }
        public async Task<IDeviceAdded> AddDevice(string entitytype, string entityid, string mobileDeviceNumber)
        {
            var request = new RestRequest("device/{entitytype}/{entityid}/{mobileDeviceNumber}", Method.POST);
            request.AddUrlSegment("entitytype", entitytype);
            request.AddUrlSegment("entityid", entityid);
            request.AddUrlSegment("mobileDeviceNumber", mobileDeviceNumber);
           return  await Client.ExecuteAsync<ZumigoDeviceAdded>(request);
        }

        public async Task DeleteDevice(string entitytype, string entityid, string mobileDeviceNumber)
        {
            var request = new RestRequest("device/{entitytype}/{entityid}/{mobileDeviceNumber}", Method.DELETE);
            request.AddUrlSegment("entitytype", entitytype);
            request.AddUrlSegment("entityid", entityid);
            request.AddUrlSegment("mobileDeviceNumber", mobileDeviceNumber);
            await Client.ExecuteAsync(request);
        }

        public async Task<IGetDeviceStatusResponse> GetDeviceStatus(string entitytype, string entityid,string mobileDeviceNumber)
        {
            var request = new RestRequest("device/{entitytype}/{entityid}/{mobileDeviceNumber}/status", Method.GET);
            request.AddUrlSegment("entitytype", entitytype);
            request.AddUrlSegment("entityid", entityid);
            request.AddUrlSegment("mobileDeviceNumber", mobileDeviceNumber);
            return await Client.ExecuteAsync<GetDeviceStatusResponse>(request);
        }

        public async Task<IGetMobileLocationAndAddressResponse> GetDeviceLocationAddress(string entitytype, string entityid,string mobileDeviceNumber)
        {
            var request = new RestRequest("device/{entitytype}/{entityid}/{mobileDeviceNumber}/location", Method.GET);
            request.AddUrlSegment("entitytype", entitytype);
            request.AddUrlSegment("entityid", entityid);
            request.AddUrlSegment("mobileDeviceNumber", mobileDeviceNumber);
            return await Client.ExecuteAsync<GetMobileLocationAndAddressResponse>(request);
        }

        public async Task<IValidateOnlineTransactionResponse> VerifyDeviceLocation(string entityType, string entityId, string mobileDeviceNumber, IValidateOnlineTransactionRequest validateOnlineTransactionRequest)
        {
            var request = new RestRequest("device/{entitytype}/{entityid}/{mobileDeviceNumber}/location/is-valid", Method.POST);
            request.AddJsonBody(validateOnlineTransactionRequest);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddUrlSegment("mobileDeviceNumber", mobileDeviceNumber);
            return await Client.ExecuteAsync<ValidateOnlineTransactionResponse>(request);
        }
        public async Task<List<IZumigoRetryData>> GetRetryRequest()
        {
            var request = new RestRequest("retry", Method.GET);         
            var result = await Client.ExecuteAsync<List<ZumigoRetryData>>(request);
            return new List<IZumigoRetryData>(result);
        }
        
    }
}
