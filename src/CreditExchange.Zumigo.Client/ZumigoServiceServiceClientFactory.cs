﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using CreditExchange.Syndication.Zumigo;

namespace CreditExchange.Zumigo.Client
{
    public class ZumigoServiceServiceClientFactory : IZumigoServiceClientFactory
    {
        public ZumigoServiceServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public IZumigoService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new ZumigoService(client);
        }

       
    }
}
