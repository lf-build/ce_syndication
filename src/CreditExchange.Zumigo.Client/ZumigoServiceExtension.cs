﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Zumigo.Client
{
    public static class ZumigoServiceExtension
    {
        public static IServiceCollection AddZumigoServiceExtension(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<IZumigoServiceClientFactory>(p => new ZumigoServiceServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IZumigoServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
