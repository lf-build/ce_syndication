﻿using CreditExchange.Syndication.Zumigo;
using LendFoundry.Security.Tokens;

namespace CreditExchange.Zumigo.Client
{
    public interface IZumigoServiceClientFactory
    {
        IZumigoService Create(ITokenReader reader);
    }
}
