﻿using System.Threading.Tasks;
using CreditExchange.Lenddo.Abstractions;
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using CreditExchange.Syndication.Lenddo;

namespace CreditExchange.Lenddo.Persistence
{
    public class LenddoRepository :MongoRepository<ILenddoData,LenddoData> ,ILenddoRepository
    {
        static LenddoRepository()
        {
            BsonClassMap.RegisterClassMap<LenddoData>(map =>
            {
                map.AutoMap();
                var type = typeof(LenddoData);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }
        public LenddoRepository(LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "lenddo-data")
        {
            CreateIndexIfNotExists("lenddo-data",
                Builders<ILenddoData>.IndexKeys.Ascending(i => i.EntityType).Ascending(i => i.EntityId));
        }
        public async Task<ILenddoData> GetByEntityId(string entityType, string entityId)
        {
            return await Query.FirstOrDefaultAsync(a => a.EntityType == entityType && a.EntityId == entityId);
        }

       
    }
}
