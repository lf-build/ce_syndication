﻿using LendFoundry.Security.Tokens;

namespace CreditExchange.Perfios.Client
{
   public interface IPerfiosServiceClientFactory
    {
        IPerfiosService Create(ITokenReader reader);
    }
}
