﻿using System;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Client;

namespace CreditExchange.Perfios.Client
{
    public class PerfiosServiceClientFactory : IPerfiosServiceClientFactory
    {
        public PerfiosServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public IPerfiosService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new PerfiosService(client);
        }
    }
}
