﻿using System;
using System.Threading.Tasks;
#if DOTNET2
using  Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.Crif;
using CreditExchange.Syndication.Crif.CreditReport;
using CreditExchange.Syndication.Crif.CreditReport.Request;
using LendFoundry.Foundation.Logging;
using Newtonsoft.Json;

namespace CreditExchange.Crif.CreditReport.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(ICrifCreditReportService crifCreditReportService,ILogger logger):base(logger)
        {
            if (crifCreditReportService == null)
                throw new ArgumentNullException(nameof(crifCreditReportService));

            CrifCreditReportService = crifCreditReportService;
        }

        private ICrifCreditReportService CrifCreditReportService { get; }
       
        [HttpPost("{entityType}/{entityId}/inquiry")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.Crif.CreditReport.Response.ICreditReportInquiryResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> SendInquiry(string entityType, string entityId, [FromBody] CreditReportInquiryRequest inquiryRequest)
        {
            Logger.Info($"[CRIF] SendInquiry endpoint invoked for [{entityType}] with id #{entityId} with  data { (inquiryRequest != null ? JsonConvert.SerializeObject(inquiryRequest) : null)}");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return
                        Ok(await CrifCreditReportService.SendInquiry(entityType, entityId, inquiryRequest));
                }
                catch (CrifException exception)
                {
                    Logger.Error($"[CRIF] Error occured when SendInquiry endpoint was called for [{entityType}] with id #{entityId} with  data { (inquiryRequest != null ? JsonConvert.SerializeObject(inquiryRequest) : null)}", exception);
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        [HttpPost("{entityType}/{entityId}/report")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.Crif.CreditReport.Response.IGetCreditReportResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetReport(string entityType, string entityId, [FromBody] CreditReportInquiryRequest inquiryRequest)
        {
            Logger.Info($"[CRIF] SendInquiry GetReport endpoint invoked for [{entityType}] with id #{entityId} with  data { (inquiryRequest != null ? JsonConvert.SerializeObject(inquiryRequest) : null)}");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await CrifCreditReportService.GetReport(entityType, entityId, inquiryRequest));
                }
                catch (CrifException exception)
                {
                    Logger.Error($"[CRIF] Error occured when GetReport endpoint was called for [{entityType}] with id #{entityId} with  data { (inquiryRequest != null ? JsonConvert.SerializeObject(inquiryRequest) : null)}", exception);
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        [HttpGet("{entityType}/{entityId}/{inquiryreferencenumber}/{reportid}")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.Crif.CreditReport.Response.IGetCreditReportResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetCRIFReport(string entityType, string entityId, string inquiryreferencenumber, string reportid)
        {
            Logger.Info($"[CRIF] SendInquiry GetCRIFReport endpoint invoked for [{entityType}] with id #{entityId} with inquiryreferencenumber :{inquiryreferencenumber} and reportid :{reportid}");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok( await CrifCreditReportService.GetReport(entityType, entityId, inquiryreferencenumber, reportid));
                }
                catch (CrifException exception)
                {
                    Logger.Error($"[CRIF] Error occured when GetCRIFReport endpoint was called for [{entityType}] with id #{entityId} with  with inquiryreferencenumber :{inquiryreferencenumber} and reportid :{reportid}", exception);
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
    }
}
