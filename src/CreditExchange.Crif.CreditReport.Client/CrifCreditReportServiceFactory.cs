﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using CreditExchange.Syndication.Crif.CreditReport;

namespace CreditExchange.Crif.CreditReport.Client
{
    public class CrifCreditReportServiceFactory : ICrifCreditReportServiceFactory
    {
        public CrifCreditReportServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public ICrifCreditReportService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new CrifCreditReportService(client);
        }

    }
}
