﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace CreditExchange.Crif.CreditReport.Client
{
    public static class CrifCreditReportServiceExtensions
    {
        public static IServiceCollection AddCrifCreditReportServiceExtensions(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<ICrifCreditReportServiceFactory>(p => new CrifCreditReportServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ICrifCreditReportServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
