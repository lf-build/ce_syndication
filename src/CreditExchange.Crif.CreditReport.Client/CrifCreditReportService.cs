﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using CreditExchange.Syndication.Crif.CreditReport;
using CreditExchange.Syndication.Crif.CreditReport.Request;
using CreditExchange.Syndication.Crif.CreditReport.Response;
using RestSharp;

namespace CreditExchange.Crif.CreditReport.Client
{
    public class CrifCreditReportService : ICrifCreditReportService
    {
        public CrifCreditReportService(IServiceClient client)
        {
            Client = client;
        }
        private IServiceClient Client { get; }

       
        
        public async Task<IGetCreditReportResponse> GetReport(string entityType, string entityId, string inquiryReferenceNumber, string reportId)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/{inquiryreferencenumber}/{reportid}", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddUrlSegment("inquiryreferencenumber", inquiryReferenceNumber);
            request.AddUrlSegment("reportid", reportId);
            return await Client.ExecuteAsync<GetCreditReportResponse>(request);
        }

        public async Task<IGetCreditReportResponse> GetReport(string entityType, string entityId, ICreditReportInquiryRequest sendInquiryRequest)
        {
            var request = new RestRequest("{entitytype}/{entityid}/report", Method.POST);
            request.AddJsonBody(sendInquiryRequest);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<GetCreditReportResponse>(request);
        }

        public async Task<ICreditReportInquiryResponse> SendInquiry(string entityType, string entityId, ICreditReportInquiryRequest inquiryRequest)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/inquiry", Method.POST);
            request.AddJsonBody(inquiryRequest);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<CreditReportInquiryResponse>(request);
        }
    }
}
