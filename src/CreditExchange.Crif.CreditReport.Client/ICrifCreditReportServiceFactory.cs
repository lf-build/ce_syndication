﻿using LendFoundry.Security.Tokens;
using CreditExchange.Syndication.Crif.CreditReport;

namespace CreditExchange.Crif.CreditReport.Client
{
    public interface ICrifCreditReportServiceFactory
    {
        ICrifCreditReportService Create(ITokenReader reader);
    }
}