using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using CreditExchange.Syndication.Zumigo;
using CreditExchange.Syndication.Zumigo.Proxy;
using LendFoundry.Tenant.Client;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif

using LendFoundry.Foundation.Lookup.Client;
using CreditExchange.Zumigo.Persistance;
using LendFoundry.VerificationEngine.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Configuration;

namespace CreditExchange.Zumigo.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            #if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Zumigo"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "CreditExchange.Zumigo.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddConfigurationService<ZumigoConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddVerificationEngineService(Settings.VerificationEngine.Host, Settings.VerificationEngine.Port);
            services.AddTransient<IZumigoConfiguration>(p =>  p.GetService<IConfigurationService<ZumigoConfiguration>>().Get());
            services.AddTransient<IZumigoRetryRepository, ZumigoRetryRepository>();
            services.AddTransient<IZumigoTryRepository, ZumigoTryRepository>();
            services.AddTransient<IZumigoService, ZumigoService>();
            services.AddTransient<IZumigoProxy, ZumigoProxy>();          
            services.AddTransient<IZumigoRetryRepositoryFactory, ZumigoRetryRepositoryFactory>();
            services.AddTransient<IZumigoTryRepositoryFactory, ZumigoTryRepositoryFactory>();
                 
            services.AddTransient<IZumigoListener, ZumigoListener>();
            services.AddLookupService(Settings.LookupService.Host, Settings.LookupService.Port);
         
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            #if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "MobileVerification Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseZumigoListener();
            app.UseMvc();
            app.UseHealthCheck();
        }
    }
}

