﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Zumigo.Api
{
    public static class Settings
    {
        public static string ServiceName { get; } = "zumigo";
        private static string Prefix { get; } = ServiceName.ToUpper();
        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "zumigo");
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");
        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");
        public static ServiceSettings LookupService { get; } = new ServiceSettings($"{Prefix}_LOOKUPSERVICE", "lookup-service");
        public static ServiceSettings VerificationEngine { get; } = new ServiceSettings($"{Prefix}_VERIFICATIONENGINE_HOST", "verification-engine", $"{Prefix}_VERIFICATIONENGINE_PORT");
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
    }
}
