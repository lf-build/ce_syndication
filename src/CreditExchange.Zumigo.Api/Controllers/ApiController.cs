﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.Zumigo;
using CreditExchange.Syndication.Zumigo.Request;
#if DOTNET2
using  Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using LendFoundry.Foundation.Logging;

namespace CreditExchange.Zumigo.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IZumigoService service,ILogger logger):base(logger)
        {
            ZumigoService = service;
        }

        private static NoContentResult NoContentResult { get; } = new NoContentResult();
        private IZumigoService ZumigoService { get; }

        [HttpPost("device/{entitytype}/{entityid}/{mobileDeviceNumber}")]
        #if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.Zumigo.Events.IDeviceAdded), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> AddDevice(string entitytype, string entityid, string mobileDeviceNumber)
        {
            Logger.Info($"[Zumigo] AddDevice endpoint invoked to add a Mobile device No. for [{entitytype}] with id #{entityid} with mobile device no. #{mobileDeviceNumber}");
            return await ExecuteAsync(async () =>
                {
                    try
                    {
                        var response = await ZumigoService.AddDevice(entitytype, entityid, mobileDeviceNumber);
                        return Ok(response);
                    }
                    catch (ZumigoException exception)
                    {
                        Logger.Error($"[Zumigo] Error occured when AddDevice endpoint was called for [{entitytype}] with id #{entityid} with mobile device no. #{mobileDeviceNumber}", exception);
                        return ErrorResult.BadRequest(exception.Message);
                    }
                }
            );
        }

        [HttpDelete("device/{entitytype}/{entityid}/{mobileDeviceNumber}")]
        #if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> DeleteDevice(string entitytype, string entityid, string mobileDeviceNumber)
        {
            Logger.Info($"[Zumigo] DeleteDevice endpoint invoked to delete a Mobile device No. for [{entitytype}] with id #{entityid} with mobile device no. #{mobileDeviceNumber}");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    await ZumigoService.DeleteDevice(entitytype, entityid, mobileDeviceNumber);
                    return NoContentResult;
                }
                catch (ZumigoException exception)
                {
                    Logger.Error($"[Zumigo] Error occured when DeleteDevice endpoint was called for [{entitytype}] with id #{entityid} with mobile device no. #{mobileDeviceNumber}", exception);
                    return ErrorResult.BadRequest(exception.Message);
                }
            });

        }

        [HttpGet("device/{entitytype}/{entityid}/{mobileDeviceNumber}/status")]
        #if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.Zumigo.Response.IGetDeviceStatusResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetDeviceStatus(string entitytype, string entityid, string mobileDeviceNumber)
        {
            Logger.Info($"[Zumigo] GetDeviceStatus endpoint invoked for [{entitytype}] with id #{entityid} with mobile device no. #{mobileDeviceNumber}");
            return await ExecuteAsync(async () =>
            {
                try
                {

                    return Ok(await ZumigoService.GetDeviceStatus(entitytype, entityid, mobileDeviceNumber));
                }
                catch (ZumigoException exception)
                {
                    Logger.Error($"[Zumigo] Error occured when GetDeviceStatus endpoint was called for [{entitytype}] with id #{entityid} with mobile device no. #{mobileDeviceNumber}", exception);
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        [HttpGet("device/{entitytype}/{entityid}/{mobileDeviceNumber}/location")]
        #if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.Zumigo.Response.IGetMobileLocationAndAddressResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetMobileLocationAndAddress(string entitytype, string entityid,
            string mobileDeviceNumber)
        {
            Logger.Info($"[Zumigo] GetMobileLocationAndAddress endpoint invoked for [{entitytype}] with id #{entityid} with mobile device no. #{mobileDeviceNumber}");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await ZumigoService.GetDeviceLocationAddress(entitytype, entityid, mobileDeviceNumber));
                }
                catch (ZumigoException exception)
                {
                    Logger.Error($"[Zumigo] Error occured when GetMobileLocationAndAddress endpoint was called for [{entitytype}] with id #{entityid} with mobile device no. #{mobileDeviceNumber}", exception);
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        [HttpPost("device/{entitytype}/{entityid}/{mobileDeviceNumber}/location/is-valid")]
        #if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.Zumigo.Response.IValidateOnlineTransactionResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> ValidateOnlineTransaction(string entitytype, string entityid, string mobileDeviceNumber,[FromBody] ValidateOnlineTransactionRequest request)
        {
            Logger.Info($"[Zumigo] ValidateOnlineTransaction endpoint invoked to validate transaction for [{entitytype}] with id #{entityid} with mobile device no. #{mobileDeviceNumber}");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await ZumigoService.VerifyDeviceLocation(entitytype, entityid, mobileDeviceNumber,request));
                }
                catch (ZumigoException exception)
                {
                    Logger.Error($"[Zumigo] Error occured when ValidateOnlineTransaction endpoint was called to validate transaction for [{entitytype}] with id #{entityid} with mobile device no. #{mobileDeviceNumber}", exception);
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        [HttpGet("retry")]
        #if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.Zumigo.IZumigoRetryData), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetRetryData()         
        {
            Logger.Info($"[Zumigo] GetRetryData endpoint invoked");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await ZumigoService.GetRetryRequest());
                }
                catch (ZumigoException exception)
                {
                    Logger.Error($"[Zumigo] Error occured when GetRetryData endpoint was called", exception);
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
    }
}
