﻿namespace CreditExchange.Syndication.Abstractions
{
    public class Response : IResponse
    {
        public Response()
        {

        }
        public Response(object response)
        {
            SyndicationResponse = response;
        }
        public object SyndicationResponse { get; set; }
    }
}
