﻿using System.Threading.Tasks;

namespace CreditExchange.Syndication.Abstractions
{
    public interface ISyndication
    {
      Task<IResponse> InitializeSyndication(string entityType, string entityId, IRequest payload);     
       Task<IResponse> ProcessSyndication(string entityType, string entityId, IRequest payload);
    }
}
