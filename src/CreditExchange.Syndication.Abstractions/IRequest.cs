﻿namespace CreditExchange.Syndication.Abstractions
{
    public interface IRequest
    {
       object SyndicationRequest { get; set; }
    }
}
