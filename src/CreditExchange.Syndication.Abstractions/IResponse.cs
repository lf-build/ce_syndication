﻿namespace CreditExchange.Syndication.Abstractions
{
    public interface IResponse
    {
        object SyndicationResponse { get; set; }
    }
}
