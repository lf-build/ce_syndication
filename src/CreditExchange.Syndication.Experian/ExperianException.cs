﻿using System;
using System.Runtime.Serialization;

namespace CreditExchange.Syndication.Experian
{
    [Serializable]
    public class ExperianException : Exception
    {
        public ExperianException()
        {
        }

        public ExperianException(string message) : base(message)
        {
        }

        public ExperianException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected ExperianException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
