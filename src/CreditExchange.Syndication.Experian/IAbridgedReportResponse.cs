﻿namespace CreditExchange.Syndication.Experian
{
    public interface IAbridgedReportResponse
    {
        string Cctrades { get; set; }
        string ExactScore { get; set; }
        string HomeLoan { get; set; }
        string LiveDPDMoreThan30 { get; set; }
        string LiveDPDMoreThan60 { get; set; }
        string LiveHL_DPDMoreThan90 { get; set; }
        string LivePL_DPDMoreThan90 { get; set; }
        string NonLiveDPDMoreThan30 { get; set; }
        string NonLiveDPDMoreThan60 { get; set; }
        string Pltrades { get; set; }
        int StgOneHitId { get; set; }
        string ErrorString { get; set; }
    }
}