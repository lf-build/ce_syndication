﻿using System.Threading.Tasks;

namespace CreditExchange.Syndication.Experian.Proxy
{
    public interface IExperianProxy
    {
        Task<AbridgedReportResponse> GetAbridgedReport(string request);
        Task<SingleActionResponse> InvokeSingleAction(string request);
    }

}