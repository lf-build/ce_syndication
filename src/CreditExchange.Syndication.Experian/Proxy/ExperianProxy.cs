﻿using LendFoundry.Foundation.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Experian.Proxy
{
    public class ExperianProxy :IExperianProxy
    {
        public ExperianProxy(ExperianConfiguration configuration, ILogger logger)
        {
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));
          
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            Configuration = configuration;
            Logger = logger;
           
        }
        private ILogger Logger { get; }
       
        private ExperianConfiguration Configuration { get; }

       public async Task<SingleActionResponse> InvokeSingleAction(string request)
       {
            if (string.IsNullOrEmpty(request))
                throw new ArgumentNullException(nameof(request));
            var client = new RestClient(Configuration.SingleActionURL);
            var restRequest = new RestRequest(Method.POST);
           
            restRequest.AddParameter("text/plain", request, ParameterType.RequestBody);
            var objectResponse = await ExecuteRequestAsync<SingleActionResponse>(client, restRequest);
            Logger.Info($"[ExperianProxy] InvokeSingleAction method response: ", objectResponse);
            return objectResponse;
        }
        public async Task<AbridgedReportResponse> GetAbridgedReport(string request)
        {
            if (string.IsNullOrEmpty(request))
                throw new ArgumentNullException(nameof(request));
            var client = new RestClient(Configuration.AbridgedReportURL);
            var restRequest = new RestRequest(Method.POST);
            restRequest.AddParameter("text/plain", request, ParameterType.RequestBody);
            var objectResponse = await ExecuteRequestAsync<AbridgedReportResponse>(client, restRequest);
            Logger.Info($"[ExperianProxy] GetAbridgedReport method response: ", objectResponse);
            return objectResponse;
        }
        private async Task<T> ExecuteRequestAsync<T>(IRestClient client, IRestRequest request) where T : class
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            var response = await client.ExecuteTaskAsync(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new ExperianException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new ExperianException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
                    throw new ExperianException(
                        $"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");
            }

            return JsonConvert.DeserializeObject<T>(response.Content);
        }
    }
}
