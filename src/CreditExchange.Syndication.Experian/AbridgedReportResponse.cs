﻿namespace CreditExchange.Syndication.Experian
{
    public class AbridgedReportResponse :IAbridgedReportResponse
    {
        public int StgOneHitId { get; set; }
        public string ExactScore { get; set; }
        public string NonLiveDPDMoreThan30 { get; set; }
        public string NonLiveDPDMoreThan60 { get; set; }
        public string LiveDPDMoreThan30 { get; set; }
        public string LiveDPDMoreThan60 { get; set; }
        public string LivePL_DPDMoreThan90 { get; set; }
        public string LiveHL_DPDMoreThan90 { get; set; }
        public string Cctrades { get; set; }
        public string Pltrades { get; set; }
        public string HomeLoan { get; set; }

        public string ErrorString { get; set; }
    }
}
