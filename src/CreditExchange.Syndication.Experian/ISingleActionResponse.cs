﻿namespace CreditExchange.Syndication.Experian
{
    public interface ISingleActionResponse
    {
        string ErrorString { get; set; }
        int NewUserId { get; set; }
        string ShowHtmlReportForCreditReport { get; set; }
        string StageOneId_ { get; set; }
        string StageTwoId_ { get; set; }
        string SubscriptionMsg { get; set; }
    }
}