﻿using System.Threading.Tasks;

namespace CreditExchange.Syndication.Experian
{
    public interface IExperianSyndicationService
    {
        Task<IAbridgedReportResponse> GetAbridgedReport(string entityType, string entityId, string request);
        Task<ISingleActionResponse> InvokeSingleAction(string entityType, string entityId, string request);
    }
}