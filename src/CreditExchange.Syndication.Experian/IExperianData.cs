﻿using LendFoundry.Foundation.Persistence;
using System;

namespace CreditExchange.Syndication.Experian
{
    public interface IExperianData : IAggregate
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
        string Request { get; set; }
        string Status { get; set; }
        string Response { get; set; }
        string Api { get; set; }
        DateTimeOffset ProcessDate { get; set; }
    }
}