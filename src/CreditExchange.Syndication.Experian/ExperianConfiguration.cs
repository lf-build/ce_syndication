﻿namespace CreditExchange.Syndication.Experian
{
    public class ExperianConfiguration
    {
        public string SingleActionURL { get; set; }
        public string AbridgedReportURL { get; set; }
        public string EmailConditionalByPass { get; set; }
        public string AllowVoucher { get; set; }
        public string AllowInput { get; set; }
        public string AllowEmailVerify { get; set; }
        public string AllowConsent { get; set; }
        public string NoValidationByPass {get;set;}
        public string AllowConsent_additional { get; set; }
        public string AllowCaptcha { get; set; }
        public string VoucherCode { get; set; }
        public string ClientName { get; set; }
       
        public string AllowEdit { get; set; }
        public string ExperianRequestBuilderRule { get; set; }
    }
}
