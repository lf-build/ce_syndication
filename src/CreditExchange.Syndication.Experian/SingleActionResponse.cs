﻿namespace CreditExchange.Syndication.Experian
{
    public class SingleActionResponse :ISingleActionResponse
    {
        public string ErrorString { get; set; }
        public string StageOneId_ { get; set; }
        public string StageTwoId_ { get; set; }
        public string ShowHtmlReportForCreditReport { get; set; }
        public int NewUserId { get; set; }
        public string SubscriptionMsg { get; set; }
    }
}
