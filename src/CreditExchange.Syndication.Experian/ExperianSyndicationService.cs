﻿using CreditExchange.Syndication.Experian.Proxy;
using System;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Experian
{
    public class ExperianSyndicationService :IExperianSyndicationService
    {
        public ExperianSyndicationService(ExperianConfiguration configuration, IExperianProxy proxy)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));

            Configuration = configuration;
            Proxy = proxy;

        }
        private IExperianProxy Proxy { get; }

        private ExperianConfiguration Configuration { get; }

        public async Task<ISingleActionResponse> InvokeSingleAction(string entityType, string entityId, string request)
        {
            try
            {
              
                var result = await Proxy.InvokeSingleAction(request);
               return result;


            }
            catch (ExperianException exception)
            {
                throw new ExperianException(exception.Message);
            }

        }

        public async Task<IAbridgedReportResponse> GetAbridgedReport(string entityType, string entityId, string request)
        {
            try
            {

                return await Proxy.GetAbridgedReport(request);
            }
            catch (ExperianException exception)
            {
                throw new ExperianException(exception.Message);
            }
        }
    }
}
