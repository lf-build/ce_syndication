﻿using LendFoundry.SyndicationStore.Events;

namespace CreditExchange.Syndication.Experian
{
    public class SingleActionPullFailed : SyndicationCalledEvent
    { 
    }
}
