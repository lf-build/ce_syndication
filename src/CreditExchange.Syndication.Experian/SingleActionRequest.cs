﻿namespace CreditExchange.Syndication.Experian
{
    public class SingleActionRequest :ISingleActionRequest
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string SurName { get; set; }
        public string DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string Flatno { get; set; }
        public string BuildingName { get; set; }
        public string Road { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Pincode { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string Pan { get; set; }

    }
}
 