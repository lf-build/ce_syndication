﻿namespace CreditExchange.Syndication.Experian
{
    public interface ISingleActionRequest
    {
        string BuildingName { get; set; }
        string City { get; set; }
        string DateOfBirth { get; set; }
        string Email { get; set; }
        string FirstName { get; set; }
        string Flatno { get; set; }
        string Gender { get; set; }
        string MiddleName { get; set; }
        string MobileNo { get; set; }
        string Pan { get; set; }
        string Pincode { get; set; }
        string Road { get; set; }
        string State { get; set; }
        string SurName { get; set; }
    }
}