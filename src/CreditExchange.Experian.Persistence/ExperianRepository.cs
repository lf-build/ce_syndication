﻿using CreditExchange.Syndication.Experian;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Experian.Persistence
{
    public class ExperianRepository : MongoRepository<IExperianData, ExperianData>, IExperianRepository
    {
        static ExperianRepository()
        {
            BsonClassMap.RegisterClassMap<ExperianData>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.ProcessDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
              
                var type = typeof(ExperianData);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public ExperianRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "experianData")
        {
            CreateIndexIfNotExists("experianData",
            Builders<IExperianData>.IndexKeys.Ascending(i => i.EntityType).Ascending(i => i.EntityId));
        }
        public async Task<IExperianData> GetByEntityId(string entityType, string entityId,string Api)
        {
            return await Query.FirstOrDefaultAsync(a => a.EntityType == entityType && a.EntityId == entityId && a.Api == Api);
        }
      
    }
}
