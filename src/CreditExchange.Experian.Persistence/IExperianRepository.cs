﻿using System.Threading.Tasks;
using CreditExchange.Syndication.Experian;
using LendFoundry.Foundation.Persistence;

namespace CreditExchange.Experian.Persistence
{
    public interface IExperianRepository : IRepository<IExperianData>
    {
        Task<IExperianData> GetByEntityId(string entityType, string entityId, string Api);
    }
}