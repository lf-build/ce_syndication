﻿using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;


namespace CreditExchange.Probe.Persistence
{
    public interface IProbeRepository : IRepository<IProbeData>
    {
        Task<IProbeData> GetByEntityId(string entityType, string entityId, string Api);
    }
}