﻿using LendFoundry.Foundation.Persistence;
using System;

namespace CreditExchange.Probe.Persistence
{
    public class ProbeData :Aggregate,IProbeData
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
       public string Api { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
        public DateTimeOffset ProcessDate { get; set; }
        public string Status { get; set; }
    }
}
