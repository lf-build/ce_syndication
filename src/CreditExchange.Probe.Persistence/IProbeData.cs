﻿using LendFoundry.Foundation.Persistence;
using System;

namespace CreditExchange.Probe.Persistence
{
    public interface IProbeData :IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        DateTimeOffset ProcessDate { get; set; }
        string Request { get; set; }
        string Response { get; set; }
        string Api { get; set; }
        string Status { get; set; }
    }
}