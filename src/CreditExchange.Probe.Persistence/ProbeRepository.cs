﻿
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Probe.Persistence
{
    public class ProbeRepository : MongoRepository<IProbeData, ProbeData> ,IProbeRepository
    {
        static ProbeRepository()
        {
            BsonClassMap.RegisterClassMap<ProbeData>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.ProcessDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                
                var type = typeof(ProbeData);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }
        public ProbeRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "probeData")
        {
            CreateIndexIfNotExists("experianData",
           Builders<IProbeData>.IndexKeys.Ascending(i => i.EntityType).Ascending(i => i.EntityId));
        }
      
        public async Task<IProbeData> GetByEntityId(string entityType, string entityId,string Api)
        {
            return await Query.FirstOrDefaultAsync(a => a.EntityType == entityType && a.EntityId == entityId && a.Api== Api);
        }
    }
}
