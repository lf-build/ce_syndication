﻿using LendFoundry.Foundation.Services;
#if DOTNET2
using  Microsoft.AspNetCore.Mvc;
using LendFoundry.EventHub;
#else
using Microsoft.AspNet.Mvc;
using LendFoundry.EventHub.Client;
#endif
using System.Threading.Tasks;
using CreditExchange.EmailHunter.Api.Events;
using CreditExchange.Syndication.EmailHunter;
using System;
using LendFoundry.Foundation.Logging;
using Newtonsoft.Json;

namespace CreditExchange.EmailHunter.Api.Controllers
{
    //TODO: Change order of Route as per standard
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IEmailHunterService service, IEventHubClient eventHub, ILogger logger):base (logger)
        {
            Service = service;
            EventHub = eventHub;
          
        }
        private IEmailHunterService Service { get; }
        private IEventHubClient EventHub { get; }
       

        [HttpGet("search-domain/{entitytype}/{entityid}/{domain}")]
#if DOTNET2
        [ProducesResponseType(typeof(IDomainSearchResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> DomainSearch(string entitytype, string entityid, string domain)
        {
            try
            {
                Logger.Info($"[Email Hunter] DomainSearch endpoint invoked for [{entitytype}] with id #{entityid} and   domain : {domain}");
                return await ExecuteAsync(async () =>
                {
                    var referenceNumber = Guid.NewGuid().ToString("N");
                    var response = (await Service.DomainSearch(entitytype,entityid, domain));
                    if (response != null)
                    {
                        Logger.Info($"[Email Hunter] Publishing DomainSearchAdded event for entityId : {entityid} and entityType :{entitytype}");
                        await EventHub.Publish(new DomainSearchAdded()
                        {
                            EntityId = entityid,
                            EntityType = entitytype,
                            Response = response,
                            Request = domain,
                            ReferenceNumber = referenceNumber,
                            Name = Settings.ServiceName

                        });
                    }
                    return Ok(response);
                });
            }
            catch (EmailHunterException exception)
            {

                Logger.Error($"[Email Hunter] Error occured when DomainSearch endpoint was called for [{entitytype}] with id #{entityid} with   domain : {domain}", exception);
                return ErrorResult.BadRequest(exception.Message);
            }
            
        
          
        }
      
        [HttpPost("search-email")]
#if DOTNET2
        [ProducesResponseType(typeof(IEmailFinderResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> EmailFinder([FromBody]EmailFinder emailFinder)
        {
            Logger.Info($"[Email Hunter] EmailFinder endpoint invoked for data : { (emailFinder != null ? JsonConvert.SerializeObject(emailFinder) : null)}");
            try
            {
                return await ExecuteAsync(async () =>
                {
                    var referenceNumber = Guid.NewGuid().ToString("N");
                    var response = (await Service.EmailFinder(emailFinder));
                    if (response != null)
                    {
                        Logger.Info($"[Email Hunter] Publishing EmailFinderAdded event for response :  { (response != null ? JsonConvert.SerializeObject(response) : null)}");
                        await EventHub.Publish(new EmailFinderAdded()
                        {
                            EntityId = emailFinder.EntityId,
                            EntityType = emailFinder.EntityType,
                            Response = response,
                            Request = emailFinder,
                            ReferenceNumber = referenceNumber,
                            Name = Settings.ServiceName

                        });
                    }
                    return Ok(response);
                });
            }
            catch (EmailHunterException exception)
            {
                Logger.Error($"[Email Hunter] Error occured when EmailFinder endpoint was called for data :{ (emailFinder != null ? JsonConvert.SerializeObject(emailFinder) : null)}", exception);
                return ErrorResult.BadRequest(exception.Message);
            }
           
            
        }

        [HttpGet("verify-email/{entitytype}/{email}")]
#if DOTNET2
        [ProducesResponseType(typeof(IEmailVerificationResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> EmailVerifier(string entitytype, string email)
        {
            Logger.Info($"[Email Hunter] EmailVerifier endpoint invoked for  [{entitytype}] and email : {email}");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var referenceNumber = Guid.NewGuid().ToString("N");
                    var decodedString = System.Net.WebUtility.UrlDecode(email);
                    var response = (await Service.EmailVerifier(entitytype, decodedString));
                    if (response != null)
                    {
                        Logger.Info($"[Email Hunter] Publishing EmailVerificationAdded event for [{entitytype}] and email : {email}");
                        await EventHub.Publish(new EmailVerificationAdded()
                        {
                            EntityId = null,
                            EntityType = entitytype,
                            Response = response,
                            Request = email,
                            ReferenceNumber = referenceNumber,
                            Name = Settings.ServiceName

                        });
                    }
                    return Ok(response);
                }
                catch (EmailHunterException exception)
                {
                    Logger.Error($"[Email Hunter] Error occured when EmailVerifier endpoint was called for  [{entitytype}] and email : {email}", exception);
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        [HttpGet("EmailCount/{entitytype}/{entityid}/{domain}")]
#if DOTNET2
        [ProducesResponseType(typeof(IEmailCountResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> EmailCount(string entitytype, string entityid, string domain)
        {
            Logger.Info($"[Email Hunter] EmailCount endpoint invoked for  [{entitytype}] with id #{entityid} and   domain : {domain}");
            try
            {
                return await ExecuteAsync(async () =>
                {
                    var referenceNumber = Guid.NewGuid().ToString("N");
                    var response = (await Service.EmailCount(entitytype,entityid,  domain));
                    if (response != null)
                    {
                        Logger.Info($"[Email Hunter] Publishing EmailCountAdded event for [{entitytype}] with id #{entityid} ");
                        await EventHub.Publish(new EmailCountAdded()
                        {
                            EntityId = entityid,
                            EntityType = entitytype,
                            Response = response,
                            Request = domain,
                            ReferenceNumber = referenceNumber,
                            Name = Settings.ServiceName

                        });
                    }
                    return Ok(response);
                });
            }
            catch (EmailHunterException exception)
            {
                Logger.Error($"[Email Hunter] Error occured when EmailCount endpoint was called for   [{entitytype}] with id #{entityid} and domain : {domain}", exception);
                return ErrorResult.BadRequest(exception.Message);
            }

           
           
        }
      

    }
}
