﻿using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.EmailHunter.Configuration;
using LendFoundry.Configuration.Client;
using CreditExchange.Syndication.EmailHunter.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Date;
using LendFoundry.Tenant.Client;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif
using CreditExchange.Syndication.EmailHunter;
using LendFoundry.Foundation.Lookup.Client;

namespace CreditExchange.EmailHunter.Api
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "EmailHunter"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "CreditExchange.EmailHunter.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();

            // interface implements
            services.AddConfigurationService<EmailHunterConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);

            // Configuration factory
            services.AddTransient< IEmailHunterConfiguration>(p =>
            {
                var configuration = p.GetService<IConfigurationService<EmailHunterConfiguration>>().Get();
                return configuration;
            });
            services.AddLookupService(Settings.LookupService.Host, Settings.LookupService.Port);
            services.AddTransient<IEmailHunterProxy, EmailHunterProxy>();
            services.AddTransient<IEmailHunterService, EmailHunterService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "MobileVerification Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseHealthCheck();
        }

      
    }
}
