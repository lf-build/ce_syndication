﻿
using LendFoundry.Syndication.Crif.CreditReport.Proxy.Inquiry;
using LendFoundry.Syndication.Crif.CreditReport.Proxy.Inquiry.Response;
using LendFoundry.Syndication.Crif.CreditReport.Proxy.Issue;

using LendFoundry.Syndication.Crif.CreditReport.Proxy.Issue.Response;

namespace LendFoundry.Syndication.Crif.CreditReport.Proxy
{
    public interface ICrifCreditReportProxy
    {
        AcknowledgementResponse SendInquiry(SendInquiryRequest inquiryRequest);
        ReportResponse IssueReport(ReportRequest reportRequest);
       
    }
}