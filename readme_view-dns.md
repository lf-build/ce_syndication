**Image Name**: registry.lendfoundry.com/ce-syndication-view-dns **Ports**:5111

##### Availalbe End Points:

1. Verify employment work email address.

   ```
   - GET (/{entityType}/{entityId}/{emailAddress}/{companyName}/{cin})  
   ```

**Configurations**

```
Configurations for view-dns

{
  "apiBaseUrl": "http://pro.viewdns.info",
  "whoisUrl": "whois",
  "apiOutputType": "json",
  "apiKey": "7eb568767fd1afa11296d82cd7becedf5593c4da",
  "reverseWhoisUrl": "reversewhois"
}

```